﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.Helpers
{
    public static class EnumHelper
    {
        public static string GetName<T>(T v)
        {
            return Enum.GetName(typeof(T), v);
        }

        public static string[] GetNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T Parse<T>(string p0)
        {
            return (T)Enum.Parse(typeof(T), p0);
        }

        public static T TryParse<T>(string p0, T defaultValue) where T : struct
        {
            T r;
            if (Enum.TryParse<T>(p0, out r))
                return r;
            return defaultValue;
        }
    }
}