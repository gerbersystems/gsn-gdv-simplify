﻿using System;

namespace GSN.GDV.Helpers
{
    public static class ValidationHelper
    {
        public static bool IsInteger(this object Value)
        {
            if (Value == null)
                return false;
            try
            {
                Convert.ToInt64(Value);
                return true;
            }
            catch (FormatException)
            {
            }
            catch (InvalidCastException)
            {
            }
            catch (OverflowException) { }

            long v;
            return long.TryParse(Value.ToString(), out v);
        }

        public static bool IsNumeric(this object Value)
        {
            if (Value == null)
                return false;
            try
            {
                Convert.ToDouble(Value);
                return true;
            }
            catch (FormatException) { }
            catch (InvalidCastException) { }
            catch (OverflowException) { }

            double v;
            return double.TryParse(Value.ToString(), out v);
        }

        public static bool IsDate(this object Value)
        {
            return IsDateTime(Value);
        }

        public static bool IsTime(this object Value)
        {
            return IsDateTime(Value);
        }

        public static bool IsDateTime(this object Value)
        {
            if (Value == null)
                return false;
            try
            {
                Convert.ToDateTime(Value);
                return true;
            }
            catch (FormatException) { }
            catch (InvalidCastException) { }
            catch (OverflowException) { }

            DateTime v;
            return DateTime.TryParse(Value.ToString(), out v);
        }
    }
}