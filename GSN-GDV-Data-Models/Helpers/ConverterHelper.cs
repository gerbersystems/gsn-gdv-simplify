﻿namespace GSN.GDV.Helpers
{
    public static class ConverterHelper
    {
        public static int To(object v, int defaultValue = 0)
        {
            if (v == null)
                return defaultValue;
            var r = defaultValue;
            if (int.TryParse(v.ToString(), out r))
                return r;
            return defaultValue;
        }

        public static decimal To(object v, decimal defaultValue = 0)
        {
            if (v == null)
                return defaultValue;
            var r = defaultValue;
            if (decimal.TryParse(v.ToString(), out r))
                return r;
            return defaultValue;
        }

        public static double To(object v, double defaultValue = 0)
        {
            if (v == null)
                return defaultValue;
            var r = defaultValue;
            if (double.TryParse(v.ToString(), out r))
                return r;
            return defaultValue;
        }
    }
}