namespace GSN.GDV.Data.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Fields_In_Parcelle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PAR_PARCELLE", "ZOT_ID", c => c.Int());
            AddColumn("dbo.PAR_PARCELLE", "PAR_SECTEUR_VISITE", c => c.String());
            AddColumn("dbo.PAR_PARCELLE", "PAR_MODE_CULTURE", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PAR_PARCELLE", "PAR_MODE_CULTURE");
            DropColumn("dbo.PAR_PARCELLE", "PAR_SECTEUR_VISITE");
            DropColumn("dbo.PAR_PARCELLE", "ZOT_ID");
        }
    }
}
