// <auto-generated />
namespace GSN.GDV.Data.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class FirstVersionMainDataContextDbMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FirstVersionMainDataContextDbMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201408211310424_FirstVersionMainDataContextDbMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
