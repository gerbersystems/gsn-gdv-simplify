﻿using GSN.GDV.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using LocalSettings = GSN.GDV.Data.Models.Properties.Settings;

namespace GSN.GDV.Data.Contextes
{
    [DbModelBuilderVersion(DbModelBuilderVersion.Latest)]
    public class MainDataContext : DbContext, IMainDataContext
    {
        public static string AvailableConnectionString = LocalSettings.Default.MainDataContext;

        static MainDataContext()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MainDataContext, GSN.GDV.Data.Models.Migrations.Configuration>());
        }

        private readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(MainDataContext));

        public MainDataContext()
            : this(AvailableConnectionString)
        {
            //IMainDataContext o=this;
            //int v=o.SaveChanges();
        }

        public MainDataContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            //Log.InfoFormat("using connection : {0}", nameOrConnectionString);
        }

        public MainDataContext(System.Data.Common.DbConnection existingConnection, bool contextOwnConenction = false)
            : base(existingConnection, contextOwnConenction)
        {
        }

        public virtual DbSet<ProprietaireModel> ProprietaireSet { get; set; }

        public virtual DbSet<LieuProductionModel> LieuProductionSet { get; set; }

        public virtual DbSet<AutreMentionModel> AutreMentionSet { get; set; }

        public virtual DbSet<RegionModel> RegionSet { get; set; }

        public virtual DbSet<CepageModel> CepageSet { get; set; }

        public virtual DbSet<AnneeModel> AnneeSet { get; set; }

        public virtual DbSet<ParametreModel> ParametreSet { get; set; }

        public virtual DbSet<ParcelleModel> ParcelleSet { get; set; }

        public virtual DbSet<AcquitModel> AcquitSet { get; set; }

        public virtual DbSet<PeseeEnteteModel> PeseeEnteteSet { get; set; }

        public virtual DbSet<PeseeDetailModel> PeseeDetailSet { get; set; }

        public virtual DbSet<CouleurModel> CouleurSet { get; set; }

        public virtual DbSet<QuotationProductionModel> QuotationProductionSet { get; set; }

        public virtual DbSet<AbstractSettingModel> SettingSet { get; set; }

        public virtual DbSet<StringSettingModel> StringSettingSet { get; set; }

        public virtual DbSet<IntegerSettingModel> IntegerSettingSet { get; set; }

        public virtual DbSet<DecimalSettingModel> DecimalSettingSet { get; set; }

        public virtual DbSet<BooleanSettingModel> BooleanSettingSet { get; set; }

        public virtual DbSet<DateTimeSettingModel> DateTimeSettingSet { get; set; }

        public virtual DbSet<ArticleModel> ArticleSet { get; set; }

        public virtual DbSet<ResponsableModel> ResponsableSet { get; set; }

        public virtual DbSet<CommuneModel> CommuneSet { get; set; }

        public virtual DbSet<DistrictModel> DistrictSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Log.InfoFormat("Entity Mapping ");

            CepageModel_OnModelCreating(cfg: modelBuilder.Entity<CepageModel>());

            AnneeModel_OnModelCreating(cfg: modelBuilder.Entity<AnneeModel>());

            QuotationProductionModel_OnModelCreating(cfg: modelBuilder.Entity<QuotationProductionModel>());

            SettingModel_OnModelCreating(cfg: modelBuilder.Entity<AbstractSettingModel>());

            AcquitModel_OnModelCreating(cfg: modelBuilder.Entity<AcquitModel>());

            ParcelleModel_OnModelCreating(cfg: modelBuilder.Entity<ParcelleModel>());

            ParametreModel_OnModelCreating(cfg: modelBuilder.Entity<ParametreModel>());

            PeseeDetailModel_OnModelCreating(cfg: modelBuilder.Entity<PeseeDetailModel>());

            PeseeEnteteModel_OnModelCreating(cfg: modelBuilder.Entity<PeseeEnteteModel>());

            CouleurModel_OnModelCreating(cfg: modelBuilder.Entity<CouleurModel>());

            ProprietaireModel_OnModelCreating(cfg: modelBuilder.Entity<ProprietaireModel>());

            LieuProductionModel_OnModelCreating(cfg: modelBuilder.Entity<LieuProductionModel>());

            RegionModel_OnModelCreating(cfg: modelBuilder.Entity<RegionModel>());

            AutreMentionModel_OnModelCreating(cfg: modelBuilder.Entity<AutreMentionModel>());

            ArticleModel_OnModelCreating(cfg: modelBuilder.Entity<ArticleModel>());

            ResponsableModel_OnModelCreating(cfg: modelBuilder.Entity<ResponsableModel>());

            CommuneModel_OnModelCreating(cfg: modelBuilder.Entity<CommuneModel>());

            DistrictModel_OnModelCreating(cfg: modelBuilder.Entity<DistrictModel>());

            base.OnModelCreating(modelBuilder);
        }

        private void DistrictModel_OnModelCreating(EntityTypeConfiguration<DistrictModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            Log.InfoFormat("Entity Configuration :" + cfg);

            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                });
                m.ToTable("DIS_DISTRICT");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("DIS_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("DIS_NOM").HasMaxLength(50).HasColumnType("varchar");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void CommuneModel_OnModelCreating(EntityTypeConfiguration<CommuneModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            Log.InfoFormat("Entity Configuration :" + cfg);

            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                    t.Numero,
                    t.DistrictId,
                });
                m.ToTable("COM_COMMUNE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("COM_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("COM_NOM").HasMaxLength(50).HasColumnType("varchar");
            cfg.Property(t => t.DistrictId).HasColumnName("DIS_ID");
            cfg.Property(t => t.Numero).HasColumnName("COM_NUMERO");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void ResponsableModel_OnModelCreating(EntityTypeConfiguration<ResponsableModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            Log.InfoFormat("Entity Configuration :" + cfg);

            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.AdresseId,
                    t.Nom,
                    t.Prenom,
                    t.IsDefault,
                });
                m.ToTable("RES_RESPONSABLE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("RES_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.AdresseId).HasColumnName("ADR_ID");
            cfg.Property(t => t.Nom).HasColumnName("RES_NOM").HasMaxLength(50).HasColumnType("varchar");
            cfg.Property(t => t.Prenom).HasColumnName("RES_PRENOM").HasMaxLength(50).HasColumnType("varchar");
            cfg.Property(t => t.IsDefault).HasColumnName("RES_DEFAULT").HasColumnType("bit");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void ArticleModel_OnModelCreating(EntityTypeConfiguration<ArticleModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            Log.InfoFormat("Entity Configuration :" + cfg);

            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.AnneeId,
                    t.Litres,
                    t.Designation,
                    t.Code,
                    t.Groupe,
                    t.CouleurId,
                    t.WinBizId,
                    t.ModeComptabilisationId,
                    t.LitresEnStock,
                    t.Version,
                });
                m.ToTable("ART_ARTICLE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("ART_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.AnneeId).HasColumnName("ANN_ID");
            cfg.Property(t => t.Litres).HasColumnName("ART_LITRES");
            cfg.Property(t => t.Designation.Courte).HasColumnName("ART_DESIGNATION_COURTE").HasMaxLength(50);
            cfg.Property(t => t.Designation.Longue).HasColumnName("ART_DESIGNATION_LONGUE").HasMaxLength(50);
            cfg.Property(t => t.Code).HasColumnName("ART_CODE").HasMaxLength(30);
            cfg.Property(t => t.Groupe.Code).HasColumnName("ART_CODE_GROUPE").HasMaxLength(15);
            cfg.Property(t => t.CouleurId).HasColumnName("COU_ID");
            cfg.Property(t => t.WinBizId).HasColumnName("ART_ID_WINBIZ");
            cfg.Property(t => t.ModeComptabilisationId).HasColumnName("MOC_ID");
            cfg.Property(t => t.LitresEnStock).HasColumnName("ART_LITRES_STOCK");
            cfg.Property(t => t.Version).HasColumnName("ART_VERSION");
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void AutreMentionModel_OnModelCreating(EntityTypeConfiguration<AutreMentionModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                });
                m.ToTable("AUT_AUTRE_MENTION");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("AUT_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("AUT_NOM").HasMaxLength(50);
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void RegionModel_OnModelCreating(EntityTypeConfiguration<RegionModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                });
                m.ToTable("REG_REGION");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("REG_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("REG_NAME").HasMaxLength(50);
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void LieuProductionModel_OnModelCreating(EntityTypeConfiguration<LieuProductionModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                    t.Numero,
                    t.RegionId,
                });
                m.ToTable("LIE_LIEU_DE_PRODUCTION");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("LIE_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("LIE_NOM").HasMaxLength(50);
            cfg.Property(t => t.RegionId).HasColumnName("REG_ID");
            cfg.Property(t => t.Numero).HasColumnName("LIE_NUMERO");
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void ProprietaireModel_OnModelCreating(EntityTypeConfiguration<ProprietaireModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.AddressId,
                    t.Nom,
                    t.Prenom,
                    t.Titre,
                    t.SCTE,
                    t.IsProprietaire,
                    t.IsProducteur,
                    t.TVA,
                    t.WinBiz,
                });
                m.ToTable("PRO_PROPRIETAIRE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("PRO_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.AddressId).HasColumnName("ADR_ID");
            cfg.Property(t => t.Nom).HasColumnName("PRO_NOM").HasMaxLength(50);
            cfg.Property(t => t.Prenom).HasColumnName("PRO_PRENOM").HasMaxLength(50);
            cfg.Property(t => t.Titre).HasColumnName("PRO_TITRE").HasMaxLength(50);
            cfg.Property(t => t.SCTE).HasColumnName("PRO_SCTE").HasMaxLength(50);
            cfg.Property(t => t.IsProprietaire).HasColumnName("PRO_IS_PROPRIETAIRE");
            cfg.Property(t => t.IsProducteur).HasColumnName("PRO_IS_PRODUCTEUR");
            cfg.Property(t => t.WinBiz.AddressId).HasColumnName("ADR_ID_WINBIZ");
            cfg.Property(t => t.TVA.Numero).HasColumnName("PRO_NUMERO_TVA").HasMaxLength(50);
            cfg.Property(t => t.TVA.ModeId).HasColumnName("MOD_ID");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void CouleurModel_OnModelCreating(EntityTypeConfiguration<CouleurModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.Nom,
                    t.HaveCepage,
                });
                m.ToTable("COU_COULEUR");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("COU_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.Nom).HasColumnName("COU_NOM");
            cfg.Property(t => t.HaveCepage).HasColumnName("COU_CEPAGE");
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void ParametreModel_OnModelCreating(EntityTypeConfiguration<ParametreModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.PAM_ID,
                    t.ANN_ID,
                    t.PAM_CLE,
                    t.PAM_HASH_MACHINE,
                    t.PAM_NUM_ENCAVEUR,
                    t.PAM_EXPIRATION,
                    t.PAM_VERSION_BD,
                    t.PAM_LAST_LICENSE,
                    t.PAM_WINBIZ_ADRESSE_DEFAUT,
                    t.PAM_WINBIZ_ADRESSE_CURRENT,
                    t.PAM_HASH_VERSION,
                    t.PAM_EXPIRATION_UPDATE,
                    t.PAM_NB_PRODUCTEUR,
                    t.PAM_MACHINE_NAME /*,
					t.PAM_DEFAULT_KG_LT_BLANC,
					t.PAM_DEFAULT_KG_LT_ROUGE,*/
                });
                m.ToTable("PAM_PARAMETRES");
            });
            cfg.HasKey(t => t.PAM_ID);
            cfg.Property(t => t.PAM_ID).HasColumnName("PAM_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.ANN_ID).HasColumnName("ANN_ID");
            cfg.Property(t => t.PAM_CLE).HasColumnName("PAM_CLE");
            cfg.Property(t => t.PAM_HASH_MACHINE).HasColumnName("PAM_HASH_MACHINE");
            cfg.Property(t => t.PAM_NUM_ENCAVEUR).HasColumnName("PAM_NUM_ENCAVEUR");
            cfg.Property(t => t.PAM_EXPIRATION).HasColumnName("PAM_EXPIRATION");
            cfg.Property(t => t.PAM_VERSION_BD).HasColumnName("PAM_VERSION_BD");
            cfg.Property(t => t.PAM_LAST_LICENSE).HasColumnName("PAM_LAST_LICENSE");
            cfg.Property(t => t.PAM_WINBIZ_ADRESSE_DEFAUT).HasColumnName("PAM_WINBIZ_ADRESSE_DEFAUT");
            cfg.Property(t => t.PAM_WINBIZ_ADRESSE_CURRENT).HasColumnName("PAM_WINBIZ_ADRESSE_CURRENT");
            cfg.Property(t => t.PAM_HASH_VERSION).HasColumnName("PAM_HASH_VERSION");
            cfg.Property(t => t.PAM_EXPIRATION_UPDATE).HasColumnName("PAM_EXPIRATION_UPDATE");
            cfg.Property(t => t.PAM_NB_PRODUCTEUR).HasColumnName("PAM_NB_PRODUCTEUR");
            cfg.Property(t => t.PAM_MACHINE_NAME).HasColumnName("PAM_MACHINE_NAME");
            //cfg.Property(t => t.PAM_DEFAULT_KG_LT_BLANC).HasColumnName("PAM_DEFAULT_KG_LT_BLANC");
            //cfg.Property(t => t.PAM_DEFAULT_KG_LT_ROUGE).HasColumnName("PAM_DEFAULT_KG_LT_ROUGE");
            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void ParcelleModel_OnModelCreating(EntityTypeConfiguration<ParcelleModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.CepageId,
                    t.AcquitId,
                    t.Folio,
                    t.Numero,
                    t.SurfaceCepage,
                    t.Quota,
                    t.Litres,
                    t.SurfaceComptee,
                    t.LieuId,
                    t.AutreMentionId,
                    t.ZoneTravailId,
                    t.SecteurVisite,
                    t.ModeCulture,
                });
                m.ToTable("PAR_PARCELLE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("PAR_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.CepageId).HasColumnName("CEP_ID");
            cfg.Property(t => t.AcquitId).HasColumnName("ACQ_ID");
            cfg.Property(t => t.Folio).HasColumnName("PAR_FOLIO");
            cfg.Property(t => t.Numero).HasColumnName("PAR_NUMERO");
            cfg.Property(t => t.SurfaceCepage).HasColumnName("PAR_SURFACE_CEPAGE");
            cfg.Property(t => t.Quota).HasColumnName("PAR_QUOTA");
            cfg.Property(t => t.Litres).HasColumnName("PAR_LITRES");
            cfg.Property(t => t.SurfaceComptee).HasColumnName("PAR_SURFACE_COMPTEE");
            cfg.Property(t => t.LieuId).HasColumnName("LIE_ID");
            cfg.Property(t => t.AutreMentionId).HasColumnName("AUT_ID");
            cfg.Property(t => t.ZoneTravailId).HasColumnName("ZOT_ID");
            cfg.Property(t => t.SecteurVisite).HasColumnName("PAR_SECTEUR_VISITE");
            cfg.Property(t => t.ModeCulture).HasColumnName("PAR_MODE_CULTURE");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        private void AcquitModel_OnModelCreating(EntityTypeConfiguration<AcquitModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.ClasseId,
                    t.CouleurId,
                    t.AnneeId,
                    t.CommuneId,
                    t.ProprietaireId,
                    t.Numero,
                    t.Date,
                    t.Surface,
                    t.Litres,
                    t.RegionId,
                    t.NumeroComplet,
                    t.ProducteurId,
                    t.Reception,
                });
                m.ToTable("ACQ_ACQUIT");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("ACQ_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.ClasseId).HasColumnName("CLA_ID");
            cfg.Property(t => t.CouleurId).HasColumnName("COU_ID");
            cfg.Property(t => t.AnneeId).HasColumnName("ANN_ID");
            cfg.Property(t => t.CommuneId).HasColumnName("COM_ID");
            cfg.Property(t => t.ProprietaireId).HasColumnName("PRO_ID");
            cfg.Property(t => t.Numero).HasColumnName("ACQ_NUMERO");
            cfg.Property(t => t.Date).HasColumnName("ACQ_DATE");
            cfg.Property(t => t.Surface).HasColumnName("ACQ_SURFACE");
            cfg.Property(t => t.Litres).HasColumnName("ACQ_LITRES");
            cfg.Property(t => t.RegionId).HasColumnName("REG_ID");
            cfg.Property(t => t.NumeroComplet).HasColumnName("ACQ_NUMERO_COMPL");
            cfg.Property(t => t.ProducteurId).HasColumnName("PRO_ID_VENDANGE");
            cfg.Property(t => t.Reception).HasColumnName("ACQ_RECEPTION");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        protected virtual void CepageModel_OnModelCreating(EntityTypeConfiguration<CepageModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new { t.Id, t.CouleurId, t.Numero, t.Nom });
                m.ToTable("CEP_CEPAGE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("CEP_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.CouleurId).HasColumnName("COU_ID");
            cfg.Property(t => t.Nom).HasColumnName("CEP_NOM");
            cfg.Property(t => t.Numero).HasColumnName("CEP_NUMERO");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        protected virtual void AnneeModel_OnModelCreating(EntityTypeConfiguration<AnneeModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.AddressId,
                    t.Annee,
                    t.ModeContabiliteId,
                    t.IsPrixKillograme,
                    t.Responsable1,
                    t.Responsable2,
                    t.Responsable3,
                    t.Responsable4,
                    t.Encaveur,
                    t.Encaveur.Numero,
                    t.Encaveur.Nom,
                    t.Encaveur.Prenom,
                    t.Encaveur.ControlSoumis,
                    t.Encaveur.Societe,
                    t.TauxConversion,
                    t.TauxConversion.Rouge,
                    t.TauxConversion.Blanc,
                    t.TauxConversionMout,
                    t.TauxConversionMout.BlancMout,
                    t.TauxConversionMout.RougeMout,
                });
                m.ToTable("ANN_ANNEE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("ANN_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.AddressId).HasColumnName("ADR_ID");
            cfg.Property(t => t.Annee).HasColumnName("ANN_AN");
            cfg.Property(t => t.Encaveur.Nom).HasColumnName("ANN_ENCAVEUR_NOM");
            cfg.Property(t => t.Encaveur.Prenom).HasColumnName("ANN_ENCAVEUR_PRENOM");
            cfg.Property(t => t.Encaveur.ControlSoumis).HasColumnName("ANN_ENCAVEUR_SOUMIS_CONTROL");
            cfg.Property(t => t.Encaveur.Numero).HasColumnName("ANN_ENCAVEUR_NUMERO");
            cfg.Property(t => t.Encaveur.Societe).HasColumnName("ANN_ENCAVEUR_SOCIETE");
            cfg.Property(t => t.Responsable1).HasColumnName("ANN_RESPONSABLE_1");
            cfg.Property(t => t.Responsable2).HasColumnName("ANN_RESPONSABLE_2");
            cfg.Property(t => t.Responsable3).HasColumnName("ANN_RESPONSABLE_3");
            cfg.Property(t => t.Responsable4).HasColumnName("ANN_RESPONSABLE_4");
            cfg.Property(t => t.ModeContabiliteId).HasColumnName("MOC_ID");
            cfg.Property(t => t.IsPrixKillograme).HasColumnName("ANN_IS_PRIX_KG");
            cfg.Property(t => t.TauxConversion.Rouge).HasColumnName("ANN_TAUX_CONVERTION_ROUGE").HasPrecision(3, 2);
            cfg.Property(t => t.TauxConversion.Blanc).HasColumnName("ANN_TAUX_CONVERTION_BLANC").HasPrecision(3, 2);
            cfg.Property(t => t.TauxConversionMout.BlancMout).HasColumnName("ANN_TAUX_CONVERSION_MOUT_BLANC").HasPrecision(3, 2);
            cfg.Property(t => t.TauxConversionMout.RougeMout).HasColumnName("ANN_TAUX_CONVERSION_MOUT_ROUGE").HasPrecision(3, 2);

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        protected virtual void QuotationProductionModel_OnModelCreating(EntityTypeConfiguration<QuotationProductionModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new
                {
                    t.Id,
                    t.CepageId,
                    t.RegionId,
                    t.Annee,
                    t.AOC,
                    /*PremierGrandsCrus = */
                    t.PremierGrandCru,
                    t.VinPays,
                    t.VinTable
                    //t.Vins,
                    //t.Vins.Pays,
                    //t.Vins.Table,
                });
                m.ToTable("QUO_QUOTA_PRODUCTION");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("QUO_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.CepageId).HasColumnName("CEP_ID");
            cfg.Property(t => t.RegionId).HasColumnName("REG_ID");
            cfg.Property(t => t.Annee).HasColumnName("ANN_AN");
            cfg.Property(t => t.AOC).HasColumnName("QUO_AOC");
            cfg.Property(t => t.PremierGrandCru).HasColumnName("QUO_PREMIER_GRANDS_CRUS");
            cfg.Property(t => t.VinPays/*t.Vins.Pays*/).HasColumnName("QUO_VINS_PAYS");
            cfg.Property(t => t.VinTable/*t.Vins.Table*/).HasColumnName("QUO_VINS_TABLE");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        protected virtual void SettingModel_OnModelCreating(EntityTypeConfiguration<AbstractSettingModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            {
                Log.InfoFormat("Entity Configuration :" + cfg);
                cfg.Map(m =>
                {
                    m.Properties(t => new
                    {
                        t.Id,
                        t.Name,
                        //t.TypeId
                    });
                    m.ToTable("SET_SETTING");
                });

                cfg.HasKey(t => t.Id);
                cfg.Property(t => t.Id).HasColumnName("SET_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
                cfg.Property(t => t.Name).HasColumnName("SET_NAME");
                Log.InfoFormat("Entity Configuration :" + cfg + " done");
            }
            {
                cfg.Map<SettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.Unknown);
                    m.Property(t => t.Value.String).HasColumnName("SET_VALUE_STRING");
                    m.Property(t => t.Value.Integer).HasColumnName("SET_VALUE_INTEGER");
                    m.Property(t => t.Value.Decimal).HasColumnName("SET_VALUE_DECIMAL");
                    m.Property(t => t.Value.Boolean).HasColumnName("SET_VALUE_BOOLEAN");
                    m.Property(t => t.Value.DateTime).HasColumnName("SET_VALUE_DATETIME");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
            {
                cfg.Map<StringSettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.String);
                    m.Property(t => t.Value).HasColumnName("SET_VALUE_STRING");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
            {
                cfg.Map<IntegerSettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.Integer);
                    m.Property(t => t.Value).HasColumnName("SET_VALUE_INTEGER");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
            {
                cfg.Map<DecimalSettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.Decimal);
                    m.Property(t => t.Value).HasColumnName("SET_VALUE_DECIMAL");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
            {
                cfg.Map<DateTimeSettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.DateTime);
                    m.Property(t => t.Value).HasColumnName("SET_VALUE_DATETIME");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
            {
                cfg.Map<BooleanSettingModel>(m =>
                {
                    m.Requires("SET_TYPE").HasValue((int)AbstractSettingModel.TypeEnum.Boolean);
                    m.Property(t => t.Value).HasColumnName("SET_VALUE_BOOLEAN");
                    Log.InfoFormat("Entity Configuration :" + m + " done");
                });
            }
        }

        protected virtual void PeseeEnteteModel_OnModelCreating(EntityTypeConfiguration<PeseeEnteteModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");
            Log.InfoFormat("Entity Configuration :" + cfg);

            cfg.Map(m =>
            {
                m.Properties(t => new { t.Id, t.AcquitId, t.AutreMentionId, t.Date, t.GrandCru, t.Lieu, t.LieuDeProductionId, t.ResponsableId, t.Remarques, t.Divers1, t.Divers2, t.Divers3 });
                m.ToTable("PEE_PESEE_ENTETE");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("PEE_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.AcquitId).HasColumnName("ACQ_ID");
            cfg.Property(t => t.ResponsableId).HasColumnName("RES_ID");
            cfg.Property(t => t.Date).HasColumnName("PEE_DATE");
            cfg.Property(t => t.Lieu).HasColumnName("PEE_LIEU").HasMaxLength(50);
            cfg.Property(t => t.GrandCru).HasColumnName("PEE_GRANDCRU");
            cfg.Property(t => t.LieuDeProductionId).HasColumnName("LIE_ID");
            cfg.Property(t => t.AutreMentionId).HasColumnName("AUT_ID");
            cfg.Property(t => t.Remarques).HasColumnName("PEE_REMARQUES");
            cfg.Property(t => t.Divers1).HasColumnName("PEE_DIVERS1");
            cfg.Property(t => t.Divers2).HasColumnName("PEE_DIVERS2");
            cfg.Property(t => t.Divers3).HasColumnName("PEE_DIVERS3");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }

        protected virtual void PeseeDetailModel_OnModelCreating(EntityTypeConfiguration<PeseeDetailModel> cfg)
        {
            Log.InfoFormat("Entity Mapping ");

            Log.InfoFormat("Entity Configuration :" + cfg);
            cfg.Map(m =>
            {
                m.Properties(t => new { t.Id, t.CepageId, t.EnteteId, t.Lot, t.QuantiteKilogramme, t.QuantiteLitre, t.SondageBricks, t.SondageOE, t.ArticleId });
                m.ToTable("PED_PESEE_DETAIL");
            });
            cfg.HasKey(t => t.Id);
            cfg.Property(t => t.Id).HasColumnName("PED_ID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            cfg.Property(t => t.CepageId).HasColumnName("CEP_ID");
            cfg.Property(t => t.EnteteId).HasColumnName("PEE_ID");
            cfg.Property(t => t.Lot).HasColumnName("PED_LOT");
            cfg.Property(t => t.SondageOE).HasColumnName("PED_SONDAGE_OE");
            cfg.Property(t => t.SondageBricks).HasColumnName("PED_SONDAGE_BRIKS");
            cfg.Property(t => t.QuantiteKilogramme).HasColumnName("PED_QUANTITE_KG");
            cfg.Property(t => t.QuantiteLitre).HasColumnName("PED_QUANTITE_LITRES");
            cfg.Property(t => t.ArticleId).HasColumnName("ART_ID");

            Log.InfoFormat("Entity Configuration :" + cfg + " done");
        }
    }
}