﻿using System;

namespace GSN.GDV.Data.Contextes
{
    public interface IMainDataContext : IDisposable
    {
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.AcquitModel> AcquitSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.AnneeModel> AnneeSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.AutreMentionModel> AutreMentionSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.BooleanSettingModel> BooleanSettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.CepageModel> CepageSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.CouleurModel> CouleurSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.DateTimeSettingModel> DateTimeSettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.DecimalSettingModel> DecimalSettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.IntegerSettingModel> IntegerSettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.LieuProductionModel> LieuProductionSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.ParametreModel> ParametreSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.ParcelleModel> ParcelleSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.PeseeDetailModel> PeseeDetailSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.PeseeEnteteModel> PeseeEnteteSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.ProprietaireModel> ProprietaireSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.QuotationProductionModel> QuotationProductionSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.RegionModel> RegionSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.AbstractSettingModel> SettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.StringSettingModel> StringSettingSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.ArticleModel> ArticleSet { get; set; }
        System.Data.Entity.DbSet<GSN.GDV.Data.Models.ResponsableModel> ResponsableSet { get; set; }

        int SaveChanges();

        //
        // Summary:
        //     Returns a System.Data.Entity.DbSet<TEntity> instance for access to entities
        //     of the given type in the context and the underlying store.
        //
        // Type parameters:
        //   TEntity:
        //     The type entity for which a set should be returned.
        //
        // Returns:
        //     A set for the given entity type.
        //
        // Remarks:
        //     Note that Entity Framework requires that this method return the same instance
        //     each time that it is called for a given context instance and entity type.
        //     Also, the non-generic System.Data.Entity.DbSet returned by the System.Data.Entity.DbContext.Set(System.Type)
        //     method must wrap the same underlying query and set of entities. These invariants
        //     must be maintained if this method is overridden for anything other than creating
        //     test doubles for unit testing.  See the System.Data.Entity.DbSet<TEntity>
        //     class for more details.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Set")]
        System.Data.Entity.DbSet<TEntity> Set<TEntity>() where TEntity : class;

        //
        // Summary:
        //     Returns a non-generic System.Data.Entity.DbSet instance for access to entities
        //     of the given type in the context and the underlying store.
        //
        // Parameters:
        //   entityType:
        //     The type of entity for which a set should be returned.
        //
        // Returns:
        //     A set for the given entity type.
        //
        // Remarks:
        //     Note that Entity Framework requires that this method return the same instance
        //     each time that it is called for a given context instance and entity type.
        //     Also, the generic System.Data.Entity.DbSet<TEntity> returned by the System.Data.Entity.DbContext.Set(System.Type)
        //     method must wrap the same underlying query and set of entities. These invariants
        //     must be maintained if this method is overridden for anything other than creating
        //     test doubles for unit testing.  See the System.Data.Entity.DbSet class for
        //     more details.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Set")]
        System.Data.Entity.DbSet Set(Type entityType);
    }
}