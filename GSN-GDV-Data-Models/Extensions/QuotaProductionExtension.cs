﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace GSN.GDV.Data.Extensions
{
    public static class QuotaProductionExtension
    {
        public static void UpdateQuota(string pathFolder, MainDataContext me)
        {
            string path = pathFolder + "\\PathDB\\ToImport\\GDV_QUOTA_PRODUCTION";
            if (File.Exists(path))
            {
                /*using (var db = new MainDataContext())
                {*/
                var file = new System.IO.FileInfo(path);
                //Uniquement pour chargement initial de la de la table !!! à implémenter correctement*
                var l = QuotaProductionExtension.ExtractFromCSV(file, ";");
                //foreach (var o in l)
                //{
                //    Log.InfoFormat("", o.GetType(), o.Id, o.Annee, o.CepageId, o.RegionId, o.VinPays, o.VinTable, o.AOC);
                //}
                QuotaProductionExtension.Import(me, l);
                //}
            }
        }

        public static IEnumerable<QuotationProductionModel> ExtractFromCSV(this FileInfo file, string separtor = ";")
        {
            CultureInfo inv = CultureInfo.InvariantCulture;
            var lc = System.IO.File.ReadAllLines(file.FullName, Encoding.ASCII);
            var q = (from line in lc
                     let fields = line.Split(new char[] { ';' })
                     select new QuotationProductionModel
                     {
                         CepageId = Int32.Parse(fields[0]),
                         RegionId = Int32.Parse(fields[1]),
                         Annee = Int32.Parse(fields[2]),
                         AOC = decimal.Parse(fields[3]),//Check decimal format!!!
                         PremierGrandCru = decimal.Parse(fields[4]),
                         VinPays = decimal.Parse(fields[5]),
                         VinTable = decimal.Parse(fields[6])
                     });
            return q;
        }

        public static void Import(IMainDataContext me, IEnumerable<QuotationProductionModel> args)
        {
            var drc = me.QuotationProductionSet;
            //foreach (var o in drc.ToList())
            //    drc.Remove(o);
            drc.RemoveRange(me.QuotationProductionSet);
            //foreach (var o in args)
            //{
            //    drc.Add(o);

            //}
            drc.AddRange(args);
            me.SaveChanges();
        }

        public static QuotationProductionModel FindBy(IMainDataContext me, int cepageId, int regionId, int annee)
        {
            return (from t in me.QuotationProductionSet where t.CepageId == cepageId && t.RegionId == regionId && t.Annee == annee select t).FirstOrDefault();
        }

        public static decimal GetTaux(IMainDataContext me, int cepageId, int regionId, int annee, int classeVinId)
        {
            var o = FindBy(me, cepageId: cepageId, regionId: regionId, annee: annee);
            return GetTaux(o, classeVinId);
        }

        public static decimal GetTaux(QuotationProductionModel o, int classeVinId)
        {
            if (o == null)
                return 0;
            switch (classeVinId)
            {
                case 1://ClassVinEnum.AOC:
                    return o.AOC ?? 0;

                case 2://ClassVinEnum.PremierGrandCru:
                    return o.PremierGrandCru ?? 0;

                case 3://ClassVinEnum.VinPays:
                    return o.VinPays ?? 0;

                case 10://ClassVinEnum.VinTable:
                    return o.VinTable ?? 0;

                default:
                    return 0;
            }
        }

        public enum ClassVinEnum : uint
        {
            AOC = 1,
            PremierGrandCru = 2,
            VinPays = 3,
            VinTable = 10
        };

        //ExtractCSV
        //SAVE
        //UPDATE
        //READ
    }
}