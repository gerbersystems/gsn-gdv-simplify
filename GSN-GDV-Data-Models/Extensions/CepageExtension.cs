﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class CepageExtension
    {
        public static CouleurModel GetCouleur(IMainDataContext me, int CepageId)
        {
            return (from t in me.CepageSet from o in me.CouleurSet where o.Id == t.CouleurId && t.Id == CepageId select o).FirstOrDefault();
        }

        public static AnneeExtension.AnneeCouleurVinEnum GetCouleurVin(IMainDataContext me, int CepageId)
        {
            var o = GetCouleur(me: me, CepageId: CepageId);
            if (o == null)
                throw new NotSupportedException();
            return AnneeExtension.ToAnneeCouleurVinEnum(o.Id);
        }
    }
}