﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class PeseeEnteteExtension
    {
        internal static IQueryable<PeseeEnteteModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId)
        {
            var q = (
                from pee in me.PeseeEnteteSet
                from acq in me.AcquitSet
                where
                    pee.AcquitId == acq.Id &&
                    acq.AnneeId == AnneeId &&
                    acq.ProprietaireId == ProducteurId
                select pee
            );
            return q;
        }

        internal static IQueryable<PeseeEnteteModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int LieuId)
        {
            var q = ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId);
            return (
                from t in q
                where
                    t.LieuDeProductionId == LieuId
                select t
            );
        }

        internal static IQueryable<PeseeEnteteModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int LieuId, int AutreMentionId)
        {
            var q = ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId, LieuId: LieuId);
            return (
                from t in q
                where
                    t.AutreMentionId == AutreMentionId
                select t
            );
        }

        internal static IQueryable<PeseeEnteteModel> ListByAutreMention(IMainDataContext me, int AnneeId, int ProducteurId, int AutreMentionId)
        {
            var q = ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId);
            return (
                from t in q
                where
                    t.AutreMentionId == AutreMentionId
                select t
            );
        }

        internal static IQueryable<PeseeEnteteModel> ListByAcquit(IMainDataContext me, int AcquitId)
        {
            return (
                from t in me.PeseeEnteteSet
                where
                    t.AcquitId == AcquitId
                select t
            );
        }

        internal static AcquitModel GetAcquit(IMainDataContext me, int PeseeEnteteId)
        {
            return (
                from o in me.AcquitSet
                from t in me.PeseeEnteteSet
                where
                    o.Id == t.AcquitId &&
                    t.Id == PeseeEnteteId
                select o
            ).FirstOrDefault();
        }

        public static int? GetAcquitId(IMainDataContext me, int PeseeEnteteId)
        {
            return (from t in me.PeseeEnteteSet where t.Id == PeseeEnteteId select t.AcquitId).FirstOrDefault();
        }

        public static CouleurModel GetCouleur(IMainDataContext me, int PeseeEnteteId)
        {
            var AcquitId = GetAcquitId(me: me, PeseeEnteteId: PeseeEnteteId);
            return AcquitId == null ? null : AcquitExtension.GetCouleur(me: me, AcquitId: AcquitId.Value);
        }

        public static AnneeExtension.AnneeCouleurVinEnum GetCouleurVin(IMainDataContext me, int PeseeEnteteId)
        {
            var o = GetCouleur(me: me, PeseeEnteteId: PeseeEnteteId);
            if (o == null)
                throw new NotSupportedException();
            return AnneeExtension.ToAnneeCouleurVinEnum(o.Id);
        }
    }
}