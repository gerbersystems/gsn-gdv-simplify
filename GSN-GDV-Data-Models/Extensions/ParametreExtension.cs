﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class ParametreExtension
    {
        public static ParametreModel Get(IMainDataContext me, int AnneeId)
        {
            return (from t in me.ParametreSet where t.ANN_ID == AnneeId select t).FirstOrDefault();
        }
    }
}