﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class ParcelleExtension
    {
        public static IQueryable<ParcelleModel> ListByAcquit(IMainDataContext me, int AcquitId)
        {
            return (from t in me.ParcelleSet where t.AcquitId == AcquitId select t);
        }

        public static IQueryable<ParcelleModel> ListByAcquit(IMainDataContext me, int AcquitId, int CepageId)
        {
            var q = ListByAcquit(me: me, AcquitId: AcquitId);
            return (from t in q where t.CepageId == CepageId select t);
        }

        public static double? GetSumLitreByAcquit(IMainDataContext me, int AcquitId)
        {
            var q = ListByAcquit(me: me, AcquitId: AcquitId);
            return GetSumLitre(q);
        }

        public static double? GetSumLitreByAcquit(IMainDataContext me, int AcquitId, int CepageId)
        {
            var q = ListByAcquit(me: me, AcquitId: AcquitId, CepageId: CepageId);
            return GetSumLitre(q);
        }

        public static double? GetSumLitre(IQueryable<ParcelleModel> q)
        {
            return (from t in q select t.Litres).Sum();
        }

        /// <summary>
        /// Cette fonction retourne la liste de toutes les parcelles sur la base du <paramref name="CepageId"/>, du AnneeId et du VendangeProprietaireId,
        /// ces deux derniers sont extrapolé du <paramref name="AcquitId"/>
        /// </summary>
        /// <param name="me"></param>
        /// <param name="CepageId"></param>
        /// <param name="AcquitId">
        /// est utilisé pour récuperer le AnneeId et le VendangeProprietaireId
        /// ce qui veux dire que ce sont pas que les parcelles de <paramref name="AcquitId"/> qui seront listé
        /// </param>
        /// <param name="LieuId"></param>
        /// <returns></returns>
        public static IQueryable<ParcelleModel> ListByLieu(IMainDataContext me, int CepageId, int AcquitId, int LieuId)
        {
            var q = (
                from par in me.ParcelleSet
                from acq in me.AcquitSet
                from acq1 in me.AcquitSet
                where
                    par.AcquitId == acq.Id &&
                    acq.AnneeId == acq1.AnneeId &&
                    acq.ProducteurId == acq1.ProducteurId &&
                    acq1.Id == AcquitId &&
                    par.LieuId == LieuId &&
                    par.CepageId == CepageId
                select par
            );
            return q;
        }

        /// <summary>
        /// Cette fonction retourne la liste de toutes les parcelles sur la base du <paramref name="CepageId"/>, du AnneeId et du VendangeProprietaireId,
        /// ces deux derniers sont extrapolé du <paramref name="AcquitId"/>
        /// </summary>
        /// <param name="me"></param>
        /// <param name="CepageId"></param>
        /// <param name="AcquitId">
        /// est utilisé pour récuperer le AnneeId et le VendangeProprietaireId
        /// ce qui veux dire que ce sont pas que les parcelles de <paramref name="AcquitId"/> qui seront listé
        /// </param>
        /// <param name="LieuId"></param>
        /// <param name="AutreMentionId"></param>
        /// <returns></returns>
        public static IQueryable<ParcelleModel> ListByLieu(IMainDataContext me, int CepageId, int AcquitId, int LieuId, int AutreMentionId)
        {
            var q = ListByLieu(me: me, CepageId: CepageId, AcquitId: AcquitId, LieuId: LieuId);
            q = (from par in q where par.AutreMentionId == AutreMentionId select par);
            return q;
        }

        /// <summary>
        /// Cette fonction retourne la liste de toutes les parcelles sur la base du <paramref name="CepageId"/>, du AnneeId et du VendangeProprietaireId,
        /// ces deux derniers sont extrapolé du <paramref name="AcquitId"/>
        /// </summary>
        /// <param name="me"></param>
        /// <param name="CepageId"></param>
        /// <param name="AcquitId">
        /// est utilisé pour récuperer le AnneeId et le VendangeProprietaireId
        /// ce qui veux dire que ce sont pas que les parcelles de <paramref name="AcquitId"/> qui seront listé
        /// </param>
        /// <param name="AutreMentionId"></param>
        /// <returns></returns>
        public static IQueryable<ParcelleModel> ListByAutreMention(IMainDataContext me, int CepageId, int AcquitId, int AutreMentionId)
        {
            var q = (
                from par in me.ParcelleSet
                from acq in me.AcquitSet
                from acq1 in me.AcquitSet
                where
                    par.AcquitId == acq.Id &&
                    acq.AnneeId == acq1.AnneeId &&
                    acq.ProducteurId == acq1.ProducteurId &&
                    acq1.Id == AcquitId &&
                    par.AutreMentionId == AutreMentionId &&
                    par.CepageId == CepageId
                select par
            );
            return q;
        }

        public static double? GetSumLitreByLieu(IMainDataContext me, int CepageId, int AcquitId, int LieuId)
        {
            return GetSumLitre(ListByLieu(me: me, CepageId: CepageId, AcquitId: AcquitId, LieuId: LieuId));
        }

        public static double? GetSumLitreByLieu(IMainDataContext me, int CepageId, int AcquitId, int LieuId, int AutreMentionId)
        {
            return GetSumLitre(ListByLieu(me: me, CepageId: CepageId, AcquitId: AcquitId, LieuId: LieuId, AutreMentionId: AutreMentionId));
        }

        public static double? GetSumLitreByAutreMention(IMainDataContext me, int CepageId, int AcquitId, int AutreMentionId)
        {
            return GetSumLitre(ListByAutreMention(me: me, CepageId: CepageId, AcquitId: AcquitId, AutreMentionId: AutreMentionId));
        }
    }
}