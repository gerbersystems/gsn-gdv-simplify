﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.Helpers;
using System;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class SettingExtension
    {
        public enum SettingEnum
        {
            ApplicationUnite,
            AcceptExceedingQuota,
            BonusType,
            FactureType,
            ShowSondageBrix,
            ShowSondageOxle,
            BonusUnite,
            DegreValeur,
            Canton,
            SyncContacts,
            Erp
        }

        public enum CantonEnum
        {
            Vaud,
            Valais,
            Neuchâtel
        }

        public enum ErpEnum
        {
            Winbiz = 0,
            XpertFIN = 1
        }

        public enum ApplicationUniteEnum { Kilo = 0, Litre = 1 }

        public enum BonusUniteEnum
        {
            FixePourcent = 0,
            VariableChf = 1,
            VariablePourcent = 2
        }

        public enum FactureTypeEnum
        {
            Simple = 0,
            LitresMout = 1,
            AvecBonus = 2,
            AvecBonusV2 = 3
        }

        public enum DegreValeur { Oechsle = 0, Brix = 1 }

        public enum SyncContactsEnum { AllowModifyLocalContacts = 0, ProhibitModifyLocalContacts = 1 }

        public static string Get(IMainDataContext me, SettingEnum name, string defaultValue)
        {
            return Get(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue);
        }

        public static int? Get(IMainDataContext me, SettingEnum name, int? defaultValue)
        {
            return Get(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue);
        }

        public static double? Get(IMainDataContext me, SettingEnum name, double? defaultValue)
        {
            return Get(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue);
        }

        public static DateTime? Get(IMainDataContext me, SettingEnum name, DateTime? defaultValue)
        {
            return Get(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue);
        }

        public static bool? Get(IMainDataContext me, SettingEnum name, bool? defaultValue)
        {
            return Get(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue);
        }

        public static T Get<T>(IMainDataContext me, SettingEnum name) where T : AbstractSettingModel
        {
            return Get<T>(me: me, name: EnumHelper.GetName(name));
        }

        public static string Get(IMainDataContext me, string name, string defaultValue)
        {
            var o = Get<StringSettingModel>(me, name);
            if (o == null || o.Value == null)
                return defaultValue;
            return o.Value;
        }

        public static int? Get(IMainDataContext me, string name, int? defaultValue)
        {
            var o = Get<IntegerSettingModel>(me, name);
            if (o == null || o.Value == null)
                return defaultValue;
            return o.Value;
        }

        public static double? Get(IMainDataContext me, string name, double? defaultValue)
        {
            var o = Get<DecimalSettingModel>(me, name);
            if (o == null || o.Value == null)
                return defaultValue;
            return o.Value;
        }

        public static bool? Get(IMainDataContext me, string name, bool? defaultValue)
        {
            var o = Get<BooleanSettingModel>(me, name);
            if (o == null || o.Value == null)
                return defaultValue;
            return o.Value;
        }

        public static DateTime? Get(IMainDataContext me, string name, DateTime? defaultValue)
        {
            var o = Get<DateTimeSettingModel>(me, name);
            if (o == null || o.Value == null)
                return defaultValue;
            return o.Value;
        }

        public static T Get<T>(IMainDataContext me, string name) where T : AbstractSettingModel
        {
            return (from t in me.Set<T>() where t.Name == name select t).FirstOrDefault();
        }

        public static bool CanSetCanton(IMainDataContext me)
        {
            return (from t in me.AcquitSet select t.AnneeId).Count() == 0 ? true : false;
        }

        public static void Set(IMainDataContext me, SettingEnum name, bool value, bool autoSave = true)
        {
            Set(me, EnumHelper.GetName(name), value);
        }

        public static void Set(IMainDataContext me, string name, bool value, bool autoSave = true)
        {
            var o = Get<BooleanSettingModel>(me, name);
            o.Value = value;
            if (autoSave) me.SaveChanges();
        }

        public static void Set(IMainDataContext me, SettingEnum name, int? value, bool autoSave = true)
        {
            Set(me, EnumHelper.GetName(name), value);
        }

        public static void Set(IMainDataContext me, string name, int? value, bool autoSave = true)
        {
            var o = Get<IntegerSettingModel>(me, name);
            o.Value = value;
            if (autoSave) me.SaveChanges();
        }

        public static void Create(IMainDataContext me, SettingEnum name, bool? defaultValue, bool autoSave = true)
        {
            Create(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue, autoSave: autoSave);
        }

        public static void Create(IMainDataContext me, SettingEnum name, int? defaultValue, bool autoSave = true)
        {
            Create(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue, autoSave: autoSave);
        }

        public static void Create(IMainDataContext me, SettingEnum name, string defaultValue, bool autoSave = true)
        {
            Create(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue, autoSave: autoSave);
        }

        public static void Create(IMainDataContext me, SettingEnum name, double? defaultValue, bool autoSave = true)
        {
            Create(me: me, name: EnumHelper.GetName(name), defaultValue: defaultValue, autoSave: autoSave);
        }

        public static void Create(IMainDataContext me, string name, bool? defaultValue, bool autoSave = true)
        {
            var o = Get<BooleanSettingModel>(me, name);
            if (o == null)
            {
                me.SettingSet.Add(o = new BooleanSettingModel { Name = name });
                o.Value = defaultValue;
                if (autoSave) me.SaveChanges();
            }
        }

        public static void Create(IMainDataContext me, string name, int? defaultValue, bool autoSave = true)
        {
            var o = Get<IntegerSettingModel>(me, name);
            if (o == null)
            {
                me.SettingSet.Add(o = new IntegerSettingModel { Name = name });
                o.Value = defaultValue;
                if (autoSave) me.SaveChanges();
            }
        }

        public static void Create(IMainDataContext me, string name, string defaultValue, bool autoSave = true)
        {
            var o = Get<StringSettingModel>(me, name);
            if (o == null)
            {
                me.SettingSet.Add(o = new StringSettingModel { Name = name });
                o.Value = defaultValue;
                if (autoSave) me.SaveChanges();
            }
        }

        public static void Create(IMainDataContext me, string name, double? defaultValue, bool autoSave = true)
        {
            var o = Get<DecimalSettingModel>(me, name);
            if (o == null)
            {
                me.SettingSet.Add(o = new DecimalSettingModel { Name = name });
                o.Value = defaultValue;
                if (autoSave) me.SaveChanges();
            }
        }

        public static void InitializeSettings(IMainDataContext me, bool autoSave = true)
        {
            Create(me: me, name: SettingEnum.AcceptExceedingQuota, defaultValue: false, autoSave: false);
            Create(me: me, name: SettingEnum.BonusType, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.ApplicationUnite, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.ShowSondageBrix, defaultValue: false, autoSave: false);
            Create(me: me, name: SettingEnum.ShowSondageOxle, defaultValue: true, autoSave: false);
            Create(me: me, name: SettingEnum.BonusUnite, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.DegreValeur, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.Canton, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.SyncContacts, defaultValue: 1, autoSave: false);
            Create(me: me, name: SettingEnum.Erp, defaultValue: 0, autoSave: false);
            Create(me: me, name: SettingEnum.FactureType, defaultValue: 3, autoSave: false);

            if (autoSave) me.SaveChanges();
        }
    }
}