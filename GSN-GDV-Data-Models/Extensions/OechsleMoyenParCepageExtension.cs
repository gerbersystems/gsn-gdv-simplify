﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.Data.Models.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class OechsleMoyenParCepageExtension
    {
        private static IQueryable<PeseeDetailModel> ListPeseeByAnnee(IMainDataContext me, int AnnId)
        {
            return (
                from ped in me.PeseeDetailSet
                from pee in me.PeseeEnteteSet
                from acq in me.AcquitSet
                where ped.EnteteId == pee.Id && pee.AcquitId == acq.Id && acq.AnneeId == AnnId
                select ped
                );
        }

        private static IQueryable<CepageModel> ListDistinctCepagePeseeByAnnee(IMainDataContext me, int AnnId)
        {
            return (
                from ped in me.PeseeDetailSet
                from pee in me.PeseeEnteteSet
                from acq in me.AcquitSet
                from cep in me.CepageSet
                where ped.EnteteId == pee.Id && pee.AcquitId == acq.Id && ped.CepageId == cep.Id && acq.AnneeId == AnnId
                select cep
                ).Distinct();
        }

        private static List<OechsleMoyenParCepageModel> GetOechsleMoyenByCepageAnnee(IMainDataContext me, int AnnId, List<OechsleMoyenParCepageModel> CepListe)
        {
            var liste = ListPeseeByAnnee(me: me, AnnId: AnnId);
            foreach (var cep in CepListe)
            {
                var t = liste.Where(c => c.CepageId == cep.CEP_ID);
                cep.OECHSLE_MOYEN = MoyennePonderee(t);
            }
            return CepListe;
        }

        public static List<OechsleMoyenParCepageModel> ListOechsleMoyenParCepage(IMainDataContext me, int AnnId)
        {
            var liste = new List<OechsleMoyenParCepageModel>();
            var ListDistinctCepage = ListDistinctCepagePeseeByAnnee(me: me, AnnId: AnnId);
            OechsleMoyenParCepageModel OechsleMoyenParCepage;
            foreach (var i in ListDistinctCepage)
            {
                OechsleMoyenParCepage = new OechsleMoyenParCepageModel();
                OechsleMoyenParCepage.CEP_ID = i.Id;
                OechsleMoyenParCepage.ANN_ID = AnnId;
                liste.Add(OechsleMoyenParCepage);
            }
            return GetOechsleMoyenByCepageAnnee(me: me, AnnId: AnnId, CepListe: liste);
        }

        public static double GetOechsleMoyenParCepage(IMainDataContext me, int AnnId, int idCepage)
        {
            var oe = ListOechsleMoyenParCepage(me: me, AnnId: AnnId).Where(i => i.CEP_ID == idCepage).FirstOrDefault();
            return (oe != null) ? oe.OECHSLE_MOYEN : (double)0;
        }

        private static double MoyennePonderee(IQueryable<PeseeDetailModel> liste)
        {
            double avg = 0;
            double total = liste.Sum(l => l.QuantiteKilogramme).GetValueOrDefault();
            foreach (var i in liste)
            {
                avg += (i.SondageOE * i.QuantiteKilogramme).GetValueOrDefault();
            }
            return avg / total;
        }
    }
}