﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class PeseeDetailExtension
    {
        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, IQueryable<PeseeEnteteModel> q, int ExcludedId)
        {
            return (
            from e in q
            from t in me.PeseeDetailSet
            where
                t.EnteteId == e.Id &&
                t.Id != ExcludedId
            select t);
        }

        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, IQueryable<PeseeDetailModel> q, int CepageId)
        {
            q = (from t in q where t.CepageId == CepageId select t);
            return q;
        }

        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int ExcludedId)
        {
            return ListBy(me: me, q: PeseeEnteteExtension.ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId), ExcludedId: ExcludedId);
        }

        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int ExcludedId)
        {
            var q = ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId, ExcludedId: ExcludedId);
            return ListBy(me: me, q: q, CepageId: CepageId);
        }

        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int LieuId, int ExcludedId)
        {
            var q = ListBy(me: me, q: PeseeEnteteExtension.ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId, LieuId: LieuId), ExcludedId: ExcludedId);
            return ListBy(me: me, q: q, CepageId: CepageId);
        }

        private static IQueryable<PeseeDetailModel> ListBy(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int LieuId, int AutreMentionId, int ExcludedId)
        {
            var q = ListBy(me: me, q: PeseeEnteteExtension.ListBy(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId, LieuId: LieuId, AutreMentionId: AutreMentionId), ExcludedId: ExcludedId);
            return ListBy(me: me, q: q, CepageId: CepageId);
        }

        private static IQueryable<PeseeDetailModel> ListByAutreMention(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int AutreMentionId, int ExcludedId)
        {
            var q = ListBy(me: me, q: PeseeEnteteExtension.ListByAutreMention(me: me, AnneeId: AnneeId, ProducteurId: ProducteurId, AutreMentionId: AutreMentionId), ExcludedId: ExcludedId);
            return ListBy(me: me, q: q, CepageId: CepageId);
        }

        private static IQueryable<PeseeDetailModel> ListByAcquit(IMainDataContext me, int AcquitId, int CepageId, int ExcludedId)
        {
            var q = ListBy(me: me, q: PeseeEnteteExtension.ListByAcquit(me: me, AcquitId: AcquitId), ExcludedId: ExcludedId);

            return ListBy(me: me, q: q, CepageId: CepageId);
        }

        private static IQueryable<PeseeDetailModel> ListByAcquit(IMainDataContext me, int AcquitId, int ExcludedId)
        {
            var q = ListBy(me: me, q: PeseeEnteteExtension.ListByAcquit(me: me, AcquitId: AcquitId), ExcludedId: ExcludedId);

            return q;
        }

        private static double? GetLitreSaisiBy(IEnumerable<PeseeDetailModel> q)
        {
            return (from t in q select t.QuantiteLitre).Sum();
        }

        public static double? GetLitreSaisiByAcquit(IMainDataContext me, int AcquitId, int ExcludedId)
        {
            var q = ListByAcquit(me, AcquitId: AcquitId, ExcludedId: ExcludedId);
            return GetLitreSaisiBy(q);
        }

        public static double? GetLitreSaisiByAcquit(IMainDataContext me, int AcquitId, int CepageId, int ExcludedId)
        {
            var q = ListByAcquit(me, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);
            return GetLitreSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiByAcquit(IMainDataContext me, int AcquitId, int CepageId, int ExcludedId)
        {
            var q = ListByAcquit(me, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);
            return GetKilogrammeSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiByAcquit(IMainDataContext me, int AcquitId, int ExcludedId)
        {
            var q = ListByAcquit(me, AcquitId: AcquitId, ExcludedId: ExcludedId);
            return GetKilogrammeSaisiBy(q);
        }

        public static double? GetLitreSaisiByLieu(IMainDataContext me, int AcquitId, int CepageId, int LieuId, int ExcludedId)
        {
            var o = (from t in me.AcquitSet where t.Id == AcquitId select t).First();
            var AnneeId = o.AnneeId.Value;
            var ProducteurId = o.ProducteurId.Value;
            return GetLitreSaisiByLieu(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, LieuId: LieuId, ExcludedId: ExcludedId);
        }

        public static double? GetLitreSaisiByLieu(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int LieuId, int ExcludedId)
        {
            var q = ListBy(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, LieuId: LieuId, ExcludedId: ExcludedId);
            return GetLitreSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiByLieu(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int LieuId, int ExcludedId)
        {
            var q = ListBy(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, LieuId: LieuId, ExcludedId: ExcludedId);
            return GetKilogrammeSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiByLieu(IMainDataContext me, int AcquitId, int CepageId, int LieuId, int ExcludedId)
        {
            var o = (from t in me.AcquitSet where t.Id == AcquitId select t).First();
            var AnneeId = o.AnneeId.Value;
            var ProducteurId = o.ProducteurId.Value;
            return GetKilogrammeSaisiByLieu(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, LieuId: LieuId, ExcludedId: ExcludedId);
        }

        public static double? GetLitreSaisiByAutreMention(IMainDataContext me, int AcquitId, int CepageId, int AutreMentionId, int ExcludedId)
        {
            var o = (from t in me.AcquitSet where t.Id == AcquitId select t).First();
            var AnneeId = o.AnneeId.Value;
            var ProducteurId = o.ProducteurId.Value;
            return GetLitreSaisiByAutreMention(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, AutreMentionId: AutreMentionId, ExcludedId: ExcludedId);
        }

        public static double? GetLitreSaisiByAutreMention(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int AutreMentionId, int ExcludedId)
        {
            var q = ListByAutreMention(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, AutreMentionId: AutreMentionId, ExcludedId: ExcludedId);
            return GetLitreSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiByAutreMention(IMainDataContext me, int AcquitId, int CepageId, int AutreMentionId, int ExcludedId)
        {
            var o = (from t in me.AcquitSet where t.Id == AcquitId select t).First();
            var AnneeId = o.AnneeId.Value;
            var ProducteurId = o.ProducteurId.Value;
            return GetKilogrammeSaisiByAutreMention(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, AutreMentionId: AutreMentionId, ExcludedId: ExcludedId);
        }

        public static double? GetKilogrammeSaisiByAutreMention(IMainDataContext me, int AnneeId, int ProducteurId, int CepageId, int AutreMentionId, int ExcludedId)
        {
            var q = ListByAutreMention(me, AnneeId: AnneeId, ProducteurId: ProducteurId, CepageId: CepageId, AutreMentionId: AutreMentionId, ExcludedId: ExcludedId);
            return GetKilogrammeSaisiBy(q);
        }

        public static double? GetKilogrammeSaisiBy(IEnumerable<PeseeDetailModel> q)
        {
            return (from t in q select t.QuantiteKilogramme).Sum();
        }

        public static int NextLot(IMainDataContext me)
        {
            return NextLot(me, Year: DateTime.Now.Year);
        }

        public static int NextLot(IMainDataContext me, int Year)
        {
            var v0 = Year * 100000;
            var v1 = v0 + 99999;

            var v = (from t in me.PeseeDetailSet where t.Lot >= v0 && t.Lot <= v1 select t.Lot).Max();
            if (v == null || v <= 1)
                return v0 + 1;
            ///Check for integer overflow
            return (int)Math.Min((long)int.MaxValue, v.Value + 1);
        }

        public static int CountLot(IMainDataContext me, int Lot, int ExcludedId = 0)
        {
            var v = (from t in me.PeseeDetailSet where t.Lot == Lot && t.Id != ExcludedId select t.Lot).Count();
            return v;
        }

        public static bool IsUniqueLot(IMainDataContext me, int Lot, int ExcludedId = 0)
        {
            return CountLot(me: me, Lot: Lot, ExcludedId: ExcludedId) == 0;
        }
    }
}