﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class AcquitExtension
    {
        public static IQueryable<CepageModel> ListCepage(IMainDataContext me, int AcquitId)
        {
            return (
                from p in me.ParcelleSet
                from t in me.CepageSet
                where
                    t.Id == p.CepageId &&
                    p.AcquitId == AcquitId
                select t
            );
        }

        public static IQueryable<ParcelleModel> ListParcelle(IMainDataContext me, int AcquitId)
        {
            return (
                from t in me.ParcelleSet
                where
                    t.AcquitId == AcquitId
                select t
            );
        }

        public static IQueryable<ParcelleModel> ListParcelle(IMainDataContext me, int AcquitId, int LieuId)
        {
            return (
                from t in me.ParcelleSet
                where
                    t.AcquitId == AcquitId &&
                    t.LieuId == LieuId
                select t
            );
        }

        public static IQueryable<ParcelleModel> ListParcelle(IMainDataContext me, int AcquitId, int LieuId, int CepageId)
        {
            return (
                from t in me.ParcelleSet
                where
                    t.AcquitId == AcquitId &&
                    t.LieuId == LieuId &&
                    t.CepageId == CepageId
                select t
            );
        }

        public static IQueryable<ParcelleModel> ListParcelleByCepage(IMainDataContext me, int AcquitId, int CepageId)
        {
            return (
                from t in me.ParcelleSet
                where
                    t.AcquitId == AcquitId &&
                    t.CepageId == CepageId
                select t
            );
        }

        public static double? GetLitreMax(IMainDataContext me, int AcquitId)
        {
            return (
                from t in me.AcquitSet
                where t.Id == AcquitId
                select t.Litres
                ).FirstOrDefault();
        }

        public static double? GetLitreMax(IMainDataContext me, int AcquitId, int LieuId)
        {
            var q = ListParcelle(me: me, AcquitId: AcquitId, LieuId: LieuId);
            return ParcelleExtension.GetSumLitre(q);
        }

        public static double? GetLitreMax(IMainDataContext me, int AcquitId, int LieuId, int CepageId)
        {
            var q = ListParcelle(me: me, AcquitId: AcquitId, LieuId: LieuId, CepageId: CepageId);
            return ParcelleExtension.GetSumLitre(q);
        }

        public static double? GetLitreMaxByCepage(IMainDataContext me, int AcquitId, int CepageId)
        {
            var q = ListParcelleByCepage(me: me, AcquitId: AcquitId, CepageId: CepageId);
            return ParcelleExtension.GetSumLitre(q);
        }

        public static CouleurModel GetCouleur(IMainDataContext me, int AcquitId)
        {
            return (from t in me.AcquitSet from o in me.CouleurSet where o.Id == t.CouleurId && t.Id == AcquitId select o).FirstOrDefault();
        }

        public static AnneeExtension.AnneeCouleurVinEnum GetCouleurVin(IMainDataContext me, int AcquitId)
        {
            var o = GetCouleur(me: me, AcquitId: AcquitId);
            if (o == null)
                throw new NotSupportedException();
            return AnneeExtension.ToAnneeCouleurVinEnum(o.Id);
        }

        public static AnneeModel GetAnnee(IMainDataContext me, int AcquitId)
        {
            return (from t in me.AcquitSet from o in me.AnneeSet where o.Id == t.AnneeId && t.Id == AcquitId select o).FirstOrDefault();
        }

        public static AnneeModel.TauxConversionModel GetTauxConversion(IMainDataContext me, int AcquitId)
        {
            var o = GetAnnee(me: me, AcquitId: AcquitId);
            return o == null ? null : o.TauxConversion;
        }
    }
}