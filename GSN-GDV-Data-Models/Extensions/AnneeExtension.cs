﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using System;
using System.Linq;

namespace GSN.GDV.Data.Extensions
{
    public static class AnneeExtension
    {
        public enum AnneeCouleurVinEnum : short
        {
            /// <summary>
            /// Les numéro de l'énum doivent correspondre au résultat d'une requête sql sur la table COU_COULEUR ?
            /// </summary>

            blanc = 1, rouge = 2
        }

        //public static decimal Get(IMainDataContext me, int Annee, AnneeCouleurVinEnum Couleur)
        //{
        //	if (Couleur == AnneeCouleurVinEnum.blanc)
        //		return (from t in me.AnneeSet where t.Annee == Annee select t.TauxConversion.Blanc).FirstOrDefault();
        //	if (Couleur == AnneeCouleurVinEnum.rouge)
        //		return (from t in me.AnneeSet where t.Annee == Annee select t.TauxConversion.Rouge).FirstOrDefault();
        //	return -1;
        //}
        public static AnneeModel.TauxConversionModel FindTauxConversion(IMainDataContext me, int Annee)
        {
            return (from t in me.AnneeSet where t.Annee == Annee select t.TauxConversion).FirstOrDefault();
        }

        public static decimal GetTauxConversion(IMainDataContext me, int Annee, AnneeCouleurVinEnum Couleur)
        {
            return GetTauxConversion(me: FindTauxConversion(me: me, Annee: Annee), Couleur: Couleur);
        }

        public static decimal GetTauxConversion(this AnneeModel.TauxConversionModel me, AnneeCouleurVinEnum Couleur)
        {
            if (me == null)
                throw new NotSupportedException();
            if (Couleur == AnneeCouleurVinEnum.blanc)
                return (me.Blanc);
            if (Couleur == AnneeCouleurVinEnum.rouge)
                return (me.Rouge);
            throw new NotSupportedException();
        }

        public static AnneeCouleurVinEnum ToAnneeCouleurVinEnum(int v)
        {
            return (AnneeCouleurVinEnum)v;
        }

        //public static implicit operator AnneeCouleurVinEnum (int v)
        //{
        //	return ToAnneeCouleurVinEnum(v);
        //}
        //public static implicit operator int(AnneeCouleurVinEnum v)
        //{
        //	return (int)v;
        //}
    }
}