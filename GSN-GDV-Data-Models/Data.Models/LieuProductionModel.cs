﻿namespace GSN.GDV.Data.Models
{
    public class LieuProductionModel
    {
        public virtual int Id { get; set; }
        public virtual string Nom { get; set; }
        public virtual int? Numero { get; set; }
        public virtual int? RegionId { get; set; }
    }
}