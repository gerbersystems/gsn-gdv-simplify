﻿namespace GSN.GDV.Data.Models
{
    public class ArticleModel
    {
        public virtual int Id { get; set; }

        public virtual int? AnneeId { get; set; }

        public virtual double? Litres { get; set; }

        public virtual DesignationModel Designation { get; set; }

        public virtual string Code { get; set; }

        public virtual GroupeModel Groupe { get; set; }

        public virtual int? CouleurId { get; set; }

        public virtual int? WinBizId { get; set; }

        public virtual int? ModeComptabilisationId { get; set; }

        public virtual double? LitresEnStock { get; set; }

        public virtual int? Version { get; set; }

        public class DesignationModel
        {
            public virtual string Longue { get; set; }
            public virtual string Courte { get; set; }
        }

        public class GroupeModel
        {
            public virtual string Code { get; set; }
        }
    }
}