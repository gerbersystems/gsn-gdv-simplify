﻿using System;

namespace GSN.GDV.Data.Models
{
    public class PeseeDetailModel
    {
        public virtual int Id { get; set; }
        public virtual int? CepageId { get; set; }
        public virtual int? EnteteId { get; set; }
        public virtual int? Lot { get; set; }
        public virtual Double? SondageOE { get; set; }
        public virtual Double? SondageBricks { get; set; }
        public virtual Double? QuantiteLitre { get; set; }
        public virtual Double? QuantiteKilogramme { get; set; }

        public virtual int? ArticleId { get; set; }
    }
}