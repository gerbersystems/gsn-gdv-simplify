﻿using System;

namespace GSN.GDV.Data.Models
{
    public class ParcelleModel
    {
        public virtual int Id { get; set; }
        public virtual int? CepageId { get; set; }
        public virtual int? AcquitId { get; set; }
        public virtual string Folio { get; set; }
        public virtual int? Numero { get; set; }
        public virtual Double? SurfaceCepage { get; set; }
        public virtual Double? Quota { get; set; }
        public virtual Double? Litres { get; set; }
        public virtual Double? SurfaceComptee { get; set; }
        public virtual int? LieuId { get; set; }
        public virtual int? AutreMentionId { get; set; }
        public virtual int? ZoneTravailId { get; set; }
        public virtual string SecteurVisite { get; set; }
        public virtual string ModeCulture { get; set; }
    }
}