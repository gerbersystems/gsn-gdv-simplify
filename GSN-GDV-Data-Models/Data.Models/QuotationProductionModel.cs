﻿namespace GSN.GDV.Data.Models
{
    public class QuotationProductionModel
    {
        public virtual int Id { get; set; }
        public virtual int? CepageId { get; set; }
        public virtual int? RegionId { get; set; }
        public virtual int? Annee { get; set; }
        public virtual decimal? AOC { get; set; }
        public virtual decimal? PremierGrandCru { get; set; }
        public virtual decimal? VinPays { get; set; }
        public virtual decimal? VinTable { get; set; }

        //public virtual bool? PremierGrandsCrus { get; set; }
        //public virtual QuotationProductionModel.VinsModel Vins { get; set; }

        //public class VinsModel {
        //    public virtual string Pays { get; set; }
        //    public virtual string Table { get; set; }
        //}
    }
}