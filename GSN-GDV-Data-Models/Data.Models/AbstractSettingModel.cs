﻿using System;
using System.Text;

namespace GSN.GDV.Data.Models
{
    public abstract class AbstractSettingModel
    {
        public virtual int Id { get; set; }

        public abstract TypeEnum TypeId { get; }

        public virtual string Name { get; set; }

        public virtual string InternalValue { get { return null; } }

        public enum TypeEnum : int
        {
            String = 0, Integer, Decimal, DateTime, Boolean, Unknown
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("{");
            ToString(sb);
            sb.Append("}");
            return sb.ToString();
        }

        protected virtual void ToString(StringBuilder sb)
        {
            sb.Append("Id:").Append(Name).Append(", ");
            sb.Append("TypeId:").Append(TypeId).Append(", ");
            sb.Append("Name:\"").Append(Name).Append("\", ");
        }
    }

    public class SettingModel : AbstractSettingModel, ISettingModel<SettingModel.MultiValue>
    {
        public override TypeEnum TypeId { get { return TypeEnum.Unknown; } }

        public virtual SettingModel.MultiValue Value { get; set; }

        public override string InternalValue { get { return "" + Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }

        public class MultiValue
        {
            public string String { get; set; }

            public int? Integer { get; set; }

            public Double? Decimal { get; set; }

            public DateTime? DateTime { get; set; }

            public bool? Boolean { get; set; }

            public override string ToString()
            {
                return string.Concat("{ ",
                "Integer:", this.Integer, ", ",
                "Decimal:", this.Decimal, ", ",
                "Boolean:", this.Boolean, ", ",
                "DateTime:\"", this.DateTime, "\", ",
                "String:\"", this.String, "\" ", "}");
            }

            public static implicit operator string(MultiValue me)
            {
                return me.String;
            }

            public static implicit operator int?(MultiValue me)
            {
                return me.Integer;
            }

            public static implicit operator Double?(MultiValue me)
            {
                return me.Decimal;
            }

            public static implicit operator bool?(MultiValue me)
            {
                return me.Boolean;
            }

            public static implicit operator DateTime?(MultiValue me)
            {
                return me.DateTime;
            }

            public static implicit operator MultiValue(string v)
            {
                return new MultiValue { String = v };
            }

            public static implicit operator MultiValue(int v)
            {
                return new MultiValue { Integer = v };
            }

            public static implicit operator MultiValue(Double? v)
            {
                return new MultiValue { Decimal = v };
            }

            public static implicit operator MultiValue(bool? v)
            {
                return new MultiValue { Boolean = v };
            }

            public static implicit operator MultiValue(DateTime? v)
            {
                return new MultiValue { DateTime = v };
            }
        }
    }

    public interface ISettingModel<V>
    {
        int Id { get; set; }

        AbstractSettingModel.TypeEnum TypeId { get; }

        string Name { get; set; }

        V Value { get; set; }
    }

    public class StringSettingModel : AbstractSettingModel, ISettingModel<string>
    {
        public override TypeEnum TypeId { get { return TypeEnum.String; } }

        public virtual string Value { get; set; }

        public override string InternalValue { get { return Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }
    }

    public class IntegerSettingModel : AbstractSettingModel, ISettingModel<int?>
    {
        public override TypeEnum TypeId { get { return TypeEnum.Integer; } }

        public virtual int? Value { get; set; }

        public override string InternalValue { get { return "" + Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }
    }

    public class DecimalSettingModel : AbstractSettingModel, ISettingModel<Double?>
    {
        public override TypeEnum TypeId { get { return TypeEnum.Decimal; } }

        public virtual Double? Value { get; set; }

        public override string InternalValue { get { return "" + Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }
    }

    public class DateTimeSettingModel : AbstractSettingModel, ISettingModel<DateTime?>
    {
        public override TypeEnum TypeId { get { return TypeEnum.DateTime; } }

        public virtual DateTime? Value { get; set; }

        public override string InternalValue { get { return "" + Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }
    }

    public class BooleanSettingModel : AbstractSettingModel, ISettingModel<bool?>
    {
        public override TypeEnum TypeId { get { return TypeEnum.Boolean; } }

        public virtual bool? Value { get; set; }

        public override string InternalValue { get { return "" + Value; } }

        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);
            sb.Append("Value:").Append(Value).Append(", ");
        }
    }
}