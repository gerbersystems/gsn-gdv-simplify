﻿namespace GSN.GDV.Data.Models.Data.Models
{
    public class OechsleMoyenParCepageModel
    {
        public virtual int CEP_ID { get; set; }
        public virtual int ANN_ID { get; set; }
        public virtual double OECHSLE_MOYEN { get; set; }
    }
}