﻿namespace GSN.GDV.Data.Models
{
    public class RegionModel
    {
        public virtual int Id { get; set; }
        public virtual string Nom { get; set; }
    }
}