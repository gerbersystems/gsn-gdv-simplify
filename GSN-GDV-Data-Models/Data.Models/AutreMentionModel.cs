﻿namespace GSN.GDV.Data.Models
{
    public class AutreMentionModel
    {
        public virtual int Id { get; set; }
        public virtual string Nom { get; set; }
    }
}