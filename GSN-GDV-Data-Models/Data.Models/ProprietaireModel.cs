﻿namespace GSN.GDV.Data.Models
{
    public class ProprietaireModel
    {
        public virtual int Id { get; set; }
        public virtual string Nom { get; set; }
        public virtual string Prenom { get; set; }
        public virtual string Titre { get; set; }
        public virtual string SCTE { get; set; }

        public virtual int? AddressId { get; set; }
        public virtual bool? IsProducteur { get; set; }
        public virtual bool? IsProprietaire { get; set; }
        public virtual TVAModel TVA { get; set; }
        public virtual WinBizModel WinBiz { get; set; }

        public class TVAModel
        {
            public virtual int? ModeId { get; set; }
            public virtual string Numero { get; set; }
        }

        public class WinBizModel
        {
            public virtual int? AddressId { get; set; }
        }
    }
}