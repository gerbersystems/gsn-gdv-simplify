﻿namespace GSN.GDV.Data.Models
{
    public class AnneeModel
    {
        public virtual int Id { get; set; }
        public virtual int? AddressId { get; set; }
        public virtual int? Annee { get; set; }
        public virtual int? ModeContabiliteId { get; set; }
        public virtual string Responsable1 { get; set; }
        public virtual string Responsable2 { get; set; }
        public virtual string Responsable3 { get; set; }
        public virtual string Responsable4 { get; set; }

        public virtual bool? IsPrixKillograme { get; set; }

        public virtual TauxConversionModel TauxConversion { get; set; }
        public virtual TauxConversionMoutModel TauxConversionMout { get; set; }

        public virtual AnneeModel.EncaveurModel Encaveur { get; set; }

        public class EncaveurModel
        {
            public virtual int? Numero { get; set; }
            public virtual string Societe { get; set; }
            public virtual string Nom { get; set; }
            public virtual string Prenom { get; set; }
            public virtual bool? ControlSoumis { get; set; }
        }

        public class TauxConversionModel
        {
            public virtual decimal Rouge { get; set; }
            public virtual decimal Blanc { get; set; }
        }

        public class TauxConversionMoutModel
        {
            public virtual decimal? RougeMout { get; set; }
            public virtual decimal? BlancMout { get; set; }
        }
    }
}