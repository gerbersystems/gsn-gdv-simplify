﻿using System;

namespace GSN.GDV.Data.Models
{
    public class AcquitModel
    {
        public virtual int Id { get; set; }

        public virtual int? ClasseId { get; set; }
        public virtual int? CouleurId { get; set; }
        public virtual int? AnneeId { get; set; }
        public virtual int? CommuneId { get; set; }
        public virtual int? ProprietaireId { get; set; }

        public virtual int? Numero { get; set; }
        public virtual DateTime? Date { get; set; }

        public virtual Double? Surface { get; set; }

        public virtual Double? Litres { get; set; }
        public virtual int? RegionId { get; set; }
        public virtual int? NumeroComplet { get; set; }
        public virtual int? ProducteurId { get; set; }

        public virtual DateTime? Reception { get; set; }
    }
}