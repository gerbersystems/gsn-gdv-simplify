﻿namespace GSN.GDV.Data.Models
{
    public class ParametreModel
    {
        public virtual int PAM_ID { get; set; }
        public virtual int? ANN_ID { get; set; }
        public virtual string PAM_CLE { get; set; }
        public virtual string PAM_HASH_MACHINE { get; set; }
        public virtual int? PAM_NUM_ENCAVEUR { get; set; }
        public virtual byte[] PAM_EXPIRATION { get; set; }
        public virtual int? PAM_VERSION_BD { get; set; }
        public virtual int? PAM_LAST_LICENSE { get; set; }
        public virtual string PAM_WINBIZ_ADRESSE_DEFAUT { get; set; }
        public virtual string PAM_WINBIZ_ADRESSE_CURRENT { get; set; }
        public virtual string PAM_HASH_VERSION { get; set; }
        public virtual byte[] PAM_EXPIRATION_UPDATE { get; set; }
        public virtual byte[] PAM_NB_PRODUCTEUR { get; set; }
        public virtual string PAM_MACHINE_NAME { get; set; }
        //public virtual decimal PAM_DEFAULT_KG_LT_BLANC { get; set; }
        //public virtual decimal PAM_DEFAULT_KG_LT_ROUGE { get; set; }
    }
}