﻿namespace GSN.GDV.Data.Models
{
    public class CepageModel
    {
        public virtual int Id { get; set; }
        public virtual int? CouleurId { get; set; }
        public virtual int? Numero { get; set; }
        public virtual string Nom { get; set; }
    }
}