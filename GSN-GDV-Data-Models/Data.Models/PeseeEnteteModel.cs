﻿using System;

namespace GSN.GDV.Data.Models
{
    public class PeseeEnteteModel
    {
        public virtual int Id { get; set; }
        public virtual int? AcquitId { get; set; }
        public virtual int? ResponsableId { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string Lieu { get; set; }
        public virtual bool? GrandCru { get; set; }
        public virtual int? LieuDeProductionId { get; set; }
        public virtual int? AutreMentionId { get; set; }
        public virtual string Remarques { get; set; }
        public virtual string Divers1 { get; set; }
        public virtual string Divers2 { get; set; }
        public virtual string Divers3 { get; set; }
    }
}