﻿namespace GSN.GDV.Data.Models
{
    public class CouleurModel
    {
        public virtual int Id { get; set; }
        public virtual bool? HaveCepage { get; set; }
        public virtual string Nom { get; set; }
    }
}