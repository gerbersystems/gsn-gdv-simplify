﻿namespace GSN.GDV.Data.Models
{
    public class ResponsableModel
    {
        public virtual int Id { get; set; }

        public virtual int? AdresseId { get; set; }

        public virtual string Nom { get; set; }

        public virtual string Prenom { get; set; }

        public virtual bool IsDefault { get; set; }
    }
}