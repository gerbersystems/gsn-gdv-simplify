﻿namespace Gestion_Des_Vendanges.Services
{
    public class XpertFINpaymentRow
    {
        /// <summary>
        /// ActionCode
        /// (mandatory: no)
        /// </summary>
        public string APIActionCode => string.Empty;

        /// <summary>
        /// EnvironmentName: Nom de l'environnement
        /// </summary>
        public string APIEnvironmentName => "UV"; // TODO Create config file to store value

        /// <summary>
        /// UserID: UserID
        /// (mandatory: no)
        /// </summary>
        public string APIUserID => string.Empty;

        /// <summary>
        /// RightsCheck: Vérification des droits
        /// (mandatory: no)
        /// </summary>
        public string APIRightsCheck => string.Empty;

        /// <summary>
        /// CompanyNumber: No de société dans XFIN [ ligne1 = 2; ligne2...n = 42857 ]
        /// (mandatory: yes)
        /// </summary>
        public int ACX_CompanyNumber_5N_ { get; set; }

        /// <summary>
        /// ITHItemNumber: Interface - No de séquence retourné par IIHWRITE
        /// (mandatory: yes)
        /// </summary>
        public double AFI_ITHItemNumber_11N_ { get; set; }

        /// <summary>
        /// LineNumber: No de ligne
        /// [ Ligne 1: Compte collectif Créanciers ]
        /// [ Ligne 2..n : Comptes CG(Charges) ]
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIDLineNumber_5N_ { get; set; }

        /// <summary>
        /// AccountNumber: Compte
        /// [ Ligne 1: Compte collectif Créanciers Vendanges ]
        /// [ Ligne 2...n: Comptes achat ]
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDAccountNumber_15A_ { get; set; }

        /// <summary>
        /// FinYearAcctNr: Compte exercice
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDFinYearAcctNr_20A_ => string.Empty;

        /// <summary>
        /// CurrencyCode: Monnaie
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDCurrencyCode_3A_ => "CHF"; // TODO Create config file to store value

        /// <summary>
        /// OriginalAmount: Montant base facture créancier [ TOTAL PAIEMENT TTC ]
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDOriginalAmount_17_2_ { get; set; }

        /// <summary>
        /// LocalAmount: Montant base 1
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDLocalAmount_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// LocalAmount2: Montant base 2
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDLocalAmount2_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// DebitCreditFlag: D-/C-Signe
        /// [ Ligne 1 = "-" ]
        /// [ Ligne 2...n = "+" or "-" ]
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDDebitCreditFlag_1A_ { get; set; }

        /// <summary>
        /// ExchangeRate: Cours 1
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDExchangeRate_12_6_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ExchangeRate2: Cours 2
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDExchangeRate2_12_6_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ItemText: Libellé écritures - Texte principal
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDItemText_50A_ { get; set; }

        /// <summary>
        /// TextId: Texte ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDTextId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// StandardTextId: Libellé standard ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDStandardTextId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// StandardTextCode: Libellé standard (Code)
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDStandardTextCode_10A_ => string.Empty;

        /// <summary>
        /// AddItemText1: Libellé complémentaire
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDAddItemText1_50A_ { get; set; }

        /// <summary>
        /// CostCenterId: Centre de frais ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDCostCenterId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CostCenterCode: Centre de frais Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCostCenterCode_10A_ => string.Empty;

        /// <summary>
        /// CostTypeId: Type de frais ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDCostTypeId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CostTypeCode: Type de frais Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCostTypeCode_10A_ => string.Empty;

        /// <summary>
        /// MainOrderId: Ordre principal ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDMainOrderId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// OrderId: Ordre ID
        /// (mandatory: yes
        /// </summary>
        public double AFI_IIDOrderId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// MainOrderCode: Ordre principal Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDMainOrderCode_12A_ => string.Empty;

        /// <summary>
        /// OrderCode: Ordre Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDOrderCode_12A_ => string.Empty;

        /// <summary>
        /// FixedAssetId: Immobilisation ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDFixedAssetId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// FixedAssetCode: Immobilisation Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDFixedAssetCode_20A_ => string.Empty;

        /// <summary>
        /// FreeAllocationId: Code libre ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDFreeAllocationId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// FreeAllocationCode: Code libre Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDFreeAllocationCode_10A_ => string.Empty;

        /// <summary>
        /// Quantity: Quantité
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDQuantity_15_4_ => 0; // TODO Create config file to store value

        /// <summary>
        /// QuantityUnit: Unité de quantité
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDQuantityUnit_10A_ => string.Empty;

        /// <summary>
        /// BranchId: Filiale ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDBranchId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// BranchCode: Filiale Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDBranchCode_10A_ => string.Empty;

        /// <summary>
        /// DepartmentId: Département ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDDepartmentId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// DepartmentCode: Département Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDDepartmentCode_10A_ => string.Empty;

        /// <summary>
        /// VATType: TVA-Type [ 10 = DEB / 20 = CRE / 13 = 100% DEB / 23 = 100% CRE ]
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDVATType_2A_ => "20"; // TODO Create config file to store value

        /// <summary>
        /// IntExtCode: Code interne
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDIntExtCode_10A_ => string.Empty;

        /// <summary>
        /// VATCodeId: Code TVA ID
        /// [ Ligne 1 = 0 ]
        /// [ Ligne 2...n = 5 (M25I) or 1 (M8I) or 25 (M0I) ]
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDVATCodeId_11N_ { get; set; }

        /// <summary>
        /// VATCode: Code TVA Code
        /// [ Ligne 1 = <empty> ]
        /// [ Ligne 2...n = CH13 or M25I or M8I or M0I ]
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDVATCode_5A_ { get; set; }

        /// <summary>
        /// VATNumber: TVA ID
        /// [Ligne 1 = <empty> ]
        /// [Ligne 2 = No TVA ]
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIDVATNumber_14A_ { get; set; }

        /// <summary>
        /// VATRate: TVA taux
        /// [ Ligne 1 = 0 ]
        /// [ Ligne 2 = 2,5 or 8 ]
        /// (mandatory:yes)
        /// </summary>
        public decimal AFI_IIDVATRate_7_4_ { get; set; }

        /// <summary>
        /// VATDueDate: TVA Échéance
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIDVATDueDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATAmountOriginal: Montant TVA Base
        /// [ Ligne 1 = 0 ]
        /// [ Ligne 2 = Montant TVA inclus calculé ]
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATAmountOriginal_17_2_ { get; set; }

        /// <summary>
        /// VATAmountLocal: Montant TVA ME 1
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATAmountLocal_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATAmountLocal2: Montant TVA ME 2
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATAmountLocal2_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATRclAmountOrig: Montant TVA imputable base
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATRclmAmountOrig_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATRclAmountLoc: Montant TVA imputable ME 1
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATRclmAmountLoc_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATRclAmountLoc2: Montant TVA imputable ME 2
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATRclmAmountLoc2_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATRclmRate: Taux TVA imputable base
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDVATRclmRate_7_4_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATNonRclmAccount: Compte
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDVATNonRclmAccount_15A_ => string.Empty;

        /// <summary>
        /// VATNumberForGL: CG TVA ID
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDVATNumberForGL_14A_ => string.Empty;

        /// <summary>
        /// VATNameForGL: TVA Nom
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDVATNameForGL_30A_ => string.Empty;

        /// <summary>
        /// VATTextId: TVA Libellé complémentaire
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDVATTextId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ContractId: Contrat ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDContractId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ContractCode: Contrat Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDContractCode_20A_ => string.Empty;

        /// <summary>
        /// CurrentAccountId: Compte courant ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDCurrentAccountId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CurrentAccountCode: Compte courant Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCurrentAccountCode_20A_ => string.Empty;

        /// <summary>
        /// VendorNumber: No Vendeur
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDVendorNumber_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// BuyerNumber: No Acheteur
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIDBuyerNumber_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// TargetCompany: Société cible
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIDTargetCompany_5N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// TargetAccount: Compte cible
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDTargetAccount_15A_ => string.Empty;

        /// <summary>
        /// InterestValueDate: Valeur intérêt
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIDInterestValueDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// WorkflowMarking: Work-flow  caractéristiques
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDWorkflowMarking_1A_ => string.Empty;

        /// <summary>
        /// ProcessingVisa1: Visa 1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDProcessingVisa1_1A_ => string.Empty;

        /// <summary>
        /// ProcessingVisa2: Visa 2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDProcessingVisa2_1A_ => string.Empty;

        /// <summary>
        /// VATCntrlTotalLcl:
        /// (mandatory: )
        /// </summary>
        public decimal AFI_IIDVATCntrlTotalLcl_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// StackId: ID Lot
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDStackId_20A_ => string.Empty;

        /// <summary>
        /// ObjectBrandCode
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDObjectBrandCode_10A_ => string.Empty;

        /// <summary>
        /// ObjectModeCode
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDObjectModeCode_10A_ => string.Empty;

        /// <summary>
        /// ObjectTypeCode: Code type
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDObjectTypeCode_20A_ => string.Empty;

        /// <summary>
        /// ObjectTypeGroup: Groupe types
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDObjectTypeGroup_10A_ => string.Empty;

        /// <summary>
        /// Agreement: No Contrat
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDAgreement_20A_ => string.Empty;

        /// <summary>
        /// ObjectNumber: No Séquence
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDObjectNumber_20A_ => string.Empty;

        /// <summary>
        /// IntExtCodeOverride
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDIntExtCodeOverride_1A_ => string.Empty;

        /// <summary>
        /// CICustomerState1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerState1_1A_ => string.Empty;

        /// <summary>
        /// CICustomerState2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerState2_1A_ => string.Empty;

        /// <summary>
        /// CICustomerState3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerState3_1A_ => string.Empty;

        /// <summary>
        /// CICustomerVState1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerVState1_1A_ => string.Empty;

        /// <summary>
        /// CICustomerVState2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerVState2_1A_ => string.Empty;

        /// <summary>
        /// CICustomerVState3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerVState3_1A_ => string.Empty;

        /// <summary>
        /// CICustomerAmount
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDCICustomerAmount_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CICustomerVAmount
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIDCICustomerVAmount_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CICustomerText
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerText_15A_ => string.Empty;

        /// <summary>
        /// CICustomerVText
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDCICustomerVText_15A_ => string.Empty;

        /// <summary>
        /// TriangularDealIndicator
        /// (mandatory: no)
        /// </summary>
        public string AFI_TriangularDealIndicator_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDReservestatus1_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDReservestatus2_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDReservestatus3_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus4
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDReservestatus4_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus5
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIDReservestatus5_1A_ => string.Empty;

        public override string ToString()
        {
            return
                $"EnvironmentName: {APIEnvironmentName}, " +
                $"CompanyNumber: {ACX_CompanyNumber_5N_}, " +
                $"ITHItemNumber: {AFI_ITHItemNumber_11N_}, " +
                $"LineNumber: {AFI_IIDLineNumber_5N_}, " +
                $"AccountNumber: {AFI_IIDAccountNumber_15A_}, " +
                $"CurrencyCode: {AFI_IIDCurrencyCode_3A_}, " +
                $"OriginalAmount: {AFI_IIDOriginalAmount_17_2_}, " +
                $"DebitCreditFlag: {AFI_IIDDebitCreditFlag_1A_}, " +
                $"ItemText: {AFI_IIDItemText_50A_}, " +
                $"AddItemText1: {AFI_IIDAddItemText1_50A_}, " +
                $"VATCodeId: {AFI_IIDVATCodeId_11N_}, " +
                $"VATCode: {AFI_IIDVATCode_5A_}, " +
                $"VATNumber: {AFI_IIDVATNumber_14A_}, " +
                $"VATRate: {AFI_IIDVATRate_7_4_}, " +
                $"VATAmountOriginal: {AFI_IIDVATAmountOriginal_17_2_}, " +
                $"CICustomerAmount: {AFI_IIDCICustomerAmount_17_2_}, " +
                $"CICustomerVAmount: { AFI_IIDCICustomerVAmount_17_2_}, ";
        }
    }
}