﻿using AutoMapper;
using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using Gestion_Des_Vendanges.XpertFINServiceReference;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Gestion_Des_Vendanges.Services
{
    public static class Extensions
    {
        // TODO Move to general Extensions class
        public static int DateFormatXperFIN(this DateTime date)
        {
            bool canParse = int.TryParse($"{date.Year}{date.Month}{date.Day}", out int convertedDate);
            return canParse ? convertedDate : -1;
        }

        public static int DateTimeFormatXpertFIN(this DateTime date)
        {
            bool canParse = int.TryParse($"{date.Year}{date.Month}{date.Day}", out int convertedDate);
            return canParse ? convertedDate : -1;
        }

        public static string Flag(this string debitCredit)
        {
            if (debitCredit == "debit") return "+";
            if (debitCredit == "credit") return "-";

            return string.Empty;
        }
    }

    public static class DecimalExtensions
    {
        public static double RoundToFiveCents(this double value) => Math.Round(value * 20, 0) / 20;
    }

    public class XpertFINService
    {
        #region Fields

        private log4net.ILog _log = log4net.LogManager.GetLogger("XpertFIN");

        private const byte NUMBER_FIRST_LINE = 1;

        private XFINInterfacesServicePortTypeClient _xpertFinService;
        private XFINInterfacesIIHWriteInputData _iHWriteInputData;
        private XFINInterfacesIIDWriteInputData _iDWriteInputData_Line1;

        private DSPesees.MOC_MODE_COMPTABILISATIONDataTable _modeComptabilisations;
        private int _mocIdCurrentYear;

        private readonly PAI_PAIEMENTTableAdapter Paiements = ConnectionManager.Instance.CreateGvTableAdapter<PAI_PAIEMENTTableAdapter>();
        private readonly LIP_LIGNE_PAIEMENT_PESEETableAdapter LignePesees = ConnectionManager.Instance.CreateGvTableAdapter<LIP_LIGNE_PAIEMENT_PESEETableAdapter>();
        private readonly LIM_LIGNE_PAIEMENT_MANUELLETableAdapter LigneManuelles = ConnectionManager.Instance.CreateGvTableAdapter<LIM_LIGNE_PAIEMENT_MANUELLETableAdapter>();
        private readonly MOC_MODE_COMPTABILISATIONTableAdapter ModeComptabilisations = ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
        private readonly ANN_ANNEETableAdapter Annees = ConnectionManager.Instance.CreateGvTableAdapter<ANN_ANNEETableAdapter>();
        private readonly PRO_PROPRIETAIRETableAdapter Producteurs = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();

        private List<XpertFINpayment> _xpertFINpayments = null;
        private DSPesees _dataset = new DSPesees();

        #endregion Fields

        #region Constructors

        public XpertFINService()
        {
            _xpertFinService = new XFINInterfacesServicePortTypeClient("XFINInterfacesServiceSOAP11port_http");

            Paiements.FillByAnnId(_dataset.PAI_PAIEMENT, Utilities.IdAnneeCourante);
            LignePesees.Fill(_dataset.LIP_LIGNE_PAIEMENT_PESEE);
            LigneManuelles.Fill(_dataset.LIM_LIGNE_PAIEMENT_MANUELLE);
            ModeComptabilisations.FillByAnnId(_dataset.MOC_MODE_COMPTABILISATION, Utilities.IdAnneeCourante);
            Annees.Fill(_dataset.ANN_ANNEE);
            Producteurs.Fill(_dataset.PRO_PROPRIETAIRE);

            _log.Info($"[ New {nameof(XpertFINService)} ]");
        }

        #endregion Constructors

        #region Public Methods

        public void CreatePayments()
        {
            _xpertFINpayments = new List<XpertFINpayment>();
            var paymentCurrentYear = Paiements.GetDataByAnnId(Utilities.IdAnneeCourante);
            _modeComptabilisations = ModeComptabilisations.GetData();
            _mocIdCurrentYear = Annees.GetMocId(Utilities.IdAnneeCourante) ?? throw new ArgumentOutOfRangeException(nameof(XpertFINService), "Impossible de récupérer l'identifiant de l'année courante");

            var paiIds = new List<int>();

            foreach (var payment in paymentCurrentYear)
            {
                var rows = new Dictionary<int, XpertFINpaymentRow>();
                var currentPesees = LignePesees.GetDataByPai(payment.PAI_ID);
                var lignesManuelles = LigneManuelles.GetDataByPai(payment.PAI_ID);
                var erpId = Producteurs.GetAdrIdWinBIZ(payment.PRO_ID) ?? -1;
                bool isAssujetti = _dataset.PRO_PROPRIETAIRE.FirstOrDefault(p => p.PRO_ID == payment.PRO_ID).MOD_ID != (int)ModeTVA.Sans;

                XpertFINpaymentHeader header = GetHeader(payment);
                _log.Info($"[Create HEADER] {header.ToString()}");

                int numberCreatedRow = GetWeighingRow(payment, rows, erpId, isAssujetti);
                numberCreatedRow++; // Add +1 because below i start with 0

                for (int i = 0; i < lignesManuelles.Count; i++)
                {
                    rows.Add(i + numberCreatedRow, GetLigneManuelleRow(lignesManuelles[i], erpId, isAssujetti));
                    _log.Info($"[Create ROW ligne manuelle] {rows.LastOrDefault().Value.ToString()}");
                }

                SetLineNumber(rows);

                _xpertFINpayments.Add(new XpertFINpayment()
                {
                    Header = header,
                    Rows = rows
                });
            }
        }

        private void SetLineNumber(Dictionary<int, XpertFINpaymentRow> rows)
        {
            foreach (var row in rows)
            {
                row.Value.AFI_IIDLineNumber_5N_ = row.Key;
            }
        }

        public async Task<bool> ExportInvoices()
        {
            bool active = true; // TODO only for development purpose waiting for final implementation
            try
            {
                CreatePayments();

                if (_xpertFINpayments == null) throw new ArgumentNullException(nameof(XpertFINService), "La liste de paiements est nulle.");

                if (!active) throw new Exception("Blocked in test mode");// TODO only for development purpose waiting for final implementation

                foreach (var payment in _xpertFINpayments)
                {
                    _log.Info($"[Start export payment] BatchNumber: {payment.Header.AFI_IIHBatchNumber_11N_}");
                    iIHWriteResponse writeResponse = await _xpertFinService.iIHWriteAsync(GetRequest(payment.Header));

                    AddResponseValue(writeResponse, payment);

                    SendRows(payment.Rows);
                }
                // TODO Catch response message for errors, please ...

                return true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return false;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private XpertFINpaymentHeader GetHeader(DSPesees.PAI_PAIEMENTRow payment)
        {
            return new XpertFINpaymentHeader()
            {
                AFI_IIHDocumentNumber_15A_ = payment.PAI_ID.ToString(),
                AFI_IIHDocumentDate_8N_ = payment.PAI_DATE.DateFormatXperFIN(),
                AFI_IIHGLDate_8N_ = payment.PAI_DATE.DateFormatXperFIN(),
                AFI_IIHValueDate_8N_ = payment.PAI_DATE.DateFormatXperFIN(),
                AFI_IIHPartnerNumber_11N_ = _dataset.PRO_PROPRIETAIRE.FirstOrDefault(p => p.PRO_ID == payment.PRO_ID).ADR_ID_WINBIZ,
                AFI_IIHExternalRefNr_30A_ = $"{payment.PAI_TEXTE} {payment.PAI_NUMERO}",
                AFI_IIHBatchNumber_11N_ = payment.PAI_DATE.DateTimeFormatXpertFIN(),
                AFI_IIHCICustomerAmount_17_2_ = Convert.ToDecimal(payment.PAI_MONTANT_LIM_TTC + payment.PAI_MONTANT_LIM_TTC), // ?
                AFI_IIHCICustomerAAmount_17_2_ = Convert.ToDecimal(payment.PAI_MONTANT_LIM_TTC + payment.PAI_MONTANT_LIM_TTC), // ?
                AFI_IIHCICustomerPAmount_17_2_ = Convert.ToDecimal(payment.PAI_MONTANT_LIM_TTC + payment.PAI_MONTANT_LIM_TTC), // ?
            };
        }

        private int GetWeighingRow(DSPesees.PAI_PAIEMENTRow payment, Dictionary<int, XpertFINpaymentRow> dictionary, int erpId, bool isAssujetti)
        {
            var yearModeComptabilisation = _modeComptabilisations.FindByMOC_ID(_mocIdCurrentYear);

            dictionary.Add(1, new XpertFINpaymentRow()
            {
                ACX_CompanyNumber_5N_ = 2, // TODO Keep in config file
                AFI_IIDAccountNumber_15A_ = yearModeComptabilisation.MOC_NO_COMPTE_COMPTABLE.ToString(),
                AFI_IIDOriginalAmount_17_2_ = (decimal)(payment.PAI_MONTANT_LIP_TTC + payment.PAI_MONTANT_LIM_TTC).RoundToFiveCents(),
                AFI_IIDDebitCreditFlag_1A_ = yearModeComptabilisation.MOC_DEBIT_CREDIT?.Flag() ?? string.Empty,
                AFI_IIDItemText_50A_ = $"{payment.PAI_TEXTE} {payment.PAI_NUMERO}",
                AFI_IIDVATCodeId_11N_ = 0,
                AFI_IIDVATCode_5A_ = string.Empty,
                AFI_IIDVATNumber_14A_ = string.Empty,
                AFI_IIDVATRate_7_4_ = 0,
                AFI_IIDVATAmountOriginal_17_2_ = 0
            });

            _log.Info($"[Create ROW pesees] {dictionary[1].ToString()}");

            dictionary.Add(2, new XpertFINpaymentRow()
            {
                ACX_CompanyNumber_5N_ = 2, //erpId, // TODO ***************** test if is ok
                AFI_IIDAccountNumber_15A_ = "300035110", // TODO Set in a config file
                AFI_IIDOriginalAmount_17_2_ = (decimal)payment.PAI_MONTANT_LIP_NET.RoundToFiveCents(),
                AFI_IIDDebitCreditFlag_1A_ = "debit".Flag() ?? string.Empty,
                AFI_IIDItemText_50A_ = $"{payment.PAI_TEXTE} {payment.PAI_NUMERO}",
                AFI_IIDVATCodeId_11N_ = isAssujetti ? 35 : 0, // TODO Set in config file
                AFI_IIDVATCode_5A_ = isAssujetti ? "CH13" : string.Empty,
                AFI_IIDVATNumber_14A_ = string.Empty,
                AFI_IIDVATRate_7_4_ = isAssujetti ? (decimal)yearModeComptabilisation.MOC_TAUX_TVA : 0m,
                AFI_IIDVATAmountOriginal_17_2_ = (decimal)(payment.PAI_MONTANT_LIP_TTC - payment.PAI_MONTANT_LIP_NET).RoundToFiveCents()
            });
            _log.Info($"[Create ROW pesees] {dictionary[2].ToString()}");
            return 2;
        }

        private XpertFINpaymentRow GetLigneManuelleRow(DSPesees.LIM_LIGNE_PAIEMENT_MANUELLERow lm, int erpId, bool isAssujetti)
        {
            DSPesees.MOC_MODE_COMPTABILISATIONRow modeComptabilisationLigne = _modeComptabilisations.FindByMOC_ID(lm.MOC_ID);
            return new XpertFINpaymentRow()
            {
                ACX_CompanyNumber_5N_ = 2, //erpId, // TODO ***************** test if is ok
                AFI_IIDAccountNumber_15A_ = modeComptabilisationLigne.MOC_NO_COMPTE_COMPTABLE.ToString(),
                AFI_IIDOriginalAmount_17_2_ = Math.Abs((decimal)lm.LIM_MONTANT_HT),
                AFI_IIDDebitCreditFlag_1A_ = modeComptabilisationLigne.MOC_DEBIT_CREDIT?.Flag() ?? string.Empty,
                AFI_IIDItemText_50A_ = lm.LIM_TEXTE,
                AFI_IIDVATCodeId_11N_ = lm.LIM_TAUX_TVA > 0 ? 35 : 0, // TODO Add in config file
                AFI_IIDVATCode_5A_ = lm.LIM_TAUX_TVA > 0 ? "CH13" : string.Empty, // TODO Store in config file
                AFI_IIDVATNumber_14A_ = string.Empty,
                AFI_IIDVATRate_7_4_ = (decimal)lm.LIM_TAUX_TVA,
                AFI_IIDVATAmountOriginal_17_2_ = (decimal)Math.Abs((lm.LIM_MONTANT_TTC - lm.LIM_MONTANT_HT)).RoundToFiveCents()
            };
        }

        private void AddResponseValue(iIHWriteResponse writeResponse, XpertFINpayment payment)
        {
            var responseNumber = writeResponse.XFINInterfacesIIHWriteResponse
                                              .XFINInterfacesIIHWriteOutputData
                                              .XFINInterfacesIIHWriteOutputDataReturns
                                              .AFI_ITHItemNumber_11N_ ??
                                 throw new ArgumentNullException(nameof(XpertFINService), "L'entête ne contient pas de réponse.");
            _log.Info($"[HEADER Returned] value: {responseNumber}");
            foreach (var row in payment.Rows)
            {
                row.Value.AFI_ITHItemNumber_11N_ = responseNumber;
            }
        }

        private async void SendRows(Dictionary<int, XpertFINpaymentRow> rows)
        {
            foreach (var row in rows)
            {
                _log.Info($"[Sending ROW...] {row.Value}");
                iIDWriteResponse writeResponse = await _xpertFinService.iIDWriteAsync(GetRequest(row.Value));
                _log.Info($"[ROW return status = {writeResponse.XFINInterfacesIIDWriteResponse.XFINInterfacesIIDWriteOutputData.ReturnedStatus}]");
            }
        }

        private XFINInterfacesIIHWriteRequest GetRequest(XpertFINpaymentHeader header)
        {
            _log.Info($"[Sending HEADER...] {header.ToString()}");
            return new XFINInterfacesIIHWriteRequest
            {
                XFINInterfacesIIHWriteInputData = Mapper.Map<XFINInterfacesIIHWriteInputData>(header)
            };
        }

        private XFINInterfacesIIDWriteRequest GetRequest(XpertFINpaymentRow row)
        {
            return new XFINInterfacesIIDWriteRequest
            {
                XFINInterfacesIIDWriteInputData = Mapper.Map<XFINInterfacesIIDWriteInputData>(row)
            };
        }

        #endregion Private Methods

        #region For test purpose ...

        private void CreateInputData()
        {
            _iHWriteInputData = new XFINInterfacesIIHWriteInputData()
            {
                APIActionCode = string.Empty,
                APIEnvironmentName = "MAY",
                APIUserID = string.Empty,
                APIRightsCheck = string.Empty,
                ACX_CompanyNumber_5N_ = 1,
                AFI_IIHITGCode_5A_ = "KREFE",
                AFI_IIHItemOwner_10A_ = "Xpert",
                AFI_IIHDocumentNumber_15A_ = "0000000999",
                AFI_IIHExternalDocumentNr_30A_ = string.Empty,
                AFI_IIHDocumentDate_8N_ = 20170101,
                AFI_IIHGLDate_8N_ = 20170101,
                AFI_IIHValueDate_8N_ = 20170101,
                AFI_IIHItemType_2A_ = "75",
                AFI_IIHTextId_11N_ = 0,
                AFI_IIHPartnerNumber_11N_ = 510021,
                AFI_IIHExternalPartnerNr_20A_ = string.Empty,
                AFI_IIHPaymentCondition_5A_ = "11",
                AFI_IIHFixedFallingDueDate_8N_ = 0,
                AFI_IIHPaymentModeId_11N_ = 0,
                AFI_IIHPaymentModeCode_10A_ = string.Empty,
                AFI_IIHPaymentRelationNr_11N_ = 0,
                AFI_IIHPaymntRelTrnsactType_6A_ = string.Empty,
                AFI_IIHPaymntRelParticp_9A_ = string.Empty,
                AFI_IIHPaymntRelPriority_3N_ = 0,
                AFI_IIHPaymentStop_1A_ = string.Empty,
                AFI_IIHBranchId_11N_ = 0,
                AFI_IIHBranchCode_10A_ = string.Empty,
                AFI_IIHDepartmentId_11N_ = 0,
                AFI_IIHDepartmentCode_10A_ = string.Empty,
                AFI_IIHOperatorType_10A_ = string.Empty,
                AFI_IIHOperatorId_11N_ = 0,
                AFI_IIHOperatorCode_10A_ = string.Empty,
                AFI_IIHIntExtCode_10A_ = string.Empty,
                AFI_IIHExternalRefNr_30A_ = string.Empty,
                AFI_IIHYourReference_30A_ = string.Empty,
                AFI_IIHVESRReference_27A_ = string.Empty,
                AFI_IIHVESRCheckNrM11_2A_ = string.Empty,
                AFI_IIHReminderInhibit_1A_ = string.Empty,
                AFI_IIHLastReminderDate_8N_ = 0,
                AFI_IIHReminderDueDate_8N_ = 0,
                AFI_IIHReminderLevelId_11N_ = 0,
                AFI_IIHReminderLevelCode_3A_ = string.Empty,
                AFI_IIHReminderLvlAbstrID_11N_ = 0,
                AFI_IIHReminderLvlAbstrCde_3A_ = string.Empty,
                AFI_IIHReminderElimCount_3N_ = 0,
                AFI_IIHInterestState_1A_ = "N",
                AFI_IIHInterestStop_1A_ = string.Empty,
                AFI_IIHBarcode_30A_ = string.Empty,
                AFI_IIHScanInState_1A_ = "N",
                AFI_IIHDeliveryDate_8N_ = 0,
                AFI_IIHStackId_20A_ = string.Empty,
                AFI_IIHSectorId_11N_ = 0,
                AFI_IIHSectorCode_10A_ = string.Empty,
                AFI_IIHCalendarYear_4N_ = 0,
                AFI_IIHBatchNumber_11N_ = 20171025,
                AFI_IIHVATCntlTotalLcl_17_2_ = 0,
                AFI_IIHVATDueDate_8N_ = 0,
                AFI_IIHCICustomerState1_1A_ = string.Empty,
                AFI_IIHCICustomerState2_1A_ = string.Empty,
                AFI_IIHCICustomerState3_1A_ = string.Empty,
                AFI_IIHCICustomerAState1_1A_ = string.Empty,
                AFI_IIHCICustomerAState2_1A_ = string.Empty,
                AFI_IIHCICustomerAState3_1A_ = string.Empty,
                AFI_IIHCICustomerPState1_1A_ = string.Empty,
                AFI_IIHCICustomerPState2_1A_ = string.Empty,
                AFI_IIHCICustomerPState3_1A_ = string.Empty,
                AFI_IIHCICustomerAmount_17_2_ = 0.00m,
                AFI_IIHCICustomerAAmount_17_2_ = 0.00m,
                AFI_IIHCICustomerPAmount_17_2_ = 0.00m,
                AFI_IIHCICustomerText_15A_ = string.Empty,
                AFI_IIHCICustomerAText_15A_ = string.Empty,
                AFI_IIHCICustomerPText_15A_ = string.Empty,
                AFI_IIHUsageControlIPI_1A_ = string.Empty,
                AFI_IIHSpecialItemUsage_1A_ = string.Empty,
                AFI_IIHCollectivItem_1A_ = string.Empty,
                ACX_ClearingNumber_5A_ = string.Empty,
                ACX_ClearingBranch_4A_ = string.Empty,
                AFI_FPRPaymtbankaccount_50A_ = string.Empty,
                AFI_IIHSettlingState_1A_ = string.Empty,
                AFI_IIHAssigPartnerNbr_11N_ = 0,
                AFI_IIHAssigTypeSurrogate_11N_ = 0,
                AFI_IIHAssigTypeCode_10A_ = string.Empty,
                AFI_IIHAssigContractNbr_20A_ = string.Empty,
                AFI_IIHKindOfFUB_1A_ = string.Empty,
                AFI_IIHFUBProcessed_1A_ = string.Empty,
                AFI_IIHReservestatus1_1A_ = string.Empty,
                AFI_IIHReservestatus2_1A_ = string.Empty,
                AFI_IIHReservestatus3_1A_ = string.Empty,
                AFI_IIHReservestatus4_1A_ = string.Empty,
                AFI_IIHReservestatus5_1A_ = string.Empty,
                AFI_IIHReservestatus6_1A_ = string.Empty,
                AFI_IIHReservestatus7_1A_ = string.Empty,
                AFI_IIHReservestatus8_1A_ = string.Empty,
                AFI_IIHReservestatus9_1A_ = string.Empty,
                AFI_IIHReservestatus10_1A_ = string.Empty,
            };

            _iDWriteInputData_Line1 = new XFINInterfacesIIDWriteInputData()
            {
                APIActionCode = string.Empty,
                APIEnvironmentName = "UVA",
                APIUserID = string.Empty,
                APIRightsCheck = string.Empty,
                ACX_CompanyNumber_5N_ = 2,
                AFI_ITHItemNumber_11N_ = 0, // Numéro de séquence retourné par IHWrite
                AFI_IIDLineNumber_5N_ = 1,
                AFI_IIDAccountNumber_15A_ = "200000", // Compte collectif créanciers vendanges
                AFI_IIDFinYearAcctNr_20A_ = string.Empty,
                AFI_IIDCurrencyCode_3A_ = "CHF",
                AFI_IIDOriginalAmount_17_2_ = 0, // Montant Facture créancier
                AFI_IIDLocalAmount_17_2_ = 0,
                AFI_IIDLocalAmount2_17_2_ = 0,
                AFI_IIDDebitCreditFlag_1A_ = "-", // Toujours - pour la ligne 1 (debit/credit flag)
                AFI_IIDExchangeRate_12_6_ = 0,
                AFI_IIDExchangeRate2_12_6_ = 0,
                AFI_IIDItemText_50A_ = "Acompte 2017 no 1-679", // Texte principal
                AFI_IIDTextId_11N_ = 0,
                AFI_IIDStandardTextId_11N_ = 0,
                AFI_IIDStandardTextCode_10A_ = string.Empty,
                AFI_IIDAddItemText1_50A_ = "", // Libellé complémentaire (Si utile, sinon vide)
                AFI_IIDCostCenterId_11N_ = 0,
                AFI_IIDCostCenterCode_10A_ = string.Empty,
                AFI_IIDCostTypeId_11N_ = 0,
                AFI_IIDCostTypeCode_10A_ = string.Empty,
                AFI_IIDMainOrderId_11N_ = 0,
                AFI_IIDOrderId_11N_ = 0,
                AFI_IIDMainOrderCode_12A_ = string.Empty,
                AFI_IIDOrderCode_12A_ = string.Empty,
                AFI_IIDFixedAssetId_11N_ = 0,
                AFI_IIDFixedAssetCode_20A_ = string.Empty,
                AFI_IIDFreeAllocationId_11N_ = 0,
                AFI_IIDFreeAllocationCode_10A_ = string.Empty,
                AFI_IIDQuantity_15_4_ = 0,
                AFI_IIDQuantityUnit_10A_ = string.Empty,
                AFI_IIDBranchId_11N_ = 0,
                AFI_IIDBranchCode_10A_ = string.Empty,
                AFI_IIDDepartmentId_11N_ = 0,
                AFI_IIDDepartmentCode_10A_ = string.Empty,
                AFI_IIDVATType_2A_ = "20", //
                AFI_IIDIntExtCode_10A_ = string.Empty,
                AFI_IIDVATCodeId_11N_ = 0,
                AFI_IIDVATCode_5A_ = "",
                AFI_IIDVATNumber_14A_ = "",
                AFI_IIDVATRate_7_4_ = 0,
                AFI_IIDVATDueDate_8N_ = 0,
                AFI_IIDVATAmountOriginal_17_2_ = 0,
                AFI_IIDVATAmountLocal_17_2_ = 0,
                AFI_IIDVATAmountLocal2_17_2_ = 0,
                AFI_IIDVATRclmAmountOrig_17_2_ = 0,
                AFI_IIDVATRclmAmountLoc_17_2_ = 0,
                AFI_IIDVATRclmAmountLoc2_17_2_ = 0,
                AFI_IIDVATRclmRate_7_4_ = 0,
                AFI_IIDVATNonRclmAccount_15A_ = string.Empty,
                AFI_IIDVATNumberForGL_14A_ = string.Empty,
                AFI_IIDVATNameForGL_30A_ = string.Empty,
                AFI_IIDVATTextId_11N_ = 0,
                AFI_IIDContractId_11N_ = 0,
                AFI_IIDContractCode_20A_ = string.Empty,
                AFI_IIDCurrentAccountId_11N_ = 0,
                AFI_IIDCurrentAccountCode_20A_ = string.Empty,
                AFI_IIDVendorNumber_11N_ = 0,
                AFI_IIDBuyerNumber_11N_ = 0,
                AFI_IIDTargetCompany_5N_ = 0,
                AFI_IIDTargetAccount_15A_ = string.Empty,
                AFI_IIDInterestValueDate_8N_ = 0,
                AFI_IIDWorkflowMarking_1A_ = string.Empty,
                AFI_IIDProcessingVisa1_1A_ = string.Empty,
                AFI_IIDProcessingVisa2_1A_ = string.Empty,
                AFI_IIDVATCntrlTotalLcl_17_2_ = 0,
                AFI_IIDStackId_20A_ = string.Empty,
                AFI_IIDObjectBrandCode_10A_ = string.Empty,
                AFI_IIDObjectModeCode_10A_ = string.Empty,
                AFI_IIDObjectTypeCode_20A_ = string.Empty,
                AFI_IIDObjectTypeGroup_10A_ = string.Empty,
                AFI_IIDAgreement_20A_ = string.Empty,
                AFI_IIDObjectNumber_20A_ = string.Empty,
                AFI_IIDIntExtCodeOverride_1A_ = string.Empty,
                AFI_IIDCICustomerState1_1A_ = string.Empty,
                AFI_IIDCICustomerState2_1A_ = string.Empty,
                AFI_IIDCICustomerState3_1A_ = string.Empty,
                AFI_IIDCICustomerVState1_1A_ = string.Empty,
                AFI_IIDCICustomerVState2_1A_ = string.Empty,
                AFI_IIDCICustomerVState3_1A_ = string.Empty,
                AFI_IIDCICustomerAmount_17_2_ = 0,
                AFI_IIDCICustomerVAmount_17_2_ = 0,
                AFI_IIDCICustomerText_15A_ = string.Empty,
                AFI_IIDCICustomerVText_15A_ = string.Empty,
                AFI_TriangularDealIndicator_1A_ = string.Empty,
                AFI_IIDReservestatus1_1A_ = string.Empty,
                AFI_IIDReservestatus2_1A_ = string.Empty,
                AFI_IIDReservestatus3_1A_ = string.Empty,
                AFI_IIDReservestatus4_1A_ = string.Empty,
                AFI_IIDReservestatus5_1A_ = string.Empty
            };
        }

        #endregion For test purpose ...
    }
}