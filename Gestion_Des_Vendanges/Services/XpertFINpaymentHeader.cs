﻿namespace Gestion_Des_Vendanges.Services
{
    public class XpertFINpaymentHeader
    {
        /// <summary>
        /// ActionCode
        /// (mandatory: no)
        /// </summary>
        public string APIActionCode => string.Empty;

        /// <summary>
        /// EnvironmentName: Nom de l'environnement
        /// (mandatory: yes)
        /// </summary>
        public string APIEnvironmentName => "UV"; // TODO Create config file to store value

        /// <summary>
        /// UserID
        /// (mandatory: no)
        /// </summary>
        public string APIUserID => string.Empty;

        /// <summary>
        /// RightsCheck: Vérification des droits
        /// (mandatory: no)
        /// </summary>
        public string APIRightsCheck => string.Empty;

        /// <summary>
        /// CompanyNumber: No de société dans XFIN
        /// (mandatory:yes)
        /// </summary>
        public int? ACX_CompanyNumber_5N_ => 2; // TODO Create config file to store value

        /// <summary>
        /// ITGCode: Groupes de pièces (XFIN)
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHITGCode_5A_ => "GDV"; // TODO Create config file to store value

        /// <summary>
        /// ItemOwner: Émetteur de pièce - utilisateur XFIN
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHItemOwner_10A_ => "GDV"; // TODO Create config file to store value

        /// <summary>
        /// DocumentNumber: No de pièce facturation [ Id facture GDV ] 10 positions
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHDocumentNumber_15A_ { get; set; }

        /// <summary>
        /// ExternalDocumentNr: No de pièce externe
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHExternalDocumentNr_30A_ => string.Empty;

        /// <summary>
        /// DocumentDate: Date pièce [ format: yyyymmdd ]
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHDocumentDate_8N_ { get; set; }

        /// <summary>
        /// GLDate: Date comptable [ format: yyyymmdd ]
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHGLDate_8N_ { get; set; }

        /// <summary>
        /// ValueDate: Date valeur [ format: yyyymmdd ]
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHValueDate_8N_ { get; set; }

        /// <summary>
        /// ItemType: Type écriture [ DEB = 45 / CRE = 75 / CG = 05 ]
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHItemType_2A_ => "75"; // TODO Create config file to store value

        /// <summary>
        /// TextId: ID Texte
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHTextId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// PartnerNumber: Partenaire - No de créancier
        /// (mandatory: true)
        /// </summary>
        public double AFI_IIHPartnerNumber_11N_ { get; set; }

        /// <summary>
        /// ExternalPartnerNr: ID partenaire externe
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHExternalPartnerNr_20A_ => string.Empty;

        /// <summary>
        /// PaymentCondition: Condition de paiement - Crossup-XCXX (Conditions de paiement 30 jours)
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHPaymentCondition_5A_ => "D30"; // TODO Create config file to store value

        /// <summary>
        /// FixedFallingDueDate: Date échéance (calculée auto dans XFIN)
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHFixedFallingDueDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// PaymentModeId: Mode paiement (ID)
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHPaymentModeId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// PaymentModeCode: Mode paiement (Code)
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHPaymentModeCode_10A_ => string.Empty;

        /// <summary>
        /// PaymentRelationNr: Relation paiement (ID)
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHPaymentRelationNr_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// PaymntRelTrnsactType: Type de transaction
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHPaymntRelTrnsactType_6A_ => string.Empty;

        /// <summary>
        /// PaymntRelParticp: Participant
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHPaymntRelParticp_9A_ => string.Empty;

        /// <summary>
        /// PaymntRelPriority: Priorité
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHPaymntRelPriority_3N_ => 1; // TODO Create config file to store value

        /// <summary>
        /// PaymentStop: Blocage paiement
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHPaymentStop_1A_ => string.Empty;

        /// <summary>
        /// BranchId: Filiale (ID)
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHBranchId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// BranchCode: Filiale (Code)
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHBranchCode_10A_ => string.Empty;

        /// <summary>
        /// DepartmentId: Département ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHDepartmentId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// DepartmentCode: Département code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHDepartmentCode_10A_ => string.Empty;

        /// <summary>
        /// OperatorType
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHOperatorType_10A_ => string.Empty;

        /// <summary>
        /// OperatorId: Responsable ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHOperatorId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// OperatorCode: Responsable code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHOperatorCode_10A_ => string.Empty;

        /// <summary>
        /// IntExtCode: Code Interne
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHIntExtCode_10A_ => string.Empty;

        /// <summary>
        /// ExternalRefNr: Référence paiement
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHExternalRefNr_30A_ { get; set; }

        /// <summary>
        /// YourReference: Votre référence
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHYourReference_30A_ => string.Empty;

        /// <summary>
        /// VESRReference: Référence BVR
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHVESRReference_27A_ => string.Empty;

        /// <summary>
        /// VESRCheckNrM11: Chiffre contrôle
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHVESRCheckNrM11_2A_ => string.Empty;

        /// <summary>
        /// ReminderInhibit: Rappel bloqué
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReminderInhibit_1A_ => string.Empty;

        /// <summary>
        /// LastReminderDate: Date dernier rappel
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHLastReminderDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ReminderDueDate: Échéance
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHReminderDueDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ReminderLevelId: Niveau rappel ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHReminderLevelId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ReminderLevelCode: Niveau rappel Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReminderLevelCode_3A_ => string.Empty;

        /// <summary>
        ///  ReminderLvlAbstrID: Niveau rappel théorique ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHReminderLvlAbstrID_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// ReminderLvlAbstrCde: Niveau rappel théorique Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReminderLvlAbstrCde_3A_ => string.Empty;

        /// <summary>
        /// ReminderElimCount: Exclusions rappel
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHReminderElimCount_3N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// InterestState: État intérêt
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHInterestState_1A_ => "N"; // TODO Create config file to store value

        /// <summary>
        /// InterestStop: Exclusions rappel
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHInterestStop_1A_ => "0"; // TODO Create config file to store value

        /// <summary>
        /// Bar code: Code-barre
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHBarcode_30A_ => string.Empty;

        /// <summary>
        /// ScanInState: Caractéristique Scanning
        /// (mandatory: yes)
        /// </summary>
        public string AFI_IIHScanInState_1A_ => "N"; // TODO Create config file to store value

        /// <summary>
        /// DeliveryDate: Date de livraison
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHDeliveryDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// StackID
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHStackId_20A_ => string.Empty;

        /// <summary>
        /// SectorId: Secteur ID
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHSectorId_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// SectorCode: Secteur Code
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHSectorCode_10A_ => string.Empty;

        /// <summary>
        /// CalendatYear: Année civile
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHCalendarYear_4N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// BatchNumber: No de lot [ Date et heure yyyymmddhhmmss ]
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHBatchNumber_11N_ { get; set; }

        /// <summary>
        /// VATCntlTotalLcl: TVA Contrôle montant
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIHVATCntlTotalLcl_17_2_ => 0; // TODO Create config file to store value

        /// <summary>
        /// VATDueDate: TVA échéance
        /// (mandatory: yes)
        /// </summary>
        public int AFI_IIHVATDueDate_8N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// CustomerState1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerState1_1A_ => string.Empty;

        /// <summary>
        /// CustomerState2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerState2_1A_ => string.Empty;

        /// <summary>
        /// CustomerState3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerState3_1A_ => string.Empty;

        /// <summary>
        /// CustomerAState1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerAState1_1A_ => string.Empty;

        /// <summary>
        /// CustomerAState2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerAState2_1A_ => string.Empty;

        /// <summary>
        /// CustomerAState3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerAState3_1A_ => string.Empty;

        /// <summary>
        /// CustomerPState1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerPState1_1A_ => string.Empty;

        /// <summary>
        /// CustomerPState2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerPState2_1A_ => string.Empty;

        /// <summary>
        /// CustomerPState3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerPState3_1A_ => string.Empty;

        /// <summary>
        /// CustomerAmount
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIHCICustomerAmount_17_2_ { get; set; }

        /// <summary>
        /// CustomerAAmount
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIHCICustomerAAmount_17_2_ { get; set; }

        /// <summary>
        /// CustomerPAmount
        /// (mandatory: yes)
        /// </summary>
        public decimal AFI_IIHCICustomerPAmount_17_2_ { get; set; }

        /// <summary>
        /// CustomerText
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerText_15A_ => string.Empty;

        /// <summary>
        /// CustomerAText
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerAText_15A_ => string.Empty;

        /// <summary>
        /// CustomerPText
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCICustomerPText_15A_ => string.Empty;

        /// <summary>
        /// UsageControlIPI
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHUsageControlIPI_1A_ => string.Empty;

        /// <summary>
        /// SpecialItemUsage
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHSpecialItemUsage_1A_ => string.Empty;

        /// <summary>
        /// CollectivItem
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHCollectivItem_1A_ => string.Empty;

        /// <summary>
        /// ClearingNumber
        /// (mandatory: no)
        /// </summary>
        public string ACX_ClearingNumber_5A_ => string.Empty;

        /// <summary>
        /// ClearingBranch
        /// (mandatory: no)
        /// </summary>
        public string ACX_ClearingBranch_4A_ => string.Empty;

        /// <summary>
        /// FPRPaymtbankaccount
        /// (mandatory: no)
        /// </summary>
        public string AFI_FPRPaymtbankaccount_50A_ => string.Empty;

        /// <summary>
        /// SettlingState
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHSettlingState_1A_ => string.Empty;

        /// <summary>
        /// AssigPartnerNbr
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHAssigPartnerNbr_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// AssigTypeSurrogate
        /// (mandatory: yes)
        /// </summary>
        public double AFI_IIHAssigTypeSurrogate_11N_ => 0; // TODO Create config file to store value

        /// <summary>
        /// AssigTypeCode
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHAssigTypeCode_10A_ => string.Empty;

        /// <summary>
        /// AssigContractNbr
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHAssigContractNbr_20A_ => string.Empty;

        /// <summary>
        /// KindOfFUB
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHKindOfFUB_1A_ => string.Empty;

        /// <summary>
        /// FUBProcessed
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHFUBProcessed_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus1
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus1_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus2
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus2_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus3
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus3_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus4
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus4_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus5
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus5_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus6
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus6_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus7
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus7_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus8
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus8_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus9
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus9_1A_ => string.Empty;

        /// <summary>
        /// Reservestatus10
        /// (mandatory: no)
        /// </summary>
        public string AFI_IIHReservestatus10_1A_ => string.Empty;

        public override string ToString()
        {
            return
            $"EnvironmentName: {APIEnvironmentName}, " +
            $"CompanyNumber: { ACX_CompanyNumber_5N_}, " +
            $"ITGCode: { AFI_IIHITGCode_5A_ }, " +
            $"ItemOwner: {AFI_IIHItemOwner_10A_ }, " +
            $"DocumentNumber: {AFI_IIHDocumentNumber_15A_ }, " +
            $"DocumentDate: {AFI_IIHDocumentDate_8N_ }, " +
            $"GLDate: {AFI_IIHGLDate_8N_ }, " +
            $"ValueDate: {AFI_IIHValueDate_8N_ }, " +
            $"ItemType: {AFI_IIHItemType_2A_ }, " +
            $"PartnerNumber: {AFI_IIHPartnerNumber_11N_ }, " +
            $"PaymentCondition: {AFI_IIHPaymentCondition_5A_ }, " +
            $"InterestState: {AFI_IIHInterestState_1A_ }, " +
            $"ScanInState: {AFI_IIHScanInState_1A_ }, " +
            $"BatchNumber: {AFI_IIHBatchNumber_11N_}, ";
        }
    }
}