﻿using System.Collections.Generic;

namespace Gestion_Des_Vendanges.Services
{
    public class XpertFINpayment
    {
        public XpertFINpaymentHeader Header { get; set; }
        public Dictionary<int, XpertFINpaymentRow> Rows { get; set; }
    }
}