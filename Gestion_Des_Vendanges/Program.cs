﻿using Gestion_Des_Vendanges.GUI;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges
{
    /* *
    *  Description: Point d'entrée principal de l'application
    *  Date de création:
    *  Modifications:
    * */

    internal static class Program
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        private static void Main(params string[] parametres)
        {
            //add culture for bug Windows 8 : New culture info for "fr-CH" default separator decimal = ","
            var ci = new CultureInfo("fr-CH");
            ci.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                //Abonnement à l’événement UnhandledException (déclenché lorsque une exception n'est pas récupérée) : écrit le fichier de logs
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                Application.Run(new WfrmGestionDesVendanges());
            }
            catch (ObjectDisposedException e)
            {
                //Se produit lorsqu'on relance le programme par programmation (Mise à jour ou changement de BD)
                Log.Fatal("Main", e);
            }
            catch (Exception e)
            {
                string message = "Le programme a rencontré une erreur inattendue et va se terminer.\n" +
                    e.Message + "\nSource: \n" + e.Source;
                string source = "Gestion des vendanges";
                Log.Fatal(message, e);
                MessageBox.Show(message, source, MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (!EventLog.SourceExists(source)) EventLog.CreateEventSource(source, "Application");

                EventLog.WriteEntry(source, message, EventLogEntryType.Error);
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Log.DebugFormat("CurrentDomain_UnhandledException(sender:{0}, e:{1})", sender, e);
            Log.Debug(e);
        }
    }
}