/*****************************************************/ 
/*** Script to insert new quota in gdv-vd database ***/
/***    ! please change values if needed	       ***/
/***    ! Uncomment code to insert values          ***/
/*** Gerber Systems & Networks                     ***/
/*** Juan Carlos GARCIA VILARCHAO - 2017-06-12     ***/
/*****************************************************/


/************/
/*** INIT ***/
/************/

/**************************************************************************/
/*** Grapes ***/
DECLARE @chasselas int = 0
DECLARE @pinotNoir int = 0
DECLARE @gamay int = 0
DECLARE @garanoir int = 0
DECLARE @gamaret int = 0
DECLARE @merlot int = 0

SET @chasselas = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'chasselas')
SET @pinotNoir = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'pinot noir')
SET @gamay = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'gamay')
SET @garanoir = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'garanoir')
SET @gamaret = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'gamaret')
SET @merlot = (SELECT CEP_ID FROM CEP_CEPAGE WHERE CEP_NOM like 'merlot')

PRINT N'@chasselas: ' + CAST(@chasselas AS nvarchar(5));
PRINT N'@pinotNoir: ' + CAST(@pinotNoir AS nvarchar(5));
PRINT N'@gamay: ' + CAST(@gamay AS nvarchar(5));
PRINT N'@garanoir: ' + CAST(@garanoir AS nvarchar(5));
PRINT N'@gamaret: ' + CAST(@gamaret AS nvarchar(5));
PRINT N'@merlot: ' + CAST(@merlot AS nvarchar(5));

/**************************************************************************/
/*** Régions ***/
DECLARE @Chablais int = 0 
DECLARE @Lavaux int = 0
DECLARE @LaCote int = 0
DECLARE @Orbe int = 0
DECLARE @Bonvillars int = 0
DECLARE @Vully int = 0

SET @Chablais = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'chablais')
SET @Lavaux = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'lavaux')
SET @LaCote = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'la côte')
SET @Orbe = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'côtes-de-l''orbe')
SET @Bonvillars = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'bonvillars')
SET @Vully = (SELECT REG_ID FROM REG_REGION WHERE REG_NAME like 'vully')

PRINT N'@Chablais: ' + CAST(@Chablais AS nvarchar(5))
PRINT N'@Lavaux: ' + CAST(@Lavaux AS nvarchar(5))
PRINT N'@LaCote: ' + CAST(@LaCote AS nvarchar(5))
PRINT N'@Orbe: ' + CAST(@Orbe AS nvarchar(5))
PRINT N'@Bonvillars: ' + CAST(@Bonvillars AS nvarchar(5))
PRINT N'@Vully: ' + CAST(@Vully AS nvarchar(5))


/*****************************/
/*** ! PLEASE SET VALUES ! ***/
/*****************************/

/**************************************************************************/
/*** Year ***/
DECLARE @year int = 2017

/**************************************************************************/
/*** Chasselas QUO_AOC ***/
DECLARE @Chasselas_Chablais float = 1
DECLARE @Chasselas_Lavaux float = 1
DECLARE @Chasselas_LaCote float = 0.96
DECLARE @Chasselas_Orbe float = 0.96
DECLARE @Chasselas_Bonvillars float = 0.96
DECLARE @Chasselas_Vully float = 0.88

/**************************************************************************/
/*** Autres cépages blancs QUO_AOC ***/
DECLARE @AutresBlancs_Chablais float = 0.96
DECLARE @AutresBlancs_Lavaux float = 0.92
DECLARE @AutresBlancs_LaCote float = 0.90
DECLARE @AutresBlancs_Orbe float = 0.90
DECLARE @AutresBlancs_Bonvillars float = 0.90
DECLARE @AutresBlancs_Vully float = 0.88

/**************************************************************************/
/*** Pinot noir QUO_AOC ***/
DECLARE @PinotNoir_Chablais float = 0.76
DECLARE @PinotNoir_Lavaux float = 0.72
DECLARE @PinotNoir_LaCote float = 0.72
DECLARE @PinotNoir_Orbe float = 0.72
DECLARE @PinotNoir_Bonvillars float = 0.72
DECLARE @PinotNoir_Vully float = 0.80

/**************************************************************************/
/*** Gamay et Garanoir QUO_AOC ***/
DECLARE @Gamay_Garanoir_Chablais float = 0.80
DECLARE @Gamay_Garanoir_Lavaux float = 0.80
DECLARE @Gamay_Garanoir_LaCote float = 0.80
DECLARE @Gamay_Garanoir_Orbe float = 0.80
DECLARE @Gamay_Garanoir_Bonvillars float = 0.80
DECLARE @Gamay_Garanoir_Vully float = 0.88

/**************************************************************************/
/*** Autres cépages blancs QUO_AOC ***/
DECLARE @AutresRouges_Chablais float = 0.72
DECLARE @AutresRouges_Lavaux float = 0.72
DECLARE @AutresRouges_LaCote float = 0.72
DECLARE @AutresRouges_Orbe float = 0.72
DECLARE @AutresRouges_Bonvillars float = 0.72
DECLARE @AutresRouges_Vully float = 0.88

/**************************************************************************/
/*** Premiers Grands Crus ***/
DECLARE @premierGrandCru_Chasselas float = 0.8
DECLARE @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot float = 0.64
DECLARE @premierGrandCru_NO float = 0

/**************************************************************************/
/*** Vins de pays ***/
DECLARE @VinsPays_BLANC float = 1.44
DECLARE @VinsPays_ROUGE float = 1.28

/**************************************************************************/
/*** Vins de table ***/
DECLARE @VinTable float = 0

/**************************************************************************/
/*** Temp table to handle data for Autres blancs and Autres rouges ***/
DECLARE @CEPAGES TABLE(
	CEP_ID int,
	REG_ID int,
	ANN_AN int,
	QUO_AOC float,
	QUO_PREMIER_GRANDS_CRUS float,
	QUO_VINS_PAYS float,
	QUO_VINS_TABLE float
)


/**************/
/*** INSERT ***/
/**************/

/**************************************************************************/
/*** Chasselas ***/
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @Chablais, @year, @Chasselas_Chablais, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @Lavaux, @year, @Chasselas_Lavaux, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @LaCote, @year, @Chasselas_LaCote, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @Orbe, @year, @Chasselas_Orbe, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @Bonvillars, @year, @Chasselas_Bonvillars, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@chasselas, @Vully, @year, @Chasselas_Vully, @premierGrandCru_Chasselas, @VinsPays_BLANC, @VinTable)

/**************************************************************************/
/*** Autres blancs ***/
/* Declare temp table */
DELETE FROM @CEPAGES

INSERT INTO @CEPAGES (CEP_ID, REG_ID, ANN_AN, QUO_PREMIER_GRANDS_CRUS, QUO_VINS_PAYS, QUO_VINS_TABLE)
	SELECT CEP_ID, REG_ID, ANN_AN = @year, QUO_PREMIER_GRANDS_CRUS = @premierGrandCru_NO, QUO_VINS_PAYS = @VinsPays_BLANC, QUO_VINS_TABLE = @VinTable
		FROM CEP_CEPAGE, REG_REGION 
		WHERE COU_ID = 1
			AND CEP_NOM NOT IN ('chasselas')

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_Chablais
	WHERE REG_ID = @Chablais

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_Lavaux
	WHERE REG_ID = @Lavaux

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_LaCote
	WHERE REG_ID = @LaCote

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_Orbe
	WHERE REG_ID = @Orbe

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_Bonvillars
	WHERE REG_ID = @Bonvillars

UPDATE @CEPAGES
	SET QUO_AOC = @AutresBlancs_Vully
	WHERE REG_ID = @Vully

INSERT INTO QUO_QUOTA_PRODUCTION
	SELECT * FROM @CEPAGES

/**************************************************************************/
/*** Pinot noir ***/
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @Chablais, @year, @PinotNoir_Chablais, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @Lavaux, @year, @PinotNoir_Lavaux, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @LaCote, @year, @PinotNoir_LaCote, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @Orbe, @year, @PinotNoir_Orbe, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @Bonvillars, @year, @PinotNoir_Bonvillars, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@pinotNoir, @Vully, @year, @PinotNoir_Vully, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)

/**************************************************************************/
/*** Gamay ***/
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @Chablais, @year, @Gamay_Garanoir_Chablais, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @Lavaux, @year, @Gamay_Garanoir_Lavaux, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @LaCote, @year, @Gamay_Garanoir_LaCote, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @Orbe, @year, @Gamay_Garanoir_Orbe, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @Bonvillars, @year, @Gamay_Garanoir_Bonvillars, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@gamay, @Vully, @year, @Gamay_Garanoir_Vully, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
/*** Garanoir ***/
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @Chablais, @year, @Gamay_Garanoir_Chablais, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @Lavaux, @year, @Gamay_Garanoir_Lavaux, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @LaCote, @year, @Gamay_Garanoir_LaCote, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @Orbe, @year, @Gamay_Garanoir_Orbe, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @Bonvillars, @year, @Gamay_Garanoir_Bonvillars, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)
INSERT INTO QUO_QUOTA_PRODUCTION VALUES (@garanoir, @Vully, @year, @Gamay_Garanoir_Vully, @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot, @VinsPays_ROUGE, @VinTable)

/**************************************************************************/
/*** Autres rouges ***/
/* Declare temp table */
DELETE FROM @CEPAGES

INSERT INTO @CEPAGES (CEP_ID, REG_ID, ANN_AN, QUO_PREMIER_GRANDS_CRUS, QUO_VINS_PAYS, QUO_VINS_TABLE)
	SELECT CEP_ID, REG_ID, ANN_AN = @year, QUO_PREMIER_GRANDS_CRUS = @premierGrandCru_NO, QUO_VINS_PAYS = @VinsPays_ROUGE, QUO_VINS_TABLE = @VinTable
		FROM CEP_CEPAGE, REG_REGION 
		WHERE COU_ID = 2
			AND CEP_NOM NOT IN ('pinot noir', 'gamay', 'garanoir')

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_Chablais
	WHERE REG_ID = @Chablais

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_Lavaux
	WHERE REG_ID = @Lavaux

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_LaCote
	WHERE REG_ID = @LaCote

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_Orbe
	WHERE REG_ID = @Orbe

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_Bonvillars
	WHERE REG_ID = @Bonvillars

UPDATE @CEPAGES
	SET QUO_AOC = @AutresRouges_Vully
	WHERE REG_ID = @Vully

/*** Update Premiers grands crus for Gamaret and Merlot ***/
UPDATE @CEPAGES
	SET QUO_PREMIER_GRANDS_CRUS = @premierGrandCru_PinotNoir_Gamay_Gamaret_Garanoir_Merlot
	WHERE CEP_ID IN (@gamaret, @merlot)

INSERT INTO QUO_QUOTA_PRODUCTION
	SELECT * FROM @CEPAGES
