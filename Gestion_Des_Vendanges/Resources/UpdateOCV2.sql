update cep_cepage set cep_nom = 'Müller Thurgau' where CEP_NOM = 'Riesling X Sylvaner'
update cep_cepage set cep_nom = 'Gewürztraminer' where cep_nom = 'Gewürztaminer'
update cep_cepage set cep_nom = 'Ancellotta' where cep_nom = 'Ancelotta'
update cep_cepage set cep_nom = 'Plant Robert' where cep_nom = 'Plant Robert Robez'
update cep_cepage set cep_nom = 'Marsanne blanche/Hermitage' where cep_nom = 'Marsanne'
update cep_cepage set cep_numero = 52 where CEP_ID = 154 and CEP_NUMERO IS NULL
update cep_cepage set cep_numero = 53 where CEP_ID = 156 and CEP_NUMERO IS NULL
update cep_cepage set cep_numero = 54 where CEP_ID = 157 and CEP_NUMERO IS NULL
update cep_cepage set cep_numero = 66 where CEP_ID = 185 and CEP_NUMERO IS NULL
update cep_cepage set cep_numero = 68 where CEP_ID = 187 and CEP_NUMERO IS NULL
update MCE_MOC_CEP set CEP_ID = 201 where CEP_ID = 208 AND (select COUNT(*) from CEP_CEPAGE where CEP_NOM = 'Nobling' AND (CEP_ID = 208 OR CEP_ID = 201)) = 2
update PAR_PARCELLE set CEP_ID = 201 where CEP_ID = 208 AND (select COUNT(*) from CEP_CEPAGE where CEP_NOM = 'Nobling' AND (CEP_ID = 208 OR CEP_ID = 201)) = 2
update PED_PESEE_DETAIL set CEP_ID = 201 where CEP_ID = 208 AND (select COUNT(*) from CEP_CEPAGE where CEP_NOM = 'Nobling' AND (CEP_ID = 208 OR CEP_ID = 201)) = 2
update VAL_VALEUR_CEPAGE set CEP_ID = 201 where CEP_ID = 208 AND (select COUNT(*) from CEP_CEPAGE where CEP_NOM = 'Nobling' AND (CEP_ID = 208 OR CEP_ID = 201)) = 2  
delete from cep_cepage where CEP_ID = 208 AND (select COUNT(*) from CEP_CEPAGE where CEP_NOM = 'Nobling' AND (CEP_ID = 208 OR CEP_ID = 201)) = 2
delete from LIC_LIEU_COMMUNE where COM_ID in (select COM_ID from  COM_COMMUNE COM WHERE COM.COM_ID NOT IN (SELECT COM_ID FROM  ACQ_ACQUIT) AND COM_ID IN (146, 147, 148, 149, 150, 151, 152, 185, 186, 187, 192,  205, 219, 228, 168,  181))
delete from COM_COMMUNE where COM_ID in (select COM_ID from  COM_COMMUNE COM WHERE COM.COM_ID NOT IN (SELECT COM_ID FROM  ACQ_ACQUIT) AND COM_ID IN (146, 147, 148, 149, 150, 151, 152, 185, 186, 187, 192,  205, 219, 228, 168,  181))
insert into COM_COMMUNE values ('Vully-les-Lacs', 5464, 2)
insert into COM_COMMUNE values ('Chavannes-le-Veyron', 5475, 6)
insert into COM_COMMUNE values ('Bourg-en-Lavaux', 5613, 5)
insert into LIC_LIEU_COMMUNE select COM_ID, 31 from COM_COMMUNE where COM_NOM = 'Vully-les-Lacs'
insert into LIC_LIEU_COMMUNE select COM_ID, 17 from COM_COMMUNE where COM_NOM = 'Chavannes-le-Veyron'
insert into LIC_LIEU_COMMUNE select COM_ID, 16 from COM_COMMUNE where COM_NOM = 'Bourg-en-Lavaux'
insert into LIC_LIEU_COMMUNE select COM_ID, 10 from COM_COMMUNE where COM_NOM = 'Bourg-en-Lavaux'
insert into LIC_LIEU_COMMUNE select COM_ID, 9 from COM_COMMUNE where COM_NOM = 'Bourg-en-Lavaux'
delete from LIC_LIEU_COMMUNE where LIE_ID = 29 AND COM_ID = 183 AND COM_ID NOT IN (SELECT COM_ID FROM ACQ_ACQUIT)
delete from LIC_LIEU_COMMUNE where LIE_ID = 28 AND COM_ID = 237 AND COM_ID NOT IN (SELECT COM_ID FROM ACQ_ACQUIT)
insert into LIC_LIEU_COMMUNE values (255, 28)
insert into LIC_LIEU_COMMUNE values (290, 29)