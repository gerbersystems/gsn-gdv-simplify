﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class WinBizExercice
    {
        private DateTime _dateDebut;
        private DateTime _dateFin;
        private int _annee;

        public DateTime DateDebut
        {
            get { return _dateDebut; }
        }

        public DateTime DateFin
        {
            get { return _dateFin; }
        }

        public int Annee
        {
            get { return _annee; }
        }

        public WinBizExercice(int annee, DateTime dateDebut, DateTime dateFin)
        {
            _annee = annee;
            _dateDebut = dateDebut;
            _dateFin = dateFin;
        }

        public bool DateIsInExercice(DateTime date)
        {
            return date >= DateDebut && date <= DateFin;
        }

        public override string ToString()
        {
            return _annee.ToString();
        }
    }
}