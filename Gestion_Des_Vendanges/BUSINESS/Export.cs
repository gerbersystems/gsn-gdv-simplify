﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows;

namespace Gestion_Des_Vendanges.BUSINESS
{
    public static class Export
    {
        private static string _acquitsQuery
        {
            get
            {
                return
                    $@"SELECT ACQ.ACQ_ID,
                               ACQ.ANN_AN,
                               ACQ.ACQ_NUMERO,
                               ACQ.ACQ_NUMERO_COMPL,
                               ACQ.ACQ_SURFACE,
                               ACQ.ACQ_LITRES,
                               ISNULL(PES.ACQUIT_KILO_LIVRE, 0) ACQUI_KILO_LIVRE,
                               ACQ.REG_NAME,
                               ACQ.CLA_NOM,
                               ACQ.COU_NOM,
                               ACQ.COM_NUMERO,
                               ACQ.COM_NOM,
                               ACQ.ACQ_RECEPTION,
                               CASE
                                   WHEN ACQ.ACQ_RECEPTION IS NULL
                                   THEN 'non'
                                   ELSE 'oui'
                               END ACQUIT_RECU,
                               ACQ.PROPRIETAIRE_ID,
                               ACQ.PROPRIETAIRE,
                               ACQ.PROPRIETAIRE_ERP_ID,
                               ACQ.PRODUCTEUR_ID,
                               ACQ.PRODUCTEUR,
                               ACQ.PRODUCTEUR_TVA,
                               ACQ.PRODUCTEUR_ERP_ID,
                               PAR.PAR_ID,
                               PAR.PAR_NUMERO,
                               PAR.CEP_NUMERO,
                               PAR.CEP_NOM,
                               PAR.PAR_SURFACE_CEPAGE,
                               PAR.PAR_QUOTA,
                               PAR.PAR_LITRES,
                               PAR.PAR_SURFACE_COMPTEE,
                               PAR.PAR_SECTEUR_VISITE,
                               PAR.PAR_MODE_CULTURE,
                               PAR.PAR_KILOS,
                               PAR.LIE_NUMERO,
                               PAR.LIE_NOM,
                               PAR.AUT_NOM
                        FROM
                        (
                            SELECT ACQ.ACQ_ID,
                                   ACQ.ANN_ID,
                                   ANN.ANN_AN,
                                   ACQ.ACQ_NUMERO,
                                   ACQ.ACQ_NUMERO_COMPL,
                                   ACQ.ACQ_SURFACE,
                                   ACQ.ACQ_LITRES,
                                   ACQ.ACQ_RECEPTION,
                                   REG.REG_NAME,
                                   CLA.CLA_NOM,
                                   COU.COU_NOM,
                                   COM.COM_NUMERO,
                                   COM.COM_NOM,
                                   PROPRIETAIRE.PRO_ID PROPRIETAIRE_ID,
                                   ISNULL(PROPRIETAIRE.PRO_SCTE+' ', '')+ISNULL(PROPRIETAIRE.PRO_NOM+' ', '')+ISNULL(PROPRIETAIRE.PRO_PRENOM, '') AS PROPRIETAIRE,
                                   PROPRIETAIRE.ADR_ID_WINBIZ PROPRIETAIRE_ERP_ID,
                                   PRODUCTEUR.PRO_ID PRODUCTEUR_ID,
                                   ISNULL(PRODUCTEUR.PRO_SCTE+' ', '')+ISNULL(PRODUCTEUR.PRO_NOM+' ', '')+ISNULL(PRODUCTEUR.PRO_PRENOM, '') AS PRODUCTEUR,
                                   PRODUCTEUR.ADR_ID_WINBIZ PRODUCTEUR_ERP_ID,
                                   TVA.MOD_NOM PRODUCTEUR_TVA
                            FROM ACQ_ACQUIT ACQ,
                                 ANN_ANNEE ANN,
                                 COU_COULEUR COU,
                                 REG_REGION REG,
                                 CLA_CLASSE CLA,
                                 COM_COMMUNE COM,
                                 PRO_PROPRIETAIRE PROPRIETAIRE,
                                 PRO_PROPRIETAIRE PRODUCTEUR
                                 LEFT JOIN MOD_MODE_TVA TVA ON TVA.MOD_ID = PRODUCTEUR.MOD_ID
                            WHERE ACQ.ANN_ID = ANN.ANN_ID
                                  AND ACQ.COU_ID = COU.COU_ID
                                  AND ACQ.REG_ID = REG.REG_ID
                                  AND ACQ.CLA_ID = CLA.CLA_ID
                                  AND ACQ.COM_ID = COM.COM_ID
                                  AND ACQ.PRO_ID = PROPRIETAIRE.PRO_ID
                                  AND ACQ.PRO_ID_VENDANGE = PRODUCTEUR.PRO_ID
                        ) AS ACQ
                        LEFT JOIN
                        (
                            SELECT PAR.PAR_ID,
                                   CEP.CEP_NUMERO,
                                   CEP.CEP_NOM,
                                   PAR.PAR_NUMERO,
                                   PAR.PAR_SURFACE_CEPAGE,
                                   PAR.PAR_QUOTA,
                                   PAR.PAR_LITRES,
                                   PAR.PAR_SURFACE_COMPTEE,
                                   PAR.PAR_SECTEUR_VISITE,
                                   PAR.PAR_MODE_CULTURE,
                                   PAR.PAR_KILOS,
                                   LIE.LIE_NUMERO,
                                   LIE.LIE_NOM,
                                   AUT.AUT_NOM,
                                   PAR.ACQ_ID
                            FROM PAR_PARCELLE PAR,
                                 CEP_CEPAGE CEP,
                                 LIE_LIEU_DE_PRODUCTION LIE,
                                 AUT_AUTRE_MENTION AUT
                            WHERE PAR.CEP_ID = CEP.CEP_ID
                                  AND PAR.LIE_ID = LIE.LIE_ID
                                  AND PAR.AUT_ID = AUT.AUT_ID
                        ) AS PAR ON ACQ.ACQ_ID = PAR.ACQ_ID
                        LEFT OUTER JOIN
                        (
                            SELECT PEE.ACQ_ID,
                                   SUM(PED.PED_QUANTITE_KG) ACQUIT_KILO_LIVRE
                            FROM PED_PESEE_DETAIL PED,
                                 PEE_PESEE_ENTETE PEE
                            WHERE PED.PEE_ID = PEE.PEE_ID
                            GROUP BY PEE.ACQ_ID
                        ) AS PES ON ACQ.ACQ_ID = PES.ACQ_ID
                        WHERE ACQ.ANN_ID = {Utilities.IdAnneeCourante}
                        ORDER BY ACQ.ACQ_NUMERO";
            }
        }

        private static string _peseesQuery
        {
            get
            {
                return
                    $@"SELECT PED.PED_LOT,
                           PED.PED_QUANTITE_KG,
                           PED.PED_QUANTITE_LITRES,
                           PED.PED_SONDAGE_BRIKS,
                           PED.PED_SONDAGE_OE,
                           PEE.ACQ_ID,
                           PEE.PEE_DATE,
                           PEE.PEE_DIVERS1,
                           PEE.PEE_DIVERS2,
                           PEE.PEE_DIVERS3,
                           REPLACE(PEE.PEE_REMARQUES, CHAR(13) + CHAR(10), ' '),
                           PEE.PEE_GRANDCRU,
                           PEE.PEE_LIEU LIEU_LIVRAISON,
                           CEP.CEP_NUMERO,
                           CEP.CEP_NOM,
                           AUT.AUT_NOM MENTION,
                           LIE.LIE_NOM,
                           ISNULL(RES.RES_NOM+' ', '')+ISNULL(RES.RES_PRENOM, '') RESPONSABLE,
                           ACQ.ACQ_RECEPTION,
                           CASE
                               WHEN ACQ.ACQ_RECEPTION IS NULL
                               THEN 'non'
                               ELSE 'oui'
                           END ACQUIT_RECU,
                           CLA.CLA_NOM ACQ_CLASSE,
                           ACQ.ACQ_LITRES,
                           COM.COM_NUMERO,
                           COM.COM_NOM,
                           PROPRIETAIRE.PRO_ID PROPRIETAIRE_ID,
                           ISNULL(PROPRIETAIRE.PRO_SCTE+' ', '')+ISNULL(PROPRIETAIRE.PRO_NOM+' ', '')+ISNULL(PROPRIETAIRE.PRO_PRENOM, '') AS PROPRIETAIRE,
                           PROPRIETAIRE.ADR_ID_WINBIZ PROPRIETAIRE_ERP_ID,
                           PRODUCTEUR.PRO_ID PRODUCTEUR_ID,
                           ISNULL(PRODUCTEUR.PRO_SCTE+' ', '')+ISNULL(PRODUCTEUR.PRO_NOM+' ', '')+ISNULL(PRODUCTEUR.PRO_PRENOM, '') AS PRODUCTEUR,
                           PRODUCTEUR.ADR_ID_WINBIZ PRODUCTEUR_ERP_ID,
                           PEE.PEE_ID NO_QUITTANCE
                    FROM PED_PESEE_DETAIL PED,
                         PEE_PESEE_ENTETE PEE,
                         CEP_CEPAGE CEP,
                         RES_RESPONSABLE RES,
                         ACQ_ACQUIT ACQ,
                         ANN_ANNEE ANN,
                         AUT_AUTRE_MENTION AUT,
                         LIE_LIEU_DE_PRODUCTION LIE,
                         CLA_CLASSE CLA,
                         COM_COMMUNE COM,
                         PRO_PROPRIETAIRE PROPRIETAIRE,
                         PRO_PROPRIETAIRE PRODUCTEUR
                    WHERE PED.PEE_ID = PEE.PEE_ID
                          AND PED.CEP_ID = CEP.CEP_ID
                          AND PEE.ACQ_ID = ACQ.ACQ_ID
                          AND ACQ.ANN_ID = ANN.ANN_ID
                          AND ACQ.CLA_ID = CLA.CLA_ID
                          AND PEE.AUT_ID = AUT.AUT_ID
                          AND PEE.LIE_ID = LIE.LIE_ID
                          AND PEE.RES_ID = RES.RES_ID
                          AND ACQ.COM_ID = COM.COM_ID
                          AND ACQ.PRO_ID = PROPRIETAIRE.PRO_ID
                          AND ACQ.PRO_ID_VENDANGE = PRODUCTEUR.PRO_ID
                          AND ACQ.ANN_ID = {Utilities.IdAnneeCourante}";
            }
        }

        private static string _fileName = "export";

        internal static DataSet Acquits
        {
            get
            {
                _fileName = "acquits";
                return GetDataSet(ConnectionManager.Instance.GVConnectionString, _acquitsQuery);
            }
        }

        internal static DataSet Pesees
        {
            get
            {
                _fileName = "pesees";
                return GetDataSet(ConnectionManager.Instance.GVConnectionString, _peseesQuery);
            }
        }

        public static DataSet GetDataSet(string ConnectionString, string SQL)
        {
            var conn = new SqlConnection(ConnectionString);
            var da = new SqlDataAdapter();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            da.SelectCommand = cmd;
            var ds = new DataSet();

            conn.Open();
            da.Fill(ds);
            conn.Close();

            return ds;
        }

        internal static bool ToCSV(this DataSet dataSet)
        {
            var _listSeparator = CultureInfo.CurrentCulture.TextInfo.ListSeparator;

            foreach (DataTable dataTable in dataSet.Tables)
            {
                StringBuilder csv = new StringBuilder();

                foreach (DataColumn column in dataTable.Columns)
                {
                    csv.Append($"{column.ColumnName.ToString()}{_listSeparator}");
                }
                csv.Replace(_listSeparator, Environment.NewLine, csv.Length - 1, 1);

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    foreach (object field in dataRow.ItemArray)
                    {
                        csv.Append($"{field.ToString()}{_listSeparator}");
                    }
                    csv.Replace(_listSeparator, Environment.NewLine, csv.Length - 1, 1);
                }

                try
                {
                    var filePath = Helpers.FileNameHelper.GetUniqueFullName($"{Environment.GetFolderPath(Environment.SpecialFolder.Desktop)}\\{_fileName}_{DateTime.Now.ToString("yyyyMMddHHmmss")}.csv");
                    File.WriteAllText(filePath, csv.ToString(), UTF8Encoding.UTF8);
                    MessageBox.Show($"Fichier d'exportation créé.{Environment.NewLine}" +
                                    $"[ {filePath} ]",
                                    "Exportation",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Write Error when exporting to CSV.");
                }
            }

            return true;
        }
    }
}