﻿namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class ImportExportLigneManuelleStandard : ImportExportLigneManuelleBase
    {
        private const short NUMBER_OF_COLUMNS = 4;
        private const byte HEADER_ROW = 1;

        protected override int AmountOfRows
        {
            get
            {
                if (_amountOfRows > -1) return _amountOfRows;
                _amountOfRows = _producteurIdList.GetLength(0) + HEADER_ROW;

                return _amountOfRows;
            }
            set { _amountOfRows = value; }
        }

        public ImportExportLigneManuelleStandard(string[,] producteurIdList, string fichier) :
            base(producteurIdList, fichier, NUMBER_OF_COLUMNS)
        {
        }

        protected override void AddHeaderProducteurListCsv()
        {
            _producteurListCsv[0, 0] = "Id";
            _producteurListCsv[0, 1] = "ProducteurNom";
            _producteurListCsv[0, 2] = "Description ligne manuelle";
            _producteurListCsv[0, 3] = "Montant ligne manuelle";
        }

        protected override void AddRowsProducteurListCsv()
        {
            for (int i = 0; i < _producteurIdList.GetLength(0); i++)
            {
                _producteurListCsv[i + HEADER_ROW, 0] = _producteurIdList[i, 0].ToString().Trim();
                _producteurListCsv[i + HEADER_ROW, 1] = _producteurIdList[i, 1].ToString().Trim();
                _producteurListCsv[i + HEADER_ROW, 2] = string.Empty;
                _producteurListCsv[i + HEADER_ROW, 3] = string.Empty;
            }
        }
    }
}