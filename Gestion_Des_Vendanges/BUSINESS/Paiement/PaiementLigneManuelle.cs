﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Données d'un ligne de paiement manuelle
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class PaiementLigneManuelle
    {
        #region Fields

        private string _texte;
        private int _proId;
        private bool _montantIsFixe;
        private decimal _valeur;
        private int _mocId;
        private PRO_NOMCOMPLETTableAdapter _proTable;

        #endregion Fields

        #region Properties

        public string Texte
        {
            get { return _texte; }
            set { _texte = value; }
        }

        public int ProId
        {
            get { return _proId; }
            set { _proId = value; }
        }

        public string Producteur => (_proId == -1) ? "Tous" : Convert.ToString(_proTable.GetNomCompletByProId(_proId));

        public bool MontantIsFixe
        {
            get { return _montantIsFixe; }
            set { _montantIsFixe = value; }
        }

        public decimal Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }

        public bool IsSelectAllProprietaire
        {
            get { return (_proId == -1); }
            set
            {
                if (value)
                    _proId = -1;
                else if (_proId == -1)
                    _proId = 0;
            }
        }

        public int MocId
        {
            get { return _mocId; }
            set { _mocId = value; }
        }

        #endregion Properties

        #region Constructors

        public PaiementLigneManuelle(string texte, bool montantIsFixe, decimal valeur)
        {
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_NOMCOMPLETTableAdapter>();
            _texte = texte;
            _montantIsFixe = montantIsFixe;
            _valeur = valeur;
            _proId = -1;
        }

        public PaiementLigneManuelle(string texte, bool montantIsFixe, decimal valeur, string producteurNomComplet)
        {
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_NOMCOMPLETTableAdapter>();
            _texte = texte;
            _montantIsFixe = montantIsFixe;
            _valeur = valeur;
            _proId = (int)_proTable.GetProIdByNomComplet(producteurNomComplet);
        }

        public PaiementLigneManuelle(string texte, bool montantIsFixe, decimal valeur, int idProducteur)
        {
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_NOMCOMPLETTableAdapter>();
            _texte = texte;
            _montantIsFixe = montantIsFixe;
            _valeur = valeur;
            _proId = idProducteur;
        }

        #endregion Constructors
    }
}