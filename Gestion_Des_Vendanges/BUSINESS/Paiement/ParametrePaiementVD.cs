﻿using Gestion_Des_Vendanges.DAO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    partial class ParametrePaiement
    {
        public void SetModePaiementCepage(int lieId,
                                          decimal pourcentage,
                                          bool isLitreMoutRaisinChecked,
                                          bool isGestionBonusActived,
                                          bool isZoneDifficulteChecked)
        {
            CheckTablesDePrixLie(lieId);
            _modePaiement = new ModePaiementParCepage(lieId, isGestionBonusActived, isLitreMoutRaisinChecked, isZoneDifficulteChecked, this);
            _pourcentage = pourcentage;
            _isLitreMoutRaisin = isLitreMoutRaisinChecked;
            _zoneDifficulteChecked = isZoneDifficulteChecked;
        }

        private void CheckTablesDePrixLie(int lieId)
        {
            foreach (int proId in _proIds)
            {
                if ((int)_valTable.GetValManquantLie(proId, _annId, lieId) != 0)
                {
                    int cepId = GetFirstCepManquant(lieId, proId);

                    throw new TablePrixNotFoundGVException(lieId, cepId, _annId);
                }
            }
        }

        private void TablesDePrixCepLieExist()
        {
            foreach (int proId in _proIds)
            {
                if ((int)_valTable.GetValManquant(proId, _annId) > 0)
                {
                    GetFirstLieCepManquant(out int cepId, out int lieId, proId);

                    throw new TablePrixNotFoundGVException(lieId, cepId, _annId);
                }
            }
        }

        private int GetFirstCepManquant(int lieId, int proId)
        {
            bool trouve = false;
            int cepId = -1;
            _vabTable.Fill(new DSPesees().VAB_VALEUR_CEPAGE_BESOIN, proId, _annId);
            DSPesees.VAB_VALEUR_CEPAGE_BESOINDataTable vabDataTable = _vabTable.GetData(proId, _annId);
            for (int i = 0; i < vabDataTable.Count && !trouve; i++)
            {
                int nb = (int)_valTable.GetNbByCepLieAnn(vabDataTable[i].CEP_ID, lieId, _annId);
                if (nb == 0)
                {
                    trouve = true;
                    cepId = vabDataTable[i].CEP_ID;
                }
            }
            return cepId;
        }

        private void GetFirstLieCepManquant(out int cepId, out int lieId, int proId)
        {
            bool trouve = false;
            cepId = -1;
            lieId = -1;
            ConnectionManager.Instance.AddGvTableAdapter(_valTable);
            _vabTable.Fill(new DSPesees().VAB_VALEUR_CEPAGE_BESOIN, proId, _annId);
            DSPesees.VAB_VALEUR_CEPAGE_BESOINDataTable vabDataTable = _vabTable.GetData(proId, _annId);
            for (int i = 0; i < vabDataTable.Count && !trouve; i++)
            {
                int nb = (int)_valTable.GetNbByCepLieAnn(vabDataTable[i].CEP_ID, vabDataTable[i].LIE_ID, _annId);
                if (nb == 0)
                {
                    trouve = true;
                    lieId = vabDataTable[i].LIE_ID;
                    cepId = vabDataTable[i].CEP_ID;
                }
            }
        }
    }
}