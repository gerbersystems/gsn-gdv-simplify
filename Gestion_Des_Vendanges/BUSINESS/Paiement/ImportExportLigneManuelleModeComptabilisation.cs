﻿using System.IO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class ImportExportLigneManuelleModeComptabilisation : ImportExportLigneManuelleBase
    {
        private const short NUMBER_OF_COLUMNS = 6;
        private const byte HEADER_ROW = 1;

        public ImportExportLigneManuelleModeComptabilisation(string[,] producteurIdList, FileInfo fichier)
            : base(producteurIdList, fichier, NUMBER_OF_COLUMNS)
        {
        }

        public ImportExportLigneManuelleModeComptabilisation(string[,] producteurIdList, string fichier)
            : base(producteurIdList, fichier, NUMBER_OF_COLUMNS)
        {
        }

        protected override int AmountOfRows
        {
            get
            {
                if (_amountOfRows > -1) return _amountOfRows;
                _amountOfRows = _producteurIdList.GetLength(0) + HEADER_ROW;

                return _amountOfRows;
            }
            set { _amountOfRows = value; }
        }

        protected override void AddHeaderProducteurListCsv()
        {
            _producteurListCsv[0, 0] = "ID";
            _producteurListCsv[0, 1] = "ERP_ID";
            _producteurListCsv[0, 2] = "NOM_PRODUCTEUR";
            _producteurListCsv[0, 3] = "DESCRIPTION";
            _producteurListCsv[0, 4] = "MONTANT";
            _producteurListCsv[0, 5] = "MODE_DE_COMPTABILISATION_ID";
        }

        protected override void AddRowsProducteurListCsv()
        {
            for (int i = 0; i < _producteurIdList.GetLength(0); i++)
            {
                _producteurListCsv[i + HEADER_ROW, 0] = _producteurIdList[i, 0].ToString().Trim();
                _producteurListCsv[i + HEADER_ROW, 1] = _producteurIdList[i, 2].ToString().Trim();
                _producteurListCsv[i + HEADER_ROW, 2] = _producteurIdList[i, 1].ToString().Trim();
                _producteurListCsv[i + HEADER_ROW, 3] = string.Empty;
                _producteurListCsv[i + HEADER_ROW, 4] = string.Empty;
                _producteurListCsv[i + HEADER_ROW, 5] = string.Empty;
            }
        }
    }
}