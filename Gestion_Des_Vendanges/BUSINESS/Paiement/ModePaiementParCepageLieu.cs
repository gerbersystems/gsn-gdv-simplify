﻿using System;
using System.Windows.Forms;
using static Gestion_Des_Vendanges.DAO.DSPesees;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Paramètres de calcul d'un paiement par cépage et lieu de production
    *  Date de création:
    *  Modifications:
    * */

    internal class ModePaiementParCepageLieu : ModePaiement
    {
        #region Constructors

        internal ModePaiementParCepageLieu(bool isGestionBonusActived,
                                           bool isLitreMoutRaisin,
                                           bool isZoneDifficulteChecked,
                                           ParametrePaiement parametre)
          : base(isGestionBonusActived, isLitreMoutRaisin, isZoneDifficulteChecked, parametre) { }

        #endregion Constructors

        #region Methods

        #region Pesees

        internal override LignePesee GetLignePesees(PED_PESEE_DETAILRow peseeDetailrow,
                                                    CoupleProPaiIds coupleProPaiId,
                                                    double overload)
        {
            _producerId = coupleProPaiId.ProId;
            _classeId = PeeTable.GetClassIdByPeeId(peseeDetailrow.PEE_ID) ?? DefaultMocId;

            var lignePesee = new LignePesee()
            {
                PaiementId = coupleProPaiId.PaiId,
                PeseeDetailId = peseeDetailrow.PED_ID,
                AnneeModeComptabilisationId = Convert.ToInt32(CepTable.GetMoc(_parametre.AnnId, peseeDetailrow.CEP_ID)),
                Kilo = (overload > 0) ? overload : peseeDetailrow.PED_QUANTITE_KG,
                PrixKilo = (overload == 0) ? 0 : CalculPrixLignePesee(pedRow: peseeDetailrow),
                TypeLignePesee = _parametre.ModePaiement.IsLitreMoutRaisin ? TypeLignePesee.MoutRaisin : TypeLignePesee.Pesee,
                Pourcentage = (int)_parametre.Pourcentage
            };

            lignePesee.TauxTVA = IsProducerAssujetis ?
                                 Convert.ToDouble(MocTable.GetTauxTva(lignePesee.AnneeModeComptabilisationId)) :
                                 0;
            lignePesee.ModeTvaId = MocTable.GetModId(lignePesee.AnneeModeComptabilisationId) ?? (int)ModeTVA.Sans;

            double pedQuantite = (overload > 0) ? overload : GetPedQuantite(peseeDetailrow);
            double montant = Math.Round(lignePesee.PrixKilo * pedQuantite, 2);

            CalculMontantTVA(lignePesee, montant);

            ArrondisCinqCentimes(lignePesee.MontantHT);
            ArrondisCinqCentimes(lignePesee.MontantTTC);

            return lignePesee;
        }

        internal override double CalculPrixLignePesee(PED_PESEE_DETAILRow pedRow)
        {
            double prix = GetPrixLignePesee(pedRow);

            return Math.Round((prix * Convert.ToDouble(_parametre.Pourcentage)) / 100, 3);
        }

        private double GetPrixLignePesee(PED_PESEE_DETAILRow pedRow)
        {
            if (_parametre == null)
                throw new ArgumentNullException("Parameter in ModePaiementParCepageLieu is null");

            int lieId = Convert.ToInt32(PeeTable.GetLieIdByPedId(pedRow.PED_ID));

            try
            {
                return (double)ValTable.GetPrixByCepLieAnnClasse(pedRow.CEP_ID, _parametre.AnnId, lieId, _classeId, _producerId);
            }
            catch (Exception)
            {
                // TODO Test if this prevent older customers with classId column is null
                try
                {
                    return (double)ValTable.GetPrixByCepLieAnn(pedRow.CEP_ID, _parametre.AnnId, lieId);
                }
                catch (Exception)
                {
                    MessageBox.Show("La table de prix bonus/malus n'a pas été trouvée.\n" +
                                $"AnneeId: {_parametre.AnnId} - CépageId: {pedRow.CEP_ID} - LieuProductionId: {lieId} - ClasseId {_classeId}",
                                "Erreur lecture bonus", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
            }
        }

        #endregion Pesees

        #region Bonus/Malus

        internal override LignePesee GetLigneBonus(LignePesee lignePesee,
                                                   PED_PESEE_DETAILRow peseeDetailrow,
                                                   double overload)
        {
            lignePesee.Pourcentage = 100; // For now only implemented with payment of 100% bonus per pai_id
            lignePesee.TypeLignePesee = TypeLignePesee.Bonus;

            // TODO Change switch: must be based on VAL_BON_TYPE value in VAL_VALEUR_CEPAGE
            //      Instead of GlobalParam Bonus unite
            switch (GlobalParam.Instance.BonusUnite)
            {
                case BonusUniteEnum.VariableChf:
                    lignePesee.PrixKilo = overload == 0 ? 0 : GetMontantBonus(peseeDetailrow);
                    break;

                case BonusUniteEnum.VariablePourcent:
                    lignePesee.PrixKilo = overload == 0 ? 0 : GetMontantBonus(peseeDetailrow);
                    lignePesee.PrixKilo = lignePesee.PrixKilo * GetPrixLignePesee(peseeDetailrow) / 100;
                    break;
            }

            var montant = GetBonus(lignePesee.PrixKilo, peseeDetailrow, overload);

            CalculMontantTVA(lignePesee, montant);

            ArrondisCinqCentimes(lignePesee.MontantHT);
            ArrondisCinqCentimes(lignePesee.MontantTTC);

            return lignePesee;
        }

        #endregion Bonus/Malus

        #endregion Methods
    }
}