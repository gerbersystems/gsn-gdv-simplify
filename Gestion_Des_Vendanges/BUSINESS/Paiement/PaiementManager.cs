﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using Gestion_Des_Vendanges.GUI;
using Gestion_Des_Vendanges.Helpers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Calcul et génération des paiements à partir de données d'un objet "ParametrePaiement"
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    *  ATH - 20.11.2009 - Insertion de la TVA dans les paiements
    * */

    partial class PaiementManager
    {
        #region Fields

        private ParametrePaiement _parametre;
        private PAI_PAIEMENTTableAdapter _paiTable;
        private PRO_PROPRIETAIRETableAdapter _proTable;
        private CEP_CEPAGETableAdapter _cepTable;
        private MOC_MODE_COMPTABILISATIONTableAdapter _mocTable;
        private ANN_ANNEETableAdapter _annTable;
        private List<CoupleProPaiIds> _coupleProPaiIds;
        private int _etape;

        #endregion Fields

        #region Constructors

        public PaiementManager()
        {
            _paiTable = ConnectionManager.Instance.CreateGvTableAdapter<PAI_PAIEMENTTableAdapter>();
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
            _cepTable = ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();
            _mocTable = ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
            _annTable = ConnectionManager.Instance.CreateGvTableAdapter<ANN_ANNEETableAdapter>();
            _coupleProPaiIds = new List<CoupleProPaiIds>();

            LoadSpecificTableAdapter();
        }

        #endregion Constructors

        #region Methods

        public void AjoutPaiement()
        {
            _parametre = new ParametrePaiement();
            int? mocId = _annTable.GetMocId(Utilities.IdAnneeCourante);

            if (mocId == null || mocId <= 0) throw new ModeComptabilisationNotSetGVException();

            CreatePaymentWizard();
        }

        private void CreatePaymentWizard()
        {
            _etape = 0;
            while (_etape >= 0 && _etape < 6)
            {
                switch (_etape)
                {
                    case 0:
                        var frm = WfrmAjoutPaiement.GetInstance(_parametre);

                        if (frm != null)
                            CheckEtape(frm.ShowDialog());
                        else
                            _etape = -1;

                        break;

                    case 1:
                        CheckEtape(new WfrmAjoutPaiementSelectProducteur(_parametre).ShowDialog());
                        break;

                    case 2:
                        CheckEtape(new WfrmAjoutPaiementMode(_parametre).ShowDialog());
                        break;

                    case 3:
                        CheckEtape(new WfrmAjoutPaiementLigneManuelle(_parametre).ShowDialog());
                        break;

                    case 4:
                        GenererPaiement();
                        _etape++;
                        break;

                    default:
                        _etape = -1;
                        break;
                }
            }
        }

        private void CheckEtape(DialogResult result)
        {
            if (result == DialogResult.OK)
                _etape++;
            else if (result == DialogResult.Retry)
                _etape--;
            else
                _etape = -1;
        }

        private void GenererPaiement()
        {
            try
            {
                CreatePaiements();
                CreateLignePesees();
                CreateLigneManuelles();
                UpdateMontantPaiements();
            }
            catch
            {
                MessageBox.Show("Erreur lors de la génération du paiement",
                                "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CreatePaiements()
        {
            // TODO | #paiements !NOT IMPLEMENTED >_isBonus lors de la création d'un paiements !indiquer si le bonus est inclus!
            bool _isBonus = false;

            foreach (int proId in _parametre.ProIds)
            {
                int paiId;
                try
                {
                    paiId = Convert.ToInt32(_paiTable.GetNextPaiId());
                }
                catch
                {
                    throw new ArgumentNullException(nameof(paiId), @"Impossible de récupérer le prochain index de la table de paiement. L'utilisateur doit être propriétaire de la base de données.");
                }

                _paiTable.Insert(PAI_TEXTE: _parametre.Texte,
                                 PAI_DATE: _parametre.Date,
                                 ANN_ID: _parametre.AnnId,
                                 PAI_NUMERO: _parametre.Code + "-" + paiId,
                                 PRO_ID: proId,
                                 PAI_ACOMPTE: _parametre.IsAcompte,
                                 PAI_ID_WINBIZ: null,
                                 PAI_POURCENTAGE: Convert.ToDouble(_parametre.Pourcentage),
                                 PAI_MONTANT_LIM_NET: 0,
                                 PAI_MONTANT_LIM_TTC: 0,
                                 PAI_MONTANT_LIP_NET: 0,
                                 PAI_MONTANT_LIP_TTC: 0,
                                 PAI_SOLDE: 0,
                                 PAI_BONUS_PAYE: _isBonus);

                paiId = (int)_paiTable.GetMaxPaiId();

                _coupleProPaiIds.Add(new CoupleProPaiIds(proId, paiId));
            }
        }

        private void UpdateMontantPaiements()
        {
            var bdvendangeControleur = new BDVendangeControleur();

            foreach (CoupleProPaiIds proPaiId in _coupleProPaiIds)
                bdvendangeControleur.UpdateMontantPaiement(proPaiId.PaiId);
        }

        private double CalculMontantTVA(int modeTVA,
                                        double tauxTVA,
                                        double montant,
                                        out double montantTTC,
                                        out double montantHT)
        {
            switch (modeTVA)
            {
                case 1: // Sans TVA
                    montantHT = montant;
                    montantTTC = montant;
                    break;

                case 2: //TVA Incluse
                    montantTTC = montant;
                    montantHT = Math.Round(montant / (1 + tauxTVA / 100), 2);
                    break;

                case 3: // TVA Exclue
                    montantHT = montant;
                    montantTTC = Math.Round(montant * (1 + tauxTVA / 100), 2);
                    break;

                default:
                    throw new Exception("Le mode de TVA est incorrect");
            }
            return montantTTC - montantHT;
        }

        // INFO | #paiements - 3. createLigneManuelles()
        private void CreateLigneManuelles()
        {
            double montant, montantTTC, montantHT;

            var limTable = ConnectionManager.Instance
                                            .CreateGvTableAdapter<LIM_LIGNE_PAIEMENT_MANUELLETableAdapter>();

            foreach (PaiementLigneManuelle ligneManuelle in _parametre.LigneManuelles)
            {
                if (ligneManuelle == null) continue;

                foreach (var coupleProPaiId in _coupleProPaiIds)
                {
                    if (ligneManuelle.ProId == coupleProPaiId.ProId || ligneManuelle.ProId == -1)
                    {
                        int modId = GetModId(coupleProPaiId.ProId, ligneManuelle.MocId);
                        double tauxTva = Convert.ToDouble(_mocTable.GetTauxTva(ligneManuelle.MocId));
                        montant = CalculPrixLigneManuelle(coupleProPaiId.PaiId, ligneManuelle);

                        if (CalculMontantTVA(modId, tauxTva, montant, out montantTTC, out montantHT) == 0)
                            tauxTva = 0;

                        limTable.Insert(ligneManuelle.Texte.Truncate(50), coupleProPaiId.PaiId, montantHT, montantTTC, ligneManuelle.MocId, modId, tauxTva);
                    }
                }
            }

            if (!_parametre.IsAcompte) CreateFinalPayment(limTable);
        }

        private void CreateFinalPayment(LIM_LIGNE_PAIEMENT_MANUELLETableAdapter limTable)
        {
            foreach (var coupleProPaiId in _coupleProPaiIds)
            {
                limTable.FillLigneAcompte(new DSPesees.LIM_LIGNE_PAIEMENT_MANUELLEDataTable(),
                                          coupleProPaiId.PaiId);

                foreach (var lignePaiementManuelleRow in limTable.GetLigneAcompte(coupleProPaiId.PaiId))
                {
                    limTable.Insert(lignePaiementManuelleRow.LIM_TEXTE,
                                    coupleProPaiId.PaiId,
                                    lignePaiementManuelleRow.LIM_MONTANT_HT,
                                    lignePaiementManuelleRow.LIM_MONTANT_TTC,
                                    lignePaiementManuelleRow.MOC_ID,
                                    lignePaiementManuelleRow.MOD_ID,
                                    lignePaiementManuelleRow.LIM_TAUX_TVA);
                }
            }
        }

        private double CalculPrixLigneManuelle(int paiId, PaiementLigneManuelle ligneManuelle)
        {
            double prix;
            if (ligneManuelle.MontantIsFixe)
            {
                prix = Convert.ToDouble(ligneManuelle.Valeur);
            }
            else if (_parametre.ZoneTravailIncluded)
            {
                //TODO | calcul de la prime de la zone de travail en récupérant toutes les parcelles du (des) producteur(s)
                prix = 0;
            }
            else
            {
                var lipPaiement = ConnectionManager.Instance
                                                   .CreateGvTableAdapter<LIP_LIGNE_PAIEMENT_PESEETableAdapter>();

                double _totalPaiement = (_mocTable.GetModId(ligneManuelle.MocId) == 2) ?
                                        (double)lipPaiement.GetTotalPaiementTTC(paiId) :
                                        (double)lipPaiement.GetTotalPaiementHT(paiId);

                prix = (_totalPaiement * Convert.ToDouble(ligneManuelle.Valeur)) / 100;
            }

            return Math.Round(prix, 2);
        }

        private int GetModId(int proId, int mocId)
        {
            int? modId = _proTable.GetModId(proId);

            if (modId == null) modId = Convert.ToInt32(_mocTable.GetModId(mocId));

            return (int)modId;
        }

        #endregion Methods
    }

    /* *
    *  Description: Id d'un producteur et Id d'un paiement
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal struct CoupleProPaiIds
    {
        private int _proId;
        private int _paiId;

        public int ProId => _proId;

        public int PaiId => _paiId;

        public CoupleProPaiIds(int proId, int paiId)
        {
            _proId = proId;
            _paiId = paiId;
        }
    }
}