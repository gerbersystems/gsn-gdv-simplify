﻿using Gestion_Des_Vendanges.DAO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Parametres de calcul d'un paiement par cépage
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class ModePaiementParCepage : ModePaiement
    {
        private int _lieId;

        public int LieId
        {
            get => _lieId;
            set => _lieId = value;
        }

        public ModePaiementParCepage(int lieId,
                                     bool isGestionBonusActived,
                                     bool isLitreMoutRaisin,
                                     bool isZoneDifficulteChecked, ParametrePaiement parametre)
            : base(isGestionBonusActived, isLitreMoutRaisin, isZoneDifficulteChecked, parametre)
        {
            _lieId = lieId;
        }

        internal override LignePesee GetLignePesees(DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                    CoupleProPaiIds coupleProPaiId,
                                                    double overload)
        {
            throw new System.NotImplementedException();
        }

        internal override double CalculPrixLignePesee(DSPesees.PED_PESEE_DETAILRow pedRow)
        {
            throw new System.NotImplementedException();
        }

        internal override LignePesee GetLigneBonus(LignePesee lignePesee,
                                                   DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                   double overload)
        {
            throw new System.NotImplementedException();
        }
    }
}