﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Gère l'exportation des paiements dans WinBIZ
    *  Date de création: 03.11.2009
    *  Modifications:
    * */

    internal class PaiementExport
    {
        private WinBizControleur _winBIZControleur;
        private BDVendangeControleur _gvControleur;

        public PaiementExport()
        {
            _gvControleur = new BDVendangeControleur();
        }

        public bool Execute()
        {
            bool withoutError = true;
            var proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
            _winBIZControleur = new WinBizControleur(Utilities.IdAnneeCourante);
            List<WinBizExercice> exercices = _winBIZControleur.GetExercices();
            List<FactureWinBIZ> allPaiements = _gvControleur.GetPaiements();

            foreach (WinBizExercice exercice in exercices)
            {
                try
                {
                    // Filtre chaque ligne de paiement selon sa date par rapport à l’année d'exercice Winbiz
                    List<FactureWinBIZ> paiements = FiltreExercice(allPaiements, exercice);

                    if (paiements.Count > 0)
                    {
                        _winBIZControleur = new WinBizControleur(exercice.Annee, false);
                        //Récupération de tous les producteurs
                        List<Adresse> adresses = _gvControleur.GetProducteurs();
                        //Ajout des adresses manquantes dans Winbiz
                        CoupleIdsGvWinBIZ[] ids = _winBIZControleur.InsertAdresses(adresses);
                        //Ajout des Ids WinBIZ dans GV
                        _gvControleur.InsertWinBizAdrIds(ids);
                        //Mise à jour des IDs Winbiz dans la liste des paiements
                        foreach (var paiement in paiements)
                        {
                            paiement.AdrIdWinBIZ = Convert.ToDecimal(proTable.GetAdrIdWinBIZ(paiement.ProId));
                        }
                        //Création des factures
                        _winBIZControleur.CreateFactures(paiements);
                        //Ajout des Ids WinBIZ dans GV
                        _gvControleur.InsertWinBizPaiIds(paiements);
                    }
                }
                catch (DirectoryNotFoundException)
                {
                    withoutError = false;
                    MessageBox.Show($"Erreur, le dossier WinBIZ de l'année: {exercice.Annee} est introuvable.\n" +
                                    "Les paiements correspondants ne seront pas sauvegardés dans WinBIZ.",
                                    "Erreur, dossier introuvable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            return withoutError;
        }

        private List<FactureWinBIZ> FiltreExercice(List<FactureWinBIZ> paiements, WinBizExercice exercice)
        {
            List<FactureWinBIZ> resultat = new List<FactureWinBIZ>();

            for (int i = 0; i < paiements.Count; i++)
            {
                if (exercice.DateIsInExercice(paiements[i].DateDocument)) resultat.Add(paiements[i]);
            }

            return resultat;
        }
    }

    internal struct CoupleIdsGvWinBIZ
    {
        private int _proId;
        private int _adrIdWinBiz;

        public int ProId => _proId;

        public int AdrIdWinBiz => _adrIdWinBiz;

        public CoupleIdsGvWinBIZ(int proId, int adrIdWinBiz)
        {
            _proId = proId;
            _adrIdWinBiz = adrIdWinBiz;
        }
    }
}