﻿using Gestion_Des_Vendanges.DAO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Paramètres de calcul d'un paiement par cépage, lieu de production et table de prix CIVCV
    *  Date de création: 05-12-14
    *  Modifications:
    * */

    internal class ModePaiementParCepageLieuTablePrix : ModePaiement
    {
        public ModePaiementParCepageLieuTablePrix(bool isGestionBonusActived,
                                                  bool isLitreMoutRaisin,
                                                  bool isZoneDifficulteChecked,
                                                  ParametrePaiement parametre)
            : base(isGestionBonusActived, isLitreMoutRaisin, isZoneDifficulteChecked, parametre)
        {
        }

        internal override double CalculPrixLignePesee(DSPesees.PED_PESEE_DETAILRow pedRow)
        {
            throw new System.NotImplementedException();
        }

        internal override LignePesee GetLigneBonus(LignePesee lignePesee,
                                                   DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                   double overload)
        {
            throw new System.NotImplementedException();
        }

        internal override LignePesee GetLignePesees(DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                    CoupleProPaiIds coupleProPaiId,
                                                    double overload)
        {
            throw new System.NotImplementedException();
        }
    }
}