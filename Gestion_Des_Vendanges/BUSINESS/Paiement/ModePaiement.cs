﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using static Gestion_Des_Vendanges.DAO.DSPesees;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Paramètres de calcul d'un paiement
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public abstract class ModePaiement
    {
        #region Fields

        private bool _isGestionBonusActived;
        private bool _isLitreMoutRaisin;
        private bool _isZoneDifficulteChecked;
        protected int? _producerId = null;
        protected int _classeId;

        internal ParametrePaiement _parametre;

        #endregion Fields

        #region Properties

        protected readonly MOC_MODE_COMPTABILISATIONTableAdapter MocTable = ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
        protected readonly PRO_PROPRIETAIRETableAdapter ProTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
        protected readonly PEE_PESEE_ENTETETableAdapter PeeTable = ConnectionManager.Instance.CreateGvTableAdapter<PEE_PESEE_ENTETETableAdapter>();
        protected readonly CEP_CEPAGETableAdapter CepTable = ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();
        protected readonly VAL_VALEUR_CEPAGETableAdapter ValTable = ConnectionManager.Instance.CreateGvTableAdapter<VAL_VALEUR_CEPAGETableAdapter>();
        protected readonly CLA_CLASSETableAdapter ClaTable = ConnectionManager.Instance.CreateGvTableAdapter<CLA_CLASSETableAdapter>();

        public int DefaultMocId => ClaTable.GetData().First(c => c.CLA_NOM.Contains("Classe 1 - AOC et Grand cru")).CLA_ID;
        public bool IsGestionBonusActived => _isGestionBonusActived;
        public bool IsLitreMoutRaisin => _isLitreMoutRaisin;
        public bool IsZoneDifficulteChecked => _isZoneDifficulteChecked;

        #endregion Properties

        #region Constructors

        internal ModePaiement(bool isGestionBonusActived,
                              bool isLitreMoutRaisin,
                              bool isZoneDifficulteChecked,
                              ParametrePaiement parametre)
        {
            _parametre = parametre;
            _isGestionBonusActived = isGestionBonusActived;
            _isLitreMoutRaisin = isLitreMoutRaisin;
            _isZoneDifficulteChecked = isZoneDifficulteChecked;
            _classeId = DefaultMocId;
        }

        #endregion Constructors

        #region Abstract Methods

        internal abstract LignePesee GetLignePesees(PED_PESEE_DETAILRow peseeDetailrow,
                                                    CoupleProPaiIds coupleProPaiId,
                                                    double overload);

        internal abstract double CalculPrixLignePesee(PED_PESEE_DETAILRow pedRow);

        internal abstract LignePesee GetLigneBonus(LignePesee lignePesee,
                                                   PED_PESEE_DETAILRow peseeDetailrow,
                                                   double overload);

        #endregion Abstract Methods

        #region Methods

        protected bool IsProducerAssujetis
        {
            get
            {
                if (_producerId == null) throw new ArgumentNullException(nameof(IsProducerAssujetis), "ProducerId is null in ModePaiement");

                int? modId = ProTable?.GetModId((int)_producerId) ?? (int)ModeTVA.Sans;
                return modId != (int)ModeTVA.Sans;
            }
        }

        internal double GetPedQuantite(PED_PESEE_DETAILRow row)
        {
            return Utilities.IsPrixKg ?
                   row.PED_QUANTITE_KG :
                   GetPedQuantiteLitres(row, _parametre.ModePaiement.IsLitreMoutRaisin);
        }

        private double GetPedQuantiteLitres(PED_PESEE_DETAILRow row, bool isLitreMoutRaisin)
        {
            var tauxConversionMoutRaisin = GetTauxConversionMoutRaisinSelonCouleur(GetCouleurCepage(row));

            return isLitreMoutRaisin ? (row.PED_QUANTITE_KG * tauxConversionMoutRaisin) : row.PED_QUANTITE_LITRES;
        }

        private double GetTauxConversionMoutRaisinSelonCouleur(Couleur couleur)
        {
            switch (couleur)
            {
                case Couleur.blanc:
                    return (double)GlobalParam.Instance.TauxConversionBlancMout;

                case Couleur.rouge:
                    return (double)GlobalParam.Instance.TauxConversionRougeMout;

                default:
                    return (double)GlobalParam.Instance.TauxConversionBlancMout;
            }
        }

        private Couleur GetCouleurCepage(PED_PESEE_DETAILRow row)
        {
            var cepTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();

            switch (((string)cepTableAdapter.GetCouleurNomById(row.CEP_ID) ?? string.Empty).ToLower())
            {
                case "blanc":
                    return Couleur.blanc;

                case "rouge":
                    return Couleur.rouge;

                default:
                    return Couleur.rouge;
            }
        }

        protected void CalculMontantTVA(LignePesee lignePesee, double montant)
        {
            lignePesee.MontantHT = montant;
            lignePesee.MontantTTC = IsProducerAssujetis ?
                                    Math.Round(montant * (1 + lignePesee.TauxTVA / 100), 2) :
                                    montant;
        }

        protected double ArrondisCinqCentimes(double montant) => (montant % 0.5 == 0) ? montant : Math.Round((montant * 20), MidpointRounding.AwayFromZero) / 20;

        protected double GetMontantBonus(PED_PESEE_DETAILRow pedRow)
        {
            if (_parametre == null)
                throw new ArgumentNullException("Parameter in ModePaiement is null.");

            Trace.WriteLine($"[Information] | ped_id:{pedRow.PED_ID} - pee_id:{pedRow.PEE_ID}");
            try
            {
                int lieId = Convert.ToInt32(PeeTable.GetLieIdByPedId(pedRow.PED_ID));

                double montantBonus = 0;
                double peseeOechsle = pedRow.PED_SONDAGE_OE;

                // TODO Get cépage moyen for custom val_valeur
                double peseeOechsleMoyen = ValTable.GetOechsleMoyenByCepLieClaAnn(pedRow.CEP_ID, _parametre.AnnId, lieId, _classeId, _producerId.GetValueOrDefault()) ?? -1;

                if (peseeOechsleMoyen >= 0)
                {
                    double diffOechslePesee = Math.Truncate(peseeOechsle - peseeOechsleMoyen);
                    try
                    {
                        montantBonus = (double)ValTable.GetBonusByLieCepClaAnnOeUp(pedRow.CEP_ID, lieId, _parametre.AnnId, _classeId, diffOechslePesee, _producerId.GetValueOrDefault());
                    }
                    catch (InvalidOperationException)
                    {
                        // Throw when BON_BONUS is not set
                        MessageBox.Show("La table de prix bonus/malus n'a pas été trouvée.",
                                        "Erreur lecture bonus", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        throw;
                    }
                    catch (NullReferenceException)
                    {
                        try
                        {
                            montantBonus = (double)ValTable.GetBonusByLieCepClaAnnOeDown(pedRow.CEP_ID, lieId, _parametre.AnnId, _classeId, diffOechslePesee, _producerId.GetValueOrDefault());
                        }
                        catch (NullReferenceException) { }
                    }
                }
                else
                {
                    throw new TablePrixNotFoundGVException(lieId, pedRow.CEP_ID, _parametre.AnnId);
                }

                return montantBonus;
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenue lors de la récupération du montant bonus.",
                                "Erreur lecture bonus", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new ArgumentOutOfRangeException();
            }
        }

        protected double GetBonus(double montantBonus, PED_PESEE_DETAILRow pedRow, double overload)
        {
            montantBonus *= overload > 0 ? overload : pedRow.PED_QUANTITE_KG;
            return Math.Round(montantBonus, 2);
        }

        #endregion Methods
    }
}