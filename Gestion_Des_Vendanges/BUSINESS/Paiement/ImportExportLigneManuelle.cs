﻿using System;
using System.Data;
using System.IO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal abstract class ImportExportLigneManuelleBase
    {
        #region Fields

        protected FichierCSV _fichierCsv;
        protected string[,] _producteurIdList;
        protected string[,] _producteurListCsv;
        protected int _amountOfRows = -1;
        protected int _numberOfColumns;

        #endregion Fields

        #region Properties

        protected abstract int AmountOfRows { get; set; }

        #endregion Properties

        #region Constructors

        private ImportExportLigneManuelleBase(string[,] producteurIdList, int numberOfColumns)
        {
            _producteurIdList = producteurIdList;
            _numberOfColumns = numberOfColumns;
        }

        public ImportExportLigneManuelleBase(string[,] producteurIdList, FileInfo fichier, int numberOfColumns)
            : this(producteurIdList, numberOfColumns)
        {
            _fichierCsv = new FichierCSV(fichier);
        }

        public ImportExportLigneManuelleBase(string[,] producteurIdList, string fichier, int numberOfColumns)
            : this(producteurIdList, numberOfColumns)
        {
            _fichierCsv = new FichierCSV(fichier);
        }

        #endregion Constructors

        #region Public Methods

        public string CreateFichierCsvProducteurs(string destinationPath, ExportCsvType exportCsvType)
        {
            if (_producteurIdList == null) throw new ArgumentNullException("ImportExportLigneManuelle", "ProducteurIdList is null");
            if (_producteurIdList.Length < 1) return string.Empty;

            _producteurListCsv = new string[AmountOfRows, _numberOfColumns];

            AddHeaderProducteurListCsv();
            AddRowsProducteurListCsv();

            return _fichierCsv.SetFichier(_producteurListCsv, FichierCSV.CsvCharacterSeparator.semicolon);
        }

        public string[,] GetFichierCsvProducteurs(string locationPath)
        {
            return new FichierCSV(locationPath).GetFichier(FichierCSV.CsvCharacterSeparator.semicolon);
        }

        #endregion Public Methods

        #region Private Methods

        protected abstract void AddHeaderProducteurListCsv();

        protected abstract void AddRowsProducteurListCsv();

        protected static int SumTotalTableRows(string[,] itemList, DataTable dataTable, bool hasHeader)
        {
            return (itemList.GetLength(0) * dataTable.Rows.Count) + (hasHeader ? 1 : 0);
        }

        #endregion Private Methods
    }
}