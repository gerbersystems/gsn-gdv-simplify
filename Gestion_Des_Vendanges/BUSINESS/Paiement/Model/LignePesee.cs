﻿namespace Gestion_Des_Vendanges.BUSINESS
{
    public class LignePesee
    {
        public int PeseeDetailId { get; set; }
        public int PaiementId { get; set; }
        public double Kilo { get; set; }
        public double PrixKilo { get; set; }
        public double MontantTTC { get; set; }
        public double MontantHT { get; set; }
        public int AnneeModeComptabilisationId { get; set; }
        public int ModeTvaId { get; set; }
        public double TauxTVA { get; set; }
        public double Pourcentage { get; set; }
        public TypeLignePesee TypeLignePesee { get; set; }
    }
}