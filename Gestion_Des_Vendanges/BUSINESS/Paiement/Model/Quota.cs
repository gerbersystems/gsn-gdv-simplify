﻿namespace Gestion_Des_Vendanges.BUSINESS
{
    public class Quota
    {
        public int GrapeId { get; set; }
        public int LocalityId { get; set; }
        public double AuthorizedQuota { get; set; }
        public double TotalKiloPaid { get; set; }
    }
}