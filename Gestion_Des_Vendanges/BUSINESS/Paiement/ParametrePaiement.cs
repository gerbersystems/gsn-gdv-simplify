﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Paramètres des paiements
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    *  ATH - 20.11.2009 - Ajout de la gestion de la TVA
    * */

    // INFO | #paiements - 4. ParametrePaiement
    partial class ParametrePaiement
    {
        #region Fields

        private int _annId;
        private bool _isAcompte = true;
        private string _texte;
        private DateTime _date;
        private List<int> _proIds;
        private ModePaiement _modePaiement;
        private List<PaiementLigneManuelle> _ligneManuelles;
        private string _code;
        private decimal _pourcentage;
        private VAL_VALEUR_CEPAGETableAdapter _valTable;
        private VAB_VALEUR_CEPAGE_BESOINTableAdapter _vabTable;
        private PRO_PROPRIETAIRETableAdapter _proTable;
        private PRO_NOMCOMPLETTableAdapter _proNomCompletTable;
        private bool _zoneDifficulteChecked;
        private bool _isLitreMoutRaisin = false;

        #endregion Fields

        #region Properties

        public List<string> Producteurs
        {
            get
            {
                List<string> producteurs = new List<string>();

                foreach (int proId in _proIds)
                    producteurs.Add(Convert.ToString(_proNomCompletTable.GetNomCompletByProId(proId)));

                return producteurs;
            }
        }

        public string[,] ProducteursIdNom
        {
            get
            {
                string[,] producteurs = new string[ProIds.Length, 3];

                for (int i = 0; i < ProIds.Length; i++)
                {
                    producteurs[i, 0] = ProIds[i].ToString();
                    producteurs[i, 1] = _proNomCompletTable.GetNomCompletByProId(ProIds[i]).ToString();
                    producteurs[i, 2] = _proTable.GetDataByProId(ProIds[i]).FirstOrDefault().ADR_ID_WINBIZ.ToString();
                }

                return producteurs;
            }
        }

        public int[] ProIds => _proIds.ToArray();

        public int CountProId => _proIds.Count;

        public int AnnId
        {
            get => _annId;
            set => _annId = value;
        }

        public string Texte
        {
            get => _texte;
            set => _texte = value;
        }

        public DateTime Date
        {
            get => _date;
            set => _date = value;
        }

        public ModePaiement ModePaiement => _modePaiement;

        public PaiementLigneManuelle[] LigneManuelles => _ligneManuelles.ToArray();

        public decimal Pourcentage => _pourcentage;

        public bool IsAcompte
        {
            get => _isAcompte;
            set => _isAcompte = value;
        }

        public string Code
        {
            get => _code;
            set => _code = value;
        }

        public bool ZoneTravailIncluded
        {
            get => _zoneDifficulteChecked;
            set => _zoneDifficulteChecked = value;
        }

        public bool IsLitreMoutRaisin
        {
            get => _isLitreMoutRaisin;
            set => _isLitreMoutRaisin = value;
        }

        #endregion Properties

        #region Constructors

        public ParametrePaiement()
        {
            _proIds = new List<int>();
            _ligneManuelles = new List<PaiementLigneManuelle>();
            _proNomCompletTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_NOMCOMPLETTableAdapter>();
            _valTable = ConnectionManager.Instance.CreateGvTableAdapter<VAL_VALEUR_CEPAGETableAdapter>();
            _vabTable = ConnectionManager.Instance.CreateGvTableAdapter<VAB_VALEUR_CEPAGE_BESOINTableAdapter>();
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
        }

        #endregion Constructors

        #region Methods

        public void AddProprietaire(int proId) => _proIds.Add(proId);

        public void ClearProprietaire() => _proIds.Clear();

        public void AddLigneManuelle(PaiementLigneManuelle ligne) => _ligneManuelles.Add(ligne);

        public void ClearLigneManuelle() => _ligneManuelles.Clear();

        public void SetModePaiementPrixFixe(decimal prix,
                                            bool isZoneDifficulteChecked,
                                            bool PaiementlitreDeMoutRaisin)
        {
            _modePaiement = new ModePaiementPrixFixe(prix,
                                                     PaiementlitreDeMoutRaisin,
                                                     isZoneDifficulteChecked,
                                                     this);
            _pourcentage = 100;
            _zoneDifficulteChecked = isZoneDifficulteChecked;
        }

        public void SetModePaiementCepLie(decimal pourcentage,
                                          bool isGestionBonusActived,
                                          bool isZoneDifficulteChecked,
                                          bool paiementlitreDeMoutRaisin)
        {
            TablesDePrixCepLieExist();

            _modePaiement = new ModePaiementParCepageLieu(isGestionBonusActived,
                                                          paiementlitreDeMoutRaisin,
                                                          isZoneDifficulteChecked,
                                                          this);
            _pourcentage = pourcentage;
            _zoneDifficulteChecked = isZoneDifficulteChecked;
        }

        public void SetModePaiementCepLieTablePrix(decimal pourcentage,
                                                   bool isGestionBonusActived,
                                                   bool isZoneDifficulteChecked,
                                                   bool paiementlitreDeMoutRaisin)
        {
            TablesDePrixCepLieExist();

            _modePaiement = new ModePaiementParCepageLieuTablePrix(isGestionBonusActived,
                                                                   paiementlitreDeMoutRaisin,
                                                                   isZoneDifficulteChecked,
                                                                   this);
            _pourcentage = pourcentage;
            _zoneDifficulteChecked = isZoneDifficulteChecked;
        }

        #endregion Methods
    }
}