﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using static Gestion_Des_Vendanges.DAO.DSPesees;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.BUSINESS
{
    partial class PaiementManager
    {
        #region Fields

        protected PEE_PESEE_ENTETETableAdapter _peeTable;
        private LIP_LIGNE_PAIEMENT_PESEETableAdapter _lipTable;
        private PED_PESEE_DETAILTableAdapter _pedTable;
        private VAL_VALEUR_CEPAGETableAdapter _valTable;

        #endregion Fields

        #region Constructors

        public void LoadSpecificTableAdapter()
        {
            _lipTable = ConnectionManager.Instance.CreateGvTableAdapter<LIP_LIGNE_PAIEMENT_PESEETableAdapter>();
            _pedTable = ConnectionManager.Instance.CreateGvTableAdapter<PED_PESEE_DETAILTableAdapter>();
            _valTable = ConnectionManager.Instance.CreateGvTableAdapter<VAL_VALEUR_CEPAGETableAdapter>();
            _peeTable = ConnectionManager.Instance.CreateGvTableAdapter<PEE_PESEE_ENTETETableAdapter>();
        }

        #endregion Constructors

        #region Methods

        private bool HasBonus(int pedId) => (int)_valTable?.HasBonus(pedId) == 1 ? true : false;

        private void CreateLignePesees()
        {
            var modePaiement = _parametre.ModePaiement;
            QuotaDataTable _quotas = null;

            try
            {
                // TODO Implement setting in wizard payment to select to not pay overloaded weighing
                bool DoNotPayOverloadWeighing = true;

                if (DoNotPayOverloadWeighing)
                {
                    _quotas = ConnectionManager.Instance.CreateGvTableAdapter<QuotaTableAdapter>()
                                                        .GetByAnnId(Utilities.IdAnneeCourante);
                }

                foreach (var coupleProPaiId in _coupleProPaiIds)
                {
                    IEnumerable<Quota> _quotaByProId = _quotas?.Where(q => q.PRODUCTEUR_ID == coupleProPaiId.ProId)
                                                               .Select(q => new Quota()
                                                               {
                                                                   GrapeId = q.CEP_ID,
                                                                   LocalityId = q.LIE_ID,
                                                                   AuthorizedQuota = q.TOTAL_LITRES / 0.8,
                                                                   TotalKiloPaid = 0
                                                               });

                    _pedTable.FillByProAnn(new PED_PESEE_DETAILDataTable(),
                                           _parametre.AnnId,
                                           coupleProPaiId.ProId);

                    var peseesDetails = _pedTable.GetDataByProAnn(_parametre.AnnId, coupleProPaiId.ProId)
                                                 .OrderByDescending(p => p.PED_SONDAGE_OE);
                    double overload = -1;

                    foreach (var peseeDetail in peseesDetails)
                    {
                        var currentQuota = _quotaByProId?.First(q => q.GrapeId == peseeDetail.CEP_ID &&
                                                                     q.LocalityId == _peeTable.GetLieIdByPedId(peseeDetail.PED_ID));

                        if (currentQuota != null)
                            overload = SetOverload(overload, peseeDetail, currentQuota);

                        switch (GlobalParam.Instance.BonusUnite)
                        {
                            case BonusUniteEnum.VariablePourcent:
                                var lignePesee = new LignePesee();
                                lignePesee = _parametre.ModePaiement.GetLignePesees(peseeDetail,
                                                                                    coupleProPaiId,
                                                                                    overload);

                                _lipTable.Insert(PED_ID: lignePesee.PeseeDetailId,
                                                 PAI_ID: lignePesee.PaiementId,
                                                 LIP_QUANTITE_KG: lignePesee.Kilo,
                                                 LIP_PRIX_KG: lignePesee.PrixKilo,
                                                 LIP_MONTANT_TTC: lignePesee.MontantTTC,
                                                 LIP_MONTANT_HT: lignePesee.MontantHT,
                                                 MOC_ID: lignePesee.AnneeModeComptabilisationId,
                                                 MOD_ID: lignePesee.ModeTvaId,
                                                 LIP_TAUX_TVA: lignePesee.TauxTVA,
                                                 LIP_POURCENTAGE: lignePesee.Pourcentage,
                                                 LIP_TYPE: (int)lignePesee.TypeLignePesee);

                                // Create new "ligne pesée" 0 CHF for the second part of the first overloaded weighing
                                if (overload > 0)
                                {
                                    _lipTable.Insert(PED_ID: lignePesee.PeseeDetailId,
                                                     PAI_ID: lignePesee.PaiementId,
                                                     LIP_QUANTITE_KG: (peseeDetail.PED_QUANTITE_KG - overload),
                                                     LIP_PRIX_KG: 0,
                                                     LIP_MONTANT_TTC: 0,
                                                     LIP_MONTANT_HT: 0,
                                                     MOC_ID: lignePesee.AnneeModeComptabilisationId,
                                                     MOD_ID: lignePesee.ModeTvaId,
                                                     LIP_TAUX_TVA: lignePesee.TauxTVA,
                                                     LIP_POURCENTAGE: lignePesee.Pourcentage,
                                                     LIP_TYPE: (int)TypeLignePesee.SurplusNonPayé);
                                }

                                if (modePaiement.IsGestionBonusActived && HasBonus(peseeDetail.PED_ID))
                                {
                                    var ligneBonus = _parametre.ModePaiement.GetLigneBonus(lignePesee,
                                                                                           peseeDetail,
                                                                                           overload);

                                    _lipTable.Insert(PED_ID: ligneBonus.PeseeDetailId,
                                                     PAI_ID: ligneBonus.PaiementId,
                                                     LIP_QUANTITE_KG: lignePesee.Kilo,
                                                     LIP_PRIX_KG: ligneBonus.PrixKilo,
                                                     LIP_MONTANT_TTC: ligneBonus.MontantTTC,
                                                     LIP_MONTANT_HT: ligneBonus.MontantHT,
                                                     MOC_ID: ligneBonus.AnneeModeComptabilisationId,
                                                     MOD_ID: ligneBonus.ModeTvaId,
                                                     LIP_TAUX_TVA: ligneBonus.TauxTVA,
                                                     LIP_POURCENTAGE: ligneBonus.Pourcentage,
                                                     LIP_TYPE: (int)ligneBonus.TypeLignePesee);
                                }

                                continue;
                        }
                        //*********************************************************************************************//
                        // TODO Please implement the code below in OO manner, like the code above please...            //
                        //*********************************************************************************************//

                        double montantTTC, montantHT;
                        double prix = CalculPrixLignePesee(pedRow: peseeDetail);
                        int mocId = Convert.ToInt32(_cepTable.GetMoc(_parametre.AnnId, peseeDetail.CEP_ID));
                        double tauxTVA = Convert.ToDouble(_mocTable.GetTauxTva(mocId));
                        int modId = GetModId(coupleProPaiId.ProId, mocId);
                        double pedQuantite = GetPedQuantite(peseeDetail);
                        double montant = Math.Round(prix * pedQuantite, 2);

                        if (CalculMontantTVA(modId, tauxTVA, montant, out montantTTC, out montantHT) == 0)
                            tauxTVA = 0;

                        montantHT = ((montantHT % 0.5) != 0) ? SetArrondis(montantHT) : montantHT;
                        montantTTC = ((montantTTC % 0.5) != 0) ? SetArrondis(montantTTC) : montantTTC;

                        // TODO | #paiement - Implémenter LIP_TYPE -> LIP_POURCENTAGE: 0, LIP_TYPE: 0  - valeur provisoire
                        // Si le paiement doit être calculé en litres de moûts de raisin [ lipType =3 ];
                        // Par Défaut les litres sont calculé en litres de vins clair.
                        // Qui est la valeur en litres stockée dans PED_PESEE_DETAIL > PED_QUANTITEE_LITRES
                        int lipType = (_parametre.ModePaiement.IsLitreMoutRaisin) ? 3 : 0;

                        _lipTable.Insert(PED_ID: peseeDetail.PED_ID,
                                         PAI_ID: coupleProPaiId.PaiId,
                                         LIP_QUANTITE_KG: pedQuantite,
                                         LIP_PRIX_KG: prix,
                                         LIP_MONTANT_TTC: montantTTC,
                                         LIP_MONTANT_HT: montantHT,
                                         MOC_ID: mocId,
                                         MOD_ID: modId,
                                         LIP_TAUX_TVA: tauxTVA,
                                         LIP_POURCENTAGE: (double)_parametre.Pourcentage,
                                         LIP_TYPE: lipType);

                        if (modePaiement.IsGestionBonusActived &&
                            modePaiement is ModePaiementParCepageLieuTablePrix)
                        {
                            prix = GetMontantBonus(peseeDetail, modePaiement, _valTable);
                            montant = GetBonus(pedRow: peseeDetail, montantBonus: prix);

                            if (CalculMontantTVA(modId, tauxTVA, montant, out montantTTC, out montantHT) == 0)
                                tauxTVA = 0;

                            montantHT = ((montantHT % 0.5) != 0) ? SetArrondis(montantHT) : montantHT;
                            montantTTC = ((montantTTC % 0.5) != 0) ? SetArrondis(montantTTC) : montantTTC;

                            _lipTable.Insert(PED_ID: peseeDetail.PED_ID,
                                             PAI_ID: coupleProPaiId.PaiId,
                                             LIP_QUANTITE_KG: pedQuantite,
                                             LIP_PRIX_KG: prix,
                                             LIP_MONTANT_TTC: montantTTC,
                                             LIP_MONTANT_HT: montantHT,
                                             MOC_ID: mocId,
                                             MOD_ID: modId,
                                             LIP_TAUX_TVA: tauxTVA,
                                             LIP_POURCENTAGE: 100,
                                             LIP_TYPE: 1);
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenue lors de la création d'une ligne de pesée",
                                "Erreur création ligne de pesée", MessageBoxButtons.OK, MessageBoxIcon.Error);

                throw new ArgumentOutOfRangeException();
            }
        }

        private double SetOverload(double overload, PED_PESEE_DETAILRow peseeDetail, Quota currentQuota)
        {
            // When we do not have find quota; permit payment
            if (currentQuota == null) return -1;

            // When all authorized quota is paid return 0; this set the price to 0 chf
            if (currentQuota.TotalKiloPaid - currentQuota.AuthorizedQuota == 0) return 0;

            // When the weighing overload authorized quota, we pay only the part authorized.
            // The overload value is used to send the amount of kilos we can pay.
            // Another payment must be done with the rest of the weighing but with the price to 0 chf.
            if (currentQuota.TotalKiloPaid + peseeDetail.PED_QUANTITE_KG > currentQuota.AuthorizedQuota)
            {
                currentQuota.TotalKiloPaid += overload;
                return currentQuota.AuthorizedQuota - currentQuota.TotalKiloPaid;
            }
            // When weighing not overload, we sum the total of paid kilos.
            else
            {
                currentQuota.TotalKiloPaid += peseeDetail.PED_QUANTITE_KG;
                return -1;
            }
        }

        private double GetPedQuantite(PED_PESEE_DETAILRow row)
        {
            return Utilities.IsPrixKg ?
                   row.PED_QUANTITE_KG :
                   GetPedQuantiteLitres(row, _parametre.ModePaiement.IsLitreMoutRaisin);
        }

        private double GetPedQuantiteLitres(PED_PESEE_DETAILRow row, bool isLitreMoutRaisin)
        {
            var tauxConversionMoutRaisin = GetTauxConversionMoutRaisinSelonCouleur(GetCouleurCepage(row));

            return isLitreMoutRaisin ? (row.PED_QUANTITE_KG * tauxConversionMoutRaisin) : row.PED_QUANTITE_LITRES;
        }

        private double GetTauxConversionMoutRaisinSelonCouleur(Couleur couleur)
        {
            switch (couleur)
            {
                case Couleur.blanc:
                    return (double)GlobalParam.Instance.TauxConversionBlancMout;

                case Couleur.rouge:
                    return (double)GlobalParam.Instance.TauxConversionRougeMout;

                default:
                    return (double)GlobalParam.Instance.TauxConversionBlancMout;
            }
        }

        private Couleur GetCouleurCepage(PED_PESEE_DETAILRow row)
        {
            var cepTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();

            switch (((string)cepTableAdapter.GetCouleurNomById(row.CEP_ID) ?? string.Empty).ToLower())
            {
                case "blanc":
                    return Couleur.blanc;

                case "rouge":
                    return Couleur.rouge;

                default:
                    return Couleur.rouge;
            }
        }

        private double SetArrondis(double montant) =>
            Math.Round((montant * 20), MidpointRounding.AwayFromZero) / 20;

        private double CalculPrixLignePesee(PED_PESEE_DETAILRow pedRow)
        {
            double prix;
            var modePaiement = _parametre.ModePaiement;

            if (modePaiement is ModePaiementPrixFixe)
            {
                prix = Convert.ToDouble(((ModePaiementPrixFixe)modePaiement).Prix);
            }
            else
            {
                int lieId;

                var valTable = ConnectionManager.Instance.CreateGvTableAdapter<VAL_VALEUR_CEPAGETableAdapter>();
                if (modePaiement is ModePaiementParCepage)
                {
                    lieId = ((ModePaiementParCepage)modePaiement).LieId;
                }
                else
                {
                    lieId = Convert.ToInt32(_peeTable.GetLieIdByPedId(pedRow.PED_ID));
                }
                if (modePaiement.IsGestionBonusActived && !(modePaiement is ModePaiementParCepageLieuTablePrix))
                {
                    try
                    {
                        prix = (double)valTable.GetPrixByLieCepAnnOeUP(pedRow.CEP_ID,
                                                                       lieId,
                                                                       _parametre.AnnId,
                                                                       Convert.ToDouble(pedRow.PED_SONDAGE_OE));
                    }
                    catch (NullReferenceException)
                    {
                        try
                        {
                            prix = (double)valTable.GetPrixByLieCepAnnOeDOWN(pedRow.CEP_ID, lieId, _parametre.AnnId, Convert.ToDouble(pedRow.PED_SONDAGE_OE));
                        }
                        catch (NullReferenceException)
                        {
                            prix = (double)valTable.GetPrixByCepLieAnn(pedRow.CEP_ID, _parametre.AnnId, lieId);
                        }
                    }
                }
                else
                {
                    prix = (double)valTable.GetPrixByCepLieAnn(pedRow.CEP_ID, _parametre.AnnId, lieId);
                }
            }
            return Math.Round((prix * Convert.ToDouble(_parametre.Pourcentage)) / 100, 3);
        }

        private double GetMontantBonus(PED_PESEE_DETAILRow pedRow,
                                       ModePaiement modePaiement,
                                       VAL_VALEUR_CEPAGETableAdapter valTable)
        {
            Trace.WriteLine($"[Information] | ped_id:{pedRow.PED_ID} - pee_id:{pedRow.PEE_ID}");
            try
            {
                int lieId = Convert.ToInt32(_peeTable.GetLieIdByPedId(pedRow.PED_ID));

                double montantBonus = 0;
                double peseeOechsle = pedRow.PED_SONDAGE_OE;
                double peseeOechsleMoyen = valTable.GetOechsleMoyenByCepLieAnn(pedRow.CEP_ID, _parametre.AnnId, lieId) ?? -1;

                if (peseeOechsleMoyen >= 0)
                {
                    double diffOechslePesee = Math.Round(peseeOechsle - peseeOechsleMoyen);
                    try
                    {
                        montantBonus = (double)valTable.GetBonusByLieCepAnnOeUp(pedRow.CEP_ID, lieId, _parametre.AnnId, diffOechslePesee);
                    }
                    catch (NullReferenceException)
                    {
                        try
                        {
                            montantBonus = (double)valTable.GetBonusByLieCepAnnOeDown(pedRow.CEP_ID, lieId, _parametre.AnnId, diffOechslePesee);
                        }
                        catch (NullReferenceException) { }
                    }
                }
                else
                {
                    throw new TablePrixNotFoundGVException(lieId, pedRow.CEP_ID, _parametre.AnnId);
                }
                return montantBonus;
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenue lors de la récupération du montant bonus.",
                                "Erreur lecture bonus", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw new ArgumentOutOfRangeException();
            }
        }

        private double GetBonus(double montantBonus, PED_PESEE_DETAILRow pedRow)
        {
            montantBonus *= (pedRow.PED_QUANTITE_KG);
            return Math.Round(montantBonus, 2);
        }

        #endregion Methods
    }
}