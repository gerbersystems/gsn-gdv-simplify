﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class ImportExportLigneManuelleZoneTravail : ImportExportLigneManuelleBase
    {
        #region Fields

        private const short NUMBER_OF_COLUMNS = 6;
        private ZOT_ZONE_TRAVAILTableAdapter _zotTableAdapter;
        private DSPesees.ZOT_ZONE_TRAVAILDataTable _zotDataTable;

        #endregion Fields

        #region Constructors

        public ImportExportLigneManuelleZoneTravail(string[,] producteurIdList, string fichier) :
            base(producteurIdList, fichier, NUMBER_OF_COLUMNS)
        {
            _zotTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<ZOT_ZONE_TRAVAILTableAdapter>();
        }

        #endregion Constructors

        #region Public Methods

        protected override int AmountOfRows
        {
            get
            {
                if (_amountOfRows > -1) return _amountOfRows;

                _zotDataTable = new DSPesees.ZOT_ZONE_TRAVAILDataTable();
                _zotTableAdapter.FillDifferentFromZero(_zotDataTable);
                _amountOfRows = SumTotalTableRows(_producteurIdList, _zotDataTable, hasHeader: true);
                return _amountOfRows;
            }
            set { _amountOfRows = value; }
        }

        #endregion Public Methods

        #region Private Methods

        protected override void AddHeaderProducteurListCsv()
        {
            _producteurListCsv[0, 0] = "Id";
            _producteurListCsv[0, 1] = "ProducteurNom";
            _producteurListCsv[0, 2] = "Description ligne manuelle";
            _producteurListCsv[0, 3] = "Quantite";
            _producteurListCsv[0, 4] = "Prix";
            _producteurListCsv[0, 5] = "Montant ligne manuelle";
        }

        protected override void AddRowsProducteurListCsv()
        {
            for (int i = 1; i < AmountOfRows; i++)
                i = CreateRowsByProducteurId(i);
        }

        protected int CreateRowsByProducteurId(int index)
        {
            for (int j = 0; j < _producteurIdList.GetLength(0); j++)
            {
                for (int k = 0; k < _zotDataTable.Rows.Count; k++, index++)
                {
                    _producteurListCsv[index, 0] = _producteurIdList[j, 0].ToString().Trim();
                    _producteurListCsv[index, 1] = _producteurIdList[j, 1].ToString().Trim();
                    _producteurListCsv[index, 2] = _zotDataTable.Rows[k]["ZOT_DESCRIPTION"].ToString().Trim();
                    _producteurListCsv[index, 3] = string.Empty;
                    _producteurListCsv[index, 4] = _zotDataTable.Rows[k]["ZOT_VALEUR"].ToString().Trim();
                    _producteurListCsv[index, 5] = string.Empty;
                }
            }

            return index;
        }

        #endregion Private Methods
    }
}