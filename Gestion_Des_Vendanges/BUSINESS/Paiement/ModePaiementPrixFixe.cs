﻿using Gestion_Des_Vendanges.DAO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Paramètres de calcul d'un paiement par prix fixe
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public class ModePaiementPrixFixe : ModePaiement
    {
        private decimal _prix;

        public decimal Prix
        {
            get => _prix;
            set => _prix = value;
        }

        internal ModePaiementPrixFixe(decimal prix,
                                      bool isLitreMoutRaisin,
                                      bool isZoneDifficulteChecked,
                                      ParametrePaiement parametre)
            : base(false, isLitreMoutRaisin, isZoneDifficulteChecked, parametre)
        {
            _prix = prix;
        }

        internal override LignePesee GetLignePesees(DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                    CoupleProPaiIds coupleProPaiId,
                                                    double overload)
        {
            throw new System.NotImplementedException();
        }

        internal override double CalculPrixLignePesee(DSPesees.PED_PESEE_DETAILRow pedRow)
        {
            throw new System.NotImplementedException();
        }

        internal override LignePesee GetLigneBonus(LignePesee lignePesee,
                                                   DSPesees.PED_PESEE_DETAILRow peseeDetailrow,
                                                   double overload)
        {
            throw new System.NotImplementedException();
        }
    }
}