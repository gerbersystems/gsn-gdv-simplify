﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Exceptions utilisées dans la Gestion Des Vendanges
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class GVException : Exception
    {
        public GVException(string message)
            : base(message)
        {
        }

        public GVException()
        {
        }
    }

    //Exception levée lorsqu'une table de prix n'existe pas
    internal class TablePrixNotFoundGVException : GVException
    {
        private int _lieId;
        private int _cepId;
        private int _annId;

        public int LieId
        {
            get { return _lieId; }
        }

        public int CepId
        {
            get { return _cepId; }
        }

        public int AnnId
        {
            get { return _annId; }
        }

        public TablePrixNotFoundGVException(int lieId, int cepId, int annId)
            : base("La table de prix du cépage: " + Utilities.GetCepage(cepId) + "\n" +
                        "dans le lieu de production: " + Utilities.GetLieuProduction(lieId) + "\n" +
                        "pour l'année: " + Utilities.GetAnnee(annId) + "\nn'a pas été créée.")
        {
            _lieId = lieId;
            _cepId = cepId;
            _annId = annId;
        }
    }

    //Exception levée lorsque un degré Oe existe déjà dans une table de prix
    internal class DegreOeIdentiqueGVException : GVException
    {
    }

    //Exception levée lorsque la licence du nombre de producteurs est insuffisante
    internal class NombreProducteurGVException : GVException
    {
    }

    //Niveau de license incorrect
    internal class LicenseGVException : GVException
    {
    }

    //Model adresse WinBIZ manquant
    internal class ModelAdresseWinBIZNotFoundGVException : GVException
    {
        private string _model;

        public string Model
        {
            get { return _model; }
        }

        public ModelAdresseWinBIZNotFoundGVException(string model)
        {
            _model = model;
        }
    }

    //Mode comptabilisation pas initialisé
    internal class ModeComptabilisationNotSetGVException : GVException
    {
    }
}