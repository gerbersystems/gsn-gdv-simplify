﻿using System;
using System.IO;
using System.Text;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Gère les fichiers au format CSV
    *  Date de création: 16.11.2009
    *  Modifications:
    * */

    internal class FichierCSV
    {
        private FileInfo _fichier;

        public FichierCSV(FileInfo fichier)
        {
            _fichier = fichier;
        }

        public FichierCSV(string fichier)
        {
            _fichier = new FileInfo(fichier);
        }

        public enum CsvCharacterSeparator { tabulation = 9, semicolon = 59 };

        public string[,] GetFichier(CsvCharacterSeparator csvSeparator = CsvCharacterSeparator.semicolon)
        {
            try
            {
                string[] lines = File.ReadAllLines(_fichier.FullName, Encoding.UTF8);
                int nbChamp = lines[0].Split((char)csvSeparator).Length;
                string[,] fileCSV = new string[lines.Length, nbChamp];

                for (int i = 0; i < lines.Length; i++)
                {
                    string[] lineSplit = lines[i].Split((char)csvSeparator);

                    for (int j = 0; j < lineSplit.Length && j < nbChamp; j++)
                        fileCSV[i, j] = lineSplit[j];
                }

                return fileCSV;
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        public string SetFichier(object[,] donnees, CsvCharacterSeparator csvSeparator = CsvCharacterSeparator.semicolon)
        {
            StringBuilder texteFichier = new StringBuilder();
            bool finI = false;

            for (int i = 0; !finI; i++)
            {
                try
                {
                    string ligne = donnees[i, 0].ToString();
                    bool fin = false;

                    for (int j = 1; !fin; j++)
                    {
                        try
                        {
                            ligne += (char)csvSeparator + donnees[i, j].ToString();
                        }
                        catch
                        {
                            fin = true;
                        }
                    }

                    texteFichier.AppendLine(ligne);
                }
                catch
                {
                    finI = true;
                }
            }

            string uniqueFilePathName = Helpers.FileNameHelper.GetUniqueFullName(_fichier.FullName);

            File.WriteAllText(path: uniqueFilePathName
                            , contents: texteFichier.ToString()
                            , encoding: UTF8Encoding.UTF8);

            return uniqueFilePathName;
        }
    }
}