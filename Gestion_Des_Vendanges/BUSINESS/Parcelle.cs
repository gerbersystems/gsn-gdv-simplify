﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Données d'une parcelle
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class Parcelle
    {
        private int _id;
        private int _cepage;
        private int _acquit;
        private string _folio;
        private int _numero;
        private decimal _surfaceCepage;
        private decimal _surfaceComptee;
        private decimal _quota;
        private decimal _litres;
        private decimal _kilos;
        private int _autreMention;
        private int _lieuProduction;
        private string _secteurVisite;
        private string _modeCulture;
        private int _commune;

        //private int cru;
        //private int appellation;
        private int? _zoneTravail;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Cepage
        {
            get { return _cepage; }
            set { _cepage = value; }
        }

        public int Acquit
        {
            get { return _acquit; }
            set { _acquit = value; }
        }

        public string Folio
        {
            get { return _folio; }
            set { _folio = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public decimal SurfaceCepage
        {
            get { return Math.Round(_surfaceCepage, 2); }
            set { _surfaceCepage = Math.Round(value, 2); }
        }

        public decimal SurfaceComptee
        {
            get { return Math.Round(_surfaceComptee, 2); }
            set { _surfaceComptee = Math.Round(value, 2); }
        }

        public decimal Quota
        {
            get { return Math.Round(_quota, 2); }
            set { _quota = Math.Round(value, 2); }
        }

        public decimal Litres
        {
            get { return Math.Round(_litres, 2); }
            set { _litres = Math.Round(value, 2); }
        }

        public decimal Kilos
        {
            get { return Math.Round(_kilos, 2); }
            set { _kilos = Math.Round(value, 2); }
        }

        public int AutreMention
        {
            get { return _autreMention; }
            set { _autreMention = value; }
        }

        public int LieuProduction
        {
            get { return _lieuProduction; }
            set { _lieuProduction = value; }
        }

        //public int Appellation
        //{
        //    get { return appellation; }
        //    set { appellation = value; }
        //}
        //public int Cru
        //{
        //    get { return cru; }
        //    set { cru = value; }
        //}
        public int? ZoneTravail
        {
            get { return _zoneTravail; }
            set { _zoneTravail = value; }
        }

        public string SecteurVisite
        {
            get { return _secteurVisite; }
            set { _secteurVisite = value; }
        }

        public string ModeCulture
        {
            get { return _modeCulture; }
            set { _modeCulture = value; }
        }

        public int Commune
        {
            get { return _commune; }
            set { _commune = value; }
        }
    }
}