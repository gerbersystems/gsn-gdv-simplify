﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Données d'une adresse provenant de WinBiz
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    *  ATH - 06.11.2009 - Ajout de _idAdrGV
    *  ATH - 20.11.2009 - Ajout de _noTVA
    * */

    public class Adresse
    {
        private int _numero;
        private int _idAdrGV;
        private int _idProGV;
        private string _societe;
        private string _titre;
        private string _nom;
        private string _prenom;
        private string _rue1;
        private string _rue2;
        private string _npa;
        private string _ville;
        private string _pays;
        private string _telephone;
        private string _fax;
        private string _mobile;
        private string _email;
        private string _noTVA;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter _mocTable;
        private int? _modeTVA; //NULL Selon Article, 1 Sans TVA, 2 Inclus, 3 Exclu

        public int IdAdrGv
        {
            get { return _idAdrGV; }
            set { _idAdrGV = value; }
        }

        public int IdProGv
        {
            get { return _idProGV; }
            set { _idProGV = value; }
        }

        public int Numero
        {
            get { return _numero; }
        }

        public decimal NumeroDecimal
        {
            set { _numero = Convert.ToInt32(value); }
        }

        public string Societe
        {
            get { return _societe; }
            set { _societe = value.TrimEnd(); }
        }

        public string Titre
        {
            get { return _titre; }
            set { _titre = value.TrimEnd(); }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value.TrimEnd(); }
        }

        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value.TrimEnd(); }
        }

        public string Rue1
        {
            get { return _rue1; }
            set { _rue1 = value.TrimEnd(); }
        }

        public string Rue2
        {
            get { return _rue2; }
            set { _rue2 = value.TrimEnd(); }
        }

        public string Npa
        {
            get { return _npa; }
            set { _npa = value.TrimEnd(); }
        }

        public string Ville
        {
            get { return _ville; }
            set { _ville = value.TrimEnd(); }
        }

        public string Pays
        {
            get { return _pays; }
            set { _pays = value.TrimEnd(); }
        }

        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value.TrimEnd(); }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value.TrimEnd(); }
        }

        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value.TrimEnd(); }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value.TrimEnd(); }
        }

        public string NoTVA
        {
            get { return _noTVA; }
            set { _noTVA = value.Trim(); }
        }

        //private int _modeComptabilisation;

        //public int ModeComptabilisation
        //{
        //    get { return _modeComptabilisation; }
        //    set { _modeComptabilisation = value; }
        //}

        //public decimal ModeComptabilisationWinBIZ
        //{
        //    get { return Convert.ToDecimal(_mocTable.GetMocIdWinBiz(_modeComptabilisation)); }
        //    set { _modeComptabilisation = Convert.ToInt32(_mocTable.GetMocId(Convert.ToInt32(value), BUSINESS.Utilities.IdAnneeCourante)); }
        //}

        public int? ModeTVA
        {
            get { return _modeTVA; }
            set { _modeTVA = value; }
        }

        public Adresse()
        {
            _numero = -1;
            _mocTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter>();
        }

        //public Adresse(int numero, string societe, string titre, string nom, string prenom, string rue1, string rue2,
        //    string npa, string ville, string pays, string telephone, string fax, string mobile, string email, string noTVA)
        //{
        //    _numero = numero;
        //    _societe = societe;
        //    _titre = titre;
        //    _nom = nom;
        //    _prenom = prenom;
        //    _rue1 = rue1;
        //    _rue2 = rue2;
        //    _npa = npa;
        //    _ville = ville;
        //    _pays = pays;
        //    _telephone = telephone;
        //    _fax = fax;
        //    _mobile = mobile;
        //    _email = email;
        //    //_modeComptabilisation = modeComptabilisation;
        //    _noTVA = noTVA;
        //    _mocTable = new DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
        //}

        //public void SetModeTVA(bool avecTVA, bool inclus)
        //{
        //    if (!avecTVA)
        //        _modeTVA = 1;
        //    else if (inclus)
        //        _modeTVA = 2;
        //    else
        //        _modeTVA = 3;
        //}
    }
}