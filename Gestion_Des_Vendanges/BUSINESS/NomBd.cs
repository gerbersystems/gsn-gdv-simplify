﻿namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Nom et description d'une base de données
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    *  ATH - 18.11.2009 - Prise en compte de l'adresse de la base
    * */

    internal class NomBd
    {
        private string _description;
        private string _nom;
        private string _adresse;

        public string Description
        {
            get { return _description; }
        }

        public string Nom
        {
            get { return _nom; }
        }

        public string Adresse
        {
            get { return _adresse; }
        }

        public NomBd(string description, string nom)
        {
            _description = description;
            _nom = nom;
            _adresse = @"localhost\sqlexpress";
        }

        public NomBd(string description, string nom, string adresse)
        {
            _description = description;
            _nom = nom;
            _adresse = adresse;
        }

        public override string ToString()
        {
            return _description;
        }
    }
}