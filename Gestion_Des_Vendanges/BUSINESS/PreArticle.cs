﻿using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.BUSINESS
{
    public class PreArticle
    {
        private int _couId;
        private int _annId;
        private int? _cepId;
        private int? _lieId;
        private int? _claId;
        private int? _autId;
        private int? _comId;
        private int? _proId;

        private List<int> _pedIds;

        private double _litres;
        private double? _litresStock;

        public int? CepId
        {
            get { return _cepId; }
            set { _cepId = value; }
        }

        public int? LieId
        {
            get { return _lieId; }
            set { _lieId = value; }
        }

        public int? ClaId
        {
            get { return _claId; }
            set { _claId = value; }
        }

        public int? AutId
        {
            get { return _autId; }
            set { _autId = value; }
        }

        public int? ComId
        {
            get { return _comId; }
            set { _comId = value; }
        }

        public int? ProId
        {
            get { return _proId; }
            set { _proId = value; }
        }

        public int CouId
        {
            get { return _couId; }
        }

        public int AnnId
        {
            get { return _annId; }
        }

        public int[] PedIds
        {
            get { return _pedIds.ToArray(); }
        }

        public double Litres
        {
            get { return _litres; }
        }

        public double LitresStock
        {
            get
            {
                if (_litresStock != null)
                    return (double)_litresStock;
                else
                    return _litres;
            }
            set { _litresStock = value; }
        }

        public PreArticle(int annId, int couId, int pedId, double litres)
        {
            _annId = annId;
            _couId = couId;
            _pedIds = new List<int>();
            _pedIds.Add(pedId);
            _litres = litres;
        }

        public bool Ajouter(PreArticle article)
        {
            bool canGroup = (_annId == article.AnnId && _couId == article.CouId && _cepId == article.CepId &&
                _lieId == article.LieId && _claId == article.ClaId && _comId == article.ComId &&
                _proId == article.ProId);

            if (canGroup)
            {
                _litres += article._litres;
                _pedIds.AddRange(article._pedIds);
            }

            return canGroup;
        }

        public void Grouper(PreArticle article)
        {
            if (CouId != article.CouId)
                throw new ArgumentException("Les deux articles ne sont pas de la même couleur.");

            if (AnnId != article.AnnId)
                throw new ArgumentException("Les deux articles ne sont pas de la même année");

            if (CepId != article.CepId)
                _cepId = null;

            if (LieId != article.LieId)
                _lieId = null;

            if (ClaId != article.ClaId)
                _claId = null;

            if (AutId != article.AutId)
                _autId = null;

            if (ComId != article.ComId)
                _comId = null;

            if (ProId != article.ProId)
                _proId = null;

            _pedIds.AddRange(article._pedIds);
            _litres += article.Litres;
        }
    }
}