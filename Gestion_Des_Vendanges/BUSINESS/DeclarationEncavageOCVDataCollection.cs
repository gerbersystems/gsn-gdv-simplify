﻿using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Collection de ligne d'une déclaration d'encavage OCV
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class DeclarationEncavageOCVDataCollection : List<DeclarationEncavageOCVData>
    {
        public DeclarationEncavageOCVDataCollection()
        {
        }

        public void AddLigne(DAO.DSPesees.DEO_DECLARATION_ENCAVAGE_OCVRow row)
        {
            DeclarationEncavageOCVData deoLigne;
            if (!getLigne(out deoLigne, row.CEP_ID, row.LIE_ID, row.COM_ID))
            {
                deoLigne = new DeclarationEncavageOCVData(row.ANN_ENCAVEUR_NUMERO, row.ANN_ENCAVEUR_SOUMIS_CONTROL,
                    Count + 1, row.COM_NUMERO, row.COM_NOM, row.LIE_NUMERO, row.LIE_NOM, row.CEP_NUMERO,
                    row.CEP_NOM, row.COM_ID, row.LIE_ID, row.CEP_ID);
                Add(deoLigne);
            }
            //throw new NotImplementedException("ADD row.CLA_ID");
            deoLigne.AddLitres(row.CLA_NUMERO, row.CLA_ID, Convert.ToInt32(row.DROITPRODUCTION), Convert.ToInt32(row.ProductionAOC), Convert.ToInt32(row.ProductionGC), Convert.ToDecimal(row.OEAvgAOC), Convert.ToDecimal(row.OEAvgGC));
        }

        private bool getLigne(out DeclarationEncavageOCVData ligne, int cepId, int lieId, int comId)
        {
            bool find = false;
            ligne = null;
            for (int i = 0; i < Count && !find; i++)
            {
                if (this[i].IdCepage == cepId && this[i].IdLieuProduction == lieId && this[i].IdCommune == comId)
                {
                    ligne = this[i];
                    find = true;
                }
            }
            return find;
        }
    }
}