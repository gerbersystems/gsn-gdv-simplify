﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /*
*  Description: Donnees d'une ligne de détail d'un document WinBIZ
*  Date de création: 03.11.2009
*  Modifications:
* */

    internal class LigneDetailWinBIZ
    {
        //  private int _documentId;
        private decimal _quantite;

        private string _compteCharge;
        private decimal _tauxTVA;
        private decimal _tVAIncl;
        private string _descriptionArticle;
        private string _unite;
        private decimal _typeTVA;
        private decimal _montantHT;
        private decimal _montantTTC;
        private decimal _prixUnitaire;
        private decimal _mocIdWinBiz;
        private string _compteTVA;
        private decimal _bnTVA;
        private decimal _ptTVA;
        private decimal _arrTVA;

        public decimal Quantite
        {
            get
            {
                return Math.Round(_quantite, 2);
            }
            set
            {
                _quantite = value;
            }
        }

        public decimal PrixUnitaire
        {
            get
            {
                if (Quantite == 1)
                {
                    return Montant;
                }
                else
                {
                    return _prixUnitaire;
                }
            }
            set
            {
                _prixUnitaire = value;
            }
        }

        public decimal MontantHT
        {
            set
            {
                _montantHT = value;
            }
        }

        public decimal MontantTTC
        {
            set
            {
                _montantTTC = value;
            }
        }

        public string Unite
        {
            get
            {
                return _unite;
            }
            set
            {
                _unite = value;
            }
        }

        public decimal Montant
        {
            get
            {
                if (TVAIncl == 1)
                {
                    return _montantTTC;
                }
                else
                {
                    return _montantHT;
                }
            }
        }

        public string CompteCharge
        {
            get
            {
                return _compteCharge;
            }
            set
            {
                _compteCharge = value;
            }
        }

        public decimal TypeTVA
        {
            get
            {
                return _typeTVA;
            }
            set
            {
                _typeTVA = value;
            }
        }

        public bool TVAInclBool
        {
            set
            {
                if (value)
                {
                    _tVAIncl = 1;
                }
                else
                {
                    _tVAIncl = 2;
                }
            }
        }

        public decimal TVAIncl
        {
            get
            {
                return _tVAIncl;
            }
        }

        public decimal TauxTVA
        {
            get
            {
                return _tauxTVA;
            }
            set
            {
                _tauxTVA = value;
            }
        }

        public decimal MontantTVA
        {
            get
            {
                return _montantTTC - _montantHT;
            }
        }

        public string DescriptionArticle
        {
            get
            {
                return _descriptionArticle;
            }
            set
            {
                _descriptionArticle = value;
            }
        }

        public decimal MocIdWinBiz
        {
            get
            {
                return _mocIdWinBiz;
            }
            set
            {
                _mocIdWinBiz = value;
            }
        }

        public LigneDetailWinBIZ()
        {
            _unite = "";
        }

        public string CompteTVA
        {
            get
            {
                return _compteTVA;
            }
            set
            {
                _compteTVA = value;
            }
        }

        public decimal BnTVA
        {
            get
            {
                return _bnTVA;
            }
            set
            {
                _bnTVA = value;
            }
        }

        public decimal PtTVA
        {
            get
            {
                return _ptTVA;
            }
            set
            {
                _ptTVA = value;
            }
        }

        public decimal ArrTVA
        {
            get
            {
                return _arrTVA;
            }
            set
            {
                _arrTVA = value;
            }
        }
    }
}