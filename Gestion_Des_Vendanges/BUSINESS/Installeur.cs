﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSMasterTableAdapters;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using GSN.GDV.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class Installeur
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Program));

        private readonly string _pathFolderToImport;
        private BDVendangeControleur _bdVendangeControleur;
        private readonly string _pathSauvegardeMaster;
        private readonly string _pathSauvegardeGv;
        private readonly string _pathInstallCheckFile;
        private readonly string _pathConfig;
        private LIC_LICENSETableAdapter _licTable;

        private LIC_LICENSETableAdapter LicTable
        {
            get
            {
                if (_licTable == null)
                {
                    _licTable = new LIC_LICENSETableAdapter();
                    _licTable.Connection.ConnectionString = ConnectionManager.Instance.MasterConnectionString;
                }
                return _licTable;
            }
        }

        public Installeur()
        {
            _bdVendangeControleur = new BDVendangeControleur();
            _pathSauvegardeMaster = $"{Utilities.ApplicationPath}\\GV_MASTER_{Utilities.Version}.bak";
            _pathSauvegardeGv = $"{Utilities.ApplicationPath}\\GESTIONVENDANGES_{Utilities.Version}.bak";
            _pathInstallCheckFile = $"{ Utilities.ApplicationPath}\\install";
            _pathConfig = Directory.GetParent(Utilities.ApplicationPath).FullName;
            _pathFolderToImport = $"{Utilities.ApplicationPath}\\GDV_QUOTA_PRODUCTION";
        }

        public void CheckInstall()
        {
            if (FirstInstallation())
            {
                string path = GetPath();
                ConnectionManager.Init();
                string connectionString = ConnectionManager.Instance.MasterConnectionString.Replace(ConnectionManager.Instance.MasterDbName, "");

                BDMasterManager.CreateDataBase(connectionString, path);

                InsertDefaultLicenceRow(path);
                RemoveInstallFile();

                var datTable = new DAT_DATABASETableAdapter();
                datTable.Connection.ConnectionString = ConnectionManager.Instance.MasterConnectionString;

                AjouterBd($"GESTIONVENDANGES_{Utilities.Version}", Utilities.LicId);

                datTable.Insert(DAT_NOM: $"GESTIONVENDANGES_{Utilities.Version}",
                                DAT_DESCRIPTION: $"Gestion des Vendanges {Utilities.Version}");
            }
            else if (NeedMigrationGVBdNamesToGV_MASTER())
            {
                MigrerBd();
            }
        }

        private void InsertDefaultLicenceRow(string path)
        {
            LicTable.Insert(null, null, null, null, null, null, null, null, null, null, null, null, path);
        }

        private void RemoveInstallFile()
        {
            File.Delete(_pathInstallCheckFile);
        }

        private bool FirstInstallation() => File.Exists(_pathInstallCheckFile);

        private void ImportCSV(string quota)
        {
            //HACK | NOT IMPLEMENTED ImportCSV _JGA
            var dataToImport = from line in System.IO.File.ReadAllLines(quota)
                               let fields = line.Split(new char[] { ';' })
                               select new QuotationProductionModel
                               {
                                   CepageId = Int32.Parse(fields[0]),
                                   RegionId = Int32.Parse(fields[1]),
                                   Annee = Int32.Parse(fields[2]),
                                   AOC = decimal.Parse(fields[3]),
                                   PremierGrandCru = decimal.Parse(fields[4]),
                                   VinPays = decimal.Parse(fields[5]),
                                   VinTable = decimal.Parse(fields[6])
                               };
        }

        private void MigrerBd()
        {
            if (File.Exists(_pathConfig + "\\GVBdNames.Config"))
            {
                List<NomBd> bdNames = ReadGVBdNames();
                string connectionString = ConfigFileManager.DefaultValue
                    .Replace(@"localhost\sqlexpress", bdNames[0].Adresse);

                bool isDbMasterExist = BDMasterManager.IsDataBaseExist(connectionString);
                if (!isDbMasterExist)
                {
                    BDMasterManager.CreateDataBase(connectionString.Replace("GV_MASTER_" + Utilities.Version, ""));
                }

                bool ok = false;
                while (!ok)
                {
                    //Essaye d'ajouter la ligne plusieurs fois (ne fonctionne pas toujours du premier coup pour une raison inconnue!!)
                    try
                    {
                        ConfigFileManager.ServerAdress = connectionString;
                        ConnectionManager.Init();
                        if (!isDbMasterExist)
                        {
                            var datTable = new DAT_DATABASETableAdapter();
                            datTable.Connection.ConnectionString = ConnectionManager.Instance.MasterConnectionString;
                            foreach (NomBd nomBd in bdNames)
                            {
                                datTable.Insert(nomBd.Nom, nomBd.Description);
                            }
                        }
                        ok = true;
                    }
                    catch { }
                }
                BDMasterManager.RecuperationLicense("GV_MASTER_" + BUSINESS.Utilities.Version, bdNames[0].Nom);
                File.Delete(_pathConfig + "\\GVBdNames.Config");
            }
            else
            {
                ConfigFileManager.ServerAdress = ConfigFileManager.DefaultValue;
                ConnectionManager.Init();

                BDMasterManager.CreateDataBase(ConnectionManager.Instance.MasterConnectionString.Replace(ConnectionManager.Instance.MasterDbName, ""));
                bool ok = false;
                while (!ok)
                {
                    //Essaye d'ajouter la ligne plusieurs fois (ne fonctionne pas toujours du premier coup pour une raison inconnue!!)
                    try
                    {
                        var datTable = new DAT_DATABASETableAdapter();
                        datTable.Connection.ConnectionString = ConnectionManager.Instance.MasterConnectionString;
                        datTable.Insert(DAT_NOM: "GESTIONVENDANGES",
                                        DAT_DESCRIPTION: "GESTIONVENDANGES");
                        ok = true;
                    }
                    catch { }
                }
                BDMasterManager.RecuperationLicense(masterBdNom: $"GV_MASTER_{Utilities.Version}",
                                                    gvBdNom: "GESTIONVENDANGES");
            }
        }

        private List<NomBd> ReadGVBdNames()
        {
            List<NomBd> bdNames = new List<NomBd>();
            bdNames = new List<NomBd>();
            try
            {
                string[] lines = File.ReadAllLines(_pathConfig + "\\GVBdNames.config");
                foreach (string line in lines)
                {
                    try
                    {
                        if (line.IndexOf(':') > 0)
                        {
                            bdNames.Add(new NomBd(line.Split('=')[0], line.Split('=')[1].Split(':')[0], @line.Split(':')[1]));
                        }
                        else
                        {
                            bdNames.Add(new NomBd(line.Split('=')[0], line.Split('=')[1]));
                        }
                    }
                    catch (Exception) { }
                }
            }
            catch (Exception) { }
            return bdNames;
        }

        /// <summary>
        /// Vérifie si besoin de migrer du système GVBdNames au système GV_MASTER
        /// </summary>
        /// <returns></returns>
        private bool NeedMigrationGVBdNamesToGV_MASTER()
        {
            bool result;
            result = !ConfigFileManager.FileExists;
            if (result)
            {
                result = File.Exists(_pathConfig + "\\GVBdNames.config");
                if (!result)
                {
                    try
                    {
                        var pamTable = new PAM_PARAMETRESTableAdapter();
                        result = (int)pamTable.GetVersionBD() <= 16;
                    }
                    catch { }
                }
            }
            return result;
        }

        public void AjouterBd(string nom, int licID)
        {
            string path = LicTable.GetPathBD(Utilities.LicId);
            if (path == null)
            {
                path = "";
            }
            if (!Directory.Exists(path))
            {
                path = GetPath();
                LicTable.SetLicPathBd(path, licID);
            }
            _bdVendangeControleur.RestoreToNewDb(_pathSauvegardeGv, path, nom);
        }

        private string GetPath()
        {
            var folderDialog = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true
            };
            MessageBox.Show("Veuillez sélectionner le dossier d'installation de la base de données",
                            "Gestion des Vendanges",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            if (folderDialog.ShowDialog() != DialogResult.OK)
            {
                throw new InvalidOperationException("Vous avez annulé l'installation, le programme va se terminer.");
            }
            return folderDialog.SelectedPath;
        }
    }
}