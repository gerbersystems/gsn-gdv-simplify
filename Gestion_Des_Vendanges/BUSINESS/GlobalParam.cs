﻿using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Business;
using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Extensions;
using GSN.GDV.Data.Models;
using System.Linq;
using static GSN.GDV.Data.Extensions.AnneeExtension;
using static GSN.GDV.Data.Extensions.BonusExtension;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.BUSINESS
{
    public class GlobalParam : IGlobalParameterProvider
    {
        #region Fields

        private readonly MainDataContext _mainDataContext;
        private readonly ILimitationProductionController _limitationProductionController;
        private AnneeModel _currentAnnee;

        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(GlobalParam));

        #endregion Fields

        #region Properties

        public CantonEnum Canton
        {
            get => (CantonEnum)Get(_mainDataContext, SettingEnum.Canton, 0);
            set => Set(_mainDataContext, SettingEnum.Canton, (int)value, autoSave: true);
        }

        public ErpEnum Erp
        {
            get => (ErpEnum)Get(_mainDataContext, SettingEnum.Erp, 0);
            set => Set(_mainDataContext, SettingEnum.Erp, (int)value, autoSave: true);
        }

        /// <summary>
        /// Prevent or allow modify contacts where they are imported from erp (winbiz)
        /// </summary>
        public SyncContactsEnum SyncContacts
        {
            get => (SyncContactsEnum)Get(_mainDataContext, SettingEnum.SyncContacts, 0);
            set => Set(_mainDataContext, SettingEnum.SyncContacts, (int)value, autoSave: true);
        }

        /// <summary>
        /// Taux de conversion en litres/Kilos pour ROUGE et BLANC
        /// </summary>
        public decimal TauxConversionRouge
        {
            get => decimal.Round(_currentAnnee.TauxConversion.Rouge, 2);
            set
            {
                _currentAnnee.TauxConversion.Rouge = decimal.Round(value, 2);
                _mainDataContext.SaveChanges();
            }
        }

        public decimal TauxConversionBlanc
        {
            get => decimal.Round(_currentAnnee.TauxConversion.Blanc, 2);
            set
            {
                _currentAnnee.TauxConversion.Blanc = decimal.Round(value, 2);
                _mainDataContext.SaveChanges();
            }
        }

        public decimal? TauxConversionRougeMout
        {
            get
            {
                if (_currentAnnee.TauxConversionMout.RougeMout == null)
                {
                    TauxConversionRougeMout = TauxConversionRouge;
                    return TauxConversionRouge;
                }
                else
                {
                    return decimal.Round((decimal)_currentAnnee.TauxConversionMout.RougeMout, 2);
                }
            }
            set
            {
                if (value == null)
                    _currentAnnee.TauxConversionMout.RougeMout = null;
                else
                    _currentAnnee.TauxConversionMout.RougeMout = decimal.Round((decimal)value, 2); _mainDataContext.SaveChanges();
            }
        }

        public decimal? TauxConversionBlancMout
        {
            get
            {
                if (_currentAnnee.TauxConversionMout.BlancMout == null)
                {
                    TauxConversionBlancMout = TauxConversionBlanc;
                    return TauxConversionBlanc;
                }
                else
                {
                    return decimal.Round((decimal)_currentAnnee.TauxConversionMout.BlancMout, 2);
                }
            }
            set
            {
                if (value == null)
                    _currentAnnee.TauxConversionMout.BlancMout = null;
                else
                    _currentAnnee.TauxConversionMout.BlancMout = decimal.Round((decimal)value, 2); _mainDataContext.SaveChanges();
            }
        }

        /// <summary>
        /// Affichage des informations de logiciel en Oechsle ou en Bricks
        /// </summary>
        public virtual bool ShowSondageOechsle
        {
            get => Get(_mainDataContext, SettingEnum.ShowSondageOxle, true) == true;
            set => Set(_mainDataContext, SettingEnum.ShowSondageOxle, value, autoSave: true);
        }

        public virtual bool ShowSondageBrix
        {
            get => Get(_mainDataContext, SettingEnum.ShowSondageBrix, true) == true;
            set => Set(_mainDataContext, SettingEnum.ShowSondageBrix, value, autoSave: true);
        }

        /// <summary>
        /// Accepte ou non que les limites indiqués lors de la saisie des pesées puissent être dépassées ou non
        /// </summary>
        public bool AcceptExceedingQuota
        {
            get => Get(_mainDataContext, SettingEnum.AcceptExceedingQuota, false) == true;
            set => Set(_mainDataContext, SettingEnum.AcceptExceedingQuota, value, autoSave: true);
        }

        /// <summary>
        /// Type de facture a afficher
        /// </summary>
        public FactureTypeEnum FactureType
        {
            get => (FactureTypeEnum)Get(_mainDataContext, SettingEnum.FactureType, 0);
            set => Set(_mainDataContext, SettingEnum.FactureType, (int)value, autoSave: true);
        }

        /// <summary>
        /// Type de Bonus : pour chaque ligne de bonus le type peut être en pourcent (%) ou en monnaie
        /// </summary>
        public BonusTypeEnum BonusType
        {
            get => (BonusTypeEnum)Get(_mainDataContext, SettingEnum.BonusType, 0);
            set => Set(_mainDataContext, SettingEnum.BonusType, (int)value, autoSave: true);
        }

        /// <summary>
        /// L'unité du Bonus/Malus est la configuration générale du logiciel pour indiquer comment afficher l'écran des Bonus | Malus
        /// </summary>
        public BonusUniteEnum BonusUnite
        {
            get => (BonusUniteEnum)Get(_mainDataContext, SettingEnum.BonusUnite, 0);
            set => Set(_mainDataContext, SettingEnum.BonusUnite, (int)value, autoSave: true);
        }

        /// <summary>
        /// Indique si les écrans, tableau de l'application affiche le taux (degrés) de sucre dans le vins en Oechsle = 0 ou en Brix = 1
        /// </summary>
        public DegreValeur DegreValeur
        {
            get => (DegreValeur)Get(_mainDataContext, SettingEnum.DegreValeur, 0);
            set => Set(_mainDataContext, SettingEnum.DegreValeur, (int)value, autoSave: true);
        }

        /// <summary>
        /// Unité de fonctionnement du logiciel en Kilogrammes ou en litres
        /// </summary>
        public ApplicationUniteEnum ApplicationUnite
        {
            get => (ApplicationUniteEnum)Get(_mainDataContext, SettingEnum.ApplicationUnite, 0);
            set => Set(_mainDataContext, SettingEnum.ApplicationUnite, (int)value, autoSave: true);
        }

        public ILimitationProductionController LimitationProductionController => _limitationProductionController;

        public static ILimitationProductionController LimitationProduction => Instance.LimitationProductionController;

        #endregion Properties

        #region Constructors

        private GlobalParam()
        {
            Log.Info("Chargement des paramètres...");
            _mainDataContext = ConnectionManager.Instance.CreateMainDataContext();
            _limitationProductionController = new LimitationProductionController(_mainDataContext, this);
            var annId = Utilities.IdAnneeCourante;
            try
            {
                _currentAnnee = (from t in _mainDataContext.AnneeSet where t.Id == annId select t).First();
            }
            catch
            {
                _currentAnnee = (from t in _mainDataContext.AnneeSet select t).OrderByDescending(t => t.Id).First();
            }
        }

        private static object _lockerInstance = new object();

        private static GlobalParam _instance = null;

        public static IGlobalParameterProvider Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                Log.Info("Initialization...");
                lock (_lockerInstance)
                {
                    if (_instance != null)
                        return _instance;
                    _instance = new GlobalParam();
                }
                return _instance;
            }
            set { _instance = null; }
        }

        #endregion Constructors

        #region Methods

        public double ConvertToKilogramme(double v, int acquitId)
        {
            if (v == 0.0)
                return 0;

            var tauxConversion = GetTauxConversion(acquitId, Utilities.AnneeCourante);
            return v / ((double)tauxConversion);
        }

        public decimal GetTauxConversionByPeseeEntete(int peseeEnteteId) => GetTauxConversionByPeseeEntete(peseeEnteteId, Utilities.AnneeCourante);

        public decimal GetTauxConversionByPeseeEntete(int peseeEnteteId, int annee)
        {
            var couleur = PeseeEnteteExtension.GetCouleurVin(_mainDataContext, peseeEnteteId);
            return GetTauxConversion(couleur, annee);
        }

        public decimal GetTauxConversionByCepage(int cepageId, int annee)
        {
            var couleur = CepageExtension.GetCouleurVin(_mainDataContext, cepageId);
            return GetTauxConversion(couleur, annee);
        }

        public decimal GetTauxConversion(int acquitId, int annee)
        {
            var couleur = AcquitExtension.GetCouleurVin(_mainDataContext, acquitId);
            return GetTauxConversion(couleur, annee);
        }

        public decimal GetTauxConversion(AnneeCouleurVinEnum couleur) => GetTauxConversion(couleur, Utilities.AnneeCourante);

        public decimal GetTauxConversion(AnneeCouleurVinEnum couleur, int annee) => AnneeExtension.GetTauxConversion(_mainDataContext, annee, couleur);

        public bool CanSetCanton() => SettingExtension.CanSetCanton(_mainDataContext);

        #endregion Methods
    }
}