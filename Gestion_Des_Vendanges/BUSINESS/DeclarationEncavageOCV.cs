﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Gère les données d'un declaration d'encavage OCV (lecture de la base de données et formatage des données)
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class DeclarationEncavageOCVManager
    {
        private int _annId;
        private DeclarationEncavageOCVDataCollection _deoData;
        private DEO_DECLARATION_ENCAVAGE_OCVTableAdapter _deoTable;

        public DeclarationEncavageOCVManager(int annId)
        {
            _annId = annId;
            _deoTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DEO_DECLARATION_ENCAVAGE_OCVTableAdapter>();
            _deoData = new DeclarationEncavageOCVDataCollection();
        }

        public object[,] GetDonnees()
        {
            object[,] donneesObject;
            _deoTable.FillByAnnId(new DSPesees().DEO_DECLARATION_ENCAVAGE_OCV, _annId);
            foreach (DAO.DSPesees.DEO_DECLARATION_ENCAVAGE_OCVRow row in _deoTable.GetDataByAnnId(_annId))
                _deoData.AddLigne(row);

            //DeclarationEncavageOCVData[] donneesDEO = _deoData.GetData();
            donneesObject = new object[_deoData.Count + 1, DeclarationEncavageOCVData.NbValues];
            donneesObject = initEntetes(donneesObject);
            for (int i = 0; i < _deoData.Count; i++)
            {
                object[] values = _deoData[i].ConvertToObjectValues();
                for (int j = 0; j < values.Length; j++)
                    donneesObject[i + 1, j] = values[j];
            }
            return donneesObject;
        }

        private object[,] initEntetes(object[,] tableau)
        {
            int i = 0;
            tableau[0, i] = "ENCAV";
            tableau[0, ++i] = "FED";
            tableau[0, ++i] = "NO LIGNE";
            tableau[0, ++i] = "NUM COMMUNE";
            tableau[0, ++i] = "COMMUNE";
            tableau[0, ++i] = "NUM LIEU PROD";
            tableau[0, ++i] = "LIEU PROD";
            tableau[0, ++i] = "NUM CEPAGE";
            tableau[0, ++i] = "CEPAGE";
            tableau[0, ++i] = "DROIT CL 1";
            tableau[0, ++i] = "PROD 1 AOC";
            tableau[0, ++i] = "DEG 1 AOC";
            tableau[0, ++i] = "PROD 1 GC";
            tableau[0, ++i] = "DEG 1 GC";
            tableau[0, ++i] = "DROIT PREM GC";
            tableau[0, ++i] = "PROD PREM GC";
            tableau[0, ++i] = "DEG PREM GC";
            tableau[0, ++i] = "DROIT CL 2";
            tableau[0, ++i] = "PROD CL 2";
            tableau[0, ++i] = "DEG CL 2";
            tableau[0, ++i] = "PROD 3";
            tableau[0, ++i] = "DEG 3";
            tableau[0, ++i] = "RAISIN";
            tableau[0, ++i] = "INDUS";
            return tableau;
        }
    }
}