﻿using System;
using System.IO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal static class ConfigFileManager
    {
        private readonly static string _path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "GV_MASTER_" + Utilities.Version + ".conf");
        public readonly static string DefaultValue = @"Data Source=localhost\sqlexpress;Initial Catalog=GV_MASTER_" + Utilities.Version + ";Integrated Security=True";

        public static bool FileExists
        {
            get { return File.Exists(_path); }
        }

        public static string ServerAdress
        {
            get { return File.ReadAllText(_path); }
            set { File.WriteAllText(_path, value); }
        }
    }
}