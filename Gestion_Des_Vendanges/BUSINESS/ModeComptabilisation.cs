﻿namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class ModeComptabilisation
    {
        #region properties

        private int _idWinbiz;
        private string _texte;
        private ModeTVA _modeTVA;
        private int _annId;
        private double _tauxTVA;
        private int? _noCompteComptable;
        private string _debitCredit;

        public int IdWinbiz
        {
            get { return _idWinbiz; }
            set { _idWinbiz = value; }
        }

        public string Texte
        {
            get { return _texte; }
            set { _texte = value.TrimEnd(); }
        }

        public ModeTVA ModeTVA
        {
            get { return _modeTVA; }
            set { _modeTVA = value; }
        }

        public int ModId
        {
            get { return (int)_modeTVA; }
            set { _modeTVA = (ModeTVA)value; }
        }

        public int AnnId
        {
            get { return _annId; }
            set { _annId = value; }
        }

        public double TauxTVA
        {
            get { return _tauxTVA; }
            set { _tauxTVA = value; }
        }

        public int? NoCompteComptable
        {
            get => _noCompteComptable;
            set => _noCompteComptable = value;
        }

        public string DebitCredit
        {
            get => _debitCredit;
            set => _debitCredit = value;
        }

        #endregion properties

        public bool Compare(ModeComptabilisation valueToCompare)
        {
            return (valueToCompare.AnnId == AnnId && valueToCompare.ModeTVA == ModeTVA
                && valueToCompare.Texte == Texte && valueToCompare.TauxTVA == TauxTVA
                && valueToCompare.IdWinbiz == IdWinbiz);
        }
    }

    internal class ModeComptabilisationWinBiz : ModeComptabilisation
    {
        public decimal TypeTVA
        {
            get { return (ModeTVA == ModeTVA.Sans) ? 1 : 3; }
        }

        public decimal TVAIncl
        {
            get { return (ModeTVA == ModeTVA.Sans) ? 2 : 1; }
        }

        public void SetModeTVA(decimal typeTva, decimal tvaIncl)
        {
            if (typeTva == 2 || typeTva == 3)
                ModeTVA = (tvaIncl == 1) ? ModeTVA.Incluse : ModeTVA.Exclue;
            else
                ModeTVA = ModeTVA.Sans;
        }
    }
}