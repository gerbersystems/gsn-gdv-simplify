﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Converti à partir d'un article "Gestion des vendanges" un article "WinBIZ"
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class ArticleExport
    {
        private DAO.DSPesees.ViewWinBizExportArticleRow _articleRow;
        private decimal _unite;

        public string CodeArticle
        {
            get { return replaspecialChar(_articleRow.ART_CODE); }
        }

        public string DesignationCourte
        {
            get { return replaspecialChar(_articleRow.ART_DESIGNATION_COURTE); }
        }

        public string DesignationLongue
        {
            get { return replaspecialChar(_articleRow.ART_DESIGNATION_LONGUE); }
        }

        public string Groupe
        {
            get { return replaspecialChar(_articleRow.GROUPE).ToUpper(); }
        }

        public decimal Litres
        {
            get { return convertToDecimal(_articleRow.ART_LITRES_STOCK); }
        }

        public string CodeGroupe
        {
            get { return replaspecialChar(_articleRow.ART_CODE_GROUPE); }
        }

        public string Annee
        {
            get { return replaspecialChar(_articleRow.ANN_AN.ToString()); }
        }

        public decimal WinBizId
        {
            get { return convertToDecimal(_articleRow.ART_ID_WINBIZ); }
        }

        public decimal Unite
        {
            get { return _unite; }
            set { _unite = value; }
        }

        public decimal ModeComptabilisation
        {
            get { return convertToDecimal(_articleRow.MOC_ID_WINBIZ); }
        }

        public ArticleExport(DAO.DSPesees.ViewWinBizExportArticleRow articleRow)
        {
            _articleRow = articleRow;
            _unite = 1;
        }

        private string replaspecialChar(string champ)
        {
            string resultat = champ.Replace('\'', ' ');
            resultat = resultat.Replace('\n', ' ');
            resultat = resultat.Trim('\r');
            return resultat;
        }

        private decimal convertToDecimal(object value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch (OverflowException)
            {
                return decimal.MaxValue;
            }
        }
    }
}