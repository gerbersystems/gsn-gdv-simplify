﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /*
 *  Description: Donnees d'une ligne d'une déclaration d'encavage OCV
 *  Date de création:
 *  Modifications:
 *  ATH - 02.11.2009 - Application des conventions de programmation
 * */

    internal class DeclarationEncavageOCVData
    {
        public static int NbValues
        {
            get { return 24; }
        }

        private int _numEncaveur;
        private bool _controleSuisseCommerceVins;
        private int _numLigne;
        private int _numCommune;
        private string _nomCommune;
        private int _numLieuProduction;
        private string _nomLieuProduction;
        private int _numCepage;
        private string _nomCepage;
        private int _droitProductionClasse1;
        private int _productionClasse1AOC;
        private decimal _degOeClasse1AOC;
        private int _productionClasse1GC;
        private decimal _degOeClasse1GC;
        private int _droitProductionClasse2;
        private int _productionClasse2;
        private decimal _degOeClasse2;
        private int _productionClasse3;
        private decimal _degOeClasse3;
        private int _droitProductionClassePremierGC;
        private int _productionClassePremierGC;
        private decimal _degOeClassePremierGC;

        public int IdCommune { get; private set; }
        public int IdLieuProduction { get; private set; }
        public int IdCepage { get; private set; }

        public int NumEncaveur
        {
            get { return _numEncaveur % 100000; }
        }

        public char ControleSuisseCommerceVins
        {
            get
            {
                if (_controleSuisseCommerceVins)
                    return 'O';
                else
                    return 'N';
            }
        }

        public int NumLigne
        {
            get { return _numLigne % 10000; }
        }

        public int NumCommune
        {
            get { return _numCommune % 10000; }
        }

        public string NomCommune
        {
            get { return formatString(_nomCommune, 20); }
        }

        public int NumLieuProcution
        {
            get { return _numLieuProduction % 1000; }
        }

        public string NomLieuProdutcion
        {
            get { return formatString(_nomLieuProduction, 25); }
        }

        public int NumCepage
        {
            get { return _numCepage % 100000; }
        }

        public string NomCepage
        {
            get { return formatString(_nomCepage, 25); }
        }

        public int DroitProductionClasse1
        {
            get { return _droitProductionClasse1 % 100000000; }
        }

        public int DroitProductionClasse2
        {
            get { return _droitProductionClasse2 % 100000000; }
        }

        public int DroitProductionClassePremierGC
        {
            get { return _droitProductionClassePremierGC % 100000000; }
        }

        public int ProductionClassePremierGC
        {
            get { return _productionClassePremierGC % 100000000; }
        }

        public int ProductionClasse1AOC
        {
            get { return _productionClasse1AOC % 100000000; }
        }

        public int ProductionClasse1GC
        {
            get { return _productionClasse1GC % 100000000; }
        }

        public int ProductionClasse2
        {
            get { return _productionClasse2 % 100000000; }
        }

        public int ProductionClasse3
        {
            get { return _productionClasse3 % 100000000; }
        }

        public decimal DegOeCalsse1AOC
        {
            get { return formatDegOe(_degOeClasse1AOC); }
        }

        public decimal DegOeClasse1GC
        {
            get { return formatDegOe(_degOeClasse1GC); }
        }

        public decimal DegOeClassePremierGC
        {
            get { return formatDegOe(_degOeClassePremierGC); }
        }

        public decimal DegOeClasse2
        {
            get { return formatDegOe(_degOeClasse2); }
        }

        public decimal DegOeClasse3
        {
            get { return formatDegOe(_degOeClasse3); }
        }

        public DeclarationEncavageOCVData(int numEncaveur, bool controleSuisseCommerceVins, int numLigne, int numCommune,
            string nomCommune, int numLieuProduction, string nomLieuProduction, int numCepage, string nomCepage,
            int idCommune, int idLieuProduction, int idCepage)
        {
            _numEncaveur = numEncaveur;
            _controleSuisseCommerceVins = controleSuisseCommerceVins;
            _numLigne = numLigne;
            _numCommune = numCommune;
            _nomCommune = nomCommune;
            _numLieuProduction = numLieuProduction;
            _nomLieuProduction = nomLieuProduction;
            _numCepage = numCepage;
            _nomCepage = nomCepage;
            IdCepage = idCepage;
            IdCommune = idCommune;
            IdLieuProduction = idLieuProduction;
        }

        public void AddLitres(int numClasse, int claId, int droitProduction, int productionAOC, int productionGC,
            decimal degOEAoc, decimal degOeGC)
        {
            switch (numClasse)
            {
                case 1:
                    if (claId == 2)
                    {
                        _droitProductionClassePremierGC = droitProduction;
                        _productionClassePremierGC = productionAOC;
                        _degOeClassePremierGC = degOEAoc;
                    }
                    else
                    {
                        _droitProductionClasse1 = droitProduction;
                        _productionClasse1AOC = productionAOC;
                        _productionClasse1GC = productionGC;
                        _degOeClasse1AOC = degOEAoc;
                        _degOeClasse1GC = degOeGC;
                    }
                    break;

                case 2:
                    _droitProductionClasse2 = droitProduction;
                    _productionClasse2 = productionAOC;
                    _degOeClasse2 = degOEAoc;
                    break;

                case 3:
                    _productionClasse3 = productionAOC;
                    _degOeClasse3 = degOEAoc;
                    break;

                default:
                    break;
            }
        }

        private string formatString(string chaine, int nbChar)
        {
            if (chaine.Length > nbChar)
            {
                chaine = chaine.Remove(nbChar);
            }
            chaine = chaine.ToUpper();
            return Utilities.RemoveAccents(chaine);
        }

        private decimal formatDegOe(decimal degOe)
        {
            if (degOe >= 100)
            {
                return Math.Round(degOe, 1) % 1000;
            }
            else
            {
                return Math.Round(degOe, 2);
            }
        }

        public object[] ConvertToObjectValues()
        {
            object[] values = new object[NbValues];
            int i = 0;
            values[i] = NumEncaveur;
            values[++i] = ControleSuisseCommerceVins;
            values[++i] = NumLigne;
            values[++i] = NumCommune;
            values[++i] = NomCommune;
            values[++i] = NumLieuProcution;
            values[++i] = NomLieuProdutcion;
            values[++i] = NumCepage;
            values[++i] = NomCepage;
            values[++i] = DroitProductionClasse1;
            values[++i] = ProductionClasse1AOC;
            values[++i] = DegOeCalsse1AOC;
            values[++i] = ProductionClasse1GC;
            values[++i] = DegOeClasse1GC;
            values[++i] = DroitProductionClassePremierGC;
            values[++i] = ProductionClassePremierGC;
            values[++i] = DegOeClassePremierGC;
            values[++i] = DroitProductionClasse2;
            values[++i] = ProductionClasse2;
            values[++i] = DegOeClasse2;
            values[++i] = ProductionClasse3;
            values[++i] = DegOeClasse3;
            values[++i] = 0;
            values[++i] = 0;
            return values;
        }
    }
}