﻿using System;
using System.Data;
using System.IO;
using System.Windows;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class FichierExcel
    {
        private FileInfo _fichier;
        //private Microsoft.Office.Interop.Excel.DataTable excelSheet;

        private Microsoft.Office.Interop.Excel.Application excel;
        private Microsoft.Office.Interop.Excel.Workbook workBook;
        //private Microsoft.Office.Interop.Excel.Worksheet worKsheeT;
        //private Microsoft.Office.Interop.Excel.Range celLrangE;

        public FichierExcel(FileInfo fichier)
        {
            _fichier = fichier;
        }

        public FichierExcel(string fichier)
        {
            _fichier = new FileInfo(fichier);
        }

        public void GetFichier()
        {
        }

        public void SetFichier(object[,] donnees)
        {
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application()
                {
                    Visible = false,
                    DisplayAlerts = false
                };
                workBook = excel.Workbooks.Add(Type.Missing);

                //worKsheeT = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                //worKsheeT.Name = "StudentRepoertCard";

                //worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[1, 8]].Merge();
                //worKsheeT.Cells[1, 1] = "Student Report Card";
                //worKsheeT.Cells.Font.Size = 15;

                //int rowcount = 2;

                //        foreach (DataRow datarow in ExportToExcel().Rows)
                //        {
                //            rowcount += 1;
                //            for (int i = 1; i <= ExportToExcel().Columns.Count; i++)
                //            {
                //                if (rowcount == 3)
                //                {
                //                    worKsheeT.Cells[2, i] = ExportToExcel().Columns[i - 1].ColumnName;
                //                    worKsheeT.Cells.Font.Color = System.Drawing.Color.Black;
                //                }

                //                worKsheeT.Cells[rowcount, i] = datarow[i - 1].ToString();

                //                if (rowcount > 3)
                //                {
                //                    if (i == ExportToExcel().Columns.Count)
                //                    {
                //                        if (rowcount % 2 == 0)
                //                        {
                //                            celLrangE = worKsheeT.Range[worKsheeT.Cells[rowcount, 1], worKsheeT.Cells[rowcount, ExportToExcel().Columns.Count]];
                //                        }
                //                    }
                //                }
                //            }
                //        }

                //        celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[rowcount, ExportToExcel().Columns.Count]];
                //        celLrangE.EntireColumn.AutoFit();
                //        Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                //        border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                //        border.Weight = 2d;

                //        celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[2, ExportToExcel().Columns.Count]];

                //        worKbooK.SaveAs(Helpers.FileNameHelper.GetUniqueFullName(_fichier.FullName)); ;
                //        worKbooK.Close();
                //        excel.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //worKsheeT = null;
                //celLrangE = null;
                workBook = null;
            }
        }

        private System.Data.DataTable ExportToExcel()
        {
            return new DataTable();
        }
    }
}