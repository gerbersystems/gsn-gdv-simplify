﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Données d'un acquit
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class Acquit
    {
        private int _id;
        private int _classeId;
        private int _couleurId;
        private int _anneeId;
        private int _communeId;
        private int _proprietaireId;
        private int _numero;
        private DateTime _date;
        private decimal _surface;
        private decimal _litres;
        private int _regionId;
        private int _proprietaireVendangeId;
        private DateTime? _dateReception;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ClasseId
        {
            get { return _classeId; }
            set { _classeId = value; }
        }

        public int CouleurId
        {
            get { return _couleurId; }
            set { _couleurId = value; }
        }

        public int AnneeId
        {
            get { return _anneeId; }
            set { _anneeId = value; }
        }

        public int CommuneId
        {
            get { return _communeId; }
            set { _communeId = value; }
        }

        public int ProprietaireId
        {
            get { return _proprietaireId; }
            set { _proprietaireId = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public decimal Surface
        {
            get { return Math.Round(_surface, 2); }
            set { _surface = Math.Round(value, 2); }
        }

        public decimal Litres
        {
            get { return Math.Round(_litres, 2); }
            set { _litres = Math.Round(value, 2); }
        }

        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

        public int? NumeroComplementaire { get; set; }

        public int ProprietaireVendangeId
        {
            get { return _proprietaireVendangeId; }
            set { _proprietaireVendangeId = value; }
        }

        public DateTime? DateReception
        {
            get { return _dateReception; }
            set { _dateReception = value; }
        }

        public Acquit()
        {
        }

        public Acquit(int id, int classeId, int couleurId, int anneeId, int communeId, int regionId, int proprietaireId, int proprietaireVendangeId, int numero, decimal surface, decimal litres)
        {
            Id = id;
            ClasseId = classeId;
            CouleurId = couleurId;
            CommuneId = communeId;
            RegionId = regionId;
            AnneeId = anneeId;
            ProprietaireId = proprietaireId;
            ProprietaireVendangeId = proprietaireVendangeId;
            Numero = numero;
            Surface = surface;
            Litres = litres;
        }
    }
}