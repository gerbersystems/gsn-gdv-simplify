﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.IO;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.BUSINESS
{
    internal class SauvegardeManager
    {
        private BDVendangeControleur _bdControleur;
        private readonly string _tmpPath;// = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tmp.bak");

        public SauvegardeManager()
        {
            _bdControleur = new Gestion_Des_Vendanges.DAO.BDVendangeControleur();
            string databasePath = new System.IO.FileInfo(
                ConnectionManager.Instance.DatTable.GetDatabasePath(ConnectionManager.Instance.GvDBName)
                .ToString())
                .Directory
                .FullName;
            _tmpPath = Path.Combine(databasePath, "tmp.bak");
        }

        public void Sauvegarder()
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Backup (*.bak)|*.bak";
            saveDialog.AddExtension = true;
            saveDialog.DefaultExt = "bak";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                string path = saveDialog.FileName;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    if (File.Exists(_tmpPath))
                        File.Delete(_tmpPath);

                    _bdControleur.Sauvegarder(_tmpPath);
                    File.Move(_tmpPath, path);
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Base de données sauvegardée avec succès!", "Sauvegarde", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception e)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Erreur, la base de données n'a pas pu être sauvegardée.\n" + e.Message, "Erreur sauvegarde", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    try
                    {
                        if (File.Exists(_tmpPath))
                            File.Delete(_tmpPath);
                    }
                    catch
                    {
                    }
                }
            }
        }

        public bool Restaurer()
        {
            bool resultat = false;
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Backup (*.bak)|*.bak";
            openDialog.DefaultExt = "bak";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                string path = openDialog.FileName;
                if (MessageBox.Show("Attention, la restauration supprimera définitivement les données existantes!\nEtes-vous sûr de vouloir continuer?", "Restauration", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        if (File.Exists(_tmpPath))
                            File.Delete(_tmpPath);

                        File.Copy(path, _tmpPath);
                        _bdControleur.Restaurer(_tmpPath);
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Base de données restaurée avec succès!", "Restauration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        resultat = true;
                    }
                    catch (Exception e)
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Erreur, la base de données n'a pas pu être restaurée.\n" + e.Message, "Erreur restauration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        try
                        {
                            File.Delete(_tmpPath);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return resultat;
        }
    }
}