﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Propriétés et méthodes static disponible pour toutes les classes
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    *  ATH - 02.11.2009 - Réécriture de BdName et ConnexionString
    *  ATH - 10.11.2009 - Ajout de FormatStringToSQL et FormatDateToFoxPro
    *  ATH - 18.11.2009 - Ajout de ServerName
    *  ATH - 19.11.2009 - Ajout PamId
    *  JGA - 12.06.2014 - Ajout PamKgLt
    * */

    public static partial class Utilities
    {
        private static string _pathDb;
        private static int? _licenseNum = null;
        private static int _anneeCourante = -1;
        private static int _idAnneeCourante = -1;
        private static int _licId = -1;
        private static int _idProducteurByPesee = -1;
        private static string _applicationPath = Environment.CurrentDirectory;
        private static PAM_PARAMETRESTableAdapter _pamTable;
        private static ANN_ANNEETableAdapter _annTable;
        private static Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters.R_AttestationControleProprietaire _producteurByPesee;
        private static DAO.DSMasterTableAdapters.LIC_LICENSETableAdapter _mlicTable;

        private static PAM_PARAMETRESTableAdapter _PamTable
        {
            get
            {
                if (_pamTable == null) _pamTable = ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();

                return _pamTable;
            }
        }

        private static Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters.R_AttestationControleProprietaire _ProducteurByPesee
        {
            get
            {
                if (_producteurByPesee == null)
                {
                    _producteurByPesee = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters.R_AttestationControleProprietaire>();
                }

                return _producteurByPesee;
            }
        }

        private static DAO.DSMasterTableAdapters.LIC_LICENSETableAdapter _MlicTable
        {
            get
            {
                if (_mlicTable == null)
                    _mlicTable = new DAO.DSMasterTableAdapters.LIC_LICENSETableAdapter();

                _mlicTable.Connection.ConnectionString = DAO.ConnectionManager.Instance.MasterConnectionString;

                return _mlicTable;
            }
        }

        private static ANN_ANNEETableAdapter _AnnTable
        {
            get
            {
                if (_annTable == null)
                    _annTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ANN_ANNEETableAdapter>();

                return _annTable;
            }
        }

        public static string ApplicationPath
        {
            get { return _applicationPath; }
        }

        public static string DbPath
        {
            get
            {
                _pathDb = _MlicTable.GetPathBD(LicId);
                return _pathDb;
            }
        }

        public static int? LicenseNum
        {
            get
            {
                if (_licenseNum == null)
                    _licenseNum = Convert.ToInt32(_MlicTable.GetLastLicense(LicId));

                return _licenseNum;
            }
            set
            {
                _licenseNum = value;

                if (_licenseNum >= 0)
                    _MlicTable.SetLastLicense(value, LicId);
            }
        }

        public static int AnneeCourante
        {
            get
            {
                if (_anneeCourante < 0)
                    SetAnneeCourante();

                return Utilities._anneeCourante;
            }
        }

        public static List<int> AnneeIds
        {
            get
            {
                List<int> anneeIds = new List<int>();
                DSPesees dsPesees = new Gestion_Des_Vendanges.DAO.DSPesees();

                _AnnTable.Fill(dsPesees.ANN_ANNEE);

                foreach (DSPesees.ANN_ANNEERow annRow in _AnnTable.GetData())
                    anneeIds.Add(annRow.ANN_ID);

                return anneeIds;
            }
        }

        public static int IdProducteurByEntete(int IdPesee)
        {
            Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters.R_AttestationControleProprietaire producteurByEnteteTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters.R_AttestationControleProprietaire>();

            if (_idProducteurByPesee < 0)
            {
                try
                {
                    _idProducteurByPesee = (int)producteurByEnteteTable.GetProIDByPEEID(IdPesee);
                }
                catch
                {
                    _idProducteurByPesee = 0;
                }
            }

            return _idProducteurByPesee;
        }

        public static int IdAnneeCourante
        {
            get
            {
                if (_idAnneeCourante < 0)
                {
                    try
                    {
                        _idAnneeCourante = (int)_AnnTable.GetAnnIdByAnnee((int)_MlicTable.GetLastAnnee(LicId));
                    }
                    catch
                    {
                        _idAnneeCourante = 0;
                    }
                }

                return _idAnneeCourante;
            }
            set
            {
                _MlicTable.SetLastAnnee((int?)_AnnTable.GetAnneeByAnnId(value), LicId);
                _idAnneeCourante = value;
                SetAnneeCourante();
            }
        }

        private static void SetAnneeCourante()
        {
            if (Utilities.IdAnneeCourante < 1) return;

            _anneeCourante = (int)_AnnTable.GetAnneeByAnnId(Utilities.IdAnneeCourante);
        }

        public static bool GetOption(int numOption)
        {
            int puissanceMod = Convert.ToInt32(Math.Pow(2, Convert.ToDouble(numOption)));
            int resMaxMod = puissanceMod / 2;
            return (_licenseNum % puissanceMod >= resMaxMod);
        }

        public static string RemoveAccents(string chaine)
        {
            chaine = chaine.Normalize(NormalizationForm.FormD);
            string res = "";

            foreach (char c in chaine)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    res += c;
            }

            return res;
        }

        public static string FormatStringToSQL(string chaine)
        {
            if (!string.IsNullOrEmpty(chaine))
            {
                chaine = chaine.Replace('\'', ' ');
                chaine = chaine.Replace('\"', ' ');
            }

            return chaine;
        }

        public static string FormatDateToFoxPro(DateTime date)
        {
            return ("Date(" + date.Year + ", " + date.Month + ", " + date.Day + ")");
        }

        public static int GetAnnee(int annId)
        {
            try
            {
                return (int)_AnnTable.GetAnneeByAnnId(annId);
            }
            catch (NullReferenceException)
            {
                return -1;
            }
        }

        public static string GetLieuProduction(int lieId)
        {
            LIE_LIEU_DE_PRODUCTIONTableAdapter lieTable = ConnectionManager.Instance.CreateGvTableAdapter<LIE_LIEU_DE_PRODUCTIONTableAdapter>();

            return (string)lieTable.GetLieNomByLieId(lieId);
        }

        public static string GetCepage(int cepId)
        {
            CEP_CEPAGETableAdapter cepTable = ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();

            return cepTable.GetNomById(cepId);
        }

        public static string GetAutreMention(int autId)
        {
            AUT_AUTRE_MENTIONTableAdapter autTable = ConnectionManager.Instance.CreateGvTableAdapter<AUT_AUTRE_MENTIONTableAdapter>();

            return autTable.GetAutNomByAutId(autId);
        }

        public static int LicId
        {
            get
            {
                if (_licId != -1)
                    return _licId;

                int? licIdTmp = _MlicTable.GetLicIdByNomMachine(Environment.MachineName);

                if (licIdTmp == null)
                {
                    if (Convert.ToBoolean(_MlicTable.IsFirstMachineInitalised()))
                        _MlicTable.AddMachine();

                    _MlicTable.SetNomMachine(Environment.MachineName);
                    licIdTmp = _MlicTable.GetLicIdByNomMachine(Environment.MachineName);
                }

                return Convert.ToInt32(licIdTmp);
            }
        }

        public static decimal ConvertToDecimal(object o)
        {
            try
            {
                return Convert.ToDecimal(o);
            }
            catch
            {
                return 0;
            }
        }

        public static bool IsPrixKg
        {
            get { return (_AnnTable.GetIsPrixKgByAnnId(IdAnneeCourante) == true); }
        }

        public static void WriteErrorLog(string text)
        {
            File.AppendAllText($"{ ApplicationPath}\\error.log", text);
        }
    }
}