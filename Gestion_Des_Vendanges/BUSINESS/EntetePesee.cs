﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    using Helpers;

    /* *
    *  Description: Donnees d'une entete de pesee
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class EntetePesee
    {
        public int Id { get; set; }

        public int? CepageId { get; set; }

        public int AcquitId { get; set; }

        public int ResponsableId { get; set; } = int.Parse(SettingsService.UserSettings.ResponsableId);

        public DateTime Date { get; set; }

        public string Lieu { get; set; } = SettingsService.UserSettings.PlaceOfDelivery;

        public bool GrandCru { get; set; }

        public int LieuProductionId { get; set; }

        public int AutreMentionId { get; set; }

        public string Remark { get; set; }

        public string Divers1 { get; set; } = SettingsService.UserSettings.RefractometerNb;

        public string Divers2 { get; set; } = SettingsService.UserSettings.ScaleNb;

        public string Divers3 { get; set; }

        public override string ToString()
        {
            var v = this;
            return string.Format(@"{0}{{
                Id:{1},
                CepageId:{2},
                AcquitId:{3},
                ResponsableId:{4},
                Date:{5},
                Lieu:{6},
                GrandCru:{7},
                LieuProductionId:{8},
                AutreMentionId:{9},
            }}", GetType().Name, Id, CepageId, AcquitId, ResponsableId, Date, Lieu, GrandCru, LieuProductionId, AutreMentionId);
        }
    }
}