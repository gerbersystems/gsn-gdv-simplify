﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Données d'une pesée
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class Pesee
    {
        private EntetePesee _entete;
        private int _id;
        private int _cepageId;
        private int _lot;
        private decimal _sondageOE;
        private decimal _sondageBriks;
        private decimal _quantiteKg;
        private decimal _quantiteLitres;
        private int _lieuProduction;

        public EntetePesee Entete
        {
            get { return _entete; }
            set { _entete = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int CepageId
        {
            get { return _cepageId; }
            set { _cepageId = value; }
        }

        public int Lot
        {
            get { return _lot; }
            set { _lot = value; }
        }

        public decimal SondageOE
        {
            get { return Math.Round(_sondageOE, 2); }
            set { _sondageOE = Math.Round(value, 2); }
        }

        public decimal SondageBriks
        {
            get { return Math.Round(_sondageBriks, 2); }
            set { _sondageBriks = Math.Round(value, 2); }
        }

        public decimal QuantiteKg
        {
            get { return Math.Round(_quantiteKg, 2); }
            set { _quantiteKg = Math.Round(value, 2); }
        }

        public decimal QuantiteLitres
        {
            get { return Math.Round(_quantiteLitres, 2); }
            set { _quantiteLitres = Math.Round(value, 2); }
        }

        public int LieuProduction
        {
            get { return _lieuProduction; }
            set { _lieuProduction = value; }
        }

        public override string ToString()
        {
            var v = this;
            return string.Format(@"{0}{{
                v.Id:{1},
                v.CepageId:{2},
                v.Lot:{3},
                v.QuantiteKg:{4},
                v.QuantiteLitres:{5},
                v.SondageBriks:{6},
                v.SondageOE:{7},
                v.Entete:{8},
            }}", GetType().Name, v.Id, v.CepageId, v.Lot, v.QuantiteKg, v.QuantiteLitres, v.SondageBriks, v.SondageOE, v.Entete);
        }
    }
}