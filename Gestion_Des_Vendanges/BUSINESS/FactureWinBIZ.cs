﻿using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Donnees d'un document WinBIZ
    *  Date de création: 03.11.2009
    *  Modifications:
    *  ATH - 09.11.2009 - Ajout de DocumentID
    *  ATH - 10.11.2009 - Remplacement de l'interface IDetailWinBIZ par LigneDetailWinBIZ
    * */

    internal class FactureWinBIZ
    {
        private int _paiId;
        private decimal _documentID;
        private int _adrIdWinBIZ;
        private int _proId;
        private string _refFacture;
        private List<LigneDetailWinBIZ> _lignesDetails;
        private int _adrBanqueId;
        private string _compteFournisseur;
        private int _nbJourEcheance;
        private DateTime _dateDocument;
        private string _compteCCP;
        private string _compteBanque;
        private string _clearing;
        private string _iban;
        private string _swift;

        //private DateTime _dateEcheance;
        //private int _typeTVA;
        //private int _bnTVA;
        //private int _ptTVA;
        //private int _arrTVA;
        //private string _compteTVA;
        //private string _motifVersement;
        private bool _isInserted;

        private bool _isPmtBanque
        {
            get { return (_compteCCP == null); }
        }

        private bool _isPmtCCP
        {
            get { return (_compteBanque == null && _iban == null && _swift == null); }
        }

        public string _texteFacture;

        public int PaiId
        {
            get { return _paiId; }
            set { _paiId = value; }
        }

        public decimal DocumentID
        {
            get { return _documentID; }
            set { _documentID = value; }
        }

        public decimal TypeDocument
        {
            get { return 44; }
        }

        public decimal NbJourEcheance
        {
            get { return _nbJourEcheance; }
            set { _nbJourEcheance = Convert.ToInt32(value); }
        }

        public DateTime DateDocument
        {
            get { return _dateDocument; }
            set { _dateDocument = value; }
        }

        public DateTime DateEcheance
        {
            get { return _dateDocument.AddDays(_nbJourEcheance); }
        }

        public decimal AdrIdWinBIZ
        {
            get { return _adrIdWinBIZ; }
            set { _adrIdWinBIZ = Convert.ToInt32(value); }
        }

        public string RefFacture
        {
            get { return _refFacture; }
            set { _refFacture = value; }
        }

        public string TexteFacture
        {
            get { return _texteFacture; }
            set { _texteFacture = value; }
        }

        public decimal MontantTVA
        {
            get
            {
                decimal montantTVA = 0;

                foreach (LigneDetailWinBIZ ligneDetail in _lignesDetails)
                    montantTVA += ligneDetail.MontantTVA;

                return montantTVA;
            }
        }

        public decimal MontantBrut
        {
            get
            {
                decimal montantNet = 0;

                foreach (LigneDetailWinBIZ ligneDetail in _lignesDetails)
                {
                    if (ligneDetail.TVAIncl == 2)
                        montantNet += ligneDetail.Montant + ligneDetail.MontantTVA;
                    else
                        montantNet += ligneDetail.Montant;
                }

                return montantNet;
            }
        }

        public decimal MontantNet
        {
            get
            {
                decimal montantBrut = 0;

                foreach (LigneDetailWinBIZ ligneDetail in _lignesDetails)
                    montantBrut += ligneDetail.Montant;

                return montantBrut;
            }
        }

        public decimal AdrBanqueId
        {
            get { return _adrBanqueId; }
            set { _adrBanqueId = Convert.ToInt32(value); }
        }

        public string CompteFournisseur
        {
            get { return _compteFournisseur; }
            set { _compteFournisseur = value; }
        }

        public LigneDetailWinBIZ[] LignesDetails
        {
            get { return _lignesDetails.ToArray(); }
        }

        public int ProId
        {
            get { return _proId; }
            set { _proId = value; }
        }

        public string CompteCCP
        {
            get { return _compteCCP; }
            set
            {
                if (_isPmtCCP)
                    _compteCCP = value;
            }
        }

        public string CompteBanque
        {
            get { return _compteBanque; }
            set
            {
                if (_isPmtBanque)
                    _compteBanque = value;
            }
        }

        public string Clearing
        {
            get { return _clearing; }
            set
            {
                if (_isPmtBanque)
                    _clearing = value;
            }
        }

        public string Iban
        {
            get { return _iban; }
            set
            {
                if (_isPmtBanque)
                    _iban = value;
            }
        }

        public string Swift
        {
            get { return _swift; }
            set
            {
                if (_isPmtBanque)
                    _swift = value;
            }
        }

        public decimal Taxe
        {
            get { return (_isPmtBanque) ? 2 : 0; }
        }

        public decimal Urgent
        {
            get { return (_isPmtBanque) ? 1 : 0; }
        }

        public decimal PaiementType
        {
            get
            {
                if (_isPmtBanque)
                    return 11;
                else if (_isPmtCCP)
                    return 1;
                else
                    return 6;
            }
        }

        public bool HasPaiementMode
        {
            get { return !(_isPmtBanque && _isPmtCCP); }
        }

        public decimal BanPay
        {
            get { return (_isPmtBanque) ? 1 : 2; }
        }

        public bool IsInserted
        {
            get { return _isInserted; }
            set { _isInserted = value; }
        }

        public FactureWinBIZ()
        {
            _lignesDetails = new List<LigneDetailWinBIZ>();
        }

        public void AddLigneDetail(LigneDetailWinBIZ ligneDetail)
        {
            //if (ligneDetail.TauxTVA == 0 && ligneDetail.TVAIncl == 0 && ligneDetail.TypeTVA == 0)
            //{
            //    ligneDetail.TauxTVA = _tauxTva;
            //    ligneDetail.TVAIncl = _tvaIncl;
            //    ligneDetail.TypeTVA = _typeTVA;
            //}
            _lignesDetails.Add(ligneDetail);
        }
    }
}