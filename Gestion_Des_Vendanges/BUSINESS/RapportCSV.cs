﻿using System.IO;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /*
*  Description: Gère les rapports au format CSV
*  Date de création:
*  Modifications:
     *  ATH - 16.11.09 - Déplacement de la méthode SetFichier dans la classe FichierCSV
* */

    internal class RapportCSV
    {
        public void SetDeclarationEncavageOCV(int annId)
        {
            DeclarationEncavageOCVManager decOCV = new DeclarationEncavageOCVManager(annId);
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.FileName = "Decla.csv";
            saveDialog.Filter = "Fichier CSV (*.csv)|*.csv";
            saveDialog.AddExtension = true;
            saveDialog.DefaultExt = "csv";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                FileInfo fichier = new FileInfo(saveDialog.FileName);
                FichierCSV csv = new FichierCSV(fichier);
                try
                {
                    csv.SetFichier(decOCV.GetDonnees());
                    MessageBox.Show("Déclaration exportée avec succès.", "Déclaration d'encavage OCV", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (System.IO.IOException)
                {
                    MessageBox.Show("Impossible d'enregistrer le fichier à cet emplacement.", "Enregistrement impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}