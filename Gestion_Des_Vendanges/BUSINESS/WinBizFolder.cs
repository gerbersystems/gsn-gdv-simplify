﻿using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.IO;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Gere et verifie le chemin du dossier WinBIZ
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class WinBizFolder
    {
        private string _current;
        private PAM_PARAMETRESTableAdapter _pamTable;

        public string Defaut
        {
            get { return Convert.ToString(_pamTable.GetWinBizAdresseDefaut()); }
        }

        public string Current
        {
            get { return _current; }
        }

        public int[] Annees
        {
            get
            {
                List<int> annees = new List<int>();
                try
                {
                    if (string.IsNullOrEmpty(_current))
                    {
                        throw new GVException("Aucune année disponible");
                    }
                    else
                    {
                        DirectoryInfo currentDirectory = new DirectoryInfo(_current);
                        foreach (DirectoryInfo dir in currentDirectory.GetDirectories())
                        {
                            int an;
                            if (int.TryParse(dir.Name, out an))
                            {
                                if (an > 1900 && an < 2100)
                                    annees.Add(an);
                            }
                        }
                    }
                }
                catch (DirectoryNotFoundException) { }
                return annees.ToArray();
            }
        }

        public WinBizFolder()
        {
            _pamTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();
            BDCurrent();
        }

        public string SetWinBizFolder(string path)
        {
            string[] folders = path.Split('\\');
            if (path != "")
            {
                if (folders[folders.Length - 1] == BUSINESS.Utilities.AnneeCourante.ToString())
                {
                    path = folders[0];
                    for (int i = 1; i < folders.Length - 1; i++)
                    {
                        path += "\\" + folders[i];
                    }
                }
                else if (!System.IO.Directory.Exists(@path + @"\" + BUSINESS.Utilities.AnneeCourante))
                {
                    throw new System.IO.DirectoryNotFoundException();
                }
            }
            _current = path;
            return _current;
        }

        public void SaveCurrentFolder()
        {
            _pamTable.SetWinBIZAdresseCurrent(_current);
        }

        public string BDCurrent()
        {
            _current = Convert.ToString(_pamTable.GetWinBIZAdresseCurrent());
            if (!Directory.Exists(_current))
            {
                _current = Defaut;
            }
            return _current;
        }
    }
}