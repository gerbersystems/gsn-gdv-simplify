﻿using System;

namespace Gestion_Des_Vendanges.BUSINESS
{
    /* *
    *  Description: Donnees d'un Bonus
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public class Bonus
    {
        private int _id;
        private int _valeurCepageId;
        private decimal _oeDebut;
        private decimal _oeFin;
        private decimal _bonus;
        private int _bonusType;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int ValeurCepageId
        {
            get { return _valeurCepageId; }
            set { _valeurCepageId = value; }
        }

        public decimal OeDebut
        {
            get { return Math.Round(_oeDebut, 2); }
            set { _oeDebut = value; }
        }

        public decimal OeFin
        {
            get { return Math.Round(_oeFin, 2); }
            set { _oeFin = value; }
        }

        public decimal ValeurBonus
        {
            get { return _bonus; }
            set { _bonus = value; }
        }

        public int BonusType
        {
            get { return _bonusType; }
            set { _bonusType = value; }
        }
    }
}