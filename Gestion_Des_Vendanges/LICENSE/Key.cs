﻿using System;

namespace Gestion_Des_Vendanges
{
    /*
*  Description: Formate les clés
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    internal class Key
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                char[] cToTrim = new char[2];
                cToTrim[0] = ' ';
                cToTrim[1] = '-';
                _value = value.Trim(cToTrim);
            }
        }

        public string[] SplitValue
        {
            get
            {
                try
                {
                    int reste = 0;
                    int i;
                    if (_value.Length % 8 != 0)
                        reste = 1;

                    string[] splitValue = new string[_value.Length / 8 + reste];
                    for (i = 0; i < splitValue.Length - reste; i++)
                    {
                        splitValue[i] = _value.Substring(i * 8, 8);
                    }
                    if (reste == 1)
                        splitValue[i] = _value.Substring(i * 8, _value.Length % 8);

                    return splitValue;
                }
                catch (NullReferenceException)
                {
                    return null;
                }
            }
            set
            {
                _value = "";
                foreach (string partKey in value)
                {
                    _value += partKey;
                }
            }
        }

        public Key(string value)
        {
            _value = value;
        }

        public Key(string[] value)
        {
            this.SplitValue = value;
        }

        public override string ToString()
        {
            try
            {
                string res = SplitValue[0];
                for (int i = 1; i < SplitValue.Length; i++)
                {
                    res += "-" + SplitValue[i];
                }
                return res;
            }
            catch (NullReferenceException)
            {
                return "";
            }
        }
    }
}