﻿using Gestion_Des_Vendanges.GV_KeyManager;

namespace Gestion_Des_Vendanges.LICENSE
{
    /* *
    *  Description: Classe proxy du service de gestion des licences
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class ProxyService
    {
        private KeyServiceUserClient _proxy;

        public ProxyService()
        {
            _proxy = new KeyServiceUserClient("BasicHttpBinding_IKeyServiceUser");
        }

        public License GetLicense(string key, string hashMachine) => _proxy.GetLicense(key, hashMachine);
    }
}