﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSMasterTableAdapters;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using Gestion_Des_Vendanges.GUI;
using Gestion_Des_Vendanges.GV_KeyManager;
using System;
using System.Deployment.Application;
using System.Management;
using System.Reflection;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.LICENSE
{
    /* *
    *  Description: Gestion et vérification des licences
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal static class LicenseManager
    {
        #region properties

        private static bool _quitApplication = false;
        private static string _hashMachine;
        private static string _cle;
        private static bool _needUpdate = false;
        private static LIC_LICENSETableAdapter _licTable;

        private static LIC_LICENSETableAdapter LicTable
        {
            get
            {
                if (_licTable == null)
                {
                    _licTable = new LIC_LICENSETableAdapter();
                    _licTable.Connection.ConnectionString = ConnectionManager.Instance.MasterConnectionString;
                }
                return _licTable;
            }
        }

        public static bool NeedUpdate
        {
            get { return _needUpdate; }
        }

        public static bool QuitApplication
        {
            get { return _quitApplication; }
            set { if (value) _quitApplication = true; }
        }

        #endregion properties

        public static bool CheckLicense()
        {
            bool serviceConnection = true;
            var proxy = new ProxyService();

            new BDVendangeControleur().UpdateBD();
            _cle = Convert.ToString(LicTable.GetCle(Utilities.LicId));

            if (string.IsNullOrEmpty(_cle))
            {
                //si la clé n'est pas saisie démarre la procédure de saisie
                SaisieCle();
            }
            else
            {
                // 1. Contrôle que la license soit toujours installée sur la même machine
                // TODO | Extraire une méthode checkLicenceCorrespondComputer()
                string saveHashMachine = Convert.ToString(LicTable.GetHashMachine(Utilities.LicId));
                _hashMachine = GetMachineCode();
                _hashMachine = GetMd5Hash(_hashMachine);

                //compare le code machine de la base de données et le code machine
                if (saveHashMachine != _hashMachine && saveHashMachine != string.Empty && saveHashMachine != null)
                {
                    MessageBox.Show("Erreur, votre code machine a changé.\nVeuillez contacter Gerber Systems & Network.",
                                    "Erreur",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    QuitApplication = true;
                }
                else
                {
                    try
                    {
                        //Essaye de récupérer les informations de license en se connectant au service
                        License license = proxy.GetLicense(_cle, _hashMachine);
                        if (license == null)
                        {
                            MessageBox.Show("Erreur, votre clé n'est pas valide.\nVeuillez saisir à nouveau votre clé d'activation\nSi le problème persiste, veuillez contacter Gerber Systems & Networks",
                                            "Gestion des vendanges",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                            SaisieCle();
                        }
                        else if (!license.Actif)
                        {
                            MessageBox.Show("Erreur, votre clé a été désactivée.\nVeuillez contacter Gerber Systems & Network pour plus de renseignements", "" +
                                            "Gestion des vendanges",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                            SaisieCle();
                        }
                        else if (license.IsExpire)
                        {
                            MessageBox.Show("Erreur, votre license est expirée.\nVeuillez contacter Gerber Systems & Network pour prolonger votre license",
                                            "Gestion des vendanges",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                            LicTable.SetCodeExpiration(null, Utilities.LicId);
                            _quitApplication = true;
                        }
                        else
                        {
                            SetDateExpiration(license.DateExpiration);
                            //Met à jour le numéro de client (utilisé pour construire le numéro sur les attestations de contrôle)
                            LicTable.SetNumEncaveur(license.CliId, Utilities.LicId);
                            if (!license.IsUpdateExpire)
                            {
                                //Si la license de mise à jour n'a pas expiré, le programme vérifie les mise à jour
                                SetDateExpirationUpdate(license.DateUpdateExpiration);
                                CheckUpdate();
                                SetHashVersion();
                                Utilities.LicenseNum = license.NiveauLicense;
                            }
                            else
                            {
                                MessageBox.Show($"Votre license de mise à jour est expirée depuis le {license.DateUpdateExpiration.ToShortDateString()}.\nContactez Gerber Systems & Networks pour prolonger votre license",
                                                "Gestion des vendanges",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Exclamation);
                                //vérification de la version du programme...
                                string saveHashVersion = Convert.ToString(LicTable.GetHashVersion(Utilities.LicId));
                                string hashVersion = GetHashVersion();
                                if (saveHashVersion != hashVersion)
                                {
                                    MessageBox.Show("La version du logiciel est incorrect.\n" +
                                                    "Veuillez contacter Gerber Systems & Network.",
                                                    "Gestion des vendanges",
                                                    MessageBoxButtons.OK,
                                                    MessageBoxIcon.Exclamation);
                                    QuitApplication = true;
                                }
                                else
                                {
                                    Utilities.LicenseNum = license.NiveauLicense;
                                }
                            }
                            if (saveHashMachine == "" || saveHashMachine == null)
                            {
                                LicTable.SetHashMachine(_hashMachine, Utilities.LicId);
                            }
                        }
                    }
                    catch (ProtocolException)
                    {
                        serviceConnection = false;
                    }
                    catch (EndpointNotFoundException)
                    {
                        serviceConnection = false;
                    }
                    if (!serviceConnection)
                    {
                        //Si le service ne peut pas être atteint la procédure de vérification de license locale est lancée
                        var hasTable = ConnectionManager.Instance.CreateGvTableAdapter<HAS_HASHTableAdapter>();
                        //Vérification de la clé avec la dernière license utilisée
                        string hashCle = GetMd5Hash(_cle + "GSN" + Utilities.LicenseNum);
                        if ((int)hasTable.GetNbByHash(hashCle) == 1)
                        {
                            if (saveHashMachine == "" || saveHashMachine == null) ActivationLocale();
                        }
                        else
                        {
                            //Vérification avec les autres licenses
                            Utilities.LicenseNum = -1;
                            for (int i = 0; i < 255 && Utilities.LicenseNum < 0; i++)
                            {
                                hashCle = GetMd5Hash(_cle + "GSN" + i);
                                if ((int)hasTable.GetNbByHash(hashCle) == 1)
                                {
                                    Utilities.LicenseNum = i;
                                    if (saveHashMachine == "" || saveHashMachine == null) ActivationLocale();
                                }
                            }
                        }
                        if (Utilities.LicenseNum < 0)
                        {
                            MessageBox.Show("Erreur, votre clé n'est pas valide.\nVeuillez saisir à nouveau votre clé d'activation\nSi le problème persiste, veuillez contacter Gerber Systems and Networks",
                                            "Gestion des vendanges",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                            SaisieCle();
                        }
                        else
                        {
                            //Vérification de la date d'expiration
                            try
                            {
                                DateTime date = GetDateExpiration(_hashMachine);
                                if (date < DateTime.Today && date != new DateTime(1900, 1, 1))
                                {
                                    LicTable.SetCodeExpiration(null, Utilities.LicId);
                                    throw new Exception();
                                }
                            }
                            catch
                            {
                                DialogResult result = MessageBox.Show("Erreur, votre license est expirée.\nVoulez-vous saisir une nouvelle clé?",
                                                                      "Gestion des vendanges",
                                                                      MessageBoxButtons.YesNo,
                                                                      MessageBoxIcon.Question);

                                if (result == DialogResult.Yes) SaisieCle();
                                else _quitApplication = true;
                            }
                        }
                        //vérification de la version du programme...
                        if (GetDateExpirationUpdate(_hashMachine) < DateTime.Today)
                        {
                            string saveHashVersion = Convert.ToString(LicTable.GetHashVersion(Utilities.LicId));
                            string hashVersion = GetHashVersion();
                            if (saveHashVersion != hashVersion)
                            {
                                MessageBox.Show("La version du logiciel est incorrect.\n" +
                                                "Veuillez contacter Gerber Systems & Network.",
                                                "Gestion des vendanges",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Exclamation);
                                QuitApplication = true;
                            }
                        }
                        else
                        {
                            SetHashVersion();
                        }
                    }
                }
            }
            return !_quitApplication;
            // return true;
        }

        public static void UpdateCle()
        {
            string key = Convert.ToString(LicTable.GetCle(Utilities.LicId));
            _quitApplication = (new WfrmKeyManager(key).ShowDialog() == DialogResult.Cancel);
            if (!_quitApplication)
            {
                LicTable.SetHashMachine("", Utilities.LicId);
                DialogResult result = MessageBox.Show("Clé mise à jour!\nLe programme doit redémarrer pour prendre en compte les nouveaux paramètres.\nVoulez-vous redémarrer maintenant?",
                                                      "Clé mise à jour",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);

                if (result == DialogResult.Yes) Application.Restart();
            }
        }

        private static string GetHashVersion()
        {
            string machineCode = GetMachineCode();
            string applicationVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return GetMd5Hash(machineCode + applicationVersion);
        }

        public static void SetHashVersion()
        {
            string hashVersion = GetHashVersion();
            if (Convert.ToString(LicTable.GetHashVersion(Utilities.LicId)) != hashVersion) LicTable.SetHashVersion(hashVersion, Utilities.LicId);
        }

        private static void SaisieCle()
        {
            //  PAM_PARAMETRESTableAdapter pamTable = new PAM_PARAMETRESTableAdapter();
            var keyForm = new WfrmKeyManager();
            _quitApplication = keyForm.ShowDialog() == DialogResult.Cancel;
            if (!_quitApplication)
            {
                LicTable.SetHashMachine(null, Utilities.LicId);
                CheckLicense();
            }
        }

        private static void ActivationLocale()
        {
            MessageBox.Show("Connexion au service d'activation impossible.\nVeuillez vous assurez de posséder une connexion internet active.",
                            "Gestion des vendanges",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
            _quitApplication = true;
        }

        private static void CheckUpdate()
        {
            UpdateCheckInfo info = null;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                try
                {
                    info = ad.CheckForDetailedUpdate();
                }
                catch (DeploymentDownloadException dde)
                {
                    MessageBox.Show($"The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: {dde.Message}");
                    return;
                }
                catch (InvalidDeploymentException ide)
                {
                    MessageBox.Show($"Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: {ide.Message}");
                    return;
                }
                catch (InvalidOperationException ioe)
                {
                    MessageBox.Show($"This application cannot be updated. It is likely not a ClickOnce application. Error: {ioe.Message}");
                    return;
                }

                if (info.UpdateAvailable)
                {
                    try
                    {
                        var pamTable = ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();
                        if (Convert.ToInt32(pamTable.GetVersionBD()) > BDVendangeUpdateManager.CurrentVersion)
                        {
                            DoUpdateForced();
                        }
                        else
                        {
                            DialogResult result = MessageBox.Show("Une mise à jour est disponible.\nVoulez-vous la télécharger?", "Nouvelle version", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (result == DialogResult.Yes) { _needUpdate = true; }
                        }
                    }
                    catch (DeploymentDownloadException dde)
                    {
                        MessageBox.Show($"Cannot install the latest version of the application. \n\nPlease check your network connection, or try again later. Error: {dde}");
                        return;
                    }
                }
            }
        }

        public static void DoUpdateForced()
        {
            var ad = ApplicationDeployment.CurrentDeployment;
            MessageBox.Show("Une mise à jour est disponible, elle va être téléchargée.",
                            "Nouvelle version",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            var message = new WfrmMessage("Mise à jour en cours. Veuillez patienter...", "Mise à jour");
            ad.Update();
            message.Close();
            _quitApplication = true;
            Application.Restart();
        }

        public static void DoUpdate()
        {
            var ad = ApplicationDeployment.CurrentDeployment;
            ad.Update();
            DialogResult dialogResult = MessageBox.Show("La mise à jour a été téléchargée.\n"
                                                        + "L'application doit être redémarrée pour prendre en compte les modifications.",
                                                        "Mise à jour",
                                                        MessageBoxButtons.OK,
                                                        MessageBoxIcon.Information);
        }

        private static string GetMachineCode()
        {
            string code = string.Empty;
            ManagementObjectCollection mbsList = null;
            var managementObjectSearcher = new ManagementObjectSearcher("Select * From Win32_BaseBoard");
            mbsList = managementObjectSearcher.Get();
            foreach (ManagementObject managementObject in mbsList)
            {
                try
                {
                    code += managementObject["SerialNumber"].ToString();
                }
                catch { }
            }

            managementObjectSearcher = new ManagementObjectSearcher("Select * From Win32_OperatingSystem");
            mbsList = managementObjectSearcher.Get();
            foreach (ManagementObject managementObject in mbsList)
            {
                code += managementObject["SerialNumber"].ToString();
                code += managementObject["InstallDate"].ToString();
            }
            return code;
        }

        private static string GetMd5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            HashAlgorithm md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new String builder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static bool CheckCle(string key)
        {
            string hashCle;
            bool result = false;
            var hasTable = ConnectionManager.Instance.CreateGvTableAdapter<HAS_HASHTableAdapter>();
            for (int i = 0; i < 255 && !result; i++)
            {
                hashCle = GetMd5Hash(key + "GSN" + i);
                if ((int)hasTable.GetNbByHash(hashCle) == 1) result = true;
            }
            return result;
        }

        public static void SetCodeHashMachine(Key codeActivation)
        {
            LicTable.SetNumEncaveur(int.Parse(codeActivation.Value.Substring(16, codeActivation.Value.Length - 16)), Utilities.LicId);
            string hashMachine = GetMachineCode();
            hashMachine = GetMd5Hash(hashMachine);
            LicTable.SetHashMachine(hashMachine, Utilities.LicId);
        }

        private static DateTime GetDateExpiration(string hashMachine) => DateTime.Parse(Decrypt(LicTable.GetExpiration(Utilities.LicId), hashMachine));

        private static DateTime GetDateExpirationUpdate(string hashMachine)
        {
            try
            {
                return DateTime.Parse(Decrypt((byte[])LicTable.GetExpirationUpdate(Utilities.LicId), hashMachine));
            }
            catch
            {
                return new DateTime(1900, 01, 01);
            }
        }

        private static void SetDateExpirationUpdate(DateTime dateExpiration)
        {
            LicTable.SetExpirationUpdate(Encrypt(dateExpiration.ToShortDateString()), Utilities.LicId);
        }

        public static void SetDateExpiration(DateTime dateExpiration)
        {
            LicTable.SetCodeExpiration(Encrypt(dateExpiration.ToShortDateString()), Utilities.LicId);
        }

        private static byte[] Encrypt(string valeur)
        {
            var algorithm = SymmetricAlgorithm.Create();
            object temp = algorithm.BlockSize;
            algorithm.Key = Encoding.Default.GetBytes(_hashMachine);
            algorithm.IV = Encoding.Default.GetBytes(_hashMachine.Substring(8, 16));
            ICryptoTransform encryptor = algorithm.CreateEncryptor();
            byte[] dateByte = Encoding.Default.GetBytes(valeur);
            return encryptor.TransformFinalBlock(dateByte, 0, dateByte.Length);
        }

        private static string Decrypt(byte[] valeur, string hashMachine)
        {
            var algorithm = SymmetricAlgorithm.Create();
            algorithm.Key = Encoding.Default.GetBytes(hashMachine);
            algorithm.IV = Encoding.Default.GetBytes(hashMachine.Substring(8, 16));
            ICryptoTransform descrambler = algorithm.CreateDecryptor();
            byte[] dateByte = descrambler.TransformFinalBlock(valeur, 0, valeur.Length);
            return Encoding.Default.GetString(dateByte, 0, dateByte.Length);
        }

        public static Key GetCodeActivation(DateTime dateExpiration, string hashMachineServer, int nbProducteur)
        {
            string codeActivation = $"{hashMachineServer}GSN{_cle}GSE{dateExpiration.ToShortDateString()}ATH{nbProducteur}";
            codeActivation = GetMd5Hash(codeActivation);
            return new Key(codeActivation.Substring(8, 16));
        }
    }
}