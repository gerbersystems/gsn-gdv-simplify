﻿using GSN.GDV.Data.Contextes;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesBonusTablePrix : CtrlWfrm, ICtrlSetting
    {
        private CtrlGestionDesBonusTablePrix()
        {
            InitializeComponent();
            Text = "Gestion des bonus";

            //for test developpement
            test();
        }

        public static CtrlGestionDesBonusTablePrix GetInstance()
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                return new CtrlGestionDesBonusTablePrix();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas la license pour utiliser cette option.\n" +
                    "Cette option est uniquement disponible avec la version étendue.", "License", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return null;
            }
        }

        public bool CanDelete
        {
            get { return true; }
        }

        public override ToolStrip ToolStrip
        {
            get { return toolStrip1; }
        }

        public override Panel OptionPanel
        {
            get { return panel2; }
        }

        //Only for test purpose to implement in a datagrid
        public void test()
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                MainDataContext mdc = DAO.ConnectionManager.Instance.CreateMainDataContext();
                var a = GSN.GDV.Data.Extensions.OechsleMoyenParCepageExtension.ListOechsleMoyenParCepage(me: mdc, AnnId: BUSINESS.Utilities.IdAnneeCourante);
                a.ToArray();

                this.OechsleMoyenParCepageDataGridView.DataSource = a;
            }
        }

        private void btnCopierLie_Click(object sender, EventArgs e)
        {
        }

        private void btnApercu_Click(object sender, EventArgs e)
        {
        }
    }
}