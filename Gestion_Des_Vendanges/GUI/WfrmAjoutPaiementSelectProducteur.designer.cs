﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiementSelectProducteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pRO_PROPRIETAIREBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRO_PROPRIETAIRETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aDR_ADRESSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter();
            this.adresseCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter();
            this.pRO_PROPRIETAIREDataGridView = new System.Windows.Forms.DataGridView();
            this.PRO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_NOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.adresseCompletBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aDRADRESSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSuivant = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnPrecedent = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRO_PROPRIETAIREBindingSource
            // 
            this.pRO_PROPRIETAIREBindingSource.DataMember = "PRO_PROPRIETAIRE";
            this.pRO_PROPRIETAIREBindingSource.DataSource = this.dSPesees;
            // 
            // pRO_PROPRIETAIRETableAdapter
            // 
            this.pRO_PROPRIETAIRETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = this.aDR_ADRESSETableAdapter;
            this.tableAdapterManager.AdresseCompletTableAdapter = this.adresseCompletTableAdapter;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = this.pRO_PROPRIETAIRETableAdapter;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aDR_ADRESSETableAdapter
            // 
            this.aDR_ADRESSETableAdapter.ClearBeforeFill = true;
            // 
            // adresseCompletTableAdapter
            // 
            this.adresseCompletTableAdapter.ClearBeforeFill = true;
            // 
            // pRO_PROPRIETAIREDataGridView
            // 
            this.pRO_PROPRIETAIREDataGridView.AllowUserToAddRows = false;
            this.pRO_PROPRIETAIREDataGridView.AllowUserToDeleteRows = false;
            this.pRO_PROPRIETAIREDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pRO_PROPRIETAIREDataGridView.AutoGenerateColumns = false;
            this.pRO_PROPRIETAIREDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pRO_PROPRIETAIREDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pRO_PROPRIETAIREDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PRO_ID,
            this.dataGridViewTextBoxColumn5,
            this.SelectColumn,
            this.dataGridViewTextBoxColumn6,
            this.PRO_NOM,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn2});
            this.pRO_PROPRIETAIREDataGridView.DataSource = this.pRO_PROPRIETAIREBindingSource;
            this.pRO_PROPRIETAIREDataGridView.Location = new System.Drawing.Point(12, 32);
            this.pRO_PROPRIETAIREDataGridView.Name = "pRO_PROPRIETAIREDataGridView";
            this.pRO_PROPRIETAIREDataGridView.Size = new System.Drawing.Size(701, 359);
            this.pRO_PROPRIETAIREDataGridView.TabIndex = 0;
            this.pRO_PROPRIETAIREDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PRO_PROPRIETAIREDataGridView_ColumnHeaderMouseClick);
            // 
            // PRO_ID
            // 
            this.PRO_ID.DataPropertyName = "PRO_ID";
            this.PRO_ID.HeaderText = "PRO_ID";
            this.PRO_ID.Name = "PRO_ID";
            this.PRO_ID.ReadOnly = true;
            this.PRO_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PRO_TITRE";
            this.dataGridViewTextBoxColumn5.HeaderText = "PRO_TITRE";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // SelectColumn
            // 
            this.SelectColumn.HeaderText = "";
            this.SelectColumn.MinimumWidth = 45;
            this.SelectColumn.Name = "Select";
            this.SelectColumn.Width = 45;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PRO_SCTE";
            this.dataGridViewTextBoxColumn6.HeaderText = "Société";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // PRO_NOM
            // 
            this.PRO_NOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_NOM.DataPropertyName = "PRO_NOM";
            this.PRO_NOM.HeaderText = "Nom";
            this.PRO_NOM.MinimumWidth = 100;
            this.PRO_NOM.Name = "PRO_NOM";
            this.PRO_NOM.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PRO_PRENOM";
            this.dataGridViewTextBoxColumn4.HeaderText = "Prénom";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ADR_ID";
            this.dataGridViewTextBoxColumn2.DataSource = this.adresseCompletBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "ADR_COMPLET";
            this.dataGridViewTextBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn2.FillWeight = 110F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Adresse";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "ADR_ID";
            // 
            // adresseCompletBindingSource
            // 
            this.adresseCompletBindingSource.DataMember = "AdresseComplet";
            this.adresseCompletBindingSource.DataSource = this.dSPesees;
            // 
            // aDRADRESSEBindingSource
            // 
            this.aDRADRESSEBindingSource.DataMember = "ADR_ADRESSE";
            this.aDRADRESSEBindingSource.DataSource = this.dSPesees;
            // 
            // btnSuivant
            // 
            this.btnSuivant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuivant.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSuivant.Location = new System.Drawing.Point(562, 397);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(75, 30);
            this.btnSuivant.TabIndex = 1;
            this.btnSuivant.Text = "Suivant";
            this.btnSuivant.UseVisualStyleBackColor = false;
            this.btnSuivant.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sélection des producteurs";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(643, 397);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 3;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.BtnAnnuler_Click);
            // 
            // btnPrecedent
            // 
            this.btnPrecedent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrecedent.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPrecedent.Location = new System.Drawing.Point(481, 397);
            this.btnPrecedent.Name = "btnPrecedent";
            this.btnPrecedent.Size = new System.Drawing.Size(75, 30);
            this.btnPrecedent.TabIndex = 4;
            this.btnPrecedent.Text = "Précédent";
            this.btnPrecedent.UseVisualStyleBackColor = false;
            this.btnPrecedent.Click += new System.EventHandler(this.BtnPrecedent_Click);
            // 
            // WfrmAjoutPaiementSelectProducteur
            // 
            this.AcceptButton = this.btnSuivant;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 438);
            this.Controls.Add(this.btnPrecedent);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSuivant);
            this.Controls.Add(this.pRO_PROPRIETAIREDataGridView);
            this.MaximumSize = new System.Drawing.Size(746, 477);
            this.MinimumSize = new System.Drawing.Size(746, 477);
            this.Name = "WfrmAjoutPaiementSelectProducteur";
            this.Text = "Gestion des paiement - Selection des producteurs";
            this.Load += new System.EventHandler(this.WfrmAjoutPaiementSelectProprietaire_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pRO_PROPRIETAIREBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter pRO_PROPRIETAIRETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView pRO_PROPRIETAIREDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter aDR_ADRESSETableAdapter;
        private System.Windows.Forms.BindingSource aDRADRESSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter adresseCompletTableAdapter;
        private System.Windows.Forms.BindingSource adresseCompletBindingSource;
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_NOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnPrecedent;
    }
}