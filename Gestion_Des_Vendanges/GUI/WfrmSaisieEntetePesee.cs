﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Data.Extensions;
using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.GUI.Models;
using GSN.GDV.Helpers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    using Helpers;

    /* *
    *  Description: Formulaire de saisie des entêtes de pesées
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 05.11.2009 - Modification de updateEtatCombobox: change l’état de autCombBox et lieComboBox
    *   ATH - 13.11.2009 - Modification du lien avec WfrmSelectAcquit;
    * */

    partial class WfrmSaisieEntetePesee : WfrmSetting
    {
        private readonly CtrlCockpit _cockpit;

        #region Properties

        private readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(WfrmSaisieEntetePesee));
        private bool _newEntete;
        private bool _fromCockpit;
        private EntetePesee _entetePesee;
        private readonly CtrlGestionDesPesees _formPesees;
        private bool _loadingAcquitInProgress = true;
        private bool _isDatabaseConnectionReady;
        private bool _limitationAlreadyRetrieved;
        private readonly List<Control> _lstControlsToHideNeuchatel;

        //private BackgroundWorker updateTotauxLitresBackgroundWorker = new BackgroundWorker();
        private readonly SettingExtension.ApplicationUniteEnum _applicationUnite;

        private int? _cepId;

        #endregion Properties

        #region Constructor

        public WfrmSaisieEntetePesee()
        {
            InitializeComponent();

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                _applicationUnite = GlobalParam.Instance.ApplicationUnite;
            }

            pEE_LIEUTextBox.Validated += pEE_LIEUTextBox_Validated;
            pEE_LIEUTextBox.TextChanged += pEE_LIEUTextBox_TextChanged;

            _lstControlsToHideNeuchatel = new List<Control>
            {
                pEE_GRANDCRULabel, pEE_GRANDCRUCheckBox,
                lIE_IDLabel, lIE_IDComboBox, transfertButton
            };
        }

        // Constructor when create new acquit
        public WfrmSaisieEntetePesee(CtrlGestionDesPesees formPesees) : this()
        {
            _formPesees = formPesees;
            _newEntete = true;

            if (_entetePesee == null)
            {
                _entetePesee = new EntetePesee();
            }

            pEE_GRANDCRUCheckBox.Enabled = true;
            pEE_GRANDCRUCheckBox.Checked = false;
        }

        // Constructor when modify existing acquit
        public WfrmSaisieEntetePesee(CtrlGestionDesPesees formPesees, EntetePesee entetePesee) : this()
        {
            _formPesees = formPesees;
            _entetePesee = entetePesee;
            _newEntete = false;
        }

        // Constructor when modify existing acquit
        public WfrmSaisieEntetePesee(EntetePesee entetePesee, CtrlCockpit cockpit) : this()
        {
            _formPesees = CtrlGestionDesPesees.GetCurrentInstance(false);
            _cockpit = cockpit;
            _newEntete = false;
            _entetePesee = entetePesee;
        }

        public int? CepageId { get; }

        //Constructor when adding weighing from cockpit
        public WfrmSaisieEntetePesee(int acquitId, int? cepageId, DateTime date, bool grandCru, int autreMentionId, int lieuProductionId, CtrlCockpit cockpit) : this()
        {
            CepageId = cepageId;
            _formPesees = CtrlGestionDesPesees.GetCurrentInstance(false);
            _cockpit = cockpit;

            if (_entetePesee == null) _entetePesee = new EntetePesee();
            _entetePesee.AcquitId = acquitId;

            _entetePesee.CepageId = cepageId;
            _entetePesee.Date = date;
            _entetePesee.GrandCru = grandCru;
            _entetePesee.AutreMentionId = autreMentionId;
            _entetePesee.LieuProductionId = lieuProductionId;

            _newEntete = true;
            _fromCockpit = true;
            _cepId = _entetePesee.CepageId;
        }

        #endregion Constructor

        #region OnLoad

        private void OnLoad(object sender, EventArgs e)
        {
            transfertButton.Visible = SettingsService.UserSettings.Rights > 0;
            toolLieuProduction.Visible = SettingsService.UserSettings.Rights > 0;
            toolNomLocal.Visible = SettingsService.UserSettings.Rights > 0;
            ResponsablesToolStripMenuItem.Visible = SettingsService.UserSettings.Rights > 0;
            switch (BUSINESS.GlobalParam.Instance.Canton)
            {
                case SettingExtension.CantonEnum.Neuchâtel:
                    HideControls(_lstControlsToHideNeuchatel);
                    aCQ_IDLabel.Text = "N° Exploitant";
                    using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                    {
                        //HACK LIE | ConnectionManager.Instance.AddGvTableAdapter(pEC_PED_COMTableAdapter);
                        //HACK LIE | pEC_PED_COMTableAdapter.Fill(dSPesees.PEC_PED_COM);
                        ConnectionManager.Instance.AddGvTableAdapter(peL_PED_LIETableAdapter);
                        peL_PED_LIETableAdapter.Fill(dSPesees.PEL_PED_LIE);
                    }
                    break;
            }
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                ConnectionManager.Instance.AddGvTableAdapter(pEE_PESEE_ENTETETableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
                ConnectionManager.Instance.AddGvTableAdapter(pED_PESEE_DETAILTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(rES_NOMCOMPLETTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(contact_NomCompletTableAdapter);

                _isDatabaseConnectionReady = true;
            }
            if (cepageComboBox != null)
                cepageComboBox.SelectedIndex = -1;
            StyleBuilderExtension.ActivateStyle(StyleBuilderExtension.TextBoxStyleEnum.ReadOnly,
                txtLitresAdmisAcq,
                txtLitresSaisisAcq,
                txtSoldeParAcquit,

                txtLitresAdmisApp,
                txtLitresSaisisApp,
                txtSoldeParLieu
            );
            //ValidationBuilderExtension.ApplyValidation(aCQ_IDComboBox, aCQ_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
            //ValidationBuilderExtension.ApplyValidation(aUT_IDComboBox, aUT_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
            //ValidationBuilderExtension.ApplyValidation(lIE_IDComboBox, lIE_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
            //ValidationBuilderExtension.ApplyValidation(rES_IDComboBox, rES_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);

            //ValidationBuilderExtension.ApplyValidation(cepageComboBox, cepageComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
            //ValidationBuilderExtension.ApplyValidation(pEE_LIEUTextBox, pEE_LIEUTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.NotEmptyString);

            cepageComboBox.Enabled = true;
            btnAddPesee.Enabled = false;

            pED_PESEE_DETAILDataGridView.MouseDoubleClick += btnModifierPesee_Click;
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                UpdateDonnees();

                if (_newEntete)
                {
                    pEE_PESEE_ENTETEBindingSource.AddNew();
                    pEE_DATEDateTimePicker.Value = System.DateTime.Now;
                    try
                    {
                        DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter annTable = ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter>();
                        pEE_LIEUTextBox.Text = string.IsNullOrWhiteSpace(_entetePesee.Lieu) ? annTable.GetVilleEncaveurByAnnID(Utilities.IdAnneeCourante) : _entetePesee.Lieu;
                    }
                    catch (Exception ex)
                    {
                        _log.Info(ex);
                    }
                }

                if (!_newEntete || _fromCockpit)
                {
                    SetChamps();
                }

                PED_SONDAGE_BRIKS.Visible = GlobalParam.Instance.ShowSondageBrix;
                PED_SONDAGE_OE.Visible = PED_SONDAGE_BRIKS.Visible == false;
                PED_QUANTITE_KG.Visible = _applicationUnite == SettingExtension.ApplicationUniteEnum.Kilo;
                PED_QUANTITE_LITRES.Visible = _applicationUnite == SettingExtension.ApplicationUniteEnum.Litre;

                pEE_IDTextBox.Hide();

                UpdateDetailPesee();
                UpdateEtatComboBox();
                SetLabel();
                _loadingAcquitInProgress = false;
                UpdateTotauxLitres();
            }
        }

        private void UpdateDonnees()
        {
            /// <Note>Inutile de continuer la connection à la BDD n' est pas encore disponible</Note>
            if (_isDatabaseConnectionReady == false)
                return;

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                var oldLoadingAcquitInProgress = _loadingAcquitInProgress;
                _loadingAcquitInProgress = true;
                rES_NOMCOMPLETTableAdapter.FillOrderedByDefault(dSPesees.RES_NOMCOMPLET);

                aCQ_ACQUITTableAdapter.FillByAnnee(dSPesees.ACQ_ACQUIT, Utilities.IdAnneeCourante);
                _loadingAcquitInProgress = oldLoadingAcquitInProgress;

                if (this.aCQ_IDComboBox.SelectedValue != null)
                {
                    var acquiId = 0;
                    if (int.TryParse(this.aCQ_IDComboBox.SelectedValue + "", out acquiId))
                        cEP_CEPAGETableAdapter.FillByAcqId(dSPesees.CEP_CEPAGE, ACQ_ID: acquiId);
                }
                if (cepageComboBox != null && cepageComboBox.Items.Count > 0 && cepageComboBox.SelectedIndex < 0)
                    cepageComboBox.SelectedIndex = 0;

                if (rES_IDComboBox != null && rES_IDComboBox.Items.Count > 0 && rES_IDComboBox.SelectedIndex < 0)
                    rES_IDComboBox.SelectedIndex = 0;

                /// <Note>Le cépage doit toujours être désactivé</Note>
                if (cepageComboBox != null)
                    cepageComboBox.Enabled = cepageComboBox.Items.Count > 1;

                if (pED_PESEE_DETAILDataGridView != null && pED_PESEE_DETAILDataGridView.RowCount > 0)
                    cepageComboBox.Enabled = false;
                else
                    cepageComboBox.Enabled = true;
                UpdateTotauxLitres();
            }
        }

        private void SetChamps()
        {
            aCQ_IDComboBox.SelectedValue = _entetePesee.AcquitId;
            try { cepageComboBox.SelectedValue = _entetePesee.CepageId; }
            catch (Exception ex) { _log.Warn("setChamps", ex); }

            UpdateComboBoxLie();
            //cRU_IDComboBox.SelectedValue = entetePesee.CruId;
            //updateComboBoxAppellation();
            // aPP_IDComboBox.SelectedValue = entetePesee.AppellationId;
            try
            {
                pEE_DATEDateTimePicker.Value = _entetePesee.Date;
            }
            catch (Exception ex) { _log.Info("Not Documented", ex); }
            pEE_LIEUTextBox.Text = _entetePesee.Lieu;
            rES_IDComboBox.SelectedValue = _entetePesee.ResponsableId;
            pEE_IDTextBox.Text = _entetePesee.Id.ToString();
            pEE_GRANDCRUCheckBox.Checked = _entetePesee.GrandCru;
            lIE_IDComboBox.SelectedValue = _entetePesee.LieuProductionId;
            UpdateComboBoxAutMention();
            aUT_IDComboBox.SelectedValue = _entetePesee.AutreMentionId;
            txtRemarques.Text = _entetePesee.Remark;
            txtRefractometerNb.Text = _entetePesee.Divers1;
            txtScaleNb.Text = _entetePesee.Divers2;
            cbxVendMec.Checked = !string.IsNullOrWhiteSpace(_entetePesee.Divers3) && _entetePesee.Divers3.Contains("1");
            UpdateTotauxLitres();
            ChkSaisie();
        }

        public void UpdateDetailPesee()
        {
            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    pED_PESEE_DETAILTableAdapter.FillByPEEID(this.dSPesees.PED_PESEE_DETAIL, new System.Nullable<int>(Int32.Parse(pEE_IDTextBox.Text)));
                }
            }
            catch { }
        }

        private void UpdateEtatComboBox()
        {
            /// <Note>Inutile de continuer la connection à la BDD n' est pas encore disponible</Note>
            if (_isDatabaseConnectionReady == false)
                return;
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                bool value;
                if (pED_PESEE_DETAILTableAdapter.GetNbPeseeByPeeId(Int32.Parse(pEE_IDTextBox.Text)) == 0)
                    value = true;
                else
                    value = false;

                aCQ_IDComboBox.Enabled = value;
                aUT_IDComboBox.Enabled = value;
                lIE_IDComboBox.Enabled = value;
                cepageComboBox.Enabled = value;

                btnSelectionAcquit.Enabled = value;

                //cepageComboBox.Enabled = cepageComboBox.Items.Count > 0 && pED_PESEE_DETAILDataGridView.RowCount < 1;
                if (cepageComboBox.SelectedIndex < 0 && cepageComboBox.Items.Count > 0)
                    cepageComboBox.SelectedIndex = 0;

                if (rES_IDComboBox != null && rES_IDComboBox.Items.Count > 0 && rES_IDComboBox.SelectedIndex < 0)
                    rES_IDComboBox.SelectedIndex = 0;
            }
        }

        private void SetLabel()
        {
            this.PED_QUANTITE_KG.Visible = _applicationUnite == SettingExtension.ApplicationUniteEnum.Kilo;
            this.PED_QUANTITE_LITRES.Visible = this.PED_QUANTITE_KG.Visible == false;

            string unite = GSN.GDV.Helpers.EnumHelper.GetName(_applicationUnite).ToLower() + "s";
            this.lblLimitParLieuAdmis.Text = "Nbre de " + unite + " admis :";
            this.lblLimitParLieuSaisis.Text = "Nbre de " + unite + " déjà saisis :";
            this.lblSoldeParLieu.Text = "Solde en " + unite + " :";

            this.lblLimitParAcquitAdmis.Text = "Nbre de " + unite + " admis :";
            this.lblLimitParAcquitSaisis.Text = "Nbre de " + unite + " déjà saisis :";
            this.lblSoldeParAcquit.Text = "Solde en " + unite + " :";
        }

        private void UpdateTotauxLitres()
        {
            if (_loadingAcquitInProgress)
                return;
            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    int acquitId = (int)aCQ_IDComboBox.SelectedValue;
                    int cepageId = (int)cepageComboBox.SelectedValue;
                    int lieuId = (int)lIE_IDComboBox.SelectedValue;

                    if (aUT_IDComboBox.SelectedValue == null)
                        aUT_IDComboBox.SelectedIndex = 0;

                    int autreMentionId = (int)aUT_IDComboBox.SelectedValue;

                    double maximumByAcquit = _limitationAlreadyRetrieved ? ConverterHelper.To(txtLitresAdmisAcq.Text, 0.0) : GlobalParam.LimitationProduction.GetMaximumByAcquit(AcquitId: acquitId, CepageId: cepageId, UniteType: _applicationUnite) ?? 0;
                    double maximumByLieu = _limitationAlreadyRetrieved ? ConverterHelper.To(txtLitresAdmisApp.Text, 0.0) : GlobalParam.LimitationProduction.GetMaximumByLieu(LieuId: lieuId, AcquitId: acquitId, CepageId: cepageId, UniteType: _applicationUnite) ?? 0;
                    //double maximumByAutreMention = limitationAlreadyRetrieved ? ConverterHelper.To(txtQuantiteAdmisParAutreMention.Text, 0.0) : GlobalParam.LimitationProduction.GetMaximumByAutreMention(AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId, UniteType: ApplicationUnite) ?? 0;

                    txtLitresAdmisAcq.Text = string.Format("{0:0.00}", maximumByAcquit);
                    txtLitresAdmisApp.Text = string.Format("{0:0.00}", maximumByLieu);

                    try
                    {
                        double saisisByAcquit = _limitationAlreadyRetrieved ? ConverterHelper.To(txtLitresSaisisAcq.Text, 0.0) : GlobalParam.LimitationProduction.GetSaisiByAcquit(AcquitId: acquitId, CepageId: cepageId, ExcludedPeseeDetailId: 0, UniteType: _applicationUnite) ?? 0;
                        double saisisByLieu = _limitationAlreadyRetrieved ? ConverterHelper.To(txtLitresSaisisApp.Text, 0.0) : GlobalParam.LimitationProduction.GetSaisiByLieu(LieuId: lieuId, AcquitId: acquitId, CepageId: cepageId, ExcludedPeseeDetailId: 0, UniteType: _applicationUnite) ?? 0;
                        //double saisisByAutreMention = limitationAlreadyRetrieved ? ConverterHelper.To(txtQuantiteSaisisParAutreMention.Text, 0.0) : GlobalParam.LimitationProduction.GetSaisiByAutreMention(AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: 0, UniteType: ApplicationUnite) ?? 0;

                        txtLitresSaisisAcq.Text = string.Format("{0:0.00}", saisisByAcquit);
                        txtLitresSaisisApp.Text = string.Format("{0:0.00}", saisisByLieu);

                        double soldeByAcquit = GlobalParam.LimitationProduction.CalculateSolde(maximum: maximumByAcquit, saisie: saisisByAcquit);
                        double soldeByLieu = GlobalParam.LimitationProduction.CalculateSolde(maximum: maximumByLieu, saisie: saisisByLieu);
                        //double soldeByAutreMention = GlobalParam.LimitationProduction.CalculateSolde(maximum: maximumByAutreMention, saisie: saisisByAutreMention);
                        txtSoldeParAcquit.Text = string.Format("{0:0.00}", soldeByAcquit);
                        txtSoldeParLieu.Text = string.Format("{0:0.00}", soldeByLieu);
                        //txtSoldeParAutreMention.Text = string.Format("{0:0.00}", soldeByAutreMention);

                        //ValidationBuilderExtension.Validate(txtSoldeParAutreMention, isValid: soldeByAutreMention >= 0, description: "Solde négative", setError: true);
                        //ValidationBuilderExtension.Validate(txtQuantiteSaisisParAutreMention, isValid: soldeByAutreMention >= 0, description: "Solde négative", setError: true);

                        ValidationBuilderExtension.Validate(txtSoldeParLieu, isValid: soldeByLieu >= 0, description: "Solde négative", setError: true);
                        ValidationBuilderExtension.Validate(txtLitresSaisisApp, isValid: soldeByLieu >= 0, description: "Solde négative", setError: true);

                        ValidationBuilderExtension.Validate(txtLitresSaisisAcq, isValid: soldeByAcquit >= 0, description: "Solde négative", setError: true);
                        ValidationBuilderExtension.Validate(txtSoldeParAcquit, isValid: soldeByAcquit >= 0, description: "Solde négative", setError: true);

                        _limitationAlreadyRetrieved = true;
                    }
                    catch
                    {
                        txtLitresSaisisApp.Text = "";
                        _limitationAlreadyRetrieved = false;
                    }
                }
            }
            catch
            {
                txtLitresAdmisApp.Text = "";
                txtLitresAdmisAcq.Text = "";
                _limitationAlreadyRetrieved = false;
            }
        }

        private bool UpdateEntetePesee()
        {
            bool result = true;
            try
            {
                if (_entetePesee == null)
                {
                    _entetePesee = new Gestion_Des_Vendanges.BUSINESS.EntetePesee();
                }
                _entetePesee.AcquitId = (int)aCQ_IDComboBox.SelectedValue;
                _entetePesee.CepageId = (int)cepageComboBox.SelectedValue; ;
                //entetePesee.AppellationId = (int)aPP_IDComboBox.SelectedValue;
                //entetePesee.CruId = (int)cRU_IDComboBox.SelectedValue;
                _entetePesee.Date = pEE_DATEDateTimePicker.Value;
                _entetePesee.Lieu = pEE_LIEUTextBox.Text;
                _entetePesee.ResponsableId = (int)rES_IDComboBox.SelectedValue;
                _entetePesee.Id = Int32.Parse(pEE_IDTextBox.Text);
                _entetePesee.GrandCru = pEE_GRANDCRUCheckBox.Checked;
                _entetePesee.AutreMentionId = (int)aUT_IDComboBox.SelectedValue;
                _entetePesee.LieuProductionId = (int)lIE_IDComboBox.SelectedValue;
                _entetePesee.Remark = txtRemarques.Text;
                _entetePesee.Divers1 = txtRefractometerNb.Text;
                _entetePesee.Divers2 = txtScaleNb.Text;
                _entetePesee.Divers3 = cbxVendMec.Checked ? "1" : "0";
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void UpdateContactNames(int acquitId)
        {
            if (acquitId < 0) return;

            try
            {
                var contactNames = contact_NomCompletTableAdapter.GetByAcquitId(acquitId)[0];
                lblOwnerAndProducerNames.Text = $"Propriétaire: {contactNames.PROPRIETAIRE_NOMCOMPLET}      |      Producteur: {contactNames.PRODUCTEUR_NOMCOMPLET}";
            }
            catch (IndexOutOfRangeException)
            {
                lblOwnerAndProducerNames.Text = string.Empty;
            }
        }

        private void WfrmSaisieEntetePesee_Shown(object sender, EventArgs e)
        {
            if (_cepId == null) return;
            _entetePesee.CepageId = _cepId;
            cepageComboBox.SelectedValue = _cepId;
        }

        #endregion OnLoad

        private bool ChkSaisie()
        {
            bool result = true;
            try
            {
                ///<Info> temporairement suspendu car le processus de validation présente quelques problèmes, il n' est pas tout jours appelé</Info>
                //ValidationBuilderExtension.Validate(aCQ_IDComboBox, aCQ_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
                //ValidationBuilderExtension.Validate(aUT_IDComboBox, aUT_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
                //ValidationBuilderExtension.Validate(lIE_IDComboBox, lIE_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
                //ValidationBuilderExtension.Validate(rES_IDComboBox, rES_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);

                //ValidationBuilderExtension.Validate(cepageComboBox, cepageComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
                //ValidationBuilderExtension.Validate(pEE_LIEUTextBox, pEE_LIEUTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.NotEmptyString);

                int testInt;
                testInt = (int)aCQ_IDComboBox.SelectedValue;
                DateTime testDate = pEE_DATEDateTimePicker.Value;
                //testInt = (int)cRU_IDComboBox.SelectedValue;
                //testInt = (int)aPP_IDComboBox.SelectedValue;
                testInt = (int)aUT_IDComboBox.SelectedValue;
                testInt = (int)lIE_IDComboBox.SelectedValue;
                testInt = (int)rES_IDComboBox.SelectedValue;

                if (cepageComboBox.SelectedValue == null) throw new InvalidOperationException("Il faudrait sélectionner un cépage valide");
                if (pEE_LIEUTextBox.Text == null) throw new Exception();
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void Save_EntetePesee()
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                this.Validate();

                this.pEE_PESEE_ENTETEBindingSource.EndEdit();
                try
                {
                    // TODO perhaps | Need be fixed
                    this.tableAdapterManager.UpdateAll(this.dSPesees);
                    if (_entetePesee != null)
                        _entetePesee.Id = ConverterHelper.To(pEE_IDTextBox.Text, _entetePesee.Id);
                }
                catch (Exception ex)
                {
                    _log.Warn("Save_EntetePesee", ex);
                }
            }
        }

        private bool AjouterEntetePesee()
        {
            bool resultat = true;
            if (ChkSaisie())
            {
                Save_EntetePesee();

                _formPesees?.UpdateDonnees();
                _cockpit?.UpdateAcquit();
                _newEntete = false;
            }
            else
            {
                resultat = false;
                MessageBox.Show("Veuillez saisir tous les champs de l'entête correctement\navant d'ajouter une pesée.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return resultat;
        }

        private BUSINESS.Pesee DataGridViewCellCollectionToPesee(DataGridViewCellCollection row)
        {
            BUSINESS.Pesee pesee = new BUSINESS.Pesee();
            pesee.Id = ConverterHelper.To(row["PED_ID"].Value, pesee.Id);

            pesee.CepageId = ConverterHelper.To(row["CEP_ID"].Value, pesee.CepageId);
            pesee.Lot = ConverterHelper.To(row["PED_LOT"].Value, pesee.Lot);

            pesee.SondageOE = ConverterHelper.To(row["PED_SONDAGE_OE"].Value, pesee.SondageOE);
            pesee.SondageBriks = ConverterHelper.To(row["PED_SONDAGE_BRIKS"].Value, pesee.SondageBriks);

            pesee.QuantiteKg = ConverterHelper.To(row["PED_QUANTITE_KG"].Value, pesee.QuantiteKg);
            pesee.QuantiteLitres = ConverterHelper.To(row["PED_QUANTITE_LITRES"].Value, pesee.QuantiteLitres);

            switch (BUSINESS.GlobalParam.Instance.Canton)
            {
                case SettingExtension.CantonEnum.Neuchâtel:
                    //HACK LIE | pesee.Commune = (int)pEC_PED_COMTableAdapter.GetDataByPed(pesee.Id).Rows[0]["COM_ID"];
                    pesee.LieuProduction = (int)peL_PED_LIETableAdapter.GetDataByPeseeDetailId(pesee.Id).Rows[0]["LIE_ID"];
                    break;
            }

            UpdateEntetePesee();
            pesee.Entete = _entetePesee;
            return pesee;
        }

        public void UpdateCurrentRow(BUSINESS.Pesee pesee)
        {
            UpdateRowPesee(this.pED_PESEE_DETAILDataGridView.CurrentRow.Cells, pesee);
            SaveDataGrid();
        }

        private void SaveDataGrid()
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                this.Validate();
                this.pED_PESEE_DETAILBindingSource.EndEdit();
                this.pED_PESEE_DETAILTableAdapter.Update(dSPesees);
            }
        }

        private void UpdateRowPesee(DataGridViewCellCollection row, BUSINESS.Pesee pesee)
        {
            row["CEP_ID"].Value = pesee.CepageId;
            row["PED_LOT"].Value = pesee.Lot;
            row["PED_SONDAGE_OE"].Value = pesee.SondageOE;
            row["PED_QUANTITE_KG"].Value = pesee.QuantiteKg;
            row["PED_QUANTITE_LITRES"].Value = pesee.QuantiteLitres;
            row["PED_SONDAGE_BRIKS"].Value = pesee.SondageBriks;

            switch (BUSINESS.GlobalParam.Instance.Canton)
            {
                case SettingExtension.CantonEnum.Neuchâtel:
                    //HACK LIE | pEC_PED_COMTableAdapter.FillByPed(dSPesees.PEC_PED_COM, pesee.Id);
                    //HACK LIE | System.Data.DataRow dr = pEC_PED_COMTableAdapter.GetDataByPed(pesee.Id).Rows[0];
                    //HACK LIE | dr["COM_ID"] = pesee.Commune;
                    //HACK LIE | pEC_PED_COMTableAdapter.Update(dr);
                    peL_PED_LIETableAdapter.FillByPeseeDetailId(dSPesees.PEL_PED_LIE, pesee.Id);
                    System.Data.DataRow dr = peL_PED_LIETableAdapter.GetDataByPeseeDetailId(pesee.Id).Rows[0];
                    dr["LIE_ID"] = pesee.LieuProduction;
                    peL_PED_LIETableAdapter.Update(dr);
                    break;
            }
        }

        private void UpdateComboBoxLie(bool propageToHierarchie = true)
        {
            /// <Note>Inutile de continuer la connection à la BDD n' est pas encore disponible</Note>
            if (_isDatabaseConnectionReady == false)
                return;
            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var cepageId = ConverterHelper.To(cepageComboBox.SelectedValue, 0);
                    var acquitId = ConverterHelper.To(aCQ_IDComboBox.SelectedValue, 0);
                    if (cepageId > 0)
                        lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByAcquitEtCepage(this.dSPesees.LIE_LIEU_DE_PRODUCTION, acquitId, cepageId);
                    else
                        lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByAcq(this.dSPesees.LIE_LIEU_DE_PRODUCTION, acquitId);
                }

                try
                {
                    lIE_IDComboBox.SelectedValue = _entetePesee.LieuProductionId;
                }
                catch
                {
                    try
                    {
                        this.lIE_IDComboBox.SelectedItem = 0;
                        //ValidationBuilderExtension.Validate(this.lIE_IDComboBox, isValid:true, description:string.Empty);
                    }
                    catch
                    {
                        var message = "L'acquit sélectionné ne contient aucune parcelle!";
                        //ValidationBuilderExtension.Validate(this.lIE_IDComboBox, isValid: false, description: message);
                        MessageBox.Show(message, "Erreur acquit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                if (lIE_IDComboBox.SelectedValue == null && lIE_IDComboBox.Items.Count == 1)
                    lIE_IDComboBox.SelectedIndex = 0;
            }
            catch (NullReferenceException)
            {
            }
            if (propageToHierarchie)
                UpdateComboBoxAutMention(propageToHierarchie: propageToHierarchie);
        }

        private void UpdateComboBoxAutMention(bool propageToHierarchie = true)
        {
            /// <Note>Inutile de continuer la connection à la BDD n' est pas encore disponible</Note>
            if (_isDatabaseConnectionReady == false)
                return;

            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    int? acquitId = ConverterHelper.To(this.aCQ_IDComboBox.SelectedValue, 0);
                    int? lieuId = ConverterHelper.To(this.lIE_IDComboBox.SelectedValue, 0);
                    int? cepageId = ConverterHelper.To(this.cepageComboBox.SelectedValue, 0);
                    //aUT_AUTRE_MENTIONTableAdapter.FillByAcqLie(dSPesees.AUT_AUTRE_MENTION, new System.Nullable<int>((int)this.aCQ_IDComboBox.SelectedValue), (int)this.lIE_IDComboBox.SelectedValue);
                    //aUT_AUTRE_MENTIONTableAdapter.FillByAcqLie(dSPesees.AUT_AUTRE_MENTION, ACQ_ID: AcquitId == 0 ? null : AcquitId, LIE_ID: LieuId == 0 ? null : LieuId);
                    aUT_AUTRE_MENTIONTableAdapter.FillByAcquitIdLieuIdCepageId(dSPesees.AUT_AUTRE_MENTION, AcquitId: acquitId == 0 ? null : acquitId, LieuId: lieuId == 0 ? null : lieuId, CepageId: cepageId == 0 ? null : cepageId);
                }
                try
                {
                    aUT_IDComboBox.SelectedValue = _entetePesee.AutreMentionId;
                }
                catch
                {
                }
                if (aUT_IDComboBox.SelectedValue == null && aUT_IDComboBox.Items.Count == 1)
                    aUT_IDComboBox.SelectedIndex = 0;
            }
            catch
            {
            }

            //this.aUT_IDComboBox.Enabled = true;
            if (propageToHierarchie)
                UpdateTotauxLitres();
        }

        private void HideControls(List<Control> lstControlsToHide)
        {
            foreach (Control c in lstControlsToHide)
            {
                c.Visible = false;
            }
        }

        #region Button Events

        private void btnSelectionAcquit_Click(object sender, EventArgs e)
        {
            WfrmSelectAcquit selectAcquit = new WfrmSelectAcquit();
            selectAcquit.ShowDialog();
            _limitationAlreadyRetrieved = false;
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                aCQ_IDComboBox.SelectedValue = selectAcquit.AcquitId;
            }
        }

        private void btnAddPesee_Click(object sender, EventArgs e)
        {
            try
            {
                AddPesee();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void AddPesee()
        {
            bool ok = true;
            _limitationAlreadyRetrieved = false;
            if (_newEntete)
            {
                ok = UpdateEntetePesee();
                if (ok && AjouterEntetePesee())
                {
                    UpdateDetailPesee();
                    SetChamps();
                }
                else
                {
                    ok = false;
                }
            }
            else if (UpdateEntetePesee())
            {
                // _formPesees?.UpdateEntetePesee(_entetePesee);
            }
            else
            {
                ok = false;
            }

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                if (ok)
                {
                    _entetePesee.Id = ConverterHelper.To(this.pEE_IDTextBox.Text, _entetePesee.Id);
                    WfrmSaisiePesee formAddPesee = new WfrmSaisiePesee(this, _entetePesee);
                    formAddPesee.ShowInTaskbar = false;
                    formAddPesee.ShowDialog();
                    UpdateTotauxLitres();
                    UpdateEtatComboBox();
                }
                else
                {
                    ok = false;
                }

            if (ok == false)
                MessageBox.Show("Veuillez saisir tous les champs de l'entête correctement\navant d'ajouter une pesée.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnValiderPesee_Click(object sender, EventArgs e)
        {
            if (_newEntete)
            {
                if (AjouterEntetePesee())
                {
                    this.Close();
                }
                _cockpit?.UpdateAcquit();
            }
            else
            {
                if (UpdateEntetePesee())
                {
                    _formPesees?.UpdateEntetePesee(_entetePesee);
                    _cockpit?.UpdateAcquit();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Veuillez saisir tous les champs de l'entête correctement\navant d'ajouter une pesée.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btnModifierPesee_Click(object sender, EventArgs e)
        {
            if (this.pED_PESEE_DETAILDataGridView.CurrentRow != null)
            {
                WfrmSaisiePesee formPesee = new WfrmSaisiePesee(this, DataGridViewCellCollectionToPesee(this.pED_PESEE_DETAILDataGridView.CurrentRow.Cells));
                formPesee.ShowInTaskbar = false;
                formPesee.ShowDialog();
                UpdateTotauxLitres();
            }
        }

        private void btnDeletePesee_Click(object sender, EventArgs e)
        {
            _limitationAlreadyRetrieved = false;
            if (pED_PESEE_DETAILBindingSource.Current != null)
            {
                try
                {
                    int pedId = (int)pED_PESEE_DETAILDataGridView.CurrentRow.Cells["PED_ID"].Value;
                    if (pED_PESEE_DETAILTableAdapter.NbRef(pedId) > 0)
                    {
                        MessageBox.Show("Vous ne pouvez pas supprimer cette pesée car elle a déjà été payée.\nVeuillez supprimer ce paiement pour déverrouiller la pesée.",
                            "Pesée verrouillée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        if (MessageBox.Show("Êtes-vous sûr de vouloir supprimer cette pesée?", "Suppression d'une pesée", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            pED_PESEE_DETAILBindingSource.RemoveCurrent();
                            SaveDataGrid();
                            UpdateTotauxLitres();
                            UpdateEtatComboBox();
                        }
                    }
                }
                catch { }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (_newEntete)
            {
                if (AjouterEntetePesee())
                {
                    //producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);
                    if (this.pED_PESEE_DETAILBindingSource.Current != null)
                    {
                        int id = (int)this.pED_PESEE_DETAILDataGridView.CurrentRow.Cells["PED_PEE_ID"].Value;
                        int producteurID = 0;
                        producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);
                        ShowAttestation(id, producteurID);
                    }
                }
            }
            else
            {
                if (UpdateEntetePesee())
                {
                    _formPesees?.UpdateEntetePesee(_entetePesee);
                    if (this.pED_PESEE_DETAILBindingSource.Current != null)
                    {
                        int id = (int)this.pED_PESEE_DETAILDataGridView.CurrentRow.Cells["PED_PEE_ID"].Value;
                        int producteurID = 0;
                        producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);
                        ShowAttestation(id, producteurID);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez saisir tous les champs de l'entête correctement\navant d'ajouter une pesée.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private static void ShowAttestation(int id, int producteurID)
        {
            CtrlReportViewer.ShowAttestationControleV2(peeID: id, proID: producteurID, annID: Utilities.IdAnneeCourante);
        }

        #endregion Button Events

        #region Events

        private void aCQ_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                var acquitId = aCQ_IDComboBox.SelectedValue == null ? 0 : (int)aCQ_IDComboBox.SelectedValue;
                if (aCQ_IDComboBox.Text != "")
                {
                    if (aCQ_ACQUITTableAdapter.GetClaIdByAcqId(acquitId) == 1)
                    {
                        pEE_GRANDCRUCheckBox.Enabled = true;
                        pEE_GRANDCRUCheckBox.Checked = false;
                    }
                    else
                    {
                        pEE_GRANDCRUCheckBox.Checked = false;
                        pEE_GRANDCRUCheckBox.Enabled = false;
                    }
                }
                else
                {
                    pEE_GRANDCRUCheckBox.Checked = false;
                    pEE_GRANDCRUCheckBox.Enabled = true;
                }

                if (acquitId > 0)
                {
                    #region ____ALERT____ ce block avec c'est 3 conditions doivent être maintenue, à moins de savoir exactement ce que l'on fait

                    var selectedCepage = cepageComboBox.SelectedValue;
                    cEP_CEPAGETableAdapter.FillByAcqId(dSPesees.CEP_CEPAGE, acquitId);
                    var newCepageId = cepageComboBox.SelectedValue;

                    if (selectedCepage != null)
                        cepageComboBox.SelectedValue = selectedCepage;

                    if (cepageComboBox.SelectedValue == null && newCepageId != null)
                        cepageComboBox.SelectedValue = newCepageId;

                    if (cepageComboBox.SelectedValue == null && cepageComboBox.Items.Count == 1)
                        cepageComboBox.SelectedIndex = 0;

                    #endregion ____ALERT____ ce block avec c'est 3 conditions doivent être maintenue, à moins de savoir exactement ce que l'on fait
                }

                //cEP_CEPAGETableAdapter.FillByCouleurFromAcquiId(dSPesees.CEP_CEPAGE, AcquitId);
                //else
                //	cEP_CEPAGETableAdapter.FillByProducteurAnnee(dSPesees.CEP_CEPAGE, AcquitId);
                UpdateContactNames(acquitId);
                this.ChkSaisie();
            }
            UpdateComboBoxLie(propageToHierarchie: false);
            pEE_LIEUTextBox.Text = _entetePesee.Lieu;
            txtRefractometerNb.Text = _entetePesee.Divers1;
            txtScaleNb.Text = _entetePesee.Divers2;
        }

        private void toolResponsable_Click(object sender, EventArgs e)
        {
            WfrmGestionDesResponsables.Wfrm.WfrmShowDialog();
            rES_NOMCOMPLETTableAdapter.FillOrderedByDefault(dSPesees.RES_NOMCOMPLET);
        }

        private void toolLieuProduction_Click(object sender, EventArgs e)
        {
            WfrmGestionDesLieuxDeProductions.Wfrm.WfrmShowDialog();
            if (aCQ_IDComboBox.SelectedValue != null)
                lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByAcq(this.dSPesees.LIE_LIEU_DE_PRODUCTION, (int)aCQ_IDComboBox.SelectedValue);
        }

        private void toolNomLocal_Click(object sender, EventArgs e)
        {
            WfrmGestionDesAutresMentions.Wfrm.WfrmShowDialog();
            if (aCQ_IDComboBox.SelectedValue != null && lIE_IDComboBox.SelectedValue != null)
                aUT_AUTRE_MENTIONTableAdapter.FillByAcqLie(this.dSPesees.AUT_AUTRE_MENTION, (int)aCQ_IDComboBox.SelectedValue, (int)lIE_IDComboBox.SelectedValue);
        }

        private void lIE_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateComboBoxAutMention(propageToHierarchie: false);
            _limitationAlreadyRetrieved = false;
            this.UpdateTotauxLitres();
        }

        private void pED_PESEE_DETAILDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    pED_PESEE_DETAILBindingSource.RemoveSort();
                    pED_PESEE_DETAILTableAdapter.FillOrderCep(dSPesees.PED_PESEE_DETAIL, int.Parse(pEE_IDTextBox.Text));
                    foreach (DataGridViewColumn column in pED_PESEE_DETAILDataGridView.Columns)
                    {
                        column.HeaderCell.SortGlyphDirection = SortOrder.None;
                    }
                    pED_PESEE_DETAILDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
            }
        }

        private void cepageComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            _limitationAlreadyRetrieved = false;
            if (_entetePesee != null)
                _entetePesee.CepageId = ConverterHelper.To(cepageComboBox.SelectedValue, _entetePesee.CepageId ?? 0);
            if (btnAddPesee != null)
                btnAddPesee.Enabled = (cepageComboBox != null && ConverterHelper.To(cepageComboBox.SelectedValue, _entetePesee == null ? 0 : _entetePesee.CepageId ?? 0) > 0);
            //this.updateTotauxLitres();
            UpdateComboBoxLie();
        }

        private void transfertButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                    if (_entetePesee != null && _entetePesee.Id > 0 && pED_PESEE_DETAILDataGridView != null && pED_PESEE_DETAILDataGridView.RowCount > 0)
                    {
                        var helper = ControllerHelper.Create();
                        var controller = new TransfertPeseeDetailWizardController(helper);
                        ITransfertPeseeDetailWizardViewModel model;

                        using (var db = helper.CreateMainDataContext())
                        {
                            model = controller.CreateViewModel(Utilities.IdAnneeCourante, db, _entetePesee.Id);
                        }

                        var form = new GSN.GDV.GUI.WindowsForms.TransfertPeseeDetailWizardWindowsForm(model);
                        form.Controller = controller;
                        form.UserControl.Controller = controller;
                        if (form.ShowDialog(this) == DialogResult.OK)
                        {
                            UpdateDetailPesee();
                            UpdateTotauxLitres();
                            UpdateEtatComboBox();
                            _formPesees?.UpdateEntetePesee(_entetePesee);
                        }

                        //Run(form);
                    }
                    else
                    {
                        MessageBox.Show("Cette option n'est disponible que si vous avez déjà ajouté une pesée");
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Erreur !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pEE_LIEUTextBox_TextChanged(object sender, EventArgs e)
        {
            pEE_LIEUTextBox_Validated(sender, e);
        }

        private void pEE_LIEUTextBox_Validated(object sender, EventArgs e)
        {
            ValidationBuilderExtension.Validate(pEE_LIEUTextBox, pEE_LIEUTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.NotEmptyString);
        }

        #endregion Events
    }
}