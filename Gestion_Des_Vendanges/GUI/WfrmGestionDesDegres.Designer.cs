﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesDegres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label cOM_NOMLabel;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView = new System.Windows.Forms.DataGridView();
            this.dEGIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dEGBRIXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dEGOECHSLEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dEG_DEGRE_OECHSLE_BRIXBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dEG_OECHSLETextBox1 = new System.Windows.Forms.TextBox();
            this.dEG_BRIXTextBox1 = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModifier = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.dEG_IDTextBox1 = new System.Windows.Forms.TextBox();
            this.dEG_DEGRE_OECHSLE_BRIXTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DEG_DEGRE_OECHSLE_BRIXTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            label1 = new System.Windows.Forms.Label();
            cOM_NOMLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dEG_DEGRE_OECHSLE_BRIXDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEG_DEGRE_OECHSLE_BRIXBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 27);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(41, 13);
            label1.TabIndex = 20;
            label1.Text = "% Brix :";
            // 
            // cOM_NOMLabel
            // 
            cOM_NOMLabel.AutoSize = true;
            cOM_NOMLabel.Location = new System.Drawing.Point(6, 63);
            cOM_NOMLabel.Name = "cOM_NOMLabel";
            cOM_NOMLabel.Size = new System.Drawing.Size(59, 13);
            cOM_NOMLabel.TabIndex = 17;
            cOM_NOMLabel.Text = "° Oechslé :";
            // 
            // dEG_DEGRE_OECHSLE_BRIXDataGridView
            // 
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.AllowUserToAddRows = false;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.AllowUserToDeleteRows = false;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.AutoGenerateColumns = false;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dEGIDDataGridViewTextBoxColumn,
            this.dEGBRIXDataGridViewTextBoxColumn,
            this.dEGOECHSLEDataGridViewTextBoxColumn});
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.DataSource = this.dEG_DEGRE_OECHSLE_BRIXBindingSource;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.Location = new System.Drawing.Point(298, 13);
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.Name = "dEG_DEGRE_OECHSLE_BRIXDataGridView";
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.ReadOnly = true;
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.Size = new System.Drawing.Size(324, 433);
            this.dEG_DEGRE_OECHSLE_BRIXDataGridView.TabIndex = 2;
            // 
            // dEGIDDataGridViewTextBoxColumn
            // 
            this.dEGIDDataGridViewTextBoxColumn.DataPropertyName = "DEG_ID";
            this.dEGIDDataGridViewTextBoxColumn.HeaderText = "DEG_ID";
            this.dEGIDDataGridViewTextBoxColumn.Name = "dEGIDDataGridViewTextBoxColumn";
            this.dEGIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.dEGIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dEGBRIXDataGridViewTextBoxColumn
            // 
            this.dEGBRIXDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dEGBRIXDataGridViewTextBoxColumn.DataPropertyName = "DEG_BRIX";
            this.dEGBRIXDataGridViewTextBoxColumn.HeaderText = "% Brix";
            this.dEGBRIXDataGridViewTextBoxColumn.MinimumWidth = 80;
            this.dEGBRIXDataGridViewTextBoxColumn.Name = "dEGBRIXDataGridViewTextBoxColumn";
            this.dEGBRIXDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dEGOECHSLEDataGridViewTextBoxColumn
            // 
            this.dEGOECHSLEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dEGOECHSLEDataGridViewTextBoxColumn.DataPropertyName = "DEG_OECHSLE";
            this.dEGOECHSLEDataGridViewTextBoxColumn.HeaderText = "° Oechslé";
            this.dEGOECHSLEDataGridViewTextBoxColumn.MinimumWidth = 80;
            this.dEGOECHSLEDataGridViewTextBoxColumn.Name = "dEGOECHSLEDataGridViewTextBoxColumn";
            this.dEGOECHSLEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dEG_DEGRE_OECHSLE_BRIXBindingSource
            // 
            this.dEG_DEGRE_OECHSLE_BRIXBindingSource.DataMember = "DEG_DEGRE_OECHSLE_BRIX";
            this.dEG_DEGRE_OECHSLE_BRIXBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dEG_OECHSLETextBox1);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.dEG_BRIXTextBox1);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnModifier);
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(cOM_NOMLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 156);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des degrés";
            // 
            // dEG_OECHSLETextBox1
            // 
            this.dEG_OECHSLETextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dEG_DEGRE_OECHSLE_BRIXBindingSource, "DEG_OECHSLE", true));
            this.dEG_OECHSLETextBox1.Location = new System.Drawing.Point(104, 60);
            this.dEG_OECHSLETextBox1.Name = "dEG_OECHSLETextBox1";
            this.dEG_OECHSLETextBox1.Size = new System.Drawing.Size(170, 20);
            this.dEG_OECHSLETextBox1.TabIndex = 1;
            // 
            // dEG_BRIXTextBox1
            // 
            this.dEG_BRIXTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dEG_DEGRE_OECHSLE_BRIXBindingSource, "DEG_BRIX", true));
            this.dEG_BRIXTextBox1.Location = new System.Drawing.Point(104, 24);
            this.dEG_BRIXTextBox1.Name = "dEG_BRIXTextBox1";
            this.dEG_BRIXTextBox1.Size = new System.Drawing.Size(170, 20);
            this.dEG_BRIXTextBox1.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDelete.Location = new System.Drawing.Point(199, 120);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Supprimer";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnModifier
            // 
            this.btnModifier.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnModifier.Location = new System.Drawing.Point(118, 120);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(75, 30);
            this.btnModifier.TabIndex = 3;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = false;
            // 
            // btnNew
            // 
            this.btnNew.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnNew.Location = new System.Drawing.Point(37, 120);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 30);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "Nouveau";
            this.btnNew.UseVisualStyleBackColor = false;
            // 
            // dEG_IDTextBox1
            // 
            this.dEG_IDTextBox1.BackColor = System.Drawing.Color.Lavender;
            this.dEG_IDTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dEG_DEGRE_OECHSLE_BRIXBindingSource, "DEG_ID", true));
            this.dEG_IDTextBox1.Enabled = false;
            this.dEG_IDTextBox1.Location = new System.Drawing.Point(395, 149);
            this.dEG_IDTextBox1.Name = "dEG_IDTextBox1";
            this.dEG_IDTextBox1.Size = new System.Drawing.Size(131, 20);
            this.dEG_IDTextBox1.TabIndex = 32;
            // 
            // dEG_DEGRE_OECHSLE_BRIXTableAdapter
            // 
            this.dEG_DEGRE_OECHSLE_BRIXTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = this.dEG_DEGRE_OECHSLE_BRIXTableAdapter;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // WfrmGestionDesDegres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 458);
            this.Controls.Add(this.dEG_IDTextBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dEG_DEGRE_OECHSLE_BRIXDataGridView);
            this.MinimumSize = new System.Drawing.Size(650, 300);
            this.Name = "WfrmGestionDesDegres";
            this.Text = "Gestion des degrés";
            this.Load += new System.EventHandler(this.WfrmGestionDesDegres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dEG_DEGRE_OECHSLE_BRIXDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dEG_DEGRE_OECHSLE_BRIXBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dEG_DEGRE_OECHSLE_BRIXDataGridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Button btnNew;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource dEG_DEGRE_OECHSLE_BRIXBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DEG_DEGRE_OECHSLE_BRIXTableAdapter dEG_DEGRE_OECHSLE_BRIXTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox dEG_OECHSLETextBox1;
        private System.Windows.Forms.TextBox dEG_BRIXTextBox1;
        private System.Windows.Forms.TextBox dEG_IDTextBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEGIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEGBRIXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEGOECHSLEDataGridViewTextBoxColumn;
    }
}