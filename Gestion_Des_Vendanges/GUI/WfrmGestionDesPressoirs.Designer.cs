﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesPressoirs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cEP_NOMLabel;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pRE_IDTextBox = new System.Windows.Forms.TextBox();
            this.pRE_PRESSOIRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aDR_IDComboBox = new System.Windows.Forms.ComboBox();
            this.AdressescontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolAddress = new System.Windows.Forms.ToolStripMenuItem();
            this.adresseCompletBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnSaveCepage = new System.Windows.Forms.Button();
            this.btnAddCepage = new System.Windows.Forms.Button();
            this.pRE_PRESSOIRTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRE_PRESSOIRTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.adresseCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter();
            this.pRE_PRESSOIRDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADR_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            cEP_NOMLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRE_PRESSOIRBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.AdressescontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRE_PRESSOIRDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cEP_NOMLabel
            // 
            cEP_NOMLabel.AutoSize = true;
            cEP_NOMLabel.Location = new System.Drawing.Point(5, 34);
            cEP_NOMLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            cEP_NOMLabel.Name = "cEP_NOMLabel";
            cEP_NOMLabel.Size = new System.Drawing.Size(51, 13);
            cEP_NOMLabel.TabIndex = 16;
            cEP_NOMLabel.Text = "Adresse :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pRE_IDTextBox);
            this.groupBox1.Controls.Add(this.aDR_IDComboBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnSaveCepage);
            this.groupBox1.Controls.Add(this.btnAddCepage);
            this.groupBox1.Controls.Add(cEP_NOMLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 122);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des pressoirs";
            // 
            // pRE_IDTextBox
            // 
            this.pRE_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRE_PRESSOIRBindingSource, "PRE_ID", true));
            this.pRE_IDTextBox.Location = new System.Drawing.Point(223, 5);
            this.pRE_IDTextBox.Name = "pRE_IDTextBox";
            this.pRE_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.pRE_IDTextBox.TabIndex = 18;
            // 
            // pRE_PRESSOIRBindingSource
            // 
            this.pRE_PRESSOIRBindingSource.DataMember = "PRE_PRESSOIR";
            this.pRE_PRESSOIRBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aDR_IDComboBox
            // 
            this.aDR_IDComboBox.ContextMenuStrip = this.AdressescontextMenuStrip;
            this.aDR_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRE_PRESSOIRBindingSource, "ADR_ID", true));
            this.aDR_IDComboBox.DataSource = this.adresseCompletBindingSource;
            this.aDR_IDComboBox.DisplayMember = "ADR_COMPLET";
            this.aDR_IDComboBox.FormattingEnabled = true;
            this.aDR_IDComboBox.Location = new System.Drawing.Point(130, 31);
            this.aDR_IDComboBox.Name = "aDR_IDComboBox";
            this.aDR_IDComboBox.Size = new System.Drawing.Size(232, 21);
            this.aDR_IDComboBox.TabIndex = 0;
            this.aDR_IDComboBox.ValueMember = "ADR_ID";
            // 
            // AdressescontextMenuStrip
            // 
            this.AdressescontextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.AdressescontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAddress});
            this.AdressescontextMenuStrip.Name = "AdressescontextMenuStrip";
            this.AdressescontextMenuStrip.Size = new System.Drawing.Size(195, 48);
            // 
            // toolAddress
            // 
            this.toolAddress.Name = "toolAddress";
            this.toolAddress.Size = new System.Drawing.Size(194, 22);
            this.toolAddress.Text = "Accéder aux adresses...";
            this.toolAddress.Click += new System.EventHandler(this.toolAddress_Click);
            // 
            // adresseCompletBindingSource
            // 
            this.adresseCompletBindingSource.DataMember = "AdresseComplet";
            this.adresseCompletBindingSource.DataSource = this.dSPesees;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(288, 87);
            this.btnSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnSaveCepage
            // 
            this.btnSaveCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveCepage.Location = new System.Drawing.Point(209, 87);
            this.btnSaveCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveCepage.Name = "btnSaveCepage";
            this.btnSaveCepage.Size = new System.Drawing.Size(75, 30);
            this.btnSaveCepage.TabIndex = 2;
            this.btnSaveCepage.Text = "Modifier";
            this.btnSaveCepage.UseVisualStyleBackColor = false;
            // 
            // btnAddCepage
            // 
            this.btnAddCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddCepage.Location = new System.Drawing.Point(130, 87);
            this.btnAddCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddCepage.Name = "btnAddCepage";
            this.btnAddCepage.Size = new System.Drawing.Size(75, 30);
            this.btnAddCepage.TabIndex = 1;
            this.btnAddCepage.Text = "Nouveau";
            this.btnAddCepage.UseVisualStyleBackColor = false;
            // 
            // pRE_PRESSOIRTableAdapter
            // 
            this.pRE_PRESSOIRTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = this.adresseCompletTableAdapter;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = this.pRE_PRESSOIRTableAdapter;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // adresseCompletTableAdapter
            // 
            this.adresseCompletTableAdapter.ClearBeforeFill = true;
            // 
            // pRE_PRESSOIRDataGridView
            // 
            this.pRE_PRESSOIRDataGridView.AllowUserToAddRows = false;
            this.pRE_PRESSOIRDataGridView.AllowUserToDeleteRows = false;
            this.pRE_PRESSOIRDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pRE_PRESSOIRDataGridView.AutoGenerateColumns = false;
            this.pRE_PRESSOIRDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pRE_PRESSOIRDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pRE_PRESSOIRDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.ADR_ID});
            this.pRE_PRESSOIRDataGridView.DataSource = this.pRE_PRESSOIRBindingSource;
            this.pRE_PRESSOIRDataGridView.Location = new System.Drawing.Point(386, 12);
            this.pRE_PRESSOIRDataGridView.Name = "pRE_PRESSOIRDataGridView";
            this.pRE_PRESSOIRDataGridView.ReadOnly = true;
            this.pRE_PRESSOIRDataGridView.Size = new System.Drawing.Size(256, 157);
            this.pRE_PRESSOIRDataGridView.TabIndex = 21;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PRE_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "PRE_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // ADR_ID
            // 
            this.ADR_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ADR_ID.DataPropertyName = "ADR_ID";
            this.ADR_ID.DataSource = this.adresseCompletBindingSource;
            this.ADR_ID.DisplayMember = "ADR_COMPLET";
            this.ADR_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ADR_ID.HeaderText = "Adresse";
            this.ADR_ID.MinimumWidth = 100;
            this.ADR_ID.Name = "ADR_ID";
            this.ADR_ID.ReadOnly = true;
            this.ADR_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ADR_ID.ValueMember = "ADR_ID";
            // 
            // WfrmGestionDesPressoirs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 181);
            this.Controls.Add(this.pRE_PRESSOIRDataGridView);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(600, 215);
            this.Name = "WfrmGestionDesPressoirs";
            this.Text = "Gestion des pressoirs";
            this.Load += new System.EventHandler(this.WfrmGestionDesPressoirs_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRE_PRESSOIRBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.AdressescontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRE_PRESSOIRDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnSaveCepage;
        private System.Windows.Forms.Button btnAddCepage;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pRE_PRESSOIRBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRE_PRESSOIRTableAdapter pRE_PRESSOIRTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView pRE_PRESSOIRDataGridView;
        private System.Windows.Forms.ComboBox aDR_IDComboBox;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter adresseCompletTableAdapter;
        private System.Windows.Forms.BindingSource adresseCompletBindingSource;
        private System.Windows.Forms.TextBox pRE_IDTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn ADR_ID;
        private System.Windows.Forms.ContextMenuStrip AdressescontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolAddress;
    }
}