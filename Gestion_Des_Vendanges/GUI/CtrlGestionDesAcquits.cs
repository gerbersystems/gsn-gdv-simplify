﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire permettant la gestion des acquits
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class CtrlGestionDesAcquits : CtrlWfrm
    {
        #region properties

        public override ToolStrip ToolStrip
        {
            get { return toolStrip1; }
        }

        public override Panel OptionPanel
        {
            get { return panel1; }
        }

        #endregion properties

        #region constructor

        //public static CtrlGestionDesAcquits GetInstance()
        //{
        //    return new CtrlGestionDesAcquits();
        //}

        private static CtrlGestionDesAcquits _instance;

        public static CtrlGestionDesAcquits GetInstance() => _instance = new CtrlGestionDesAcquits();

        public static CtrlGestionDesAcquits GetCurrentInstance() => _instance ?? new CtrlGestionDesAcquits();

        private CtrlGestionDesAcquits()
        {
            InitializeComponent();
            WfrmGestionDesAcquits_Load(null, null);
        }

        private void WfrmGestionDesAcquits_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rEG_REGIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cLA_CLASSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);

            UpdateDonnees();
            aCQ_ACQUITDataGridView.MouseDoubleClick += BtnModifyAcquit_Click;
            Controls.Remove(OptionPanel);
        }

        #endregion constructor

        #region Methods

        public bool FromCockpit = false;

        public override void UpdateDonnees()
        {
            bool isStartup = cbAnnee.SelectedValue == null;

            if (FromCockpit) isStartup = false;

            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
            cbAnnee.SelectedValue = Utilities.IdAnneeCourante;
            rEG_REGIONTableAdapter.Fill(dSPesees.REG_REGION);
            cOM_COMMUNETableAdapter.Fill(dSPesees.COM_COMMUNE);
            cOU_COULEURTableAdapter.Fill(dSPesees.COU_COULEUR);
            cLA_CLASSETableAdapter.Fill(dSPesees.CLA_CLASSE);
            pRO_NOMCOMPLETTableAdapter.Fill(dSPesees.PRO_NOMCOMPLET);
            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
            aCQ_ACQUITTableAdapter.FillByAnnee(dSPesees.ACQ_ACQUIT, new int?(Utilities.IdAnneeCourante));
            pAR_PARCELLETableAdapter.Fill(dSPesees.PAR_PARCELLE);
            cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);

            ACQ_LITRES.Visible = (GlobalParam.Instance.ApplicationUnite == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre);
            Kilos.Visible = (ACQ_LITRES.Visible == false);

            // Auto select last year of the folder on start
            if (isStartup)
                cbAnnee.SelectedIndex = cbAnnee.Items.Count > 0 ? cbAnnee.Items.Count - 1 : -1;
        }

        private void SaveDataGrid()
        {
            Validate();
            aCQ_ACQUITBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(dSPesees);
        }

        public void UpdateAcquit(Acquit acquit)
        {
            int id = -1;
            int idCurrentRow;
            UpdateDonnees();
            try
            {
                idCurrentRow = (int)aCQ_ACQUITDataGridView.CurrentRow.Cells["ACQ_ID"].Value;
            }
            catch
            {
                idCurrentRow = -1;
            }
            if (acquit.Id != idCurrentRow)
            {
                var x = aCQ_ACQUITDataGridView.Rows.Count;

                for (int i = 0; i < aCQ_ACQUITDataGridView.RowCount && id < 0; i++)
                {
                    if (acquit.Id == (int)aCQ_ACQUITDataGridView["ACQ_ID", i].Value) id = i;
                }

                UpdateRowAcquit(aCQ_ACQUITDataGridView.Rows[id].Cells, acquit);
            }
            else
            {
                UpdateRowAcquit(aCQ_ACQUITDataGridView.CurrentRow.Cells, acquit);
            }

            SaveDataGrid();
        }

        private Acquit DataGridViewCellCollectionToAcquit(DataGridViewCellCollection row)
        {
            var acquit = new Acquit()
            {
                Id = (int)row["ACQ_ID"].Value,
                ClasseId = (int)row["CLA_ID"].Value,
                CouleurId = (int)row["COU_ID"].Value,
                AnneeId = (int)row["ANN_ID"].Value,
                CommuneId = (int)row["COM_ID"].Value,
                ProprietaireId = (int)row["PRO_ID"].Value,
                Numero = (int)row["ACQ_NUMERO"].Value,
                Surface = decimal.Parse(row["ACQ_SURFACE"].Value.ToString()),
                Litres = decimal.Parse(row["ACQ_LITRES"].Value.ToString()),
                RegionId = (int)row["REG_ID"].Value,
                ProprietaireVendangeId = (int)row["PRO_ID_VENDANGE"].Value
            };

            try { acquit.Date = (DateTime)row["ACQ_DATE"].Value; }
            catch { }

            try { acquit.DateReception = (DateTime?)row["ACQ_RECEPTION"].Value; }
            catch { }

            try { acquit.NumeroComplementaire = (int)row["ACQ_NUMERO_COMPL"].Value; }
            catch { }

            return acquit;
        }

        private void UpdateRowAcquit(DataGridViewCellCollection row, Acquit acquit)
        {
            row["CLA_ID"].Value = acquit.ClasseId;
            row["COU_ID"].Value = acquit.CouleurId;
            row["ANN_ID"].Value = acquit.AnneeId;
            row["COM_ID"].Value = acquit.CommuneId;
            row["PRO_ID"].Value = acquit.ProprietaireId;
            row["ACQ_NUMERO"].Value = acquit.Numero;
            row["ACQ_DATE"].Value = acquit.Date;
            row["ACQ_SURFACE"].Value = acquit.Surface;
            row["ACQ_LITRES"].Value = acquit.Litres;
            row["REG_ID"].Value = acquit.RegionId;
            if (acquit.DateReception == null)
            {
                row["ACQ_RECEPTION"].Value = DBNull.Value;
            }
            else
            {
                row["ACQ_RECEPTION"].Value = acquit.DateReception;
            }
            if (acquit.NumeroComplementaire != null)
            {
                row["ACQ_NUMERO_COMPL"].Value = acquit.NumeroComplementaire;
            }
            row["PRO_ID_VENDANGE"].Value = acquit.ProprietaireVendangeId;
        }

        #endregion Methods

        #region event

        private void BtnAddAcquit_Click(object sender, EventArgs e)
        {
            try
            {
                var formAcquit = new WfrmSaisieAcquit(this);
                formAcquit.WfrmShowDialog();
                UpdateDonnees();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnModifyAcquit_Click(object sender, EventArgs e)
        {
            try
            {
                if (aCQ_ACQUITDataGridView.CurrentRow != null)
                {
                    var formAddAcquit = new WfrmSaisieAcquit(this, DataGridViewCellCollectionToAcquit(aCQ_ACQUITDataGridView.CurrentRow.Cells));
                    formAddAcquit.WfrmShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnDeleteAcquit_Click(object sender, EventArgs e)
        {
            try
            {
                if (aCQ_ACQUITDataGridView.CurrentRow != null)
                {
                    int? acqId = (int?)aCQ_ACQUITDataGridView.CurrentRow.Cells["ACQ_ID"].Value;
                    if (acqId != null)
                    {
                        if (aCQ_ACQUITTableAdapter.NbRef((int)acqId) == 0)
                        {
                            if (MessageBox.Show("Êtes vous sûr de vouloir supprimer cet acquit?", "Suppression d'un acquit", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                            {
                                aCQ_ACQUITBindingSource.RemoveCurrent();
                                SaveDataGrid();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Suppression de l'acquit impossible.\n" +
                            "Des pesées ont été enregistrées pour cet acquit!", "Suppression impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void CmdReportAcquits_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante);
        }

        private void BtnDroitsProduction_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDroitsDeProduction(Utilities.IdAnneeCourante);
        }

        private void ACQ_ACQUITDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool isSorted = true;
            int idAn = Utilities.IdAnneeCourante;
            switch (e.ColumnIndex)
            {
                case 4:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderPro(dSPesees.ACQ_ACQUIT, idAn);
                    break;

                case 5:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderProVen(dSPesees.ACQ_ACQUIT, idAn);
                    break;

                case 7:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderReg(dSPesees.ACQ_ACQUIT, idAn);
                    break;

                case 8:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderCom(dSPesees.ACQ_ACQUIT, idAn);
                    break;

                default:
                    isSorted = false;
                    break;
            }

            if (isSorted)
            {
                foreach (DataGridViewColumn column in aCQ_ACQUITDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                aCQ_ACQUITDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void CbAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAnnee.SelectedValue != null && Convert.ToInt32(cbAnnee.SelectedValue) != Utilities.IdAnneeCourante)
            {
                Utilities.IdAnneeCourante = Convert.ToInt32(cbAnnee.SelectedValue);
                UpdateDonnees();
            }
        }

        private void ACQ_ACQUITDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == aCQ_ACQUITDataGridView.Columns["Kilos"].Index)
            {
                int couleurId = -1;
                decimal rateConv = -1;
                if (aCQ_ACQUITDataGridView.Rows[e.RowIndex].Cells["ACQ_ID"].Value != null)
                {
                    int id = (int)aCQ_ACQUITDataGridView.Rows[e.RowIndex].Cells["ACQ_ID"].Value;
                    couleurId = (int)aCQ_ACQUITTableAdapter.GetCouleurIdByACQ_ID(id);
                }

                switch (couleurId)
                {
                    case 1: //BLANC
                        rateConv = GlobalParam.Instance.TauxConversionBlanc;
                        break;

                    case 2: //ROUGE
                        rateConv = GlobalParam.Instance.TauxConversionRouge;
                        break;

                    default:
                        rateConv = -1;
                        break;
                }

                e.FormattingApplied = true;
                DataGridViewRow row = aCQ_ACQUITDataGridView.Rows[e.RowIndex];
                decimal kilo = -1;
                if (row.Cells["ACQ_LITRES"].Value != null)
                {
                    kilo = Convert.ToDecimal(row.Cells["ACQ_LITRES"].Value) / rateConv;
                }
                e.Value = kilo.ToString("0");
            }
        }

        private void ToolPrintListeAcquitsVendange_Click_1(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquitsVendangeCepage(Utilities.IdAnneeCourante);
        }

        private void AcquitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante);
        }

        private void ListeDesAcquitsDeVendangeParCépageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquitsVendangeCepage(Utilities.IdAnneeCourante);
        }

        private void ListeDesAcquitsDeVendangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquitsVendange(Utilities.IdAnneeCourante);
        }

        private void BtnVisiteVignes_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeParcellesVisiteVignes(Utilities.IdAnneeCourante);
        }

        #endregion event
    }
}