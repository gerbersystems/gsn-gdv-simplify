﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using System.IO;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire d'ajout de lignes manuelles aux paiements
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmAjoutPaiementLigneManuelle : WfrmBase, ICtrlSetting
    {
        #region properties

        private ParametrePaiement _parametrePaiement;

        //private static WfrmAjoutPaiementLigneManuelle _wfrm;
        private FormSettingsManagerPaiementManuel _manager;

        public bool CanDelete
        {
            get { return true; }
        }

        public Form GetActiveForm()
        {
            return this;
        }

        #endregion properties

        #region Constructor

        public WfrmAjoutPaiementLigneManuelle(ParametrePaiement parametrePaiement)
        {
            InitializeComponent();
            _parametrePaiement = parametrePaiement;

            foreach (string producteur in parametrePaiement.Producteurs)
                cbProducteur.Items.Add(producteur);
        }

        private void WfrmAjoutPaiementLigneManuel_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_PROPRIETAIRETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            this.mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(this.dSPesees.MOC_MODE_COMPTABILISATION, _parametrePaiement.AnnId);
            this.pRO_PROPRIETAIRETableAdapter.Fill(this.dSPesees.PRO_PROPRIETAIRE);
            this.pRO_NOMCOMPLETTableAdapter.Fill(this.dSPesees.PRO_NOMCOMPLET);

            _manager = new FormSettingsManagerPaiementManuel(form: this
                                                           , focusControl: txtTexte
                                                           , btnNew: btnAddCepage
                                                           , btnUpdate: btnSaveCepage
                                                           , btnDelete: btnSupprimer
                                                           , dataGridView: dgwLigneManuelle
                                                           , ligneManuelles: _parametrePaiement.LigneManuelles);

            _manager.AddControl(txtTexte);
            _manager.AddControl(mOC_IDComboBox);

            _manager.AddControl(optTous);
            _manager.AddControl(cbProducteur);
            _manager.AddControl(optProprietaire);

            _manager.AddControl(optFixe);
            _manager.AddControl(optPourcentage);
            _manager.AddControl(txtMontantFixe);
            _manager.AddControl(numPourcentage);
            _manager.AddControl(labelTexte);

            _manager.AddControl(gpbExportImport);
        }

        #endregion Constructor

        public PaiementLigneManuelle GetLigneManuelle()
        {
            PaiementLigneManuelle ligneManuelle;
            string text = txtTexte.Text;
            bool montantIsFixe = optFixe.Checked;
            decimal valeur;

            if (optFixe.Checked)
            {
                try
                {
                    valeur = Math.Round(decimal.Parse(txtMontantFixe.Text), 2);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Veuillez saisir un montant correct.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    throw e;
                }
            }
            else
            {
                valeur = Math.Round(numPourcentage.Value, 2);
            }

            if (optTous.Checked)
            {
                ligneManuelle = new PaiementLigneManuelle(text, montantIsFixe, valeur);
            }
            else
            {
                string producteur = cbProducteur.Text;
                bool trouver = false;

                for (int i = 0; !trouver && i < cbProducteur.Items.Count; i++)
                {
                    if (cbProducteur.Items[i].ToString() == producteur)
                        trouver = true;
                }

                if (!trouver)
                {
                    MessageBox.Show("Veuillez sélectionner un producteur.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    throw new Exception();
                }

                ligneManuelle = new PaiementLigneManuelle(text, montantIsFixe, valeur, producteur);
            }

            try
            {
                ligneManuelle.MocId = (int)mOC_IDComboBox.SelectedValue;
            }
            catch (Exception e)
            {
                MessageBox.Show("Veuillez sélectionner un mode de comptabilisation", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                throw e;
            }

            return ligneManuelle;
        }

        public void SetChamps(PaiementLigneManuelle ligneManuelle)
        {
            txtTexte.Text = ligneManuelle.Texte;
            if (ligneManuelle.ProId == -1)
            {
                optTous.Checked = true;
            }
            else
            {
                optProprietaire.Checked = true;
                cbProducteur.Text = ligneManuelle.Producteur;
            }
            if (ligneManuelle.MontantIsFixe)
            {
                optFixe.Checked = true;
                txtMontantFixe.Text = ligneManuelle.Valeur.ToString();
            }
            else
            {
                optPourcentage.Checked = true;
                numPourcentage.Value = ligneManuelle.Valeur;
            }
            mOC_IDComboBox.SelectedValue = ligneManuelle.MocId;
        }

        private void CheckedOption(object sender, EventArgs e)
        {
            CheckedOption();
        }

        public void CheckedOption()
        {
            cbProducteur.Enabled = (optProprietaire.Checked);
            txtMontantFixe.Enabled = (optFixe.Checked);
            numPourcentage.Enabled = (optPourcentage.Checked);
        }

        private void SaveDonnees()
        {
            _parametrePaiement.ClearLigneManuelle();

            foreach (PaiementLigneManuelle ligne in _manager.LignesManuelles)
                _parametrePaiement.AddLigneManuelle(ligne);
        }

        private void BtnSuivant_Click(object sender, EventArgs e)
        {
            try
            {
                SaveDonnees();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtnPrecedent_Click(object sender, EventArgs e)
        {
            SaveDonnees();
            DialogResult = DialogResult.Retry;
        }

        private void BtnParcourirExport_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
                ExportDestinationPathTextBox.Text = fbd.SelectedPath;
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportCsvFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ExportCsvFile()
        {
            if (ExportTypeCombobox.SelectedIndex == 0)
            {
                MessageBox.Show("Veuillez sélectionner le format pour l'exportation.",
                                "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (ExportDestinationPathTextBox.Text == "")
            {
                MessageBox.Show("Veuillez sélectionner le dossier de destination du fichier à exporter.",
                                "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (!Directory.Exists(ExportDestinationPathTextBox.Text))
            {
                MessageBox.Show("Le chemin du dossier est incorrect.",
                                "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            _manager.ExportCsvFileTo((ExportCsvType)ExportTypeCombobox.SelectedIndex, ExportDestinationPathTextBox.Text);
        }

        private void BtnParcourirImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog()
            {
                Title = "Sélectionner un fichier",
                Filter = "Fichiers tableur|*.csv"
            };
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtParcourirImport.Text = ofd.FileName;
            }
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                ImportCsvFile();
            }
            catch (Exception)
            {
                MessageBox.Show("Le format du fichier\n" + Path.GetFileName(txtParcourirImport.Text) + "\nne correspond pas au format " + ExportTypeCombobox.Text + "\nou\nCertaines valeurs sont manquantes", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ImportCsvFile()
        {
            if (ExportTypeCombobox.SelectedIndex > 0)
            {
                if (txtParcourirImport.Text != "")
                {
                    if (File.Exists(txtParcourirImport.Text))
                        _manager.ImportCsvFileFrom(exportType: (ExportCsvType)ExportTypeCombobox.SelectedIndex,
                                                   filePath: txtParcourirImport.Text,
                                                   modeComptabilisationId: (int)mOC_IDComboBox.SelectedValue,
                                                   typeFormatImport: ExportTypeCombobox.Text);
                    else
                        MessageBox.Show("Le chemin du fichier est incorrect.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Veuillez sélectionner le fichier csv à importer.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner le format pour l'importation.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        internal string[,] GetProducteurIdList()
        {
            return _parametrePaiement.ProducteursIdNom;
        }
    }
}