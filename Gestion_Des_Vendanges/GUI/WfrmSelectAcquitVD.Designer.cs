﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSelectAcquit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nOMCOMPLETLabel1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmSelectAcquit));
            this.label1 = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aCQ_ACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.pRO_PROPRIETAIRETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter();
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.aCQ_ACQUITDataGridView = new System.Windows.Forms.DataGridView();
            this.cLACLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPeseesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOMCOMPLETBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rEGREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pROPROPRIETAIREBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.dsPesees1 = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pRO_NOMCOMPLETFiltreBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbFiltreProducteur = new System.Windows.Forms.ComboBox();
            this.chkProducteurIgnore = new System.Windows.Forms.CheckBox();
            this.prO_NOMCOMPLETTableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.chkCepageIgnore = new System.Windows.Forms.CheckBox();
            this.cbCepage = new System.Windows.Forms.ComboBox();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsMaster1 = new Gestion_Des_Vendanges.DAO.DSMaster();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.chkLieuIgnore = new System.Windows.Forms.CheckBox();
            this.cbLieu = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pARPARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.chkAutreMentionIgnore = new System.Windows.Forms.CheckBox();
            this.cbAutreMention = new System.Windows.Forms.ComboBox();
            this.lICLIEUCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIC_LIEU_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIC_LIEU_COMMUNETableAdapter();
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.peE_PESEE_ENTETETableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PRO_ID_VENDANGE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.droitLitresTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.droitKilogrammesTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoldeAcquit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            nOMCOMPLETLabel1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPeseesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROPROPRIETAIREBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_NOMCOMPLETFiltreBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMaster1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pARPARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lICLIEUCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nOMCOMPLETLabel1
            // 
            nOMCOMPLETLabel1.AutoSize = true;
            nOMCOMPLETLabel1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            nOMCOMPLETLabel1.Location = new System.Drawing.Point(18, 42);
            nOMCOMPLETLabel1.Name = "nOMCOMPLETLabel1";
            nOMCOMPLETLabel1.Size = new System.Drawing.Size(91, 16);
            nOMCOMPLETLabel1.TabIndex = 25;
            nOMCOMPLETLabel1.Text = "Producteur :";
            // 
            // label2
            // 
            label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            label2.AutoSize = true;
            label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label2.Location = new System.Drawing.Point(507, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(68, 16);
            label2.TabIndex = 28;
            label2.Text = "Cépage :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label3.Location = new System.Drawing.Point(18, 71);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(45, 16);
            label3.TabIndex = 31;
            label3.Text = "Lieu :";
            // 
            // label4
            // 
            label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            label4.AutoSize = true;
            label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label4.Location = new System.Drawing.Point(507, 71);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(112, 16);
            label4.TabIndex = 34;
            label4.Text = "Autre mention :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Sélection de l\'acquit";
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdOK.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cmdOK.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cmdOK.Location = new System.Drawing.Point(976, 508);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(2);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 30);
            this.cmdOK.TabIndex = 0;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.btnSelectAcquit_Click);
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aCQ_ACQUITBindingSource
            // 
            this.aCQ_ACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQ_ACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = this.cLA_CLASSETableAdapter;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = this.cOM_COMMUNETableAdapter;
            this.tableAdapterManager.COU_COULEURTableAdapter = this.cOU_COULEURTableAdapter;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = this.pRO_PROPRIETAIRETableAdapter;
            this.tableAdapterManager.REG_REGIONTableAdapter = this.rEG_REGIONTableAdapter;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // pRO_PROPRIETAIRETableAdapter
            // 
            this.pRO_PROPRIETAIRETableAdapter.ClearBeforeFill = true;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // aCQ_ACQUITDataGridView
            // 
            this.aCQ_ACQUITDataGridView.AllowUserToAddRows = false;
            this.aCQ_ACQUITDataGridView.AllowUserToDeleteRows = false;
            this.aCQ_ACQUITDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aCQ_ACQUITDataGridView.AutoGenerateColumns = false;
            this.aCQ_ACQUITDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aCQ_ACQUITDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aCQ_ACQUITDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn5,
            this.PRO_ID_VENDANGE,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn9,
            this.droitLitresTextBoxColumn,
            this.ACQ_ID,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.droitKilogrammesTextBoxColumn,
            this.SoldeAcquit});
            this.aCQ_ACQUITDataGridView.DataSource = this.aCQ_ACQUITBindingSource;
            this.aCQ_ACQUITDataGridView.Location = new System.Drawing.Point(12, 98);
            this.aCQ_ACQUITDataGridView.MultiSelect = false;
            this.aCQ_ACQUITDataGridView.Name = "aCQ_ACQUITDataGridView";
            this.aCQ_ACQUITDataGridView.ReadOnly = true;
            this.aCQ_ACQUITDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.aCQ_ACQUITDataGridView.Size = new System.Drawing.Size(1039, 405);
            this.aCQ_ACQUITDataGridView.TabIndex = 25;
            this.aCQ_ACQUITDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.aCQ_ACQUITDataGridView_CellFormatting);
            this.aCQ_ACQUITDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.aCQ_ACQUITDataGridView_CellContentDoubleClick);
            this.aCQ_ACQUITDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.aCQ_ACQUITDataGridView_ColumnHeaderMouseClick);
            // 
            // cLACLASSEBindingSource
            // 
            this.cLACLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLACLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPeseesBindingSource;
            // 
            // dSPeseesBindingSource
            // 
            this.dSPeseesBindingSource.DataSource = this.dSPesees;
            this.dSPeseesBindingSource.Position = 0;
            // 
            // pRONOMCOMPLETBindingSource1
            // 
            this.pRONOMCOMPLETBindingSource1.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource1.DataSource = this.dSPesees;
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // rEGREGIONBindingSource
            // 
            this.rEGREGIONBindingSource.DataMember = "REG_REGION";
            this.rEGREGIONBindingSource.DataSource = this.dSPesees;
            // 
            // pROPROPRIETAIREBindingSource
            // 
            this.pROPROPRIETAIREBindingSource.DataMember = "PRO_PROPRIETAIRE";
            this.pROPROPRIETAIREBindingSource.DataSource = this.dSPesees;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // dsPesees1
            // 
            this.dsPesees1.DataSetName = "DSPesees";
            this.dsPesees1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRO_NOMCOMPLETFiltreBindingSource
            // 
            this.pRO_NOMCOMPLETFiltreBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRO_NOMCOMPLETFiltreBindingSource.DataSource = this.dsPesees1;
            // 
            // cbFiltreProducteur
            // 
            this.cbFiltreProducteur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFiltreProducteur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFiltreProducteur.DataSource = this.pRO_NOMCOMPLETFiltreBindingSource;
            this.cbFiltreProducteur.DisplayMember = "NOMCOMPLET";
            this.cbFiltreProducteur.Enabled = false;
            this.cbFiltreProducteur.FormattingEnabled = true;
            this.cbFiltreProducteur.Location = new System.Drawing.Point(171, 42);
            this.cbFiltreProducteur.Name = "cbFiltreProducteur";
            this.cbFiltreProducteur.Size = new System.Drawing.Size(330, 21);
            this.cbFiltreProducteur.TabIndex = 26;
            this.cbFiltreProducteur.ValueMember = "PRO_ID";
            this.cbFiltreProducteur.SelectedIndexChanged += new System.EventHandler(this.cbFiltreProducteur_SelectedIndexChanged);
            // 
            // chkProducteurIgnore
            // 
            this.chkProducteurIgnore.AutoSize = true;
            this.chkProducteurIgnore.Checked = true;
            this.chkProducteurIgnore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProducteurIgnore.Location = new System.Drawing.Point(115, 42);
            this.chkProducteurIgnore.Name = "chkProducteurIgnore";
            this.chkProducteurIgnore.Size = new System.Drawing.Size(50, 17);
            this.chkProducteurIgnore.TabIndex = 27;
            this.chkProducteurIgnore.Text = "Tous";
            this.chkProducteurIgnore.UseVisualStyleBackColor = true;
            this.chkProducteurIgnore.CheckedChanged += new System.EventHandler(this.chkProducteurIgnore_CheckedChanged);
            // 
            // prO_NOMCOMPLETTableAdapter1
            // 
            this.prO_NOMCOMPLETTableAdapter1.ClearBeforeFill = true;
            // 
            // chkCepageIgnore
            // 
            this.chkCepageIgnore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkCepageIgnore.AutoSize = true;
            this.chkCepageIgnore.Checked = true;
            this.chkCepageIgnore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCepageIgnore.Location = new System.Drawing.Point(639, 42);
            this.chkCepageIgnore.Name = "chkCepageIgnore";
            this.chkCepageIgnore.Size = new System.Drawing.Size(50, 17);
            this.chkCepageIgnore.TabIndex = 30;
            this.chkCepageIgnore.Text = "Tous";
            this.chkCepageIgnore.UseVisualStyleBackColor = true;
            this.chkCepageIgnore.CheckedChanged += new System.EventHandler(this.chkCepageIgnore_CheckedChanged);
            // 
            // cbCepage
            // 
            this.cbCepage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCepage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCepage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCepage.DataSource = this.cEPCEPAGEBindingSource;
            this.cbCepage.DisplayMember = "CEP_NOM";
            this.cbCepage.Enabled = false;
            this.cbCepage.FormattingEnabled = true;
            this.cbCepage.Location = new System.Drawing.Point(695, 42);
            this.cbCepage.Name = "cbCepage";
            this.cbCepage.Size = new System.Drawing.Size(350, 21);
            this.cbCepage.TabIndex = 29;
            this.cbCepage.ValueMember = "CEP_ID";
            this.cbCepage.SelectionChangeCommitted += new System.EventHandler(this.cbCepage_SelectionChangeCommitted);
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // dsMaster1
            // 
            this.dsMaster1.DataSetName = "DSMaster";
            this.dsMaster1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // chkLieuIgnore
            // 
            this.chkLieuIgnore.AutoSize = true;
            this.chkLieuIgnore.Checked = true;
            this.chkLieuIgnore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLieuIgnore.Location = new System.Drawing.Point(115, 71);
            this.chkLieuIgnore.Name = "chkLieuIgnore";
            this.chkLieuIgnore.Size = new System.Drawing.Size(50, 17);
            this.chkLieuIgnore.TabIndex = 33;
            this.chkLieuIgnore.Text = "Tous";
            this.chkLieuIgnore.UseVisualStyleBackColor = true;
            this.chkLieuIgnore.CheckedChanged += new System.EventHandler(this.chkLieuIgnore_CheckedChanged);
            // 
            // cbLieu
            // 
            this.cbLieu.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLieu.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLieu.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.cbLieu.DisplayMember = "LIE_NOM";
            this.cbLieu.Enabled = false;
            this.cbLieu.FormattingEnabled = true;
            this.cbLieu.Location = new System.Drawing.Point(171, 71);
            this.cbLieu.Name = "cbLieu";
            this.cbLieu.Size = new System.Drawing.Size(330, 21);
            this.cbLieu.TabIndex = 32;
            this.cbLieu.ValueMember = "LIE_ID";
            this.cbLieu.SelectionChangeCommitted += new System.EventHandler(this.cbLieu_SelectionChangeCommitted);
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // pARPARCELLEBindingSource
            // 
            this.pARPARCELLEBindingSource.DataMember = "PAR_PARCELLE";
            this.pARPARCELLEBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // chkAutreMentionIgnore
            // 
            this.chkAutreMentionIgnore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAutreMentionIgnore.AutoSize = true;
            this.chkAutreMentionIgnore.Checked = true;
            this.chkAutreMentionIgnore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutreMentionIgnore.Location = new System.Drawing.Point(639, 71);
            this.chkAutreMentionIgnore.Name = "chkAutreMentionIgnore";
            this.chkAutreMentionIgnore.Size = new System.Drawing.Size(50, 17);
            this.chkAutreMentionIgnore.TabIndex = 36;
            this.chkAutreMentionIgnore.Text = "Tous";
            this.chkAutreMentionIgnore.UseVisualStyleBackColor = true;
            this.chkAutreMentionIgnore.CheckedChanged += new System.EventHandler(this.chkAutreMentionIgnore_CheckedChanged);
            // 
            // cbAutreMention
            // 
            this.cbAutreMention.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAutreMention.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbAutreMention.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbAutreMention.DataSource = this.cEPCEPAGEBindingSource;
            this.cbAutreMention.DisplayMember = "CEP_NOM";
            this.cbAutreMention.Enabled = false;
            this.cbAutreMention.FormattingEnabled = true;
            this.cbAutreMention.Location = new System.Drawing.Point(695, 71);
            this.cbAutreMention.Name = "cbAutreMention";
            this.cbAutreMention.Size = new System.Drawing.Size(350, 21);
            this.cbAutreMention.TabIndex = 35;
            this.cbAutreMention.ValueMember = "CEP_ID";
            this.cbAutreMention.SelectionChangeCommitted += new System.EventHandler(this.cbAutreMention_SelectionChangeCommitted);
            // 
            // lICLIEUCOMMUNEBindingSource
            // 
            this.lICLIEUCOMMUNEBindingSource.DataMember = "LIC_LIEU_COMMUNE";
            this.lICLIEUCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // lIC_LIEU_COMMUNETableAdapter
            // 
            this.lIC_LIEU_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // peE_PESEE_ENTETETableAdapter1
            // 
            this.peE_PESEE_ENTETETableAdapter1.ClearBeforeFill = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ACQ_NUMERO";
            this.dataGridViewTextBoxColumn7.HeaderText = "Numéro";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 80;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ACQ_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn8.HeaderText = "Date";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 80;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CLA_ID";
            this.dataGridViewTextBoxColumn2.DataSource = this.cLACLASSEBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "CLA_NOM";
            this.dataGridViewTextBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn2.HeaderText = "Classe";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "CLA_ID";
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "COU_ID";
            this.dataGridViewTextBoxColumn3.DataSource = this.cOUCOULEURBindingSource;
            this.dataGridViewTextBoxColumn3.DisplayMember = "COU_NOM";
            this.dataGridViewTextBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn3.HeaderText = "Couleur";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn3.ValueMember = "COU_ID";
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COM_ID";
            this.dataGridViewTextBoxColumn5.DataSource = this.cOMCOMMUNEBindingSource;
            this.dataGridViewTextBoxColumn5.DisplayMember = "COM_NOM";
            this.dataGridViewTextBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn5.HeaderText = "Commune";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn5.ValueMember = "COM_ID";
            // 
            // PRO_ID_VENDANGE
            // 
            this.PRO_ID_VENDANGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_ID_VENDANGE.DataPropertyName = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.DataSource = this.pRONOMCOMPLETBindingSource1;
            this.PRO_ID_VENDANGE.DisplayMember = "NOMCOMPLET";
            this.PRO_ID_VENDANGE.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PRO_ID_VENDANGE.HeaderText = "Producteur";
            this.PRO_ID_VENDANGE.MinimumWidth = 100;
            this.PRO_ID_VENDANGE.Name = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.ReadOnly = true;
            this.PRO_ID_VENDANGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PRO_ID_VENDANGE.ValueMember = "PRO_ID";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PRO_ID";
            this.dataGridViewTextBoxColumn6.DataSource = this.pRONOMCOMPLETBindingSource;
            this.dataGridViewTextBoxColumn6.DisplayMember = "NOMCOMPLET";
            this.dataGridViewTextBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn6.HeaderText = "Propriétaire";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn6.ValueMember = "PRO_ID";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ACQ_SURFACE";
            this.dataGridViewTextBoxColumn9.HeaderText = "Surface";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 60;
            // 
            // droitLitresTextBoxColumn
            // 
            this.droitLitresTextBoxColumn.DataPropertyName = "ACQ_LITRES";
            this.droitLitresTextBoxColumn.HeaderText = "Droit(L)";
            this.droitLitresTextBoxColumn.Name = "droitLitresTextBoxColumn";
            this.droitLitresTextBoxColumn.ReadOnly = true;
            this.droitLitresTextBoxColumn.Width = 60;
            // 
            // ACQ_ID
            // 
            this.ACQ_ID.DataPropertyName = "ACQ_ID";
            this.ACQ_ID.HeaderText = "ACQ_ID";
            this.ACQ_ID.Name = "ACQ_ID";
            this.ACQ_ID.ReadOnly = true;
            this.ACQ_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ANN_ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "ANN_ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ACQ_NUMERO_COMPL";
            this.dataGridViewTextBoxColumn12.HeaderText = "ACQ_NUMERO_COMPL";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "PRO_ID_VENDANGE";
            this.dataGridViewTextBoxColumn13.HeaderText = "PRO_ID_VENDANGE";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // droitKilogrammesTextBoxColumn
            // 
            this.droitKilogrammesTextBoxColumn.HeaderText = "Droit(Kg)";
            this.droitKilogrammesTextBoxColumn.Name = "droitKilogrammesTextBoxColumn";
            this.droitKilogrammesTextBoxColumn.ReadOnly = true;
            // 
            // SoldeAcquit
            // 
            this.SoldeAcquit.HeaderText = "Solde";
            this.SoldeAcquit.Name = "SoldeAcquit";
            this.SoldeAcquit.ReadOnly = true;
            // 
            // WfrmSelectAcquit
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(1064, 549);
            this.Controls.Add(this.chkAutreMentionIgnore);
            this.Controls.Add(label4);
            this.Controls.Add(this.cbAutreMention);
            this.Controls.Add(this.chkLieuIgnore);
            this.Controls.Add(label3);
            this.Controls.Add(this.cbLieu);
            this.Controls.Add(this.chkCepageIgnore);
            this.Controls.Add(label2);
            this.Controls.Add(this.cbCepage);
            this.Controls.Add(this.chkProducteurIgnore);
            this.Controls.Add(nOMCOMPLETLabel1);
            this.Controls.Add(this.cbFiltreProducteur);
            this.Controls.Add(this.aCQ_ACQUITDataGridView);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 300);
            this.Name = "WfrmSelectAcquit";
            this.Text = "Sélection acquit";
            this.Load += new System.EventHandler(this.WfrmSelectAcquit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPeseesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROPROPRIETAIREBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_NOMCOMPLETFiltreBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMaster1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pARPARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lICLIEUCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdOK;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aCQ_ACQUITBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView aCQ_ACQUITDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
        private System.Windows.Forms.BindingSource cLACLASSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.BindingSource dSPeseesBindingSource;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter pRO_PROPRIETAIRETableAdapter;
        private System.Windows.Forms.BindingSource pROPROPRIETAIREBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
        private System.Windows.Forms.BindingSource rEGREGIONBindingSource;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource1;
        private DAO.DSPesees dsPesees1;
        private System.Windows.Forms.BindingSource pRO_NOMCOMPLETFiltreBindingSource;
        private System.Windows.Forms.ComboBox cbFiltreProducteur;
        private System.Windows.Forms.CheckBox chkProducteurIgnore;
        private DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter prO_NOMCOMPLETTableAdapter1;
        private System.Windows.Forms.CheckBox chkCepageIgnore;
        private System.Windows.Forms.ComboBox cbCepage;
        private DAO.DSMaster dsMaster1;
		private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
		private DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
		private System.Windows.Forms.CheckBox chkLieuIgnore;
		private System.Windows.Forms.ComboBox cbLieu;
		private System.Windows.Forms.BindingSource pARPARCELLEBindingSource;
		private DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
		private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
		private DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
		private System.Windows.Forms.CheckBox chkAutreMentionIgnore;
		private System.Windows.Forms.ComboBox cbAutreMention;
		private System.Windows.Forms.BindingSource lICLIEUCOMMUNEBindingSource;
		private DAO.DSPeseesTableAdapters.LIC_LIEU_COMMUNETableAdapter lIC_LIEU_COMMUNETableAdapter;
		private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter peE_PESEE_ENTETETableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn PRO_ID_VENDANGE;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn droitLitresTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn droitKilogrammesTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoldeAcquit;
    }
}