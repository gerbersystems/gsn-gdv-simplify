﻿using Gestion_Des_Vendanges.BUSINESS;
using GSN.GDV.Reports;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire contenant la barre de menu de l'application (les autres formulaire hérite de celui-ci)
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public partial class WfrmMenu : WfrmBase
    {
        #region Constructor

        public WfrmMenu()
        {
            InitializeComponent();
            InitializeEvents();
        }

        private void InitializeEvents()
        {
            ExportAcquitsToolStripMenuItem.Click += (s, e) => Export.Acquits.ToCSV();
            ExportPeseesToolStripMenuItem.Click += (s, e) => Export.Pesees.ToCSV();
        }

        #endregion Constructor

        protected virtual void UpdateDonnees()
        {
        }

        private void QuitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous quitter la gestion des vendanges ?", "Quitter", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void ShowWfrmSettings(WfrmSetting wfrm)
        {
            wfrm.WfrmShowDialog();
            UpdateDonnees();
        }

        private void CommuneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesCommunes.Wfrm);
        }

        private void ProprietaireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesProprietaires.Wfrm);
        }

        private void AnToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesAnnees.Wfrm);
        }

        private void CepToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesCepages.Wfrm);
        }

        private void GestionDesAdressesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesAdresses.Wfrm);
        }

        private void ClasToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesClasses.Wfrm);
        }

        private void GestionDesCouleursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesCouleurs.Wfrm);
        }

        private void GestionDesDegrésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesDegres.Wfrm);
        }

        private void ChangerCleToolStrip_Click(object sender, EventArgs e)
        {
            LICENSE.LicenseManager.UpdateCle();
        }

        private void AProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void ListeAcquitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void AttestationsDeControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void GestionDesLieuxDeProductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesLieuxDeProductions.Wfrm);
        }

        private void CepToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesCepages.Wfrm);
        }

        private void GestionDesMentionsParticulièresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesAutresMentions.Wfrm);
        }

        #region AttestationsDeControle events

        private void AttestationsDeControleStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(Utilities.IdAnneeCourante);
        }

        private void DocAttestationsControleStripMenuItem3_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        private void XlsAttestationsControleStripMenuItem3_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void PdfAttestationsControleStripMenuItem3_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void ApercuAttestationsControleStripMenuItem3_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowAttestationsControle(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        #endregion AttestationsDeControle events

        #region Acquits events

        private void ListeAcquitsStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante);
        }

        private void ApercuListeAcquitsStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void PdfListeAcquitsStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void XlsListeAcquitsStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void DocListeAcquitsStripMenuItem2_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeAcquits(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion Acquits events

        #region TablesDesPrix events

        private void TablesDesPrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowTablesDesPrix(Utilities.IdAnneeCourante);
        }

        private void ApercutablesDesPrix_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowTablesDesPrix(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void PDFtablesDesPrix_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowTablesDesPrix(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void ExceltablesDesPrix_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowTablesDesPrix(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void WordtablesDesPrix_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowTablesDesPrix(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion TablesDesPrix events

        private void DeclarationDencavageOCVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BUSINESS.RapportCSV rapport = new RapportCSV();
            rapport.SetDeclarationEncavageOCV(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void AperçuAvantFactures_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowPaiements(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void FacturesFournisseursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowPaiements(Utilities.IdAnneeCourante);
        }

        #region Facture events

        private void PDFFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowPaiements(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void XlsFacture_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowPaiements(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void FacturemicrosoftWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowPaiements(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion Facture events

        private void DossierWinBIZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new WfrmWinBizFolder().WfrmShowDialog();
        }

        private void GlobalParamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new WfrmGlobalSettings().WfrmShowDialog();
        }

        private void RestaurerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BUSINESS.SauvegardeManager saveManager = new BUSINESS.SauvegardeManager();
            if (saveManager.Restaurer())
                UpdateDonnees();
        }

        private void SauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new BUSINESS.SauvegardeManager().Sauvegarder();
        }

        private void ToolTeleAssitance_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(@"Resources\AMMYY_Admin.exe");
            }
            catch (Win32Exception)
            {
                //Se produit si l'utilisateur annule le lancement du programme
            }
        }

        private void ToolDriverOLEDB_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(@"Resources\VFPOLEDBSetup.msi");
            }
            catch (Win32Exception)
            {
                //Se produit si l'utilisateur annule le lancement du programme
            }
        }

        private void ChangerDeDossierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmSelectionBd frmSelectBd = new WfrmSelectionBd();
            if (frmSelectBd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string oldDbName = DAO.ConnectionManager.Instance.GvDBName;
                try
                {
                    DAO.ConnectionManager.Instance.ChangeDBGv(frmSelectBd.NomSelection);
                    GlobalParam.Instance = null;
                    new DAO.BDVendangeControleur().UpdateBD();
                    UpdateTitre();
                    UpdateDonnees();
                }
                catch (Exception)
                {
                    if (!CanRestoreToLastDB(oldDbName))
                    {
                        MessageBox.Show("La tentative de reconnexion à l'ancien dossier a échoué.\n" + oldDbName, "Erreur : restauration à l'ancien dossier", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        throw;
                    }
                }
            }
        }

        private bool CanRestoreToLastDB(string oldDbName)
        {
            if (!String.IsNullOrWhiteSpace(oldDbName))
            {
                DAO.ConnectionManager.Instance.ChangeDBGv(oldDbName);
                return true;
            }
            return false;
        }

        protected void UpdateTitre()
        {
            Text = "Gestion des vendanges - " + DAO.ConnectionManager.Instance.GvDBDescription;
        }

        private void ToolStripGererLesDossiers_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesDossiers.Wfrm);
        }

        #region LivraisonVendanges_Date_Kilo events

        private void VendangesLivreesParDateEnKilos_AperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnKilos(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void VendangesLivreesParDateEnKilos_PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnKilos(BUSINESS.Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void VendangesLivreesParDateEnKilos_ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnKilos(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void VendangesLivreesParDateEnKilos_WordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnKilos(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion LivraisonVendanges_Date_Kilo events

        #region LivraisonVendanges_Date_Litres events

        private void VendangesLivreesParDateEnLitres_AperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnLitres(aNNID: BUSINESS.Utilities.IdAnneeCourante);
        }

        private void VendangesLivreesParDateEnLitres_PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnLitres(aNNID: BUSINESS.Utilities.IdAnneeCourante, format: FormatReport.PDF);
        }

        private void VendangesLivreesParDateEnLitres_ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnLitres(aNNID: BUSINESS.Utilities.IdAnneeCourante, format: FormatReport.Excel);
        }

        private void VendangesLivreesParDateEnLitres_WordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnLitres(aNNID: BUSINESS.Utilities.IdAnneeCourante, format: FormatReport.Word);
        }

        #endregion LivraisonVendanges_Date_Litres events

        #region LivraisonVendanges_Producteur events

        private void VendangesLivreesParProducteur_AperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParProducteur(Utilities.IdAnneeCourante, format: FormatReport.Apercu);
        }

        private void VendangesLivreesParProducteur_PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParProducteur(Utilities.IdAnneeCourante, format: FormatReport.PDF);
        }

        private void VendangesLivreesParProducteur_ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParProducteur(Utilities.IdAnneeCourante, format: FormatReport.Excel);
        }

        private void VendangesLivreesParProducteur_WordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParProducteur(Utilities.IdAnneeCourante, format: FormatReport.Word);
        }

        #endregion LivraisonVendanges_Producteur events

        #region LivraisonVendanges_Cepage events

        private void VendangesLivreesParCepage_AperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowVendangesParCepage(Utilities.IdAnneeCourante, format: FormatReport.Apercu);
        }

        private void VendangesLivreesParCepage_PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowVendangesParCepage(Utilities.IdAnneeCourante, format: FormatReport.PDF);
        }

        private void VendangesLivreesParCepage_ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowVendangesParCepage(Utilities.IdAnneeCourante, format: FormatReport.Excel);
        }

        private void VendangesLivreesParCepage_WordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowVendangesParCepage(Utilities.IdAnneeCourante, format: FormatReport.Word);
        }

        #endregion LivraisonVendanges_Cepage events

        #region LivraisonVendanges_LieuDeProduction

        private void VendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParLieuDeProduction(annID: Utilities.IdAnneeCourante, format: FormatReport.Apercu);
        }

        private void VendangesLivreesParLieuDeProduction_PDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParLieuDeProduction(annID: Utilities.IdAnneeCourante, format: FormatReport.PDF);
        }

        private void VendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParLieuDeProduction(annID: Utilities.IdAnneeCourante, format: FormatReport.Excel);
        }

        private void VendangesLivreesParLieuDeProduction_WordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParLieuDeProduction(annID: Utilities.IdAnneeCourante, format: FormatReport.Word);
        }

        #endregion LivraisonVendanges_LieuDeProduction

        #region SondageMoyenParCepage events

        private void SondageMoyenParCepageAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyen(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void SondageMoyenParCepagePDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyen(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void SondageMoyenParCepagExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyen(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void SondageMoyenParCepageWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyen(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion SondageMoyenParCepage events

        #region SondageMoyenParCepageLieuDeProduction events

        private void SondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyenParLieuDeProduction(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void SondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyenParLieuDeProduction(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void SondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyenParLieuDeProduction(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void SondageMoyenParCepageLieuDeProductionWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowSondageMoyenParLieuDeProduction(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion SondageMoyenParCepageLieuDeProduction events

        #region ApportsParProducteurKilos events

        private void ApportsParProducteurKilosAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteur(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void ApportsParProducteurKilosPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteur(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void ApportsParProducteurKilosExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteur(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void ApportsParProducteurKilosWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteur(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion ApportsParProducteurKilos events

        #region ApportsParProducteurKilosDetailSondageOe

        private void ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurGrpByOe(Utilities.IdAnneeCourante);
        }

        private void ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurGrpByOe(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurGrpByOe(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurGrpByOe(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion ApportsParProducteurKilosDetailSondageOe

        #region ApportsParProducteurKilosLitres events

        private void ApportsParProducteurKilosLitresAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurMoutRaisin(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void ApportsParProducteurKilosLitresPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurMoutRaisin(BUSINESS.Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void ApportsParProducteurKilosLitresExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurMoutRaisin(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void ApportsParProducteurKilosLitresWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowApportsProducteurMoutRaisin(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion ApportsParProducteurKilosLitres events

        #region DroitsProd events

        private void DroitsProductionAperçuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDroitsDeProduction(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void DroitsProductionPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDroitsDeProduction(BUSINESS.Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void DroitsProductionExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDroitsDeProduction(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void DroitsProductionWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDroitsDeProduction(BUSINESS.Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion DroitsProd events
    }
}