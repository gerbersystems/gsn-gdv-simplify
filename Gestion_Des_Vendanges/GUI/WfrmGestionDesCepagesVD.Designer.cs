﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesCepages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cOU_IDLabel;
            System.Windows.Forms.Label cEP_NOMLabel;
            System.Windows.Forms.Label label1;
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.cEP_CEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.cEP_IDTextBox = new System.Windows.Forms.TextBox();
            this.cEP_NOMTextBox = new System.Windows.Forms.TextBox();
            this.cOU_IDComboBox = new System.Windows.Forms.ComboBox();
            this.CouleursContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxCouleursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAddCepage = new System.Windows.Forms.Button();
            this.btnSaveCepage = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cEP_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.gbModeComtabilisation = new System.Windows.Forms.GroupBox();
            this.optSelonCouleur = new System.Windows.Forms.RadioButton();
            this.optSpecifique = new System.Windows.Forms.RadioButton();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.cEP_CEPAGEDataGridView = new System.Windows.Forms.DataGridView();
            this.CEP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEP_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            cOU_IDLabel = new System.Windows.Forms.Label();
            cEP_NOMLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEP_CEPAGEBindingSource)).BeginInit();
            this.CouleursContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbModeComtabilisation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEP_CEPAGEDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cOU_IDLabel
            // 
            cOU_IDLabel.AutoSize = true;
            cOU_IDLabel.Location = new System.Drawing.Point(5, 87);
            cOU_IDLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            cOU_IDLabel.Name = "cOU_IDLabel";
            cOU_IDLabel.Size = new System.Drawing.Size(52, 13);
            cOU_IDLabel.TabIndex = 14;
            cOU_IDLabel.Text = "Couleur : ";
            // 
            // cEP_NOMLabel
            // 
            cEP_NOMLabel.AutoSize = true;
            cEP_NOMLabel.Location = new System.Drawing.Point(5, 63);
            cEP_NOMLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            cEP_NOMLabel.Name = "cEP_NOMLabel";
            cEP_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cEP_NOMLabel.TabIndex = 16;
            cEP_NOMLabel.Text = "Nom : ";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(5, 38);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(50, 13);
            label1.TabIndex = 18;
            label1.Text = "Numéro :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cEP_CEPAGEBindingSource
            // 
            this.cEP_CEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEP_CEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = this.cEP_CEPAGETableAdapter;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = this.cOU_COULEURTableAdapter;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // cEP_IDTextBox
            // 
            this.cEP_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.cEP_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cEP_CEPAGEBindingSource, "CEP_ID", true));
            this.cEP_IDTextBox.Enabled = false;
            this.cEP_IDTextBox.Location = new System.Drawing.Point(223, 10);
            this.cEP_IDTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.cEP_IDTextBox.Name = "cEP_IDTextBox";
            this.cEP_IDTextBox.Size = new System.Drawing.Size(92, 20);
            this.cEP_IDTextBox.TabIndex = 13;
            // 
            // cEP_NOMTextBox
            // 
            this.cEP_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cEP_CEPAGEBindingSource, "CEP_NOM", true));
            this.cEP_NOMTextBox.Location = new System.Drawing.Point(113, 60);
            this.cEP_NOMTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.cEP_NOMTextBox.Name = "cEP_NOMTextBox";
            this.cEP_NOMTextBox.Size = new System.Drawing.Size(232, 20);
            this.cEP_NOMTextBox.TabIndex = 2;
            // 
            // cOU_IDComboBox
            // 
            this.cOU_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cOU_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cOU_IDComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.cOU_IDComboBox.ContextMenuStrip = this.CouleursContextMenuStrip;
            this.cOU_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cEP_CEPAGEBindingSource, "COU_ID", true));
            this.cOU_IDComboBox.DataSource = this.cOUCOULEURBindingSource;
            this.cOU_IDComboBox.DisplayMember = "COU_NOM";
            this.cOU_IDComboBox.FormattingEnabled = true;
            this.cOU_IDComboBox.Location = new System.Drawing.Point(113, 84);
            this.cOU_IDComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.cOU_IDComboBox.Name = "cOU_IDComboBox";
            this.cOU_IDComboBox.Size = new System.Drawing.Size(232, 21);
            this.cOU_IDComboBox.TabIndex = 3;
            this.cOU_IDComboBox.ValueMember = "COU_ID";
            this.cOU_IDComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.COU_IDComboBox_KeyPress);
            // 
            // CouleursContextMenuStrip
            // 
            this.CouleursContextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.CouleursContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxCouleursToolStripMenuItem});
            this.CouleursContextMenuStrip.Name = "CouleursContextMenuStrip";
            this.CouleursContextMenuStrip.Size = new System.Drawing.Size(193, 26);
            // 
            // accéderAuxCouleursToolStripMenuItem
            // 
            this.accéderAuxCouleursToolStripMenuItem.Name = "accéderAuxCouleursToolStripMenuItem";
            this.accéderAuxCouleursToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.accéderAuxCouleursToolStripMenuItem.Text = "Accéder aux couleurs ...";
            this.accéderAuxCouleursToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxCouleursToolStripMenuItem_Click);
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // btnAddCepage
            // 
            this.btnAddCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddCepage.Location = new System.Drawing.Point(113, 218);
            this.btnAddCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddCepage.Name = "btnAddCepage";
            this.btnAddCepage.Size = new System.Drawing.Size(75, 30);
            this.btnAddCepage.TabIndex = 5;
            this.btnAddCepage.Text = "Nouveau";
            this.btnAddCepage.UseVisualStyleBackColor = false;
            // 
            // btnSaveCepage
            // 
            this.btnSaveCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveCepage.Location = new System.Drawing.Point(192, 218);
            this.btnSaveCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveCepage.Name = "btnSaveCepage";
            this.btnSaveCepage.Size = new System.Drawing.Size(75, 30);
            this.btnSaveCepage.TabIndex = 6;
            this.btnSaveCepage.Text = "Modifier";
            this.btnSaveCepage.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.cEP_NUMEROTextBox);
            this.groupBox1.Controls.Add(this.gbModeComtabilisation);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.cEP_IDTextBox);
            this.groupBox1.Controls.Add(this.btnSaveCepage);
            this.groupBox1.Controls.Add(this.btnAddCepage);
            this.groupBox1.Controls.Add(this.cOU_IDComboBox);
            this.groupBox1.Controls.Add(cOU_IDLabel);
            this.groupBox1.Controls.Add(cEP_NOMLabel);
            this.groupBox1.Controls.Add(this.cEP_NOMTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 253);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des cépages";
            // 
            // cEP_NUMEROTextBox
            // 
            this.cEP_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cEP_CEPAGEBindingSource, "CEP_NUMERO", true));
            this.cEP_NUMEROTextBox.Location = new System.Drawing.Point(113, 35);
            this.cEP_NUMEROTextBox.Name = "cEP_NUMEROTextBox";
            this.cEP_NUMEROTextBox.Size = new System.Drawing.Size(232, 20);
            this.cEP_NUMEROTextBox.TabIndex = 1;
            // 
            // gbModeComtabilisation
            // 
            this.gbModeComtabilisation.Controls.Add(this.optSelonCouleur);
            this.gbModeComtabilisation.Controls.Add(this.optSpecifique);
            this.gbModeComtabilisation.Controls.Add(this.mOC_IDComboBox);
            this.gbModeComtabilisation.Location = new System.Drawing.Point(6, 110);
            this.gbModeComtabilisation.Name = "gbModeComtabilisation";
            this.gbModeComtabilisation.Size = new System.Drawing.Size(339, 96);
            this.gbModeComtabilisation.TabIndex = 4;
            this.gbModeComtabilisation.TabStop = false;
            this.gbModeComtabilisation.Text = "Mode de comptabilisation";
            // 
            // optSelonCouleur
            // 
            this.optSelonCouleur.AutoSize = true;
            this.optSelonCouleur.Location = new System.Drawing.Point(19, 30);
            this.optSelonCouleur.Name = "optSelonCouleur";
            this.optSelonCouleur.Size = new System.Drawing.Size(104, 17);
            this.optSelonCouleur.TabIndex = 0;
            this.optSelonCouleur.TabStop = true;
            this.optSelonCouleur.Text = " Selon la couleur";
            this.optSelonCouleur.UseVisualStyleBackColor = true;
            // 
            // optSpecifique
            // 
            this.optSpecifique.AutoSize = true;
            this.optSpecifique.Location = new System.Drawing.Point(19, 58);
            this.optSpecifique.Name = "optSpecifique";
            this.optSpecifique.Size = new System.Drawing.Size(14, 13);
            this.optSpecifique.TabIndex = 1;
            this.optSpecifique.TabStop = true;
            this.optSpecifique.UseVisualStyleBackColor = true;
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mOC_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mOC_IDComboBox.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(39, 55);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(168, 21);
            this.mOC_IDComboBox.TabIndex = 2;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(271, 218);
            this.btnSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 7;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // cEP_CEPAGEDataGridView
            // 
            this.cEP_CEPAGEDataGridView.AllowUserToAddRows = false;
            this.cEP_CEPAGEDataGridView.AllowUserToDeleteRows = false;
            this.cEP_CEPAGEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cEP_CEPAGEDataGridView.AutoGenerateColumns = false;
            this.cEP_CEPAGEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.cEP_CEPAGEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cEP_CEPAGEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CEP_ID,
            this.CEP_NUMERO,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn2});
            this.cEP_CEPAGEDataGridView.DataSource = this.cEP_CEPAGEBindingSource;
            this.cEP_CEPAGEDataGridView.Location = new System.Drawing.Point(373, 12);
            this.cEP_CEPAGEDataGridView.Name = "cEP_CEPAGEDataGridView";
            this.cEP_CEPAGEDataGridView.ReadOnly = true;
            this.cEP_CEPAGEDataGridView.Size = new System.Drawing.Size(344, 460);
            this.cEP_CEPAGEDataGridView.TabIndex = 1;
            // 
            // CEP_ID
            // 
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.HeaderText = "CEP_ID";
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Visible = false;
            // 
            // CEP_NUMERO
            // 
            this.CEP_NUMERO.DataPropertyName = "CEP_NUMERO";
            this.CEP_NUMERO.HeaderText = "Numéro";
            this.CEP_NUMERO.Name = "CEP_NUMERO";
            this.CEP_NUMERO.ReadOnly = true;
            this.CEP_NUMERO.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CEP_NOM";
            this.dataGridViewTextBoxColumn3.HeaderText = "Cépage";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "COU_ID";
            this.dataGridViewTextBoxColumn2.DataSource = this.cOUCOULEURBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "COU_NOM";
            this.dataGridViewTextBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn2.HeaderText = "Couleur";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "COU_ID";
            this.dataGridViewTextBoxColumn2.Width = 90;
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesCepages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(729, 484);
            this.Controls.Add(this.cEP_CEPAGEDataGridView);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(700, 350);
            this.Name = "WfrmGestionDesCepages";
            this.Text = "Gestion des cépages";
            this.Load += new System.EventHandler(this.WfrmGestionDesCepages_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEP_CEPAGEBindingSource)).EndInit();
            this.CouleursContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbModeComtabilisation.ResumeLayout(false);
            this.gbModeComtabilisation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEP_CEPAGEDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource cEP_CEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private System.Windows.Forms.TextBox cEP_IDTextBox;
        private System.Windows.Forms.TextBox cEP_NOMTextBox;
        private System.Windows.Forms.ComboBox cOU_IDComboBox;
        private System.Windows.Forms.Button btnAddCepage;
        private System.Windows.Forms.Button btnSaveCepage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.ContextMenuStrip CouleursContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxCouleursToolStripMenuItem;
        private System.Windows.Forms.DataGridView cEP_CEPAGEDataGridView;
        private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
        private System.Windows.Forms.TextBox cEP_NUMEROTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEP_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
        private System.Windows.Forms.GroupBox gbModeComtabilisation;
        private System.Windows.Forms.RadioButton optSelonCouleur;
        private System.Windows.Forms.RadioButton optSpecifique;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
    }
}