﻿using System;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesClasses
    {
        private void InitSpecificTableAdapters()
        {
        }

        private void InitManager()
        {
            _manager = new FormSettingsManagerPesees(this, cLA_IDTextBox, cLA_NUMEROTextBox, btnAddClass, btnSaveClass, btnSupprimer, cLA_CLASSEDataGridView, cLA_CLASSEBindingSource, tableAdapterManager, dSPesees);
        }

        private void btnAddClass_Click(object sender, EventArgs e)
        {
        }
    }
}