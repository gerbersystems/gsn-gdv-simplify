﻿using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de base pour les formulaires utilisant les classes "FormSettingsManager"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmSetting : WfrmBase, ICtrlSetting
    {
        virtual public bool CanDelete
        {
            get { return true; }
        }

        public Form GetActiveForm()
        {
            return this;
        }

        public WfrmSetting()
        {
            InitializeComponent();
        }
    }
}