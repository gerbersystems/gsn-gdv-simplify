﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmWinBizImportAdresse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.lan = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbAnnee
            // 
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(110, 16);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(159, 21);
            this.cbAnnee.TabIndex = 3;
            // 
            // lan
            // 
            this.lan.AutoSize = true;
            this.lan.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lan.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lan.Location = new System.Drawing.Point(12, 17);
            this.lan.Name = "lan";
            this.lan.Size = new System.Drawing.Size(64, 16);
            this.lan.TabIndex = 4;
            this.lan.Text = "Année : ";
            // 
            // cmdCancel
            // 
            this.cmdCancel.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(194, 59);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 30);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Annuler";
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdOK.Location = new System.Drawing.Point(113, 59);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 30);
            this.cmdOK.TabIndex = 7;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.btnImporter_Click);
            // 
            // WfrmWinBizImportAdresse
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(281, 101);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lan);
            this.Controls.Add(this.cbAnnee);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(264, 115);
            this.Name = "WfrmWinBizImportAdresse";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Importation des producteurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.Label lan;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOK;
    }
}