﻿using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire contenant la barre de menu de l'application (les autres formulaire hérite de celui-ci)
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public partial class WfrmBase : Form
    {
        protected bool _licenceIsOk;

        public WfrmBase()
        {
            _licenceIsOk = true;
            InitializeComponent();
        }

        public void WfrmShowDialog()
        {
            if (this.Visible)
            {
                MessageBox.Show("Cette fenêtre est déjà ouverte!", "Ouverture fenêtre", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (_licenceIsOk)
                {
                    ShowInTaskbar = false;
                    ShowDialog();
                }
                Dispose();
            }
        }
    }
}