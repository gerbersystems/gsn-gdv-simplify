﻿using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire affichant un message (sans bouton)
*  Date de création:
*  Modifications:
* */

    public partial class WfrmMessage : Form
    {
        public WfrmMessage(string text, string caption)
        {
            InitializeComponent();
            Cursor = Cursors.WaitCursor;
            lbMessage.Text = text;
            Text = "Gestion des vendanges - " + caption;
            Show();
            Refresh();
        }
    }
}