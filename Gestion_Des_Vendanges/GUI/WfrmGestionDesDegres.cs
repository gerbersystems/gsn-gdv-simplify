﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des degrés
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesDegres : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesDegres _wfrm;

        public static WfrmGestionDesDegres Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesDegres();
                }
                return _wfrm;
            }
        }

        private WfrmGestionDesDegres()
        {
            InitializeComponent();
        }

        private void dEG_DEGRE_OECHSLE_BRIXBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.dEG_DEGRE_OECHSLE_BRIXBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dSPesees);
        }

        private void WfrmGestionDesDegres_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            ConnectionManager.Instance.AddGvTableAdapter(dEG_DEGRE_OECHSLE_BRIXTableAdapter);
            this.dEG_DEGRE_OECHSLE_BRIXTableAdapter.Fill(this.dSPesees.DEG_DEGRE_OECHSLE_BRIX);
            _manager = new FormSettingsManagerPesees(this, dEG_IDTextBox1, dEG_BRIXTextBox1, btnNew, btnModifier, btnDelete,
                dEG_DEGRE_OECHSLE_BRIXDataGridView, dEG_DEGRE_OECHSLE_BRIXBindingSource,
                tableAdapterManager, dSPesees);
            _manager.AddControl(dEG_BRIXTextBox1);
            _manager.AddControl(dEG_OECHSLETextBox1);
        }
    }
}