﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement du formulaire "LigneManuelle"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class FormSettingsManagerLigneManuelle : FormSettingsManagerPesees
    {
        private TextBox _paiIdTextBox;
        private int _paiId;

        public FormSettingsManagerLigneManuelle(WfrmSetting form
                                              , TextBox limIdTextBox
                                              , TextBox paiIdTextBox
                                              , int paiId
                                              , Control focusControl
                                              , Button btnNew
                                              , Button btnUpdate
                                              , Button btnDelete
                                              , DataGridView dataGridView
                                              , BindingSource bindingSource
                                              , TableAdapterManager tableAdapterManager
                                              , DSPesees dsPesees)
            : base(form, limIdTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dsPesees)
        {
            _paiId = paiId;
            _paiIdTextBox = paiIdTextBox;
            paiIdTextBox.Visible = false;
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            _paiIdTextBox.Text = _paiId.ToString();
            base.BtnOk_Click(sender, e);
        }
    }
}