﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashScreen));
            this.lText = new System.Windows.Forms.Label();
            this.timerClose = new System.Windows.Forms.Timer(this.components);
            this.lTitre = new System.Windows.Forms.Label();
            this.lCopyRight = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lText
            // 
            this.lText.AutoSize = true;
            this.lText.BackColor = System.Drawing.Color.Transparent;
            this.lText.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lText.Location = new System.Drawing.Point(80, 50);
            this.lText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lText.Name = "lText";
            this.lText.Size = new System.Drawing.Size(0, 15);
            this.lText.TabIndex = 0;
            // 
            // timerClose
            // 
            this.timerClose.Interval = 5;
            this.timerClose.Tick += new System.EventHandler(this.timerClose_Tick);
            // 
            // lTitre
            // 
            this.lTitre.AutoSize = true;
            this.lTitre.BackColor = System.Drawing.Color.Transparent;
            this.lTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTitre.ForeColor = System.Drawing.Color.White;
            this.lTitre.Location = new System.Drawing.Point(78, 27);
            this.lTitre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lTitre.Name = "lTitre";
            this.lTitre.Size = new System.Drawing.Size(209, 24);
            this.lTitre.TabIndex = 1;
            this.lTitre.Text = "Gestion des vendanges";
            // 
            // lCopyRight
            // 
            this.lCopyRight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lCopyRight.BackColor = System.Drawing.Color.Transparent;
            this.lCopyRight.Font = new System.Drawing.Font("Lucida Sans Unicode", 8F);
            this.lCopyRight.Location = new System.Drawing.Point(11, 216);
            this.lCopyRight.Margin = new System.Windows.Forms.Padding(0);
            this.lCopyRight.Name = "lCopyRight";
            this.lCopyRight.Size = new System.Drawing.Size(335, 30);
            this.lCopyRight.TabIndex = 2;
            this.lCopyRight.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Gestion_Des_Vendanges.Properties.Resources.Splash_screen1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(400, 251);
            this.ControlBox = false;
            this.Controls.Add(this.lCopyRight);
            this.Controls.Add(this.lTitre);
            this.Controls.Add(this.lText);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Lucida Sans Unicode", 7F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "SplashScreen";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SplashScreen";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.SplashScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lText;
        private System.Windows.Forms.Timer timerClose;
        private System.Windows.Forms.Label lTitre;
        private System.Windows.Forms.Label lCopyRight;
    }
}