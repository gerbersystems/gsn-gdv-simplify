﻿using Gestion_Des_Vendanges.BUSINESS;
using GSN.GDV.Data.Extensions;
using GSN.GDV.Helpers;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de sélection d'un acquit
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
*   ATH - 13.11.2009 - Correction si acquit null, suppression de la liaison avec WfrmEntetePesee
* */

    partial class WfrmSelectAcquit
    {
        private void aCQ_ACQUITDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool isSorted = true;
            int idAn = BUSINESS.Utilities.IdAnneeCourante;
            int? proIdVendange = (int?)cbFiltreProducteur.SelectedValue;
            switch (e.ColumnIndex)
            {
                case 4:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    if (chkProducteurIgnore.Checked)
                    {
                        aCQ_ACQUITTableAdapter.FillOrderCom(dSPesees.ACQ_ACQUIT, idAn);
                    }
                    else
                    {
                        aCQ_ACQUITTableAdapter.FillOrderComByProVen(dSPesees.ACQ_ACQUIT, idAn, proIdVendange);
                    }
                    break;

                case 5:
                    if (chkProducteurIgnore.Checked)
                    {
                        aCQ_ACQUITBindingSource.RemoveSort();

                        aCQ_ACQUITTableAdapter.FillOrderProVen(dSPesees.ACQ_ACQUIT, idAn);
                    }
                    else
                    {
                        isSorted = false;
                    }
                    break;

                case 6:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    if (chkProducteurIgnore.Checked)
                    {
                        aCQ_ACQUITTableAdapter.FillOrderPro(dSPesees.ACQ_ACQUIT, idAn);
                    }
                    else
                    {
                        aCQ_ACQUITTableAdapter.FillOrderProByProVen(dSPesees.ACQ_ACQUIT, idAn, proIdVendange);
                    }
                    break;

                default:
                    isSorted = false;
                    break;
            }
            if (isSorted)
            {
                foreach (DataGridViewColumn column in aCQ_ACQUITDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                aCQ_ACQUITDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aCQ_ACQUITDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (GlobalParam.Instance.ApplicationUnite == SettingExtension.ApplicationUniteEnum.Kilo && e.ColumnIndex == this.droitKilogrammesTextBoxColumn.Index)
            {
                var dgv = this.droitKilogrammesTextBoxColumn.DataGridView;
                int couleurId = -1;
                decimal rateConv = -1;
                var AcquitId = ConverterHelper.To(dgv.Rows[e.RowIndex].Cells["ACQ_ID"].Value, 0);
                if (AcquitId > 0)
                {
                    couleurId = (int)aCQ_ACQUITTableAdapter.GetCouleurIdByACQ_ID(AcquitId);
                }

                switch (couleurId)
                {
                    case 1: //BLANC
                        rateConv = Gestion_Des_Vendanges.BUSINESS.GlobalParam.Instance.TauxConversionBlanc;
                        break;

                    case 2: //ROUGE
                        rateConv = Gestion_Des_Vendanges.BUSINESS.GlobalParam.Instance.TauxConversionRouge;
                        break;

                    default:
                        rateConv = -1;
                        break;
                }

                e.FormattingApplied = true;
                var row = dgv.Rows[e.RowIndex];
                var litres = ConverterHelper.To(row.Cells[this.droitLitresTextBoxColumn.Index].Value, 0.0m);
                decimal kilo = litres / rateConv;
                e.Value = string.Format("{0:0}", kilo);
            }
            //Solde kilos admis pour un acquit
            if (e.ColumnIndex == this.SoldeAcquit.Index)
            {
                string solde = "";
                var dgv = this.SoldeAcquit.DataGridView;

                var AcquitId = ConverterHelper.To(dgv.Rows[e.RowIndex].Cells["ACQ_ID"].Value, 0);
                if (AcquitId > 0)
                {
                    ///<Note>Possible erreur, l'unité du solde est recuperé selon la valeur de UniteType: GlobalParam.Instance.ApplicationUnite</Note>
                    solde = string.Format("{0:0}", GlobalParam.LimitationProduction.GetSoldeByAcquit(AcquitId: AcquitId, ExcludedPeseeDetailId: -1, UniteType: GlobalParam.Instance.ApplicationUnite));
                }

                if (string.IsNullOrWhiteSpace(solde))
                {
                    ///<Note>Possible erreur, l'unité du solde est recuperé selon la valeur de UniteType: GlobalParam.Instance.ApplicationUnite</Note>
                    switch (GlobalParam.Instance.ApplicationUnite)
                    {
                        case SettingExtension.ApplicationUniteEnum.Kilo:
                            solde = "" + dgv.Rows[e.RowIndex].Cells[this.droitKilogrammesTextBoxColumn.Index].Value;
                            break;

                        case SettingExtension.ApplicationUniteEnum.Litre:
                            solde = "" + dgv.Rows[e.RowIndex].Cells[this.droitLitresTextBoxColumn.Index].Value;
                            break;
                    }
                }

                e.Value = solde;
            }
        }
    }
}