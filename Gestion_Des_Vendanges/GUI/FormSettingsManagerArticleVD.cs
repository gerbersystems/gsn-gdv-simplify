﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;

namespace Gestion_Des_Vendanges.GUI
{
    partial class FormSettingsManagerArticle
    {
        private void setNullArtId(int artId)
        {
            ConnectionManager
                .Instance
                .CreateGvTableAdapter<PED_PESEE_DETAILTableAdapter>()
                .SetNullArtId(artId);
        }
    }
}