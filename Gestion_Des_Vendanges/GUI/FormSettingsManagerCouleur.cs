﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class FormSettingsManagerCouleur : FormSettingsManagerPesees
    {
        private CheckBox _chkCouleurCepage;
        private ComboBox _cbModeComptabilisation;
        private bool _optionWinBiz;
        private TextBox _idTextBox;
        private DAO.DSPeseesTableAdapters.MCO_MOC_COUTableAdapter _mcoTable;

        public FormSettingsManagerCouleur(WfrmSetting form, TextBox idTextBox, Control focusControl,
            Button btnNew, Button btnUpdate, Button btnDelete, DataGridView dataGridView,
            BindingSource bindingSource, TableAdapterManager tableAdapterManager, DSPesees dsPesees,
            CheckBox chkCouleurCepage, ComboBox cbModeComptabilisation, bool optionWinBiz) :
            base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete,
           dataGridView, bindingSource, tableAdapterManager, dsPesees)
        {
            _chkCouleurCepage = chkCouleurCepage;
            _cbModeComptabilisation = cbModeComptabilisation;
            _cbModeComptabilisation.Enabled = false;
            _optionWinBiz = optionWinBiz;
            _chkCouleurCepage.CheckedChanged += chkCouleurCepageCheckedChanged;
            _idTextBox = idTextBox;
            DataGridView.SelectionChanged += dataGridSelectionChanged;
            _mcoTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.MCO_MOC_COUTableAdapter>();
        }

        private void dataGridSelectionChanged(object sender, EventArgs e)
        {
            int couId = Convert.ToInt32(_idTextBox.Text);
            try
            {
                _cbModeComptabilisation.SelectedValue = _mcoTable.GetMocId(couId, BUSINESS.Utilities.IdAnneeCourante);
            }
            catch (ArgumentNullException)
            {
            }
        }

        private void chkCouleurCepageCheckedChanged(object sender, EventArgs e)
        {
            _cbModeComptabilisation.Enabled = (_chkCouleurCepage.Checked && _optionWinBiz && _chkCouleurCepage.Enabled);
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            bool toModif = _cbModeComptabilisation.Enabled;
            base.BtnOk_Click(sender, e);
            int couId = Convert.ToInt32(_idTextBox.Text);
            int? mcoId = _mcoTable.GetMcoId(couId, BUSINESS.Utilities.IdAnneeCourante);
            int mocId = (int)_cbModeComptabilisation.SelectedValue;
            if (toModif)
            {
                if (mcoId == null)
                {
                    _mcoTable.Insert((int)mocId, couId);
                }
                else
                {
                    _mcoTable.UpdateMoc((int)mocId, (int)mcoId);
                }
            }
        }

        protected override void ChangeEditMode(bool editMode)
        {
            base.ChangeEditMode(editMode);
            if (_chkCouleurCepage != null)
            {
                chkCouleurCepageCheckedChanged(null, null);
            }
        }
    }
}