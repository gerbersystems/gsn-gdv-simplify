﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.REPORTING;
using GSN.GDV.Reports;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlReportViewer
    {
        protected List<Control> ctrlDateFilter;
        protected List<Control> ctrlProducteurFilter;

        private DateTime _dateRangeFrom
        {
            get { return datePickerFrom.Value.Date; }
            set
            {
                if (value != null)
                    datePickerFrom.Value = new DateTime(value.Year, value.Month, value.Day, 0, 0, 0);
            }
        }

        private DateTime _dateRangeTo
        {
            get { return new DateTime(datePickerTo.Value.Year, datePickerTo.Value.Month, datePickerTo.Value.Day, 23, 59, 59); }
            set
            {
                if (value != null)
                    datePickerTo.Value = new DateTime(value.Year, value.Month, value.Day, 23, 59, 59);
            }
        }

        #region zoom

        private class ZoomItem
        {
            private int _percent;
            private ZoomMode _mode;

            public ZoomMode Mode
            {
                get { return _mode; }
            }

            public int Percent
            {
                get { return _percent; }
            }

            public string Text
            {
                get
                {
                    if (_mode == ZoomMode.FullPage)
                        return "Page entière";
                    else if (_mode == ZoomMode.PageWidth)
                        return "Largeur page";
                    else
                        return _percent + " %";
                }
            }

            public ZoomItem(ZoomMode mode)
            {
                _mode = mode;
            }

            public ZoomItem(int percent)
            {
                _percent = percent;
                _mode = ZoomMode.Percent;
            }

            public override string ToString()
            {
                return Text;
            }
        }

        #endregion zoom

        private GSN.GDV.Reports.IReportInfo _reportInfo;
        private int _nbTotalPage;
        private bool _firstReport;

        public override Panel OptionPanel
        {
            get { return panel1; }
        }

        public override ToolStrip ToolStrip
        {
            get { return toolStrip1; }
        }

        private int _anneeCourante
        {
            get { return BUSINESS.Utilities.AnneeCourante; }
        }

        private static int _idAnneeCourante
        {
            get { return BUSINESS.Utilities.IdAnneeCourante; }
            set { BUSINESS.Utilities.IdAnneeCourante = value; }
        }

        public CtrlReportViewer()
        {
            InitializeComponent();
            _firstReport = true;
            initToolZoomCb();

            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);

            UpdateDonnees();

            cbAnnee.SelectedValue = _idAnneeCourante;
            Text = "Gestion des vendanges - Rapport";
            reportViewer1.LocalReport.SubreportProcessing += subreportProcessingEventHandler;

            ctrlDateFilter = new List<Control> { labDate, labFrom, labTo, datePickerFrom, datePickerTo, btnFilterDate };
            ctrlProducteurFilter = new List<Control> { chkAllProd, cbProducteur, labProducteur };

            datePickerFrom.MinDate = datePickerTo.MinDate = new DateTime(_anneeCourante, 1, 1);
            datePickerFrom.Value = new DateTime(_anneeCourante, 9, 1);
            datePickerTo.Value = new DateTime(_anneeCourante + 1, 12, 31);
        }

        private void subreportProcessingEventHandler(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Clear();
            if (_reportInfo != null)
            {
                foreach (var dataSource in _reportInfo.SubReportDataSources)
                    e.DataSources.Add(dataSource);
            }
        }

        public override void UpdateDonnees()
        {
            pRO_NOMCOMPLETTableAdapter.Fill(dSPesees.PRO_NOMCOMPLET);
            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
            toolRefresh.PerformClick();
        }

        #region ShowReport

        public static void ShowListeVendangesLivreesParProducteur(int annID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeVendangesLivreesParProducteurReport(format: format, annID: annID));
        }

        internal static void ShowListeVendangesLivreesParLieuDeProduction(int annID, FormatReport format)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeVendangesLivreesParLieuDeProductionReport(format: format, annID: annID));
        }

        public static void ShowAttestationControleAvecSolde(int peeID, int proID, int annID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_AttestationControleAvecSoldeReport(format: format, peeID: peeID, proID: proID, annID: annID));
        }

        public static void ShowAttestationControleV2(int peeID, int proID, int annID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_AttestationControleV2Report(format: format, peeID: peeID, proID: proID, annID: annID));
        }

        public static void ShowAttestationControleAvecSoldeParAcquit(int peeID, int proID, int annID, int acqID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_AttestationControleAvecSoldeParAcquitReport(format: format, peeID: peeID, proID: proID, annID: annID, acqID: acqID));
        }

        public static void ShowTest(GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_testReport(format: format));
        }

        public static void ShowListeAcquitsVendange(int aNNID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu, float tauxConversionKgLitres = 0.8f)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeAcquitsVendangesReport(format: format, annId: aNNID, taux: tauxConversionKgLitres));
        }

        public static void ShowListeAcquitsVendangeCepage(int aNNID, GSN.GDV.Reports.FormatReport format = GSN.GDV.Reports.FormatReport.Apercu, float tauxConversionKgLitres = 0.8f)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeAcquitsVendangeCepageReport(format, annID: aNNID, tauxConversionKgLitres: tauxConversionKgLitres));
        }

        public static void ShowListeAcquits(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new ListeAcquitsReport(format, aNNID));
        }

        public static void ShowDroitsProduction(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            //HACK REMOVE
            showReport(new DroitsProductionReport(format, aNNID));
        }

        public static void ShowDroitsDeProduction(int idAnneeCourante, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_DroitsDeProductionReport(format, idAnneeCourante));
        }

        public static void ShowQuittancePesee(int pEEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new QuittancePeseeReport(format, pEEID));
        }

        public static void ShowQuittanceV2Pesee(int peeID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_QuittanceV2Report(format: format, peeID: peeID));
        }

        public static void ShowAttestationControle(int pEEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new AttestationDeControleReport(format, peeId: pEEID));
        }

        public static void ShowAttestationsControle(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new AttestationDeControleReport(format, aNNID));
        }

        public static void ShowVendangesParCepage(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeVendangesParCepageReport(format, annID: aNNID));
        }

        public static void ShowListeParcellesVisiteVignes(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeParcellesVisiteVignesReport(format, aNNID));
        }

        public static void ShowListeVendangesLivreesParDateEnKilos(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeVendangesLivreesParDateReport(format, aNNID));
        }

        public static void ShowListeVendangesLivreesParDateEnLitres(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ListeVendangesLivreesParDateEnLitresReport(format, aNNID));
        }

        private static void showReport(ReportInfo report)
        {
            CtrlReportViewer reportViewer = new CtrlReportViewer();
            reportViewer._reportInfo = report;

            if (report.Format == FormatReport.Apercu)
            {
                WfrmHostCtrWfrm.Show(control: reportViewer, showMenu: false, windowState: FormWindowState.Maximized);
                reportViewer.reportViewer1.ShowToolBar = true;
            }
            reportViewer.showAttestation(isRDLC: false);
        }

        private static void showReport(GSN.GDV.Reports.IReportInfo reportInfo)
        {
            CtrlReportViewer reportViewer = new CtrlReportViewer();
            reportViewer._reportInfo = reportInfo;

            if (reportInfo.Format == GSN.GDV.Reports.FormatReport.Apercu)
            {
                WfrmHostCtrWfrm.Show(control: reportViewer, showMenu: false, windowState: FormWindowState.Maximized);
                reportViewer.reportViewer1.ShowToolBar = true;
            }
            reportViewer.showAttestation(isRDLC: true);
        }

        public static void ShowTablesDesPrix(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new TableDesPrixReport(format, aNNID));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour voir ce rapport.\n" +
                                "Commander la version étendue pour gérer les tables de prix.", "License",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowTableDesPrix(int aNNID, int lIEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new TableDesPrixReport(format, aNNID, lIEID));
        }

        public static void ShowPaiement(int paiId, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new PaiementReport(format, paiId: paiId));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowPaiementBonus(int paiId, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_PaiementReport(format: format, annId: _idAnneeCourante, paiId: paiId));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowPaiementBonusV2(int paiId, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_PaiementReportV2(format: format, annId: _idAnneeCourante, paiId: paiId));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowPaiementFacture(int paiId, FormatReport format = FormatReport.Apercu)
        {
            if (CheckLicenceLevel(1))
                showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_PaiementFactureReport(format: format, annId: _idAnneeCourante, paiId: paiId));
        }

        private static bool CheckLicenceLevel(int licenceLevelToCheck)
        {
            if (BUSINESS.Utilities.GetOption(licenceLevelToCheck))
                return true;
            else
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            return false;
        }

        public static void ShowPaiementDetailBonus(int paiId, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_PaiementDetailBonusReport(format: format, annId: _idAnneeCourante, paiId: paiId));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowPaiementRapport(FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_PaiementRapportReport(format: format, annId: _idAnneeCourante));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static void ShowPaiements(int annId, FormatReport format = FormatReport.Apercu)
        {
            if (BUSINESS.Utilities.GetOption(1))
            {
                showReport(new PaiementReport(format, annId));
            }
            else
            {
                MessageBox.Show("Vous n'avez pas licence pour afficher ce rapport.\n" +
                                "Commander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrect",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion ShowReport

        private void showAttestation(bool isRDLC)
        {
            string extension = isRDLC ? ".rdlc" : ".rdl";
            string pathFolderReporting = File.Exists(BUSINESS.Utilities.ApplicationPath + @"\REPORTING\" + _reportInfo.Name + extension) ? @"\REPORTING\" : @"\";
            string fullPathReporting = BUSINESS.Utilities.ApplicationPath + pathFolderReporting + _reportInfo.Name + extension;

            reportViewer1.LocalReport.ReportPath = fullPathReporting;
            reportViewer1.LocalReport.DataSources.Clear();

            foreach (ReportDataSource dataSource in _reportInfo.DataSources)
                reportViewer1.LocalReport.DataSources.Add(dataSource);

            if (_firstReport)
            {
                reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                _firstReport = false;
            }
            else
            {
                reportViewer1.ResetPageSettings();
                reportViewer1.RefreshReport();
            }

            toolStrip1.Enabled = true;

            if (_reportInfo.Format != GSN.GDV.Reports.FormatReport.Apercu)
            {
                try
                {
                    showAttestationFile(_reportInfo.Format, _reportInfo.Name);
                }
                catch
                {
                    MessageBox.Show("Le rapport n'est pas accessible.\nVeuillez vérifier qu'aucun rapport n'est déjà ouvert.", "Ouverture impossible", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void showAttestationFile(GSN.GDV.Reports.FormatReport format, string name)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass())
            {
                string formatString;
                string path = Environment.GetEnvironmentVariable("temp") + @"\" + name + ".";
                switch (format)
                {
                    case GSN.GDV.Reports.FormatReport.Excel:
                        formatString = "Excel";
                        path += "xls";
                        break;

                    case GSN.GDV.Reports.FormatReport.Word:
                        formatString = "Word";
                        path += "doc";
                        break;

                    case GSN.GDV.Reports.FormatReport.PDF:
                        formatString = "PDF";
                        path += "pdf";
                        break;

                    default:
                        throw new InvalidEnumArgumentException();
                }

                if (File.Exists(path))
                    File.SetAttributes(path, FileAttributes.Normal);

                NumberFormatInfo numberFormat = Thread.CurrentThread.CurrentCulture.NumberFormat; //garde en mémoire le format de nombre
                byte[] bytes = reportViewer1.LocalReport.Render(formatString); //création du rapport et modification du format de nombre
                Thread.CurrentThread.CurrentCulture.NumberFormat = numberFormat; //rétablissement du format de nombre
                FileStream fs = new FileStream(@path, FileMode.Create);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                File.SetAttributes(path, FileAttributes.ReadOnly);
                Process.Start(path);
            }
        }

        #region toolStripBar

        private void initToolZoomCb()
        {
            ZoomItem item = new ZoomItem(ZoomMode.FullPage);
            toolZoomCb.Items.Add(item);
            toolZoomCb.Items.Add(new ZoomItem(ZoomMode.PageWidth));
            toolZoomCb.Items.Add(new ZoomItem(500));
            toolZoomCb.Items.Add(new ZoomItem(200));
            toolZoomCb.Items.Add(new ZoomItem(150));
            toolZoomCb.Items.Add(new ZoomItem(100));
            toolZoomCb.Items.Add(new ZoomItem(75));
            toolZoomCb.Items.Add(new ZoomItem(50));
            toolZoomCb.Items.Add(new ZoomItem(25));
            toolZoomCb.SelectedItem = item;
        }

        private void toolZoomCb_Click(object sender, EventArgs e)
        {
            ZoomItem zoom = (ZoomItem)toolZoomCb.SelectedItem;
            reportViewer1.ZoomMode = zoom.Mode;

            if (zoom.Mode == ZoomMode.Percent)
                reportViewer1.ZoomPercent = zoom.Percent * 2 / 5;
        }

        private void tooPrint_Click(object sender, EventArgs e)
        {
            reportViewer1.PrintDialog();
        }

        private void toolLargeurPage_Click(object sender, EventArgs e)
        {
            foreach (ZoomItem item in toolZoomCb.Items)
            {
                if (item.Mode == ZoomMode.FullPage)
                    toolZoomCb.SelectedItem = item;
            }
            reportViewer1.ZoomMode = ZoomMode.FullPage;
        }

        private void setTotalPage()
        {
            _nbTotalPage = reportViewer1.GetTotalPages();
            toolLabelPage.Text = "sur " + _nbTotalPage;
        }

        private void updateToolSelectionPage()
        {
            // If user close window before this method is called then stop the method
            if (toolPage.IsDisposed)
                return;
            toolPage.Text = reportViewer1.CurrentPage.ToString();

            if (_nbTotalPage == 1 || _nbTotalPage == 0 || toolLabelPage.Text == "sur ")
            {
                toolBefore.Enabled = false;
                toolNext.Enabled = false;
                toolFirst.Enabled = false;
                toolLast.Enabled = false;
                toolPage.ReadOnly = true;
            }
            else
            {
                toolPage.ReadOnly = true;
                bool lastPage = (_nbTotalPage == reportViewer1.CurrentPage);
                bool firstPage = (reportViewer1.CurrentPage == 1);
                toolNext.Enabled = !lastPage;
                toolLast.Enabled = !lastPage;
                toolFirst.Enabled = !firstPage;
                toolBefore.Enabled = !firstPage;
                btnFilterDate.Enabled = true;
            }
        }

        private void toolBefore_Click(object sender, EventArgs e)
        {
            if (reportViewer1.CurrentPage > 1)
                reportViewer1.CurrentPage--;

            updateToolSelectionPage();
        }

        private void toolNext_Click(object sender, EventArgs e)
        {
            if (toolLabelPage.Text != "sur " && toolLabelPage.Text != "sur 0")
                reportViewer1.CurrentPage++;

            updateToolSelectionPage();
        }

        private void toolLast_Click(object sender, EventArgs e)
        {
            if (toolLabelPage.Text != "sur " && toolLabelPage.Text != "sur 0")
                reportViewer1.CurrentPage = _nbTotalPage;

            updateToolSelectionPage();
        }

        private void toolFirst_Click(object sender, EventArgs e)
        {
            if (reportViewer1.CurrentPage > 1)
                reportViewer1.CurrentPage = 1;

            updateToolSelectionPage();
        }

        private void toolRefresh_Click(object sender, EventArgs e)
        {
            toolLabelPage.Text = "sur ";
            reportViewer1.RefreshReport();
            updateToolSelectionPage();
        }

        private void portableDocumentFormatPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_reportInfo != null)
                showAttestationFile(FormatReport.PDF, _reportInfo.Name);
        }

        private void microsoftWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_reportInfo != null)
                showAttestationFile(FormatReport.Word, _reportInfo.Name);
        }

        private void microsoftExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_reportInfo != null)
                showAttestationFile(FormatReport.Excel, _reportInfo.Name);
        }

        private void reportViewer1_RenderingBegin(object sender, System.ComponentModel.CancelEventArgs e)
        {
            toolExporter.Enabled = false;
            toolLast.Enabled = false;
            toolRefresh.Enabled = false;
        }

        private void reportViewer1_RenderingComplete(object sender, RenderingCompleteEventArgs e)
        {
            setTotalPage();
            updateToolSelectionPage();
            toolExporter.Enabled = true;
            if (_nbTotalPage != 1)
                toolLast.Enabled = true;
            toolRefresh.Enabled = true;
        }

        private void toolPage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                int page = Convert.ToInt32(toolPage.Text);
                if (page <= 0 || page > _nbTotalPage)
                    toolPage.Text = reportViewer1.CurrentPage.ToString();
            }
            else
            {
                e.Handled = true;
            }
        }

        private void reportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
        {
            toolPage.Text = reportViewer1.CurrentPage.ToString();
        }

        #endregion toolStripBar

        public static CtrlReportViewer GetInstance()
        {
            return new CtrlReportViewer();
        }

        private void cbProducteur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((optPaiements.Checked || optPaiements.Checked || optBonusDetail.Checked || optAttestationControleAvecSolde.Checked
                || optAttestationControleAvecSoldeParAcquit.Checked) && cbProducteur.SelectedValue != null)
                buildAttestation(sender);
        }

        private void cbAllProd_CheckedChanged(object sender, EventArgs e)
        {
            cbProducteur.Enabled = !chkAllProd.Checked;
            if (!chkAllProd.Checked)
                pRO_NOMCOMPLETTableAdapter.FillProducteurByPaiementAnnId(dSPesees.PRO_NOMCOMPLET, _idAnneeCourante);

            buildAttestation(sender);
        }

        private void cbAnnee_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cbAnnee.SelectedValue != null && Convert.ToInt32(cbAnnee.SelectedValue) != _idAnneeCourante)
            {
                _idAnneeCourante = Convert.ToInt32(cbAnnee.SelectedValue);
                if (optPaiements.Checked)
                    pRO_NOMCOMPLETTableAdapter.FillProducteurByPaiementAnnId(dSPesees.PRO_NOMCOMPLET, _idAnneeCourante);

                buildAttestation(sender);
            }
        }

        private void btnFilterDate_Click(object sender, EventArgs e)
        {
            if (isDateRangeValid())
            {
                toolLabelPage.Text = "sur ";
                btnFilterDate.Enabled = false;
                buildAttestation(sender, dateRangeFrom: _dateRangeFrom, dateRangeTo: _dateRangeTo, isReportAutoLoadedOverride: true);
            }
        }
    }
}