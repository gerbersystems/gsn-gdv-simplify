﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGlobalSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExceedQuotaGroupBox = new System.Windows.Forms.GroupBox();
            this.rdbFalseExceedQuota = new System.Windows.Forms.RadioButton();
            this.TrueExceedQuotaRadioButton = new System.Windows.Forms.RadioButton();
            this.UniteGroupBox = new System.Windows.Forms.GroupBox();
            this.LitresRadioButton = new System.Windows.Forms.RadioButton();
            this.KiloRadioButton = new System.Windows.Forms.RadioButton();
            this.RateConvVinClairGroupBox = new System.Windows.Forms.GroupBox();
            this.RougeClairLabel = new System.Windows.Forms.Label();
            this.RougeVinClairNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.BlancClairLabel = new System.Windows.Forms.Label();
            this.BlancVinClairNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.SondageGroupBox = new System.Windows.Forms.GroupBox();
            this.sondageOechsleRadioButton = new System.Windows.Forms.RadioButton();
            this.sondageBricksRadioButton = new System.Windows.Forms.RadioButton();
            this.BonusUniteGroupBox = new System.Windows.Forms.GroupBox();
            this.typeTableBonusComboBox = new System.Windows.Forms.ComboBox();
            this.MainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FactureSelectionGroupBox = new System.Windows.Forms.GroupBox();
            this.typeFactureComboBox = new System.Windows.Forms.ComboBox();
            this.CantonGroupBox = new System.Windows.Forms.GroupBox();
            this.CantonComboBox = new System.Windows.Forms.ComboBox();
            this.ErpGroupBox = new System.Windows.Forms.GroupBox();
            this.ErpComboBox = new System.Windows.Forms.ComboBox();
            this.SyncContactGroupBox = new System.Windows.Forms.GroupBox();
            this.syncContactRadioButton = new System.Windows.Forms.RadioButton();
            this.unSyncContactRadioButton = new System.Windows.Forms.RadioButton();
            this.RateConvMoutRaisinGroupBox = new System.Windows.Forms.GroupBox();
            this.RougeMoutLabel = new System.Windows.Forms.Label();
            this.RougeMoutRaisinNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.BlancMoutLabel = new System.Windows.Forms.Label();
            this.BlancMoutRaisinNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.DegreValeurGroupBox = new System.Windows.Forms.GroupBox();
            this.BrixRadioButton = new System.Windows.Forms.RadioButton();
            this.OechsleRadioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ExceedQuotaGroupBox.SuspendLayout();
            this.UniteGroupBox.SuspendLayout();
            this.RateConvVinClairGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RougeVinClairNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlancVinClairNumericUpDown)).BeginInit();
            this.SondageGroupBox.SuspendLayout();
            this.BonusUniteGroupBox.SuspendLayout();
            this.MainTableLayoutPanel.SuspendLayout();
            this.FactureSelectionGroupBox.SuspendLayout();
            this.CantonGroupBox.SuspendLayout();
            this.ErpGroupBox.SuspendLayout();
            this.SyncContactGroupBox.SuspendLayout();
            this.RateConvMoutRaisinGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RougeMoutRaisinNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlancMoutRaisinNumericUpDown)).BeginInit();
            this.DegreValeurGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExceedQuotaGroupBox
            // 
            this.ExceedQuotaGroupBox.AutoSize = true;
            this.ExceedQuotaGroupBox.Controls.Add(this.rdbFalseExceedQuota);
            this.ExceedQuotaGroupBox.Controls.Add(this.TrueExceedQuotaRadioButton);
            this.ExceedQuotaGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExceedQuotaGroupBox.Location = new System.Drawing.Point(28, 440);
            this.ExceedQuotaGroupBox.Name = "ExceedQuotaGroupBox";
            this.ExceedQuotaGroupBox.Size = new System.Drawing.Size(328, 60);
            this.ExceedQuotaGroupBox.TabIndex = 7;
            this.ExceedQuotaGroupBox.TabStop = false;
            this.ExceedQuotaGroupBox.Text = "[ PESEE ] Dépassement quota";
            // 
            // rdbFalseExceedQuota
            // 
            this.rdbFalseExceedQuota.AutoSize = true;
            this.rdbFalseExceedQuota.Location = new System.Drawing.Point(226, 24);
            this.rdbFalseExceedQuota.Name = "rdbFalseExceedQuota";
            this.rdbFalseExceedQuota.Size = new System.Drawing.Size(45, 17);
            this.rdbFalseExceedQuota.TabIndex = 0;
            this.rdbFalseExceedQuota.TabStop = true;
            this.rdbFalseExceedQuota.Text = "Non";
            this.rdbFalseExceedQuota.UseVisualStyleBackColor = true;
            // 
            // TrueExceedQuotaRadioButton
            // 
            this.TrueExceedQuotaRadioButton.AutoSize = true;
            this.TrueExceedQuotaRadioButton.Location = new System.Drawing.Point(83, 24);
            this.TrueExceedQuotaRadioButton.Name = "TrueExceedQuotaRadioButton";
            this.TrueExceedQuotaRadioButton.Size = new System.Drawing.Size(41, 17);
            this.TrueExceedQuotaRadioButton.TabIndex = 5;
            this.TrueExceedQuotaRadioButton.TabStop = true;
            this.TrueExceedQuotaRadioButton.Text = "Oui";
            this.TrueExceedQuotaRadioButton.UseVisualStyleBackColor = true;
            // 
            // UniteGroupBox
            // 
            this.UniteGroupBox.AutoSize = true;
            this.UniteGroupBox.Controls.Add(this.LitresRadioButton);
            this.UniteGroupBox.Controls.Add(this.KiloRadioButton);
            this.UniteGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UniteGroupBox.Location = new System.Drawing.Point(28, 644);
            this.UniteGroupBox.Name = "UniteGroupBox";
            this.UniteGroupBox.Size = new System.Drawing.Size(328, 60);
            this.UniteGroupBox.TabIndex = 11;
            this.UniteGroupBox.TabStop = false;
            this.UniteGroupBox.Text = "Unité";
            // 
            // LitresRadioButton
            // 
            this.LitresRadioButton.AutoSize = true;
            this.LitresRadioButton.Location = new System.Drawing.Point(226, 24);
            this.LitresRadioButton.Name = "LitresRadioButton";
            this.LitresRadioButton.Size = new System.Drawing.Size(50, 17);
            this.LitresRadioButton.TabIndex = 0;
            this.LitresRadioButton.TabStop = true;
            this.LitresRadioButton.Text = "Litres";
            this.LitresRadioButton.UseVisualStyleBackColor = true;
            // 
            // KiloRadioButton
            // 
            this.KiloRadioButton.AutoSize = true;
            this.KiloRadioButton.Location = new System.Drawing.Point(83, 24);
            this.KiloRadioButton.Name = "KiloRadioButton";
            this.KiloRadioButton.Size = new System.Drawing.Size(84, 17);
            this.KiloRadioButton.TabIndex = 0;
            this.KiloRadioButton.TabStop = true;
            this.KiloRadioButton.Text = "Kilogrammes";
            this.KiloRadioButton.UseVisualStyleBackColor = true;
            // 
            // RateConvVinClairGroupBox
            // 
            this.RateConvVinClairGroupBox.AutoSize = true;
            this.RateConvVinClairGroupBox.Controls.Add(this.RougeClairLabel);
            this.RateConvVinClairGroupBox.Controls.Add(this.RougeVinClairNumericUpDown);
            this.RateConvVinClairGroupBox.Controls.Add(this.BlancClairLabel);
            this.RateConvVinClairGroupBox.Controls.Add(this.BlancVinClairNumericUpDown);
            this.RateConvVinClairGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RateConvVinClairGroupBox.Location = new System.Drawing.Point(28, 232);
            this.RateConvVinClairGroupBox.Name = "RateConvVinClairGroupBox";
            this.RateConvVinClairGroupBox.Size = new System.Drawing.Size(328, 65);
            this.RateConvVinClairGroupBox.TabIndex = 4;
            this.RateConvVinClairGroupBox.TabStop = false;
            this.RateConvVinClairGroupBox.Text = "Taux conversion Lt/Kg en vin clair";
            // 
            // RougeClairLabel
            // 
            this.RougeClairLabel.AutoSize = true;
            this.RougeClairLabel.Location = new System.Drawing.Point(20, 28);
            this.RougeClairLabel.Name = "RougeClairLabel";
            this.RougeClairLabel.Size = new System.Drawing.Size(57, 13);
            this.RougeClairLabel.TabIndex = 6;
            this.RougeClairLabel.Text = "Vin Rouge";
            // 
            // RougeVinClairNumericUpDown
            // 
            this.RougeVinClairNumericUpDown.DecimalPlaces = 2;
            this.RougeVinClairNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.RougeVinClairNumericUpDown.Location = new System.Drawing.Point(83, 26);
            this.RougeVinClairNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.RougeVinClairNumericUpDown.Name = "RougeVinClairNumericUpDown";
            this.RougeVinClairNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.RougeVinClairNumericUpDown.TabIndex = 0;
            this.RougeVinClairNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            // 
            // BlancClairLabel
            // 
            this.BlancClairLabel.AutoSize = true;
            this.BlancClairLabel.Location = new System.Drawing.Point(169, 28);
            this.BlancClairLabel.Name = "BlancClairLabel";
            this.BlancClairLabel.Size = new System.Drawing.Size(51, 13);
            this.BlancClairLabel.TabIndex = 7;
            this.BlancClairLabel.Text = "Vin blanc";
            // 
            // BlancVinClairNumericUpDown
            // 
            this.BlancVinClairNumericUpDown.DecimalPlaces = 2;
            this.BlancVinClairNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.BlancVinClairNumericUpDown.Location = new System.Drawing.Point(226, 26);
            this.BlancVinClairNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.BlancVinClairNumericUpDown.Name = "BlancVinClairNumericUpDown";
            this.BlancVinClairNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.BlancVinClairNumericUpDown.TabIndex = 0;
            this.BlancVinClairNumericUpDown.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            // 
            // SondageGroupBox
            // 
            this.SondageGroupBox.AutoSize = true;
            this.SondageGroupBox.Controls.Add(this.sondageOechsleRadioButton);
            this.SondageGroupBox.Controls.Add(this.sondageBricksRadioButton);
            this.SondageGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SondageGroupBox.Location = new System.Drawing.Point(28, 374);
            this.SondageGroupBox.Name = "SondageGroupBox";
            this.SondageGroupBox.Size = new System.Drawing.Size(328, 60);
            this.SondageGroupBox.TabIndex = 6;
            this.SondageGroupBox.TabStop = false;
            this.SondageGroupBox.Text = "[ PESEE ] Sondage";
            // 
            // sondageOechsleRadioButton
            // 
            this.sondageOechsleRadioButton.AutoSize = true;
            this.sondageOechsleRadioButton.Location = new System.Drawing.Point(83, 24);
            this.sondageOechsleRadioButton.Name = "sondageOechsleRadioButton";
            this.sondageOechsleRadioButton.Size = new System.Drawing.Size(40, 17);
            this.sondageOechsleRadioButton.TabIndex = 0;
            this.sondageOechsleRadioButton.TabStop = true;
            this.sondageOechsleRadioButton.Text = "OE";
            this.sondageOechsleRadioButton.UseVisualStyleBackColor = true;
            // 
            // sondageBricksRadioButton
            // 
            this.sondageBricksRadioButton.AutoSize = true;
            this.sondageBricksRadioButton.Location = new System.Drawing.Point(226, 24);
            this.sondageBricksRadioButton.Name = "sondageBricksRadioButton";
            this.sondageBricksRadioButton.Size = new System.Drawing.Size(42, 17);
            this.sondageBricksRadioButton.TabIndex = 0;
            this.sondageBricksRadioButton.TabStop = true;
            this.sondageBricksRadioButton.Text = "Brix";
            this.sondageBricksRadioButton.UseVisualStyleBackColor = true;
            // 
            // BonusUniteGroupBox
            // 
            this.BonusUniteGroupBox.AutoSize = true;
            this.BonusUniteGroupBox.Controls.Add(this.typeTableBonusComboBox);
            this.BonusUniteGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BonusUniteGroupBox.Location = new System.Drawing.Point(28, 506);
            this.BonusUniteGroupBox.Name = "BonusUniteGroupBox";
            this.BonusUniteGroupBox.Size = new System.Drawing.Size(328, 63);
            this.BonusUniteGroupBox.TabIndex = 8;
            this.BonusUniteGroupBox.TabStop = false;
            this.BonusUniteGroupBox.Text = "[ BONUS ] Unité Bonus | Malus";
            // 
            // typeTableBonusComboBox
            // 
            this.typeTableBonusComboBox.FormattingEnabled = true;
            this.typeTableBonusComboBox.Location = new System.Drawing.Point(65, 23);
            this.typeTableBonusComboBox.Name = "typeTableBonusComboBox";
            this.typeTableBonusComboBox.Size = new System.Drawing.Size(237, 21);
            this.typeTableBonusComboBox.TabIndex = 1;
            // 
            // MainTableLayoutPanel
            // 
            this.MainTableLayoutPanel.ColumnCount = 1;
            this.MainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.MainTableLayoutPanel.Controls.Add(this.FactureSelectionGroupBox, 0, 11);
            this.MainTableLayoutPanel.Controls.Add(this.CantonGroupBox, 0, 0);
            this.MainTableLayoutPanel.Controls.Add(this.ErpGroupBox, 0, 1);
            this.MainTableLayoutPanel.Controls.Add(this.SyncContactGroupBox, 0, 2);
            this.MainTableLayoutPanel.Controls.Add(this.RateConvVinClairGroupBox, 0, 3);
            this.MainTableLayoutPanel.Controls.Add(this.RateConvMoutRaisinGroupBox, 0, 4);
            this.MainTableLayoutPanel.Controls.Add(this.SondageGroupBox, 0, 5);
            this.MainTableLayoutPanel.Controls.Add(this.ExceedQuotaGroupBox, 0, 6);
            this.MainTableLayoutPanel.Controls.Add(this.BonusUniteGroupBox, 0, 7);
            this.MainTableLayoutPanel.Controls.Add(this.DegreValeurGroupBox, 0, 9);
            this.MainTableLayoutPanel.Controls.Add(this.UniteGroupBox, 0, 10);
            this.MainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.MainTableLayoutPanel.Name = "MainTableLayoutPanel";
            this.MainTableLayoutPanel.Padding = new System.Windows.Forms.Padding(25);
            this.MainTableLayoutPanel.RowCount = 12;
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MainTableLayoutPanel.Size = new System.Drawing.Size(384, 822);
            this.MainTableLayoutPanel.TabIndex = 6;
            // 
            // FactureSelectionGroupBox
            // 
            this.FactureSelectionGroupBox.AutoSize = true;
            this.FactureSelectionGroupBox.Controls.Add(this.typeFactureComboBox);
            this.FactureSelectionGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FactureSelectionGroupBox.Location = new System.Drawing.Point(28, 710);
            this.FactureSelectionGroupBox.Name = "FactureSelectionGroupBox";
            this.FactureSelectionGroupBox.Size = new System.Drawing.Size(328, 84);
            this.FactureSelectionGroupBox.TabIndex = 12;
            this.FactureSelectionGroupBox.TabStop = false;
            this.FactureSelectionGroupBox.Text = "[ RAPPORT ] Sélection du type de facture";
            // 
            // typeFactureComboBox
            // 
            this.typeFactureComboBox.FormattingEnabled = true;
            this.typeFactureComboBox.Location = new System.Drawing.Point(65, 23);
            this.typeFactureComboBox.Name = "typeFactureComboBox";
            this.typeFactureComboBox.Size = new System.Drawing.Size(237, 21);
            this.typeFactureComboBox.TabIndex = 1;
            // 
            // CantonGroupBox
            // 
            this.CantonGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CantonGroupBox.AutoSize = true;
            this.CantonGroupBox.Controls.Add(this.CantonComboBox);
            this.CantonGroupBox.Location = new System.Drawing.Point(28, 28);
            this.CantonGroupBox.Name = "CantonGroupBox";
            this.CantonGroupBox.Size = new System.Drawing.Size(328, 63);
            this.CantonGroupBox.TabIndex = 1;
            this.CantonGroupBox.TabStop = false;
            this.CantonGroupBox.Text = "[ GDV ] Canton";
            // 
            // CantonComboBox
            // 
            this.CantonComboBox.FormattingEnabled = true;
            this.CantonComboBox.Location = new System.Drawing.Point(65, 23);
            this.CantonComboBox.Name = "CantonComboBox";
            this.CantonComboBox.Size = new System.Drawing.Size(237, 21);
            this.CantonComboBox.TabIndex = 0;
            // 
            // ErpGroupBox
            // 
            this.ErpGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ErpGroupBox.AutoSize = true;
            this.ErpGroupBox.Controls.Add(this.ErpComboBox);
            this.ErpGroupBox.Location = new System.Drawing.Point(28, 97);
            this.ErpGroupBox.Name = "ErpGroupBox";
            this.ErpGroupBox.Size = new System.Drawing.Size(328, 63);
            this.ErpGroupBox.TabIndex = 2;
            this.ErpGroupBox.TabStop = false;
            this.ErpGroupBox.Text = "[ PAIEMENT ] Erp";
            // 
            // ErpComboBox
            // 
            this.ErpComboBox.FormattingEnabled = true;
            this.ErpComboBox.Location = new System.Drawing.Point(65, 23);
            this.ErpComboBox.Name = "ErpComboBox";
            this.ErpComboBox.Size = new System.Drawing.Size(237, 21);
            this.ErpComboBox.TabIndex = 0;
            this.ErpComboBox.Text = "Erp";
            // 
            // SyncContactGroupBox
            // 
            this.SyncContactGroupBox.AutoSize = true;
            this.SyncContactGroupBox.Controls.Add(this.syncContactRadioButton);
            this.SyncContactGroupBox.Controls.Add(this.unSyncContactRadioButton);
            this.SyncContactGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SyncContactGroupBox.Location = new System.Drawing.Point(28, 166);
            this.SyncContactGroupBox.Name = "SyncContactGroupBox";
            this.SyncContactGroupBox.Size = new System.Drawing.Size(328, 60);
            this.SyncContactGroupBox.TabIndex = 3;
            this.SyncContactGroupBox.TabStop = false;
            this.SyncContactGroupBox.Text = "[ PAIEMENT ] Synchroniser contacts avec l\'ERP";
            // 
            // syncContactRadioButton
            // 
            this.syncContactRadioButton.AutoSize = true;
            this.syncContactRadioButton.Location = new System.Drawing.Point(83, 24);
            this.syncContactRadioButton.Name = "syncContactRadioButton";
            this.syncContactRadioButton.Size = new System.Drawing.Size(41, 17);
            this.syncContactRadioButton.TabIndex = 0;
            this.syncContactRadioButton.TabStop = true;
            this.syncContactRadioButton.Text = "Oui";
            this.syncContactRadioButton.UseVisualStyleBackColor = true;
            // 
            // unSyncContactRadioButton
            // 
            this.unSyncContactRadioButton.AutoSize = true;
            this.unSyncContactRadioButton.Location = new System.Drawing.Point(227, 24);
            this.unSyncContactRadioButton.Name = "unSyncContactRadioButton";
            this.unSyncContactRadioButton.Size = new System.Drawing.Size(45, 17);
            this.unSyncContactRadioButton.TabIndex = 0;
            this.unSyncContactRadioButton.TabStop = true;
            this.unSyncContactRadioButton.Text = "Non";
            this.unSyncContactRadioButton.UseVisualStyleBackColor = true;
            // 
            // RateConvMoutRaisinGroupBox
            // 
            this.RateConvMoutRaisinGroupBox.AutoSize = true;
            this.RateConvMoutRaisinGroupBox.Controls.Add(this.RougeMoutLabel);
            this.RateConvMoutRaisinGroupBox.Controls.Add(this.RougeMoutRaisinNumericUpDown);
            this.RateConvMoutRaisinGroupBox.Controls.Add(this.BlancMoutLabel);
            this.RateConvMoutRaisinGroupBox.Controls.Add(this.BlancMoutRaisinNumericUpDown);
            this.RateConvMoutRaisinGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RateConvMoutRaisinGroupBox.Location = new System.Drawing.Point(28, 303);
            this.RateConvMoutRaisinGroupBox.Name = "RateConvMoutRaisinGroupBox";
            this.RateConvMoutRaisinGroupBox.Size = new System.Drawing.Size(328, 65);
            this.RateConvMoutRaisinGroupBox.TabIndex = 5;
            this.RateConvMoutRaisinGroupBox.TabStop = false;
            this.RateConvMoutRaisinGroupBox.Text = "Taux conversion Lt/Kg en moût de raisin";
            // 
            // RougeMoutLabel
            // 
            this.RougeMoutLabel.AutoSize = true;
            this.RougeMoutLabel.Location = new System.Drawing.Point(20, 28);
            this.RougeMoutLabel.Name = "RougeMoutLabel";
            this.RougeMoutLabel.Size = new System.Drawing.Size(57, 13);
            this.RougeMoutLabel.TabIndex = 6;
            this.RougeMoutLabel.Text = "Vin Rouge";
            // 
            // RougeMoutRaisinNumericUpDown
            // 
            this.RougeMoutRaisinNumericUpDown.DecimalPlaces = 2;
            this.RougeMoutRaisinNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.RougeMoutRaisinNumericUpDown.Location = new System.Drawing.Point(83, 26);
            this.RougeMoutRaisinNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.RougeMoutRaisinNumericUpDown.Name = "RougeMoutRaisinNumericUpDown";
            this.RougeMoutRaisinNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.RougeMoutRaisinNumericUpDown.TabIndex = 0;
            this.RougeMoutRaisinNumericUpDown.Value = new decimal(new int[] {
            76,
            0,
            0,
            131072});
            // 
            // BlancMoutLabel
            // 
            this.BlancMoutLabel.AutoSize = true;
            this.BlancMoutLabel.Location = new System.Drawing.Point(169, 28);
            this.BlancMoutLabel.Name = "BlancMoutLabel";
            this.BlancMoutLabel.Size = new System.Drawing.Size(51, 13);
            this.BlancMoutLabel.TabIndex = 7;
            this.BlancMoutLabel.Text = "Vin blanc";
            // 
            // BlancMoutRaisinNumericUpDown
            // 
            this.BlancMoutRaisinNumericUpDown.DecimalPlaces = 2;
            this.BlancMoutRaisinNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.BlancMoutRaisinNumericUpDown.Location = new System.Drawing.Point(226, 26);
            this.BlancMoutRaisinNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.BlancMoutRaisinNumericUpDown.Name = "BlancMoutRaisinNumericUpDown";
            this.BlancMoutRaisinNumericUpDown.Size = new System.Drawing.Size(76, 20);
            this.BlancMoutRaisinNumericUpDown.TabIndex = 0;
            this.BlancMoutRaisinNumericUpDown.Value = new decimal(new int[] {
            76,
            0,
            0,
            131072});
            // 
            // DegreValeurGroupBox
            // 
            this.DegreValeurGroupBox.AutoSize = true;
            this.DegreValeurGroupBox.Controls.Add(this.BrixRadioButton);
            this.DegreValeurGroupBox.Controls.Add(this.OechsleRadioButton);
            this.DegreValeurGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.DegreValeurGroupBox.Location = new System.Drawing.Point(28, 575);
            this.DegreValeurGroupBox.Name = "DegreValeurGroupBox";
            this.DegreValeurGroupBox.Size = new System.Drawing.Size(328, 63);
            this.DegreValeurGroupBox.TabIndex = 10;
            this.DegreValeurGroupBox.TabStop = false;
            this.DegreValeurGroupBox.Text = "Echelle degrés";
            // 
            // BrixRadioButton
            // 
            this.BrixRadioButton.AutoSize = true;
            this.BrixRadioButton.Location = new System.Drawing.Point(226, 27);
            this.BrixRadioButton.Name = "BrixRadioButton";
            this.BrixRadioButton.Size = new System.Drawing.Size(62, 17);
            this.BrixRadioButton.TabIndex = 0;
            this.BrixRadioButton.TabStop = true;
            this.BrixRadioButton.Text = "Brix (°B)";
            this.BrixRadioButton.UseVisualStyleBackColor = true;
            // 
            // OechsleRadioButton
            // 
            this.OechsleRadioButton.AutoSize = true;
            this.OechsleRadioButton.Location = new System.Drawing.Point(83, 27);
            this.OechsleRadioButton.Name = "OechsleRadioButton";
            this.OechsleRadioButton.Size = new System.Drawing.Size(91, 17);
            this.OechsleRadioButton.TabIndex = 0;
            this.OechsleRadioButton.TabStop = true;
            this.OechsleRadioButton.Text = "Oechsle (°Oe)";
            this.OechsleRadioButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(384, 20);
            this.label1.TabIndex = 19;
            this.label1.Text = "Paramètres de l\'application";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WfrmGlobalSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(384, 822);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MainTableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MinimumSize = new System.Drawing.Size(395, 564);
            this.Name = "WfrmGlobalSettings";
            this.Text = "Paramètres - Gestion des vendanges";
            this.TopMost = true;
            this.ExceedQuotaGroupBox.ResumeLayout(false);
            this.ExceedQuotaGroupBox.PerformLayout();
            this.UniteGroupBox.ResumeLayout(false);
            this.UniteGroupBox.PerformLayout();
            this.RateConvVinClairGroupBox.ResumeLayout(false);
            this.RateConvVinClairGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RougeVinClairNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlancVinClairNumericUpDown)).EndInit();
            this.SondageGroupBox.ResumeLayout(false);
            this.SondageGroupBox.PerformLayout();
            this.BonusUniteGroupBox.ResumeLayout(false);
            this.MainTableLayoutPanel.ResumeLayout(false);
            this.MainTableLayoutPanel.PerformLayout();
            this.FactureSelectionGroupBox.ResumeLayout(false);
            this.CantonGroupBox.ResumeLayout(false);
            this.ErpGroupBox.ResumeLayout(false);
            this.SyncContactGroupBox.ResumeLayout(false);
            this.SyncContactGroupBox.PerformLayout();
            this.RateConvMoutRaisinGroupBox.ResumeLayout(false);
            this.RateConvMoutRaisinGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RougeMoutRaisinNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlancMoutRaisinNumericUpDown)).EndInit();
            this.DegreValeurGroupBox.ResumeLayout(false);
            this.DegreValeurGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox ExceedQuotaGroupBox;
        private System.Windows.Forms.RadioButton rdbFalseExceedQuota;
        private System.Windows.Forms.RadioButton TrueExceedQuotaRadioButton;
        private System.Windows.Forms.GroupBox UniteGroupBox;
        private System.Windows.Forms.RadioButton LitresRadioButton;
        private System.Windows.Forms.RadioButton KiloRadioButton;
        private System.Windows.Forms.GroupBox RateConvVinClairGroupBox;
        private System.Windows.Forms.Label BlancClairLabel;
        private System.Windows.Forms.Label RougeClairLabel;
        private System.Windows.Forms.NumericUpDown BlancVinClairNumericUpDown;
        private System.Windows.Forms.NumericUpDown RougeVinClairNumericUpDown;
        private System.Windows.Forms.GroupBox SondageGroupBox;
        private System.Windows.Forms.RadioButton sondageOechsleRadioButton;
        private System.Windows.Forms.RadioButton sondageBricksRadioButton;
        private System.Windows.Forms.GroupBox BonusUniteGroupBox;
        private System.Windows.Forms.TableLayoutPanel MainTableLayoutPanel;
        private System.Windows.Forms.GroupBox DegreValeurGroupBox;
        private System.Windows.Forms.RadioButton BrixRadioButton;
        private System.Windows.Forms.RadioButton OechsleRadioButton;
        private System.Windows.Forms.GroupBox RateConvMoutRaisinGroupBox;
        private System.Windows.Forms.Label BlancMoutLabel;
        private System.Windows.Forms.Label RougeMoutLabel;
        private System.Windows.Forms.NumericUpDown BlancMoutRaisinNumericUpDown;
        private System.Windows.Forms.NumericUpDown RougeMoutRaisinNumericUpDown;
        private System.Windows.Forms.GroupBox CantonGroupBox;
        private System.Windows.Forms.ComboBox CantonComboBox;
        private System.Windows.Forms.GroupBox SyncContactGroupBox;
        private System.Windows.Forms.RadioButton unSyncContactRadioButton;
        private System.Windows.Forms.RadioButton syncContactRadioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox ErpGroupBox;
        private System.Windows.Forms.ComboBox ErpComboBox;
        private System.Windows.Forms.ComboBox typeTableBonusComboBox;
        private System.Windows.Forms.GroupBox FactureSelectionGroupBox;
        private System.Windows.Forms.ComboBox typeFactureComboBox;
    }
}