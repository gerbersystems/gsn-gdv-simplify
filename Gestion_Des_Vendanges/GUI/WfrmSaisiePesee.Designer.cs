﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSaisiePesee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label lblProducteur;
            System.Windows.Forms.Label lblProdPrenomNom;
            System.Windows.Forms.Label soldeKilosLabel;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label soldeLitresLabel;
            System.Windows.Forms.Label totalLitresLabel;
            System.Windows.Forms.Label totalLitresPeseesParAcquitLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmSaisiePesee));
            this.cOM_IDLabel = new System.Windows.Forms.Label();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pED_PESEE_DETAILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pED_PESEE_DETAILTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.pAM_PARAMETRESTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAM_PARAMETRESTableAdapter();
            this.pED_IDTextBox = new System.Windows.Forms.TextBox();
            this.CépagesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxCépagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pED_QUANTITE_KGTextBox = new System.Windows.Forms.TextBox();
            this.pED_QUANTITE_LITRESTextBox = new System.Windows.Forms.TextBox();
            this.lTitre = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.pEE_IDTextBox = new System.Windows.Forms.TextBox();
            this.pAM_PARAMETRESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.cEP_IDComboBox = new System.Windows.Forms.ComboBox();
            this.txtConversionLKg = new System.Windows.Forms.TextBox();
            this.optBlocageKg = new System.Windows.Forms.RadioButton();
            this.optBlocageLitre = new System.Windows.Forms.RadioButton();
            this.optBlocageConversion = new System.Windows.Forms.RadioButton();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.btnPrintQuittance = new System.Windows.Forms.Button();
            this.txtProducteur = new System.Windows.Forms.TextBox();
            this.pEE_TOTAL_QUANTITEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEE_TOTAL_QUANTITETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_TOTAL_QUANTITETableAdapter();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.limitationKilogrammesParCepageGroupBox = new System.Windows.Forms.GroupBox();
            this.soldeKilosTextBox = new System.Windows.Forms.TextBox();
            this.txtKilosAdmis = new System.Windows.Forms.TextBox();
            this.txtKilosSaisis = new System.Windows.Forms.TextBox();
            this.limitationLitresParCepageGroupBox = new System.Windows.Forms.GroupBox();
            this.soldeLitresTextBox = new System.Windows.Forms.TextBox();
            this.txtLitresAdmis = new System.Windows.Forms.TextBox();
            this.txtLitresSaisis = new System.Windows.Forms.TextBox();
            this.sondageBrixGroupBox = new System.Windows.Forms.GroupBox();
            this.pED_SONDAGE_BRIKSTextBox = new System.Windows.Forms.NumericUpDown();
            this.sondageOxelGroupBox = new System.Windows.Forms.GroupBox();
            this.pED_SONDAGE_OETextBox = new System.Windows.Forms.NumericUpDown();
            this.pED_LOTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.cOM_IDCombobox = new System.Windows.Forms.ComboBox();
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.pEC_PED_COMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEC_PED_COMTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEC_PED_COMTableAdapter();
            this.peL_PED_LIETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEL_PED_LIETableAdapter();
            this.lIEU_IDLabel = new System.Windows.Forms.Label();
            this.lIEU_IDCombobox = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMCOMMUNEBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            lblProducteur = new System.Windows.Forms.Label();
            lblProdPrenomNom = new System.Windows.Forms.Label();
            soldeKilosLabel = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            soldeLitresLabel = new System.Windows.Forms.Label();
            totalLitresLabel = new System.Windows.Forms.Label();
            totalLitresPeseesParAcquitLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILBindingSource)).BeginInit();
            this.CépagesContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAM_PARAMETRESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITEBindingSource)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.limitationKilogrammesParCepageGroupBox.SuspendLayout();
            this.limitationLitresParCepageGroupBox.SuspendLayout();
            this.sondageBrixGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pED_SONDAGE_BRIKSTextBox)).BeginInit();
            this.sondageOxelGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pED_SONDAGE_OETextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEC_PED_COMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label2.Location = new System.Drawing.Point(15, 98);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(27, 14);
            label2.TabIndex = 27;
            label2.Text = "Lot";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label3.Location = new System.Drawing.Point(48, 20);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(81, 14);
            label3.TabIndex = 28;
            label3.Text = "Sondage OE";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label4.Location = new System.Drawing.Point(15, 123);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(82, 14);
            label4.TabIndex = 29;
            label4.Text = "Quantité Kg";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label5.Location = new System.Drawing.Point(15, 174);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(104, 14);
            label5.TabIndex = 30;
            label5.Text = "Quantité Litres ";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label6.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label6.Location = new System.Drawing.Point(48, 15);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(96, 14);
            label6.TabIndex = 31;
            label6.Text = "Sondage Briks";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label1.Location = new System.Drawing.Point(15, 71);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(53, 14);
            label1.TabIndex = 76;
            label1.Text = "Cépage";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label7.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label7.Location = new System.Drawing.Point(15, 149);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(109, 14);
            label7.TabIndex = 78;
            label7.Text = "Conversion l/kg";
            // 
            // lblProducteur
            // 
            lblProducteur.AutoSize = true;
            lblProducteur.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblProducteur.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblProducteur.Location = new System.Drawing.Point(15, 46);
            lblProducteur.Name = "lblProducteur";
            lblProducteur.Size = new System.Drawing.Size(74, 14);
            lblProducteur.TabIndex = 84;
            lblProducteur.Text = "Producteur";
            // 
            // lblProdPrenomNom
            // 
            lblProdPrenomNom.AutoSize = true;
            lblProdPrenomNom.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblProdPrenomNom.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblProdPrenomNom.Location = new System.Drawing.Point(147, 46);
            lblProdPrenomNom.Name = "lblProdPrenomNom";
            lblProdPrenomNom.Size = new System.Drawing.Size(0, 14);
            lblProdPrenomNom.TabIndex = 85;
            // 
            // soldeKilosLabel
            // 
            soldeKilosLabel.AutoSize = true;
            soldeKilosLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            soldeKilosLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            soldeKilosLabel.Location = new System.Drawing.Point(45, 78);
            soldeKilosLabel.Name = "soldeKilosLabel";
            soldeKilosLabel.Size = new System.Drawing.Size(41, 14);
            soldeKilosLabel.TabIndex = 73;
            soldeKilosLabel.Text = "Solde";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label8.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label8.Location = new System.Drawing.Point(45, 27);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(47, 14);
            label8.TabIndex = 69;
            label8.Text = "Admis";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label9.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label9.Location = new System.Drawing.Point(45, 52);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(75, 14);
            label9.TabIndex = 71;
            label9.Text = "Déjà saisis";
            // 
            // soldeLitresLabel
            // 
            soldeLitresLabel.AutoSize = true;
            soldeLitresLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            soldeLitresLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            soldeLitresLabel.Location = new System.Drawing.Point(45, 78);
            soldeLitresLabel.Name = "soldeLitresLabel";
            soldeLitresLabel.Size = new System.Drawing.Size(41, 14);
            soldeLitresLabel.TabIndex = 73;
            soldeLitresLabel.Text = "Solde";
            // 
            // totalLitresLabel
            // 
            totalLitresLabel.AutoSize = true;
            totalLitresLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            totalLitresLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            totalLitresLabel.Location = new System.Drawing.Point(45, 27);
            totalLitresLabel.Name = "totalLitresLabel";
            totalLitresLabel.Size = new System.Drawing.Size(47, 14);
            totalLitresLabel.TabIndex = 69;
            totalLitresLabel.Text = "Admis";
            // 
            // totalLitresPeseesParAcquitLabel
            // 
            totalLitresPeseesParAcquitLabel.AutoSize = true;
            totalLitresPeseesParAcquitLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            totalLitresPeseesParAcquitLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            totalLitresPeseesParAcquitLabel.Location = new System.Drawing.Point(45, 52);
            totalLitresPeseesParAcquitLabel.Name = "totalLitresPeseesParAcquitLabel";
            totalLitresPeseesParAcquitLabel.Size = new System.Drawing.Size(75, 14);
            totalLitresPeseesParAcquitLabel.TabIndex = 71;
            totalLitresPeseesParAcquitLabel.Text = "Déjà saisis";
            // 
            // cOM_IDLabel
            // 
            this.cOM_IDLabel.AutoSize = true;
            this.cOM_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOM_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cOM_IDLabel.Location = new System.Drawing.Point(15, 231);
            this.cOM_IDLabel.Name = "cOM_IDLabel";
            this.cOM_IDLabel.Size = new System.Drawing.Size(71, 14);
            this.cOM_IDLabel.TabIndex = 89;
            this.cOM_IDLabel.Text = "Commune";
            this.cOM_IDLabel.Visible = false;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pED_PESEE_DETAILBindingSource
            // 
            this.pED_PESEE_DETAILBindingSource.DataMember = "PED_PESEE_DETAIL";
            this.pED_PESEE_DETAILBindingSource.DataSource = this.dSPesees;
            // 
            // pED_PESEE_DETAILTableAdapter
            // 
            this.pED_PESEE_DETAILTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = this.cEP_CEPAGETableAdapter;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = this.lIE_LIEU_DE_PRODUCTIONTableAdapter;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = this.pAM_PARAMETRESTableAdapter;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = this.pED_PESEE_DETAILTableAdapter;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // pAM_PARAMETRESTableAdapter
            // 
            this.pAM_PARAMETRESTableAdapter.ClearBeforeFill = true;
            // 
            // pED_IDTextBox
            // 
            this.pED_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pED_PESEE_DETAILBindingSource, "PED_ID", true));
            this.pED_IDTextBox.Location = new System.Drawing.Point(313, 12);
            this.pED_IDTextBox.Name = "pED_IDTextBox";
            this.pED_IDTextBox.Size = new System.Drawing.Size(108, 20);
            this.pED_IDTextBox.TabIndex = 2;
            this.pED_IDTextBox.Text = " ";
            // 
            // CépagesContextMenuStrip
            // 
            this.CépagesContextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.CépagesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxCépagesToolStripMenuItem});
            this.CépagesContextMenuStrip.Name = "CépagesContextMenuStrip";
            this.CépagesContextMenuStrip.Size = new System.Drawing.Size(197, 26);
            // 
            // accéderAuxCépagesToolStripMenuItem
            // 
            this.accéderAuxCépagesToolStripMenuItem.Name = "accéderAuxCépagesToolStripMenuItem";
            this.accéderAuxCépagesToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.accéderAuxCépagesToolStripMenuItem.Text = "Accéder aux cépages ...";
            this.accéderAuxCépagesToolStripMenuItem.Click += new System.EventHandler(this.accederAuxCepagesToolStripMenuItem_Click);
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // pED_QUANTITE_KGTextBox
            // 
            this.pED_QUANTITE_KGTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pED_PESEE_DETAILBindingSource, "PED_QUANTITE_KG", true));
            this.pED_QUANTITE_KGTextBox.Location = new System.Drawing.Point(150, 122);
            this.pED_QUANTITE_KGTextBox.Name = "pED_QUANTITE_KGTextBox";
            this.pED_QUANTITE_KGTextBox.Size = new System.Drawing.Size(201, 20);
            this.pED_QUANTITE_KGTextBox.TabIndex = 4;
            this.pED_QUANTITE_KGTextBox.TextChanged += new System.EventHandler(this.pED_QUANTITE_KGTextBox_TextChanged);
            this.pED_QUANTITE_KGTextBox.Validated += new System.EventHandler(this.ConversionChanged);
            // 
            // pED_QUANTITE_LITRESTextBox
            // 
            this.pED_QUANTITE_LITRESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pED_PESEE_DETAILBindingSource, "PED_QUANTITE_LITRES", true));
            this.pED_QUANTITE_LITRESTextBox.Location = new System.Drawing.Point(150, 174);
            this.pED_QUANTITE_LITRESTextBox.Name = "pED_QUANTITE_LITRESTextBox";
            this.pED_QUANTITE_LITRESTextBox.ReadOnly = true;
            this.pED_QUANTITE_LITRESTextBox.Size = new System.Drawing.Size(201, 20);
            this.pED_QUANTITE_LITRESTextBox.TabIndex = 6;
            this.pED_QUANTITE_LITRESTextBox.TextChanged += new System.EventHandler(this.pED_QUANTITE_LITRESTextBox_TextChanged);
            this.pED_QUANTITE_LITRESTextBox.Validated += new System.EventHandler(this.ConversionChanged);
            // 
            // lTitre
            // 
            this.lTitre.AutoSize = true;
            this.lTitre.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lTitre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lTitre.Location = new System.Drawing.Point(12, 9);
            this.lTitre.Name = "lTitre";
            this.lTitre.Size = new System.Drawing.Size(155, 18);
            this.lTitre.TabIndex = 24;
            this.lTitre.Text = "Ajout d\'une pesée";
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdOK.Location = new System.Drawing.Point(565, 221);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 30);
            this.cmdOK.TabIndex = 7;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // pEE_IDTextBox
            // 
            this.pEE_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pED_PESEE_DETAILBindingSource, "PEE_ID", true));
            this.pEE_IDTextBox.Location = new System.Drawing.Point(188, 12);
            this.pEE_IDTextBox.Name = "pEE_IDTextBox";
            this.pEE_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.pEE_IDTextBox.TabIndex = 26;
            this.pEE_IDTextBox.Text = "PEE ID";
            // 
            // pAM_PARAMETRESBindingSource
            // 
            this.pAM_PARAMETRESBindingSource.DataMember = "PAM_PARAMETRES";
            this.pAM_PARAMETRESBindingSource.DataSource = this.dSPesees;
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // cEP_IDComboBox
            // 
            this.cEP_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cEP_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cEP_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pED_PESEE_DETAILBindingSource, "CEP_ID", true));
            this.cEP_IDComboBox.DataSource = this.cEPCEPAGEBindingSource;
            this.cEP_IDComboBox.DisplayMember = "CEP_NOM";
            this.cEP_IDComboBox.FormattingEnabled = true;
            this.cEP_IDComboBox.Location = new System.Drawing.Point(150, 69);
            this.cEP_IDComboBox.Name = "cEP_IDComboBox";
            this.cEP_IDComboBox.Size = new System.Drawing.Size(201, 21);
            this.cEP_IDComboBox.TabIndex = 0;
            this.cEP_IDComboBox.ValueMember = "CEP_ID";
            this.cEP_IDComboBox.SelectedValueChanged += new System.EventHandler(this.cEP_IDComboBox_SelectedValueChanged);
            // 
            // txtConversionLKg
            // 
            this.txtConversionLKg.Location = new System.Drawing.Point(150, 148);
            this.txtConversionLKg.Name = "txtConversionLKg";
            this.txtConversionLKg.Size = new System.Drawing.Size(201, 20);
            this.txtConversionLKg.TabIndex = 5;
            this.txtConversionLKg.Validated += new System.EventHandler(this.ConversionChanged);
            // 
            // optBlocageKg
            // 
            this.optBlocageKg.Appearance = System.Windows.Forms.Appearance.Button;
            this.optBlocageKg.AutoSize = true;
            this.optBlocageKg.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.optBlocageKg.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.optBlocageKg.Image = global::Gestion_Des_Vendanges.Properties.Resources.punaise;
            this.optBlocageKg.Location = new System.Drawing.Point(357, 121);
            this.optBlocageKg.Name = "optBlocageKg";
            this.optBlocageKg.Size = new System.Drawing.Size(22, 22);
            this.optBlocageKg.TabIndex = 79;
            this.optBlocageKg.UseVisualStyleBackColor = false;
            this.optBlocageKg.CheckedChanged += new System.EventHandler(this.optBlocageKg_CheckedChanged);
            // 
            // optBlocageLitre
            // 
            this.optBlocageLitre.Appearance = System.Windows.Forms.Appearance.Button;
            this.optBlocageLitre.AutoSize = true;
            this.optBlocageLitre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.optBlocageLitre.Checked = true;
            this.optBlocageLitre.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.optBlocageLitre.Image = global::Gestion_Des_Vendanges.Properties.Resources.punaise;
            this.optBlocageLitre.Location = new System.Drawing.Point(357, 173);
            this.optBlocageLitre.Name = "optBlocageLitre";
            this.optBlocageLitre.Size = new System.Drawing.Size(22, 22);
            this.optBlocageLitre.TabIndex = 80;
            this.optBlocageLitre.TabStop = true;
            this.optBlocageLitre.UseVisualStyleBackColor = false;
            this.optBlocageLitre.CheckedChanged += new System.EventHandler(this.optBlocageKg_CheckedChanged);
            // 
            // optBlocageConversion
            // 
            this.optBlocageConversion.Appearance = System.Windows.Forms.Appearance.Button;
            this.optBlocageConversion.AutoSize = true;
            this.optBlocageConversion.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.optBlocageConversion.Image = global::Gestion_Des_Vendanges.Properties.Resources.punaise;
            this.optBlocageConversion.Location = new System.Drawing.Point(357, 147);
            this.optBlocageConversion.Name = "optBlocageConversion";
            this.optBlocageConversion.Size = new System.Drawing.Size(22, 22);
            this.optBlocageConversion.TabIndex = 81;
            this.optBlocageConversion.UseVisualStyleBackColor = false;
            this.optBlocageConversion.CheckedChanged += new System.EventHandler(this.optBlocageKg_CheckedChanged);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(646, 221);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 30);
            this.cmdCancel.TabIndex = 8;
            this.cmdCancel.Text = "Annuler";
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // btnPrintQuittance
            // 
            this.btnPrintQuittance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintQuittance.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPrintQuittance.Location = new System.Drawing.Point(484, 220);
            this.btnPrintQuittance.Name = "btnPrintQuittance";
            this.btnPrintQuittance.Size = new System.Drawing.Size(75, 30);
            this.btnPrintQuittance.TabIndex = 82;
            this.btnPrintQuittance.Text = "Quittance";
            this.btnPrintQuittance.UseVisualStyleBackColor = false;
            this.btnPrintQuittance.Click += new System.EventHandler(this.btnPrintQuittance_Click);
            // 
            // txtProducteur
            // 
            this.txtProducteur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(255)))), ((int)(((byte)(190)))));
            this.txtProducteur.CausesValidation = false;
            this.txtProducteur.Location = new System.Drawing.Point(150, 44);
            this.txtProducteur.Name = "txtProducteur";
            this.txtProducteur.ReadOnly = true;
            this.txtProducteur.Size = new System.Drawing.Size(201, 20);
            this.txtProducteur.TabIndex = 83;
            // 
            // pEE_TOTAL_QUANTITEBindingSource
            // 
            this.pEE_TOTAL_QUANTITEBindingSource.DataMember = "PEE_TOTAL_QUANTITE";
            this.pEE_TOTAL_QUANTITEBindingSource.DataSource = this.dSPesees;
            // 
            // pEE_TOTAL_QUANTITETableAdapter
            // 
            this.pEE_TOTAL_QUANTITETableAdapter.ClearBeforeFill = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.limitationKilogrammesParCepageGroupBox);
            this.flowLayoutPanel1.Controls.Add(this.limitationLitresParCepageGroupBox);
            this.flowLayoutPanel1.Controls.Add(this.sondageBrixGroupBox);
            this.flowLayoutPanel1.Controls.Add(this.sondageOxelGroupBox);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(385, 38);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(336, 176);
            this.flowLayoutPanel1.TabIndex = 86;
            // 
            // limitationKilogrammesParCepageGroupBox
            // 
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(soldeKilosLabel);
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(this.soldeKilosTextBox);
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(label8);
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(label9);
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(this.txtKilosAdmis);
            this.limitationKilogrammesParCepageGroupBox.Controls.Add(this.txtKilosSaisis);
            this.limitationKilogrammesParCepageGroupBox.Location = new System.Drawing.Point(3, 3);
            this.limitationKilogrammesParCepageGroupBox.Name = "limitationKilogrammesParCepageGroupBox";
            this.limitationKilogrammesParCepageGroupBox.Size = new System.Drawing.Size(326, 107);
            this.limitationKilogrammesParCepageGroupBox.TabIndex = 77;
            this.limitationKilogrammesParCepageGroupBox.TabStop = false;
            this.limitationKilogrammesParCepageGroupBox.Text = "Limitation kilos par cépage";
            // 
            // soldeKilosTextBox
            // 
            this.soldeKilosTextBox.BackColor = System.Drawing.Color.White;
            this.soldeKilosTextBox.Location = new System.Drawing.Point(184, 78);
            this.soldeKilosTextBox.Name = "soldeKilosTextBox";
            this.soldeKilosTextBox.ReadOnly = true;
            this.soldeKilosTextBox.Size = new System.Drawing.Size(100, 20);
            this.soldeKilosTextBox.TabIndex = 74;
            this.soldeKilosTextBox.TabStop = false;
            // 
            // txtKilosAdmis
            // 
            this.txtKilosAdmis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtKilosAdmis.Location = new System.Drawing.Point(184, 26);
            this.txtKilosAdmis.Name = "txtKilosAdmis";
            this.txtKilosAdmis.ReadOnly = true;
            this.txtKilosAdmis.Size = new System.Drawing.Size(100, 20);
            this.txtKilosAdmis.TabIndex = 70;
            this.txtKilosAdmis.TabStop = false;
            // 
            // txtKilosSaisis
            // 
            this.txtKilosSaisis.BackColor = System.Drawing.Color.White;
            this.txtKilosSaisis.Location = new System.Drawing.Point(184, 52);
            this.txtKilosSaisis.Name = "txtKilosSaisis";
            this.txtKilosSaisis.ReadOnly = true;
            this.txtKilosSaisis.Size = new System.Drawing.Size(100, 20);
            this.txtKilosSaisis.TabIndex = 72;
            this.txtKilosSaisis.TabStop = false;
            // 
            // limitationLitresParCepageGroupBox
            // 
            this.limitationLitresParCepageGroupBox.Controls.Add(soldeLitresLabel);
            this.limitationLitresParCepageGroupBox.Controls.Add(this.soldeLitresTextBox);
            this.limitationLitresParCepageGroupBox.Controls.Add(totalLitresLabel);
            this.limitationLitresParCepageGroupBox.Controls.Add(totalLitresPeseesParAcquitLabel);
            this.limitationLitresParCepageGroupBox.Controls.Add(this.txtLitresAdmis);
            this.limitationLitresParCepageGroupBox.Controls.Add(this.txtLitresSaisis);
            this.limitationLitresParCepageGroupBox.Location = new System.Drawing.Point(335, 3);
            this.limitationLitresParCepageGroupBox.Name = "limitationLitresParCepageGroupBox";
            this.limitationLitresParCepageGroupBox.Size = new System.Drawing.Size(326, 114);
            this.limitationLitresParCepageGroupBox.TabIndex = 76;
            this.limitationLitresParCepageGroupBox.TabStop = false;
            this.limitationLitresParCepageGroupBox.Text = "Limitation litres par cépage";
            // 
            // soldeLitresTextBox
            // 
            this.soldeLitresTextBox.BackColor = System.Drawing.Color.White;
            this.soldeLitresTextBox.Location = new System.Drawing.Point(184, 78);
            this.soldeLitresTextBox.Name = "soldeLitresTextBox";
            this.soldeLitresTextBox.ReadOnly = true;
            this.soldeLitresTextBox.Size = new System.Drawing.Size(100, 20);
            this.soldeLitresTextBox.TabIndex = 74;
            this.soldeLitresTextBox.TabStop = false;
            // 
            // txtLitresAdmis
            // 
            this.txtLitresAdmis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLitresAdmis.Location = new System.Drawing.Point(184, 26);
            this.txtLitresAdmis.Name = "txtLitresAdmis";
            this.txtLitresAdmis.ReadOnly = true;
            this.txtLitresAdmis.Size = new System.Drawing.Size(100, 20);
            this.txtLitresAdmis.TabIndex = 70;
            this.txtLitresAdmis.TabStop = false;
            // 
            // txtLitresSaisis
            // 
            this.txtLitresSaisis.BackColor = System.Drawing.Color.White;
            this.txtLitresSaisis.Location = new System.Drawing.Point(184, 52);
            this.txtLitresSaisis.Name = "txtLitresSaisis";
            this.txtLitresSaisis.ReadOnly = true;
            this.txtLitresSaisis.Size = new System.Drawing.Size(100, 20);
            this.txtLitresSaisis.TabIndex = 72;
            this.txtLitresSaisis.TabStop = false;
            // 
            // sondageBrixGroupBox
            // 
            this.sondageBrixGroupBox.Controls.Add(this.pED_SONDAGE_BRIKSTextBox);
            this.sondageBrixGroupBox.Controls.Add(label6);
            this.sondageBrixGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sondageBrixGroupBox.Location = new System.Drawing.Point(332, 120);
            this.sondageBrixGroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.sondageBrixGroupBox.Name = "sondageBrixGroupBox";
            this.sondageBrixGroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.sondageBrixGroupBox.Size = new System.Drawing.Size(329, 41);
            this.sondageBrixGroupBox.TabIndex = 90;
            this.sondageBrixGroupBox.TabStop = false;
            // 
            // pED_SONDAGE_BRIKSTextBox
            // 
            this.pED_SONDAGE_BRIKSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pED_PESEE_DETAILBindingSource, "PED_SONDAGE_BRIKS", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N0"));
            this.pED_SONDAGE_BRIKSTextBox.DecimalPlaces = 1;
            this.pED_SONDAGE_BRIKSTextBox.Location = new System.Drawing.Point(187, 11);
            this.pED_SONDAGE_BRIKSTextBox.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.pED_SONDAGE_BRIKSTextBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pED_SONDAGE_BRIKSTextBox.Name = "pED_SONDAGE_BRIKSTextBox";
            this.pED_SONDAGE_BRIKSTextBox.Size = new System.Drawing.Size(100, 20);
            this.pED_SONDAGE_BRIKSTextBox.TabIndex = 88;
            this.pED_SONDAGE_BRIKSTextBox.Value = new decimal(new int[] {
            19,
            0,
            0,
            0});
            // 
            // sondageOxelGroupBox
            // 
            this.sondageOxelGroupBox.Controls.Add(this.pED_SONDAGE_OETextBox);
            this.sondageOxelGroupBox.Controls.Add(label3);
            this.sondageOxelGroupBox.Location = new System.Drawing.Point(664, 0);
            this.sondageOxelGroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.sondageOxelGroupBox.Name = "sondageOxelGroupBox";
            this.sondageOxelGroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.sondageOxelGroupBox.Size = new System.Drawing.Size(329, 44);
            this.sondageOxelGroupBox.TabIndex = 91;
            this.sondageOxelGroupBox.TabStop = false;
            // 
            // pED_SONDAGE_OETextBox
            // 
            this.pED_SONDAGE_OETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pED_PESEE_DETAILBindingSource, "PED_SONDAGE_OE", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N0"));
            this.pED_SONDAGE_OETextBox.DecimalPlaces = 1;
            this.pED_SONDAGE_OETextBox.Location = new System.Drawing.Point(187, 16);
            this.pED_SONDAGE_OETextBox.Maximum = new decimal(new int[] {
            130,
            0,
            0,
            0});
            this.pED_SONDAGE_OETextBox.Minimum = new decimal(new int[] {
            39,
            0,
            0,
            65536});
            this.pED_SONDAGE_OETextBox.Name = "pED_SONDAGE_OETextBox";
            this.pED_SONDAGE_OETextBox.Size = new System.Drawing.Size(100, 20);
            this.pED_SONDAGE_OETextBox.TabIndex = 89;
            this.pED_SONDAGE_OETextBox.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // pED_LOTTextBox
            // 
            this.pED_LOTTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pED_PESEE_DETAILBindingSource, "PED_LOT", true));
            this.pED_LOTTextBox.Location = new System.Drawing.Point(150, 96);
            this.pED_LOTTextBox.Mask = "0000 00000";
            this.pED_LOTTextBox.Name = "pED_LOTTextBox";
            this.pED_LOTTextBox.Size = new System.Drawing.Size(201, 20);
            this.pED_LOTTextBox.TabIndex = 87;
            this.pED_LOTTextBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // cOM_IDCombobox
            // 
            this.cOM_IDCombobox.DataSource = this.cOMCOMMUNEBindingSource;
            this.cOM_IDCombobox.DisplayMember = "COM_NOM";
            this.cOM_IDCombobox.FormattingEnabled = true;
            this.cOM_IDCombobox.Location = new System.Drawing.Point(150, 230);
            this.cOM_IDCombobox.Name = "cOM_IDCombobox";
            this.cOM_IDCombobox.Size = new System.Drawing.Size(201, 21);
            this.cOM_IDCombobox.TabIndex = 88;
            this.cOM_IDCombobox.ValueMember = "COM_ID";
            this.cOM_IDCombobox.Visible = false;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // pEC_PED_COMBindingSource
            // 
            this.pEC_PED_COMBindingSource.DataMember = "PEC_PED_COM";
            this.pEC_PED_COMBindingSource.DataSource = this.dSPesees;
            // 
            // pEC_PED_COMTableAdapter
            // 
            this.pEC_PED_COMTableAdapter.ClearBeforeFill = true;
            // 
            // peL_PED_LIETableAdapter
            // 
            this.peL_PED_LIETableAdapter.ClearBeforeFill = true;
            // 
            // lIEU_IDLabel
            // 
            this.lIEU_IDLabel.AutoSize = true;
            this.lIEU_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lIEU_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lIEU_IDLabel.Location = new System.Drawing.Point(15, 201);
            this.lIEU_IDLabel.Name = "lIEU_IDLabel";
            this.lIEU_IDLabel.Size = new System.Drawing.Size(35, 14);
            this.lIEU_IDLabel.TabIndex = 91;
            this.lIEU_IDLabel.Text = "AOC";
            this.lIEU_IDLabel.Visible = false;
            // 
            // lIEU_IDCombobox
            // 
            this.lIEU_IDCombobox.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.lIEU_IDCombobox.DisplayMember = "LIE_NOM";
            this.lIEU_IDCombobox.FormattingEnabled = true;
            this.lIEU_IDCombobox.Location = new System.Drawing.Point(150, 200);
            this.lIEU_IDCombobox.Name = "lIEU_IDCombobox";
            this.lIEU_IDCombobox.Size = new System.Drawing.Size(201, 21);
            this.lIEU_IDCombobox.TabIndex = 90;
            this.lIEU_IDCombobox.ValueMember = "LIE_ID";
            this.lIEU_IDCombobox.Visible = false;
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // cOMCOMMUNEBindingSource1
            // 
            this.cOMCOMMUNEBindingSource1.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource1.DataSource = this.dSPesees;
            // 
            // WfrmSaisiePesee
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(733, 262);
            this.Controls.Add(this.lIEU_IDLabel);
            this.Controls.Add(this.lIEU_IDCombobox);
            this.Controls.Add(this.cOM_IDLabel);
            this.Controls.Add(this.cOM_IDCombobox);
            this.Controls.Add(this.pED_LOTTextBox);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(lblProdPrenomNom);
            this.Controls.Add(lblProducteur);
            this.Controls.Add(this.txtProducteur);
            this.Controls.Add(this.btnPrintQuittance);
            this.Controls.Add(label1);
            this.Controls.Add(this.pEE_IDTextBox);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lTitre);
            this.Controls.Add(this.pED_IDTextBox);
            this.Controls.Add(this.cEP_IDComboBox);
            this.Controls.Add(this.optBlocageConversion);
            this.Controls.Add(this.optBlocageLitre);
            this.Controls.Add(label7);
            this.Controls.Add(this.optBlocageKg);
            this.Controls.Add(this.txtConversionLKg);
            this.Controls.Add(label4);
            this.Controls.Add(label2);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.pED_QUANTITE_LITRESTextBox);
            this.Controls.Add(label5);
            this.Controls.Add(this.pED_QUANTITE_KGTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(749, 358000);
            this.MinimumSize = new System.Drawing.Size(749, 300);
            this.Name = "WfrmSaisiePesee";
            this.Text = "Gestion des vendanges - Saisie d\'une pesée";
            this.Load += new System.EventHandler(this.OnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILBindingSource)).EndInit();
            this.CépagesContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAM_PARAMETRESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITEBindingSource)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.limitationKilogrammesParCepageGroupBox.ResumeLayout(false);
            this.limitationKilogrammesParCepageGroupBox.PerformLayout();
            this.limitationLitresParCepageGroupBox.ResumeLayout(false);
            this.limitationLitresParCepageGroupBox.PerformLayout();
            this.sondageBrixGroupBox.ResumeLayout(false);
            this.sondageBrixGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pED_SONDAGE_BRIKSTextBox)).EndInit();
            this.sondageOxelGroupBox.ResumeLayout(false);
            this.sondageOxelGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pED_SONDAGE_OETextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEC_PED_COMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pED_PESEE_DETAILBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter pED_PESEE_DETAILTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
		private System.Windows.Forms.TextBox pED_IDTextBox;
        private System.Windows.Forms.TextBox pED_QUANTITE_KGTextBox;
		private System.Windows.Forms.TextBox pED_QUANTITE_LITRESTextBox;
        private System.Windows.Forms.Label lTitre;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.TextBox pEE_IDTextBox;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAM_PARAMETRESTableAdapter pAM_PARAMETRESTableAdapter;
        private System.Windows.Forms.BindingSource pAM_PARAMETRESBindingSource;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.ContextMenuStrip CépagesContextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem accéderAuxCépagesToolStripMenuItem;
        private System.Windows.Forms.ComboBox cEP_IDComboBox;
        private System.Windows.Forms.TextBox txtConversionLKg;
        private System.Windows.Forms.RadioButton optBlocageKg;
        private System.Windows.Forms.RadioButton optBlocageLitre;
        private System.Windows.Forms.RadioButton optBlocageConversion;
		private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button btnPrintQuittance;
        private System.Windows.Forms.TextBox txtProducteur;
        private System.Windows.Forms.BindingSource pEE_TOTAL_QUANTITEBindingSource;
		private DAO.DSPeseesTableAdapters.PEE_TOTAL_QUANTITETableAdapter pEE_TOTAL_QUANTITETableAdapter;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.GroupBox limitationKilogrammesParCepageGroupBox;
		public System.Windows.Forms.TextBox soldeKilosTextBox;
		public System.Windows.Forms.TextBox txtKilosAdmis;
		public System.Windows.Forms.TextBox txtKilosSaisis;
		private System.Windows.Forms.GroupBox limitationLitresParCepageGroupBox;
		public System.Windows.Forms.TextBox soldeLitresTextBox;
		public System.Windows.Forms.TextBox txtLitresAdmis;
		public System.Windows.Forms.TextBox txtLitresSaisis;
		private System.Windows.Forms.MaskedTextBox pED_LOTTextBox;
		private System.Windows.Forms.NumericUpDown pED_SONDAGE_BRIKSTextBox;
		private System.Windows.Forms.NumericUpDown pED_SONDAGE_OETextBox;
		private System.Windows.Forms.GroupBox sondageBrixGroupBox;
		private System.Windows.Forms.GroupBox sondageOxelGroupBox;
        private System.Windows.Forms.ComboBox cOM_IDCombobox;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.Label cOM_IDLabel;
        private System.Windows.Forms.BindingSource pEC_PED_COMBindingSource;
        private DAO.DSPeseesTableAdapters.PEC_PED_COMTableAdapter pEC_PED_COMTableAdapter;
        private DAO.DSPeseesTableAdapters.PEL_PED_LIETableAdapter peL_PED_LIETableAdapter;
        private System.Windows.Forms.Label lIEU_IDLabel;
        private System.Windows.Forms.ComboBox lIEU_IDCombobox;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource1;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
    }
}