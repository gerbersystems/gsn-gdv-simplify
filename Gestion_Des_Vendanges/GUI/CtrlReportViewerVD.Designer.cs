﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtrlReportViewer));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolFirst = new System.Windows.Forms.ToolStripButton();
            this.toolBefore = new System.Windows.Forms.ToolStripButton();
            this.toolPage = new System.Windows.Forms.ToolStripTextBox();
            this.toolLabelPage = new System.Windows.Forms.ToolStripLabel();
            this.toolNext = new System.Windows.Forms.ToolStripButton();
            this.toolLast = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tooPrint = new System.Windows.Forms.ToolStripButton();
            this.toolLargeurPage = new System.Windows.Forms.ToolStripButton();
            this.toolExporter = new System.Windows.Forms.ToolStripDropDownButton();
            this.portableDocumentFormatPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.microsoftWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.microsoftExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolZoomCb = new System.Windows.Forms.ToolStripComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.YearPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labProducteur = new System.Windows.Forms.Label();
            this.chkAllProd = new System.Windows.Forms.CheckBox();
            this.cbProducteur = new System.Windows.Forms.ComboBox();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.labDate = new System.Windows.Forms.Label();
            this.btnFilterDate = new System.Windows.Forms.Button();
            this.datePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.labTo = new System.Windows.Forms.Label();
            this.labFrom = new System.Windows.Forms.Label();
            this.datePickerTo = new System.Windows.Forms.DateTimePicker();
            this.gbRapport = new System.Windows.Forms.GroupBox();
            this.optAttestationsFiscales = new System.Windows.Forms.RadioButton();
            this.optApportsProducteurMout = new System.Windows.Forms.RadioButton();
            this.optApportsProducteur = new System.Windows.Forms.RadioButton();
            this.optAttestationControleAvecSoldeParAcquit = new System.Windows.Forms.RadioButton();
            this.optAttestationControleAvecSolde = new System.Windows.Forms.RadioButton();
            this.optListeParcelles = new System.Windows.Forms.RadioButton();
            this.optBonusDetail = new System.Windows.Forms.RadioButton();
            this.optPaiements = new System.Windows.Forms.RadioButton();
            this.optTablesPrix = new System.Windows.Forms.RadioButton();
            this.optAttestationControle = new System.Windows.Forms.RadioButton();
            this.optDroitsProduction = new System.Windows.Forms.RadioButton();
            this.optDeclarationEncavage = new System.Windows.Forms.RadioButton();
            this.optListeAcquit = new System.Windows.Forms.RadioButton();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.YearPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.gbRapport.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip1.Enabled = false;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolFirst,
            this.toolBefore,
            this.toolPage,
            this.toolLabelPage,
            this.toolNext,
            this.toolLast,
            this.toolStripSeparator1,
            this.toolRefresh,
            this.toolStripSeparator3,
            this.tooPrint,
            this.toolLargeurPage,
            this.toolExporter,
            this.toolStripSeparator2,
            this.toolZoomCb});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(543, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolFirst
            // 
            this.toolFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolFirst.Image = ((System.Drawing.Image)(resources.GetObject("toolFirst.Image")));
            this.toolFirst.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolFirst.Name = "toolFirst";
            this.toolFirst.Size = new System.Drawing.Size(23, 22);
            this.toolFirst.Text = "Première page";
            this.toolFirst.Click += new System.EventHandler(this.toolFirst_Click);
            // 
            // toolBefore
            // 
            this.toolBefore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBefore.Image = ((System.Drawing.Image)(resources.GetObject("toolBefore.Image")));
            this.toolBefore.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolBefore.Name = "toolBefore";
            this.toolBefore.Size = new System.Drawing.Size(23, 22);
            this.toolBefore.Text = "Page précédente";
            this.toolBefore.Click += new System.EventHandler(this.toolBefore_Click);
            // 
            // toolPage
            // 
            this.toolPage.Name = "toolPage";
            this.toolPage.Size = new System.Drawing.Size(40, 25);
            this.toolPage.Text = "0";
            this.toolPage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolPage_KeyPress);
            // 
            // toolLabelPage
            // 
            this.toolLabelPage.Name = "toolLabelPage";
            this.toolLabelPage.Size = new System.Drawing.Size(26, 22);
            this.toolLabelPage.Text = "sur ";
            // 
            // toolNext
            // 
            this.toolNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNext.Image = ((System.Drawing.Image)(resources.GetObject("toolNext.Image")));
            this.toolNext.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolNext.Name = "toolNext";
            this.toolNext.Size = new System.Drawing.Size(23, 22);
            this.toolNext.Text = "Page suivante";
            this.toolNext.Click += new System.EventHandler(this.toolNext_Click);
            // 
            // toolLast
            // 
            this.toolLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolLast.Image = ((System.Drawing.Image)(resources.GetObject("toolLast.Image")));
            this.toolLast.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolLast.Name = "toolLast";
            this.toolLast.Size = new System.Drawing.Size(23, 22);
            this.toolLast.Text = "Dernière page";
            this.toolLast.Click += new System.EventHandler(this.toolLast_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolRefresh
            // 
            this.toolRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolRefresh.Image = global::Gestion_Des_Vendanges.Properties.Resources.refresh;
            this.toolRefresh.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolRefresh.Name = "toolRefresh";
            this.toolRefresh.Size = new System.Drawing.Size(23, 22);
            this.toolRefresh.Text = "Actualiser";
            this.toolRefresh.Click += new System.EventHandler(this.toolRefresh_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tooPrint
            // 
            this.tooPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tooPrint.Image = global::Gestion_Des_Vendanges.Properties.Resources.print;
            this.tooPrint.ImageTransparentColor = System.Drawing.Color.Black;
            this.tooPrint.Name = "tooPrint";
            this.tooPrint.Size = new System.Drawing.Size(23, 22);
            this.tooPrint.Text = "Imprimer";
            this.tooPrint.Click += new System.EventHandler(this.tooPrint_Click);
            // 
            // toolLargeurPage
            // 
            this.toolLargeurPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolLargeurPage.Image = global::Gestion_Des_Vendanges.Properties.Resources.sizePage;
            this.toolLargeurPage.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolLargeurPage.Name = "toolLargeurPage";
            this.toolLargeurPage.Size = new System.Drawing.Size(23, 22);
            this.toolLargeurPage.Text = "Page entière";
            this.toolLargeurPage.Click += new System.EventHandler(this.toolLargeurPage_Click);
            // 
            // toolExporter
            // 
            this.toolExporter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolExporter.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.portableDocumentFormatPDFToolStripMenuItem,
            this.microsoftWordToolStripMenuItem,
            this.microsoftExcelToolStripMenuItem});
            this.toolExporter.Image = global::Gestion_Des_Vendanges.Properties.Resources.save;
            this.toolExporter.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolExporter.Name = "toolExporter";
            this.toolExporter.Size = new System.Drawing.Size(29, 22);
            this.toolExporter.Text = "Exporter";
            // 
            // portableDocumentFormatPDFToolStripMenuItem
            // 
            this.portableDocumentFormatPDFToolStripMenuItem.Name = "portableDocumentFormatPDFToolStripMenuItem";
            this.portableDocumentFormatPDFToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.portableDocumentFormatPDFToolStripMenuItem.Text = "Portable Document Format (PDF)";
            this.portableDocumentFormatPDFToolStripMenuItem.Click += new System.EventHandler(this.portableDocumentFormatPDFToolStripMenuItem_Click);
            // 
            // microsoftWordToolStripMenuItem
            // 
            this.microsoftWordToolStripMenuItem.Name = "microsoftWordToolStripMenuItem";
            this.microsoftWordToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.microsoftWordToolStripMenuItem.Text = "Microsoft Word";
            this.microsoftWordToolStripMenuItem.Click += new System.EventHandler(this.microsoftWordToolStripMenuItem_Click);
            // 
            // microsoftExcelToolStripMenuItem
            // 
            this.microsoftExcelToolStripMenuItem.Name = "microsoftExcelToolStripMenuItem";
            this.microsoftExcelToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.microsoftExcelToolStripMenuItem.Text = "Microsoft Excel";
            this.microsoftExcelToolStripMenuItem.Click += new System.EventHandler(this.microsoftExcelToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolZoomCb
            // 
            this.toolZoomCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolZoomCb.Name = "toolZoomCb";
            this.toolZoomCb.Size = new System.Drawing.Size(100, 25);
            this.toolZoomCb.SelectedIndexChanged += new System.EventHandler(this.toolZoomCb_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.panel1.Controls.Add(this.YearPanel);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.gbRapport);
            this.panel1.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.panel1.Location = new System.Drawing.Point(186, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(190, 584);
            this.panel1.TabIndex = 29;
            // 
            // YearPanel
            // 
            this.YearPanel.Controls.Add(this.label3);
            this.YearPanel.Controls.Add(this.cbAnnee);
            this.YearPanel.Location = new System.Drawing.Point(5, 357);
            this.YearPanel.Name = "YearPanel";
            this.YearPanel.Size = new System.Drawing.Size(180, 48);
            this.YearPanel.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label3.Location = new System.Drawing.Point(3, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Année";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(3, 19);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(174, 21);
            this.cbAnnee.TabIndex = 1;
            this.cbAnnee.ValueMember = "ANN_ID";
            this.cbAnnee.SelectedIndexChanged += new System.EventHandler(this.cbAnnee_SelectedIndexChanged_1);
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labProducteur);
            this.panel3.Controls.Add(this.chkAllProd);
            this.panel3.Controls.Add(this.cbProducteur);
            this.panel3.Location = new System.Drawing.Point(5, 411);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(180, 61);
            this.panel3.TabIndex = 25;
            // 
            // labProducteur
            // 
            this.labProducteur.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labProducteur.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.labProducteur.Location = new System.Drawing.Point(3, 3);
            this.labProducteur.Margin = new System.Windows.Forms.Padding(0);
            this.labProducteur.Name = "labProducteur";
            this.labProducteur.Size = new System.Drawing.Size(174, 16);
            this.labProducteur.TabIndex = 7;
            this.labProducteur.Text = "Producteur";
            this.labProducteur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labProducteur.Visible = false;
            // 
            // chkAllProd
            // 
            this.chkAllProd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkAllProd.Checked = true;
            this.chkAllProd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllProd.Location = new System.Drawing.Point(3, 19);
            this.chkAllProd.Name = "chkAllProd";
            this.chkAllProd.Size = new System.Drawing.Size(51, 17);
            this.chkAllProd.TabIndex = 8;
            this.chkAllProd.Text = "Tous";
            this.chkAllProd.UseVisualStyleBackColor = true;
            this.chkAllProd.Visible = false;
            this.chkAllProd.CheckedChanged += new System.EventHandler(this.cbAllProd_CheckedChanged);
            // 
            // cbProducteur
            // 
            this.cbProducteur.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbProducteur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbProducteur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbProducteur.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRONOMCOMPLETBindingSource, "PRO_ID", true));
            this.cbProducteur.DataSource = this.pRONOMCOMPLETBindingSource;
            this.cbProducteur.DisplayMember = "NOMCOMPLET";
            this.cbProducteur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProducteur.Enabled = false;
            this.cbProducteur.FormattingEnabled = true;
            this.cbProducteur.Location = new System.Drawing.Point(3, 37);
            this.cbProducteur.Name = "cbProducteur";
            this.cbProducteur.Size = new System.Drawing.Size(174, 21);
            this.cbProducteur.TabIndex = 2;
            this.cbProducteur.ValueMember = "PRO_ID";
            this.cbProducteur.Visible = false;
            this.cbProducteur.SelectedIndexChanged += new System.EventHandler(this.cbProducteur_SelectedIndexChanged);
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labDate);
            this.panel2.Controls.Add(this.btnFilterDate);
            this.panel2.Controls.Add(this.datePickerFrom);
            this.panel2.Controls.Add(this.labTo);
            this.panel2.Controls.Add(this.labFrom);
            this.panel2.Controls.Add(this.datePickerTo);
            this.panel2.Location = new System.Drawing.Point(5, 478);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(180, 101);
            this.panel2.TabIndex = 24;
            // 
            // labDate
            // 
            this.labDate.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labDate.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.labDate.Location = new System.Drawing.Point(3, 4);
            this.labDate.Margin = new System.Windows.Forms.Padding(0);
            this.labDate.Name = "labDate";
            this.labDate.Size = new System.Drawing.Size(174, 16);
            this.labDate.TabIndex = 20;
            this.labDate.Text = "Date";
            this.labDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labDate.Visible = false;
            // 
            // btnFilterDate
            // 
            this.btnFilterDate.Location = new System.Drawing.Point(76, 74);
            this.btnFilterDate.Name = "btnFilterDate";
            this.btnFilterDate.Size = new System.Drawing.Size(101, 23);
            this.btnFilterDate.TabIndex = 5;
            this.btnFilterDate.Text = "Filtrer par date";
            this.btnFilterDate.UseVisualStyleBackColor = true;
            this.btnFilterDate.Visible = false;
            this.btnFilterDate.Click += new System.EventHandler(this.btnFilterDate_Click);
            // 
            // datePickerFrom
            // 
            this.datePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePickerFrom.Location = new System.Drawing.Point(32, 23);
            this.datePickerFrom.Name = "datePickerFrom";
            this.datePickerFrom.Size = new System.Drawing.Size(145, 20);
            this.datePickerFrom.TabIndex = 3;
            this.datePickerFrom.Visible = false;
            this.datePickerFrom.Validated += new System.EventHandler(this.datePickerFrom_Validated);
            // 
            // labTo
            // 
            this.labTo.AutoSize = true;
            this.labTo.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labTo.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.labTo.Location = new System.Drawing.Point(3, 49);
            this.labTo.Margin = new System.Windows.Forms.Padding(0);
            this.labTo.Name = "labTo";
            this.labTo.Size = new System.Drawing.Size(20, 15);
            this.labTo.TabIndex = 23;
            this.labTo.Text = "au";
            this.labTo.Visible = false;
            // 
            // labFrom
            // 
            this.labFrom.AutoSize = true;
            this.labFrom.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.labFrom.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.labFrom.Location = new System.Drawing.Point(3, 23);
            this.labFrom.Margin = new System.Windows.Forms.Padding(0);
            this.labFrom.Name = "labFrom";
            this.labFrom.Size = new System.Drawing.Size(21, 15);
            this.labFrom.TabIndex = 21;
            this.labFrom.Text = "du";
            this.labFrom.Visible = false;
            // 
            // datePickerTo
            // 
            this.datePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePickerTo.Location = new System.Drawing.Point(32, 49);
            this.datePickerTo.Name = "datePickerTo";
            this.datePickerTo.Size = new System.Drawing.Size(145, 20);
            this.datePickerTo.TabIndex = 4;
            this.datePickerTo.Visible = false;
            this.datePickerTo.Validated += new System.EventHandler(this.datePickerTo_Validated);
            // 
            // gbRapport
            // 
            this.gbRapport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRapport.Controls.Add(this.optAttestationsFiscales);
            this.gbRapport.Controls.Add(this.optApportsProducteurMout);
            this.gbRapport.Controls.Add(this.optApportsProducteur);
            this.gbRapport.Controls.Add(this.optAttestationControleAvecSoldeParAcquit);
            this.gbRapport.Controls.Add(this.optAttestationControleAvecSolde);
            this.gbRapport.Controls.Add(this.optListeParcelles);
            this.gbRapport.Controls.Add(this.optBonusDetail);
            this.gbRapport.Controls.Add(this.optPaiements);
            this.gbRapport.Controls.Add(this.optTablesPrix);
            this.gbRapport.Controls.Add(this.optAttestationControle);
            this.gbRapport.Controls.Add(this.optDroitsProduction);
            this.gbRapport.Controls.Add(this.optDeclarationEncavage);
            this.gbRapport.Controls.Add(this.optListeAcquit);
            this.gbRapport.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gbRapport.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.gbRapport.Location = new System.Drawing.Point(5, 3);
            this.gbRapport.Name = "gbRapport";
            this.gbRapport.Size = new System.Drawing.Size(181, 400);
            this.gbRapport.TabIndex = 0;
            this.gbRapport.TabStop = false;
            this.gbRapport.Text = "Rapports";
            // 
            // optAttestationsFiscales
            // 
            this.optAttestationsFiscales.AutoSize = true;
            this.optAttestationsFiscales.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optAttestationsFiscales.Location = new System.Drawing.Point(6, 328);
            this.optAttestationsFiscales.Name = "optAttestationsFiscales";
            this.optAttestationsFiscales.Size = new System.Drawing.Size(139, 20);
            this.optAttestationsFiscales.TabIndex = 14;
            this.optAttestationsFiscales.Text = "Attestations fiscales";
            this.optAttestationsFiscales.UseVisualStyleBackColor = true;
            this.optAttestationsFiscales.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optApportsProducteurMout
            // 
            this.optApportsProducteurMout.AutoSize = true;
            this.optApportsProducteurMout.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optApportsProducteurMout.Location = new System.Drawing.Point(24, 302);
            this.optApportsProducteurMout.Name = "optApportsProducteurMout";
            this.optApportsProducteurMout.Size = new System.Drawing.Size(118, 20);
            this.optApportsProducteurMout.TabIndex = 5;
            this.optApportsProducteurMout.Text = "en litre de moût";
            this.optApportsProducteurMout.UseVisualStyleBackColor = true;
            this.optApportsProducteurMout.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optApportsProducteur
            // 
            this.optApportsProducteur.AutoSize = true;
            this.optApportsProducteur.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optApportsProducteur.Location = new System.Drawing.Point(6, 277);
            this.optApportsProducteur.Name = "optApportsProducteur";
            this.optApportsProducteur.Size = new System.Drawing.Size(142, 20);
            this.optApportsProducteur.TabIndex = 4;
            this.optApportsProducteur.Text = "Apports/producteur";
            this.optApportsProducteur.UseVisualStyleBackColor = true;
            this.optApportsProducteur.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optAttestationControleAvecSoldeParAcquit
            // 
            this.optAttestationControleAvecSoldeParAcquit.AutoSize = true;
            this.optAttestationControleAvecSoldeParAcquit.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optAttestationControleAvecSoldeParAcquit.Location = new System.Drawing.Point(24, 125);
            this.optAttestationControleAvecSoldeParAcquit.Name = "optAttestationControleAvecSoldeParAcquit";
            this.optAttestationControleAvecSoldeParAcquit.Size = new System.Drawing.Size(134, 20);
            this.optAttestationControleAvecSoldeParAcquit.TabIndex = 10;
            this.optAttestationControleAvecSoldeParAcquit.Text = "+ solde par acquit";
            this.optAttestationControleAvecSoldeParAcquit.UseVisualStyleBackColor = true;
            this.optAttestationControleAvecSoldeParAcquit.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optAttestationControleAvecSolde
            // 
            this.optAttestationControleAvecSolde.AutoSize = true;
            this.optAttestationControleAvecSolde.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optAttestationControleAvecSolde.Location = new System.Drawing.Point(24, 100);
            this.optAttestationControleAvecSolde.Name = "optAttestationControleAvecSolde";
            this.optAttestationControleAvecSolde.Size = new System.Drawing.Size(71, 20);
            this.optAttestationControleAvecSolde.TabIndex = 9;
            this.optAttestationControleAvecSolde.Text = "+ solde";
            this.optAttestationControleAvecSolde.UseVisualStyleBackColor = true;
            this.optAttestationControleAvecSolde.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optListeParcelles
            // 
            this.optListeParcelles.AutoSize = true;
            this.optListeParcelles.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optListeParcelles.Location = new System.Drawing.Point(6, 252);
            this.optListeParcelles.Name = "optListeParcelles";
            this.optListeParcelles.Size = new System.Drawing.Size(133, 20);
            this.optListeParcelles.TabIndex = 3;
            this.optListeParcelles.Text = "Liste des parcelles";
            this.optListeParcelles.UseVisualStyleBackColor = true;
            this.optListeParcelles.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optBonusDetail
            // 
            this.optBonusDetail.AutoSize = true;
            this.optBonusDetail.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optBonusDetail.Location = new System.Drawing.Point(6, 202);
            this.optBonusDetail.Name = "optBonusDetail";
            this.optBonusDetail.Size = new System.Drawing.Size(124, 20);
            this.optBonusDetail.TabIndex = 1;
            this.optBonusDetail.Text = "Paiements bonus";
            this.optBonusDetail.UseVisualStyleBackColor = true;
            this.optBonusDetail.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optPaiements
            // 
            this.optPaiements.AutoSize = true;
            this.optPaiements.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optPaiements.Location = new System.Drawing.Point(6, 176);
            this.optPaiements.Name = "optPaiements";
            this.optPaiements.Size = new System.Drawing.Size(153, 20);
            this.optPaiements.TabIndex = 0;
            this.optPaiements.Text = "Paiements fournisseur";
            this.optPaiements.UseVisualStyleBackColor = true;
            this.optPaiements.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optTablesPrix
            // 
            this.optTablesPrix.AutoSize = true;
            this.optTablesPrix.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optTablesPrix.Location = new System.Drawing.Point(6, 227);
            this.optTablesPrix.Name = "optTablesPrix";
            this.optTablesPrix.Size = new System.Drawing.Size(117, 20);
            this.optTablesPrix.TabIndex = 2;
            this.optTablesPrix.Text = "Tables des prix";
            this.optTablesPrix.UseVisualStyleBackColor = true;
            this.optTablesPrix.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optAttestationControle
            // 
            this.optAttestationControle.AutoSize = true;
            this.optAttestationControle.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optAttestationControle.Location = new System.Drawing.Point(6, 75);
            this.optAttestationControle.Name = "optAttestationControle";
            this.optAttestationControle.Size = new System.Drawing.Size(143, 20);
            this.optAttestationControle.TabIndex = 8;
            this.optAttestationControle.Text = "Attestations contrôle";
            this.optAttestationControle.UseVisualStyleBackColor = true;
            this.optAttestationControle.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optDroitsProduction
            // 
            this.optDroitsProduction.AutoSize = true;
            this.optDroitsProduction.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optDroitsProduction.Location = new System.Drawing.Point(6, 50);
            this.optDroitsProduction.Name = "optDroitsProduction";
            this.optDroitsProduction.Size = new System.Drawing.Size(145, 20);
            this.optDroitsProduction.TabIndex = 7;
            this.optDroitsProduction.Text = "Droits de production";
            this.optDroitsProduction.UseVisualStyleBackColor = true;
            this.optDroitsProduction.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optDeclarationEncavage
            // 
            this.optDeclarationEncavage.AutoSize = true;
            this.optDeclarationEncavage.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optDeclarationEncavage.Location = new System.Drawing.Point(6, 150);
            this.optDeclarationEncavage.Name = "optDeclarationEncavage";
            this.optDeclarationEncavage.Size = new System.Drawing.Size(160, 20);
            this.optDeclarationEncavage.TabIndex = 11;
            this.optDeclarationEncavage.Text = "Déclaration d\'encavage";
            this.optDeclarationEncavage.UseVisualStyleBackColor = true;
            this.optDeclarationEncavage.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // optListeAcquit
            // 
            this.optListeAcquit.AutoSize = true;
            this.optListeAcquit.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F);
            this.optListeAcquit.Location = new System.Drawing.Point(6, 25);
            this.optListeAcquit.Name = "optListeAcquit";
            this.optListeAcquit.Size = new System.Drawing.Size(122, 20);
            this.optListeAcquit.TabIndex = 6;
            this.optListeAcquit.Text = "Liste des acquits";
            this.optListeAcquit.UseVisualStyleBackColor = true;
            this.optListeAcquit.CheckedChanged += new System.EventHandler(this.opt_CheckedChanged);
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.PageCountMode = Microsoft.Reporting.WinForms.PageCountMode.Actual;
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowExportButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowPageNavigationControls = false;
            this.reportViewer1.ShowParameterPrompts = false;
            this.reportViewer1.ShowPrintButton = false;
            this.reportViewer1.ShowPromptAreaButton = false;
            this.reportViewer1.ShowRefreshButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.ShowToolBar = false;
            this.reportViewer1.ShowZoomControl = false;
            this.reportViewer1.Size = new System.Drawing.Size(543, 661);
            this.reportViewer1.TabIndex = 0;
            this.reportViewer1.RenderingComplete += new Microsoft.Reporting.WinForms.RenderingCompleteEventHandler(this.reportViewer1_RenderingComplete);
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // CtrlReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "CtrlReportViewer";
            this.Size = new System.Drawing.Size(543, 661);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.YearPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbRapport.ResumeLayout(false);
            this.gbRapport.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolExporter;
        private System.Windows.Forms.ToolStripMenuItem portableDocumentFormatPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem microsoftWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem microsoftExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tooPrint;
        private System.Windows.Forms.ToolStripButton toolFirst;
        private System.Windows.Forms.ToolStripButton toolBefore;
        private System.Windows.Forms.ToolStripTextBox toolPage;
        private System.Windows.Forms.ToolStripLabel toolLabelPage;
        private System.Windows.Forms.ToolStripButton toolNext;
        private System.Windows.Forms.ToolStripButton toolLast;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolLargeurPage;
        private System.Windows.Forms.ToolStripButton toolRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gbRapport;
        private System.Windows.Forms.RadioButton optListeAcquit;
        private System.Windows.Forms.RadioButton optDroitsProduction;
        private System.Windows.Forms.RadioButton optDeclarationEncavage;
        private System.Windows.Forms.RadioButton optAttestationControle;
        private System.Windows.Forms.RadioButton optTablesPrix;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.ToolStripComboBox toolZoomCb;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Label labProducteur;
        private System.Windows.Forms.CheckBox chkAllProd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.ComboBox cbProducteur;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.RadioButton optPaiements;
        private System.Windows.Forms.RadioButton optBonusDetail;
        private System.Windows.Forms.Label labTo;
        private System.Windows.Forms.DateTimePicker datePickerTo;
        private System.Windows.Forms.Label labFrom;
        private System.Windows.Forms.Label labDate;
        private System.Windows.Forms.DateTimePicker datePickerFrom;
        private System.Windows.Forms.Button btnFilterDate;
        private System.Windows.Forms.RadioButton optListeParcelles;
        private System.Windows.Forms.RadioButton optAttestationControleAvecSoldeParAcquit;
        private System.Windows.Forms.RadioButton optAttestationControleAvecSolde;
        private System.Windows.Forms.RadioButton optApportsProducteur;
        private System.Windows.Forms.RadioButton optApportsProducteurMout;
        private System.Windows.Forms.Panel YearPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton optAttestationsFiscales;
    }
}