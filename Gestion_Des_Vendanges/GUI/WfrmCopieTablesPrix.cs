﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmCopieTablesPrix : WfrmBase
    {
        private int _idSelection;

        public WfrmCopieTablesPrix(int idSelection)
        {
            InitializeComponent();
            _idSelection = idSelection;
        }

        private void WfrmCopieBonus_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterDest);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            FillOnLoad();
            cmbSource.SelectedValue = -1;
            cmbSource.SelectedValue = _idSelection;
        }

        private void lIE_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int id = (int)cmbSource.SelectedValue;
                fillVal(id);
                foreach (DataGridViewRow row in vAL_VALEUR_CEPAGEDataGridView.Rows)
                {
                    row.Cells["SelectToCopy"].Value = true;
                }
            }
            catch { }
        }

        private void btnCopierLie_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> rowsToInsert = new List<DataGridViewRow>();
            List<DataGridViewRow> rowsToUpdate = new List<DataGridViewRow>();
            try
            {
                int idDestination = (int)cmbDestination.SelectedValue;
                foreach (DataGridViewRow row in vAL_VALEUR_CEPAGEDataGridView.Rows)
                {
                    bool isSelected;
                    if (row.Cells["SelectToCopy"].Value == null)
                    {
                        isSelected = false;
                    }
                    else
                    {
                        isSelected = (bool)row.Cells["SelectToCopy"].Value;
                    }
                    if (isSelected)
                    {
                        int id = (int)row.Cells[_columnName].Value;
                        if (GetNbVal(id, idDestination) > 0)
                            rowsToUpdate.Add(row);
                        else
                            rowsToInsert.Add(row);
                    }
                }
                bool copie = false;
                if (rowsToUpdate.Count > 0)
                {
                    DialogResult result = MessageBox.Show("Il existe déjà des tables de prix pour cette destination.\n" +
                        "Voulez-vous les remplacer?", "Données existentes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                    switch (result)
                    {
                        case DialogResult.Yes:
                            updateRow(rowsToUpdate, idDestination);
                            insertRow(rowsToInsert, idDestination);
                            copie = true;
                            break;

                        case DialogResult.No:
                            insertRow(rowsToInsert, idDestination);
                            copie = true;
                            break;

                        default:
                            break;
                    }
                }
                else if (rowsToInsert.Count == 0)
                {
                    MessageBox.Show("Aucun élément n'est sélectionné. Veuillez les choisir dans la liste.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    insertRow(rowsToInsert, idDestination);
                    copie = true;
                }
                if (copie)
                {
                    FillByBonusId();
                    MessageBox.Show("Les tables de prix ont été copiées avec succès.", "Copie terminée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (NullReferenceException)
            {
            }
        }

        private void updateRow(List<DataGridViewRow> rowsToUpdate, int idDestination)
        {
            foreach (DataGridViewRow row in rowsToUpdate)
            {
                int id = (int)row.Cells[_columnName].Value;
                int valIdSource = (int)row.Cells["VAL_ID"].Value;
                int valIdDestination = GetValId(id, idDestination);
                double valeur = (double)row.Cells["VAL_VALEUR"].Value;
                vAL_VALEUR_CEPAGETableAdapter.UpdateValeurByValId(valeur, valIdDestination);
                updateBonus(valIdSource, valIdDestination);
            }
        }

        private void insertRow(List<DataGridViewRow> rowsToInsert, int idDestination)
        {
            foreach (DataGridViewRow row in rowsToInsert)
            {
                int id = (int)row.Cells[_columnName].Value;
                int annId = (int)row.Cells["ANN_ID"].Value;
                double valeur = (double)row.Cells["VAL_VALEUR"].Value;
                int valIdSource = (int)row.Cells["VAL_ID"].Value;
                InsertVal(id, idDestination, annId, valeur);
                int valIdDestination = (int)vAL_VALEUR_CEPAGETableAdapter.GetMaxValId();
                updateBonus(valIdSource, valIdDestination);
            }
        }

        private void updateBonus(int valIdSource, int valIdDestination)
        {
            var bonusTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<BON_BONUSTableAdapter>();
            bonusTableAdapter.DeleteBonusByValId(valIdDestination);
            bonusTableAdapter.FillByVal(dSPesees.BON_BONUS, valIdSource);
            foreach (DAO.DSPesees.BON_BONUSRow row in bonusTableAdapter.GetDataByVal(valIdSource))
            {
                bonusTableAdapter.Insert(valIdDestination, row.BON_OE, row.BON_BRIX, row.BON_BONUS);
            }
        }

        private void vAL_VALEUR_CEPAGEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == vAL_VALEUR_CEPAGEDataGridView.Columns["SelectToCopy"].Index)
            {
                bool allSelect = true;
                vAL_VALEUR_CEPAGEDataGridView.CurrentCell = vAL_VALEUR_CEPAGEDataGridView.CurrentRow.Cells[_columnName];
                for (int i = 0; i < vAL_VALEUR_CEPAGEDataGridView.Rows.Count && allSelect; i++)
                {
                    if (vAL_VALEUR_CEPAGEDataGridView.Rows[i].Cells["SelectToCopy"].Value == null)
                    {
                        allSelect = false;
                    }
                    else
                    {
                        allSelect = (bool)vAL_VALEUR_CEPAGEDataGridView.Rows[i].Cells["SelectToCopy"].Value;
                    }
                }
                foreach (DataGridViewRow row in vAL_VALEUR_CEPAGEDataGridView.Rows)
                {
                    row.Cells["SelectToCopy"].Value = !allSelect;
                }
            }
            else
            {
                if (e.ColumnIndex == vAL_VALEUR_CEPAGEDataGridView.Columns[_columnName].Index)
                {
                    try
                    {
                        int id = (int)cmbSource.SelectedValue;
                        fillValSorted(id);
                        vAL_VALEUR_CEPAGEDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                    }
                    catch { }
                }
            }
        }
    }
}