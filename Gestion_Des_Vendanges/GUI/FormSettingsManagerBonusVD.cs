﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class FormSettingsManagerBonus
    {
        private LIE_LIEU_DE_PRODUCTIONTableAdapter _filtreLieTableAdapter;

        public FormSettingsManagerBonus(ICtrlSetting form,
                                        TextBox idTextBox,
                                        Control focusControl,
                                        ToolStripButton btnNew,
                                        ToolStripButton btnUpdate,
                                        ToolStripButton btnDelete,
                                        Button btnOK,
                                        Button btnCancel,
                                        DataGridView dataGridView,
                                        BindingSource bindingSource,
                                        TableAdapterManager tableAdapterManager,
                                        DSPesees dsPesees,
                                        ComboBox cbCepageId,
                                        ComboBox cbLieuProductionId,
                                        TextBox txtAnnId,
                                        int annId,
                                        VAL_VALEUR_CEPAGETableAdapter valTableAdapter,
                                        DataGridView bonusDataGridView,
                                        BindingSource bonusBindingSource,
                                        BON_BONUSTableAdapter bonusTableAdapter,
                                        ViewBonusMaxTableAdapter viewBonusMaxTableAdapter,
                                        ListBox cbFiltreLieuDeProduction,
                                        LIE_LIEU_DE_PRODUCTIONTableAdapter filtreLieTableAdapter,
                                        DSPesees dsPeseesFiltre,
                                        ComboBox cbAnnee,
                                        ToolStripButton btnCopyLieuProduction,
                                        ToolStripButton btnCopyCepage,
                                        Label lprix100)
            : base(form,
                   idTextBox,
                   focusControl,
                   btnNew,
                   btnUpdate,
                   btnDelete,
                   btnOK,
                   btnCancel,
                   dataGridView,
                   bindingSource,
                   tableAdapterManager,
                   dsPesees)
        {
            _cbFiltre = cbFiltreLieuDeProduction;
            _cbFiltre.SelectedIndexChanged += LIE_IDComboBox_SelectedIndexChanged;
            _cbFiltre.EnabledChanged += CbFiltreLieuDeProductionEnbledChanged;
            _filtreLieTableAdapter = filtreLieTableAdapter;
            ConnectionManager.Instance.AddGvTableAdapter(_filtreLieTableAdapter);

            Init(dataGridView,
                 dsPesees,
                 cbCepageId,
                 cbLieuProductionId,
                 txtAnnId,
                 annId,
                 valTableAdapter,
                 bonusDataGridView,
                 bonusBindingSource,
                 bonusTableAdapter,
                 viewBonusMaxTableAdapter,
                 dsPeseesFiltre,
                 cbAnnee,
                 btnCopyLieuProduction,
                 btnCopyCepage,
                 lprix100);
        }

        private void BtnCopyLieuProductionClick(object sender, EventArgs e)
        {
            int lieId;
            try
            {
                lieId = (int)_cbFiltre.SelectedValue;
            }
            catch (NullReferenceException)
            {
                lieId = -1;
            }
            new WfrmCopieTablesPrix(lieId).ShowDialog();
            _filtreLieTableAdapter.FillByValAnn(_dsPeseesFiltre.LIE_LIEU_DE_PRODUCTION, _annId);
            _bonusTableAdapter.Fill(_dsPesees.BON_BONUS);
            _viewBonusMaxTableAdapter.Fill(_dsPesees.ViewBonusMax);
        }

        private void CbFiltreLieuDeProductionEnbledChanged(object sender, EventArgs e)
        {
            _cbAnnee.Enabled = _cbFiltre.Enabled;
        }

        protected override void BtnUpdate_Click(object sender, EventArgs e)
        {
            _bonusDataGridView.AllowUserToAddRows = true;
            base.BtnUpdate_Click(sender, e);
            _cbLieuProductionId.Enabled = false;
            _cbCepageId.Enabled = false;
            _cbFiltre.Enabled = false;
        }

        private void LIE_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int lieId = (int)_cbFiltre.SelectedValue;
                _valTableAdapter.FillByLieAnn(_dsPesees.VAL_VALEUR_CEPAGE, lieId, Utilities.IdAnneeCourante);
            }
            catch (Exception) { }
        }

        private void UpdateDataGrid()
        {
            _cbAnnee.SelectedValue = _annId;
            _filtreLieTableAdapter.FillByValAnn(_dsPeseesFiltre.LIE_LIEU_DE_PRODUCTION, _annId);

            if (_cbFiltre.Items.Count > 0)
                _valTableAdapter.FillByLieAnn(_dsPesees.VAL_VALEUR_CEPAGE, (int)_cbFiltre.SelectedValue, _annId);
            else
                _valTableAdapter.FillByLieAnn(_dsPesees.VAL_VALEUR_CEPAGE, -1, _annId);

            _txtAnnId.Text = _annId.ToString();
        }

        private void BtnCopyCepageClick(object sender, EventArgs e)
        {
            try
            {
                int valId = Convert.ToInt32(DataGridView.SelectedRows[0].Cells["DGVAL_ID"].Value);
                int cepId = Convert.ToInt32(DataGridView.SelectedRows[0].Cells["CEP_ID"].Value);

                WfrmSelectionCepage wfrmSelectCepage = new WfrmSelectionCepage();
                if (wfrmSelectCepage.ShowDialog() == DialogResult.OK)
                {
                    int cepDestId = wfrmSelectCepage.CepId;
                    if (cepDestId > 0 && valId > 0 && cepDestId != cepId)
                    {
                        bool ok = true;
                        int? valIdToDelete = (int?)_valTableAdapter.GetValByCepVal(valId, cepDestId);
                        if (valIdToDelete != null)
                        {
                            if (MessageBox.Show("Une table de prix pour ce cépage existe déjà. Voulez-vous la remplacer?",
                                                "Table de prix",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                _bonusTableAdapter.DeleteBonusByValId((int)valIdToDelete);
                                _valTableAdapter.DeleteByValId((int)valIdToDelete);
                            }
                            else
                            {
                                ok = false;
                            }
                        }
                        if (ok)
                        {
                            _valTableAdapter.CopyCepByValId(cepDestId, valId);
                            int newValId = (int)_valTableAdapter.GetMaxValId();
                            _bonusTableAdapter.CopyBonusByValId(valId, newValId);

                            int lieId = (int)_cbFiltre.SelectedValue;
                            _valTableAdapter.FillByLieAnn(_dsPesees.VAL_VALEUR_CEPAGE, lieId, Utilities.IdAnneeCourante);
                            _bonusTableAdapter.Fill(_dsPesees.BON_BONUS);
                            _viewBonusMaxTableAdapter.Fill(_dsPesees.ViewBonusMax);
                        }
                    }
                }
            }
            catch (NullReferenceException) { }
            catch (InvalidCastException) { }
            catch (ArgumentOutOfRangeException) { }
        }

        private void UpdateFilter(int cepId, int lieId)
        {
            if (Convert.ToInt32(_cbFiltre.SelectedValue) != lieId)
            {
                UpdateDataGrid();
                _cbFiltre.SelectedValue = lieId;
            }
        }

        private void BonusDataGridViewBeginEdit(object sender, EventArgs e)
        {
            _isBonusDataGridViewEditMode = true;
            if (IsNew)
            {
                try
                {
                    int cepId = (int)_cbCepageId.SelectedValue;
                    int lieId = (int)_cbLieuProductionId.SelectedValue;
                    if (_valTableAdapter.GetNbByCepLieAnn(cepId, lieId, _annId) == 0)
                    {
                        SaveData();
                        int valId = (int)_valDataGridView.CurrentRow.Cells["DGVAL_ID"].Value;
                        _filtreLieTableAdapter.FillByValAnn(_dsPeseesFiltre.LIE_LIEU_DE_PRODUCTION, _annId);
                        _cbFiltre.SelectedValue = _cbLieuProductionId.SelectedValue;
                        LIE_IDComboBox_SelectedIndexChanged(null, null);
                        SetIsNew(false);
                        bool trouve = false;
                        int index;
                        for (index = 0; index < _valDataGridView.Rows.Count && !trouve; index++)
                        {
                            if ((int)_valDataGridView.Rows[index].Cells["DGVAL_ID"].Value == valId)
                            {
                                trouve = true;
                                index--;
                            }
                        }
                        try
                        {
                            _valDataGridView.CurrentCell = _valDataGridView.Rows[index].Cells["CEP_ID"];
                        }
                        catch (InvalidOperationException) { }

                        _bonusDataGridView.CurrentCell = _bonusDataGridView.Rows[0].Cells[0];
                    }
                    else
                    {
                        _bonusDataGridView.Enabled = false;
                        _bonusDataGridView.Enabled = true;
                        _bonusBindingSource.RemoveCurrent();
                        _isBonusDataGridViewEditMode = false;
                        MessageBox.Show("Les bonus pour ce cépage et ce lieu de production existent déjà!\n" +
                                        "Veuillez choisir un autre cépage ou lieu de production.",
                                        "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                catch (NullReferenceException)
                {
                    _bonusDataGridView.CancelEdit();
                    MessageBox.Show("Veuillez saisir un lieu de production et un cépage.",
                                    "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                //bonusDataGridView.BeginEdit(false);
            }
        }

        protected override void BtnNew_Click(object sender, EventArgs e)
        {
            _bonusDataGridView.AllowUserToAddRows = true;
            base.BtnNew_Click(sender, e);
            _txtAnnId.Text = _annId.ToString();
            _cbFiltre.Enabled = false;
            try
            {
                _cbLieuProductionId.SelectedValue = _cbFiltre.SelectedValue;
            }
            catch (ArgumentNullException) { }
        }
    }
}