﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbAnnId = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.optAcompte = new System.Windows.Forms.RadioButton();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.optFinal = new System.Windows.Forms.RadioButton();
            this.gBType = new System.Windows.Forms.GroupBox();
            this.txtNumeroPmt = new System.Windows.Forms.TextBox();
            this.txtTitre = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnSuivant = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.ExerciceComptableComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.gBType.SuspendLayout();
            this.SuspendLayout();
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(23, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Titre du paiement :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label3.Location = new System.Drawing.Point(23, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Date de la facture :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label4.Location = new System.Drawing.Point(23, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "Numéro de paiement :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label1.Location = new System.Drawing.Point(23, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Année de vendange:";
            // 
            // cbAnnId
            // 
            this.cbAnnId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnId.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnId.DisplayMember = "ANN_AN";
            this.cbAnnId.FormattingEnabled = true;
            this.cbAnnId.Location = new System.Drawing.Point(272, 61);
            this.cbAnnId.Name = "cbAnnId";
            this.cbAnnId.Size = new System.Drawing.Size(185, 21);
            this.cbAnnId.TabIndex = 0;
            this.cbAnnId.ValueMember = "ANN_ID";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(212, 165);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(196, 23);
            this.dateTimePicker2.TabIndex = 4;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(212, 128);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(196, 23);
            this.textBox3.TabIndex = 3;
            // 
            // comboBox2
            // 
            this.comboBox2.DisplayMember = "ANN_AN";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(212, -34);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(196, 23);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.ValueMember = "ANN_ID";
            // 
            // button2
            // 
            this.button2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.button2.Location = new System.Drawing.Point(333, 210);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Suivant";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // optAcompte
            // 
            this.optAcompte.AutoSize = true;
            this.optAcompte.Location = new System.Drawing.Point(25, 31);
            this.optAcompte.Name = "optAcompte";
            this.optAcompte.Size = new System.Drawing.Size(76, 19);
            this.optAcompte.TabIndex = 1;
            this.optAcompte.TabStop = true;
            this.optAcompte.Text = "Acompte";
            this.optAcompte.UseVisualStyleBackColor = true;
            this.optAcompte.CheckedChanged += new System.EventHandler(this.OptAcompte_CheckedChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(212, 89);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(196, 23);
            this.textBox4.TabIndex = 2;
            // 
            // optFinal
            // 
            this.optFinal.AutoSize = true;
            this.optFinal.Location = new System.Drawing.Point(254, 31);
            this.optFinal.Name = "optFinal";
            this.optFinal.Size = new System.Drawing.Size(50, 19);
            this.optFinal.TabIndex = 2;
            this.optFinal.TabStop = true;
            this.optFinal.Text = "Final";
            this.optFinal.UseVisualStyleBackColor = true;
            this.optFinal.CheckedChanged += new System.EventHandler(this.OptFinal_CheckedChanged);
            // 
            // gBType
            // 
            this.gBType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gBType.Controls.Add(this.optFinal);
            this.gBType.Controls.Add(this.textBox4);
            this.gBType.Controls.Add(this.optAcompte);
            this.gBType.Controls.Add(this.button2);
            this.gBType.Controls.Add(this.comboBox2);
            this.gBType.Controls.Add(this.textBox3);
            this.gBType.Controls.Add(this.dateTimePicker2);
            this.gBType.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gBType.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.gBType.Location = new System.Drawing.Point(272, 102);
            this.gBType.Name = "gBType";
            this.gBType.Size = new System.Drawing.Size(365, 69);
            this.gBType.TabIndex = 1;
            this.gBType.TabStop = false;
            this.gBType.Text = "Type de paiement";
            // 
            // txtNumeroPmt
            // 
            this.txtNumeroPmt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumeroPmt.Location = new System.Drawing.Point(272, 193);
            this.txtNumeroPmt.Name = "txtNumeroPmt";
            this.txtNumeroPmt.Size = new System.Drawing.Size(365, 20);
            this.txtNumeroPmt.TabIndex = 2;
            // 
            // txtTitre
            // 
            this.txtTitre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTitre.Location = new System.Drawing.Point(272, 232);
            this.txtTitre.Name = "txtTitre";
            this.txtTitre.Size = new System.Drawing.Size(365, 20);
            this.txtTitre.TabIndex = 3;
            // 
            // dtpDate
            // 
            this.dtpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpDate.CustomFormat = "dd.MM.yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(272, 301);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(185, 20);
            this.dtpDate.TabIndex = 5;
            // 
            // btnSuivant
            // 
            this.btnSuivant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuivant.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSuivant.Location = new System.Drawing.Point(562, 397);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(75, 30);
            this.btnSuivant.TabIndex = 6;
            this.btnSuivant.Text = "Suivant";
            this.btnSuivant.UseVisualStyleBackColor = false;
            this.btnSuivant.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label5.Location = new System.Drawing.Point(22, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Nouveaux paiements";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(643, 397);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 7;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.BtnAnnuler_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label7.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label7.Location = new System.Drawing.Point(23, 271);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Exercice comptable :";
            // 
            // ExerciceComptable
            // 
            this.ExerciceComptableComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExerciceComptableComboBox.DisplayMember = "ANN_ID";
            this.ExerciceComptableComboBox.FormattingEnabled = true;
            this.ExerciceComptableComboBox.Location = new System.Drawing.Point(272, 267);
            this.ExerciceComptableComboBox.Name = "ExerciceComptable";
            this.ExerciceComptableComboBox.Size = new System.Drawing.Size(185, 21);
            this.ExerciceComptableComboBox.TabIndex = 15;
            this.ExerciceComptableComboBox.ValueMember = "ANN_ID";
            this.ExerciceComptableComboBox.SelectedIndexChanged += new System.EventHandler(this.CbExerciceWinBIZ_SelectedIndexChanged);
            // 
            // WfrmAjoutPaiement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 436);
            this.Controls.Add(this.ExerciceComptableComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNumeroPmt);
            this.Controls.Add(this.btnSuivant);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gBType);
            this.Controls.Add(this.txtTitre);
            this.Controls.Add(this.cbAnnId);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(746, 475);
            this.MinimumSize = new System.Drawing.Size(746, 475);
            this.Name = "WfrmAjoutPaiement";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Gestion des paiements - Nouveaux";
            this.Load += new System.EventHandler(this.WfrmAjoutPaiement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.gBType.ResumeLayout(false);
            this.gBType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAnnId;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton optAcompte;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.RadioButton optFinal;
        private System.Windows.Forms.GroupBox gBType;
        private System.Windows.Forms.TextBox txtNumeroPmt;
        private System.Windows.Forms.TextBox txtTitre;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ExerciceComptableComboBox;
    }
}