﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesArticles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cOU_IDLabel;
            System.Windows.Forms.Label aNN_IDLabel;
            System.Windows.Forms.Label aRT_LITRESLabel;
            System.Windows.Forms.Label aRT_DESIGNATION_COURTELabel;
            System.Windows.Forms.Label aRT_DESIGNATION_LONGUELabel;
            System.Windows.Forms.Label aRT_CODELabel;
            System.Windows.Forms.Label aRT_CODE_GROUPELabel;
            System.Windows.Forms.Label mOC_IDLabel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aRT_ARTICLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aRT_ARTICLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ART_ARTICLETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cLACLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.splitContainer1 = new Gestion_Des_Vendanges.GUI.GVSplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.aRT_ARTICLEDataGridView = new System.Windows.Forms.DataGridView();
            this.ART_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ART_ID_WINBIZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COU_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SelectArticle = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ART_VERSION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLitreStock = new System.Windows.Forms.TextBox();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cOU_IDComboBox = new System.Windows.Forms.ComboBox();
            this.aNN_IDComboBox = new System.Windows.Forms.ComboBox();
            this.aRT_LITRESTextBox = new System.Windows.Forms.TextBox();
            this.aRT_DESIGNATION_COURTETextBox = new System.Windows.Forms.TextBox();
            this.aRT_DESIGNATION_LONGUETextBox = new System.Windows.Forms.TextBox();
            this.aRT_CODETextBox = new System.Windows.Forms.TextBox();
            this.aRT_CODE_GROUPETextBox = new System.Windows.Forms.TextBox();
            this.aRT_IDTextBox = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolGenerer = new System.Windows.Forms.ToolStripButton();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBtnGrouper = new System.Windows.Forms.ToolStripButton();
            this.toolBtnDissocier = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolExportWinBIZ = new System.Windows.Forms.ToolStripButton();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolOk = new System.Windows.Forms.ToolStripButton();
            this.toolCancel = new System.Windows.Forms.ToolStripButton();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            cOU_IDLabel = new System.Windows.Forms.Label();
            aNN_IDLabel = new System.Windows.Forms.Label();
            aRT_LITRESLabel = new System.Windows.Forms.Label();
            aRT_DESIGNATION_COURTELabel = new System.Windows.Forms.Label();
            aRT_DESIGNATION_LONGUELabel = new System.Windows.Forms.Label();
            aRT_CODELabel = new System.Windows.Forms.Label();
            aRT_CODE_GROUPELabel = new System.Windows.Forms.Label();
            mOC_IDLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRT_ARTICLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aRT_ARTICLEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cOU_IDLabel
            // 
            cOU_IDLabel.AutoSize = true;
            cOU_IDLabel.Location = new System.Drawing.Point(4, 132);
            cOU_IDLabel.Name = "cOU_IDLabel";
            cOU_IDLabel.Size = new System.Drawing.Size(49, 13);
            cOU_IDLabel.TabIndex = 49;
            cOU_IDLabel.Text = "Couleur :";
            // 
            // aNN_IDLabel
            // 
            aNN_IDLabel.AutoSize = true;
            aNN_IDLabel.Location = new System.Drawing.Point(3, 56);
            aNN_IDLabel.Name = "aNN_IDLabel";
            aNN_IDLabel.Size = new System.Drawing.Size(44, 13);
            aNN_IDLabel.TabIndex = 37;
            aNN_IDLabel.Text = "Année :";
            // 
            // aRT_LITRESLabel
            // 
            aRT_LITRESLabel.AutoSize = true;
            aRT_LITRESLabel.Location = new System.Drawing.Point(3, 307);
            aRT_LITRESLabel.Name = "aRT_LITRESLabel";
            aRT_LITRESLabel.Size = new System.Drawing.Size(78, 13);
            aRT_LITRESLabel.TabIndex = 39;
            aRT_LITRESLabel.Text = "Pesées (litres) :";
            // 
            // aRT_DESIGNATION_COURTELabel
            // 
            aRT_DESIGNATION_COURTELabel.AutoSize = true;
            aRT_DESIGNATION_COURTELabel.Location = new System.Drawing.Point(3, 189);
            aRT_DESIGNATION_COURTELabel.Name = "aRT_DESIGNATION_COURTELabel";
            aRT_DESIGNATION_COURTELabel.Size = new System.Drawing.Size(102, 13);
            aRT_DESIGNATION_COURTELabel.TabIndex = 41;
            aRT_DESIGNATION_COURTELabel.Text = "Désignation courte :";
            // 
            // aRT_DESIGNATION_LONGUELabel
            // 
            aRT_DESIGNATION_LONGUELabel.AutoSize = true;
            aRT_DESIGNATION_LONGUELabel.Location = new System.Drawing.Point(3, 218);
            aRT_DESIGNATION_LONGUELabel.Name = "aRT_DESIGNATION_LONGUELabel";
            aRT_DESIGNATION_LONGUELabel.Size = new System.Drawing.Size(104, 13);
            aRT_DESIGNATION_LONGUELabel.TabIndex = 43;
            aRT_DESIGNATION_LONGUELabel.Text = "Désignation longue :";
            // 
            // aRT_CODELabel
            // 
            aRT_CODELabel.AutoSize = true;
            aRT_CODELabel.Location = new System.Drawing.Point(3, 81);
            aRT_CODELabel.Name = "aRT_CODELabel";
            aRT_CODELabel.Size = new System.Drawing.Size(69, 13);
            aRT_CODELabel.TabIndex = 45;
            aRT_CODELabel.Text = "Code article :";
            // 
            // aRT_CODE_GROUPELabel
            // 
            aRT_CODE_GROUPELabel.AutoSize = true;
            aRT_CODE_GROUPELabel.Location = new System.Drawing.Point(3, 106);
            aRT_CODE_GROUPELabel.Name = "aRT_CODE_GROUPELabel";
            aRT_CODE_GROUPELabel.Size = new System.Drawing.Size(74, 13);
            aRT_CODE_GROUPELabel.TabIndex = 47;
            aRT_CODE_GROUPELabel.Text = "Code groupe :";
            // 
            // mOC_IDLabel
            // 
            mOC_IDLabel.AutoSize = true;
            mOC_IDLabel.Location = new System.Drawing.Point(3, 161);
            mOC_IDLabel.Name = "mOC_IDLabel";
            mOC_IDLabel.Size = new System.Drawing.Size(118, 13);
            mOC_IDLabel.TabIndex = 53;
            mOC_IDLabel.Text = "Mode comptabilisation :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(3, 334);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(91, 13);
            label4.TabIndex = 55;
            label4.Text = "Stock réel (litres) :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.Locale = new System.Globalization.CultureInfo("fr-CH");
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aRT_ARTICLEBindingSource
            // 
            this.aRT_ARTICLEBindingSource.DataMember = "ART_ARTICLE";
            this.aRT_ARTICLEBindingSource.DataSource = this.dSPesees;
            // 
            // aRT_ARTICLETableAdapter
            // 
            this.aRT_ARTICLETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = this.aNN_ANNEETableAdapter;
            this.tableAdapterManager.ART_ARTICLETableAdapter = this.aRT_ARTICLETableAdapter;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = this.cOM_COMMUNETableAdapter;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // cLACLASSEBindingSource
            // 
            this.cLACLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLACLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnCancel.Location = new System.Drawing.Point(213, 367);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnOK.Location = new System.Drawing.Point(132, 367);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.aRT_ARTICLEDataGridView);
            this.splitContainer1.Panel1.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitContainer1.Panel2.Controls.Add(label4);
            this.splitContainer1.Panel2.Controls.Add(this.txtLitreStock);
            this.splitContainer1.Panel2.Controls.Add(mOC_IDLabel);
            this.splitContainer1.Panel2.Controls.Add(this.mOC_IDComboBox);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.cOU_IDComboBox);
            this.splitContainer1.Panel2.Controls.Add(cOU_IDLabel);
            this.splitContainer1.Panel2.Controls.Add(aNN_IDLabel);
            this.splitContainer1.Panel2.Controls.Add(this.aNN_IDComboBox);
            this.splitContainer1.Panel2.Controls.Add(aRT_LITRESLabel);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_LITRESTextBox);
            this.splitContainer1.Panel2.Controls.Add(aRT_DESIGNATION_COURTELabel);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_DESIGNATION_COURTETextBox);
            this.splitContainer1.Panel2.Controls.Add(aRT_DESIGNATION_LONGUELabel);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_DESIGNATION_LONGUETextBox);
            this.splitContainer1.Panel2.Controls.Add(aRT_CODELabel);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_CODETextBox);
            this.splitContainer1.Panel2.Controls.Add(aRT_CODE_GROUPELabel);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_CODE_GROUPETextBox);
            this.splitContainer1.Panel2.Controls.Add(this.aRT_IDTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.splitContainer1.Size = new System.Drawing.Size(881, 506);
            this.splitContainer1.SplitterDistance = 585;
            this.splitContainer1.TabIndex = 38;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbAnnee);
            this.panel1.Location = new System.Drawing.Point(165, 132);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 211);
            this.panel1.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label3.Location = new System.Drawing.Point(3, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 21);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label2.Location = new System.Drawing.Point(10, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 20);
            this.label2.TabIndex = 27;
            this.label2.Text = "Gestion des Articles";
            // 
            // aRT_ARTICLEDataGridView
            // 
            this.aRT_ARTICLEDataGridView.AllowUserToAddRows = false;
            this.aRT_ARTICLEDataGridView.AllowUserToDeleteRows = false;
            this.aRT_ARTICLEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_ARTICLEDataGridView.AutoGenerateColumns = false;
            this.aRT_ARTICLEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aRT_ARTICLEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aRT_ARTICLEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ART_ID,
            this.ART_ID_WINBIZ,
            this.COU_ID,
            this.dataGridViewTextBoxColumn2,
            this.SelectArticle,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn7,
            this.ART_VERSION});
            this.aRT_ARTICLEDataGridView.DataSource = this.aRT_ARTICLEBindingSource;
            this.aRT_ARTICLEDataGridView.Location = new System.Drawing.Point(3, 55);
            this.aRT_ARTICLEDataGridView.Name = "aRT_ARTICLEDataGridView";
            this.aRT_ARTICLEDataGridView.Size = new System.Drawing.Size(575, 448);
            this.aRT_ARTICLEDataGridView.TabIndex = 3;
            // 
            // ART_ID
            // 
            this.ART_ID.DataPropertyName = "ART_ID";
            this.ART_ID.HeaderText = "ART_ID";
            this.ART_ID.Name = "ART_ID";
            this.ART_ID.ReadOnly = true;
            this.ART_ID.Visible = false;
            // 
            // ART_ID_WINBIZ
            // 
            this.ART_ID_WINBIZ.DataPropertyName = "ART_ID_WINBIZ";
            this.ART_ID_WINBIZ.HeaderText = "ART_ID_WINBIZ";
            this.ART_ID_WINBIZ.Name = "ART_ID_WINBIZ";
            this.ART_ID_WINBIZ.Visible = false;
            // 
            // COU_ID
            // 
            this.COU_ID.DataPropertyName = "COU_ID";
            this.COU_ID.HeaderText = "COU_ID";
            this.COU_ID.Name = "COU_ID";
            this.COU_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ANN_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "ANN_ID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // SelectArticle
            // 
            this.SelectArticle.HeaderText = "";
            this.SelectArticle.Name = "SelectArticle";
            this.SelectArticle.Visible = false;
            this.SelectArticle.Width = 20;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ART_CODE";
            this.dataGridViewTextBoxColumn11.FillWeight = 95F;
            this.dataGridViewTextBoxColumn11.HeaderText = "Code article";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ART_CODE_GROUPE";
            this.dataGridViewTextBoxColumn12.FillWeight = 95F;
            this.dataGridViewTextBoxColumn12.HeaderText = "Code groupe";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ART_DESIGNATION_COURTE";
            this.dataGridViewTextBoxColumn9.FillWeight = 242F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Désignation courte";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ART_DESIGNATION_LONGUE";
            this.dataGridViewTextBoxColumn10.HeaderText = "ART_DESIGNATION_LONGUE";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            this.dataGridViewTextBoxColumn10.Width = 200;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ART_LITRES_STOCK";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle1.Format = "N1";
            dataGridViewCellStyle1.NullValue = null;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn7.HeaderText = "Stock (litres)";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 50;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // ART_VERSION
            // 
            this.ART_VERSION.DataPropertyName = "ART_VERSION";
            this.ART_VERSION.HeaderText = "ART_VERSION";
            this.ART_VERSION.Name = "ART_VERSION";
            this.ART_VERSION.Visible = false;
            // 
            // txtLitreStock
            // 
            this.txtLitreStock.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLitreStock.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_LITRES_STOCK", true));
            this.txtLitreStock.Location = new System.Drawing.Point(127, 331);
            this.txtLitreStock.Name = "txtLitreStock";
            this.txtLitreStock.Size = new System.Drawing.Size(161, 20);
            this.txtLitreStock.TabIndex = 8;
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mOC_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mOC_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mOC_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aRT_ARTICLEBindingSource, "MOC_ID", true));
            this.mOC_IDComboBox.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(127, 158);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(161, 21);
            this.mOC_IDComboBox.TabIndex = 4;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 51;
            this.label1.Text = "Saisie Article";
            // 
            // cOU_IDComboBox
            // 
            this.cOU_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cOU_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aRT_ARTICLEBindingSource, "COU_ID", true));
            this.cOU_IDComboBox.DataSource = this.cOUCOULEURBindingSource;
            this.cOU_IDComboBox.DisplayMember = "COU_NOM";
            this.cOU_IDComboBox.FormattingEnabled = true;
            this.cOU_IDComboBox.Location = new System.Drawing.Point(127, 129);
            this.cOU_IDComboBox.Name = "cOU_IDComboBox";
            this.cOU_IDComboBox.Size = new System.Drawing.Size(161, 21);
            this.cOU_IDComboBox.TabIndex = 3;
            this.cOU_IDComboBox.ValueMember = "COU_ID";
            // 
            // aNN_IDComboBox
            // 
            this.aNN_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aNN_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aRT_ARTICLEBindingSource, "ANN_ID", true));
            this.aNN_IDComboBox.DataSource = this.aNNANNEEBindingSource;
            this.aNN_IDComboBox.DisplayMember = "ANN_AN";
            this.aNN_IDComboBox.Enabled = false;
            this.aNN_IDComboBox.FormattingEnabled = true;
            this.aNN_IDComboBox.Location = new System.Drawing.Point(127, 51);
            this.aNN_IDComboBox.Name = "aNN_IDComboBox";
            this.aNN_IDComboBox.Size = new System.Drawing.Size(161, 21);
            this.aNN_IDComboBox.TabIndex = 0;
            this.aNN_IDComboBox.ValueMember = "ANN_ID";
            // 
            // aRT_LITRESTextBox
            // 
            this.aRT_LITRESTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_LITRESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_LITRES", true));
            this.aRT_LITRESTextBox.Location = new System.Drawing.Point(127, 304);
            this.aRT_LITRESTextBox.Name = "aRT_LITRESTextBox";
            this.aRT_LITRESTextBox.ReadOnly = true;
            this.aRT_LITRESTextBox.Size = new System.Drawing.Size(161, 20);
            this.aRT_LITRESTextBox.TabIndex = 7;
            // 
            // aRT_DESIGNATION_COURTETextBox
            // 
            this.aRT_DESIGNATION_COURTETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_DESIGNATION_COURTETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_DESIGNATION_COURTE", true));
            this.aRT_DESIGNATION_COURTETextBox.Location = new System.Drawing.Point(127, 186);
            this.aRT_DESIGNATION_COURTETextBox.MaxLength = 40;
            this.aRT_DESIGNATION_COURTETextBox.Name = "aRT_DESIGNATION_COURTETextBox";
            this.aRT_DESIGNATION_COURTETextBox.Size = new System.Drawing.Size(161, 20);
            this.aRT_DESIGNATION_COURTETextBox.TabIndex = 5;
            // 
            // aRT_DESIGNATION_LONGUETextBox
            // 
            this.aRT_DESIGNATION_LONGUETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_DESIGNATION_LONGUETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_DESIGNATION_LONGUE", true));
            this.aRT_DESIGNATION_LONGUETextBox.Location = new System.Drawing.Point(6, 234);
            this.aRT_DESIGNATION_LONGUETextBox.MaxLength = 250;
            this.aRT_DESIGNATION_LONGUETextBox.Multiline = true;
            this.aRT_DESIGNATION_LONGUETextBox.Name = "aRT_DESIGNATION_LONGUETextBox";
            this.aRT_DESIGNATION_LONGUETextBox.Size = new System.Drawing.Size(282, 64);
            this.aRT_DESIGNATION_LONGUETextBox.TabIndex = 6;
            // 
            // aRT_CODETextBox
            // 
            this.aRT_CODETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_CODETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_CODE", true));
            this.aRT_CODETextBox.Location = new System.Drawing.Point(127, 78);
            this.aRT_CODETextBox.MaxLength = 30;
            this.aRT_CODETextBox.Name = "aRT_CODETextBox";
            this.aRT_CODETextBox.Size = new System.Drawing.Size(161, 20);
            this.aRT_CODETextBox.TabIndex = 1;
            // 
            // aRT_CODE_GROUPETextBox
            // 
            this.aRT_CODE_GROUPETextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aRT_CODE_GROUPETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_CODE_GROUPE", true));
            this.aRT_CODE_GROUPETextBox.Location = new System.Drawing.Point(127, 103);
            this.aRT_CODE_GROUPETextBox.MaxLength = 25;
            this.aRT_CODE_GROUPETextBox.Name = "aRT_CODE_GROUPETextBox";
            this.aRT_CODE_GROUPETextBox.Size = new System.Drawing.Size(161, 20);
            this.aRT_CODE_GROUPETextBox.TabIndex = 2;
            // 
            // aRT_IDTextBox
            // 
            this.aRT_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aRT_ARTICLEBindingSource, "ART_ID", true));
            this.aRT_IDTextBox.Location = new System.Drawing.Point(126, -53);
            this.aRT_IDTextBox.Name = "aRT_IDTextBox";
            this.aRT_IDTextBox.Size = new System.Drawing.Size(121, 20);
            this.aRT_IDTextBox.TabIndex = 33;
            this.aRT_IDTextBox.Text = "ART_ID";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolGenerer,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator2,
            this.toolBtnGrouper,
            this.toolBtnDissocier,
            this.toolStripSeparator1,
            this.toolExportWinBIZ,
            this.toolAdd,
            this.toolStripSeparator3,
            this.toolOk,
            this.toolCancel});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(881, 25);
            this.toolStrip2.TabIndex = 39;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolGenerer
            // 
            this.toolGenerer.Image = global::Gestion_Des_Vendanges.Properties.Resources.refresh;
            this.toolGenerer.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolGenerer.Name = "toolGenerer";
            this.toolGenerer.Size = new System.Drawing.Size(68, 22);
            this.toolGenerer.Text = "Générer";
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = global::Gestion_Des_Vendanges.Properties.Resources.open;
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(72, 22);
            this.toolUpdate.Text = "Modifier";
            // 
            // toolDelete
            // 
            this.toolDelete.Image = global::Gestion_Des_Vendanges.Properties.Resources.delete;
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(82, 22);
            this.toolDelete.Text = "Supprimer";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolBtnGrouper
            // 
            this.toolBtnGrouper.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolBtnGrouper.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnGrouper.Name = "toolBtnGrouper";
            this.toolBtnGrouper.Size = new System.Drawing.Size(54, 22);
            this.toolBtnGrouper.Text = "Grouper";
            // 
            // toolBtnDissocier
            // 
            this.toolBtnDissocier.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolBtnDissocier.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnDissocier.Name = "toolBtnDissocier";
            this.toolBtnDissocier.Size = new System.Drawing.Size(58, 22);
            this.toolBtnDissocier.Text = "Dissocier";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolExportWinBIZ
            // 
            this.toolExportWinBIZ.Image = global::Gestion_Des_Vendanges.Properties.Resources.newdoc;
            this.toolExportWinBIZ.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolExportWinBIZ.Name = "toolExportWinBIZ";
            this.toolExportWinBIZ.Size = new System.Drawing.Size(139, 22);
            this.toolExportWinBIZ.Text = "Exporter dans WinBIZ";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = global::Gestion_Des_Vendanges.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(66, 22);
            this.toolAdd.Text = "Ajouter";
            this.toolAdd.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolOk
            // 
            this.toolOk.AutoSize = false;
            this.toolOk.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.toolOk.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolOk.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.toolOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolOk.Name = "toolOk";
            this.toolOk.Size = new System.Drawing.Size(48, 22);
            this.toolOk.Text = "OK";
            this.toolOk.Visible = false;
            // 
            // toolCancel
            // 
            this.toolCancel.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.toolCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.toolCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolCancel.Name = "toolCancel";
            this.toolCancel.Size = new System.Drawing.Size(47, 22);
            this.toolCancel.Text = "Annuler";
            this.toolCancel.Visible = false;
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // CtrlGestionDesArticles
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.splitContainer1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Name = "CtrlGestionDesArticles";
            this.Size = new System.Drawing.Size(881, 506);
            this.Load += new System.EventHandler(this.WfrmGestionDesArticles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRT_ARTICLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aRT_ARTICLEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aRT_ARTICLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ART_ARTICLETableAdapter aRT_ARTICLETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        //private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        //private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private System.Windows.Forms.BindingSource cLACLASSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private System.Windows.Forms.TextBox aRT_IDTextBox;
        private System.Windows.Forms.DataGridView aRT_ARTICLEDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cOU_IDComboBox;
        private System.Windows.Forms.ComboBox aNN_IDComboBox;
        private System.Windows.Forms.TextBox aRT_LITRESTextBox;
        private System.Windows.Forms.TextBox aRT_DESIGNATION_COURTETextBox;
        private System.Windows.Forms.TextBox aRT_DESIGNATION_LONGUETextBox;
        private System.Windows.Forms.TextBox aRT_CODETextBox;
        private System.Windows.Forms.TextBox aRT_CODE_GROUPETextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripButton toolGenerer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolExportWinBIZ;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
        private GVSplitContainer splitContainer1;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
        private System.Windows.Forms.ToolStripButton toolBtnGrouper;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolBtnDissocier;
        private System.Windows.Forms.TextBox txtLitreStock;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolOk;
        private System.Windows.Forms.ToolStripButton toolCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ART_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ART_ID_WINBIZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn COU_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectArticle;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn ART_VERSION;
    }
}