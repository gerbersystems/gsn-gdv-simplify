﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmMenu));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changerDeDossierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripGererLesDossiers = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.ExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportAcquitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportPeseesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restaurerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adressesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProprietaireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesResponsablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesPressoirsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.gestionDesAdressesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesCommunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CommuneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesLieuxDeProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesDistrictsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.gestionDesMentionsParticulièresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GlobalParamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AnToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.CepToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesDegrésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.gestionDesCouleursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClasToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.zonesDeTravailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.dossierWinBIZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rapportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeAcquitsStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.apercuListeAcquitsStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfListeAcquitsStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.xlsListeAcquitsStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.docListeAcquitsStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParProducteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParProducteur_WordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnKilosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnLitresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParCepageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParCepage_PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParCepage_WordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesParcellesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aperçuListeParcellesVisiteVignesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfListeParcellesVisiteVignesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xlsListeParcellesVisiteVignesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordListeParcellesVisiteVignesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.droitsProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.droitsProductionAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.droitsProductionPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.droitsProductionExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.droitsProductionWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AttestationsDeControleStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.apercuAttestationsControleStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfAttestationsControleStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.xlsAttestationsControleStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.docAttestationsControleStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.declarationDencavageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apercuDeclarationEncavage = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfDeclarationEncavage = new System.Windows.Forms.ToolStripMenuItem();
            this.xlsDeclarationEncavage = new System.Windows.Forms.ToolStripMenuItem();
            this.docDeclarationEncavage = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosLitresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepagePDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepagExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tablesDesPrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apercutablesDesPrix = new System.Windows.Forms.ToolStripMenuItem();
            this.PDFtablesDesPrix = new System.Windows.Forms.ToolStripMenuItem();
            this.ExceltablesDesPrix = new System.Windows.Forms.ToolStripMenuItem();
            this.WordtablesDesPrix = new System.Windows.Forms.ToolStripMenuItem();
            this.facturesFournisseursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aperçuAvantFactures = new System.Windows.Forms.ToolStripMenuItem();
            this.PDFFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xlsFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.FacturemicrosoftWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.déclarationDencavageOCVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolDriverOLEDB = new System.Windows.Forms.ToolStripMenuItem();
            this.changerCleToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTeleAssitance = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.menuStrip.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.menuStrip.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.adressesToolStripMenuItem,
            this.gestionDesCommunesToolStripMenuItem,
            this.pToolStripMenuItem,
            this.rapportsToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(680, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changerDeDossierToolStripMenuItem,
            this.toolStripGererLesDossiers,
            this.toolStripSeparator8,
            this.ExportToolStripMenuItem,
            this.sauvegarderToolStripMenuItem,
            this.restaurerToolStripMenuItem,
            this.toolStripSeparator3,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "&Fichier";
            // 
            // changerDeDossierToolStripMenuItem
            // 
            this.changerDeDossierToolStripMenuItem.Name = "changerDeDossierToolStripMenuItem";
            this.changerDeDossierToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.changerDeDossierToolStripMenuItem.Text = "&Changer de dossier";
            this.changerDeDossierToolStripMenuItem.Click += new System.EventHandler(this.ChangerDeDossierToolStripMenuItem_Click);
            // 
            // toolStripGererLesDossiers
            // 
            this.toolStripGererLesDossiers.Name = "toolStripGererLesDossiers";
            this.toolStripGererLesDossiers.Size = new System.Drawing.Size(209, 22);
            this.toolStripGererLesDossiers.Text = "&Gérer les dossiers";
            this.toolStripGererLesDossiers.Click += new System.EventHandler(this.ToolStripGererLesDossiers_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(206, 6);
            // 
            // ExportToolStripMenuItem
            // 
            this.ExportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportAcquitsToolStripMenuItem,
            this.ExportPeseesToolStripMenuItem});
            this.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem";
            this.ExportToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.ExportToolStripMenuItem.Text = "&Exporter ...";
            // 
            // ExportAcquitsToolStripMenuItem
            // 
            this.ExportAcquitsToolStripMenuItem.Name = "ExportAcquitsToolStripMenuItem";
            this.ExportAcquitsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ExportAcquitsToolStripMenuItem.Text = "Acquits";
            // 
            // ExportPeseesToolStripMenuItem
            // 
            this.ExportPeseesToolStripMenuItem.Name = "ExportPeseesToolStripMenuItem";
            this.ExportPeseesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ExportPeseesToolStripMenuItem.Text = "Pesées";
            // 
            // sauvegarderToolStripMenuItem
            // 
            this.sauvegarderToolStripMenuItem.Name = "sauvegarderToolStripMenuItem";
            this.sauvegarderToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.sauvegarderToolStripMenuItem.Text = "&Sauvegarder les données";
            this.sauvegarderToolStripMenuItem.Click += new System.EventHandler(this.SauvegarderToolStripMenuItem_Click);
            // 
            // restaurerToolStripMenuItem
            // 
            this.restaurerToolStripMenuItem.Name = "restaurerToolStripMenuItem";
            this.restaurerToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.restaurerToolStripMenuItem.Text = "&Restaurer une sauvegarde";
            this.restaurerToolStripMenuItem.Click += new System.EventHandler(this.RestaurerToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(206, 6);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.quitterToolStripMenuItem.Text = "&Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.QuitterToolStripMenuItem_Click);
            // 
            // adressesToolStripMenuItem
            // 
            this.adressesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProprietaireToolStripMenuItem,
            this.gestionDesResponsablesToolStripMenuItem,
            this.gestionDesPressoirsToolStripMenuItem,
            this.toolStripSeparator2,
            this.gestionDesAdressesToolStripMenuItem});
            this.adressesToolStripMenuItem.Name = "adressesToolStripMenuItem";
            this.adressesToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.adressesToolStripMenuItem.Text = "&Adresses";
            // 
            // ProprietaireToolStripMenuItem
            // 
            this.ProprietaireToolStripMenuItem.Name = "ProprietaireToolStripMenuItem";
            this.ProprietaireToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.ProprietaireToolStripMenuItem.Text = "&Propriétaires / producteurs";
            this.ProprietaireToolStripMenuItem.Click += new System.EventHandler(this.ProprietaireToolStripMenuItem_Click);
            // 
            // gestionDesResponsablesToolStripMenuItem
            // 
            this.gestionDesResponsablesToolStripMenuItem.Name = "gestionDesResponsablesToolStripMenuItem";
            this.gestionDesResponsablesToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.gestionDesResponsablesToolStripMenuItem.Text = "&Responsables de pesées";
            this.gestionDesResponsablesToolStripMenuItem.Click += new System.EventHandler(this.gestionDesResponsablesToolStripMenuItem_Click);
            // 
            // gestionDesPressoirsToolStripMenuItem
            // 
            this.gestionDesPressoirsToolStripMenuItem.Name = "gestionDesPressoirsToolStripMenuItem";
            this.gestionDesPressoirsToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.gestionDesPressoirsToolStripMenuItem.Text = "Pr&essoirs";
            this.gestionDesPressoirsToolStripMenuItem.Click += new System.EventHandler(this.gestionDesPressoirsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(212, 6);
            // 
            // gestionDesAdressesToolStripMenuItem
            // 
            this.gestionDesAdressesToolStripMenuItem.Name = "gestionDesAdressesToolStripMenuItem";
            this.gestionDesAdressesToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.gestionDesAdressesToolStripMenuItem.Text = "Gestion des &adresses";
            this.gestionDesAdressesToolStripMenuItem.Click += new System.EventHandler(this.GestionDesAdressesToolStripMenuItem_Click);
            // 
            // gestionDesCommunesToolStripMenuItem
            // 
            this.gestionDesCommunesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CommuneToolStripMenuItem,
            this.gestionDesLieuxDeProductionToolStripMenuItem,
            this.gestionDesDistrictsToolStripMenuItem,
            this.RegionToolStripMenuItem,
            this.toolStripSeparator7,
            this.gestionDesMentionsParticulièresToolStripMenuItem});
            this.gestionDesCommunesToolStripMenuItem.Name = "gestionDesCommunesToolStripMenuItem";
            this.gestionDesCommunesToolStripMenuItem.Size = new System.Drawing.Size(136, 20);
            this.gestionDesCommunesToolStripMenuItem.Text = "&Communes et régions";
            // 
            // CommuneToolStripMenuItem
            // 
            this.CommuneToolStripMenuItem.Name = "CommuneToolStripMenuItem";
            this.CommuneToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.CommuneToolStripMenuItem.Text = "&Communes";
            this.CommuneToolStripMenuItem.Click += new System.EventHandler(this.CommuneToolStripMenuItem_Click);
            // 
            // gestionDesLieuxDeProductionToolStripMenuItem
            // 
            this.gestionDesLieuxDeProductionToolStripMenuItem.Name = "gestionDesLieuxDeProductionToolStripMenuItem";
            this.gestionDesLieuxDeProductionToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.gestionDesLieuxDeProductionToolStripMenuItem.Text = "&Lieu de production";
            this.gestionDesLieuxDeProductionToolStripMenuItem.Click += new System.EventHandler(this.GestionDesLieuxDeProductionToolStripMenuItem_Click);
            // 
            // gestionDesDistrictsToolStripMenuItem
            // 
            this.gestionDesDistrictsToolStripMenuItem.Name = "gestionDesDistrictsToolStripMenuItem";
            this.gestionDesDistrictsToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.gestionDesDistrictsToolStripMenuItem.Text = "&Districts";
            this.gestionDesDistrictsToolStripMenuItem.Click += new System.EventHandler(this.gestionDesDistrictsToolStripMenuItem_Click);
            // 
            // RegionToolStripMenuItem
            // 
            this.RegionToolStripMenuItem.Name = "RegionToolStripMenuItem";
            this.RegionToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.RegionToolStripMenuItem.Text = "&Régions";
            this.RegionToolStripMenuItem.Click += new System.EventHandler(this.RegionToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(188, 6);
            // 
            // gestionDesMentionsParticulièresToolStripMenuItem
            // 
            this.gestionDesMentionsParticulièresToolStripMenuItem.Name = "gestionDesMentionsParticulièresToolStripMenuItem";
            this.gestionDesMentionsParticulièresToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.gestionDesMentionsParticulièresToolStripMenuItem.Text = "&Mentions particulières";
            this.gestionDesMentionsParticulièresToolStripMenuItem.Click += new System.EventHandler(this.GestionDesMentionsParticulièresToolStripMenuItem_Click);
            // 
            // pToolStripMenuItem
            // 
            this.pToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GlobalParamToolStripMenuItem,
            this.AnToolStripMenuItem2,
            this.toolStripSeparator4,
            this.CepToolStripMenuItem2,
            this.gestionDesDegrésToolStripMenuItem,
            this.toolStripSeparator5,
            this.gestionDesCouleursToolStripMenuItem,
            this.ClasToolStripMenuItem2,
            this.zonesDeTravailToolStripMenuItem,
            this.toolStripSeparator1,
            this.dossierWinBIZToolStripMenuItem});
            this.pToolStripMenuItem.Name = "pToolStripMenuItem";
            this.pToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.pToolStripMenuItem.Text = "&Paramètres";
            // 
            // GlobalParamToolStripMenuItem
            // 
            this.GlobalParamToolStripMenuItem.Name = "GlobalParamToolStripMenuItem";
            this.GlobalParamToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.GlobalParamToolStripMenuItem.Text = "Paramètres";
            this.GlobalParamToolStripMenuItem.Click += new System.EventHandler(this.GlobalParamToolStripMenuItem_Click);
            // 
            // AnToolStripMenuItem2
            // 
            this.AnToolStripMenuItem2.Name = "AnToolStripMenuItem2";
            this.AnToolStripMenuItem2.Size = new System.Drawing.Size(190, 22);
            this.AnToolStripMenuItem2.Text = "Paramètres de l\'&année";
            this.AnToolStripMenuItem2.Click += new System.EventHandler(this.AnToolStripMenuItem2_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // CepToolStripMenuItem2
            // 
            this.CepToolStripMenuItem2.Name = "CepToolStripMenuItem2";
            this.CepToolStripMenuItem2.Size = new System.Drawing.Size(190, 22);
            this.CepToolStripMenuItem2.Text = "&Cépages";
            this.CepToolStripMenuItem2.Click += new System.EventHandler(this.CepToolStripMenuItem2_Click);
            // 
            // gestionDesDegrésToolStripMenuItem
            // 
            this.gestionDesDegrésToolStripMenuItem.Name = "gestionDesDegrésToolStripMenuItem";
            this.gestionDesDegrésToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.gestionDesDegrésToolStripMenuItem.Text = "&Degrés (Oe/Brix)";
            this.gestionDesDegrésToolStripMenuItem.Click += new System.EventHandler(this.GestionDesDegrésToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(187, 6);
            // 
            // gestionDesCouleursToolStripMenuItem
            // 
            this.gestionDesCouleursToolStripMenuItem.Name = "gestionDesCouleursToolStripMenuItem";
            this.gestionDesCouleursToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.gestionDesCouleursToolStripMenuItem.Text = "&Couleurs";
            this.gestionDesCouleursToolStripMenuItem.Click += new System.EventHandler(this.GestionDesCouleursToolStripMenuItem_Click);
            // 
            // ClasToolStripMenuItem2
            // 
            this.ClasToolStripMenuItem2.Name = "ClasToolStripMenuItem2";
            this.ClasToolStripMenuItem2.Size = new System.Drawing.Size(190, 22);
            this.ClasToolStripMenuItem2.Text = "&Classes";
            this.ClasToolStripMenuItem2.Click += new System.EventHandler(this.ClasToolStripMenuItem2_Click);
            // 
            // zonesDeTravailToolStripMenuItem
            // 
            this.zonesDeTravailToolStripMenuItem.Name = "zonesDeTravailToolStripMenuItem";
            this.zonesDeTravailToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.zonesDeTravailToolStripMenuItem.Text = "Zones de travail";
            this.zonesDeTravailToolStripMenuItem.Click += new System.EventHandler(this.zonesDeTravailToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
            // 
            // dossierWinBIZToolStripMenuItem
            // 
            this.dossierWinBIZToolStripMenuItem.Name = "dossierWinBIZToolStripMenuItem";
            this.dossierWinBIZToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.dossierWinBIZToolStripMenuItem.Text = "D&ossier WinBIZ";
            this.dossierWinBIZToolStripMenuItem.Click += new System.EventHandler(this.DossierWinBIZToolStripMenuItem_Click);
            // 
            // rapportsToolStripMenuItem
            // 
            this.rapportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeAcquitsStripMenuItem2,
            this.vendangesLivreesParToolStripMenuItem,
            this.listeDesParcellesToolStripMenuItem,
            this.droitsProductionToolStripMenuItem,
            this.AttestationsDeControleStripMenuItem2,
            this.declarationDencavageToolStripMenuItem,
            this.ApportsParProducteurToolStripMenuItem,
            this.sondageMoyenParToolStripMenuItem,
            this.tablesDesPrixToolStripMenuItem,
            this.facturesFournisseursToolStripMenuItem,
            this.toolStripSeparator6,
            this.déclarationDencavageOCVToolStripMenuItem});
            this.rapportsToolStripMenuItem.Name = "rapportsToolStripMenuItem";
            this.rapportsToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.rapportsToolStripMenuItem.Text = "&Rapports";
            // 
            // listeAcquitsStripMenuItem2
            // 
            this.listeAcquitsStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apercuListeAcquitsStripMenuItem2,
            this.pdfListeAcquitsStripMenuItem2,
            this.xlsListeAcquitsStripMenuItem2,
            this.docListeAcquitsStripMenuItem2});
            this.listeAcquitsStripMenuItem2.Name = "listeAcquitsStripMenuItem2";
            this.listeAcquitsStripMenuItem2.Size = new System.Drawing.Size(296, 22);
            this.listeAcquitsStripMenuItem2.Text = "&Liste des acquits";
            this.listeAcquitsStripMenuItem2.Click += new System.EventHandler(this.ListeAcquitsStripMenuItem2_Click);
            // 
            // apercuListeAcquitsStripMenuItem2
            // 
            this.apercuListeAcquitsStripMenuItem2.Name = "apercuListeAcquitsStripMenuItem2";
            this.apercuListeAcquitsStripMenuItem2.Size = new System.Drawing.Size(250, 22);
            this.apercuListeAcquitsStripMenuItem2.Text = "&Aperçu avant impression";
            this.apercuListeAcquitsStripMenuItem2.Click += new System.EventHandler(this.ApercuListeAcquitsStripMenuItem2_Click);
            // 
            // pdfListeAcquitsStripMenuItem2
            // 
            this.pdfListeAcquitsStripMenuItem2.Name = "pdfListeAcquitsStripMenuItem2";
            this.pdfListeAcquitsStripMenuItem2.Size = new System.Drawing.Size(250, 22);
            this.pdfListeAcquitsStripMenuItem2.Text = "&Portable Document Format (PDF)";
            this.pdfListeAcquitsStripMenuItem2.Click += new System.EventHandler(this.PdfListeAcquitsStripMenuItem2_Click);
            // 
            // xlsListeAcquitsStripMenuItem2
            // 
            this.xlsListeAcquitsStripMenuItem2.Name = "xlsListeAcquitsStripMenuItem2";
            this.xlsListeAcquitsStripMenuItem2.Size = new System.Drawing.Size(250, 22);
            this.xlsListeAcquitsStripMenuItem2.Text = "Microsoft &Excel";
            this.xlsListeAcquitsStripMenuItem2.Click += new System.EventHandler(this.XlsListeAcquitsStripMenuItem2_Click);
            // 
            // docListeAcquitsStripMenuItem2
            // 
            this.docListeAcquitsStripMenuItem2.Name = "docListeAcquitsStripMenuItem2";
            this.docListeAcquitsStripMenuItem2.Size = new System.Drawing.Size(250, 22);
            this.docListeAcquitsStripMenuItem2.Text = "Microsoft &Word";
            this.docListeAcquitsStripMenuItem2.Click += new System.EventHandler(this.DocListeAcquitsStripMenuItem2_Click);
            // 
            // vendangesLivreesParToolStripMenuItem
            // 
            this.vendangesLivreesParToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParProducteurToolStripMenuItem,
            this.vendangesLivreesParDateToolStripMenuItem,
            this.vendangesLivreesParCepageToolStripMenuItem,
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem});
            this.vendangesLivreesParToolStripMenuItem.Name = "vendangesLivreesParToolStripMenuItem";
            this.vendangesLivreesParToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.vendangesLivreesParToolStripMenuItem.Text = "&Vendanges livrées par ...";
            // 
            // vendangesLivreesParProducteurToolStripMenuItem
            // 
            this.vendangesLivreesParProducteurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem,
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem,
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem,
            this.vendangesLivreesParProducteur_WordToolStripMenuItem});
            this.vendangesLivreesParProducteurToolStripMenuItem.Name = "vendangesLivreesParProducteurToolStripMenuItem";
            this.vendangesLivreesParProducteurToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.vendangesLivreesParProducteurToolStripMenuItem.Text = "Producteur";
            this.vendangesLivreesParProducteurToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParProducteur_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParProducteur_AperçuToolStripMenuItem
            // 
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem.Name = "vendangesLivreesParProducteur_AperçuToolStripMenuItem";
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.vendangesLivreesParProducteur_AperçuToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParProducteur_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParProducteur_PDFToolStripMenuItem
            // 
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem.Name = "vendangesLivreesParProducteur_PDFToolStripMenuItem";
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem.Text = "PDF";
            this.vendangesLivreesParProducteur_PDFToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParProducteur_PDFToolStripMenuItem_Click);
            // 
            // vendangesLivreesParProducteur_ExcelToolStripMenuItem
            // 
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem.Name = "vendangesLivreesParProducteur_ExcelToolStripMenuItem";
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem.Text = "Excel";
            this.vendangesLivreesParProducteur_ExcelToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParProducteur_ExcelToolStripMenuItem_Click);
            // 
            // vendangesLivreesParProducteur_WordToolStripMenuItem
            // 
            this.vendangesLivreesParProducteur_WordToolStripMenuItem.Name = "vendangesLivreesParProducteur_WordToolStripMenuItem";
            this.vendangesLivreesParProducteur_WordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParProducteur_WordToolStripMenuItem.Text = "Word";
            this.vendangesLivreesParProducteur_WordToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParProducteur_WordToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateToolStripMenuItem
            // 
            this.vendangesLivreesParDateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParDateEnKilosToolStripMenuItem,
            this.vendangesLivreesParDateEnLitresToolStripMenuItem});
            this.vendangesLivreesParDateToolStripMenuItem.Name = "vendangesLivreesParDateToolStripMenuItem";
            this.vendangesLivreesParDateToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.vendangesLivreesParDateToolStripMenuItem.Text = "Date";
            // 
            // vendangesLivreesParDateEnKilosToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnKilosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem,
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem,
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem,
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem});
            this.vendangesLivreesParDateEnKilosToolStripMenuItem.Name = "vendangesLivreesParDateEnKilosToolStripMenuItem";
            this.vendangesLivreesParDateEnKilosToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.vendangesLivreesParDateEnKilosToolStripMenuItem.Text = "Kilos";
            this.vendangesLivreesParDateEnKilosToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnKilos_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem.Name = "vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem";
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem.Text = "Aperçu avant impression";
            this.vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnKilos_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnKilos_PDFToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem.Name = "vendangesLivreesParDateEnKilos_PDFToolStripMenuItem";
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem.Text = "PDF";
            this.vendangesLivreesParDateEnKilos_PDFToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnKilos_PDFToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem.Name = "vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem";
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem.Text = "Excel";
            this.vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnKilos_ExcelToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnKilos_WordToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem.Name = "vendangesLivreesParDateEnKilos_WordToolStripMenuItem";
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem.Text = "Word";
            this.vendangesLivreesParDateEnKilos_WordToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnKilos_WordToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnLitresToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnLitresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem,
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem,
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem,
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem});
            this.vendangesLivreesParDateEnLitresToolStripMenuItem.Name = "vendangesLivreesParDateEnLitresToolStripMenuItem";
            this.vendangesLivreesParDateEnLitresToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.vendangesLivreesParDateEnLitresToolStripMenuItem.Text = "Litres";
            this.vendangesLivreesParDateEnLitresToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnLitres_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem.Name = "vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem";
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem.Text = "Aperçu avant impresssion";
            this.vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnLitres_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnLitres_PDFToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem.Name = "vendangesLivreesParDateEnLitres_PDFToolStripMenuItem";
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem.Text = "PDF";
            this.vendangesLivreesParDateEnLitres_PDFToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnLitres_PDFToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem.Name = "vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem";
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem.Text = "Excel";
            this.vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnLitres_ExcelToolStripMenuItem_Click);
            // 
            // vendangesLivreesParDateEnLitres_WordToolStripMenuItem
            // 
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem.Name = "vendangesLivreesParDateEnLitres_WordToolStripMenuItem";
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem.Text = "Word";
            this.vendangesLivreesParDateEnLitres_WordToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParDateEnLitres_WordToolStripMenuItem_Click);
            // 
            // vendangesLivreesParCepageToolStripMenuItem
            // 
            this.vendangesLivreesParCepageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem,
            this.vendangesLivreesParCepage_PDFToolStripMenuItem,
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem,
            this.vendangesLivreesParCepage_WordToolStripMenuItem});
            this.vendangesLivreesParCepageToolStripMenuItem.Name = "vendangesLivreesParCepageToolStripMenuItem";
            this.vendangesLivreesParCepageToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.vendangesLivreesParCepageToolStripMenuItem.Text = "Cépage";
            this.vendangesLivreesParCepageToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParCepage_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParCepage_AperçuToolStripMenuItem
            // 
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem.Name = "vendangesLivreesParCepage_AperçuToolStripMenuItem";
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.vendangesLivreesParCepage_AperçuToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParCepage_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParCepage_PDFToolStripMenuItem
            // 
            this.vendangesLivreesParCepage_PDFToolStripMenuItem.Name = "vendangesLivreesParCepage_PDFToolStripMenuItem";
            this.vendangesLivreesParCepage_PDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParCepage_PDFToolStripMenuItem.Text = "PDF";
            this.vendangesLivreesParCepage_PDFToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParCepage_PDFToolStripMenuItem_Click);
            // 
            // vendangesLivreesParCepage_ExcelToolStripMenuItem
            // 
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem.Name = "vendangesLivreesParCepage_ExcelToolStripMenuItem";
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem.Text = "Excel";
            this.vendangesLivreesParCepage_ExcelToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParCepage_ExcelToolStripMenuItem_Click);
            // 
            // vendangesLivreesParCepage_WordToolStripMenuItem
            // 
            this.vendangesLivreesParCepage_WordToolStripMenuItem.Name = "vendangesLivreesParCepage_WordToolStripMenuItem";
            this.vendangesLivreesParCepage_WordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParCepage_WordToolStripMenuItem.Text = "Word";
            this.vendangesLivreesParCepage_WordToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParCepage_WordToolStripMenuItem_Click);
            // 
            // vendangesLivreesParLieuDeProductionToolStripMenuItem
            // 
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem,
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem,
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem,
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem});
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem.Name = "vendangesLivreesParLieuDeProductionToolStripMenuItem";
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem.Text = "Lieu de production";
            this.vendangesLivreesParLieuDeProductionToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem
            // 
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem.Name = "vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem";
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem_Click);
            // 
            // vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem
            // 
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem.Name = "vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem";
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem.Text = "PDF";
            this.vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParLieuDeProduction_PDFToolStripMenuItem_Click);
            // 
            // vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem
            // 
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem.Name = "vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem";
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem.Text = "Excel";
            this.vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem_Click);
            // 
            // vendangesLivreesParLieuDeProduction_WordToolStripMenuItem
            // 
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem.Name = "vendangesLivreesParLieuDeProduction_WordToolStripMenuItem";
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem.Text = "Word";
            this.vendangesLivreesParLieuDeProduction_WordToolStripMenuItem.Click += new System.EventHandler(this.VendangesLivreesParLieuDeProduction_WordToolStripMenuItem_Click);
            // 
            // listeDesParcellesToolStripMenuItem
            // 
            this.listeDesParcellesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aperçuListeParcellesVisiteVignesMenuItem,
            this.pdfListeParcellesVisiteVignesMenuItem,
            this.xlsListeParcellesVisiteVignesMenuItem,
            this.wordListeParcellesVisiteVignesMenuItem});
            this.listeDesParcellesToolStripMenuItem.Name = "listeDesParcellesToolStripMenuItem";
            this.listeDesParcellesToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.listeDesParcellesToolStripMenuItem.Text = "Liste des parcelles pour la visite des vignes";
            this.listeDesParcellesToolStripMenuItem.Click += new System.EventHandler(this.aperçuListeParcellesVisiteVignesMenuItem_Click);
            // 
            // aperçuListeParcellesVisiteVignesMenuItem
            // 
            this.aperçuListeParcellesVisiteVignesMenuItem.Name = "aperçuListeParcellesVisiteVignesMenuItem";
            this.aperçuListeParcellesVisiteVignesMenuItem.Size = new System.Drawing.Size(218, 22);
            this.aperçuListeParcellesVisiteVignesMenuItem.Text = "&Aperçu avant impression";
            this.aperçuListeParcellesVisiteVignesMenuItem.Click += new System.EventHandler(this.aperçuListeParcellesVisiteVignesMenuItem_Click);
            // 
            // pdfListeParcellesVisiteVignesMenuItem
            // 
            this.pdfListeParcellesVisiteVignesMenuItem.Name = "pdfListeParcellesVisiteVignesMenuItem";
            this.pdfListeParcellesVisiteVignesMenuItem.Size = new System.Drawing.Size(218, 22);
            this.pdfListeParcellesVisiteVignesMenuItem.Text = "&Portable Document Format";
            this.pdfListeParcellesVisiteVignesMenuItem.Click += new System.EventHandler(this.pdfListeParcellesVisiteVignesMenuItem_Click);
            // 
            // xlsListeParcellesVisiteVignesMenuItem
            // 
            this.xlsListeParcellesVisiteVignesMenuItem.Name = "xlsListeParcellesVisiteVignesMenuItem";
            this.xlsListeParcellesVisiteVignesMenuItem.Size = new System.Drawing.Size(218, 22);
            this.xlsListeParcellesVisiteVignesMenuItem.Text = "Microsoft &Excel";
            this.xlsListeParcellesVisiteVignesMenuItem.Click += new System.EventHandler(this.xlsListeParcellesVisiteVignesMenuItem_Click);
            // 
            // wordListeParcellesVisiteVignesMenuItem
            // 
            this.wordListeParcellesVisiteVignesMenuItem.Name = "wordListeParcellesVisiteVignesMenuItem";
            this.wordListeParcellesVisiteVignesMenuItem.Size = new System.Drawing.Size(218, 22);
            this.wordListeParcellesVisiteVignesMenuItem.Text = "Microsoft &Word";
            this.wordListeParcellesVisiteVignesMenuItem.Click += new System.EventHandler(this.wordListeParcellesVisiteVignesMenuItem_Click);
            // 
            // droitsProductionToolStripMenuItem
            // 
            this.droitsProductionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.droitsProductionAperçuToolStripMenuItem,
            this.droitsProductionPDFToolStripMenuItem,
            this.droitsProductionExcelToolStripMenuItem,
            this.droitsProductionWordToolStripMenuItem});
            this.droitsProductionToolStripMenuItem.Name = "droitsProductionToolStripMenuItem";
            this.droitsProductionToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.droitsProductionToolStripMenuItem.Text = "D&roits de production";
            this.droitsProductionToolStripMenuItem.Click += new System.EventHandler(this.DroitsProductionAperçuToolStripMenuItem_Click);
            // 
            // droitsProductionAperçuToolStripMenuItem
            // 
            this.droitsProductionAperçuToolStripMenuItem.Name = "droitsProductionAperçuToolStripMenuItem";
            this.droitsProductionAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.droitsProductionAperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.droitsProductionAperçuToolStripMenuItem.Click += new System.EventHandler(this.DroitsProductionAperçuToolStripMenuItem_Click);
            // 
            // droitsProductionPDFToolStripMenuItem
            // 
            this.droitsProductionPDFToolStripMenuItem.Name = "droitsProductionPDFToolStripMenuItem";
            this.droitsProductionPDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.droitsProductionPDFToolStripMenuItem.Text = "PDF";
            this.droitsProductionPDFToolStripMenuItem.Click += new System.EventHandler(this.DroitsProductionPDFToolStripMenuItem_Click);
            // 
            // droitsProductionExcelToolStripMenuItem
            // 
            this.droitsProductionExcelToolStripMenuItem.Name = "droitsProductionExcelToolStripMenuItem";
            this.droitsProductionExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.droitsProductionExcelToolStripMenuItem.Text = "Excel";
            this.droitsProductionExcelToolStripMenuItem.Click += new System.EventHandler(this.DroitsProductionExcelToolStripMenuItem_Click);
            // 
            // droitsProductionWordToolStripMenuItem
            // 
            this.droitsProductionWordToolStripMenuItem.Name = "droitsProductionWordToolStripMenuItem";
            this.droitsProductionWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.droitsProductionWordToolStripMenuItem.Text = "Word";
            this.droitsProductionWordToolStripMenuItem.Click += new System.EventHandler(this.DroitsProductionWordToolStripMenuItem_Click);
            // 
            // AttestationsDeControleStripMenuItem2
            // 
            this.AttestationsDeControleStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apercuAttestationsControleStripMenuItem3,
            this.pdfAttestationsControleStripMenuItem3,
            this.xlsAttestationsControleStripMenuItem3,
            this.docAttestationsControleStripMenuItem3});
            this.AttestationsDeControleStripMenuItem2.Name = "AttestationsDeControleStripMenuItem2";
            this.AttestationsDeControleStripMenuItem2.Size = new System.Drawing.Size(296, 22);
            this.AttestationsDeControleStripMenuItem2.Text = "&Attestations de contrôle";
            this.AttestationsDeControleStripMenuItem2.Click += new System.EventHandler(this.AttestationsDeControleStripMenuItem2_Click);
            // 
            // apercuAttestationsControleStripMenuItem3
            // 
            this.apercuAttestationsControleStripMenuItem3.Name = "apercuAttestationsControleStripMenuItem3";
            this.apercuAttestationsControleStripMenuItem3.Size = new System.Drawing.Size(250, 22);
            this.apercuAttestationsControleStripMenuItem3.Text = "&Aperçu avant impression";
            this.apercuAttestationsControleStripMenuItem3.Click += new System.EventHandler(this.ApercuAttestationsControleStripMenuItem3_Click);
            // 
            // pdfAttestationsControleStripMenuItem3
            // 
            this.pdfAttestationsControleStripMenuItem3.Name = "pdfAttestationsControleStripMenuItem3";
            this.pdfAttestationsControleStripMenuItem3.Size = new System.Drawing.Size(250, 22);
            this.pdfAttestationsControleStripMenuItem3.Text = "&Portable Document Format (PDF)";
            this.pdfAttestationsControleStripMenuItem3.Click += new System.EventHandler(this.PdfAttestationsControleStripMenuItem3_Click);
            // 
            // xlsAttestationsControleStripMenuItem3
            // 
            this.xlsAttestationsControleStripMenuItem3.Name = "xlsAttestationsControleStripMenuItem3";
            this.xlsAttestationsControleStripMenuItem3.Size = new System.Drawing.Size(250, 22);
            this.xlsAttestationsControleStripMenuItem3.Text = "Microsoft &Excel";
            this.xlsAttestationsControleStripMenuItem3.Click += new System.EventHandler(this.XlsAttestationsControleStripMenuItem3_Click);
            // 
            // docAttestationsControleStripMenuItem3
            // 
            this.docAttestationsControleStripMenuItem3.Name = "docAttestationsControleStripMenuItem3";
            this.docAttestationsControleStripMenuItem3.Size = new System.Drawing.Size(250, 22);
            this.docAttestationsControleStripMenuItem3.Text = "Microsoft &Word";
            this.docAttestationsControleStripMenuItem3.Click += new System.EventHandler(this.DocAttestationsControleStripMenuItem3_Click);
            // 
            // declarationDencavageToolStripMenuItem
            // 
            this.declarationDencavageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apercuDeclarationEncavage,
            this.pdfDeclarationEncavage,
            this.xlsDeclarationEncavage,
            this.docDeclarationEncavage});
            this.declarationDencavageToolStripMenuItem.Name = "declarationDencavageToolStripMenuItem";
            this.declarationDencavageToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.declarationDencavageToolStripMenuItem.Text = "&Déclaration d\'encavage";
            // 
            // apercuDeclarationEncavage
            // 
            this.apercuDeclarationEncavage.Name = "apercuDeclarationEncavage";
            this.apercuDeclarationEncavage.Size = new System.Drawing.Size(250, 22);
            this.apercuDeclarationEncavage.Text = "&Aperçu avant impression";
            this.apercuDeclarationEncavage.Click += new System.EventHandler(this.apercuDeclarationEncavage_Click);
            // 
            // pdfDeclarationEncavage
            // 
            this.pdfDeclarationEncavage.Name = "pdfDeclarationEncavage";
            this.pdfDeclarationEncavage.Size = new System.Drawing.Size(250, 22);
            this.pdfDeclarationEncavage.Text = "&Portable Document Format (PDF)";
            this.pdfDeclarationEncavage.Click += new System.EventHandler(this.pdfDeclarationEncavage_Click);
            // 
            // xlsDeclarationEncavage
            // 
            this.xlsDeclarationEncavage.Name = "xlsDeclarationEncavage";
            this.xlsDeclarationEncavage.Size = new System.Drawing.Size(250, 22);
            this.xlsDeclarationEncavage.Text = "Microsoft &Excel";
            this.xlsDeclarationEncavage.Click += new System.EventHandler(this.xlsDeclarationEncavage_Click);
            // 
            // docDeclarationEncavage
            // 
            this.docDeclarationEncavage.Name = "docDeclarationEncavage";
            this.docDeclarationEncavage.Size = new System.Drawing.Size(250, 22);
            this.docDeclarationEncavage.Text = "Microsoft &Word";
            this.docDeclarationEncavage.Click += new System.EventHandler(this.docDeclarationEncavage_Click);
            // 
            // ApportsParProducteurToolStripMenuItem
            // 
            this.ApportsParProducteurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApportsParProducteurKilosToolStripMenuItem,
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem,
            this.ApportsParProducteurKilosLitresToolStripMenuItem});
            this.ApportsParProducteurToolStripMenuItem.Name = "ApportsParProducteurToolStripMenuItem";
            this.ApportsParProducteurToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.ApportsParProducteurToolStripMenuItem.Text = "App&orts par producteur en ...";
            // 
            // ApportsParProducteurKilosToolStripMenuItem
            // 
            this.ApportsParProducteurKilosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApportsParProducteurKilosAperçuToolStripMenuItem,
            this.ApportsParProducteurKilosPDFToolStripMenuItem,
            this.ApportsParProducteurKilosExcelToolStripMenuItem,
            this.ApportsParProducteurKilosWordToolStripMenuItem});
            this.ApportsParProducteurKilosToolStripMenuItem.Name = "ApportsParProducteurKilosToolStripMenuItem";
            this.ApportsParProducteurKilosToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.ApportsParProducteurKilosToolStripMenuItem.Text = "Kilos";
            this.ApportsParProducteurKilosToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosAperçuToolStripMenuItem
            // 
            this.ApportsParProducteurKilosAperçuToolStripMenuItem.Name = "ApportsParProducteurKilosAperçuToolStripMenuItem";
            this.ApportsParProducteurKilosAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosAperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.ApportsParProducteurKilosAperçuToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosPDFToolStripMenuItem
            // 
            this.ApportsParProducteurKilosPDFToolStripMenuItem.Name = "ApportsParProducteurKilosPDFToolStripMenuItem";
            this.ApportsParProducteurKilosPDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosPDFToolStripMenuItem.Text = "PDF";
            this.ApportsParProducteurKilosPDFToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosPDFToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosExcelToolStripMenuItem
            // 
            this.ApportsParProducteurKilosExcelToolStripMenuItem.Name = "ApportsParProducteurKilosExcelToolStripMenuItem";
            this.ApportsParProducteurKilosExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosExcelToolStripMenuItem.Text = "Excel";
            this.ApportsParProducteurKilosExcelToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosExcelToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosWordToolStripMenuItem
            // 
            this.ApportsParProducteurKilosWordToolStripMenuItem.Name = "ApportsParProducteurKilosWordToolStripMenuItem";
            this.ApportsParProducteurKilosWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosWordToolStripMenuItem.Text = "Word";
            this.ApportsParProducteurKilosWordToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosWordToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosDetailSondageOeToolStripMenuItem
            // 
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem,
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem,
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem,
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem});
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem.Name = "ApportsParProducteurKilosDetailSondageOeToolStripMenuItem";
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem.Text = "Kilos [ sondage °Oe détaillé ]";
            this.ApportsParProducteurKilosDetailSondageOeToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem
            // 
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem.Name = "ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem";
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem.Text = "Aperçu avant impression";
            this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem
            // 
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem.Name = "ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem";
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem.Text = "PDF";
            this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem
            // 
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem.Name = "ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem";
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem.Text = "Excel";
            this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem
            // 
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem.Name = "ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem";
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem.Text = "Word";
            this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosLitresToolStripMenuItem
            // 
            this.ApportsParProducteurKilosLitresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem,
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem,
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem,
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem});
            this.ApportsParProducteurKilosLitresToolStripMenuItem.Name = "ApportsParProducteurKilosLitresToolStripMenuItem";
            this.ApportsParProducteurKilosLitresToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.ApportsParProducteurKilosLitresToolStripMenuItem.Text = "Kilos, Litres [ vin clair et moût de raisin ]";
            this.ApportsParProducteurKilosLitresToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosLitresAperçuToolStripMenuItem
            // 
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem.Name = "ApportsParProducteurKilosLitresAperçuToolStripMenuItem";
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem.Text = "Aperçu avant impression";
            this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosLitresAperçuToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosLitresPDFToolStripMenuItem
            // 
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem.Name = "ApportsParProducteurKilosLitresPDFToolStripMenuItem";
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem.Text = "PDF";
            this.ApportsParProducteurKilosLitresPDFToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosLitresPDFToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosLitresExcelToolStripMenuItem
            // 
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem.Name = "ApportsParProducteurKilosLitresExcelToolStripMenuItem";
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem.Text = "Excel";
            this.ApportsParProducteurKilosLitresExcelToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosLitresExcelToolStripMenuItem_Click);
            // 
            // ApportsParProducteurKilosLitresWordToolStripMenuItem
            // 
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem.Name = "ApportsParProducteurKilosLitresWordToolStripMenuItem";
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem.Text = "Word";
            this.ApportsParProducteurKilosLitresWordToolStripMenuItem.Click += new System.EventHandler(this.ApportsParProducteurKilosLitresWordToolStripMenuItem_Click);
            // 
            // sondageMoyenParToolStripMenuItem
            // 
            this.sondageMoyenParToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sondageMoyenParCepageToolStripMenuItem,
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem});
            this.sondageMoyenParToolStripMenuItem.Name = "sondageMoyenParToolStripMenuItem";
            this.sondageMoyenParToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.sondageMoyenParToolStripMenuItem.Text = "&Sondage moyen par ...";
            // 
            // sondageMoyenParCepageToolStripMenuItem
            // 
            this.sondageMoyenParCepageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sondageMoyenParCepageAperçuToolStripMenuItem,
            this.sondageMoyenParCepagePDFToolStripMenuItem,
            this.sondageMoyenParCepagExcelToolStripMenuItem,
            this.sondageMoyenParCepageWordToolStripMenuItem});
            this.sondageMoyenParCepageToolStripMenuItem.Name = "sondageMoyenParCepageToolStripMenuItem";
            this.sondageMoyenParCepageToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.sondageMoyenParCepageToolStripMenuItem.Text = "Cépage";
            this.sondageMoyenParCepageToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageAperçuToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageAperçuToolStripMenuItem
            // 
            this.sondageMoyenParCepageAperçuToolStripMenuItem.Name = "sondageMoyenParCepageAperçuToolStripMenuItem";
            this.sondageMoyenParCepageAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageAperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.sondageMoyenParCepageAperçuToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageAperçuToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepagePDFToolStripMenuItem
            // 
            this.sondageMoyenParCepagePDFToolStripMenuItem.Name = "sondageMoyenParCepagePDFToolStripMenuItem";
            this.sondageMoyenParCepagePDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepagePDFToolStripMenuItem.Text = "&PDF";
            this.sondageMoyenParCepagePDFToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepagePDFToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepagExcelToolStripMenuItem
            // 
            this.sondageMoyenParCepagExcelToolStripMenuItem.Name = "sondageMoyenParCepagExcelToolStripMenuItem";
            this.sondageMoyenParCepagExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepagExcelToolStripMenuItem.Text = "&Excel";
            this.sondageMoyenParCepagExcelToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepagExcelToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageWordToolStripMenuItem
            // 
            this.sondageMoyenParCepageWordToolStripMenuItem.Name = "sondageMoyenParCepageWordToolStripMenuItem";
            this.sondageMoyenParCepageWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageWordToolStripMenuItem.Text = "&Word";
            this.sondageMoyenParCepageWordToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageWordToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageLieuDeProductionToolStripMenuItem
            // 
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem,
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem,
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem,
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem});
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem.Name = "sondageMoyenParCepageLieuDeProductionToolStripMenuItem";
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem.Text = "Cépage, Lieu de production";
            this.sondageMoyenParCepageLieuDeProductionToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem
            // 
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem.Name = "sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem";
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem.Text = "&Aperçu avant impression";
            this.sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem
            // 
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem.Name = "sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem";
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem.Text = "&PDF";
            this.sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem
            // 
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem.Name = "sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem";
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem.Text = "&Excel";
            this.sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem_Click);
            // 
            // sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem
            // 
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem.Name = "sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem";
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem.Text = "&Word";
            this.sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem.Click += new System.EventHandler(this.SondageMoyenParCepageLieuDeProductionWordToolStripMenuItem_Click);
            // 
            // tablesDesPrixToolStripMenuItem
            // 
            this.tablesDesPrixToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apercutablesDesPrix,
            this.PDFtablesDesPrix,
            this.ExceltablesDesPrix,
            this.WordtablesDesPrix});
            this.tablesDesPrixToolStripMenuItem.Name = "tablesDesPrixToolStripMenuItem";
            this.tablesDesPrixToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.tablesDesPrixToolStripMenuItem.Text = "&Tables des prix (bonus/malus)";
            this.tablesDesPrixToolStripMenuItem.Click += new System.EventHandler(this.TablesDesPrixToolStripMenuItem_Click);
            // 
            // apercutablesDesPrix
            // 
            this.apercutablesDesPrix.Name = "apercutablesDesPrix";
            this.apercutablesDesPrix.Size = new System.Drawing.Size(250, 22);
            this.apercutablesDesPrix.Text = "&Aperçu avant impression";
            this.apercutablesDesPrix.Click += new System.EventHandler(this.ApercutablesDesPrix_Click);
            // 
            // PDFtablesDesPrix
            // 
            this.PDFtablesDesPrix.Name = "PDFtablesDesPrix";
            this.PDFtablesDesPrix.Size = new System.Drawing.Size(250, 22);
            this.PDFtablesDesPrix.Text = "&Portable Document Format (PDF)";
            this.PDFtablesDesPrix.Click += new System.EventHandler(this.PDFtablesDesPrix_Click);
            // 
            // ExceltablesDesPrix
            // 
            this.ExceltablesDesPrix.Name = "ExceltablesDesPrix";
            this.ExceltablesDesPrix.Size = new System.Drawing.Size(250, 22);
            this.ExceltablesDesPrix.Text = "Microsoft &Excel";
            this.ExceltablesDesPrix.Click += new System.EventHandler(this.ExceltablesDesPrix_Click);
            // 
            // WordtablesDesPrix
            // 
            this.WordtablesDesPrix.Name = "WordtablesDesPrix";
            this.WordtablesDesPrix.Size = new System.Drawing.Size(250, 22);
            this.WordtablesDesPrix.Text = "Microsoft &Word";
            this.WordtablesDesPrix.Click += new System.EventHandler(this.WordtablesDesPrix_Click);
            // 
            // facturesFournisseursToolStripMenuItem
            // 
            this.facturesFournisseursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aperçuAvantFactures,
            this.PDFFactureToolStripMenuItem,
            this.xlsFacture,
            this.FacturemicrosoftWordToolStripMenuItem});
            this.facturesFournisseursToolStripMenuItem.Name = "facturesFournisseursToolStripMenuItem";
            this.facturesFournisseursToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.facturesFournisseursToolStripMenuItem.Text = "&Paiements fournisseurs";
            this.facturesFournisseursToolStripMenuItem.Click += new System.EventHandler(this.FacturesFournisseursToolStripMenuItem_Click);
            // 
            // aperçuAvantFactures
            // 
            this.aperçuAvantFactures.Name = "aperçuAvantFactures";
            this.aperçuAvantFactures.Size = new System.Drawing.Size(250, 22);
            this.aperçuAvantFactures.Text = "&Aperçu avant impression";
            this.aperçuAvantFactures.Click += new System.EventHandler(this.AperçuAvantFactures_Click);
            // 
            // PDFFactureToolStripMenuItem
            // 
            this.PDFFactureToolStripMenuItem.Name = "PDFFactureToolStripMenuItem";
            this.PDFFactureToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.PDFFactureToolStripMenuItem.Text = "&Portable Document Format (PDF)";
            this.PDFFactureToolStripMenuItem.Click += new System.EventHandler(this.PDFFactureToolStripMenuItem_Click);
            // 
            // xlsFacture
            // 
            this.xlsFacture.Name = "xlsFacture";
            this.xlsFacture.Size = new System.Drawing.Size(250, 22);
            this.xlsFacture.Text = "Microsoft &Excel";
            this.xlsFacture.Click += new System.EventHandler(this.XlsFacture_Click);
            // 
            // FacturemicrosoftWordToolStripMenuItem
            // 
            this.FacturemicrosoftWordToolStripMenuItem.Name = "FacturemicrosoftWordToolStripMenuItem";
            this.FacturemicrosoftWordToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.FacturemicrosoftWordToolStripMenuItem.Text = "Microsoft &Word";
            this.FacturemicrosoftWordToolStripMenuItem.Click += new System.EventHandler(this.FacturemicrosoftWordToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(293, 6);
            // 
            // déclarationDencavageOCVToolStripMenuItem
            // 
            this.déclarationDencavageOCVToolStripMenuItem.Name = "déclarationDencavageOCVToolStripMenuItem";
            this.déclarationDencavageOCVToolStripMenuItem.Size = new System.Drawing.Size(296, 22);
            this.déclarationDencavageOCVToolStripMenuItem.Text = "Déclaration d\'encavage &OCV";
            this.déclarationDencavageOCVToolStripMenuItem.Click += new System.EventHandler(this.DeclarationDencavageOCVToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aProposToolStripMenuItem,
            this.toolDriverOLEDB,
            this.changerCleToolStrip,
            this.toolTeleAssitance});
            this.toolStripMenuItem1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
            this.toolStripMenuItem1.Text = "&?";
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.aProposToolStripMenuItem.Text = "A &propos...";
            this.aProposToolStripMenuItem.Click += new System.EventHandler(this.AProposToolStripMenuItem_Click);
            // 
            // toolDriverOLEDB
            // 
            this.toolDriverOLEDB.Name = "toolDriverOLEDB";
            this.toolDriverOLEDB.Size = new System.Drawing.Size(235, 22);
            this.toolDriverOLEDB.Text = "&Installer driver OLEDB Foxpro...";
            this.toolDriverOLEDB.Click += new System.EventHandler(this.ToolDriverOLEDB_Click);
            // 
            // changerCleToolStrip
            // 
            this.changerCleToolStrip.Name = "changerCleToolStrip";
            this.changerCleToolStrip.Size = new System.Drawing.Size(235, 22);
            this.changerCleToolStrip.Text = "&Changer de clé d\'activation...";
            this.changerCleToolStrip.Click += new System.EventHandler(this.ChangerCleToolStrip_Click);
            // 
            // toolTeleAssitance
            // 
            this.toolTeleAssitance.Name = "toolTeleAssitance";
            this.toolTeleAssitance.Size = new System.Drawing.Size(235, 22);
            this.toolTeleAssitance.Text = "&Télé-assitance...";
            this.toolTeleAssitance.Click += new System.EventHandler(this.ToolTeleAssitance_Click);
            // 
            // WfrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(680, 86);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WfrmMenu";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changerCleToolStrip;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rapportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem declarationDencavageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apercuDeclarationEncavage;
        private System.Windows.Forms.ToolStripMenuItem pdfDeclarationEncavage;
        private System.Windows.Forms.ToolStripMenuItem xlsDeclarationEncavage;
        private System.Windows.Forms.ToolStripMenuItem AttestationsDeControleStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem apercuAttestationsControleStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem pdfAttestationsControleStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem xlsAttestationsControleStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem docDeclarationEncavage;
        private System.Windows.Forms.ToolStripMenuItem docAttestationsControleStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem droitsProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem droitsProductionAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem droitsProductionPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem droitsProductionExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem droitsProductionWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeAcquitsStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem apercuListeAcquitsStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pdfListeAcquitsStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem xlsListeAcquitsStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem docListeAcquitsStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tablesDesPrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apercutablesDesPrix;
        private System.Windows.Forms.ToolStripMenuItem PDFtablesDesPrix;
        private System.Windows.Forms.ToolStripMenuItem ExceltablesDesPrix;
        private System.Windows.Forms.ToolStripMenuItem WordtablesDesPrix;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem déclarationDencavageOCVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturesFournisseursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aperçuAvantFactures;
        private System.Windows.Forms.ToolStripMenuItem PDFFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xlsFacture;
        private System.Windows.Forms.ToolStripMenuItem FacturemicrosoftWordToolStripMenuItem;
        protected System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem gestionDesCommunesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adressesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CommuneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesLieuxDeProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesDistrictsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RegionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem gestionDesMentionsParticulièresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesAdressesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProprietaireToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesResponsablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesPressoirsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AnToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem CepToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gestionDesDegrésToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem gestionDesCouleursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClasToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem dossierWinBIZToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restaurerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolTeleAssitance;
        private System.Windows.Forms.ToolStripMenuItem changerDeDossierToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolStripGererLesDossiers;
        private System.Windows.Forms.ToolStripMenuItem toolDriverOLEDB;
        private System.Windows.Forms.ToolStripMenuItem GlobalParamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zonesDeTravailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesParcellesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aperçuListeParcellesVisiteVignesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pdfListeParcellesVisiteVignesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xlsListeParcellesVisiteVignesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wordListeParcellesVisiteVignesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParProducteurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParProducteur_AperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParProducteur_PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParProducteur_ExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParProducteur_WordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParCepageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParCepage_AperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParCepage_PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParCepage_ExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParCepage_WordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParLieuDeProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParLieuDeProduction_AperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParLieuDeProduction_PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParLieuDeProduction_ExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParLieuDeProduction_WordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnKilosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnKilos_AperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnKilos_PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnKilos_ExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnKilos_WordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnLitresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnLitres_AperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnLitres_PDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnLitres_ExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendangesLivreesParDateEnLitres_WordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepagePDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepagExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageLieuDeProductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageLieuDeProductionAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageLieuDeProductionPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageLieuDeProductionExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sondageMoyenParCepageLieuDeProductionWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosLitresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosLitresAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosLitresPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosLitresExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosLitresWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosDetailSondageOeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosDetailSondageOeAperçuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosDetailSondageOePDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosDetailSondageOeExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ApportsParProducteurKilosDetailSondageOeWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportAcquitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportPeseesToolStripMenuItem;
    }
}