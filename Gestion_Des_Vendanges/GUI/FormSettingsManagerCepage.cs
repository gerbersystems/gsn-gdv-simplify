﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using log4net;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class FormSettingsManagerCepage : FormSettingsManagerPesees
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(FormSettingsManagerCepage));
        private bool _optionWinBiz;
        private GroupBox _gbModeComptabilisation;
        private ComboBox _cbModeComptabilisation;
        private RadioButton _optSpecifique;
        private ComboBox _cou_idComboBox;
        private TextBox _cep_nomTextBox;
        private RadioButton _optSelonCouleur;
        private TextBox _idTextBox;
        private CEP_CEPAGETableAdapter _cepTable;
        private MCE_MOC_CEPTableAdapter _mceTable;

        public FormSettingsManagerCepage(WfrmSetting form,
                                         TextBox idTextBox,
                                         Control focusControl,
                                         Button btnNew,
                                         Button btnUpdate,
                                         Button btnDelete,
                                         DataGridView dataGridView,
                                         BindingSource bindingSource,
                                         TableAdapterManager tableAdapterManager,
                                         DSPesees dsPesees,
                                         ComboBox cbModeComptabilisation,
                                         GroupBox gbModeComptabilisation,
                                         RadioButton optSelonCouleur,
                                         RadioButton optSpecifique,
                                         CEP_CEPAGETableAdapter cepTable,
                                         bool optionWinBiz,
                                         ComboBox cOU_IDComboBox,
                                         TextBox cEP_NOMTextBox)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dsPesees)
        {
            _idTextBox = idTextBox;
            _optionWinBiz = optionWinBiz;
            _cbModeComptabilisation = cbModeComptabilisation;
            _gbModeComptabilisation = gbModeComptabilisation;
            _optSelonCouleur = optSelonCouleur;
            _optSpecifique = optSpecifique;
            _cou_idComboBox = cOU_IDComboBox;
            _cep_nomTextBox = cEP_NOMTextBox;
            DataGridView.SelectionChanged += DataGridSelectionChanged;
            _cepTable = cepTable;
            _mceTable = ConnectionManager.Instance.CreateGvTableAdapter<MCE_MOC_CEPTableAdapter>();
            DataGridSelectionChanged(null, null);
        }

        private void DataGridSelectionChanged(object sender, EventArgs e)
        {
            int cepId = Convert.ToInt32(_idTextBox.Text);
            try
            {
                _cbModeComptabilisation.SelectedValue = _mceTable.GetMocId(cepId, Utilities.IdAnneeCourante);
                _optSpecifique.Checked = true;
            }
            catch (ArgumentNullException)
            {
                _optSelonCouleur.Checked = true;
            }
        }

        private void OptSelectionChanged(object sender, EventArgs e)
        {
            if (_optSpecifique.Checked && _gbModeComptabilisation.Enabled)
                _cbModeComptabilisation.Enabled = true;
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            if (!Valide()) return;

            base.BtnOk_Click(sender, e);
            int cepId = Convert.ToInt32(_idTextBox.Text);
            int? mceId = _mceTable.GetMceId(cepId, Utilities.IdAnneeCourante);
            int? mocId = (int?)_cbModeComptabilisation.SelectedValue;
            if (_optSelonCouleur.Checked || mocId == null)
            {
                if (mceId != null) _mceTable.DeleteByMce((int)mceId);
            }
            else
            {
                if (mceId != null)
                    _mceTable.UpdateMoc((int)mocId, (int)mceId);
                else
                    _mceTable.Insert((int)mocId, (int)cepId);
            }
        }

        private bool Valide()
        {
            if (string.IsNullOrWhiteSpace(_cep_nomTextBox.Text)) return InformAndLog("nom");
            if (_cou_idComboBox.SelectedIndex < 0) return InformAndLog("couleur");

            return true;
        }

        private bool InformAndLog(string who)
        {
            string MESSAGE = $"Le champ {who} n'est pas valide.";
            MessageBox.Show(MESSAGE,
                            "Erreur lors de la création d'un nouveau cépage",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
            _log.Info(MESSAGE);

            return false;
        }

        protected override void ChangeEditMode(bool editMode)
        {
            base.ChangeEditMode(editMode);

            if (!_optionWinBiz && _gbModeComptabilisation != null)
                _gbModeComptabilisation.Enabled = false;
        }
    }
}