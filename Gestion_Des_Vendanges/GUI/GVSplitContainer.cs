﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class GVSplitContainer : SplitContainer
    {
        public GVSplitContainer()
        {
            InitializeComponent();
            SplitterMoved += ctrResized;
        }

        private void ctrResized(object sender, EventArgs e)
        {
            unselectComboBox(this.Panel1.Controls);
            unselectComboBox(this.Panel2.Controls);
        }

        private void unselectComboBox(Control.ControlCollection controls)
        {
            foreach (System.Windows.Forms.Control ctr in controls)
            {
                if (ctr is System.Windows.Forms.ComboBox)
                {
                    ((System.Windows.Forms.ComboBox)ctr).SelectionLength = 0;
                }
                else
                {
                    unselectComboBox(ctr.Controls);
                }
            }
        }
    }
}