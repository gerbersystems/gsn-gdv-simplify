﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSaisieEntetePesee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label lblCepage;
            this.aCQ_IDLabel = new System.Windows.Forms.Label();
            this.pEE_GRANDCRULabel = new System.Windows.Forms.Label();
            this.lIE_IDLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pEE_IDTextBox = new System.Windows.Forms.TextBox();
            this.pEE_PESEE_ENTETEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.rES_IDComboBox = new System.Windows.Forms.ComboBox();
            this.ResponsablesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ResponsablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rESNOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEE_DATEDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.pEE_LIEUTextBox = new System.Windows.Forms.TextBox();
            this.btnSelectionAcquit = new System.Windows.Forms.Button();
            this.aCQ_IDComboBox = new System.Windows.Forms.ComboBox();
            this.aCQACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEE_PESEE_ENTETETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.pED_PESEE_DETAILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pED_PESEE_DETAILTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter();
            this.pED_PESEE_DETAILDataGridView = new System.Windows.Forms.DataGridView();
            this.PED_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_PEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEP_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PED_LOT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_SONDAGE_OE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_QUANTITE_KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_QUANTITE_LITRES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_SONDAGE_BRIKS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.btnModifierPesee = new System.Windows.Forms.Button();
            this.btnDeletePesee = new System.Windows.Forms.Button();
            this.btnAddPesee = new System.Windows.Forms.Button();
            this.btnValiderPesee = new System.Windows.Forms.Button();
            this.tootTipCru = new System.Windows.Forms.ToolTip(this.components);
            this.limitationParLieuGroupBox = new System.Windows.Forms.GroupBox();
            this.lblSoldeParLieu = new System.Windows.Forms.Label();
            this.txtSoldeParLieu = new System.Windows.Forms.TextBox();
            this.txtLitresAdmisApp = new System.Windows.Forms.TextBox();
            this.txtLitresSaisisApp = new System.Windows.Forms.TextBox();
            this.lblLimitParLieuAdmis = new System.Windows.Forms.Label();
            this.lblLimitParLieuSaisis = new System.Windows.Forms.Label();
            this.pEE_GRANDCRUCheckBox = new System.Windows.Forms.CheckBox();
            this.lIE_IDComboBox = new System.Windows.Forms.ComboBox();
            this.contextLieuDeProduction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolLieuProduction = new System.Windows.Forms.ToolStripMenuItem();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aUT_IDComboBox = new System.Windows.Forms.ComboBox();
            this.contextNomLocal = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolNomLocal = new System.Windows.Forms.ToolStripMenuItem();
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.limitationParAcquitGroupBox = new System.Windows.Forms.GroupBox();
            this.lblSoldeParAcquit = new System.Windows.Forms.Label();
            this.txtSoldeParAcquit = new System.Windows.Forms.TextBox();
            this.lblLimitParAcquitSaisis = new System.Windows.Forms.Label();
            this.lblLimitParAcquitAdmis = new System.Windows.Forms.Label();
            this.txtLitresAdmisAcq = new System.Windows.Forms.TextBox();
            this.txtLitresSaisisAcq = new System.Windows.Forms.TextBox();
            this.rES_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_NOMCOMPLETTableAdapter();
            this.cepageComboBox = new System.Windows.Forms.ComboBox();
            this.transfertButton = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.pEC_PED_COMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEC_PED_COMTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEC_PED_COMTableAdapter();
            this.peL_PED_LIETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEL_PED_LIETableAdapter();
            this.txtRemarques = new System.Windows.Forms.TextBox();
            this.LblRemarques = new System.Windows.Forms.Label();
            this.LblMecanique = new System.Windows.Forms.Label();
            this.cbxVendMec = new System.Windows.Forms.CheckBox();
            this.txtScaleNb = new System.Windows.Forms.TextBox();
            this.LblBalance = new System.Windows.Forms.Label();
            this.txtRefractometerNb = new System.Windows.Forms.TextBox();
            this.lblRefractometre = new System.Windows.Forms.Label();
            this.contact_NomCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.Contact_NomCompletTableAdapter();
            this.lblOwnerAndProducerNames = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            lblCepage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.ResponsablesContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rESNOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            this.limitationParLieuGroupBox.SuspendLayout();
            this.contextLieuDeProduction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            this.contextNomLocal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            this.limitationParAcquitGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pEC_PED_COMBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label1.Location = new System.Drawing.Point(14, 213);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(68, 15);
            label1.TabIndex = 57;
            label1.Text = "Nom local :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label2.Location = new System.Drawing.Point(14, 243);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(82, 15);
            label2.TabIndex = 58;
            label2.Text = "Responsable :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label3.Location = new System.Drawing.Point(14, 100);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(93, 15);
            label3.TabIndex = 59;
            label3.Text = "Date de pesée :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label4.Location = new System.Drawing.Point(14, 126);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(102, 15);
            label4.TabIndex = 60;
            label4.Text = "Lieu de livraison :";
            // 
            // lblCepage
            // 
            lblCepage.AutoSize = true;
            lblCepage.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblCepage.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblCepage.Location = new System.Drawing.Point(14, 155);
            lblCepage.Name = "lblCepage";
            lblCepage.Size = new System.Drawing.Size(54, 15);
            lblCepage.TabIndex = 81;
            lblCepage.Text = "Cepage :";
            // 
            // aCQ_IDLabel
            // 
            this.aCQ_IDLabel.AutoSize = true;
            this.aCQ_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aCQ_IDLabel.Location = new System.Drawing.Point(14, 49);
            this.aCQ_IDLabel.Name = "aCQ_IDLabel";
            this.aCQ_IDLabel.Size = new System.Drawing.Size(66, 15);
            this.aCQ_IDLabel.TabIndex = 61;
            this.aCQ_IDLabel.Text = "N° Acquit :";
            // 
            // pEE_GRANDCRULabel
            // 
            this.pEE_GRANDCRULabel.AutoSize = true;
            this.pEE_GRANDCRULabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.pEE_GRANDCRULabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.pEE_GRANDCRULabel.Location = new System.Drawing.Point(14, 75);
            this.pEE_GRANDCRULabel.Name = "pEE_GRANDCRULabel";
            this.pEE_GRANDCRULabel.Size = new System.Drawing.Size(68, 15);
            this.pEE_GRANDCRULabel.TabIndex = 77;
            this.pEE_GRANDCRULabel.Text = "Grand cru :";
            // 
            // lIE_IDLabel
            // 
            this.lIE_IDLabel.AutoSize = true;
            this.lIE_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lIE_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lIE_IDLabel.Location = new System.Drawing.Point(14, 184);
            this.lIE_IDLabel.Name = "lIE_IDLabel";
            this.lIE_IDLabel.Size = new System.Drawing.Size(117, 15);
            this.lIE_IDLabel.TabIndex = 79;
            this.lIE_IDLabel.Text = "Lieu de production :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label8.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label8.Location = new System.Drawing.Point(12, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 21);
            this.label8.TabIndex = 39;
            this.label8.Text = "Attestation de contrôle";
            // 
            // pEE_IDTextBox
            // 
            this.pEE_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pEE_PESEE_ENTETEBindingSource, "PEE_ID", true));
            this.pEE_IDTextBox.HideSelection = false;
            this.pEE_IDTextBox.Location = new System.Drawing.Point(372, -2);
            this.pEE_IDTextBox.Name = "pEE_IDTextBox";
            this.pEE_IDTextBox.Size = new System.Drawing.Size(200, 23);
            this.pEE_IDTextBox.TabIndex = 43;
            this.pEE_IDTextBox.TabStop = false;
            this.pEE_IDTextBox.Text = "PEE_ID";
            // 
            // pEE_PESEE_ENTETEBindingSource
            // 
            this.pEE_PESEE_ENTETEBindingSource.DataMember = "PEE_PESEE_ENTETE";
            this.pEE_PESEE_ENTETEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rES_IDComboBox
            // 
            this.rES_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.rES_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.rES_IDComboBox.ContextMenuStrip = this.ResponsablesContextMenuStrip;
            this.rES_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pEE_PESEE_ENTETEBindingSource, "RES_ID", true));
            this.rES_IDComboBox.DataSource = this.rESNOMCOMPLETBindingSource;
            this.rES_IDComboBox.DisplayMember = "NOMCOMPLET";
            this.rES_IDComboBox.FormattingEnabled = true;
            this.rES_IDComboBox.Location = new System.Drawing.Point(148, 239);
            this.rES_IDComboBox.Name = "rES_IDComboBox";
            this.rES_IDComboBox.Size = new System.Drawing.Size(451, 23);
            this.rES_IDComboBox.TabIndex = 80;
            this.rES_IDComboBox.ValueMember = "RES_ID";
            // 
            // ResponsablesContextMenuStrip
            // 
            this.ResponsablesContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ResponsablesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResponsablesToolStripMenuItem});
            this.ResponsablesContextMenuStrip.Name = "ResponsablesContextMenuStrip";
            this.ResponsablesContextMenuStrip.Size = new System.Drawing.Size(222, 26);
            // 
            // ResponsablesToolStripMenuItem
            // 
            this.ResponsablesToolStripMenuItem.Name = "ResponsablesToolStripMenuItem";
            this.ResponsablesToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.ResponsablesToolStripMenuItem.Text = "Accéder aux responsables ...";
            this.ResponsablesToolStripMenuItem.Click += new System.EventHandler(this.toolResponsable_Click);
            // 
            // rESNOMCOMPLETBindingSource
            // 
            this.rESNOMCOMPLETBindingSource.DataMember = "RES_NOMCOMPLET";
            this.rESNOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // pEE_DATEDateTimePicker
            // 
            this.pEE_DATEDateTimePicker.CustomFormat = "dd.MM.yyyy";
            this.pEE_DATEDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pEE_PESEE_ENTETEBindingSource, "PEE_DATE", true));
            this.pEE_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.pEE_DATEDateTimePicker.Location = new System.Drawing.Point(148, 94);
            this.pEE_DATEDateTimePicker.Name = "pEE_DATEDateTimePicker";
            this.pEE_DATEDateTimePicker.Size = new System.Drawing.Size(108, 23);
            this.pEE_DATEDateTimePicker.TabIndex = 30;
            // 
            // pEE_LIEUTextBox
            // 
            this.pEE_LIEUTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pEE_PESEE_ENTETEBindingSource, "PEE_LIEU", true));
            this.pEE_LIEUTextBox.Location = new System.Drawing.Point(148, 123);
            this.pEE_LIEUTextBox.Name = "pEE_LIEUTextBox";
            this.pEE_LIEUTextBox.Size = new System.Drawing.Size(451, 23);
            this.pEE_LIEUTextBox.TabIndex = 40;
            // 
            // btnSelectionAcquit
            // 
            this.btnSelectionAcquit.AutoSize = true;
            this.btnSelectionAcquit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSelectionAcquit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSelectionAcquit.Location = new System.Drawing.Point(328, 46);
            this.btnSelectionAcquit.Name = "btnSelectionAcquit";
            this.btnSelectionAcquit.Size = new System.Drawing.Size(65, 25);
            this.btnSelectionAcquit.TabIndex = 10;
            this.btnSelectionAcquit.Text = "Selection";
            this.btnSelectionAcquit.UseVisualStyleBackColor = false;
            this.btnSelectionAcquit.Click += new System.EventHandler(this.btnSelectionAcquit_Click);
            // 
            // aCQ_IDComboBox
            // 
            this.aCQ_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pEE_PESEE_ENTETEBindingSource, "ACQ_ID", true));
            this.aCQ_IDComboBox.DataSource = this.aCQACQUITBindingSource;
            this.aCQ_IDComboBox.DisplayMember = "ACQ_NUMERO";
            this.aCQ_IDComboBox.FormattingEnabled = true;
            this.aCQ_IDComboBox.Location = new System.Drawing.Point(148, 46);
            this.aCQ_IDComboBox.Name = "aCQ_IDComboBox";
            this.aCQ_IDComboBox.Size = new System.Drawing.Size(174, 23);
            this.aCQ_IDComboBox.TabIndex = 0;
            this.aCQ_IDComboBox.ValueMember = "ACQ_ID";
            this.aCQ_IDComboBox.SelectedIndexChanged += new System.EventHandler(this.aCQ_IDComboBox_SelectedIndexChanged);
            // 
            // aCQACQUITBindingSource
            // 
            this.aCQACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // pEE_PESEE_ENTETETableAdapter
            // 
            this.pEE_PESEE_ENTETETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = this.pEE_PESEE_ENTETETableAdapter;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // pED_PESEE_DETAILBindingSource
            // 
            this.pED_PESEE_DETAILBindingSource.DataMember = "PED_PESEE_DETAIL";
            this.pED_PESEE_DETAILBindingSource.DataSource = this.dSPesees;
            // 
            // pED_PESEE_DETAILTableAdapter
            // 
            this.pED_PESEE_DETAILTableAdapter.ClearBeforeFill = true;
            // 
            // pED_PESEE_DETAILDataGridView
            // 
            this.pED_PESEE_DETAILDataGridView.AllowUserToAddRows = false;
            this.pED_PESEE_DETAILDataGridView.AllowUserToDeleteRows = false;
            this.pED_PESEE_DETAILDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pED_PESEE_DETAILDataGridView.AutoGenerateColumns = false;
            this.pED_PESEE_DETAILDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pED_PESEE_DETAILDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pED_PESEE_DETAILDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PED_ID,
            this.PED_PEE_ID,
            this.CEP_ID,
            this.PED_LOT,
            this.PED_SONDAGE_OE,
            this.PED_QUANTITE_KG,
            this.PED_QUANTITE_LITRES,
            this.PED_SONDAGE_BRIKS});
            this.pED_PESEE_DETAILDataGridView.DataSource = this.pED_PESEE_DETAILBindingSource;
            this.pED_PESEE_DETAILDataGridView.Location = new System.Drawing.Point(16, 346);
            this.pED_PESEE_DETAILDataGridView.MultiSelect = false;
            this.pED_PESEE_DETAILDataGridView.Name = "pED_PESEE_DETAILDataGridView";
            this.pED_PESEE_DETAILDataGridView.ReadOnly = true;
            this.pED_PESEE_DETAILDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.pED_PESEE_DETAILDataGridView.Size = new System.Drawing.Size(1024, 329);
            this.pED_PESEE_DETAILDataGridView.TabIndex = 64;
            this.pED_PESEE_DETAILDataGridView.TabStop = false;
            this.pED_PESEE_DETAILDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.pED_PESEE_DETAILDataGridView_ColumnHeaderMouseClick);
            // 
            // PED_ID
            // 
            this.PED_ID.DataPropertyName = "PED_ID";
            this.PED_ID.HeaderText = "PED_ID";
            this.PED_ID.Name = "PED_ID";
            this.PED_ID.ReadOnly = true;
            this.PED_ID.Visible = false;
            // 
            // PED_PEE_ID
            // 
            this.PED_PEE_ID.DataPropertyName = "PEE_ID";
            this.PED_PEE_ID.HeaderText = "PEE_ID";
            this.PED_PEE_ID.Name = "PED_PEE_ID";
            this.PED_PEE_ID.ReadOnly = true;
            this.PED_PEE_ID.Visible = false;
            // 
            // CEP_ID
            // 
            this.CEP_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.DataSource = this.cEPCEPAGEBindingSource;
            this.CEP_ID.DisplayMember = "CEP_NOM";
            this.CEP_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CEP_ID.HeaderText = "Cépage";
            this.CEP_ID.MinimumWidth = 100;
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CEP_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.CEP_ID.ValueMember = "CEP_ID";
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // PED_LOT
            // 
            this.PED_LOT.DataPropertyName = "PED_LOT";
            this.PED_LOT.HeaderText = "Lot";
            this.PED_LOT.Name = "PED_LOT";
            this.PED_LOT.ReadOnly = true;
            this.PED_LOT.Width = 80;
            // 
            // PED_SONDAGE_OE
            // 
            this.PED_SONDAGE_OE.DataPropertyName = "PED_SONDAGE_OE";
            this.PED_SONDAGE_OE.HeaderText = "Sondage OE";
            this.PED_SONDAGE_OE.Name = "PED_SONDAGE_OE";
            this.PED_SONDAGE_OE.ReadOnly = true;
            // 
            // PED_QUANTITE_KG
            // 
            this.PED_QUANTITE_KG.DataPropertyName = "PED_QUANTITE_KG";
            this.PED_QUANTITE_KG.HeaderText = "Kg";
            this.PED_QUANTITE_KG.Name = "PED_QUANTITE_KG";
            this.PED_QUANTITE_KG.ReadOnly = true;
            // 
            // PED_QUANTITE_LITRES
            // 
            this.PED_QUANTITE_LITRES.DataPropertyName = "PED_QUANTITE_LITRES";
            this.PED_QUANTITE_LITRES.HeaderText = "Litres";
            this.PED_QUANTITE_LITRES.Name = "PED_QUANTITE_LITRES";
            this.PED_QUANTITE_LITRES.ReadOnly = true;
            // 
            // PED_SONDAGE_BRIKS
            // 
            this.PED_SONDAGE_BRIKS.DataPropertyName = "PED_SONDAGE_BRIKS";
            this.PED_SONDAGE_BRIKS.HeaderText = "Sondage Briks";
            this.PED_SONDAGE_BRIKS.Name = "PED_SONDAGE_BRIKS";
            this.PED_SONDAGE_BRIKS.ReadOnly = true;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // btnModifierPesee
            // 
            this.btnModifierPesee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModifierPesee.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnModifierPesee.Location = new System.Drawing.Point(1044, 381);
            this.btnModifierPesee.Name = "btnModifierPesee";
            this.btnModifierPesee.Size = new System.Drawing.Size(106, 30);
            this.btnModifierPesee.TabIndex = 160;
            this.btnModifierPesee.TabStop = false;
            this.btnModifierPesee.Text = "Modifier pesée";
            this.btnModifierPesee.UseVisualStyleBackColor = false;
            this.btnModifierPesee.Click += new System.EventHandler(this.btnModifierPesee_Click);
            // 
            // btnDeletePesee
            // 
            this.btnDeletePesee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePesee.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDeletePesee.Location = new System.Drawing.Point(1044, 417);
            this.btnDeletePesee.Name = "btnDeletePesee";
            this.btnDeletePesee.Size = new System.Drawing.Size(105, 30);
            this.btnDeletePesee.TabIndex = 180;
            this.btnDeletePesee.TabStop = false;
            this.btnDeletePesee.Text = "Supprimer pesée";
            this.btnDeletePesee.UseVisualStyleBackColor = false;
            this.btnDeletePesee.Click += new System.EventHandler(this.btnDeletePesee_Click);
            // 
            // btnAddPesee
            // 
            this.btnAddPesee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPesee.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddPesee.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnAddPesee.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnAddPesee.Location = new System.Drawing.Point(1044, 346);
            this.btnAddPesee.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddPesee.Name = "btnAddPesee";
            this.btnAddPesee.Size = new System.Drawing.Size(106, 30);
            this.btnAddPesee.TabIndex = 130;
            this.btnAddPesee.Text = "Ajouter pesée";
            this.btnAddPesee.UseVisualStyleBackColor = false;
            this.btnAddPesee.Click += new System.EventHandler(this.btnAddPesee_Click);
            // 
            // btnValiderPesee
            // 
            this.btnValiderPesee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValiderPesee.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnValiderPesee.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnValiderPesee.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnValiderPesee.Location = new System.Drawing.Point(1045, 624);
            this.btnValiderPesee.Margin = new System.Windows.Forms.Padding(2);
            this.btnValiderPesee.Name = "btnValiderPesee";
            this.btnValiderPesee.Size = new System.Drawing.Size(105, 50);
            this.btnValiderPesee.TabIndex = 150;
            this.btnValiderPesee.Text = "Valider l\'attestation";
            this.btnValiderPesee.UseVisualStyleBackColor = false;
            this.btnValiderPesee.Click += new System.EventHandler(this.btnValiderPesee_Click);
            // 
            // limitationParLieuGroupBox
            // 
            this.limitationParLieuGroupBox.Controls.Add(this.lblSoldeParLieu);
            this.limitationParLieuGroupBox.Controls.Add(this.txtSoldeParLieu);
            this.limitationParLieuGroupBox.Controls.Add(this.txtLitresAdmisApp);
            this.limitationParLieuGroupBox.Controls.Add(this.txtLitresSaisisApp);
            this.limitationParLieuGroupBox.Controls.Add(this.lblLimitParLieuAdmis);
            this.limitationParLieuGroupBox.Controls.Add(this.lblLimitParLieuSaisis);
            this.limitationParLieuGroupBox.Location = new System.Drawing.Point(605, 49);
            this.limitationParLieuGroupBox.Name = "limitationParLieuGroupBox";
            this.limitationParLieuGroupBox.Size = new System.Drawing.Size(436, 107);
            this.limitationParLieuGroupBox.TabIndex = 75;
            this.limitationParLieuGroupBox.TabStop = false;
            this.limitationParLieuGroupBox.Text = "Limitation par lieu de production";
            // 
            // lblSoldeParLieu
            // 
            this.lblSoldeParLieu.AutoSize = true;
            this.lblSoldeParLieu.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblSoldeParLieu.Location = new System.Drawing.Point(19, 76);
            this.lblSoldeParLieu.Name = "lblSoldeParLieu";
            this.lblSoldeParLieu.Size = new System.Drawing.Size(110, 16);
            this.lblSoldeParLieu.TabIndex = 85;
            this.lblSoldeParLieu.Text = "lblSoldeParLieu";
            // 
            // txtSoldeParLieu
            // 
            this.txtSoldeParLieu.BackColor = System.Drawing.Color.White;
            this.txtSoldeParLieu.Location = new System.Drawing.Point(225, 76);
            this.txtSoldeParLieu.Name = "txtSoldeParLieu";
            this.txtSoldeParLieu.ReadOnly = true;
            this.txtSoldeParLieu.Size = new System.Drawing.Size(172, 23);
            this.txtSoldeParLieu.TabIndex = 84;
            this.txtSoldeParLieu.TabStop = false;
            // 
            // txtLitresAdmisApp
            // 
            this.txtLitresAdmisApp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLitresAdmisApp.Location = new System.Drawing.Point(225, 26);
            this.txtLitresAdmisApp.Name = "txtLitresAdmisApp";
            this.txtLitresAdmisApp.ReadOnly = true;
            this.txtLitresAdmisApp.Size = new System.Drawing.Size(172, 23);
            this.txtLitresAdmisApp.TabIndex = 70;
            this.txtLitresAdmisApp.TabStop = false;
            // 
            // txtLitresSaisisApp
            // 
            this.txtLitresSaisisApp.BackColor = System.Drawing.Color.White;
            this.txtLitresSaisisApp.Location = new System.Drawing.Point(225, 51);
            this.txtLitresSaisisApp.Name = "txtLitresSaisisApp";
            this.txtLitresSaisisApp.ReadOnly = true;
            this.txtLitresSaisisApp.Size = new System.Drawing.Size(172, 23);
            this.txtLitresSaisisApp.TabIndex = 72;
            this.txtLitresSaisisApp.TabStop = false;
            // 
            // lblLimitParLieuAdmis
            // 
            this.lblLimitParLieuAdmis.AutoSize = true;
            this.lblLimitParLieuAdmis.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblLimitParLieuAdmis.Location = new System.Drawing.Point(19, 27);
            this.lblLimitParLieuAdmis.Name = "lblLimitParLieuAdmis";
            this.lblLimitParLieuAdmis.Size = new System.Drawing.Size(147, 16);
            this.lblLimitParLieuAdmis.TabIndex = 80;
            this.lblLimitParLieuAdmis.Text = "lblLimitParLieuAdmis";
            // 
            // lblLimitParLieuSaisis
            // 
            this.lblLimitParLieuSaisis.AutoSize = true;
            this.lblLimitParLieuSaisis.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblLimitParLieuSaisis.Location = new System.Drawing.Point(19, 51);
            this.lblLimitParLieuSaisis.Name = "lblLimitParLieuSaisis";
            this.lblLimitParLieuSaisis.Size = new System.Drawing.Size(143, 16);
            this.lblLimitParLieuSaisis.TabIndex = 80;
            this.lblLimitParLieuSaisis.Text = "lblLimitParLieuSaisis";
            // 
            // pEE_GRANDCRUCheckBox
            // 
            this.pEE_GRANDCRUCheckBox.AutoSize = true;
            this.pEE_GRANDCRUCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.pEE_PESEE_ENTETEBindingSource, "PEE_GRANDCRU", true));
            this.pEE_GRANDCRUCheckBox.Location = new System.Drawing.Point(148, 75);
            this.pEE_GRANDCRUCheckBox.Name = "pEE_GRANDCRUCheckBox";
            this.pEE_GRANDCRUCheckBox.Size = new System.Drawing.Size(15, 14);
            this.pEE_GRANDCRUCheckBox.TabIndex = 20;
            this.pEE_GRANDCRUCheckBox.UseVisualStyleBackColor = true;
            // 
            // lIE_IDComboBox
            // 
            this.lIE_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lIE_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lIE_IDComboBox.ContextMenuStrip = this.contextLieuDeProduction;
            this.lIE_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pEE_PESEE_ENTETEBindingSource, "LIE_ID", true));
            this.lIE_IDComboBox.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.lIE_IDComboBox.DisplayMember = "LIE_NOM";
            this.lIE_IDComboBox.Enabled = false;
            this.lIE_IDComboBox.FormattingEnabled = true;
            this.lIE_IDComboBox.Location = new System.Drawing.Point(148, 181);
            this.lIE_IDComboBox.Name = "lIE_IDComboBox";
            this.lIE_IDComboBox.Size = new System.Drawing.Size(451, 23);
            this.lIE_IDComboBox.TabIndex = 60;
            this.lIE_IDComboBox.ValueMember = "LIE_ID";
            this.lIE_IDComboBox.SelectedIndexChanged += new System.EventHandler(this.lIE_IDComboBox_SelectedIndexChanged);
            // 
            // contextLieuDeProduction
            // 
            this.contextLieuDeProduction.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextLieuDeProduction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLieuProduction});
            this.contextLieuDeProduction.Name = "ResponsablesContextMenuStrip";
            this.contextLieuDeProduction.Size = new System.Drawing.Size(253, 26);
            // 
            // toolLieuProduction
            // 
            this.toolLieuProduction.Name = "toolLieuProduction";
            this.toolLieuProduction.Size = new System.Drawing.Size(252, 22);
            this.toolLieuProduction.Text = "Accéder aux lieux de production...";
            this.toolLieuProduction.Click += new System.EventHandler(this.toolLieuProduction_Click);
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // aUT_IDComboBox
            // 
            this.aUT_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.aUT_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aUT_IDComboBox.ContextMenuStrip = this.contextNomLocal;
            this.aUT_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pEE_PESEE_ENTETEBindingSource, "AUT_ID", true));
            this.aUT_IDComboBox.DataSource = this.aUTAUTREMENTIONBindingSource;
            this.aUT_IDComboBox.DisplayMember = "AUT_NOM";
            this.aUT_IDComboBox.Enabled = false;
            this.aUT_IDComboBox.FormattingEnabled = true;
            this.aUT_IDComboBox.Location = new System.Drawing.Point(148, 210);
            this.aUT_IDComboBox.Name = "aUT_IDComboBox";
            this.aUT_IDComboBox.Size = new System.Drawing.Size(451, 23);
            this.aUT_IDComboBox.TabIndex = 70;
            this.aUT_IDComboBox.ValueMember = "AUT_ID";
            // 
            // contextNomLocal
            // 
            this.contextNomLocal.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextNomLocal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNomLocal});
            this.contextNomLocal.Name = "ResponsablesContextMenuStrip";
            this.contextNomLocal.Size = new System.Drawing.Size(268, 26);
            // 
            // toolNomLocal
            // 
            this.toolNomLocal.Name = "toolNomLocal";
            this.toolNomLocal.Size = new System.Drawing.Size(267, 22);
            this.toolNomLocal.Text = "Accéder aux mentions particulières...";
            this.toolNomLocal.Click += new System.EventHandler(this.toolNomLocal_Click);
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // limitationParAcquitGroupBox
            // 
            this.limitationParAcquitGroupBox.Controls.Add(this.lblSoldeParAcquit);
            this.limitationParAcquitGroupBox.Controls.Add(this.txtSoldeParAcquit);
            this.limitationParAcquitGroupBox.Controls.Add(this.lblLimitParAcquitSaisis);
            this.limitationParAcquitGroupBox.Controls.Add(this.lblLimitParAcquitAdmis);
            this.limitationParAcquitGroupBox.Controls.Add(this.txtLitresAdmisAcq);
            this.limitationParAcquitGroupBox.Controls.Add(this.txtLitresSaisisAcq);
            this.limitationParAcquitGroupBox.Location = new System.Drawing.Point(605, 162);
            this.limitationParAcquitGroupBox.Name = "limitationParAcquitGroupBox";
            this.limitationParAcquitGroupBox.Size = new System.Drawing.Size(436, 105);
            this.limitationParAcquitGroupBox.TabIndex = 76;
            this.limitationParAcquitGroupBox.TabStop = false;
            this.limitationParAcquitGroupBox.Text = "Limitation par acquit";
            // 
            // lblSoldeParAcquit
            // 
            this.lblSoldeParAcquit.AutoSize = true;
            this.lblSoldeParAcquit.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblSoldeParAcquit.Location = new System.Drawing.Point(19, 76);
            this.lblSoldeParAcquit.Name = "lblSoldeParAcquit";
            this.lblSoldeParAcquit.Size = new System.Drawing.Size(125, 16);
            this.lblSoldeParAcquit.TabIndex = 83;
            this.lblSoldeParAcquit.Text = "lblSoldeParAcquit";
            // 
            // txtSoldeParAcquit
            // 
            this.txtSoldeParAcquit.BackColor = System.Drawing.Color.White;
            this.txtSoldeParAcquit.Location = new System.Drawing.Point(225, 76);
            this.txtSoldeParAcquit.Name = "txtSoldeParAcquit";
            this.txtSoldeParAcquit.ReadOnly = true;
            this.txtSoldeParAcquit.Size = new System.Drawing.Size(172, 23);
            this.txtSoldeParAcquit.TabIndex = 82;
            this.txtSoldeParAcquit.TabStop = false;
            // 
            // lblLimitParAcquitSaisis
            // 
            this.lblLimitParAcquitSaisis.AutoSize = true;
            this.lblLimitParAcquitSaisis.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblLimitParAcquitSaisis.Location = new System.Drawing.Point(19, 51);
            this.lblLimitParAcquitSaisis.Name = "lblLimitParAcquitSaisis";
            this.lblLimitParAcquitSaisis.Size = new System.Drawing.Size(158, 16);
            this.lblLimitParAcquitSaisis.TabIndex = 81;
            this.lblLimitParAcquitSaisis.Text = "lblLimitParAcquitSaisis";
            // 
            // lblLimitParAcquitAdmis
            // 
            this.lblLimitParAcquitAdmis.AutoSize = true;
            this.lblLimitParAcquitAdmis.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.lblLimitParAcquitAdmis.Location = new System.Drawing.Point(19, 26);
            this.lblLimitParAcquitAdmis.Name = "lblLimitParAcquitAdmis";
            this.lblLimitParAcquitAdmis.Size = new System.Drawing.Size(162, 16);
            this.lblLimitParAcquitAdmis.TabIndex = 80;
            this.lblLimitParAcquitAdmis.Text = "lblLimitParAcquitAdmis";
            // 
            // txtLitresAdmisAcq
            // 
            this.txtLitresAdmisAcq.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLitresAdmisAcq.Location = new System.Drawing.Point(225, 26);
            this.txtLitresAdmisAcq.Name = "txtLitresAdmisAcq";
            this.txtLitresAdmisAcq.ReadOnly = true;
            this.txtLitresAdmisAcq.Size = new System.Drawing.Size(172, 23);
            this.txtLitresAdmisAcq.TabIndex = 70;
            this.txtLitresAdmisAcq.TabStop = false;
            // 
            // txtLitresSaisisAcq
            // 
            this.txtLitresSaisisAcq.BackColor = System.Drawing.Color.White;
            this.txtLitresSaisisAcq.Location = new System.Drawing.Point(225, 51);
            this.txtLitresSaisisAcq.Name = "txtLitresSaisisAcq";
            this.txtLitresSaisisAcq.ReadOnly = true;
            this.txtLitresSaisisAcq.Size = new System.Drawing.Size(172, 23);
            this.txtLitresSaisisAcq.TabIndex = 72;
            this.txtLitresSaisisAcq.TabStop = false;
            // 
            // rES_NOMCOMPLETTableAdapter
            // 
            this.rES_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // cepageComboBox
            // 
            this.cepageComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cepageComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cepageComboBox.ContextMenuStrip = this.ResponsablesContextMenuStrip;
            this.cepageComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pED_PESEE_DETAILBindingSource, "CEP_ID", true));
            this.cepageComboBox.DataSource = this.cEPCEPAGEBindingSource;
            this.cepageComboBox.DisplayMember = "CEP_NOM";
            this.cepageComboBox.FormattingEnabled = true;
            this.cepageComboBox.Location = new System.Drawing.Point(148, 152);
            this.cepageComboBox.Name = "cepageComboBox";
            this.cepageComboBox.Size = new System.Drawing.Size(451, 23);
            this.cepageComboBox.TabIndex = 50;
            this.cepageComboBox.ValueMember = "CEP_ID";
            this.cepageComboBox.SelectedValueChanged += new System.EventHandler(this.cepageComboBox_SelectedValueChanged);
            // 
            // transfertButton
            // 
            this.transfertButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.transfertButton.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.transfertButton.Location = new System.Drawing.Point(1045, 453);
            this.transfertButton.Name = "transfertButton";
            this.transfertButton.Size = new System.Drawing.Size(105, 30);
            this.transfertButton.TabIndex = 190;
            this.transfertButton.TabStop = false;
            this.transfertButton.Text = "Transférer pesées";
            this.transfertButton.UseVisualStyleBackColor = false;
            this.transfertButton.Click += new System.EventHandler(this.transfertButton_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPrint.Location = new System.Drawing.Point(1045, 569);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(105, 50);
            this.btnPrint.TabIndex = 140;
            this.btnPrint.Text = "Attestation de contrôle";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // pEC_PED_COMBindingSource
            // 
            this.pEC_PED_COMBindingSource.DataMember = "PEC_PED_COM";
            this.pEC_PED_COMBindingSource.DataSource = this.dSPesees;
            // 
            // pEC_PED_COMTableAdapter
            // 
            this.pEC_PED_COMTableAdapter.ClearBeforeFill = true;
            // 
            // peL_PED_LIETableAdapter
            // 
            this.peL_PED_LIETableAdapter.ClearBeforeFill = true;
            // 
            // txtRemarques
            // 
            this.txtRemarques.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtRemarques.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pEE_PESEE_ENTETEBindingSource, "PEE_REMARQUES", true));
            this.txtRemarques.Location = new System.Drawing.Point(605, 291);
            this.txtRemarques.Multiline = true;
            this.txtRemarques.Name = "txtRemarques";
            this.txtRemarques.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarques.Size = new System.Drawing.Size(436, 49);
            this.txtRemarques.TabIndex = 120;
            // 
            // LblRemarques
            // 
            this.LblRemarques.AutoSize = true;
            this.LblRemarques.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Bold);
            this.LblRemarques.Location = new System.Drawing.Point(602, 270);
            this.LblRemarques.Name = "LblRemarques";
            this.LblRemarques.Size = new System.Drawing.Size(93, 16);
            this.LblRemarques.TabIndex = 87;
            this.LblRemarques.Text = "Remarques :";
            // 
            // LblMecanique
            // 
            this.LblMecanique.AutoSize = true;
            this.LblMecanique.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.LblMecanique.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.LblMecanique.Location = new System.Drawing.Point(12, 267);
            this.LblMecanique.Name = "LblMecanique";
            this.LblMecanique.Size = new System.Drawing.Size(113, 15);
            this.LblMecanique.TabIndex = 89;
            this.LblMecanique.Text = "Vend. mécaniques :";
            // 
            // cbxVendMec
            // 
            this.cbxVendMec.AutoSize = true;
            this.cbxVendMec.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.pEE_PESEE_ENTETEBindingSource, "PEE_DIVERS3", true));
            this.cbxVendMec.Location = new System.Drawing.Point(148, 268);
            this.cbxVendMec.Name = "cbxVendMec";
            this.cbxVendMec.Size = new System.Drawing.Size(15, 14);
            this.cbxVendMec.TabIndex = 90;
            this.cbxVendMec.UseVisualStyleBackColor = true;
            // 
            // txtScaleNb
            // 
            this.txtScaleNb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtScaleNb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pEE_PESEE_ENTETEBindingSource, "PEE_DIVERS2", true));
            this.txtScaleNb.Location = new System.Drawing.Point(148, 288);
            this.txtScaleNb.Name = "txtScaleNb";
            this.txtScaleNb.Size = new System.Drawing.Size(245, 23);
            this.txtScaleNb.TabIndex = 100;
            // 
            // LblBalance
            // 
            this.LblBalance.AutoSize = true;
            this.LblBalance.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.LblBalance.Location = new System.Drawing.Point(14, 294);
            this.LblBalance.Name = "LblBalance";
            this.LblBalance.Size = new System.Drawing.Size(89, 15);
            this.LblBalance.TabIndex = 91;
            this.LblBalance.Text = "N° de balance :";
            // 
            // txtRefractometerNb
            // 
            this.txtRefractometerNb.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtRefractometerNb.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pEE_PESEE_ENTETEBindingSource, "PEE_DIVERS1", true));
            this.txtRefractometerNb.Location = new System.Drawing.Point(148, 317);
            this.txtRefractometerNb.Name = "txtRefractometerNb";
            this.txtRefractometerNb.Size = new System.Drawing.Size(245, 23);
            this.txtRefractometerNb.TabIndex = 110;
            // 
            // lblRefractometre
            // 
            this.lblRefractometre.AutoSize = true;
            this.lblRefractometre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblRefractometre.Location = new System.Drawing.Point(14, 320);
            this.lblRefractometre.Name = "lblRefractometre";
            this.lblRefractometre.Size = new System.Drawing.Size(128, 15);
            this.lblRefractometre.TabIndex = 93;
            this.lblRefractometre.Text = "N° de refractomètre :";
            // 
            // contact_NomCompletTableAdapter
            // 
            this.contact_NomCompletTableAdapter.ClearBeforeFill = true;
            // 
            // lblOwnerAndProducerNames
            // 
            this.lblOwnerAndProducerNames.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblOwnerAndProducerNames.Location = new System.Drawing.Point(204, 10);
            this.lblOwnerAndProducerNames.Name = "lblOwnerAndProducerNames";
            this.lblOwnerAndProducerNames.Size = new System.Drawing.Size(808, 23);
            this.lblOwnerAndProducerNames.TabIndex = 191;
            this.lblOwnerAndProducerNames.Text = "Propriétaire: Tar Tempion | Producteur: Ploume DEAU";
            this.lblOwnerAndProducerNames.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WfrmSaisieEntetePesee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(1164, 681);
            this.Controls.Add(this.lblOwnerAndProducerNames);
            this.Controls.Add(this.txtRefractometerNb);
            this.Controls.Add(this.lblRefractometre);
            this.Controls.Add(this.txtScaleNb);
            this.Controls.Add(this.LblBalance);
            this.Controls.Add(this.LblMecanique);
            this.Controls.Add(this.cbxVendMec);
            this.Controls.Add(this.LblRemarques);
            this.Controls.Add(this.txtRemarques);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.transfertButton);
            this.Controls.Add(this.cepageComboBox);
            this.Controls.Add(lblCepage);
            this.Controls.Add(this.limitationParAcquitGroupBox);
            this.Controls.Add(this.aUT_IDComboBox);
            this.Controls.Add(this.lIE_IDLabel);
            this.Controls.Add(this.lIE_IDComboBox);
            this.Controls.Add(this.pEE_GRANDCRULabel);
            this.Controls.Add(this.pEE_GRANDCRUCheckBox);
            this.Controls.Add(this.limitationParLieuGroupBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnValiderPesee);
            this.Controls.Add(this.btnModifierPesee);
            this.Controls.Add(this.btnDeletePesee);
            this.Controls.Add(this.btnAddPesee);
            this.Controls.Add(this.pED_PESEE_DETAILDataGridView);
            this.Controls.Add(this.aCQ_IDComboBox);
            this.Controls.Add(this.btnSelectionAcquit);
            this.Controls.Add(this.aCQ_IDLabel);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.pEE_IDTextBox);
            this.Controls.Add(this.rES_IDComboBox);
            this.Controls.Add(this.pEE_DATEDateTimePicker);
            this.Controls.Add(this.pEE_LIEUTextBox);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(1070, 650);
            this.Name = "WfrmSaisieEntetePesee";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Gestion des Vendanges - Attestation de pesées";
            this.Load += new System.EventHandler(this.OnLoad);
            this.Shown += new System.EventHandler(this.WfrmSaisieEntetePesee_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.ResponsablesContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rESNOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pED_PESEE_DETAILDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            this.limitationParLieuGroupBox.ResumeLayout(false);
            this.limitationParLieuGroupBox.PerformLayout();
            this.contextLieuDeProduction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            this.contextNomLocal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            this.limitationParAcquitGroupBox.ResumeLayout(false);
            this.limitationParAcquitGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pEC_PED_COMBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        //
        private System.Windows.Forms.TextBox pEE_IDTextBox;
        private System.Windows.Forms.ComboBox rES_IDComboBox;
        private System.Windows.Forms.DateTimePicker pEE_DATEDateTimePicker;
        private System.Windows.Forms.TextBox pEE_LIEUTextBox;
        private System.Windows.Forms.Button btnSelectionAcquit;
        private System.Windows.Forms.ComboBox aCQ_IDComboBox;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pEE_PESEE_ENTETEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter pEE_PESEE_ENTETETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
     //   private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
       // private System.Windows.Forms.BindingSource cRUCRUBindingSource;
        //  private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private System.Windows.Forms.BindingSource aCQACQUITBindingSource;
        private System.Windows.Forms.BindingSource pED_PESEE_DETAILBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter pED_PESEE_DETAILTableAdapter;
        private System.Windows.Forms.DataGridView pED_PESEE_DETAILDataGridView;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private System.Windows.Forms.Button btnModifierPesee;
        private System.Windows.Forms.Button btnDeletePesee;
        private System.Windows.Forms.Button btnAddPesee;
        private System.Windows.Forms.Button btnValiderPesee;
        private System.Windows.Forms.ContextMenuStrip ResponsablesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ResponsablesToolStripMenuItem;
        private System.Windows.Forms.ToolTip tootTipCru;
        private System.Windows.Forms.GroupBox limitationParLieuGroupBox;
        public System.Windows.Forms.TextBox txtLitresAdmisApp;
        public System.Windows.Forms.TextBox txtLitresSaisisApp;
        private System.Windows.Forms.CheckBox pEE_GRANDCRUCheckBox;
        //private System.Windows.Forms.BindingSource mETMENTIONTXTBindingSource;
       // private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MET_MENTIONTXTTableAdapter mET_MENTIONTXTTableAdapter;
        private System.Windows.Forms.ComboBox lIE_IDComboBox;
        private System.Windows.Forms.ComboBox aUT_IDComboBox;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private System.Windows.Forms.GroupBox limitationParAcquitGroupBox;
        public System.Windows.Forms.TextBox txtLitresAdmisAcq;
        public System.Windows.Forms.TextBox txtLitresSaisisAcq;
        private System.Windows.Forms.BindingSource rESNOMCOMPLETBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_NOMCOMPLETTableAdapter rES_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_PEE_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_LOT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_SONDAGE_OE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_QUANTITE_KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_QUANTITE_LITRES;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_SONDAGE_BRIKS;
        private System.Windows.Forms.ContextMenuStrip contextLieuDeProduction;
        private System.Windows.Forms.ToolStripMenuItem toolLieuProduction;
        private System.Windows.Forms.ContextMenuStrip contextNomLocal;
        private System.Windows.Forms.ToolStripMenuItem toolNomLocal;
        private System.Windows.Forms.Label lblLimitParLieuAdmis;
        private System.Windows.Forms.Label lblLimitParLieuSaisis;
        private System.Windows.Forms.Label lblLimitParAcquitAdmis;
        private System.Windows.Forms.Label lblLimitParAcquitSaisis;
		private System.Windows.Forms.Label lblSoldeParAcquit;
		public System.Windows.Forms.TextBox txtSoldeParAcquit;
		private System.Windows.Forms.Label lblSoldeParLieu;
		public System.Windows.Forms.TextBox txtSoldeParLieu;
		private System.Windows.Forms.ComboBox cepageComboBox;
		private System.Windows.Forms.Button transfertButton;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label pEE_GRANDCRULabel;
        private System.Windows.Forms.Label lIE_IDLabel;
        private System.Windows.Forms.Label aCQ_IDLabel;
        private System.Windows.Forms.BindingSource pEC_PED_COMBindingSource;
        private DAO.DSPeseesTableAdapters.PEC_PED_COMTableAdapter pEC_PED_COMTableAdapter;
        private DAO.DSPeseesTableAdapters.PEL_PED_LIETableAdapter peL_PED_LIETableAdapter;
        public System.Windows.Forms.TextBox txtRemarques;
        private System.Windows.Forms.Label LblRemarques;
        private System.Windows.Forms.Label LblMecanique;
        private System.Windows.Forms.CheckBox cbxVendMec;
        public System.Windows.Forms.TextBox txtScaleNb;
        private System.Windows.Forms.Label LblBalance;
        public System.Windows.Forms.TextBox txtRefractometerNb;
        private System.Windows.Forms.Label lblRefractometre;
        private DAO.DSPeseesTableAdapters.Contact_NomCompletTableAdapter contact_NomCompletTableAdapter;
        private System.Windows.Forms.Label lblOwnerAndProducerNames;
    }
}