﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de sélection du dossier WinBIZ
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    public partial class WfrmWinBizFolder : WfrmBase
    {
        private WinBizFolder _winBizFolder;
        private static WfrmWinBizFolder _wfrm;

        public static WfrmWinBizFolder Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmWinBizFolder();
                }
                return _wfrm;
            }
        }

        public WfrmWinBizFolder()
        {
            InitializeComponent();
            _winBizFolder = new WinBizFolder();
            txtChemin.Text = _winBizFolder.Current;
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtChemin.Text = _winBizFolder.Current;
        }

        private void btnParcourir_Click(object sender, EventArgs e)
        {
            bool quit = false;
            while (!quit)
            {
                quit = true;
                folderBrowserDialog1.ShowDialog();
                try
                {
                    _winBizFolder.SetWinBizFolder(folderBrowserDialog1.SelectedPath);
                    txtChemin.Text = folderBrowserDialog1.SelectedPath;
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    DialogResult result = MessageBox.Show("Ce chemin ne contient pas le dossier de l'année courante.\nVeuillez sélectionner un autre dossier.", "Erreur dossier", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                    if (result == DialogResult.OK)
                    {
                        quit = false;
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _winBizFolder.SaveCurrentFolder();
            DialogResult = DialogResult.OK;
        }
    }
}