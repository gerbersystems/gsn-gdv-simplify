﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesAnnees
    {
        private void InitSpecificValues(int ligne)
        {
            aNN_RESPONSABLE_1TextBox.Text = aNN_ANNEEDataGridView["ANN_RESPONSABLE_1", ligne].Value.ToString();
            aNN_RESPONSABLE_2TextBox.Text = aNN_ANNEEDataGridView["ANN_RESPONSABLE_2", ligne].Value.ToString();
            aNN_RESPONSABLE_3TextBox.Text = aNN_ANNEEDataGridView["ANN_RESPONSABLE_3", ligne].Value.ToString();
            aNN_RESPONSABLE_4TextBox.Text = aNN_ANNEEDataGridView["ANN_RESPONSABLE_4", ligne].Value.ToString();
        }

        private void AddSpecificControls()
        {
            _manager.AddControl(aNN_RESPONSABLE_1TextBox);
            _manager.AddControl(aNN_RESPONSABLE_2TextBox);
            _manager.AddControl(aNN_RESPONSABLE_3TextBox);
            _manager.AddControl(aNN_RESPONSABLE_4TextBox);
        }
    }
}