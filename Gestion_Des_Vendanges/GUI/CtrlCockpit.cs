﻿namespace Gestion_Des_Vendanges.GUI
{
    using BUSINESS;
    using DAO;
    using GSN.GDV.GUI.Controllers;
    using GSN.GDV.GUI.Models;
    using GSN.GDV.GUI.WindowsForms;
    using GSN.GDV.Reports;
    using Helpers;
    using log4net;
    using System;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;

    partial class CtrlCockpit : CtrlWfrm
    {
        #region Fields

        private const string TOUS = "Tous";
        private const string DEFAULT_KILOS_DELIVERED_LABEL = "Livré";
        private const string DEFAULT_TOTAL_LABEL = "Total";
        private const string DEFAULT_TOTAL_LITERS_QUOTA_LABEL = "Droit de production";
        private const string DEFAULT_TOTAL_SURFACE_AREA_LABEL = "Surface";
        private ILog _log;
        private DSPesees.CockpitAcquitsDataTable _cockpitAcquitsDataTable;
        private DataGridViewCellEventArgs _mouseLocation;

        #endregion Fields

        #region Properties

        public int? ExploitantId => cbxExploitant.SelectedValue as int? ?? 0;
        public int? NullableExploitantId => cbxExploitant.SelectedValue as int?;

        private int SelectedPeseeEnteteId
        {
            get
            {
                string _currentPeeId = cockpitWeighingsDataGridView.SelectedRows[0].Cells["PEE_ID"].Value.ToString().TrimEnd();

                if (string.IsNullOrWhiteSpace(_currentPeeId) || !int.TryParse(_currentPeeId, out int peeId)) return -1;

                return peeId;
            }
        }

        public int SelectedPeseeDetailId { get; private set; }

        public override Panel OptionPanel => pnlYear;

        public override ToolStrip ToolStrip => toolStrip1;

        #endregion Properties

        #region Constructors

        public static CtrlCockpit GetInstance() => new CtrlCockpit();

        private CtrlCockpit()
        {
            _log = LogManager.GetLogger(GetType());

            InitializeComponent();
            CreateMissingColumns();
            InitializeToolStripButton();
            AddContextMenu();

            pnlYear.Visible = SettingsService.UserSettings.Rights > 0;
            btnAddAcq.Visible = SettingsService.UserSettings.Rights > 0;
            toolStrip1.Visible = SettingsService.UserSettings.Rights > 0;
        }

        private void CtrlCockpit_Load(object sender, EventArgs e)
        {
            _log = LogManager.GetLogger(GetType());

            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(peE_PESEE_ENTETETableAdapter1);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cockpitAcquitsTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cockpitWeighingsTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(acQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            var exploitants = pRO_NOMCOMPLETTableAdapter.GetProducteurs().Select(p => new { Nom = p.NOMCOMPLET, Id = p.PRO_ID }).OrderBy(p => p.Nom).ToList();
            exploitants.Reverse();
            exploitants.Add(new { Nom = TOUS, Id = 0 });
            exploitants.Reverse();

            cbxExploitant.DataSource = exploitants;
            cbxExploitant.ValueMember = "Id";
            cbxExploitant.DisplayMember = "Nom";

            UpdateDonnees();
            Controls.Remove(OptionPanel);
            InitializeEvents();
        }

        private void InitializeToolStripButton()
        {
            toolAdd.Click += (s, e) => ShowSaisieAcquit_New();
            toolUpdate.Click += (s, e) => ShowSaisieAcquit_Edit();
            ShowR_ApportsButton.Click += (s, e) => CtrlReportViewer.ShowApports(NullableExploitantId, Utilities.IdAnneeCourante, FormatReport.Apercu); ;
        }

        private void InitializeEvents()
        {
            cbAnnee.SelectedIndexChanged += (s, e) => ChangeYear();
            cbxExploitant.SelectedIndexChanged += (sender, e) => UpdateAcquit();
            btnAddAcq.Click += (s, e) => ShowSaisieAcquit_New();

            cockpitAcquitsDataGridView.SelectionChanged += (s, e) => UpdateWeighings();
            cockpitAcquitsDataGridView.MouseDoubleClick += (s, e) => ShowSaisieAcquit_Edit();
            cockpitAcquitsDataGridView.CellClick += ShowSaisieEntetePesee_New;

            cockpitWeighingsDataGridView.MouseDoubleClick += (s, e) => ShowSaisieEntetePesee_Edit();
        }

        private void CreateMissingColumns()
        {
            var rightsKgColumn = new DataGridViewTextBoxColumn
            {
                DataPropertyName = "PAR_LITRES",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
                HeaderText = "Droit L",
                Visible = true,
                DividerWidth = 1,
                DisplayIndex = 10,
                DefaultCellStyle =
                                     {
                                         Alignment = DataGridViewContentAlignment.MiddleRight,
                                         Format = "# ### \\L"
                                     }
            };

            cockpitAcquitsDataGridView.Columns.Add(rightsKgColumn);
        }

        private void AddContextMenu()
        {
            var strip = new ContextMenuStrip();

            var toolStripItemTransfert = new ToolStripMenuItem { Text = "Transfert..." };
            toolStripItemTransfert.Click += ToolStripItemMenuWeighingsTransfert_Click;

            var toolStripItemSplit = new ToolStripMenuItem { Text = "Split..." };
            toolStripItemSplit.Click += ToolStripItemMenuWeighingsSplit_Click;

            foreach (DataGridViewColumn column in cockpitWeighingsDataGridView.Columns)
            {
                column.ContextMenuStrip = strip;
                column.ContextMenuStrip.Items.Add(toolStripItemTransfert);
                column.ContextMenuStrip.Items.Add(toolStripItemSplit);
            }

            cockpitWeighingsDataGridView.CellMouseEnter += (s, location) => _mouseLocation = location;
            cockpitWeighingsDataGridView.SelectionChanged += (s, e) =>
            {
                SelectedPeseeDetailId = cockpitWeighingsDataGridView.SelectedRows.Count > 0 ?
                                            (int)cockpitWeighingsDataGridView.SelectedRows[0]?.Cells["PED_ID"].Value :
                                            -1;
            };
        }

        private void ToolStripItemMenuWeighingsTransfert_Click(object sender, EventArgs e)
        {
            if (_mouseLocation == null) return;

            SelectedPeseeDetailId = (int)cockpitWeighingsDataGridView.Rows[_mouseLocation.RowIndex].Cells["PED_ID"].Value;
            TransferWeighing();
        }

        private void ToolStripItemMenuWeighingsSplit_Click(object sender, EventArgs e)
        {
            if (_mouseLocation == null) return;

            SelectedPeseeDetailId = (int)cockpitWeighingsDataGridView.Rows[_mouseLocation.RowIndex].Cells["PED_ID"].Value;
            SplitWeighing();
        }

        #endregion Constructors

        #region Public Methods

        public void UpdateAcquit()
        {
            if (cbxExploitant.SelectedIndex < 0) return;

            try
            {
                _cockpitAcquitsDataTable = ExploitantId > 0
                                                            ? cockpitAcquitsTableAdapter.GetData(ExploitantId, Utilities.IdAnneeCourante)
                                                            : cockpitAcquitsTableAdapter.GetAllProducersAcqs(Utilities.IdAnneeCourante);

                cockpitAcquitsDataGridView.DataSource = _cockpitAcquitsDataTable;
                UpdateTotal();
                UpdateWeighings();
            }
            catch (Exception ex)
            {
                _log.Error($"Error in {MethodBase.GetCurrentMethod().Name}", ex);
            }
        }

        public override void UpdateDonnees()
        {
            var previousSelectedExploitant = cbxExploitant.SelectedIndex;

            try
            {
                aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
                cbAnnee.SelectedValue = Utilities.IdAnneeCourante;
                pRO_NOMCOMPLETTableAdapter.FillProducteurs(dSPesees.PRO_NOMCOMPLET);
                pAR_PARCELLETableAdapter.Fill(dSPesees.PAR_PARCELLE);
            }
            catch (ConstraintException ex)
            {
                _log.Error($"Error when Fill data in method {MethodBase.GetCurrentMethod().Name}", ex);
            }
            catch (Exception ex)
            {
                _log.Error($"Error in {MethodBase.GetCurrentMethod().Name}", ex);
                throw;
            }
            UpdateAcquit();
            UpdateSelectedExploitant(previousSelectedExploitant);
            UpdateWeighings();
        }

        #endregion Public Methods

        #region Private Methods

        private void TransferWeighing()
        {
            ITransfertPeseeDetailWizardViewModel model;
            var helper = ControllerHelper.Create();
            var controller = new TransfertPeseeDetailWizardController(helper);

            using (var db = helper.CreateMainDataContext())
            {
                model = controller.CreateViewModel(Utilities.IdAnneeCourante, db, SelectedPeseeEnteteId, SelectedPeseeDetailId);
            }

            var form = new TransfertPeseeDetailWizardWindowsForm(model) { Controller = controller };
            form.UserControl.Controller = controller;
            if (form.ShowDialog(this) != DialogResult.OK) return;

            UpdateDonnees();
        }

        private void SplitWeighing()
        {
            ITransfertPeseeDetailWizardViewModel model;
            var helper = ControllerHelper.Create();
            var controller = new TransfertPeseeDetailWizardController(helper);

            using (var db = helper.CreateMainDataContext())
            {
                model = controller.CreateViewModel(Utilities.IdAnneeCourante, db, SelectedPeseeEnteteId, SelectedPeseeDetailId, 0, (double?)cockpitWeighingsDataGridView.Rows[_mouseLocation.RowIndex].Cells["PED_QUANTITE_KG"].Value);
            }

            var form = new TransfertPeseeDetailWizardWindowsForm(model) { Controller = controller };
            form.UserControl.Controller = controller;
            if (form.ShowDialog(this) != DialogResult.OK) return;

            UpdateDonnees();
        }

        private void ShowSaisieAcquit_New()
        {
            try
            {
                var formAcquit = new WfrmSaisieAcquit(this);
                formAcquit.WfrmShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ShowSaisieAcquit_Edit()
        {
            if (SettingsService.UserSettings.Rights < 1) return;
            try
            {
                if (cockpitAcquitsDataGridView.CurrentRow == null) return;
                if (!int.TryParse(cockpitAcquitsDataGridView.SelectedRows[0].Cells[0].Value.ToString(), out int acqId)) return;
                var acq = acQ_ACQUITTableAdapter.GetByAcqId(acqId).FirstOrDefault();
                if (acq == null) return;
                var acqObj = new Acquit(acq.ACQ_ID,
                                        acq.CLA_ID,
                                        acq.COU_ID,
                                        acq.ANN_ID,
                                        acq.COM_ID,
                                        acq.REG_ID,
                                        acq.PRO_ID,
                                        acq.PRO_ID_VENDANGE,
                                        acq.ACQ_NUMERO,
                                        (decimal)acq.ACQ_SURFACE,
                                        (decimal)acq.ACQ_LITRES
                                       );
                try
                {
                    acqObj.Date = acq.ACQ_DATE;
                }
                catch
                {
                }

                try
                {
                    acqObj.DateReception = acq.ACQ_RECEPTION;
                }
                catch
                {
                }

                try
                {
                    acqObj.NumeroComplementaire = acq.ACQ_NUMERO_COMPL;
                }
                catch
                {
                }

                var formAddAcquit = new WfrmSaisieAcquit(this, acqObj);
                formAddAcquit.WfrmShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ShowSaisieEntetePesee_Edit()
        {
            if (cockpitAcquitsDataGridView.SelectedRows.Count < 1) return;
            if (SelectedPeseeEnteteId < 0) return;

            var acqId = cockpitAcquitsDataGridView.SelectedRows[0].Cells[1].Value.ToString().TrimEnd();
            if (string.IsNullOrWhiteSpace(acqId)) return;

            var pee = peE_PESEE_ENTETETableAdapter1.GetDataByPed(SelectedPeseeDetailId).FirstOrDefault();
            int.TryParse(cockpitAcquitsDataGridView.SelectedRows[0].Cells["CEP_ID"].Value.ToString(), out int cepId);

            if (pee == null) return;
            var w = new WfrmSaisieEntetePesee(new EntetePesee
            {
                AcquitId = pee.ACQ_ID,
                AutreMentionId = pee.AUT_ID,
                CepageId = cepId,
                Date = pee.PEE_DATE,
                GrandCru = !pee.IsPEE_GRANDCRUNull() && pee.PEE_GRANDCRU,
                Id = pee.PEE_ID,
                Lieu = pee.PEE_LIEU,
                LieuProductionId = pee.LIE_ID,
                ResponsableId = pee.RES_ID,
                Remark = pee.IsPEE_REMARQUESNull() ? "" : pee.PEE_REMARQUES,
                Divers1 = pee.IsPEE_DIVERS1Null() ? "" : pee.PEE_DIVERS1,
                Divers2 = pee.IsPEE_DIVERS2Null() ? "" : pee.PEE_DIVERS2,
                Divers3 = pee.IsPEE_DIVERS3Null() ? "" : pee.PEE_DIVERS3.ToString()
            }, this);
            w.Show();
        }

        private void ShowSaisieEntetePesee_New(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (e.RowIndex < 0) return;
            if (!(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn)) return;

            var acqId = cockpitAcquitsDataGridView[1, e.RowIndex].Value.ToString().TrimEnd();
            if (string.IsNullOrWhiteSpace(acqId)) return;

            var parIdValue = int.TryParse(cockpitAcquitsDataGridView["PAR_ID", e.RowIndex].Value.ToString(), out int parId);
            var acq = _cockpitAcquitsDataTable.FirstOrDefault(a => a.No__acquit.TrimEnd() == acqId && (!parIdValue || a.PAR_ID == parId));

            if (acq == null) return;

            var cepIdValue = int.TryParse(cockpitAcquitsDataGridView["CEP_ID", e.RowIndex].Value.ToString(), out int cepId);

            var v = new WfrmSaisieEntetePesee(acq.ACQ_ID, cepIdValue ? cepId : acq.CEP_ID, DateTime.Now, false, acq.AUT_ID, acq.LIE_ID, this);

            v.Show();
        }

        private void ChangeYear()
        {
            if (cbAnnee.SelectedValue == null || Convert.ToInt32(cbAnnee.SelectedValue) == Utilities.IdAnneeCourante) return;

            Utilities.IdAnneeCourante = Convert.ToInt32(cbAnnee.SelectedValue);
            UpdateDonnees();
        }

        private void UpdateSelectedExploitant(int previousSelectedExploitant)
        {
            cbxExploitant.SelectedIndex = previousSelectedExploitant == -1 && cbxExploitant.Items.Count > 0 ? 0 : previousSelectedExploitant;
        }

        private void UpdateTotal()
        {
            TotalLabel.Text = $"{DEFAULT_TOTAL_LABEL} {Utilities.AnneeCourante}";

            TotalSurfaceAreaLabel.Text = $"{DEFAULT_TOTAL_SURFACE_AREA_LABEL} {_cockpitAcquitsDataTable.Sum(c => c.Surface_m2).ToString("N0")} m2";
            TotalLitersQuotaLabel.Text = $"{DEFAULT_TOTAL_LITERS_QUOTA_LABEL} {_cockpitAcquitsDataTable.Sum(c => c.ACQ_LITRES).ToString("N0")} L";
            TotalKilosDeliveredLabel.Text = $"{DEFAULT_KILOS_DELIVERED_LABEL} {_cockpitAcquitsDataTable.Sum(c => c.Kg_livré).ToString("N0")} kg";
        }

        private void SetWeighings(DSPesees.CockpitAcquitsRow acq)
        {
            if (acq == null) return;
            cockpitWeighingsTableAdapter.Fill(dSPesees.CockpitWeighings, acq.No__acquit, acq.ACQ_ID, acq.CEP_ID, acq.LIE_ID, acq.AUT_ID);
            lblWeighings.Text = $"Apports : {acq.Couleur} - {acq.COM_NOM} - {acq.Nom_Local}";
        }

        private void UpdateWeighings()
        {
            if (cockpitAcquitsDataGridView.SelectedRows.Count < 1)
            {
                SelectedPeseeDetailId = -1;
                return;
            }

            var acqId = cockpitAcquitsDataGridView.SelectedRows[0].Cells[1].Value.ToString().TrimEnd();
            if (string.IsNullOrWhiteSpace(acqId)) return;

            var parIdValue = int.TryParse(cockpitAcquitsDataGridView.SelectedRows[0].Cells["PAR_ID"].Value.ToString(), out int parId);
            var acq = _cockpitAcquitsDataTable?.FirstOrDefault(a => a.No__acquit.TrimEnd() == acqId && (!parIdValue || a.PAR_ID == parId));

            SetWeighings(acq);
        }

        #endregion Private Methods
    }
}