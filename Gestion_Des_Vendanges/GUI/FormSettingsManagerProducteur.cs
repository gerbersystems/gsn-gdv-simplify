﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement du formulaire "Producteur"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class FormSettingsManagerProducteur : FormSettingsManagerRefAdresse
    {
        private ADR_ADRESSETableAdapter _adrTable;
        private TextBox _idTextBox;

        //private int _nbLicence;
        private TextBox _txtId;

        private PRO_PROPRIETAIRETableAdapter _proTable;
        private CheckBox _chkIsProducteur;
        private ComboBox _cbAdrId;
        private ComboBox _cbModeTVA;
        private MOD_MODE_TVATableAdapter _modTable;
        private Button _btnImportWinbiz;

        private bool HasErpId => Convert.ToBoolean(DataGridView.CurrentRow.Cells["ADR_ID_WINBIZ"].Value);
        private bool IsSynchronized => HasErpId && GlobalParam.Instance.SyncContacts == SyncContactsEnum.ProhibitModifyLocalContacts;

        public FormSettingsManagerProducteur(WfrmSetting form
                                           , TextBox idTextBox
                                           , Control focusControl
                                           , Button btnNew
                                           , Button btnUpdate
                                           , Button btnDelete
                                           , DataGridView dataGridView
                                           , BindingSource bindingSource
                                           , TableAdapterManager tableAdapterManager
                                           , DSPesees dsPesees
                                           , CheckBox chkIsProducteur
                                           , ComboBox cbAdrId
                                           , ComboBox cbModeTVA
                                           , Button btnImportWinbiz
                                           , AdresseCompletTableAdapter adrCompletTable) :
            base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dsPesees, adrCompletTable)
        {
            _adrTable = ConnectionManager.Instance.CreateGvTableAdapter<ADR_ADRESSETableAdapter>();
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
            _modTable = ConnectionManager.Instance.CreateGvTableAdapter<MOD_MODE_TVATableAdapter>();

            _chkIsProducteur = chkIsProducteur;
            _txtId = idTextBox;
            _cbAdrId = cbAdrId;
            _cbModeTVA = cbModeTVA;
            _idTextBox = idTextBox;
            _btnImportWinbiz = btnImportWinbiz;

            DataGridView.SelectionChanged += DataGridSelectionChanged;
            DataGridView.CellEndEdit += CellEndEdit;
        }

        private void DataGridSelectionChanged(object sender, EventArgs e)
        {
            if (_idTextBox.Text != "")
            {
                int proId = Convert.ToInt32(_idTextBox.Text);
                try
                {
                    int? modId = _proTable.GetModId(proId);
                    if (modId == null)
                    {
                        modId = -1;
                    }
                    _cbModeTVA.SelectedValue = modId;
                }
                catch (ArgumentNullException)
                {
                }
            }
        }

        private void CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == DataGridView.Columns["PRO_IS_PROPRIETAIRE"].Index)
            {
                try
                {
                    int proId = Convert.ToInt32(DataGridView.Rows[e.RowIndex].Cells["PRO_ID"].Value);
                    if (Convert.ToInt32(_proTable.NbRef(proId)) != 0 && !Convert.ToBoolean(DataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value))
                    {
                        DataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                        MessageBox.Show("Ce propriétaire est utilisé dans une pesée.", "Mise à jour impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        SaveData();
                    }
                }
                catch { }
            }
        }

        protected override void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsSynchronized)
                {
                    MessageBox.Show("Vous ne pouvez pas modifier cet enregistrement car il a été importé depuis WinBiz.\n" +
                        "Veuillez effectuer les modifications dans WinBiz et relancer l'importation.", "Modification impossible",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    _btnImportWinbiz.Enabled = false;
                    base.BtnUpdate_Click(sender, e);
                }
            }
            catch (InvalidCastException)
            {
                _btnImportWinbiz.Enabled = false;
                base.BtnUpdate_Click(sender, e);
            }
            catch (NullReferenceException)
            {
            }
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            if (_cbAdrId.SelectedValue != null)
            {
                if (!_chkIsProducteur.Checked && !IsNew)
                {
                    int nbRef = (int)_proTable.GetNbRefProducteur(int.Parse(_txtId.Text));
                    if (nbRef > 0)
                    {
                        MessageBox.Show("Vous ne pouvez pas désactiver ce producteur car il est utilisé dans un acquit",
                            "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        DoOk(sender, e);
                    }
                }
                else
                {
                    DoOk(sender, e);
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner une adresse.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DoOk(object sender, EventArgs e)
        {
            base.BtnOk_Click(sender, e);
            //Mode de TVA
            int proId = Convert.ToInt32(_idTextBox.Text);
            int? modId = (int?)_cbModeTVA.SelectedValue;
            if (modId == -1)
            {
                modId = null;
            }
            _proTable.SetModId(modId, proId);
            _btnImportWinbiz.Enabled = true;
        }

        protected override void BtnCancel_Click(object sender, EventArgs e)
        {
            _btnImportWinbiz.Enabled = true;
            base.BtnCancel_Click(sender, e);
        }

        protected override void BtnNew_Click(object sender, EventArgs e)
        {
            _btnImportWinbiz.Enabled = false;
            base.BtnNew_Click(sender, e);
        }
    }
}