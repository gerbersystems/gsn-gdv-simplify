﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement des formulaires "standards"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class FormSettingsManagerPesees : FormSettingsManager<DSPesees, TableAdapterManager>
    {
        public FormSettingsManagerPesees(ICtrlSetting form
                                       , TextBox idTextBox
                                       , Control focusControl
                                       , Button btnNew
                                       , Button btnUpdate
                                       , Button btnDelete
                                       , DataGridView dataGridView
                                       , BindingSource bindingSource
                                       , TableAdapterManager tableAdapterManager
                                       , DSPesees dataSet)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
        }

        public FormSettingsManagerPesees(ICtrlSetting ctrlSetting
                                       , TextBox idTextBox
                                       , Control focusControl
                                       , ToolStripButton btnNew
                                       , ToolStripButton btnUpdate
                                       , ToolStripButton btnDelete
                                       , Button btnOk
                                       , Button btnCancel
                                       , DataGridView dataGridView
                                       , BindingSource bindingSource
                                       , TableAdapterManager tableAdapterManager
                                       , DSPesees dataSet)
            : base(ctrlSetting, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, btnOk, btnCancel, dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
        }

        protected override void SaveData()
        {
            base.SaveData();
            TableAdapterManager.UpdateAll(DataSet);
        }
    }

    internal class FormSettingsManagerMaster : FormSettingsManager<DSMaster, DAO.DSMasterTableAdapters.TableAdapterManager>
    {
        public FormSettingsManagerMaster(ICtrlSetting form, TextBox idTextBox, Control focusControl, Button btnNew, Button btnUpdate, Button btnDelete, DataGridView dataGridView, BindingSource bindingSource, DAO.DSMasterTableAdapters.TableAdapterManager tableAdapterManager, DSMaster dataSet)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
        }

        public FormSettingsManagerMaster(ICtrlSetting ctrlSetting, TextBox idTextBox, Control focusControl, ToolStripButton btnNew, ToolStripButton btnUpdate, ToolStripButton btnDelete, Button btnOk, Button btnCancel, DataGridView dataGridView, BindingSource bindingSource, DAO.DSMasterTableAdapters.TableAdapterManager tableAdapterManager, DSMaster dataSet)
            : base(ctrlSetting, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, btnOk, btnCancel, dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
        }

        protected override void SaveData()
        {
            base.SaveData();
            TableAdapterManager.UpdateAll(DataSet);
        }
    }

    internal class FormSettingsManager<TDataSet, TTableAdapterManager>
        where TDataSet : System.Data.DataSet
        where TTableAdapterManager : Component
    {
        private bool _isNew;
        private ICtrlSetting _ctrlSetting;
        private TextBox _idTextBox;
        private Control _focusControl;
        private IButtonSettings _btnNew;
        private IButtonSettings _btnUpdate;
        private IButtonSettings _btnDelete;
        private IButtonSettings _btnOK;
        private IButtonSettings _btnCancel;
        private DataGridView _dataGridView;
        private BindingSource _bindingSource;
        private TTableAdapterManager _tableAdapterManager;
        private TDataSet _dataSet;
        private List<Control> _controls;
        private List<DateTimePicker> _dateTimePickers;
        private bool _isDeleted;

        protected bool IsDeleted
        {
            get { return _isDeleted; }
        }

        protected DataGridView DataGridView
        {
            get { return _dataGridView; }
        }

        protected TDataSet DataSet
        {
            get { return _dataSet; }
        }

        protected Control[] Controls
        {
            get { return _controls.ToArray(); }
        }

        protected ICtrlSetting Form
        {
            get { return _ctrlSetting; }
        }

        protected TTableAdapterManager TableAdapterManager
        {
            get { return _tableAdapterManager; }
        }

        public bool IsNew
        {
            get { return _isNew; }
        }

        public FormSettingsManager(ICtrlSetting form
                                 , TextBox idTextBox
                                 , Control focusControl
                                 , Button btnNew
                                 , Button btnUpdate
                                 , Button btnDelete
                                 , DataGridView dataGridView
                                 , BindingSource bindingSource
                                 , TTableAdapterManager tableAdapterManager
                                 , TDataSet dataSet)
        {
            _ctrlSetting = form;
            _ctrlSetting.GetActiveForm().KeyDown += FormKeyDown;
            _ctrlSetting.GetActiveForm().KeyPreview = true;
            _idTextBox = idTextBox;
            _focusControl = focusControl;
            _btnNew = new ButtonSettings(btnNew);
            _btnUpdate = new ButtonSettings(btnUpdate);
            _btnDelete = new ButtonSettings(btnDelete);
            _btnOK = _btnUpdate;
            _btnCancel = _btnDelete;
            _dataGridView = dataGridView;
            _dataGridView.MouseDoubleClick += BtnUpdate_Click;
            _bindingSource = bindingSource;
            _tableAdapterManager = tableAdapterManager;
            _dataSet = dataSet;
            //isCancel = false;
            _isNew = false;
            _idTextBox.Hide();
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.MultiSelect = false;
            btnNew.Click += this.BtnNew_Click;
            form.Disposed += Form_FormClosing;
            _controls = new List<Control>();
            _dateTimePickers = new List<DateTimePicker>();
            ChangeEditMode(false);
            _dataGridView.SelectionChanged += DatagridSelectionChanged;
        }

        public FormSettingsManager(ICtrlSetting ctrlSetting
                                 , TextBox idTextBox
                                 , Control focusControl
                                 , ToolStripButton btnNew
                                 , ToolStripButton btnUpdate
                                 , ToolStripButton btnDelete
                                 , Button btnOk
                                 , Button btnCancel
                                 , DataGridView dataGridView
                                 , BindingSource bindingSource
                                 , TTableAdapterManager tableAdapterManager
                                 , TDataSet dataSet)
        {
            _ctrlSetting = ctrlSetting;
            _ctrlSetting.GetActiveForm().KeyDown += FormKeyDown;
            _ctrlSetting.GetActiveForm().KeyPreview = true;
            _idTextBox = idTextBox;
            _focusControl = focusControl;
            _btnNew = new ToolStripButtonSettings(btnNew);
            _btnUpdate = new ToolStripButtonSettings(btnUpdate);
            _btnCancel = new ButtonSettings(btnCancel);
            _btnOK = new ButtonSettings(btnOk);
            _btnDelete = new ToolStripButtonSettings(btnDelete);
            _dataGridView = dataGridView;
            _dataGridView.MouseDoubleClick += BtnUpdate_Click;
            _bindingSource = bindingSource;
            _tableAdapterManager = tableAdapterManager;
            _dataSet = dataSet;
            //isCancel = false;
            _isNew = false;
            _idTextBox.Hide();
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.MultiSelect = false;
            btnNew.Click += this.BtnNew_Click;
            ctrlSetting.Disposed += Form_FormClosing;
            _controls = new List<Control>();
            _dateTimePickers = new List<DateTimePicker>();
            ChangeEditMode(false);
            _dataGridView.SelectionChanged += DatagridSelectionChanged;
        }

        protected virtual void ChangeEditMode(bool editMode)
        {
            _btnCancel.RemoveClickEvent(BtnCancel_Click);
            _btnOK.RemoveClickEvent(BtnOk_Click);
            _btnUpdate.RemoveClickEvent(BtnUpdate_Click);
            _btnDelete.RemoveClickEvent(BtnDelete_Click);

            if (!editMode)
            {
                _btnNew.Enabled();
                _btnOK.Disabled();
                _btnCancel.Disabled();
                _btnUpdate.Enabled();
                _btnDelete.Enabled();
                _btnUpdate.AddClickEvent(BtnUpdate_Click);
                _btnDelete.AddClickEvent(BtnDelete_Click);
                _btnUpdate.Text = "Modifier";
                _btnDelete.Text = "Supprimer";

                foreach (Control control in _controls)
                    control.Enabled = false;

                _dataGridView.Enabled = true;
            }
            else
            {
                _btnUpdate.Disabled();
                _btnDelete.Disabled();
                _btnNew.Disabled();
                _btnOK.Enabled();
                _btnCancel.Enabled();
                _btnCancel.AddClickEvent(BtnCancel_Click);
                _btnOK.AddClickEvent(BtnOk_Click);
                _btnCancel.Text = "Annuler";
                _btnOK.Text = "OK";
                _dataGridView.Enabled = false;

                foreach (Control control in _controls)
                    control.Enabled = true;
            }
        }

        protected virtual void DatagridSelectionChanged(object sender, EventArgs e)
        {
            if (_dataGridView.SelectedRows.Count == 0)
            {
                _btnUpdate.Disabled();
                _btnDelete.Disabled();
            }
            else
            {
                _btnUpdate.Enabled();
                _btnDelete.Enabled();
            }
        }

        virtual protected void BtnNew_Click(object sender, EventArgs e)
        {
            NewData();
        }

        private void NewData()
        {
            _bindingSource.CancelEdit();
            //isCancel = true;
            _bindingSource.AddNew();
            //isCancel = false;
            _isNew = true;
            //btnUpdate.Text = "Ajouter";
            //btnNew.Enabled = false;
            this.ChangeEditMode(true);
            _focusControl.Focus();
            foreach (DateTimePicker dateTimePicker in _dateTimePickers)
            {
                dateTimePicker.Format = DateTimePickerFormat.Custom;
                dateTimePicker.CustomFormat = "dd.MM.yyyy";
                dateTimePicker.Value = DateTime.Now;
                dateTimePicker.Value = dateTimePicker.Value.AddYears(1);
                dateTimePicker.Value = dateTimePicker.Value.AddYears(-1);
            }
        }

        virtual protected void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (_dataGridView.Enabled)
            {
                if (e.KeyCode == Keys.Insert)
                {
                    BtnNew_Click(sender, e);
                }
                else if (_dataGridView.Focused)
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Delete:
                            e.Handled = true;
                            BtnDelete_Click(sender, e);
                            break;

                        case Keys.Enter:
                            e.Handled = true;
                            BtnUpdate_Click(sender, e);
                            break;

                        default:
                            break;
                    }
                }
            }
            else
            {
                switch (e.KeyCode)
                {
                    case Keys.Escape:
                        e.Handled = true;
                        BtnCancel_Click(sender, e);
                        break;

                    case Keys.Enter:
                        e.Handled = true;
                        BtnOk_Click(sender, e);
                        break;

                    default:
                        break;
                }
            }
        }

        virtual protected void BtnOk_Click(object sender, EventArgs e)
        {
            SaveData();
            this.ChangeEditMode(false);
        }

        virtual protected void BtnCancel_Click(object sender, EventArgs e)
        {
            _bindingSource.CancelEdit();
            this.ChangeEditMode(false);
        }

        virtual protected void BtnDelete_Click(object sender, EventArgs e)
        {
            _isDeleted = false;
            if (DataGridView.CurrentRow != null)
            {
                if (_ctrlSetting.CanDelete)
                {
                    if (MessageBox.Show("Êtes-vous sûr de vouloir supprimer cet enregistrement?", "Suppression données", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        _bindingSource.RemoveCurrent();
                        SaveData();
                        ChangeEditMode(false);
                        _isDeleted = true;
                    }
                }
                else
                {
                    MessageBox.Show("Cet enregistrement est utilisé par une autre table\n" +
                        "et donc ne peut être supprimé.", "Suppression impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un enregistrement.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        virtual protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (DataGridView.CurrentRow != null)
            {
                _isNew = false;
                ChangeEditMode(true);
                _focusControl.Focus();
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un enregistrement.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected virtual void SaveData()
        {
            _ctrlSetting.Validate();
            _bindingSource.EndEdit();
        }

        private void Form_FormClosing(object sender, EventArgs e)
        {
            _bindingSource.CancelEdit();
        }

        public void AddControl(Control control)
        {
            _controls.Add(control);
            if (control is DateTimePicker)
            {
                _dateTimePickers.Add((DateTimePicker)control);
            }
            control.Enabled = false;
        }

        protected void SetIsNew(bool value)
        {
            _isNew = value;
        }

        protected bool AllValuesIsInit()
        {
            bool resultat = true;
            for (int i = 0; i < Controls.Length && resultat; i++)
            {
                if (Controls[i] is ComboBox)
                {
                    if (((ComboBox)Controls[i]).SelectedValue == null)
                        resultat = false;
                }
                else if (Controls[i] is TextBox)
                {
                    if (((TextBox)Controls[i]).Text == "")
                        resultat = false;
                }
                else if (Controls[i] is CheckBox)
                {
                    if (((CheckBox)Controls[i]).CheckState == CheckState.Indeterminate)
                        resultat = false;
                }
            }
            return resultat;
        }
    }
}