﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class SplashScreen : Form
    {
        public string Message
        {
            set
            {
                Visible = true;
                lCopyRight.Refresh();
                lText.Refresh();
                lTitre.Refresh();
                lText.Text = value;
                lText.Refresh();
            }
        }

        public SplashScreen()
        {
            InitializeComponent();

            Color c = Color.FromArgb(2, 101, 131);
            lTitre.ForeColor = c;
            lText.ForeColor = c;
            lCopyRight.ForeColor = c;

            var versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);

            lCopyRight.Text = "Gestion des vendanges - Version : " + BUSINESS.Utilities.Version + " " + Assembly.GetExecutingAssembly().GetName().Version.ToString()
                + Environment.NewLine + versionInfo.LegalCopyright; //"\nCopyright © Gerber Systems && Networks 2013";
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            Refresh();
        }

        public void SplashClose()
        {
            timerClose.Start();
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            Opacity -= 0.2;
            if (Opacity == 0)
            {
                timerClose.Stop();
                Close();
            }
        }
    }
}