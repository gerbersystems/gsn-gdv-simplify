﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesCouleurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cOU_NOMLabel;
            System.Windows.Forms.Label cOU_CEPAGELabel;
            System.Windows.Forms.Label mOC_IDLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cOU_COULEURDataGridView = new System.Windows.Forms.DataGridView();
            this.COU_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COU_CEPAGE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cOU_COULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.cOU_IDTextBox = new System.Windows.Forms.TextBox();
            this.cOU_NOMTextBox = new System.Windows.Forms.TextBox();
            this.btnAddCouleur = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOU_CEPAGECheckBox = new System.Windows.Forms.CheckBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            cOU_NOMLabel = new System.Windows.Forms.Label();
            cOU_CEPAGELabel = new System.Windows.Forms.Label();
            mOC_IDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cOU_COULEURDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOU_COULEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cOU_NOMLabel
            // 
            cOU_NOMLabel.AutoSize = true;
            cOU_NOMLabel.Location = new System.Drawing.Point(6, 34);
            cOU_NOMLabel.Name = "cOU_NOMLabel";
            cOU_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cOU_NOMLabel.TabIndex = 18;
            cOU_NOMLabel.Text = "Nom : ";
            // 
            // cOU_CEPAGELabel
            // 
            cOU_CEPAGELabel.AutoSize = true;
            cOU_CEPAGELabel.Location = new System.Drawing.Point(6, 64);
            cOU_CEPAGELabel.Name = "cOU_CEPAGELabel";
            cOU_CEPAGELabel.Size = new System.Drawing.Size(103, 13);
            cOU_CEPAGELabel.TabIndex = 18;
            cOU_CEPAGELabel.Text = "Couleur de cépage :";
            // 
            // mOC_IDLabel
            // 
            mOC_IDLabel.AutoSize = true;
            mOC_IDLabel.Location = new System.Drawing.Point(6, 91);
            mOC_IDLabel.Name = "mOC_IDLabel";
            mOC_IDLabel.Size = new System.Drawing.Size(133, 13);
            mOC_IDLabel.TabIndex = 19;
            mOC_IDLabel.Text = "Mode de comptabilisation :";
            // 
            // cOU_COULEURDataGridView
            // 
            this.cOU_COULEURDataGridView.AllowUserToAddRows = false;
            this.cOU_COULEURDataGridView.AllowUserToDeleteRows = false;
            this.cOU_COULEURDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cOU_COULEURDataGridView.AutoGenerateColumns = false;
            this.cOU_COULEURDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.cOU_COULEURDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cOU_COULEURDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COU_ID,
            this.dataGridViewTextBoxColumn2,
            this.COU_CEPAGE});
            this.cOU_COULEURDataGridView.DataSource = this.cOU_COULEURBindingSource;
            this.cOU_COULEURDataGridView.Location = new System.Drawing.Point(405, 12);
            this.cOU_COULEURDataGridView.Name = "cOU_COULEURDataGridView";
            this.cOU_COULEURDataGridView.ReadOnly = true;
            this.cOU_COULEURDataGridView.RowTemplate.Height = 24;
            this.cOU_COULEURDataGridView.Size = new System.Drawing.Size(271, 235);
            this.cOU_COULEURDataGridView.TabIndex = 15;
            // 
            // COU_ID
            // 
            this.COU_ID.DataPropertyName = "COU_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.COU_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.COU_ID.HeaderText = "Id";
            this.COU_ID.Name = "COU_ID";
            this.COU_ID.ReadOnly = true;
            this.COU_ID.Visible = false;
            this.COU_ID.Width = 70;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "COU_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // COU_CEPAGE
            // 
            this.COU_CEPAGE.DataPropertyName = "COU_CEPAGE";
            this.COU_CEPAGE.HeaderText = "Cépage";
            this.COU_CEPAGE.Name = "COU_CEPAGE";
            this.COU_CEPAGE.ReadOnly = true;
            this.COU_CEPAGE.Width = 60;
            // 
            // cOU_COULEURBindingSource
            // 
            this.cOU_COULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOU_COULEURBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cOU_IDTextBox
            // 
            this.cOU_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.cOU_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cOU_COULEURBindingSource, "COU_ID", true));
            this.cOU_IDTextBox.Enabled = false;
            this.cOU_IDTextBox.Location = new System.Drawing.Point(170, 46);
            this.cOU_IDTextBox.Name = "cOU_IDTextBox";
            this.cOU_IDTextBox.Size = new System.Drawing.Size(131, 19);
            this.cOU_IDTextBox.TabIndex = 17;
            // 
            // cOU_NOMTextBox
            // 
            this.cOU_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cOU_COULEURBindingSource, "COU_NOM", true));
            this.cOU_NOMTextBox.Location = new System.Drawing.Point(146, 31);
            this.cOU_NOMTextBox.Name = "cOU_NOMTextBox";
            this.cOU_NOMTextBox.Size = new System.Drawing.Size(235, 19);
            this.cOU_NOMTextBox.TabIndex = 0;
            // 
            // btnAddCouleur
            // 
            this.btnAddCouleur.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddCouleur.Location = new System.Drawing.Point(144, 158);
            this.btnAddCouleur.Name = "btnAddCouleur";
            this.btnAddCouleur.Size = new System.Drawing.Size(75, 30);
            this.btnAddCouleur.TabIndex = 3;
            this.btnAddCouleur.Text = "Nouveau";
            this.btnAddCouleur.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(225, 158);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(mOC_IDLabel);
            this.groupBox1.Controls.Add(this.mOC_IDComboBox);
            this.groupBox1.Controls.Add(cOU_CEPAGELabel);
            this.groupBox1.Controls.Add(this.cOU_CEPAGECheckBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.cOU_NOMTextBox);
            this.groupBox1.Controls.Add(this.btnAddCouleur);
            this.groupBox1.Controls.Add(cOU_NOMLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(387, 194);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des couleurs";
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mOC_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mOC_IDComboBox.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(146, 88);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(235, 21);
            this.mOC_IDComboBox.TabIndex = 2;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // cOU_CEPAGECheckBox
            // 
            this.cOU_CEPAGECheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.cOU_COULEURBindingSource, "COU_CEPAGE", true));
            this.cOU_CEPAGECheckBox.Location = new System.Drawing.Point(146, 59);
            this.cOU_CEPAGECheckBox.Name = "cOU_CEPAGECheckBox";
            this.cOU_CEPAGECheckBox.Size = new System.Drawing.Size(19, 24);
            this.cOU_CEPAGECheckBox.TabIndex = 1;
            this.cOU_CEPAGECheckBox.UseVisualStyleBackColor = true;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(306, 158);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 5;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = this.cOU_COULEURTableAdapter;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesCouleurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(688, 259);
            this.Controls.Add(this.cOU_IDTextBox);
            this.Controls.Add(this.cOU_COULEURDataGridView);
            this.Controls.Add(this.groupBox1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(680, 245);
            this.Name = "WfrmGestionDesCouleurs";
            this.Text = "Gestion des couleurs";
            this.Load += new System.EventHandler(this.WfrmGestionDesCouleurs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cOU_COULEURDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOU_COULEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource cOU_COULEURBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView cOU_COULEURDataGridView;
        private System.Windows.Forms.TextBox cOU_IDTextBox;
        private System.Windows.Forms.TextBox cOU_NOMTextBox;
        private System.Windows.Forms.Button btnAddCouleur;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.DataGridViewTextBoxColumn COU_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn COU_CEPAGE;
        private System.Windows.Forms.CheckBox cOU_CEPAGECheckBox;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
    }
}