﻿using Gestion_Des_Vendanges.REPORTING;
using GSN.GDV.Reports;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Génère et affiche les rapports de reporting services
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 06.11.2009 - Prise en compte du paramètre "BD_NAME" dans tous les rapports
    * */

    partial class CtrlReportViewer : CtrlWfrm
    {
        #region ShowReport

        public static void ShowSondageMoyen(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new SondageMoyenReport(format, aNNID));
        }

        public static void ShowSondageMoyenParLieuDeProduction(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_SondageMoyenParLieuDeProductionReport(format, aNNID));
        }

        public static void ShowDeclarationEncavage(int aNNID, FormatReport format = FormatReport.PDF)
        {
            showReport(new DeclarationEncavageReport(format, aNNID));
        }

        public static void ShowApports(int? proId, int annId, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ApportsReport(format, annId, proId));
        }

        public static void ShowApportsProducteur(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ApportsParProducteurReport(format, aNNID));
        }

        public static void ShowApportsProducteur(int aNNID, int pEEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ApportsParProducteurReport(format, aNNID, pEEID));
        }

        public static void ShowApportsProducteurGrpByOe(int aNNID, int pEEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new ApportsProducteurGrpByOeReport(format, aNNID, pEEID));
        }

        public static void ShowApportsProducteurGrpByOe(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new ApportsProducteurGrpByOeReport(format, aNNID));
        }

        public static void ShowApportsProducteurMoutRaisin(int aNNID, int pEEID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ApportsParProducteurMoutReport(format, aNNID, pEEID));
        }

        public static void ShowApportsProducteurMoutRaisin(int aNNID, FormatReport format = FormatReport.Apercu)
        {
            showReport(new Gestion_Des_Vendanges_Reports.REPORTING.R_ApportsParProducteurMoutReport(format, aNNID));
        }

        #endregion ShowReport

        private void buildAttestation(object sender, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null, bool isReportAutoLoadedOverride = false)
        {
            int producteurId = (cbProducteur.Enabled && cbProducteur.SelectedValue != null) ? Convert.ToInt32(cbProducteur.SelectedValue) : 0;
            try
            {
                Report rapport = ReportFactory.Create((sender as Control).Name);
                if (rapport.haveProperLicenceLevel())
                {
                    controlsForFiltering(needProducteurFilter: rapport.needProducteurFilter, needDateFilter: rapport.needDateFilter);

                    if (rapport.isReportAutoLoaded || isReportAutoLoadedOverride)
                    {
                        _reportInfo = rapport.GetReportInfo(format: FormatReport.Apercu, annId: _idAnneeCourante, proId: producteurId, dateRangeFrom: dateRangeFrom, dateRangeTo: dateRangeTo);
                        showAttestation(rapport.isRDLC);
                    }
                }
            }
            catch (AccessViolationException)
            {
                MessageBox.Show("Vous n'avez pas la licence pour afficher ce rapport.\nCommander la version étendue pour gérer les paiements des fournisseurs.", "Licence incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Erreur avec le rapport.", "Erreur rapports", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void opt_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                toolLabelPage.Text = "sur ";
                if (sender != optPaiements && sender != optBonusDetail)
                    btnFilterDate.Enabled = false;
                buildAttestation(sender);
            }
        }

        private void datePickerFrom_Validated(object sender, EventArgs e)
        {
            if (!isDateRangeValid(additionalErrorMessage: "\nLa date de début ne peut être supérieure à la date de fin."))
                _dateRangeFrom = _dateRangeTo;
        }

        private void datePickerTo_Validated(object sender, EventArgs e)
        {
            if (!isDateRangeValid(additionalErrorMessage: "\nLa date de fin ne peut être inférieure à la date de début."))
                _dateRangeTo = _dateRangeFrom;
        }

        private void controlsForFiltering(bool needProducteurFilter, bool needDateFilter)
        {
            changeControlVisibility(controlList: ctrlProducteurFilter, setVisibilityStateTo: needProducteurFilter);
            changeControlVisibility(controlList: ctrlDateFilter, setVisibilityStateTo: needDateFilter);
        }

        private void changeControlVisibility(List<Control> controlList, bool setVisibilityStateTo)
        {
            foreach (var ctrl in controlList)
                ctrl.Visible = setVisibilityStateTo;
        }

        private bool isDateRangeValid(string additionalErrorMessage = "")
        {
            if (datePickerFrom.Value != null && datePickerTo.Value != null && datePickerFrom.Value <= datePickerTo.Value)
                return true;
            else
                MessageBox.Show(string.Concat("Veuillez vérifier la date sélectionnée.", additionalErrorMessage), "Dates invalides", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;
        }
    }
}