﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesDistricts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cLA_NOMLabel;
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.dIS_DISTRICTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dIS_DISTRICTTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.dIS_DISTRICTDataGridView = new System.Windows.Forms.DataGridView();
            this.DIS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dIS_IDTextBox = new System.Windows.Forms.TextBox();
            this.dIS_NOMTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnAddClass = new System.Windows.Forms.Button();
            this.btnSaveClass = new System.Windows.Forms.Button();
            cLA_NOMLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIS_DISTRICTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIS_DISTRICTDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cLA_NOMLabel
            // 
            cLA_NOMLabel.AutoSize = true;
            cLA_NOMLabel.Location = new System.Drawing.Point(6, 37);
            cLA_NOMLabel.Name = "cLA_NOMLabel";
            cLA_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cLA_NOMLabel.TabIndex = 15;
            cLA_NOMLabel.Text = "Nom : ";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dIS_DISTRICTBindingSource
            // 
            this.dIS_DISTRICTBindingSource.DataMember = "DIS_DISTRICT";
            this.dIS_DISTRICTBindingSource.DataSource = this.dSPesees;
            // 
            // dIS_DISTRICTTableAdapter
            // 
            this.dIS_DISTRICTTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = this.dIS_DISTRICTTableAdapter;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // dIS_DISTRICTDataGridView
            // 
            this.dIS_DISTRICTDataGridView.AllowUserToAddRows = false;
            this.dIS_DISTRICTDataGridView.AllowUserToDeleteRows = false;
            this.dIS_DISTRICTDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dIS_DISTRICTDataGridView.AutoGenerateColumns = false;
            this.dIS_DISTRICTDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.dIS_DISTRICTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dIS_DISTRICTDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DIS_ID,
            this.dataGridViewTextBoxColumn2});
            this.dIS_DISTRICTDataGridView.DataSource = this.dIS_DISTRICTBindingSource;
            this.dIS_DISTRICTDataGridView.Location = new System.Drawing.Point(379, 12);
            this.dIS_DISTRICTDataGridView.Name = "dIS_DISTRICTDataGridView";
            this.dIS_DISTRICTDataGridView.ReadOnly = true;
            this.dIS_DISTRICTDataGridView.Size = new System.Drawing.Size(231, 246);
            this.dIS_DISTRICTDataGridView.TabIndex = 2;
            // 
            // DIS_ID
            // 
            this.DIS_ID.DataPropertyName = "DIS_ID";
            this.DIS_ID.HeaderText = "DIS_ID";
            this.DIS_ID.Name = "DIS_ID";
            this.DIS_ID.ReadOnly = true;
            this.DIS_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DIS_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "District";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dIS_IDTextBox
            // 
            this.dIS_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dIS_DISTRICTBindingSource, "DIS_ID", true));
            this.dIS_IDTextBox.Enabled = false;
            this.dIS_IDTextBox.Location = new System.Drawing.Point(242, 12);
            this.dIS_IDTextBox.Name = "dIS_IDTextBox";
            this.dIS_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.dIS_IDTextBox.TabIndex = 4;
            // 
            // dIS_NOMTextBox
            // 
            this.dIS_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dIS_DISTRICTBindingSource, "DIS_NOM", true));
            this.dIS_NOMTextBox.Location = new System.Drawing.Point(118, 34);
            this.dIS_NOMTextBox.Name = "dIS_NOMTextBox";
            this.dIS_NOMTextBox.Size = new System.Drawing.Size(237, 20);
            this.dIS_NOMTextBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnAddClass);
            this.groupBox1.Controls.Add(cLA_NOMLabel);
            this.groupBox1.Controls.Add(this.dIS_NOMTextBox);
            this.groupBox1.Controls.Add(this.btnSaveClass);
            this.groupBox1.Controls.Add(this.dIS_IDTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(361, 123);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des disticts";
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(280, 87);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnAddClass
            // 
            this.btnAddClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddClass.Location = new System.Drawing.Point(118, 87);
            this.btnAddClass.Name = "btnAddClass";
            this.btnAddClass.Size = new System.Drawing.Size(75, 30);
            this.btnAddClass.TabIndex = 1;
            this.btnAddClass.Text = "Nouveau";
            this.btnAddClass.UseVisualStyleBackColor = false;
            // 
            // btnSaveClass
            // 
            this.btnSaveClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveClass.Location = new System.Drawing.Point(199, 87);
            this.btnSaveClass.Name = "btnSaveClass";
            this.btnSaveClass.Size = new System.Drawing.Size(75, 30);
            this.btnSaveClass.TabIndex = 2;
            this.btnSaveClass.Text = "Modifier";
            this.btnSaveClass.UseVisualStyleBackColor = false;
            // 
            // WfrmGestionDesDistricts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 270);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dIS_DISTRICTDataGridView);
            this.MinimumSize = new System.Drawing.Size(600, 250);
            this.Name = "WfrmGestionDesDistricts";
            this.Text = "Gestion des districts";
            this.Load += new System.EventHandler(this.WfrmGestionDesDistricts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIS_DISTRICTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIS_DISTRICTDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource dIS_DISTRICTBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter dIS_DISTRICTTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView dIS_DISTRICTDataGridView;
        private System.Windows.Forms.TextBox dIS_IDTextBox;
        private System.Windows.Forms.TextBox dIS_NOMTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnAddClass;
        private System.Windows.Forms.Button btnSaveClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIS_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}