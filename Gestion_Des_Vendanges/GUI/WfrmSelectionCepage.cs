﻿using System.Collections.Generic;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSelectionCepage : WfrmSelection
    {
        private class Cepage
        {
            private string _nom;
            private int _id;

            public string Nom
            {
                get
                {
                    return _nom;
                }
            }

            public int Id
            {
                get
                {
                    return _id;
                }
            }

            public Cepage(string nom, int id)
            {
                _nom = nom;
                _id = id;
            }

            public override string ToString()
            {
                return Nom;
            }
        }

        public int CepId
        {
            get
            {
                try
                {
                    return ((Cepage)Selection).Id;
                }
                catch
                {
                    return -1;
                }
            }
        }

        public WfrmSelectionCepage()
            : base("Gestion des vendanges - Sélection du cépage", "Cépage")
        {
            DAO.DSPesees dsPesees = new DAO.DSPesees();
            DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cepTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter>();
            cepTable.Fill(dsPesees.CEP_CEPAGE);
            List<Cepage> cepages = new List<Cepage>();
            foreach (DAO.DSPesees.CEP_CEPAGERow row in cepTable.GetData().Rows)
            {
                cepages.Add(new Cepage(row.CEP_NOM, row.CEP_ID));
            }
            Choix = cepages.ToArray();
        }
    }
}