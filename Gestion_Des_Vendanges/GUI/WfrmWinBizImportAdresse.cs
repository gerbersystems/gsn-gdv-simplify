﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de sélection de l'année pour la synchronisation des adresses WinBIZ
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    public partial class WfrmWinBizImportAdresse : WfrmBase
    {
        private WinBizFolder _winBizFolder;
        /*private static WfrmWinBizImportAdresse _wfrm;
        public static WfrmWinBizImportAdresse Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmWinBizImportAdresse();
                }
                return _wfrm;
            }
        }*/

        public WfrmWinBizImportAdresse()
        {
            InitializeComponent();
            _winBizFolder = new WinBizFolder();
            updateItemCbAnnee();
        }

        private void updateItemCbAnnee()
        {
            cbAnnee.Items.Clear();
            cbAnnee.Enabled = true;
            int anMax = 0;
            foreach (int an in _winBizFolder.Annees)
            {
                if (an > anMax)
                {
                    anMax = an;
                }
                cbAnnee.Items.Add(an);
            }
            if (cbAnnee.Items.Count == 0)
            {
                throw new GVException("Aucune année disponible");
            }
            else
            {
                cbAnnee.SelectedItem = anMax;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnImporter_Click(object sender, EventArgs e)
        {
            int an = (int)cbAnnee.SelectedItem;
            try
            {
                DAO.WinBizControleur winBizControleur = new Gestion_Des_Vendanges.DAO.WinBizControleur(an, false);
                DAO.BDVendangeControleur bdVendangeControleur = new Gestion_Des_Vendanges.DAO.BDVendangeControleur();
                bdVendangeControleur.UpdateModeComptabilisations(BUSINESS.Utilities.IdAnneeCourante);
                bdVendangeControleur.ImporterAdresses(winBizControleur.GetAdresses());
                MessageBox.Show("Adresses importées", "Import adresses", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Veuillez sélectionner une année.", "Erreur sélection année", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                System.Windows.Forms.MessageBox.Show("Erreur, le dossier WinBIZ de l'année: " + an + " est introuvable.\n",
                    "Erreur, dossier introuvable", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
            }
        }
    }
}