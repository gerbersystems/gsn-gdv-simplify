﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des "Classes"
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesClasses : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesClasses _wfrm;

        public static WfrmGestionDesClasses Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesClasses();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int claId = (int)cLA_CLASSEDataGridView.CurrentRow.Cells["CLA_ID"].Value;
                    resultat = (cLA_CLASSETableAdapter.NbRef(claId) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesClasses()
        {
            InitializeComponent();
            // isCancelEdit = false;
        }

        private void WfrmGestionDesClasses_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(cLA_CLASSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            cLA_CLASSETableAdapter.Fill(dSPesees.CLA_CLASSE);
            InitSpecificTableAdapters();

            /* Inscription de l'année dans le titre de la fenêtre*/
            String s = "Gestion des vendanges";
            if ((BUSINESS.Utilities.IdAnneeCourante != 0))
                s = s + " - " + Convert.ToString(BUSINESS.Utilities.AnneeCourante);

            this.Text = s;

            InitManager();
            _manager.AddControl(cLA_NOMTextBox);
            _manager.AddControl(cLA_NUMEROTextBox);
            _manager.AddControl(cLA_NUMERO_CSCVTextBox);
        }
    }
}