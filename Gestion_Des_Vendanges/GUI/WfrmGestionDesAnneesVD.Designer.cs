﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesAnnees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label aNN_ANLabel;
            System.Windows.Forms.Label aDR_IDLabel;
            System.Windows.Forms.Label aNN_ENCAVEUR_NOMLabel;
            System.Windows.Forms.Label aNN_ENCAVEUR_PRENOMLabel;
            System.Windows.Forms.Label aNN_ENCAVEUR_SOUMIS_CONTROLLabel;
            System.Windows.Forms.Label aNN_RESPONSABLE_1Label;
            System.Windows.Forms.Label aNN_RESPONSABLE_4Label;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label aNN_ENCAVEUR_NUMEROLabel;
            System.Windows.Forms.Label label3;
            this.mOC_IDLabel = new System.Windows.Forms.Label();
            this.aNN_ANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aNN_ANTextBox = new System.Windows.Forms.TextBox();
            this.btnAddYear = new System.Windows.Forms.Button();
            this.btnSaveYear = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.aNN_IS_PRIX_KGRadioButton = new System.Windows.Forms.RadioButton();
            this.optLitres = new System.Windows.Forms.RadioButton();
            this.lPrix = new System.Windows.Forms.Label();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ENCAVEUR_SOCIETETextBox = new System.Windows.Forms.TextBox();
            this.aNN_ENCAVEUR_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.aNN_RESPONSABLE_4TextBox = new System.Windows.Forms.TextBox();
            this.aNN_RESPONSABLE_3TextBox = new System.Windows.Forms.TextBox();
            this.aNN_RESPONSABLE_2TextBox = new System.Windows.Forms.TextBox();
            this.aNN_RESPONSABLE_1TextBox = new System.Windows.Forms.TextBox();
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox = new System.Windows.Forms.CheckBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.aNN_ENCAVEUR_PRENOMTextBox = new System.Windows.Forms.TextBox();
            this.aNN_ENCAVEUR_NOMTextBox = new System.Windows.Forms.TextBox();
            this.aDR_IDComboBox = new System.Windows.Forms.ComboBox();
            this.AdressesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AdressesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adresseCompletBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_IDTextBox = new System.Windows.Forms.TextBox();
            this.aNN_ANNEEDataGridView = new System.Windows.Forms.DataGridView();
            this.adresseCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            this.ANN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_TAUX_TVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_AN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ENCAVEUR_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ENCAVEUR_SOCIETE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ENCAVEUR_NOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ENCAVEUR_PRENOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADR_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ANN_ENCAVEUR_SOUMIS_CONTROL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ANN_RESPONSABLE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_RESPONSABLE_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_RESPONSABLE_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_RESPONSABLE_4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOC_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_IS_PRIX_KG = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            aNN_ANLabel = new System.Windows.Forms.Label();
            aDR_IDLabel = new System.Windows.Forms.Label();
            aNN_ENCAVEUR_NOMLabel = new System.Windows.Forms.Label();
            aNN_ENCAVEUR_PRENOMLabel = new System.Windows.Forms.Label();
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel = new System.Windows.Forms.Label();
            aNN_RESPONSABLE_1Label = new System.Windows.Forms.Label();
            aNN_RESPONSABLE_4Label = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            aNN_ENCAVEUR_NUMEROLabel = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            this.AdressesContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // aNN_ANLabel
            // 
            aNN_ANLabel.AutoSize = true;
            aNN_ANLabel.Location = new System.Drawing.Point(22, 34);
            aNN_ANLabel.Name = "aNN_ANLabel";
            aNN_ANLabel.Size = new System.Drawing.Size(44, 13);
            aNN_ANLabel.TabIndex = 11;
            aNN_ANLabel.Text = "Année :";
            // 
            // aDR_IDLabel
            // 
            aDR_IDLabel.AutoSize = true;
            aDR_IDLabel.Location = new System.Drawing.Point(22, 85);
            aDR_IDLabel.Name = "aDR_IDLabel";
            aDR_IDLabel.Size = new System.Drawing.Size(49, 13);
            aDR_IDLabel.TabIndex = 16;
            aDR_IDLabel.Text = "Société :";
            // 
            // aNN_ENCAVEUR_NOMLabel
            // 
            aNN_ENCAVEUR_NOMLabel.AutoSize = true;
            aNN_ENCAVEUR_NOMLabel.Location = new System.Drawing.Point(22, 110);
            aNN_ENCAVEUR_NOMLabel.Name = "aNN_ENCAVEUR_NOMLabel";
            aNN_ENCAVEUR_NOMLabel.Size = new System.Drawing.Size(83, 13);
            aNN_ENCAVEUR_NOMLabel.TabIndex = 19;
            aNN_ENCAVEUR_NOMLabel.Text = "Nom encaveur :";
            // 
            // aNN_ENCAVEUR_PRENOMLabel
            // 
            aNN_ENCAVEUR_PRENOMLabel.AutoSize = true;
            aNN_ENCAVEUR_PRENOMLabel.Location = new System.Drawing.Point(22, 136);
            aNN_ENCAVEUR_PRENOMLabel.Name = "aNN_ENCAVEUR_PRENOMLabel";
            aNN_ENCAVEUR_PRENOMLabel.Size = new System.Drawing.Size(97, 13);
            aNN_ENCAVEUR_PRENOMLabel.TabIndex = 20;
            aNN_ENCAVEUR_PRENOMLabel.Text = "Prénom encaveur :";
            // 
            // aNN_ENCAVEUR_SOUMIS_CONTROLLabel
            // 
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.AutoSize = true;
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.Location = new System.Drawing.Point(399, 137);
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.Name = "aNN_ENCAVEUR_SOUMIS_CONTROLLabel";
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.Size = new System.Drawing.Size(103, 13);
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.TabIndex = 20;
            aNN_ENCAVEUR_SOUMIS_CONTROLLabel.Text = "Soumis au contrôle :";
            // 
            // aNN_RESPONSABLE_1Label
            // 
            aNN_RESPONSABLE_1Label.AutoSize = true;
            aNN_RESPONSABLE_1Label.Location = new System.Drawing.Point(399, 34);
            aNN_RESPONSABLE_1Label.Name = "aNN_RESPONSABLE_1Label";
            aNN_RESPONSABLE_1Label.Size = new System.Drawing.Size(81, 13);
            aNN_RESPONSABLE_1Label.TabIndex = 21;
            aNN_RESPONSABLE_1Label.Text = "Responsable 1:";
            // 
            // aNN_RESPONSABLE_4Label
            // 
            aNN_RESPONSABLE_4Label.AutoSize = true;
            aNN_RESPONSABLE_4Label.Location = new System.Drawing.Point(399, 110);
            aNN_RESPONSABLE_4Label.Name = "aNN_RESPONSABLE_4Label";
            aNN_RESPONSABLE_4Label.Size = new System.Drawing.Size(81, 13);
            aNN_RESPONSABLE_4Label.TabIndex = 24;
            aNN_RESPONSABLE_4Label.Text = "Responsable 4:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(399, 59);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(81, 13);
            label1.TabIndex = 26;
            label1.Text = "Responsable 2:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(399, 85);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(81, 13);
            label2.TabIndex = 27;
            label2.Text = "Responsable 3:";
            // 
            // aNN_ENCAVEUR_NUMEROLabel
            // 
            aNN_ENCAVEUR_NUMEROLabel.AutoSize = true;
            aNN_ENCAVEUR_NUMEROLabel.Location = new System.Drawing.Point(22, 59);
            aNN_ENCAVEUR_NUMEROLabel.Name = "aNN_ENCAVEUR_NUMEROLabel";
            aNN_ENCAVEUR_NUMEROLabel.Size = new System.Drawing.Size(106, 13);
            aNN_ENCAVEUR_NUMEROLabel.TabIndex = 27;
            aNN_ENCAVEUR_NUMEROLabel.Text = "Numéro d\'encaveur :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(22, 164);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(51, 13);
            label3.TabIndex = 30;
            label3.Text = "Adresse :";
            // 
            // mOC_IDLabel
            // 
            this.mOC_IDLabel.AutoSize = true;
            this.mOC_IDLabel.Location = new System.Drawing.Point(399, 164);
            this.mOC_IDLabel.Name = "mOC_IDLabel";
            this.mOC_IDLabel.Size = new System.Drawing.Size(133, 13);
            this.mOC_IDLabel.TabIndex = 30;
            this.mOC_IDLabel.Text = "Mode de comptabilisation :";
            // 
            // aNN_ANNEEBindingSource
            // 
            this.aNN_ANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNN_ANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = this.aNN_ANNEETableAdapter;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aNN_ANTextBox
            // 
            this.aNN_ANTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_AN", true));
            this.aNN_ANTextBox.Location = new System.Drawing.Point(145, 31);
            this.aNN_ANTextBox.Name = "aNN_ANTextBox";
            this.aNN_ANTextBox.ReadOnly = true;
            this.aNN_ANTextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_ANTextBox.TabIndex = 1;
            // 
            // btnAddYear
            // 
            this.btnAddYear.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddYear.Location = new System.Drawing.Point(508, 230);
            this.btnAddYear.Name = "btnAddYear";
            this.btnAddYear.Size = new System.Drawing.Size(75, 30);
            this.btnAddYear.TabIndex = 15;
            this.btnAddYear.Text = "Nouveau";
            this.btnAddYear.UseVisualStyleBackColor = false;
            // 
            // btnSaveYear
            // 
            this.btnSaveYear.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveYear.Location = new System.Drawing.Point(591, 230);
            this.btnSaveYear.Name = "btnSaveYear";
            this.btnSaveYear.Size = new System.Drawing.Size(75, 30);
            this.btnSaveYear.TabIndex = 16;
            this.btnSaveYear.Text = "Ajouter";
            this.btnSaveYear.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.aNN_IS_PRIX_KGRadioButton);
            this.groupBox1.Controls.Add(this.optLitres);
            this.groupBox1.Controls.Add(this.lPrix);
            this.groupBox1.Controls.Add(this.mOC_IDLabel);
            this.groupBox1.Controls.Add(this.mOC_IDComboBox);
            this.groupBox1.Controls.Add(this.aNN_ENCAVEUR_SOCIETETextBox);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(aNN_ENCAVEUR_NUMEROLabel);
            this.groupBox1.Controls.Add(this.aNN_ENCAVEUR_NUMEROTextBox);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(aNN_RESPONSABLE_4Label);
            this.groupBox1.Controls.Add(this.aNN_RESPONSABLE_4TextBox);
            this.groupBox1.Controls.Add(this.aNN_RESPONSABLE_3TextBox);
            this.groupBox1.Controls.Add(this.aNN_RESPONSABLE_2TextBox);
            this.groupBox1.Controls.Add(aNN_RESPONSABLE_1Label);
            this.groupBox1.Controls.Add(this.aNN_RESPONSABLE_1TextBox);
            this.groupBox1.Controls.Add(aNN_ENCAVEUR_SOUMIS_CONTROLLabel);
            this.groupBox1.Controls.Add(this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox);
            this.groupBox1.Controls.Add(aNN_ENCAVEUR_PRENOMLabel);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.aNN_ENCAVEUR_PRENOMTextBox);
            this.groupBox1.Controls.Add(aNN_ENCAVEUR_NOMLabel);
            this.groupBox1.Controls.Add(this.aNN_ENCAVEUR_NOMTextBox);
            this.groupBox1.Controls.Add(aDR_IDLabel);
            this.groupBox1.Controls.Add(this.aDR_IDComboBox);
            this.groupBox1.Controls.Add(this.aNN_ANTextBox);
            this.groupBox1.Controls.Add(this.btnAddYear);
            this.groupBox1.Controls.Add(this.btnSaveYear);
            this.groupBox1.Controls.Add(aNN_ANLabel);
            this.groupBox1.Controls.Add(this.aNN_IDTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(753, 266);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie paramètres de l\'année";
            // 
            // aNN_IS_PRIX_KGRadioButton
            // 
            this.aNN_IS_PRIX_KGRadioButton.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.aNN_ANNEEBindingSource, "ANN_IS_PRIX_KG", true));
            this.aNN_IS_PRIX_KGRadioButton.Location = new System.Drawing.Point(145, 191);
            this.aNN_IS_PRIX_KGRadioButton.Name = "aNN_IS_PRIX_KGRadioButton";
            this.aNN_IS_PRIX_KGRadioButton.Size = new System.Drawing.Size(50, 24);
            this.aNN_IS_PRIX_KGRadioButton.TabIndex = 7;
            this.aNN_IS_PRIX_KGRadioButton.TabStop = true;
            this.aNN_IS_PRIX_KGRadioButton.Text = "Kg";
            this.aNN_IS_PRIX_KGRadioButton.UseVisualStyleBackColor = true;
            // 
            // optLitres
            // 
            this.optLitres.AutoSize = true;
            this.optLitres.Checked = true;
            this.optLitres.Location = new System.Drawing.Point(224, 195);
            this.optLitres.Name = "optLitres";
            this.optLitres.Size = new System.Drawing.Size(50, 17);
            this.optLitres.TabIndex = 8;
            this.optLitres.TabStop = true;
            this.optLitres.Text = "Litres";
            this.optLitres.UseVisualStyleBackColor = true;
            // 
            // lPrix
            // 
            this.lPrix.AutoSize = true;
            this.lPrix.Location = new System.Drawing.Point(22, 197);
            this.lPrix.Name = "lPrix";
            this.lPrix.Size = new System.Drawing.Size(96, 13);
            this.lPrix.TabIndex = 32;
            this.lPrix.Text = "Calcul des prix en :";
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aNN_ANNEEBindingSource, "MOC_ID", true));
            this.mOC_IDComboBox.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(538, 161);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(209, 21);
            this.mOC_IDComboBox.TabIndex = 14;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ENCAVEUR_SOCIETETextBox
            // 
            this.aNN_ENCAVEUR_SOCIETETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_ENCAVEUR_SOCIETE", true));
            this.aNN_ENCAVEUR_SOCIETETextBox.Location = new System.Drawing.Point(145, 82);
            this.aNN_ENCAVEUR_SOCIETETextBox.Name = "aNN_ENCAVEUR_SOCIETETextBox";
            this.aNN_ENCAVEUR_SOCIETETextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_ENCAVEUR_SOCIETETextBox.TabIndex = 3;
            // 
            // aNN_ENCAVEUR_NUMEROTextBox
            // 
            this.aNN_ENCAVEUR_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_ENCAVEUR_NUMERO", true));
            this.aNN_ENCAVEUR_NUMEROTextBox.Location = new System.Drawing.Point(145, 56);
            this.aNN_ENCAVEUR_NUMEROTextBox.Name = "aNN_ENCAVEUR_NUMEROTextBox";
            this.aNN_ENCAVEUR_NUMEROTextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_ENCAVEUR_NUMEROTextBox.TabIndex = 2;
            // 
            // aNN_RESPONSABLE_4TextBox
            // 
            this.aNN_RESPONSABLE_4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_RESPONSABLE_4", true));
            this.aNN_RESPONSABLE_4TextBox.Location = new System.Drawing.Point(538, 107);
            this.aNN_RESPONSABLE_4TextBox.Name = "aNN_RESPONSABLE_4TextBox";
            this.aNN_RESPONSABLE_4TextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_RESPONSABLE_4TextBox.TabIndex = 12;
            // 
            // aNN_RESPONSABLE_3TextBox
            // 
            this.aNN_RESPONSABLE_3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_RESPONSABLE_3", true));
            this.aNN_RESPONSABLE_3TextBox.Location = new System.Drawing.Point(538, 82);
            this.aNN_RESPONSABLE_3TextBox.Name = "aNN_RESPONSABLE_3TextBox";
            this.aNN_RESPONSABLE_3TextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_RESPONSABLE_3TextBox.TabIndex = 11;
            // 
            // aNN_RESPONSABLE_2TextBox
            // 
            this.aNN_RESPONSABLE_2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_RESPONSABLE_2", true));
            this.aNN_RESPONSABLE_2TextBox.Location = new System.Drawing.Point(538, 56);
            this.aNN_RESPONSABLE_2TextBox.Name = "aNN_RESPONSABLE_2TextBox";
            this.aNN_RESPONSABLE_2TextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_RESPONSABLE_2TextBox.TabIndex = 10;
            // 
            // aNN_RESPONSABLE_1TextBox
            // 
            this.aNN_RESPONSABLE_1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_RESPONSABLE_1", true));
            this.aNN_RESPONSABLE_1TextBox.Location = new System.Drawing.Point(538, 31);
            this.aNN_RESPONSABLE_1TextBox.Name = "aNN_RESPONSABLE_1TextBox";
            this.aNN_RESPONSABLE_1TextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_RESPONSABLE_1TextBox.TabIndex = 9;
            // 
            // aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox
            // 
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.aNN_ANNEEBindingSource, "ANN_ENCAVEUR_SOUMIS_CONTROL", true));
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.Location = new System.Drawing.Point(538, 132);
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.Name = "aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox";
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.Size = new System.Drawing.Size(70, 24);
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.TabIndex = 13;
            this.aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.UseVisualStyleBackColor = true;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(672, 230);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 17;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // aNN_ENCAVEUR_PRENOMTextBox
            // 
            this.aNN_ENCAVEUR_PRENOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_ENCAVEUR_PRENOM", true));
            this.aNN_ENCAVEUR_PRENOMTextBox.Location = new System.Drawing.Point(145, 133);
            this.aNN_ENCAVEUR_PRENOMTextBox.Name = "aNN_ENCAVEUR_PRENOMTextBox";
            this.aNN_ENCAVEUR_PRENOMTextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_ENCAVEUR_PRENOMTextBox.TabIndex = 5;
            // 
            // aNN_ENCAVEUR_NOMTextBox
            // 
            this.aNN_ENCAVEUR_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_ENCAVEUR_NOM", true));
            this.aNN_ENCAVEUR_NOMTextBox.Location = new System.Drawing.Point(145, 107);
            this.aNN_ENCAVEUR_NOMTextBox.Name = "aNN_ENCAVEUR_NOMTextBox";
            this.aNN_ENCAVEUR_NOMTextBox.Size = new System.Drawing.Size(209, 19);
            this.aNN_ENCAVEUR_NOMTextBox.TabIndex = 4;
            // 
            // aDR_IDComboBox
            // 
            this.aDR_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.aDR_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aDR_IDComboBox.ContextMenuStrip = this.AdressesContextMenuStrip;
            this.aDR_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aNN_ANNEEBindingSource, "ADR_ID", true));
            this.aDR_IDComboBox.DataSource = this.adresseCompletBindingSource;
            this.aDR_IDComboBox.DisplayMember = "ADR_COMPLET";
            this.aDR_IDComboBox.FormattingEnabled = true;
            this.aDR_IDComboBox.Location = new System.Drawing.Point(145, 161);
            this.aDR_IDComboBox.Name = "aDR_IDComboBox";
            this.aDR_IDComboBox.Size = new System.Drawing.Size(209, 21);
            this.aDR_IDComboBox.TabIndex = 6;
            this.aDR_IDComboBox.ValueMember = "ADR_ID";
            // 
            // AdressesContextMenuStrip
            // 
            this.AdressesContextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.AdressesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdressesToolStripMenuItem});
            this.AdressesContextMenuStrip.Name = "AdressesContextMenuStrip";
            this.AdressesContextMenuStrip.Size = new System.Drawing.Size(198, 26);
            // 
            // AdressesToolStripMenuItem
            // 
            this.AdressesToolStripMenuItem.Name = "AdressesToolStripMenuItem";
            this.AdressesToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.AdressesToolStripMenuItem.Text = "Accéder aux adresses ...";
            this.AdressesToolStripMenuItem.Click += new System.EventHandler(this.AdressesContextMenuStrip_Click);
            // 
            // adresseCompletBindingSource
            // 
            this.adresseCompletBindingSource.DataMember = "AdresseComplet";
            this.adresseCompletBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_IDTextBox
            // 
            this.aNN_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aNN_ANNEEBindingSource, "ANN_ID", true));
            this.aNN_IDTextBox.Location = new System.Drawing.Point(158, 31);
            this.aNN_IDTextBox.Name = "aNN_IDTextBox";
            this.aNN_IDTextBox.Size = new System.Drawing.Size(100, 19);
            this.aNN_IDTextBox.TabIndex = 16;
            // 
            // aNN_ANNEEDataGridView
            // 
            this.aNN_ANNEEDataGridView.AllowUserToAddRows = false;
            this.aNN_ANNEEDataGridView.AllowUserToDeleteRows = false;
            this.aNN_ANNEEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aNN_ANNEEDataGridView.AutoGenerateColumns = false;
            this.aNN_ANNEEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aNN_ANNEEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aNN_ANNEEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ANN_ID,
            this.ANN_TAUX_TVA,
            this.ANN_AN,
            this.ANN_ENCAVEUR_NUMERO,
            this.ANN_ENCAVEUR_SOCIETE,
            this.ANN_ENCAVEUR_NOM,
            this.ANN_ENCAVEUR_PRENOM,
            this.ADR_ID,
            this.ANN_ENCAVEUR_SOUMIS_CONTROL,
            this.ANN_RESPONSABLE_1,
            this.ANN_RESPONSABLE_2,
            this.ANN_RESPONSABLE_3,
            this.ANN_RESPONSABLE_4,
            this.MOC_ID,
            this.ANN_IS_PRIX_KG});
            this.aNN_ANNEEDataGridView.DataSource = this.aNN_ANNEEBindingSource;
            this.aNN_ANNEEDataGridView.Location = new System.Drawing.Point(12, 284);
            this.aNN_ANNEEDataGridView.Name = "aNN_ANNEEDataGridView";
            this.aNN_ANNEEDataGridView.ReadOnly = true;
            this.aNN_ANNEEDataGridView.Size = new System.Drawing.Size(753, 266);
            this.aNN_ANNEEDataGridView.TabIndex = 16;
            this.aNN_ANNEEDataGridView.TabStop = false;
            // 
            // adresseCompletTableAdapter
            // 
            this.adresseCompletTableAdapter.ClearBeforeFill = true;
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // ANN_ID
            // 
            this.ANN_ID.DataPropertyName = "ANN_ID";
            this.ANN_ID.HeaderText = "ANN_ID";
            this.ANN_ID.Name = "ANN_ID";
            this.ANN_ID.ReadOnly = true;
            this.ANN_ID.Visible = false;
            // 
            // ANN_TAUX_TVA
            // 
            this.ANN_TAUX_TVA.DataPropertyName = "ANN_TAUX_TVA";
            this.ANN_TAUX_TVA.HeaderText = "ANN_TAUX_TVA";
            this.ANN_TAUX_TVA.Name = "ANN_TAUX_TVA";
            this.ANN_TAUX_TVA.ReadOnly = true;
            this.ANN_TAUX_TVA.Visible = false;
            // 
            // ANN_AN
            // 
            this.ANN_AN.DataPropertyName = "ANN_AN";
            this.ANN_AN.HeaderText = "Année";
            this.ANN_AN.Name = "ANN_AN";
            this.ANN_AN.ReadOnly = true;
            this.ANN_AN.Width = 70;
            // 
            // ANN_ENCAVEUR_NUMERO
            // 
            this.ANN_ENCAVEUR_NUMERO.DataPropertyName = "ANN_ENCAVEUR_NUMERO";
            this.ANN_ENCAVEUR_NUMERO.HeaderText = "N°";
            this.ANN_ENCAVEUR_NUMERO.Name = "ANN_ENCAVEUR_NUMERO";
            this.ANN_ENCAVEUR_NUMERO.ReadOnly = true;
            this.ANN_ENCAVEUR_NUMERO.Visible = false;
            this.ANN_ENCAVEUR_NUMERO.Width = 40;
            // 
            // ANN_ENCAVEUR_SOCIETE
            // 
            this.ANN_ENCAVEUR_SOCIETE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ANN_ENCAVEUR_SOCIETE.DataPropertyName = "ANN_ENCAVEUR_SOCIETE";
            this.ANN_ENCAVEUR_SOCIETE.HeaderText = "Société";
            this.ANN_ENCAVEUR_SOCIETE.MinimumWidth = 120;
            this.ANN_ENCAVEUR_SOCIETE.Name = "ANN_ENCAVEUR_SOCIETE";
            this.ANN_ENCAVEUR_SOCIETE.ReadOnly = true;
            // 
            // ANN_ENCAVEUR_NOM
            // 
            this.ANN_ENCAVEUR_NOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ANN_ENCAVEUR_NOM.DataPropertyName = "ANN_ENCAVEUR_NOM";
            this.ANN_ENCAVEUR_NOM.HeaderText = "Nom encaveur";
            this.ANN_ENCAVEUR_NOM.MinimumWidth = 120;
            this.ANN_ENCAVEUR_NOM.Name = "ANN_ENCAVEUR_NOM";
            this.ANN_ENCAVEUR_NOM.ReadOnly = true;
            // 
            // ANN_ENCAVEUR_PRENOM
            // 
            this.ANN_ENCAVEUR_PRENOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ANN_ENCAVEUR_PRENOM.DataPropertyName = "ANN_ENCAVEUR_PRENOM";
            this.ANN_ENCAVEUR_PRENOM.HeaderText = "Prénom encaveur";
            this.ANN_ENCAVEUR_PRENOM.MinimumWidth = 120;
            this.ANN_ENCAVEUR_PRENOM.Name = "ANN_ENCAVEUR_PRENOM";
            this.ANN_ENCAVEUR_PRENOM.ReadOnly = true;
            // 
            // ADR_ID
            // 
            this.ADR_ID.DataPropertyName = "ADR_ID";
            this.ADR_ID.DataSource = this.adresseCompletBindingSource;
            this.ADR_ID.DisplayMember = "ADR_COMPLET";
            this.ADR_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ADR_ID.HeaderText = "Adresse";
            this.ADR_ID.Name = "ADR_ID";
            this.ADR_ID.ReadOnly = true;
            this.ADR_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ADR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ADR_ID.ValueMember = "ADR_ID";
            this.ADR_ID.Width = 150;
            // 
            // ANN_ENCAVEUR_SOUMIS_CONTROL
            // 
            this.ANN_ENCAVEUR_SOUMIS_CONTROL.DataPropertyName = "ANN_ENCAVEUR_SOUMIS_CONTROL";
            this.ANN_ENCAVEUR_SOUMIS_CONTROL.HeaderText = "Soumis au contrôle";
            this.ANN_ENCAVEUR_SOUMIS_CONTROL.Name = "ANN_ENCAVEUR_SOUMIS_CONTROL";
            this.ANN_ENCAVEUR_SOUMIS_CONTROL.ReadOnly = true;
            this.ANN_ENCAVEUR_SOUMIS_CONTROL.Width = 80;
            // 
            // ANN_RESPONSABLE_1
            // 
            this.ANN_RESPONSABLE_1.DataPropertyName = "ANN_RESPONSABLE_1";
            this.ANN_RESPONSABLE_1.HeaderText = "Responsable";
            this.ANN_RESPONSABLE_1.Name = "ANN_RESPONSABLE_1";
            this.ANN_RESPONSABLE_1.ReadOnly = true;
            this.ANN_RESPONSABLE_1.Visible = false;
            this.ANN_RESPONSABLE_1.Width = 120;
            // 
            // ANN_RESPONSABLE_2
            // 
            this.ANN_RESPONSABLE_2.DataPropertyName = "ANN_RESPONSABLE_2";
            this.ANN_RESPONSABLE_2.HeaderText = "ANN_RESPONSABLE_2";
            this.ANN_RESPONSABLE_2.Name = "ANN_RESPONSABLE_2";
            this.ANN_RESPONSABLE_2.ReadOnly = true;
            this.ANN_RESPONSABLE_2.Visible = false;
            // 
            // ANN_RESPONSABLE_3
            // 
            this.ANN_RESPONSABLE_3.DataPropertyName = "ANN_RESPONSABLE_3";
            this.ANN_RESPONSABLE_3.HeaderText = "ANN_RESPONSABLE_3";
            this.ANN_RESPONSABLE_3.Name = "ANN_RESPONSABLE_3";
            this.ANN_RESPONSABLE_3.ReadOnly = true;
            this.ANN_RESPONSABLE_3.Visible = false;
            // 
            // ANN_RESPONSABLE_4
            // 
            this.ANN_RESPONSABLE_4.DataPropertyName = "ANN_RESPONSABLE_4";
            this.ANN_RESPONSABLE_4.HeaderText = "ANN_RESPONSABLE_4";
            this.ANN_RESPONSABLE_4.Name = "ANN_RESPONSABLE_4";
            this.ANN_RESPONSABLE_4.ReadOnly = true;
            this.ANN_RESPONSABLE_4.Visible = false;
            // 
            // MOC_ID
            // 
            this.MOC_ID.DataPropertyName = "MOC_ID";
            this.MOC_ID.HeaderText = "MOC_ID";
            this.MOC_ID.Name = "MOC_ID";
            this.MOC_ID.ReadOnly = true;
            this.MOC_ID.Visible = false;
            // 
            // ANN_IS_PRIX_KG
            // 
            this.ANN_IS_PRIX_KG.DataPropertyName = "ANN_IS_PRIX_KG";
            this.ANN_IS_PRIX_KG.HeaderText = "ANN_IS_PRIX_KG";
            this.ANN_IS_PRIX_KG.Name = "ANN_IS_PRIX_KG";
            this.ANN_IS_PRIX_KG.ReadOnly = true;
            this.ANN_IS_PRIX_KG.Visible = false;
            // 
            // WfrmGestionDesAnnees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(777, 562);
            this.Controls.Add(this.aNN_ANNEEDataGridView);
            this.Controls.Add(this.groupBox1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(793, 600);
            this.Name = "WfrmGestionDesAnnees";
            this.Text = "Gestion des années";
            this.Load += new System.EventHandler(this.WfrmGestionDesAnnees_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            this.AdressesContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aNN_ANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox aNN_ANTextBox;
        private System.Windows.Forms.Button btnAddYear;
        private System.Windows.Forms.Button btnSaveYear;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox aNN_IDTextBox;
    //    private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.ComboBox aDR_IDComboBox;
        private System.Windows.Forms.TextBox aNN_ENCAVEUR_PRENOMTextBox;
        private System.Windows.Forms.TextBox aNN_ENCAVEUR_NOMTextBox;
        private System.Windows.Forms.ContextMenuStrip AdressesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem AdressesToolStripMenuItem;
        private System.Windows.Forms.DataGridView aNN_ANNEEDataGridView;
        private System.Windows.Forms.BindingSource adresseCompletBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter adresseCompletTableAdapter;
     //   private System.Windows.Forms.DataGridViewTextBoxColumn ANN_DEGRE;
        private System.Windows.Forms.CheckBox aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox;
        private System.Windows.Forms.TextBox aNN_RESPONSABLE_4TextBox;
        private System.Windows.Forms.TextBox aNN_RESPONSABLE_3TextBox;
        private System.Windows.Forms.TextBox aNN_RESPONSABLE_2TextBox;
        private System.Windows.Forms.TextBox aNN_RESPONSABLE_1TextBox;
        private System.Windows.Forms.TextBox aNN_ENCAVEUR_NUMEROTextBox;
        private System.Windows.Forms.TextBox aNN_ENCAVEUR_SOCIETETextBox;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
        private System.Windows.Forms.Label mOC_IDLabel;
        private System.Windows.Forms.Label lPrix;
        private System.Windows.Forms.RadioButton optLitres;
        private System.Windows.Forms.RadioButton aNN_IS_PRIX_KGRadioButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_TAUX_TVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_AN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ENCAVEUR_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ENCAVEUR_SOCIETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ENCAVEUR_NOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ENCAVEUR_PRENOM;
        private System.Windows.Forms.DataGridViewComboBoxColumn ADR_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ANN_ENCAVEUR_SOUMIS_CONTROL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_RESPONSABLE_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_RESPONSABLE_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_RESPONSABLE_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_RESPONSABLE_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOC_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ANN_IS_PRIX_KG;


    }
}