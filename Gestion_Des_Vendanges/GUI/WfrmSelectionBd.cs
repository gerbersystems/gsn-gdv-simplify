﻿using System.Collections.Generic;

namespace Gestion_Des_Vendanges.GUI
{
    internal class WfrmSelectionBd : WfrmSelection
    {
        private class Bd
        {
            private string _nom;
            private string _description;

            public string Nom
            {
                get { return _nom; }
            }

            public string Description
            {
                get { return _description; }
            }

            public Bd(string nom, string description)
            {
                _nom = nom;
                _description = description;
            }

            public override string ToString()
            {
                return _description;
            }
        }

        public string NomSelection
        {
            get
            {
                if (Selection != null)
                {
                    return ((Bd)Selection).Nom;
                }
                else
                {
                    return null;
                }
            }
        }

        public WfrmSelectionBd()
            : base("Gestion des vendanges - Sélection du dossier", "Dossier")
        {
            DAO.DSMaster dsMaster = new DAO.DSMaster();
            DAO.DSMasterTableAdapters.DAT_DATABASETableAdapter datTable = new DAO.DSMasterTableAdapters.DAT_DATABASETableAdapter();
            datTable.Connection.ConnectionString = DAO.ConnectionManager.Instance.MasterConnectionString;
            datTable.Fill(dsMaster.DAT_DATABASE);
            List<Bd> dataBases = new List<Bd>();
            foreach (DAO.DSMaster.DAT_DATABASERow row in datTable.GetData().Rows)
            {
                dataBases.Add(new Bd(row.DAT_NOM, row.DAT_DESCRIPTION));
            }
            Choix = dataBases.ToArray();
        }
    }
}