﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesLieuxDeProductions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cOM_IDLabel;
            System.Windows.Forms.Label cOM_NOMLabel;
            System.Windows.Forms.Label cOM_NPALabel;
            System.Windows.Forms.Label lIE_NUMEROLabel;
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.lIE_LIEU_DE_PRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.lIC_LIEU_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIC_LIEU_COMMUNETableAdapter();
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONDataGridView = new System.Windows.Forms.DataGridView();
            this.LIE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REG_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.rEGREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIE_IDTextBox = new System.Windows.Forms.TextBox();
            this.lIE_NOMTextBox = new System.Windows.Forms.TextBox();
            this.rEG_IDComboBox = new System.Windows.Forms.ComboBox();
            this.regionMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.regionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lIC_LIEU_COMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_IDComboBox = new System.Windows.Forms.ComboBox();
            this.communeMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.communeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lIE_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listLic = new System.Windows.Forms.ListBox();
            this.cmdDeleteLIC = new System.Windows.Forms.Button();
            this.cmdAddLIC = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnSaveLie = new System.Windows.Forms.Button();
            this.btnAddLie = new System.Windows.Forms.Button();
            cOM_IDLabel = new System.Windows.Forms.Label();
            cOM_NOMLabel = new System.Windows.Forms.Label();
            cOM_NPALabel = new System.Windows.Forms.Label();
            lIE_NUMEROLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIE_LIEU_DE_PRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIE_LIEU_DE_PRODUCTIONDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).BeginInit();
            this.regionMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIC_LIEU_COMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            this.communeMenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cOM_IDLabel
            // 
            cOM_IDLabel.AutoSize = true;
            cOM_IDLabel.Location = new System.Drawing.Point(6, 33);
            cOM_IDLabel.Name = "cOM_IDLabel";
            cOM_IDLabel.Size = new System.Drawing.Size(60, 13);
            cOM_IDLabel.TabIndex = 9;
            cOM_IDLabel.Text = "Commune :";
            // 
            // cOM_NOMLabel
            // 
            cOM_NOMLabel.AutoSize = true;
            cOM_NOMLabel.Location = new System.Drawing.Point(18, 30);
            cOM_NOMLabel.Name = "cOM_NOMLabel";
            cOM_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cOM_NOMLabel.TabIndex = 17;
            cOM_NOMLabel.Text = "Nom : ";
            // 
            // cOM_NPALabel
            // 
            cOM_NPALabel.AutoSize = true;
            cOM_NPALabel.Location = new System.Drawing.Point(18, 59);
            cOM_NPALabel.Name = "cOM_NPALabel";
            cOM_NPALabel.Size = new System.Drawing.Size(50, 13);
            cOM_NPALabel.TabIndex = 19;
            cOM_NPALabel.Text = "Région : ";
            // 
            // lIE_NUMEROLabel
            // 
            lIE_NUMEROLabel.AutoSize = true;
            lIE_NUMEROLabel.Location = new System.Drawing.Point(18, 88);
            lIE_NUMEROLabel.Name = "lIE_NUMEROLabel";
            lIE_NUMEROLabel.Size = new System.Drawing.Size(50, 13);
            lIE_NUMEROLabel.TabIndex = 19;
            lIE_NUMEROLabel.Text = "Numéro :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lIE_LIEU_DE_PRODUCTIONBindingSource
            // 
            this.lIE_LIEU_DE_PRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIE_LIEU_DE_PRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = this.cOM_COMMUNETableAdapter;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = this.lIC_LIEU_COMMUNETableAdapter;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = this.lIE_LIEU_DE_PRODUCTIONTableAdapter;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = this.rEG_REGIONTableAdapter;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // lIC_LIEU_COMMUNETableAdapter
            // 
            this.lIC_LIEU_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONDataGridView
            // 
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.AllowUserToAddRows = false;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.AllowUserToDeleteRows = false;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.AutoGenerateColumns = false;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LIE_ID,
            this.dataGridViewTextBoxColumn2,
            this.REG_ID});
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.DataSource = this.lIE_LIEU_DE_PRODUCTIONBindingSource;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.Location = new System.Drawing.Point(406, 12);
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.Name = "lIE_LIEU_DE_PRODUCTIONDataGridView";
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.ReadOnly = true;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.Size = new System.Drawing.Size(324, 502);
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.TabIndex = 2;
            this.lIE_LIEU_DE_PRODUCTIONDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.lIE_LIEU_DE_PRODUCTIONDataGridView_ColumnHeaderMouseClick);
            // 
            // LIE_ID
            // 
            this.LIE_ID.DataPropertyName = "LIE_ID";
            this.LIE_ID.HeaderText = "LIE_ID";
            this.LIE_ID.Name = "LIE_ID";
            this.LIE_ID.ReadOnly = true;
            this.LIE_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "LIE_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // REG_ID
            // 
            this.REG_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.REG_ID.DataPropertyName = "REG_ID";
            this.REG_ID.DataSource = this.rEGREGIONBindingSource;
            this.REG_ID.DisplayMember = "REG_NAME";
            this.REG_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.REG_ID.HeaderText = "Région";
            this.REG_ID.MinimumWidth = 100;
            this.REG_ID.Name = "REG_ID";
            this.REG_ID.ReadOnly = true;
            this.REG_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REG_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.REG_ID.ValueMember = "REG_ID";
            // 
            // rEGREGIONBindingSource
            // 
            this.rEGREGIONBindingSource.DataMember = "REG_REGION";
            this.rEGREGIONBindingSource.DataSource = this.dSPesees;
            // 
            // lIE_IDTextBox
            // 
            this.lIE_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIE_LIEU_DE_PRODUCTIONBindingSource, "LIE_ID", true));
            this.lIE_IDTextBox.Location = new System.Drawing.Point(105, 53);
            this.lIE_IDTextBox.Name = "lIE_IDTextBox";
            this.lIE_IDTextBox.Size = new System.Drawing.Size(121, 20);
            this.lIE_IDTextBox.TabIndex = 3;
            this.lIE_IDTextBox.Text = "LIE_ID";
            // 
            // lIE_NOMTextBox
            // 
            this.lIE_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIE_LIEU_DE_PRODUCTIONBindingSource, "LIE_NOM", true));
            this.lIE_NOMTextBox.Location = new System.Drawing.Point(131, 27);
            this.lIE_NOMTextBox.Name = "lIE_NOMTextBox";
            this.lIE_NOMTextBox.Size = new System.Drawing.Size(250, 20);
            this.lIE_NOMTextBox.TabIndex = 0;
            // 
            // rEG_IDComboBox
            // 
            this.rEG_IDComboBox.ContextMenuStrip = this.regionMenuStrip;
            this.rEG_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lIE_LIEU_DE_PRODUCTIONBindingSource, "REG_ID", true));
            this.rEG_IDComboBox.DataSource = this.rEGREGIONBindingSource;
            this.rEG_IDComboBox.DisplayMember = "REG_NAME";
            this.rEG_IDComboBox.FormattingEnabled = true;
            this.rEG_IDComboBox.Location = new System.Drawing.Point(131, 56);
            this.rEG_IDComboBox.Name = "rEG_IDComboBox";
            this.rEG_IDComboBox.Size = new System.Drawing.Size(250, 21);
            this.rEG_IDComboBox.TabIndex = 1;
            this.rEG_IDComboBox.ValueMember = "REG_ID";
            // 
            // regionMenuStrip
            // 
            this.regionMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regionMenuItem});
            this.regionMenuStrip.Name = "districtMenuStrip";
            this.regionMenuStrip.Size = new System.Drawing.Size(190, 26);
            // 
            // regionMenuItem
            // 
            this.regionMenuItem.Name = "regionMenuItem";
            this.regionMenuItem.Size = new System.Drawing.Size(189, 22);
            this.regionMenuItem.Text = "Accéder aux régions...";
            this.regionMenuItem.Click += new System.EventHandler(this.regionMenuItem_Click);
            // 
            // lIC_LIEU_COMMUNEBindingSource
            // 
            this.lIC_LIEU_COMMUNEBindingSource.DataMember = "FK_LIC_LIEU_COMMUNE_LIE_LIEU_DE_PRODUCTION";
            this.lIC_LIEU_COMMUNEBindingSource.DataSource = this.lIE_LIEU_DE_PRODUCTIONBindingSource;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_IDComboBox
            // 
            this.cOM_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cOM_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cOM_IDComboBox.ContextMenuStrip = this.communeMenuStrip;
            this.cOM_IDComboBox.DataSource = this.cOMCOMMUNEBindingSource;
            this.cOM_IDComboBox.DisplayMember = "COM_NOM";
            this.cOM_IDComboBox.FormattingEnabled = true;
            this.cOM_IDComboBox.Location = new System.Drawing.Point(103, 30);
            this.cOM_IDComboBox.Name = "cOM_IDComboBox";
            this.cOM_IDComboBox.Size = new System.Drawing.Size(158, 21);
            this.cOM_IDComboBox.TabIndex = 0;
            this.cOM_IDComboBox.ValueMember = "COM_ID";
            // 
            // communeMenuStrip
            // 
            this.communeMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.communeMenuItem});
            this.communeMenuStrip.Name = "districtMenuStrip";
            this.communeMenuStrip.Size = new System.Drawing.Size(211, 26);
            // 
            // communeMenuItem
            // 
            this.communeMenuItem.Name = "communeMenuItem";
            this.communeMenuItem.Size = new System.Drawing.Size(210, 22);
            this.communeMenuItem.Text = "Accéder aux communes...";
            this.communeMenuItem.Click += new System.EventHandler(this.communeMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(lIE_NUMEROLabel);
            this.groupBox1.Controls.Add(this.lIE_NUMEROTextBox);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnSaveLie);
            this.groupBox1.Controls.Add(this.btnAddLie);
            this.groupBox1.Controls.Add(cOM_NOMLabel);
            this.groupBox1.Controls.Add(this.lIE_NOMTextBox);
            this.groupBox1.Controls.Add(cOM_NPALabel);
            this.groupBox1.Controls.Add(this.rEG_IDComboBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(388, 502);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des lieux de production";
            // 
            // lIE_NUMEROTextBox
            // 
            this.lIE_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIE_LIEU_DE_PRODUCTIONBindingSource, "LIE_NUMERO", true));
            this.lIE_NUMEROTextBox.Location = new System.Drawing.Point(131, 85);
            this.lIE_NUMEROTextBox.Name = "lIE_NUMEROTextBox";
            this.lIE_NUMEROTextBox.Size = new System.Drawing.Size(250, 20);
            this.lIE_NUMEROTextBox.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listLic);
            this.groupBox2.Controls.Add(this.cmdDeleteLIC);
            this.groupBox2.Controls.Add(this.cmdAddLIC);
            this.groupBox2.Controls.Add(cOM_IDLabel);
            this.groupBox2.Controls.Add(this.cOM_IDComboBox);
            this.groupBox2.Location = new System.Drawing.Point(21, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(360, 343);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Communes du lieu de production";
            // 
            // listLic
            // 
            this.listLic.FormattingEnabled = true;
            this.listLic.Location = new System.Drawing.Point(103, 57);
            this.listLic.Name = "listLic";
            this.listLic.Size = new System.Drawing.Size(251, 277);
            this.listLic.Sorted = true;
            this.listLic.TabIndex = 3;
            // 
            // cmdDeleteLIC
            // 
            this.cmdDeleteLIC.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdDeleteLIC.Location = new System.Drawing.Point(9, 57);
            this.cmdDeleteLIC.Name = "cmdDeleteLIC";
            this.cmdDeleteLIC.Size = new System.Drawing.Size(75, 30);
            this.cmdDeleteLIC.TabIndex = 2;
            this.cmdDeleteLIC.Text = "Retirer";
            this.cmdDeleteLIC.UseVisualStyleBackColor = false;
            // 
            // cmdAddLIC
            // 
            this.cmdAddLIC.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdAddLIC.Location = new System.Drawing.Point(279, 21);
            this.cmdAddLIC.Name = "cmdAddLIC";
            this.cmdAddLIC.Size = new System.Drawing.Size(75, 30);
            this.cmdAddLIC.TabIndex = 1;
            this.cmdAddLIC.Text = "Ajouter";
            this.cmdAddLIC.UseVisualStyleBackColor = false;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(307, 466);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 6;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnSaveLie
            // 
            this.btnSaveLie.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveLie.Location = new System.Drawing.Point(226, 466);
            this.btnSaveLie.Name = "btnSaveLie";
            this.btnSaveLie.Size = new System.Drawing.Size(75, 30);
            this.btnSaveLie.TabIndex = 5;
            this.btnSaveLie.Text = "Modifier";
            this.btnSaveLie.UseVisualStyleBackColor = false;
            // 
            // btnAddLie
            // 
            this.btnAddLie.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddLie.Location = new System.Drawing.Point(145, 466);
            this.btnAddLie.Name = "btnAddLie";
            this.btnAddLie.Size = new System.Drawing.Size(75, 30);
            this.btnAddLie.TabIndex = 4;
            this.btnAddLie.Text = "Nouveau";
            this.btnAddLie.UseVisualStyleBackColor = false;
            // 
            // WfrmGestionDesLieuxDeProductions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 526);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lIE_IDTextBox);
            this.Controls.Add(this.lIE_LIEU_DE_PRODUCTIONDataGridView);
            this.MinimumSize = new System.Drawing.Size(750, 560);
            this.Name = "WfrmGestionDesLieuxDeProductions";
            this.Text = "Gestion des lieux de production";
            this.Load += new System.EventHandler(this.WfrmGestionDesLieuxDeProductions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIE_LIEU_DE_PRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIE_LIEU_DE_PRODUCTIONDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).EndInit();
            this.regionMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIC_LIEU_COMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            this.communeMenuStrip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource lIE_LIEU_DE_PRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView lIE_LIEU_DE_PRODUCTIONDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIC_LIEU_COMMUNETableAdapter lIC_LIEU_COMMUNETableAdapter;
        private System.Windows.Forms.TextBox lIE_IDTextBox;
        private System.Windows.Forms.TextBox lIE_NOMTextBox;
        private System.Windows.Forms.ComboBox rEG_IDComboBox;
        private System.Windows.Forms.BindingSource lIC_LIEU_COMMUNEBindingSource;
        private System.Windows.Forms.ComboBox cOM_IDComboBox;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
        private System.Windows.Forms.BindingSource rEGREGIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnSaveLie;
        private System.Windows.Forms.Button btnAddLie;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmdDeleteLIC;
        private System.Windows.Forms.Button cmdAddLIC;
        private System.Windows.Forms.TextBox lIE_NUMEROTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn REG_ID;
        private System.Windows.Forms.ContextMenuStrip regionMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem regionMenuItem;
        private System.Windows.Forms.ListBox listLic;
        private System.Windows.Forms.ContextMenuStrip communeMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem communeMenuItem;
    }
}