﻿namespace Gestion_Des_Vendanges.GUI
{
	partial class WfrmGestionDesAcquits
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddAcquit = new System.Windows.Forms.Button();
            this.btnDeleteAcquit = new System.Windows.Forms.Button();
            this.aCQ_ACQUITDataGridView = new System.Windows.Forms.DataGridView();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.cLACLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rEGREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCQ_ACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_PARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btn_ModifyAcquit = new System.Windows.Forms.Button();
            this.cmdReportAcquits = new System.Windows.Forms.Button();
            this.btnDroitsProduction = new System.Windows.Forms.Button();
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.ACQ_NUMERO_COMPL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CLA_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.REG_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.COM_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.COU_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ACQ_SURFACE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_LITRES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_ID_VENDANGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(12, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Gestion des acquits";
            // 
            // btnAddAcquit
            // 
            this.btnAddAcquit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAcquit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddAcquit.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnAddAcquit.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnAddAcquit.Location = new System.Drawing.Point(859, 99);
            this.btnAddAcquit.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddAcquit.Name = "btnAddAcquit";
            this.btnAddAcquit.Size = new System.Drawing.Size(104, 53);
            this.btnAddAcquit.TabIndex = 0;
            this.btnAddAcquit.Text = "Ajouter un acquit";
            this.btnAddAcquit.UseVisualStyleBackColor = false;
            this.btnAddAcquit.Click += new System.EventHandler(this.btnAddAcquit_Click);
            // 
            // btnDeleteAcquit
            // 
            this.btnDeleteAcquit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAcquit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDeleteAcquit.Location = new System.Drawing.Point(858, 216);
            this.btnDeleteAcquit.Name = "btnDeleteAcquit";
            this.btnDeleteAcquit.Size = new System.Drawing.Size(104, 53);
            this.btnDeleteAcquit.TabIndex = 2;
            this.btnDeleteAcquit.Text = "Supprimer l\'acquit";
            this.btnDeleteAcquit.UseVisualStyleBackColor = false;
            this.btnDeleteAcquit.Click += new System.EventHandler(this.btnDeleteAcquit_Click);
            // 
            // aCQ_ACQUITDataGridView
            // 
            this.aCQ_ACQUITDataGridView.AllowUserToAddRows = false;
            this.aCQ_ACQUITDataGridView.AllowUserToDeleteRows = false;
            this.aCQ_ACQUITDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aCQ_ACQUITDataGridView.AutoGenerateColumns = false;
            this.aCQ_ACQUITDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor2;
            this.aCQ_ACQUITDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aCQ_ACQUITDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACQ_NUMERO_COMPL,
            this.ACQ_ID,
            this.ANN_ID,
            this.ACQ_NUMERO,
            this.ACQ_DATE,
            this.PRO_ID,
            this.CLA_ID,
            this.REG_ID,
            this.COM_ID,
            this.COU_ID,
            this.ACQ_SURFACE,
            this.ACQ_LITRES,
            this.PRO_ID_VENDANGE});
            this.aCQ_ACQUITDataGridView.DataSource = this.aCQ_ACQUITBindingSource;
            this.aCQ_ACQUITDataGridView.Location = new System.Drawing.Point(12, 89);
            this.aCQ_ACQUITDataGridView.MultiSelect = false;
            this.aCQ_ACQUITDataGridView.Name = "aCQ_ACQUITDataGridView";
            this.aCQ_ACQUITDataGridView.ReadOnly = true;
            this.aCQ_ACQUITDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.aCQ_ACQUITDataGridView.Size = new System.Drawing.Size(842, 414);
            this.aCQ_ACQUITDataGridView.TabIndex = 5;
            this.aCQ_ACQUITDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.aCQ_ACQUITDataGridView_ColumnHeaderMouseClick);
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cLACLASSEBindingSource
            // 
            this.cLACLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLACLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // rEGREGIONBindingSource
            // 
            this.rEGREGIONBindingSource.DataMember = "REG_REGION";
            this.rEGREGIONBindingSource.DataSource = this.dSPesees;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // aCQ_ACQUITBindingSource
            // 
            this.aCQ_ACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQ_ACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_PARCELLEBindingSource
            // 
            this.pAR_PARCELLEBindingSource.DataMember = "FK_PAR_PARC_CONTIENT_ACQ_ACQU";
            this.pAR_PARCELLEBindingSource.DataSource = this.aCQ_ACQUITBindingSource;
            // 
            // btn_ModifyAcquit
            // 
            this.btn_ModifyAcquit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_ModifyAcquit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btn_ModifyAcquit.Location = new System.Drawing.Point(859, 157);
            this.btn_ModifyAcquit.Name = "btn_ModifyAcquit";
            this.btn_ModifyAcquit.Size = new System.Drawing.Size(104, 53);
            this.btn_ModifyAcquit.TabIndex = 1;
            this.btn_ModifyAcquit.Text = "Modifier l\'acquit";
            this.btn_ModifyAcquit.UseVisualStyleBackColor = false;
            this.btn_ModifyAcquit.Click += new System.EventHandler(this.btnModifyAcquit_Click);
            // 
            // cmdReportAcquits
            // 
            this.cmdReportAcquits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdReportAcquits.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdReportAcquits.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cmdReportAcquits.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cmdReportAcquits.Location = new System.Drawing.Point(859, 452);
            this.cmdReportAcquits.Margin = new System.Windows.Forms.Padding(2);
            this.cmdReportAcquits.Name = "cmdReportAcquits";
            this.cmdReportAcquits.Size = new System.Drawing.Size(104, 53);
            this.cmdReportAcquits.TabIndex = 4;
            this.cmdReportAcquits.Text = "Liste des acquits";
            this.cmdReportAcquits.UseVisualStyleBackColor = false;
            this.cmdReportAcquits.Click += new System.EventHandler(this.cmdReportAcquits_Click);
            // 
            // btnDroitsProduction
            // 
            this.btnDroitsProduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDroitsProduction.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDroitsProduction.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnDroitsProduction.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnDroitsProduction.Location = new System.Drawing.Point(859, 395);
            this.btnDroitsProduction.Margin = new System.Windows.Forms.Padding(2);
            this.btnDroitsProduction.Name = "btnDroitsProduction";
            this.btnDroitsProduction.Size = new System.Drawing.Size(104, 53);
            this.btnDroitsProduction.TabIndex = 3;
            this.btnDroitsProduction.Text = "Liste des droits de production";
            this.btnDroitsProduction.UseVisualStyleBackColor = false;
            this.btnDroitsProduction.Click += new System.EventHandler(this.btnDroitsProduction_Click);
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.Articles_exportTableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.CON_CONVERSIONTableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = this.pAR_PARCELLETableAdapter;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.Taux_KLTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // ACQ_NUMERO_COMPL
            // 
            this.ACQ_NUMERO_COMPL.DataPropertyName = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.HeaderText = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.Name = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.ReadOnly = true;
            this.ACQ_NUMERO_COMPL.Visible = false;
            // 
            // ACQ_ID
            // 
            this.ACQ_ID.DataPropertyName = "ACQ_ID";
            this.ACQ_ID.FillWeight = 40F;
            this.ACQ_ID.HeaderText = "ID";
            this.ACQ_ID.Name = "ACQ_ID";
            this.ACQ_ID.ReadOnly = true;
            this.ACQ_ID.Visible = false;
            this.ACQ_ID.Width = 40;
            // 
            // ANN_ID
            // 
            this.ANN_ID.DataPropertyName = "ANN_ID";
            this.ANN_ID.HeaderText = "Année";
            this.ANN_ID.Name = "ANN_ID";
            this.ANN_ID.ReadOnly = true;
            this.ANN_ID.Visible = false;
            // 
            // ACQ_NUMERO
            // 
            this.ACQ_NUMERO.DataPropertyName = "ACQ_NUMERO";
            this.ACQ_NUMERO.FillWeight = 60F;
            this.ACQ_NUMERO.HeaderText = "Numéro";
            this.ACQ_NUMERO.Name = "ACQ_NUMERO";
            this.ACQ_NUMERO.ReadOnly = true;
            this.ACQ_NUMERO.Width = 50;
            // 
            // ACQ_DATE
            // 
            this.ACQ_DATE.DataPropertyName = "ACQ_DATE";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.ACQ_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.ACQ_DATE.FillWeight = 80F;
            this.ACQ_DATE.HeaderText = "Date";
            this.ACQ_DATE.Name = "ACQ_DATE";
            this.ACQ_DATE.ReadOnly = true;
            this.ACQ_DATE.Width = 80;
            // 
            // PRO_ID
            // 
            this.PRO_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_ID.DataPropertyName = "PRO_ID";
            this.PRO_ID.DataSource = this.pRONOMCOMPLETBindingSource;
            this.PRO_ID.DisplayMember = "NOMCOMPLET";
            this.PRO_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PRO_ID.HeaderText = "Propriétaire";
            this.PRO_ID.MinimumWidth = 100;
            this.PRO_ID.Name = "PRO_ID";
            this.PRO_ID.ReadOnly = true;
            this.PRO_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PRO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PRO_ID.ValueMember = "PRO_ID";
            // 
            // CLA_ID
            // 
            this.CLA_ID.DataPropertyName = "CLA_ID";
            this.CLA_ID.DataSource = this.cLACLASSEBindingSource;
            this.CLA_ID.DisplayMember = "CLA_NOM";
            this.CLA_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CLA_ID.HeaderText = "Classe";
            this.CLA_ID.MinimumWidth = 150;
            this.CLA_ID.Name = "CLA_ID";
            this.CLA_ID.ReadOnly = true;
            this.CLA_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CLA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CLA_ID.ValueMember = "CLA_ID";
            this.CLA_ID.Width = 150;
            // 
            // REG_ID
            // 
            this.REG_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.REG_ID.DataPropertyName = "REG_ID";
            this.REG_ID.DataSource = this.rEGREGIONBindingSource;
            this.REG_ID.DisplayMember = "REG_NAME";
            this.REG_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.REG_ID.HeaderText = "Région";
            this.REG_ID.MinimumWidth = 90;
            this.REG_ID.Name = "REG_ID";
            this.REG_ID.ReadOnly = true;
            this.REG_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REG_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.REG_ID.ValueMember = "REG_ID";
            // 
            // COM_ID
            // 
            this.COM_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.COM_ID.DataPropertyName = "COM_ID";
            this.COM_ID.DataSource = this.cOMCOMMUNEBindingSource;
            this.COM_ID.DisplayMember = "COM_NOM";
            this.COM_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.COM_ID.HeaderText = "Commune";
            this.COM_ID.MinimumWidth = 100;
            this.COM_ID.Name = "COM_ID";
            this.COM_ID.ReadOnly = true;
            this.COM_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.COM_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.COM_ID.ValueMember = "COM_ID";
            // 
            // COU_ID
            // 
            this.COU_ID.DataPropertyName = "COU_ID";
            this.COU_ID.DataSource = this.cOUCOULEURBindingSource;
            this.COU_ID.DisplayMember = "COU_NOM";
            this.COU_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.COU_ID.HeaderText = "Couleur";
            this.COU_ID.MinimumWidth = 65;
            this.COU_ID.Name = "COU_ID";
            this.COU_ID.ReadOnly = true;
            this.COU_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.COU_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.COU_ID.ValueMember = "COU_ID";
            this.COU_ID.Width = 65;
            // 
            // ACQ_SURFACE
            // 
            this.ACQ_SURFACE.DataPropertyName = "ACQ_SURFACE";
            this.ACQ_SURFACE.HeaderText = "Surface";
            this.ACQ_SURFACE.MinimumWidth = 70;
            this.ACQ_SURFACE.Name = "ACQ_SURFACE";
            this.ACQ_SURFACE.ReadOnly = true;
            this.ACQ_SURFACE.Width = 70;
            // 
            // ACQ_LITRES
            // 
            this.ACQ_LITRES.DataPropertyName = "ACQ_LITRES";
            this.ACQ_LITRES.HeaderText = "Litres";
            this.ACQ_LITRES.MinimumWidth = 60;
            this.ACQ_LITRES.Name = "ACQ_LITRES";
            this.ACQ_LITRES.ReadOnly = true;
            this.ACQ_LITRES.Width = 60;
            // 
            // PRO_ID_VENDANGE
            // 
            this.PRO_ID_VENDANGE.DataPropertyName = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.HeaderText = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.Name = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.ReadOnly = true;
            this.PRO_ID_VENDANGE.Visible = false;
            // 
            // WfrmGestionDesAcquits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor2;
            this.ClientSize = new System.Drawing.Size(968, 520);
            this.Controls.Add(this.btnDroitsProduction);
            this.Controls.Add(this.cmdReportAcquits);
            this.Controls.Add(this.btn_ModifyAcquit);
            this.Controls.Add(this.aCQ_ACQUITDataGridView);
            this.Controls.Add(this.btnDeleteAcquit);
            this.Controls.Add(this.btnAddAcquit);
            this.Controls.Add(this.label1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(471, 431);
            this.Name = "WfrmGestionDesAcquits";
            this.ShowInTaskbar = false;
            this.Text = "Gestion des acquits";
            this.Load += new System.EventHandler(this.WfrmGestionDesAcquits_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WfrmGestionDesAcquits_FormClosed);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.btnAddAcquit, 0);
            this.Controls.SetChildIndex(this.btnDeleteAcquit, 0);
            this.Controls.SetChildIndex(this.aCQ_ACQUITDataGridView, 0);
            this.Controls.SetChildIndex(this.btn_ModifyAcquit, 0);
            this.Controls.SetChildIndex(this.cmdReportAcquits, 0);
            this.Controls.SetChildIndex(this.btnDroitsProduction, 0);
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddAcquit;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
      //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.Button btnDeleteAcquit;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
       // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
      //  private System.Windows.Forms.BindingSource litres_Cat1_by_AcquitBindingSource2;
       // private System.Windows.Forms.BindingSource litres_Cat2_by_AcquitBindingSource2;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aCQ_ACQUITBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView aCQ_ACQUITDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
        private System.Windows.Forms.BindingSource pAR_PARCELLEBindingSource;
        private System.Windows.Forms.Button btn_ModifyAcquit;
     //   private System.Windows.Forms.BindingSource maxAnneeBindingSource;
        //    private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MaxAnneeTableAdapter maxAnneeTableAdapter;
        private System.Windows.Forms.BindingSource cLACLASSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
        private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
       // private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
     //   private System.Windows.Forms.BindingSource aPPAPPELLATIONBindingSource;
     //   private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
        private System.Windows.Forms.Button cmdReportAcquits;
        private System.Windows.Forms.Button btnDroitsProduction;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.BindingSource rEGREGIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_NUMERO_COMPL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_DATE;
        private System.Windows.Forms.DataGridViewComboBoxColumn PRO_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn CLA_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn REG_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn COM_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn COU_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_SURFACE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_LITRES;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_ID_VENDANGE;


    }
}