﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des responsables
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesResponsables : WfrmSetting
    {
        private FormSettingsManagerRefAdresse _manager;
        private static WfrmGestionDesResponsables _wfrm;

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int resId = (int)rES_RESPONSABLEDataGridView.CurrentRow.Cells["RES_ID"].Value;
                    resultat = (rES_RESPONSABLETableAdapter.NbRef(resId) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        public static WfrmGestionDesResponsables Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesResponsables();
                }
                return _wfrm;
            }
        }

        private WfrmGestionDesResponsables()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesResponsables_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(adresseCompletTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aDR_ADRESSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rES_RESPONSABLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            this.adresseCompletTableAdapter.Fill(this.dSPesees.AdresseComplet);
            this.aDR_ADRESSETableAdapter.Fill(this.dSPesees.ADR_ADRESSE);
            this.rES_RESPONSABLETableAdapter.Fill(this.dSPesees.RES_RESPONSABLE);
            _manager = new FormSettingsManagerRefAdresse(this,
                rES_IDTextBox,
                rES_NOMTextBox,
                btnNew,
                btnUpdate,
                btnSupprimer,
                rES_RESPONSABLEDataGridView,
                rES_RESPONSABLEBindingSource,
                tableAdapterManager,
                dSPesees,
                adresseCompletTableAdapter
            );
            _manager.AddControl(rES_NOMTextBox);
            _manager.AddControl(rES_PRENOMTextBox);
            _manager.AddControl(aDR_IDComboBox);
            _manager.AddControl(rES_DEFAULTCheckBox);
        }

        private void AdressesontextMenuStrip_Click(object sender, EventArgs e)
        {
            object item = aDR_IDComboBox.SelectedValue;
            WfrmGestionDesAdresses.Wfrm.ShowDialog();
            this.adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
            this.aDR_ADRESSETableAdapter.Fill(dSPesees.ADR_ADRESSE);
            try
            {
                aDR_IDComboBox.SelectedValue = item;
            }
            catch { }
        }

        private void rES_RESPONSABLEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                rES_RESPONSABLEBindingSource.RemoveSort();
                rES_RESPONSABLETableAdapter.FillOrderAdr(dSPesees.RES_RESPONSABLE);
                foreach (DataGridViewColumn column in rES_RESPONSABLEDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                rES_RESPONSABLEDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }
    }
}