﻿using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmHostCtrWfrm : WfrmMenu
    {
        private CtrlWfrm _control;

        private WfrmHostCtrWfrm(CtrlWfrm control)
        {
            InitializeComponent();
            _control = control;
            setControl();
        }

        private void setControl()
        {
            Text = _control.Text;
            _control.Dock = DockStyle.Fill;
            Controls.Add(_control.ToolStrip);
            Controls.Add(_control);
            _control.OptionPanel.Hide();
        }

        public static DialogResult ShowDialog(CtrlWfrm control, bool showMenu = true, FormWindowState windowState = FormWindowState.Normal)
        {
            WfrmHostCtrWfrm wfrm = new WfrmHostCtrWfrm(control);
            wfrm.menuStrip.Visible = showMenu;
            wfrm.WindowState = windowState;
            return wfrm.ShowDialog();
        }

        public static void Show(CtrlWfrm control, bool showMenu = true, FormWindowState windowState = FormWindowState.Normal)
        {
            WfrmHostCtrWfrm wfrm = new WfrmHostCtrWfrm(control);
            wfrm.menuStrip.Visible = showMenu;
            wfrm.WindowState = windowState;
            wfrm.Show();
        }
    }
}