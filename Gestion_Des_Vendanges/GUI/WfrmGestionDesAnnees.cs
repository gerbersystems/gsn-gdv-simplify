﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using System.IO;
using System.Windows.Forms;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des années
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmGestionDesAnnees : WfrmSetting
    {
        private FormSettingsManagerRefAdresse _manager;
        private static WfrmGestionDesAnnees _wfrm;

        public static WfrmGestionDesAnnees Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed) _wfrm = new WfrmGestionDesAnnees();
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                if (aNN_ANNEEDataGridView.CurrentRow.Cells["ANN_ID"].Value == null)
                    return true;

                if (aNN_ANNEETableAdapter.Count() <= 1)
                    return false;

                return aNN_ANNEETableAdapter.NbRef((int)aNN_ANNEEDataGridView.CurrentRow.Cells["ANN_ID"].Value) == 0;
            }
        }

        private WfrmGestionDesAnnees()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesAnnees_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(adresseCompletTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            mOC_MODE_COMPTABILISATIONTableAdapter.Fill(dSPesees.MOC_MODE_COMPTABILISATION);
            adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);

            _manager = new FormSettingsManagerAnnee(form: this, idTextBox: aNN_IDTextBox
                                                              , focusControl: aNN_ANTextBox
                                                              , btnNew: btnAddYear
                                                              , btnUpdate: btnSaveYear
                                                              , btnDelete: btnSupprimer
                                                              , dataGridView: aNN_ANNEEDataGridView
                                                              , bindingSource: aNN_ANNEEBindingSource
                                                              , tableAdapterManager: tableAdapterManager
                                                              , dsPesees: dSPesees
                                                              , mocTable: mOC_MODE_COMPTABILISATIONTableAdapter
                                                              , annTable: aNN_ANNEETableAdapter
                                                              , cbModeComptabilisation: mOC_IDComboBox
                                                              , optKg: aNN_IS_PRIX_KGRadioButton
                                                              , optLitre: optLitres
                                                              , adrCompletTable: adresseCompletTableAdapter);
            _manager.AddControl(aNN_ENCAVEUR_NUMEROTextBox);
            _manager.AddControl(aNN_ENCAVEUR_SOCIETETextBox);
            _manager.AddControl(aNN_ENCAVEUR_NOMTextBox);
            _manager.AddControl(aNN_ENCAVEUR_PRENOMTextBox);
            _manager.AddControl(aNN_IS_PRIX_KGRadioButton);
            _manager.AddControl(optLitres);
            _manager.AddControl(aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox);
            AddSpecificControls();

            if (Utilities.GetOption(2))
            {
                if (GlobalParam.Instance.Erp == ErpEnum.Winbiz) UpdateWinbizModeComptabilisation();

                _manager.AddControl(mOC_IDComboBox);
                mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(dSPesees.MOC_MODE_COMPTABILISATION,
                                                                  Utilities.IdAnneeCourante);
            }
            else
            {
                mOC_IDComboBox.Hide();
                mOC_IDLabel.Hide();
                optLitres.Hide();
                aNN_IS_PRIX_KGRadioButton.Hide();
                lPrix.Hide();
            }
            _manager.AddControl(aDR_IDComboBox);
            btnAddYear.Click += BtnAddYear_Click;
        }

        private void UpdateWinbizModeComptabilisation()
        {
            try
            {
                new BDVendangeControleur().UpdateModeComptabilisations();
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Au moins un dossier WinBIZ est indisponible.\n" +
                                "Les modes de comptabilisation de ce(s) dossier(s) n'ont pas été synchronisés.",
                                "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BtnAddYear_Click(object sender, EventArgs e)
        {
            int ligne = aNN_ANNEEDataGridView.RowCount - 2;
            aNN_ANTextBox.Text = (aNN_ANNEETableAdapter.GetMaxAn() + 1).ToString();

            try
            {
                aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.Checked = false;
                aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.Checked = (bool)aNN_ANNEEDataGridView["ANN_ENCAVEUR_SOUMIS_CONTROL", ligne].Value;
            }
            catch (InvalidCastException)
            {
                aNN_ENCAVEUR_SOUMIS_CONTROLCheckBox.CheckState = CheckState.Unchecked;
            }

            aNN_ENCAVEUR_NOMTextBox.Text = aNN_ANNEEDataGridView["ANN_ENCAVEUR_NOM", ligne].Value.ToString();
            aNN_ENCAVEUR_PRENOMTextBox.Text = aNN_ANNEEDataGridView["ANN_ENCAVEUR_PRENOM", ligne].Value.ToString();
            aDR_IDComboBox.SelectedValue = aNN_ANNEEDataGridView["ADR_ID", ligne].Value;
            aNN_ENCAVEUR_NUMEROTextBox.Text = aNN_ANNEEDataGridView["ANN_ENCAVEUR_NUMERO", ligne].Value.ToString();
            aNN_ENCAVEUR_SOCIETETextBox.Text = aNN_ANNEEDataGridView["ANN_ENCAVEUR_SOCIETE", ligne].Value.ToString();
            mOC_IDComboBox.SelectedValue = aNN_ANNEEDataGridView["MOC_ID", ligne].Value;

            InitSpecificValues(ligne);

            try
            {
                aNN_IS_PRIX_KGRadioButton.Checked = (bool)aNN_ANNEEDataGridView["ANN_IS_PRIX_KG", ligne].Value;
            }
            catch (InvalidCastException)
            {
                aNN_IS_PRIX_KGRadioButton.Checked = false;
            }
        }

        private void AdressesContextMenuStrip_Click(object sender, EventArgs e)
        {
            WfrmGestionDesAdresses.Wfrm.WfrmShowDialog();
            adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
        }
    }
}