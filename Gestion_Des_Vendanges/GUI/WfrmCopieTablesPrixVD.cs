﻿namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire permettant la copie des tables de prix d'un lieu de production
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    public partial class WfrmCopieTablesPrix
    {
        private readonly string _columnName = "CEP_ID";

        private int GetValId(int id, int idDestination)
        {
            return (int)vAL_VALEUR_CEPAGETableAdapter.GetValIdByCepAnnLie(id, idDestination, BUSINESS.Utilities.IdAnneeCourante);
        }

        private void fillValSorted(int id)
        {
            vAL_VALEUR_CEPAGETableAdapter.FillByLieAnnOrderByCep(dSPesees.VAL_VALEUR_CEPAGE, id, BUSINESS.Utilities.IdAnneeCourante);
        }

        private void FillByBonusId()
        {
            lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByBonusAnnId(dSPesees.LIE_LIEU_DE_PRODUCTION, BUSINESS.Utilities.IdAnneeCourante);
        }

        private int GetNbVal(int id, int idDestination)
        {
            return (int)vAL_VALEUR_CEPAGETableAdapter.GetNbByCepLieAnn(id, idDestination, BUSINESS.Utilities.IdAnneeCourante);
        }

        private void fillVal(int id)
        {
            vAL_VALEUR_CEPAGETableAdapter.FillByLieAnn(dSPesees.VAL_VALEUR_CEPAGE, id, BUSINESS.Utilities.IdAnneeCourante);
        }

        private void FillOnLoad()
        {
            lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByBonusAnnId(dSPesees.LIE_LIEU_DE_PRODUCTION, BUSINESS.Utilities.IdAnneeCourante);
            tableAdapterDest.Fill(dsPeseesDest.LIE_LIEU_DE_PRODUCTION);
            cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
        }

        private void InsertVal(int id, int idDestination, int annId, double valeur)
        {
            //TODO | !Next TO IMPLEMENT VAL_OECHSLE_MOYEN: 0, VAL_BON_TYPE: 0
            vAL_VALEUR_CEPAGETableAdapter.Insert(CEP_ID: id, ANN_ID: annId, VAL_VALEUR: valeur, LIE_ID: idDestination, VAL_OECHSLE_MOYEN: 0, VAL_BON_TYPE: 0, CLA_ID: 1); // TODO Set cla_id to the right value
        }
    }
}