﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des cépages
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmGestionDesCepages : WfrmSetting
    {
        private readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(WfrmGestionDesCepages));
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesCepages _wfrm;

        public static WfrmGestionDesCepages Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesCepages();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int cepID = (int)cEP_CEPAGEDataGridView.CurrentRow.Cells["CEP_ID"].Value;
                    resultat = ((int)cEP_CEPAGETableAdapter.NbRef(cepID) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesCepages()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesCepages_Load(object sender, EventArgs e)
        {
            cEP_CEPAGEDataGridView.DataError += new DataGridViewDataErrorEventHandler(CEP_CEPAGEDataGridView_DataError);

            InitSepecificTableAdapters();

            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);

            cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
            cOU_COULEURTableAdapter.FillCouCepage(dSPesees.COU_COULEUR);

            bool optionWinBiz = BUSINESS.Utilities.GetOption(2);
            if (optionWinBiz)
            {
                try
                {
                    new BDVendangeControleur().UpdateModeComptabilisations(BUSINESS.Utilities.IdAnneeCourante);
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    MessageBox.Show("Attention le dossier WinBIZ est indisponible.\n" +
                                    "Les modes de comptabilisation ne sont pas synchronisés.", "Gestion des vendanges",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                }
                mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(this.dSPesees.MOC_MODE_COMPTABILISATION, BUSINESS.Utilities.IdAnneeCourante);
            }

            AddYearCurrentFolderToTitleWindow();

            InitManager(optionWinBiz);

            _manager.AddControl(cEP_NOMTextBox);
            _manager.AddControl(cEP_NUMEROTextBox);
            _manager.AddControl(gbModeComtabilisation);
            _manager.AddControl(optSpecifique);
            _manager.AddControl(mOC_IDComboBox);
        }

        private void CEP_CEPAGEDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Erreur lors de la création de la grille des cépages.\n" +
                            "Ceci peut se produire lorsque tous les champs ne sont pas saisis.",
                            "Gestion des vendanges",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
            _log.Warn(e.ToString());
        }

        private void AddYearCurrentFolderToTitleWindow()
        {
            if ((BUSINESS.Utilities.IdAnneeCourante != 0))
                Text += " - " + BUSINESS.Utilities.AnneeCourante.ToString();
        }

        private void COU_IDComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)13:
                    SendKeys.Send("{TAB}");
                    break;

                default:
                    break;
            }
        }

        private void AccederAuxCouleursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCouleurs.Wfrm.WfrmShowDialog();
            cOU_COULEURTableAdapter.Fill(dSPesees.COU_COULEUR);
        }
    }
}