﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesPesees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pEE_PESEE_ENTETEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pEE_PESEE_ENTETETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.pEE_PESEE_ENTETEDataGridView = new System.Windows.Forms.DataGridView();
            this.PEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aCQACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PEE_ID_PRODUCTEUR = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RES_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.rESNOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LIE_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AUT_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PEE_ID_KG = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pEE_TOTAL_QUANTITE_BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PEE_ID_LITRES = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pEE_TOTAL_QUANTITE_BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.PEE_GRANDCRU = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PROID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.PEE_LIEU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acquit_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_REMARQUES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_DIVERS1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_DIVERS2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_DIVERS3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.rES_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_NOMCOMPLETTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrintQuittance = new System.Windows.Forms.ToolStripSplitButton();
            this.quittanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quittanceAvecInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrintProduction = new System.Windows.Forms.ToolStripSplitButton();
            this.groupéParOeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrintListeVendanges = new System.Windows.Forms.ToolStripSplitButton();
            this.listeDesVendangesLivréesParDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.pEE_PRODUCTEUR_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PRODUCTEUR_NOMCOMPLETTableAdapter();
            this.pEE_TOTAL_QUANTITETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_TOTAL_QUANTITETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rESNOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITE_BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITE_BindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pEE_PESEE_ENTETEBindingSource
            // 
            this.pEE_PESEE_ENTETEBindingSource.DataMember = "PEE_PESEE_ENTETE";
            this.pEE_PESEE_ENTETEBindingSource.DataSource = this.dSPesees;
            // 
            // pEE_PESEE_ENTETETableAdapter
            // 
            this.pEE_PESEE_ENTETETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = this.pEE_PESEE_ENTETETableAdapter;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // pEE_PESEE_ENTETEDataGridView
            // 
            this.pEE_PESEE_ENTETEDataGridView.AllowUserToAddRows = false;
            this.pEE_PESEE_ENTETEDataGridView.AllowUserToDeleteRows = false;
            this.pEE_PESEE_ENTETEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pEE_PESEE_ENTETEDataGridView.AutoGenerateColumns = false;
            this.pEE_PESEE_ENTETEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pEE_PESEE_ENTETEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pEE_PESEE_ENTETEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PEE_ID,
            this.PEE_DATE,
            this.ACQ_ID,
            this.PEE_ID_PRODUCTEUR,
            this.RES_ID,
            this.LIE_ID,
            this.AUT_ID,
            this.PEE_ID_KG,
            this.PEE_ID_LITRES,
            this.PEE_GRANDCRU,
            this.PROID,
            this.PEE_LIEU,
            this.acquit_id,
            this.PEE_REMARQUES,
            this.PEE_DIVERS1,
            this.PEE_DIVERS2,
            this.PEE_DIVERS3});
            this.pEE_PESEE_ENTETEDataGridView.DataSource = this.pEE_PESEE_ENTETEBindingSource;
            this.pEE_PESEE_ENTETEDataGridView.Location = new System.Drawing.Point(3, 55);
            this.pEE_PESEE_ENTETEDataGridView.MultiSelect = false;
            this.pEE_PESEE_ENTETEDataGridView.Name = "pEE_PESEE_ENTETEDataGridView";
            this.pEE_PESEE_ENTETEDataGridView.ReadOnly = true;
            this.pEE_PESEE_ENTETEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.pEE_PESEE_ENTETEDataGridView.Size = new System.Drawing.Size(882, 398);
            this.pEE_PESEE_ENTETEDataGridView.TabIndex = 24;
            this.pEE_PESEE_ENTETEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PEE_PESEE_ENTETEDataGridView_ColumnHeaderMouseClick);
            // 
            // PEE_ID
            // 
            this.PEE_ID.DataPropertyName = "PEE_ID";
            this.PEE_ID.HeaderText = "Bulletin";
            this.PEE_ID.Name = "PEE_ID";
            this.PEE_ID.ReadOnly = true;
            this.PEE_ID.Width = 60;
            // 
            // PEE_DATE
            // 
            this.PEE_DATE.DataPropertyName = "PEE_DATE";
            dataGridViewCellStyle5.Format = "d";
            dataGridViewCellStyle5.NullValue = null;
            this.PEE_DATE.DefaultCellStyle = dataGridViewCellStyle5;
            this.PEE_DATE.HeaderText = "Date";
            this.PEE_DATE.Name = "PEE_DATE";
            this.PEE_DATE.ReadOnly = true;
            this.PEE_DATE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PEE_DATE.Width = 80;
            // 
            // ACQ_ID
            // 
            this.ACQ_ID.DataPropertyName = "ACQ_ID";
            this.ACQ_ID.DataSource = this.aCQACQUITBindingSource;
            this.ACQ_ID.DisplayMember = "ACQ_NUMERO";
            this.ACQ_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ACQ_ID.HeaderText = "Acquit";
            this.ACQ_ID.Name = "ACQ_ID";
            this.ACQ_ID.ReadOnly = true;
            this.ACQ_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ACQ_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.ACQ_ID.ValueMember = "ACQ_ID";
            this.ACQ_ID.Width = 60;
            // 
            // aCQACQUITBindingSource
            // 
            this.aCQACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // PEE_ID_PRODUCTEUR
            // 
            this.PEE_ID_PRODUCTEUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PEE_ID_PRODUCTEUR.DataPropertyName = "PEE_ID";
            this.PEE_ID_PRODUCTEUR.DataSource = this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource;
            this.PEE_ID_PRODUCTEUR.DisplayMember = "NOMCOMPLET";
            this.PEE_ID_PRODUCTEUR.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PEE_ID_PRODUCTEUR.HeaderText = "Producteur";
            this.PEE_ID_PRODUCTEUR.MinimumWidth = 100;
            this.PEE_ID_PRODUCTEUR.Name = "PEE_ID_PRODUCTEUR";
            this.PEE_ID_PRODUCTEUR.ReadOnly = true;
            this.PEE_ID_PRODUCTEUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PEE_ID_PRODUCTEUR.ValueMember = "PEE_ID";
            // 
            // pEE_PRODUCTEUR_NOMCOMPLET_BindingSource
            // 
            this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource.DataMember = "PEE_PRODUCTEUR_NOMCOMPLET";
            this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource.DataSource = this.dSPesees;
            // 
            // RES_ID
            // 
            this.RES_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RES_ID.DataPropertyName = "RES_ID";
            this.RES_ID.DataSource = this.rESNOMCOMPLETBindingSource;
            this.RES_ID.DisplayMember = "NOMCOMPLET";
            this.RES_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.RES_ID.HeaderText = "Responsable";
            this.RES_ID.MinimumWidth = 100;
            this.RES_ID.Name = "RES_ID";
            this.RES_ID.ReadOnly = true;
            this.RES_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RES_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.RES_ID.ValueMember = "RES_ID";
            // 
            // rESNOMCOMPLETBindingSource
            // 
            this.rESNOMCOMPLETBindingSource.DataMember = "RES_NOMCOMPLET";
            this.rESNOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // LIE_ID
            // 
            this.LIE_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LIE_ID.DataPropertyName = "LIE_ID";
            this.LIE_ID.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.LIE_ID.DisplayMember = "LIE_NOM";
            this.LIE_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.LIE_ID.HeaderText = "Lieu de production";
            this.LIE_ID.MinimumWidth = 100;
            this.LIE_ID.Name = "LIE_ID";
            this.LIE_ID.ReadOnly = true;
            this.LIE_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LIE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.LIE_ID.ValueMember = "LIE_ID";
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // AUT_ID
            // 
            this.AUT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AUT_ID.DataPropertyName = "AUT_ID";
            this.AUT_ID.DataSource = this.aUTAUTREMENTIONBindingSource;
            this.AUT_ID.DisplayMember = "AUT_NOM";
            this.AUT_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.AUT_ID.HeaderText = "Nom local";
            this.AUT_ID.MinimumWidth = 60;
            this.AUT_ID.Name = "AUT_ID";
            this.AUT_ID.ReadOnly = true;
            this.AUT_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AUT_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.AUT_ID.ValueMember = "AUT_ID";
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // PEE_ID_KG
            // 
            this.PEE_ID_KG.DataPropertyName = "PEE_ID";
            this.PEE_ID_KG.DataSource = this.pEE_TOTAL_QUANTITE_BindingSource;
            this.PEE_ID_KG.DisplayMember = "SUM_KG";
            this.PEE_ID_KG.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PEE_ID_KG.HeaderText = "Kg";
            this.PEE_ID_KG.Name = "PEE_ID_KG";
            this.PEE_ID_KG.ReadOnly = true;
            this.PEE_ID_KG.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PEE_ID_KG.ValueMember = "PEE_ID";
            this.PEE_ID_KG.Width = 50;
            // 
            // pEE_TOTAL_QUANTITE_BindingSource
            // 
            this.pEE_TOTAL_QUANTITE_BindingSource.DataMember = "PEE_TOTAL_QUANTITE";
            this.pEE_TOTAL_QUANTITE_BindingSource.DataSource = this.dSPesees;
            // 
            // PEE_ID_LITRES
            // 
            this.PEE_ID_LITRES.DataPropertyName = "PEE_ID";
            this.PEE_ID_LITRES.DataSource = this.pEE_TOTAL_QUANTITE_BindingSource1;
            this.PEE_ID_LITRES.DisplayMember = "SUM_LITRES";
            this.PEE_ID_LITRES.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PEE_ID_LITRES.HeaderText = "Litres";
            this.PEE_ID_LITRES.Name = "PEE_ID_LITRES";
            this.PEE_ID_LITRES.ReadOnly = true;
            this.PEE_ID_LITRES.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PEE_ID_LITRES.ValueMember = "PEE_ID";
            this.PEE_ID_LITRES.Width = 50;
            // 
            // pEE_TOTAL_QUANTITE_BindingSource1
            // 
            this.pEE_TOTAL_QUANTITE_BindingSource1.DataMember = "PEE_TOTAL_QUANTITE";
            this.pEE_TOTAL_QUANTITE_BindingSource1.DataSource = this.dSPesees;
            // 
            // PEE_GRANDCRU
            // 
            this.PEE_GRANDCRU.DataPropertyName = "PEE_GRANDCRU";
            this.PEE_GRANDCRU.HeaderText = "Grand cru";
            this.PEE_GRANDCRU.Name = "PEE_GRANDCRU";
            this.PEE_GRANDCRU.ReadOnly = true;
            this.PEE_GRANDCRU.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PEE_GRANDCRU.Width = 50;
            // 
            // PROID
            // 
            this.PROID.DataPropertyName = "PEE_ID";
            this.PROID.DataSource = this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource;
            this.PROID.DisplayMember = "PRO_ID";
            this.PROID.HeaderText = "PRO_ID";
            this.PROID.Name = "PROID";
            this.PROID.ReadOnly = true;
            this.PROID.ValueMember = "PEE_ID";
            this.PROID.Visible = false;
            // 
            // PEE_LIEU
            // 
            this.PEE_LIEU.DataPropertyName = "PEE_LIEU";
            this.PEE_LIEU.HeaderText = "PEE_LIEU";
            this.PEE_LIEU.Name = "PEE_LIEU";
            this.PEE_LIEU.ReadOnly = true;
            this.PEE_LIEU.Visible = false;
            // 
            // acquit_id
            // 
            this.acquit_id.DataPropertyName = "ACQ_ID";
            this.acquit_id.HeaderText = "ACQ_ID";
            this.acquit_id.Name = "acquit_id";
            this.acquit_id.ReadOnly = true;
            this.acquit_id.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.acquit_id.Visible = false;
            // 
            // PEE_REMARQUES
            // 
            this.PEE_REMARQUES.DataPropertyName = "PEE_REMARQUES";
            this.PEE_REMARQUES.HeaderText = "PEE_REMARQUES";
            this.PEE_REMARQUES.Name = "PEE_REMARQUES";
            this.PEE_REMARQUES.ReadOnly = true;
            this.PEE_REMARQUES.Visible = false;
            // 
            // PEE_DIVERS1
            // 
            this.PEE_DIVERS1.DataPropertyName = "PEE_DIVERS1";
            this.PEE_DIVERS1.HeaderText = "PEE_DIVERS1";
            this.PEE_DIVERS1.Name = "PEE_DIVERS1";
            this.PEE_DIVERS1.ReadOnly = true;
            this.PEE_DIVERS1.Visible = false;
            // 
            // PEE_DIVERS2
            // 
            this.PEE_DIVERS2.DataPropertyName = "PEE_DIVERS2";
            this.PEE_DIVERS2.HeaderText = "PEE_DIVERS2";
            this.PEE_DIVERS2.Name = "PEE_DIVERS2";
            this.PEE_DIVERS2.ReadOnly = true;
            this.PEE_DIVERS2.Visible = false;
            // 
            // PEE_DIVERS3
            // 
            this.PEE_DIVERS3.DataPropertyName = "PEE_DIVERS3";
            this.PEE_DIVERS3.HeaderText = "PEE_DIVERS3";
            this.PEE_DIVERS3.Name = "PEE_DIVERS3";
            this.PEE_DIVERS3.ReadOnly = true;
            this.PEE_DIVERS3.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 18);
            this.label1.TabIndex = 25;
            this.label1.Text = "Attestation de contrôle";
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // rES_NOMCOMPLETTableAdapter
            // 
            this.rES_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbAnnee);
            this.panel1.Location = new System.Drawing.Point(157, 158);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 211);
            this.panel1.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 21);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            this.cbAnnee.SelectedIndexChanged += new System.EventHandler(this.CbAnnee_SelectedIndexChanged);
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator1,
            this.toolStripSplitButton1,
            this.toolPrintQuittance,
            this.toolPrintProduction,
            this.toolPrintListeVendanges,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(888, 27);
            this.toolStrip1.TabIndex = 28;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = global::Gestion_Des_Vendanges.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(70, 24);
            this.toolAdd.Text = "Ajouter";
            this.toolAdd.Click += new System.EventHandler(this.BtnAddAcquit_Click);
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = global::Gestion_Des_Vendanges.Properties.Resources.open;
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(76, 24);
            this.toolUpdate.Text = "Modifier";
            this.toolUpdate.Click += new System.EventHandler(this.BtnModifierPesee_Click);
            // 
            // toolDelete
            // 
            this.toolDelete.Image = global::Gestion_Des_Vendanges.Properties.Resources.delete;
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(86, 24);
            this.toolDelete.Text = "Supprimer";
            this.toolDelete.Click += new System.EventHandler(this.BtnDeleteAcquit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.toolStripSplitButton1.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(164, 24);
            this.toolStripSplitButton1.Text = "Attestation de contrôle";
            this.toolStripSplitButton1.ButtonClick += new System.EventHandler(this.AttestationDeContrôleV2ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(309, 22);
            this.toolStripMenuItem1.Text = "Attestation de contrôle";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.AttestationDeContrôleToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(309, 22);
            this.toolStripMenuItem4.Text = "Attestation de contrôle V2";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.AttestationDeContrôleV2ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(309, 22);
            this.toolStripMenuItem2.Text = "Attestation de contrôle avec solde";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.AttestationDeContrôleAvecSoldeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(309, 22);
            this.toolStripMenuItem3.Text = "Attestation de contrôle avec solde par acquit";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.AttestationDeContrôleAvecSoldeParAcquitToolStripMenuItem_Click);
            // 
            // toolPrintQuittance
            // 
            this.toolPrintQuittance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quittanceToolStripMenuItem,
            this.quittanceAvecInformationToolStripMenuItem});
            this.toolPrintQuittance.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintQuittance.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintQuittance.Name = "toolPrintQuittance";
            this.toolPrintQuittance.Size = new System.Drawing.Size(95, 24);
            this.toolPrintQuittance.Text = "Quittance";
            this.toolPrintQuittance.ButtonClick += new System.EventHandler(this.BtnAfficherQuittanceV2_Click);
            // 
            // quittanceToolStripMenuItem
            // 
            this.quittanceToolStripMenuItem.Name = "quittanceToolStripMenuItem";
            this.quittanceToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.quittanceToolStripMenuItem.Text = "Quittance";
            this.quittanceToolStripMenuItem.Click += new System.EventHandler(this.BtnAfficherQuittance_Click);
            // 
            // quittanceAvecInformationToolStripMenuItem
            // 
            this.quittanceAvecInformationToolStripMenuItem.Name = "quittanceAvecInformationToolStripMenuItem";
            this.quittanceAvecInformationToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.quittanceAvecInformationToolStripMenuItem.Text = "Quittance avec informations";
            this.quittanceAvecInformationToolStripMenuItem.Click += new System.EventHandler(this.BtnAfficherQuittanceV2_Click);
            // 
            // toolPrintProduction
            // 
            this.toolPrintProduction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.groupéParOeToolStripMenuItem,
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem});
            this.toolPrintProduction.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintProduction.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintProduction.Name = "toolPrintProduction";
            this.toolPrintProduction.Size = new System.Drawing.Size(167, 24);
            this.toolPrintProduction.Text = "Apports par producteur";
            this.toolPrintProduction.ButtonClick += new System.EventHandler(this.BtnAfficherApportsProducteur_Click);
            // 
            // groupéParOeToolStripMenuItem
            // 
            this.groupéParOeToolStripMenuItem.Name = "groupéParOeToolStripMenuItem";
            this.groupéParOeToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.groupéParOeToolStripMenuItem.Text = "Groupé par °Oe";
            this.groupéParOeToolStripMenuItem.Click += new System.EventHandler(this.BtnAfficherApportsProducteurGrpOe_Click);
            // 
            // avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem
            // 
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem.Name = "avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem";
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem.Text = "Avec droit en litres de moût de raisin";
            this.avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem.Click += new System.EventHandler(this.AvecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem_Click);
            // 
            // toolPrintListeVendanges
            // 
            this.toolPrintListeVendanges.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesVendangesLivréesParDateToolStripMenuItem,
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem});
            this.toolPrintListeVendanges.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintListeVendanges.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintListeVendanges.Name = "toolPrintListeVendanges";
            this.toolPrintListeVendanges.Size = new System.Drawing.Size(184, 24);
            this.toolPrintListeVendanges.Text = "Liste des vendanges livrées";
            // 
            // listeDesVendangesLivréesParDateToolStripMenuItem
            // 
            this.listeDesVendangesLivréesParDateToolStripMenuItem.Name = "listeDesVendangesLivréesParDateToolStripMenuItem";
            this.listeDesVendangesLivréesParDateToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.listeDesVendangesLivréesParDateToolStripMenuItem.Text = "Liste des vendanges livrées par date";
            this.listeDesVendangesLivréesParDateToolStripMenuItem.Click += new System.EventHandler(this.BtnAfficherVendangesLivreesParDate);
            // 
            // listeDesVendangesLivréesParProducteurToolStripMenuItem
            // 
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem.Name = "listeDesVendangesLivréesParProducteurToolStripMenuItem";
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem.Text = "Liste des vendanges livrées par producteur";
            this.listeDesVendangesLivréesParProducteurToolStripMenuItem.Click += new System.EventHandler(this.BtnAfficherVendangesLivreesParProducteur);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // pEE_PRODUCTEUR_NOMCOMPLETTableAdapter
            // 
            this.pEE_PRODUCTEUR_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // pEE_TOTAL_QUANTITETableAdapter
            // 
            this.pEE_TOTAL_QUANTITETableAdapter.ClearBeforeFill = true;
            // 
            // CtrlGestionDesPesees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pEE_PESEE_ENTETEDataGridView);
            this.Name = "CtrlGestionDesPesees";
            this.Size = new System.Drawing.Size(888, 456);
            this.Load += new System.EventHandler(this.WfrmGestionDesPesees_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PESEE_ENTETEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_PRODUCTEUR_NOMCOMPLET_BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rESNOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITE_BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pEE_TOTAL_QUANTITE_BindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pEE_PESEE_ENTETEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter pEE_PESEE_ENTETETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView pEE_PESEE_ENTETEDataGridView;
        private System.Windows.Forms.Label label1;
        //  private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
       // private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
        //private System.Windows.Forms.BindingSource aPPAPPELLATIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private System.Windows.Forms.BindingSource aCQACQUITBindingSource;
        private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.BindingSource rESNOMCOMPLETBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_NOMCOMPLETTableAdapter rES_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSplitButton toolPrintProduction;
        private System.Windows.Forms.BindingSource pEE_PRODUCTEUR_NOMCOMPLET_BindingSource;
        private DAO.DSPeseesTableAdapters.PEE_PRODUCTEUR_NOMCOMPLETTableAdapter pEE_PRODUCTEUR_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.BindingSource pEE_TOTAL_QUANTITE_BindingSource;
        private DAO.DSPeseesTableAdapters.PEE_TOTAL_QUANTITETableAdapter pEE_TOTAL_QUANTITETableAdapter;
        private System.Windows.Forms.BindingSource pEE_TOTAL_QUANTITE_BindingSource1;
        private System.Windows.Forms.ToolStripSplitButton toolPrintQuittance;
        private System.Windows.Forms.ToolStripSplitButton toolPrintListeVendanges;
        private System.Windows.Forms.ToolStripMenuItem listeDesVendangesLivréesParProducteurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesVendangesLivréesParDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupéParOeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_DATE;
        private System.Windows.Forms.DataGridViewComboBoxColumn ACQ_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn PEE_ID_PRODUCTEUR;
        private System.Windows.Forms.DataGridViewComboBoxColumn RES_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn LIE_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn AUT_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn PEE_ID_KG;
        private System.Windows.Forms.DataGridViewComboBoxColumn PEE_ID_LITRES;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PEE_GRANDCRU;
        private System.Windows.Forms.DataGridViewComboBoxColumn PROID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_LIEU;
        private System.Windows.Forms.DataGridViewTextBoxColumn acquit_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_REMARQUES;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_DIVERS1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_DIVERS2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_DIVERS3;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem quittanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quittanceAvecInformationToolStripMenuItem;
    }
}