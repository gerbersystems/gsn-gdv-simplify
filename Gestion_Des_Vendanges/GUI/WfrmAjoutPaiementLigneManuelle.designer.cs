﻿using System;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiementLigneManuelle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label mOC_IDLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mOC_MODE_COMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPeseesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dgwLigneManuelle = new System.Windows.Forms.DataGridView();
            this.idList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.producteur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.texte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montantIsFixe = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.valeur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.btnSuivant = new System.Windows.Forms.Button();
            this.pROPROPRIETAIREBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRO_PROPRIETAIRETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrecedent = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.btnAddCepage = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnSaveCepage = new System.Windows.Forms.Button();
            this.labelTexte = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbProducteur = new System.Windows.Forms.ComboBox();
            this.optProprietaire = new System.Windows.Forms.RadioButton();
            this.optTous = new System.Windows.Forms.RadioButton();
            this.txtTexte = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numPourcentage = new System.Windows.Forms.NumericUpDown();
            this.optPourcentage = new System.Windows.Forms.RadioButton();
            this.txtMontantFixe = new System.Windows.Forms.TextBox();
            this.optFixe = new System.Windows.Forms.RadioButton();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.gpbExportImport = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gpbLine = new System.Windows.Forms.GroupBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnParcourirImport = new System.Windows.Forms.Button();
            this.txtParcourirImport = new System.Windows.Forms.TextBox();
            this.lblImport = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnParcourirExport = new System.Windows.Forms.Button();
            this.ExportDestinationPathTextBox = new System.Windows.Forms.TextBox();
            this.lblExport = new System.Windows.Forms.Label();
            this.ExportTypeCombobox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            mOC_IDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mOC_MODE_COMPTABILISATIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPeseesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwLigneManuelle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROPROPRIETAIREBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).BeginInit();
            this.gpbExportImport.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mOC_IDLabel
            // 
            mOC_IDLabel.AutoSize = true;
            mOC_IDLabel.Location = new System.Drawing.Point(24, 55);
            mOC_IDLabel.Name = "mOC_IDLabel";
            mOC_IDLabel.Size = new System.Drawing.Size(133, 13);
            mOC_IDLabel.TabIndex = 7;
            mOC_IDLabel.Text = "Mode de comptabilisation :\r\n";
            // 
            // mOC_MODE_COMPTABILISATIONBindingSource
            // 
            this.mOC_MODE_COMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOC_MODE_COMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPeseesBindingSource;
            // 
            // dSPeseesBindingSource
            // 
            this.dSPeseesBindingSource.DataSource = this.dSPesees;
            this.dSPeseesBindingSource.Position = 0;
            // 
            // dgwLigneManuelle
            // 
            this.dgwLigneManuelle.AllowUserToAddRows = false;
            this.dgwLigneManuelle.AllowUserToDeleteRows = false;
            this.dgwLigneManuelle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgwLigneManuelle.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwLigneManuelle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgwLigneManuelle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwLigneManuelle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idList,
            this.producteur,
            this.texte,
            this.montantIsFixe,
            this.valeur});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgwLigneManuelle.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgwLigneManuelle.Location = new System.Drawing.Point(386, 38);
            this.dgwLigneManuelle.Name = "dgwLigneManuelle";
            this.dgwLigneManuelle.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgwLigneManuelle.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgwLigneManuelle.Size = new System.Drawing.Size(456, 496);
            this.dgwLigneManuelle.TabIndex = 2;
            // 
            // idList
            // 
            this.idList.HeaderText = "idList";
            this.idList.Name = "idList";
            this.idList.ReadOnly = true;
            this.idList.Visible = false;
            // 
            // producteur
            // 
            this.producteur.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.producteur.HeaderText = "Producteur";
            this.producteur.MinimumWidth = 80;
            this.producteur.Name = "producteur";
            this.producteur.ReadOnly = true;
            // 
            // texte
            // 
            this.texte.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.texte.FillWeight = 120F;
            this.texte.HeaderText = "Descriptif";
            this.texte.MinimumWidth = 80;
            this.texte.Name = "texte";
            this.texte.ReadOnly = true;
            // 
            // montantIsFixe
            // 
            this.montantIsFixe.HeaderText = "Montant fixe";
            this.montantIsFixe.Name = "montantIsFixe";
            this.montantIsFixe.ReadOnly = true;
            this.montantIsFixe.Width = 50;
            // 
            // valeur
            // 
            this.valeur.HeaderText = "Valeur";
            this.valeur.Name = "valeur";
            this.valeur.ReadOnly = true;
            this.valeur.Width = 60;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // btnSuivant
            // 
            this.btnSuivant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuivant.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSuivant.Location = new System.Drawing.Point(689, 550);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(72, 30);
            this.btnSuivant.TabIndex = 0;
            this.btnSuivant.Text = "Terminer";
            this.btnSuivant.UseVisualStyleBackColor = false;
            this.btnSuivant.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // pROPROPRIETAIREBindingSource
            // 
            this.pROPROPRIETAIREBindingSource.DataMember = "PRO_PROPRIETAIRE";
            this.pROPROPRIETAIREBindingSource.DataSource = this.dSPesees;
            // 
            // pRO_PROPRIETAIRETableAdapter
            // 
            this.pRO_PROPRIETAIRETableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Saisie des lignes manuelles";
            // 
            // btnPrecedent
            // 
            this.btnPrecedent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrecedent.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPrecedent.Location = new System.Drawing.Point(611, 550);
            this.btnPrecedent.Name = "btnPrecedent";
            this.btnPrecedent.Size = new System.Drawing.Size(72, 30);
            this.btnPrecedent.TabIndex = 7;
            this.btnPrecedent.Text = "Précédent";
            this.btnPrecedent.UseVisualStyleBackColor = false;
            this.btnPrecedent.Click += new System.EventHandler(this.BtnPrecedent_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(767, 550);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 8;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.BtnAnnuler_Click);
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = this.mOC_MODE_COMPTABILISATIONTableAdapter;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = this.pRO_NOMCOMPLETTableAdapter;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = this.pRO_PROPRIETAIRETableAdapter;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // btnAddCepage
            // 
            this.btnAddCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddCepage.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnAddCepage.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnAddCepage.Location = new System.Drawing.Point(29, 518);
            this.btnAddCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddCepage.Name = "btnAddCepage";
            this.btnAddCepage.Size = new System.Drawing.Size(72, 30);
            this.btnAddCepage.TabIndex = 4;
            this.btnAddCepage.Text = "Nouveau";
            this.btnAddCepage.UseVisualStyleBackColor = false;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnSupprimer.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnSupprimer.Location = new System.Drawing.Point(181, 518);
            this.btnSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(73, 30);
            this.btnSupprimer.TabIndex = 6;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnSaveCepage
            // 
            this.btnSaveCepage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveCepage.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnSaveCepage.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnSaveCepage.Location = new System.Drawing.Point(105, 518);
            this.btnSaveCepage.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveCepage.Name = "btnSaveCepage";
            this.btnSaveCepage.Size = new System.Drawing.Size(72, 30);
            this.btnSaveCepage.TabIndex = 5;
            this.btnSaveCepage.Text = "Modifier";
            this.btnSaveCepage.UseVisualStyleBackColor = false;
            // 
            // labelTexte
            // 
            this.labelTexte.AutoSize = true;
            this.labelTexte.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.labelTexte.Location = new System.Drawing.Point(24, 22);
            this.labelTexte.Name = "labelTexte";
            this.labelTexte.Size = new System.Drawing.Size(40, 13);
            this.labelTexte.TabIndex = 5;
            this.labelTexte.Text = "Texte :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbProducteur);
            this.groupBox2.Controls.Add(this.optProprietaire);
            this.groupBox2.Controls.Add(this.optTous);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.groupBox2.Location = new System.Drawing.Point(27, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 86);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Choix du producteur";
            // 
            // cbProducteur
            // 
            this.cbProducteur.DisplayMember = "PRO_ID";
            this.cbProducteur.FormattingEnabled = true;
            this.cbProducteur.Location = new System.Drawing.Point(43, 55);
            this.cbProducteur.Name = "cbProducteur";
            this.cbProducteur.Size = new System.Drawing.Size(184, 21);
            this.cbProducteur.TabIndex = 2;
            this.cbProducteur.ValueMember = "PRO_ID";
            // 
            // optProprietaire
            // 
            this.optProprietaire.AutoSize = true;
            this.optProprietaire.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.optProprietaire.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.optProprietaire.Location = new System.Drawing.Point(16, 58);
            this.optProprietaire.Name = "optProprietaire";
            this.optProprietaire.Size = new System.Drawing.Size(14, 13);
            this.optProprietaire.TabIndex = 1;
            this.optProprietaire.TabStop = true;
            this.optProprietaire.UseVisualStyleBackColor = true;
            this.optProprietaire.CheckedChanged += new System.EventHandler(this.CheckedOption);
            // 
            // optTous
            // 
            this.optTous.AutoSize = true;
            this.optTous.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.optTous.Location = new System.Drawing.Point(16, 22);
            this.optTous.Name = "optTous";
            this.optTous.Size = new System.Drawing.Size(55, 17);
            this.optTous.TabIndex = 0;
            this.optTous.TabStop = true;
            this.optTous.Text = "  Tous";
            this.optTous.UseVisualStyleBackColor = true;
            this.optTous.CheckedChanged += new System.EventHandler(this.CheckedOption);
            // 
            // txtTexte
            // 
            this.txtTexte.Location = new System.Drawing.Point(167, 22);
            this.txtTexte.Name = "txtTexte";
            this.txtTexte.Size = new System.Drawing.Size(184, 19);
            this.txtTexte.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numPourcentage);
            this.groupBox3.Controls.Add(this.optPourcentage);
            this.groupBox3.Controls.Add(this.txtMontantFixe);
            this.groupBox3.Controls.Add(this.optFixe);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.groupBox3.Location = new System.Drawing.Point(27, 178);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 81);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calcul du montant";
            // 
            // numPourcentage
            // 
            this.numPourcentage.DecimalPlaces = 2;
            this.numPourcentage.Location = new System.Drawing.Point(141, 51);
            this.numPourcentage.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPourcentage.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numPourcentage.Name = "numPourcentage";
            this.numPourcentage.Size = new System.Drawing.Size(151, 19);
            this.numPourcentage.TabIndex = 3;
            // 
            // optPourcentage
            // 
            this.optPourcentage.AutoSize = true;
            this.optPourcentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.optPourcentage.Location = new System.Drawing.Point(16, 51);
            this.optPourcentage.Name = "optPourcentage";
            this.optPourcentage.Size = new System.Drawing.Size(92, 17);
            this.optPourcentage.TabIndex = 1;
            this.optPourcentage.TabStop = true;
            this.optPourcentage.Text = "Pourcentage :";
            this.optPourcentage.UseVisualStyleBackColor = true;
            this.optPourcentage.CheckedChanged += new System.EventHandler(this.CheckedOption);
            // 
            // txtMontantFixe
            // 
            this.txtMontantFixe.Location = new System.Drawing.Point(141, 20);
            this.txtMontantFixe.Name = "txtMontantFixe";
            this.txtMontantFixe.Size = new System.Drawing.Size(151, 19);
            this.txtMontantFixe.TabIndex = 2;
            this.txtMontantFixe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // optFixe
            // 
            this.optFixe.AutoSize = true;
            this.optFixe.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.optFixe.Location = new System.Drawing.Point(16, 21);
            this.optFixe.Name = "optFixe";
            this.optFixe.Size = new System.Drawing.Size(113, 17);
            this.optFixe.TabIndex = 0;
            this.optFixe.TabStop = true;
            this.optFixe.Text = "Montant fixe CHF :";
            this.optFixe.UseVisualStyleBackColor = true;
            this.optFixe.CheckedChanged += new System.EventHandler(this.CheckedOption);
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mOC_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mOC_IDComboBox.DataSource = this.mOC_MODE_COMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(167, 52);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(184, 21);
            this.mOC_IDComboBox.TabIndex = 1;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            // 
            // gpbExportImport
            // 
            this.gpbExportImport.Controls.Add(this.groupBox4);
            this.gpbExportImport.Controls.Add(this.gpbLine);
            this.gpbExportImport.Controls.Add(this.btnImport);
            this.gpbExportImport.Controls.Add(this.btnParcourirImport);
            this.gpbExportImport.Controls.Add(this.txtParcourirImport);
            this.gpbExportImport.Controls.Add(this.lblImport);
            this.gpbExportImport.Controls.Add(this.btnExport);
            this.gpbExportImport.Controls.Add(this.btnParcourirExport);
            this.gpbExportImport.Controls.Add(this.ExportDestinationPathTextBox);
            this.gpbExportImport.Controls.Add(this.lblExport);
            this.gpbExportImport.Controls.Add(this.ExportTypeCombobox);
            this.gpbExportImport.Enabled = false;
            this.gpbExportImport.Location = new System.Drawing.Point(27, 265);
            this.gpbExportImport.Name = "gpbExportImport";
            this.gpbExportImport.Size = new System.Drawing.Size(310, 237);
            this.gpbExportImport.TabIndex = 8;
            this.gpbExportImport.TabStop = false;
            this.gpbExportImport.Text = "Export/Import";
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(1, 45);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(308, 10);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            // 
            // gpbLine
            // 
            this.gpbLine.Location = new System.Drawing.Point(1, 138);
            this.gpbLine.Name = "gpbLine";
            this.gpbLine.Size = new System.Drawing.Size(308, 10);
            this.gpbLine.TabIndex = 9;
            this.gpbLine.TabStop = false;
            // 
            // btnImport
            // 
            this.btnImport.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnImport.Location = new System.Drawing.Point(16, 201);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(196, 30);
            this.btnImport.TabIndex = 8;
            this.btnImport.Text = "Importer Liste Lignes Manuelles";
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // btnParcourirImport
            // 
            this.btnParcourirImport.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnParcourirImport.Location = new System.Drawing.Point(218, 170);
            this.btnParcourirImport.Name = "btnParcourirImport";
            this.btnParcourirImport.Size = new System.Drawing.Size(74, 23);
            this.btnParcourirImport.TabIndex = 7;
            this.btnParcourirImport.Text = "Parcourir...";
            this.btnParcourirImport.UseVisualStyleBackColor = false;
            this.btnParcourirImport.Click += new System.EventHandler(this.BtnParcourirImport_Click);
            // 
            // txtParcourirImport
            // 
            this.txtParcourirImport.BackColor = System.Drawing.Color.White;
            this.txtParcourirImport.Location = new System.Drawing.Point(16, 172);
            this.txtParcourirImport.Name = "txtParcourirImport";
            this.txtParcourirImport.Size = new System.Drawing.Size(196, 19);
            this.txtParcourirImport.TabIndex = 6;
            // 
            // lblImport
            // 
            this.lblImport.AutoSize = true;
            this.lblImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.lblImport.Location = new System.Drawing.Point(13, 156);
            this.lblImport.Name = "lblImport";
            this.lblImport.Size = new System.Drawing.Size(183, 13);
            this.lblImport.TabIndex = 5;
            this.lblImport.Text = "Sélectionner le fichier csv à importer :";
            // 
            // btnExport
            // 
            this.btnExport.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnExport.Location = new System.Drawing.Point(16, 107);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(196, 30);
            this.btnExport.TabIndex = 4;
            this.btnExport.Text = "Exporter Liste Fournisseurs";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // btnParcourirExport
            // 
            this.btnParcourirExport.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnParcourirExport.Location = new System.Drawing.Point(218, 76);
            this.btnParcourirExport.Name = "btnParcourirExport";
            this.btnParcourirExport.Size = new System.Drawing.Size(74, 23);
            this.btnParcourirExport.TabIndex = 3;
            this.btnParcourirExport.Text = "Parcourir...";
            this.btnParcourirExport.UseVisualStyleBackColor = false;
            this.btnParcourirExport.Click += new System.EventHandler(this.BtnParcourirExport_Click);
            // 
            // ExportDestinationPathTextBox
            // 
            this.ExportDestinationPathTextBox.BackColor = System.Drawing.Color.White;
            this.ExportDestinationPathTextBox.Location = new System.Drawing.Point(16, 78);
            this.ExportDestinationPathTextBox.Name = "ExportDestinationPathTextBox";
            this.ExportDestinationPathTextBox.Size = new System.Drawing.Size(196, 19);
            this.ExportDestinationPathTextBox.TabIndex = 2;
            // 
            // lblExport
            // 
            this.lblExport.AutoSize = true;
            this.lblExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.lblExport.Location = new System.Drawing.Point(13, 62);
            this.lblExport.Name = "lblExport";
            this.lblExport.Size = new System.Drawing.Size(284, 13);
            this.lblExport.TabIndex = 1;
            this.lblExport.Text = "Sélectionner le dossier de destination du fichier à exporter :";
            // 
            // ExportTypeCombobox
            // 
            this.ExportTypeCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ExportTypeCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExportTypeCombobox.BackColor = System.Drawing.Color.White;
            this.ExportTypeCombobox.FormattingEnabled = true;
            this.ExportTypeCombobox.Items.AddRange(new object[] {
            "Sélectionner le format...",
            "Standard",
            "Zone de difficulté de travail",
            "Avec mode de comptabilisation"});
            this.ExportTypeCombobox.Location = new System.Drawing.Point(16, 21);
            this.ExportTypeCombobox.Name = "ExportTypeCombobox";
            this.ExportTypeCombobox.Size = new System.Drawing.Size(196, 21);
            this.ExportTypeCombobox.TabIndex = 0;
            this.ExportTypeCombobox.Text = "Sélectionner le format...";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gpbExportImport);
            this.groupBox1.Controls.Add(mOC_IDLabel);
            this.groupBox1.Controls.Add(this.mOC_IDComboBox);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.txtTexte);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.labelTexte);
            this.groupBox1.Controls.Add(this.btnSaveCepage);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnAddCepage);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(357, 556);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ligne manuelle";
            // 
            // WfrmAjoutPaiementLigneManuelle
            // 
            this.AcceptButton = this.btnSuivant;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 591);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnPrecedent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSuivant);
            this.Controls.Add(this.dgwLigneManuelle);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(870, 630);
            this.MinimumSize = new System.Drawing.Size(870, 630);
            this.Name = "WfrmAjoutPaiementLigneManuelle";
            this.Text = "Gestion des paiements - Lignes manuelles";
            this.Load += new System.EventHandler(this.WfrmAjoutPaiementLigneManuel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mOC_MODE_COMPTABILISATIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPeseesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwLigneManuelle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROPROPRIETAIREBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).EndInit();
            this.gpbExportImport.ResumeLayout(false);
            this.gpbExportImport.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.DataGridView dgwLigneManuelle;
        private System.Windows.Forms.BindingSource dSPeseesBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.BindingSource pROPROPRIETAIREBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter pRO_PROPRIETAIRETableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idList;
        private System.Windows.Forms.DataGridViewTextBoxColumn producteur;
        private System.Windows.Forms.DataGridViewTextBoxColumn texte;
        private System.Windows.Forms.DataGridViewCheckBoxColumn montantIsFixe;
        private System.Windows.Forms.DataGridViewTextBoxColumn valeur;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrecedent;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.BindingSource mOC_MODE_COMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
        private DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button btnAddCepage;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnSaveCepage;
        private System.Windows.Forms.Label labelTexte;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbProducteur;
        private System.Windows.Forms.RadioButton optProprietaire;
        private System.Windows.Forms.RadioButton optTous;
        private System.Windows.Forms.TextBox txtTexte;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numPourcentage;
        private System.Windows.Forms.RadioButton optPourcentage;
        private System.Windows.Forms.TextBox txtMontantFixe;
        private System.Windows.Forms.RadioButton optFixe;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
        private System.Windows.Forms.GroupBox gpbExportImport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnParcourirImport;
        private System.Windows.Forms.TextBox txtParcourirImport;
        private System.Windows.Forms.Label lblImport;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnParcourirExport;
        private System.Windows.Forms.TextBox ExportDestinationPathTextBox;
        private System.Windows.Forms.Label lblExport;
        private System.Windows.Forms.ComboBox ExportTypeCombobox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gpbLine;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}