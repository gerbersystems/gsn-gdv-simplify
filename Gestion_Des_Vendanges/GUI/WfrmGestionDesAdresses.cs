﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des adresses
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
*   ATH - 16.11.2009 - Auto-complétion ville et npa
     *   Amélioration de adr_NPA_textBox_KeyPress
* */

    partial class WfrmGestionDesAdresses : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesAdresses _wfrm;

        public static WfrmGestionDesAdresses Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesAdresses();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int adrId = (int)aDR_ADRESSEDataGridView.CurrentRow.Cells["ADR_ID"].Value;
                    resultat = ((int)aDR_ADRESSETableAdapter.NbRef(adrId) == 0);
                }
                catch (Exception)
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesAdresses()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesAdresses_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(cOA_COMMUNE_ADRESSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aDR_ADRESSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            cOA_COMMUNE_ADRESSETableAdapter.Fill(dSPesees.COA_COMMUNE_ADRESSE);
            aDR_ADRESSETableAdapter.Fill(dSPesees.ADR_ADRESSE);
            cbVille.Items.AddRange(cOA_COMMUNE_ADRESSETableAdapter
                .GetData()
                .Select(c => c.COA_NOM)
                .Distinct()
                .ToArray());

            String s = "Gestion des vendanges";
            if ((BUSINESS.Utilities.IdAnneeCourante != 0))
            {
                s += " - " + BUSINESS.Utilities.AnneeCourante;
            }
            Text = s;

            _manager = new FormSettingsManagerPesees(this, aDR_IDTextBox, aDR_ADRESSE1TextBox, btnNewAdresse, btnAddAdress, btnSupprimer, this.aDR_ADRESSEDataGridView, aDR_ADRESSEBindingSource, tableAdapterManager, dSPesees);
            _manager.AddControl(aDR_ADRESSE1TextBox);
            _manager.AddControl(aDR_ADRESSE2TextBox);
            _manager.AddControl(aDR_ADRESSE3TextBox);
            _manager.AddControl(aDR_NPATextBox);
            _manager.AddControl(cbVille);
            _manager.AddControl(aDR_PAYSTextBox);
            _manager.AddControl(aDR_TELTextBox);
            _manager.AddControl(aDR_FAXTextBox);
            _manager.AddControl(aDR_NATELTextBox);
            _manager.AddControl(aDR_EMAILTextBox);
            _manager.AddControl(txtErpId);
        }

        private void aDR_NPATextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)8 && e.KeyChar != (char)13)
            {
                e.Handled = true;
            }
        }

        private void aDR_NPATextBox_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cbVille.Text))
            {
                try
                {
                    string ville = cOA_COMMUNE_ADRESSETableAdapter.GetNomByNpa(int.Parse(aDR_NPATextBox.Text));
                    if (ville != null)
                    {
                        cbVille.Text = ville;
                    }
                }
                catch
                {
                }
            }
        }

        private void cbVille_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(aDR_NPATextBox.Text))
            {
                int? npa = cOA_COMMUNE_ADRESSETableAdapter.GetNpaByNom(cbVille.Text);
                if (npa != null)
                {
                    aDR_NPATextBox.Text = npa.ToString();
                }
            }
        }
    }
}