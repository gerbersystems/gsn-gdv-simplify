﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des articles
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class CtrlGestionDesArticles : CtrlWfrm, ICtrlSetting
    {
        private FormSettingsManagerArticle _manager;

        public bool CanDelete
        {
            get
            {
                return aRT_ARTICLETableAdapter.GetArtIdWinBiz(Convert.ToInt32(aRT_IDTextBox.Text)) == null;
            }
        }

        public override ToolStrip ToolStrip
        {
            get
            {
                return toolStrip2;
            }
        }

        public override Panel OptionPanel
        {
            get
            {
                return panel1;
            }
        }

        private CtrlGestionDesArticles()
        {
            InitializeComponent();
        }

        public static CtrlGestionDesArticles GetInstance()
        {
            if (!BUSINESS.Utilities.GetOption(2))
            {
                MessageBox.Show("Attention, vous n'avez pas l'option exportation WinBIZ.\n" +
                "Vous ne pouvez pas exporter d'articles sans cette option.", "Gestion des vendanges", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            return new CtrlGestionDesArticles();
        }

        public override void UpdateDonnees()
        {
            mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(dSPesees.MOC_MODE_COMPTABILISATION, BUSINESS.Utilities.IdAnneeCourante);
            cOU_COULEURTableAdapter.Fill(this.dSPesees.COU_COULEUR);
            cLA_CLASSETableAdapter.Fill(this.dSPesees.CLA_CLASSE);
            cEP_CEPAGETableAdapter.Fill(this.dSPesees.CEP_CEPAGE);
            aUT_AUTRE_MENTIONTableAdapter.Fill(this.dSPesees.AUT_AUTRE_MENTION);
            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(this.dSPesees.LIE_LIEU_DE_PRODUCTION);
            cOM_COMMUNETableAdapter.Fill(this.dSPesees.COM_COMMUNE);
            aNN_ANNEETableAdapter.Fill(this.dSPesees.ANN_ANNEE);
            aRT_ARTICLETableAdapter.FillByAn(this.dSPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
            cbAnnee.SelectedValue = BUSINESS.Utilities.IdAnneeCourante;
        }

        private void WfrmGestionDesArticles_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aRT_ARTICLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cLA_CLASSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            if (BUSINESS.Utilities.GetOption(2))
            {
                try
                {
                    DAO.BDVendangeControleur controleur = new BDVendangeControleur();
                    DAO.WinBizControleur wbControleur = new WinBizControleur(BUSINESS.Utilities.IdAnneeCourante);
                    controleur.UpdateModeComptabilisations(BUSINESS.Utilities.IdAnneeCourante);
                    controleur.UpdateWinBizArticleIds(wbControleur.GetArticleIds(), BUSINESS.Utilities.IdAnneeCourante);
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    MessageBox.Show("Attention le dossier WinBIZ est indisponible.\n" +
                        "Les modes de comptabilisation ne sont pas synchronisés.", "Gestion des vendanges", MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
            }

            UpdateDonnees();

            _manager = new FormSettingsManagerArticle(this, aRT_IDTextBox, aRT_CODETextBox, toolAdd, toolUpdate, toolDelete,
                btnOK, btnCancel, aRT_ARTICLEDataGridView,
                aRT_ARTICLEBindingSource, tableAdapterManager, dSPesees, aRT_ARTICLETableAdapter,
                aRT_DESIGNATION_LONGUETextBox,
                toolGenerer, toolBtnGrouper, toolBtnDissocier, toolExportWinBIZ, cbAnnee, toolOk, toolCancel);
            _manager.AddControl(aRT_CODETextBox);
            _manager.AddControl(aRT_CODE_GROUPETextBox);
            _manager.AddControl(cOU_IDComboBox);
            _manager.AddControl(mOC_IDComboBox);
            _manager.AddControl(aRT_DESIGNATION_COURTETextBox);
            _manager.AddControl(aRT_DESIGNATION_LONGUETextBox);
            _manager.AddControl(aRT_LITRESTextBox);
            _manager.AddControl(txtLitreStock);
            toolAdd.Click += btnNew_Click;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            aNN_IDComboBox.Enabled = true;
            aNN_IDComboBox.SelectedValue = BUSINESS.Utilities.IdAnneeCourante;
            aNN_IDComboBox.Enabled = false;
            aRT_DESIGNATION_COURTETextBox.Text = string.Empty;
        }
    }
}