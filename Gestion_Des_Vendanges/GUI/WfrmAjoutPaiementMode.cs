﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiementMode : WfrmBase
    {
        #region Fields

        private ParametrePaiement _parametrePaiement;

        #endregion Fields

        #region Constructors

        public WfrmAjoutPaiementMode(ParametrePaiement parametrePaiement)
        {
            InitializeComponent();
            _parametrePaiement = parametrePaiement;
            lPrixFixe.Text = "Prix par " + ((Utilities.IsPrixKg) ? "Kg :" : "litre :");
        }

        private void WfrmAjoutPaiementMode_Load(object sender, EventArgs e)
        {
            AddTableAdapter();
            FillTableAdapter();

            if (_parametrePaiement.ModePaiement is ModePaiementPrixFixe)
            {
                optPrixFixe.Checked = true;
                txtPrixFixe.Text = ((ModePaiementPrixFixe)_parametrePaiement.ModePaiement).Prix.ToString();
            }
            else if (_parametrePaiement.ModePaiement is ModePaiementParCepageLieu)
            {
                optPrixCepLie.Checked = true;
                chkBonus.Checked = _parametrePaiement.ModePaiement.IsGestionBonusActived;
            }
            else if (_parametrePaiement.ModePaiement is ModePaiementParCepageLieuTablePrix)
            {
                optPrixCepLieTablePrix.Checked = true;
                chkBonus.Checked = _parametrePaiement.ModePaiement.IsGestionBonusActived;
            }
            else
            {
                optPrixCepage.Checked = true;
                InitComboBoxValue();

                if (_parametrePaiement.ModePaiement != null)
                    chkBonus.Checked = _parametrePaiement.ModePaiement.IsGestionBonusActived;
            }

            grbPaiementLitresOptions.Enabled = !Utilities.IsPrixKg;
        }

        #endregion Constructors

        #region Methods

        private void OptPrix_CheckedChanged(object sender, EventArgs e)
        {
            gbCalcul.Enabled = !optPrixFixe.Checked;
            gbPrixFixe.Enabled = optPrixFixe.Checked;
            gbCepage.Enabled = optPrixCepage.Checked;
        }

        private void ChkZoneTravail_CheckedChanged(object sender, EventArgs e)
        {
            ToggleZoneTravailButtons();
        }

        private void ToggleZoneTravailButtons()
        {
            optDefaultModeCompta.Enabled = chkZoneTravail.Checked;
            optSelectModeCompta.Enabled = chkZoneTravail.Checked;
            cbModeCompta.Enabled = optSelectModeCompta.Checked;

            if (optDefaultModeCompta.Checked == false && optSelectModeCompta.Checked == false)
                optDefaultModeCompta.Checked = true;
        }

        private void BtnSuivant_Click(object sender, EventArgs e)
        {
            if (SaveDonnees(false))
                DialogResult = DialogResult.OK;
        }

        private bool SaveDonnees(bool sansEchec)
        {
            bool ok = false;

            if (optPrixFixe.Checked)
            {
                ok = SetModePaiementPrixFixe(sansEchec, ok);
            }
            else if (optPrixCepage.Checked)
            {
                ok = SetModePaiementSpecifique(sansEchec, ok);
            }
            else if (optPrixCepLie.Checked)
            {
                ok = SetModePaiementCepageLieu(sansEchec, ok);
            }
            else if (optPrixCepLieTablePrix.Checked)
            {
                ok = SetModePaiementCepageLieuTablePrix(sansEchec, ok);
            }
            else
            {
                if (!sansEchec)
                    MessageBox.Show("Veuillez sélectionner un mode de paiement.",
                                    "Erreur sélection mode de paiement", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return ok;
        }

        #region Set Mode de Paiement

        private bool SetModePaiementCepageLieuTablePrix(bool sansEchec, bool ok)
        {
            try
            {
                _parametrePaiement.SetModePaiementCepLieTablePrix(pourcentage: numPourcentage.Value
                                                               , isGestionBonusActived: chkBonus.Checked
                                                               , isZoneDifficulteChecked: chkZoneTravail.Checked
                                                               , paiementlitreDeMoutRaisin: chkLitreMoutRaisin.Checked);
                ok = true;
            }
            catch (TablePrixNotFoundGVException ex)
            {
                if (!sansEchec)
                    TablePrixNotFound(ex);
            }

            return ok;
        }

        private bool SetModePaiementPrixFixe(bool sansEchec, bool ok)
        {
            try
            {
                decimal prixFixe = Math.Round(decimal.Parse(txtPrixFixe.Text), 3);
                _parametrePaiement.SetModePaiementPrixFixe(prix: prixFixe,
                                                           isZoneDifficulteChecked: chkZoneTravail.Checked,
                                                           PaiementlitreDeMoutRaisin: chkLitreMoutRaisin.Checked);
                ok = true;
            }
            catch
            {
                if (!sansEchec)
                    MessageBox.Show("Veuillez saisir un prix correct.",
                                    "Erreur prix", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return ok;
        }

        private bool SetModePaiementSpecifique(bool sansEchec, bool ok)
        {
            try
            {
                SetModePaiementSpecifique();
                ok = true;
            }
            catch (NullReferenceException)
            {
                if (!sansEchec)
                    MessageBox.Show("Veuillez sélectionner un lieu de production",
                                    "Erreur lieu de production", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (TablePrixNotFoundGVException ex)
            {
                if (!sansEchec)
                    TablePrixNotFound(ex);
            }

            return ok;
        }

        private bool SetModePaiementCepageLieu(bool sansEchec, bool ok)
        {
            try
            {
                _parametrePaiement.SetModePaiementCepLie(pourcentage: numPourcentage.Value,
                                                         isGestionBonusActived: chkBonus.Checked,
                                                         isZoneDifficulteChecked: chkZoneTravail.Checked,
                                                         paiementlitreDeMoutRaisin: chkLitreMoutRaisin.Checked);
                ok = true;
            }
            catch (TablePrixNotFoundGVException ex)
            {
                if (!sansEchec) TablePrixNotFound(ex);
            }

            return ok;
        }

        #endregion Set Mode de Paiement

        private void TablePrixNotFound(TablePrixNotFoundGVException ex)
        {
            DialogResult dialogResult = MessageBox.Show(ex.Message +
                                                        "\nVeuillez saisir cette table de prix ou changer de mode de paiement.",
                                                        "Erreur tables de prix",
                                                        MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            if (dialogResult == DialogResult.OK)
            {
                var ctrGestionBonus = CtrlGestionDesBonus.GetInstance(_parametrePaiement.AnnId);
                ctrGestionBonus.NewTable(ex.LieId, ex.CepId);
                WfrmHostCtrWfrm.ShowDialog(ctrGestionBonus, false);
            }
        }

        private void AccederAuxTablesDePrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowBonusTable();
        }

        private void ShowBonusTable()
        {
            var ctrGestionBonus = CtrlGestionDesBonus.GetInstance(_parametrePaiement.AnnId);
            WfrmHostCtrWfrm.ShowDialog(ctrGestionBonus, false);
            FillTableAdapter();
        }

        private void BtnAnnuler_Click(object sender, EventArgs e) => DialogResult = DialogResult.Cancel;

        private void BtnPrecedent_Click(object sender, EventArgs e)
        {
            SaveDonnees(true);
            DialogResult = DialogResult.Retry;
        }

        #endregion Methods
    }
}