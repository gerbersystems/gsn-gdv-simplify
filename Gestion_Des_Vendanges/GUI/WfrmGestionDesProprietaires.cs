﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des adresses des propriétaires
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmGestionDesProprietaires : WfrmSetting
    {
        private FormSettingsManagerProducteur _manager;

        #region Constructor

        private static WfrmGestionDesProprietaires _wfrm;

        public static WfrmGestionDesProprietaires Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                    _wfrm = new WfrmGestionDesProprietaires();

                return _wfrm;
            }
        }

        private WfrmGestionDesProprietaires()
        {
            InitializeComponent();

            btnImportWinBiz.Visible = BUSINESS.Utilities.GetOption(1);
        }

        private void WfrmGestionDesProprietaires_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(mOD_MODE_TVATableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(adresseCompletTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aDR_ADRESSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_PROPRIETAIRETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            fillTableAdapter();

            _manager = new FormSettingsManagerProducteur(this, pRO_IDTextBox, pRO_TITRETextBox, btnAddProprio,
                                                         btnUpdate, btnDelete, pRO_PROPRIETAIREDataGridView,
                                                         pRO_PROPRIETAIREBindingSource, tableAdapterManager, dSPesees,
                                                         chkProducteur, aDR_IDComboBox, mOD_IDComboBox, btnImportWinBiz, adresseCompletTableAdapter);

            _manager.AddControl(pRO_TITRETextBox);
            _manager.AddControl(pRO_NOMTextBox);
            _manager.AddControl(pRO_PRENOMTextBox);
            _manager.AddControl(pRO_SCTETextBox);
            _manager.AddControl(aDR_IDComboBox);
            _manager.AddControl(chkProprietaire);
            _manager.AddControl(chkProducteur);
            _manager.AddControl(pRO_NUMERO_TVATextBox);
            _manager.AddControl(mOD_IDComboBox);
            _manager.AddControl(ErpIdTextBox);
        }

        #endregion Constructor

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int proId = (int)pRO_PROPRIETAIREDataGridView.CurrentRow.Cells["PRO_ID"].Value;
                    resultat = (Convert.ToInt32(pRO_PROPRIETAIRETableAdapter.NbRef(proId)) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private void fillTableAdapter()
        {
            mOD_MODE_TVATableAdapter.FillWithArticle(dSPesees.MOD_MODE_TVA);
            adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
            aDR_ADRESSETableAdapter.Fill(dSPesees.ADR_ADRESSE);
            pRO_PROPRIETAIRETableAdapter.Fill(dSPesees.PRO_PROPRIETAIRE);
        }

        private void aDR_IDComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                default:
                    break;

                case (char)13:
                    SendKeys.Send("{TAB}");
                    break;
            }
        }

        private void AdressestoolStripMenuItem_Click(object sender, EventArgs e)
        {
            object item = aDR_IDComboBox.SelectedValue;
            WfrmGestionDesAdresses.Wfrm.WfrmShowDialog();
            this.adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
            this.aDR_ADRESSETableAdapter.Fill(dSPesees.ADR_ADRESSE);
            try
            {
                aDR_IDComboBox.SelectedValue = item;
            }
            catch { }
        }

        private void pRO_PROPRIETAIREDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                pRO_PROPRIETAIREBindingSource.RemoveSort();
                pRO_PROPRIETAIRETableAdapter.FillOrderAdr(dSPesees.PRO_PROPRIETAIRE);
                foreach (DataGridViewColumn column in pRO_PROPRIETAIREDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                pRO_PROPRIETAIREDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void btnImportWinBiz_Click(object sender, EventArgs e)
        {
            if (BUSINESS.Utilities.GetOption(2) && BUSINESS.Utilities.GetOption(1))
            {
                bool fin = false;
                while (!fin)
                {
                    try
                    {
                        if (new WfrmWinBizImportAdresse().ShowDialog() == DialogResult.OK)
                        {
                            pRO_PROPRIETAIRETableAdapter.Fill(dSPesees.PRO_PROPRIETAIRE);
                            fillTableAdapter();
                        }
                        fin = true;
                    }
                    catch (BUSINESS.GVException)
                    {
                        DialogResult msgResult = MessageBox.Show("Le dossier WinBIZ n'est pas défini ou incorrect.", "Dossier WinBIZ", MessageBoxButtons.OKCancel);
                        if (msgResult == DialogResult.OK)
                        {
                            if (new WfrmWinBizFolder().ShowDialog() == DialogResult.Cancel)
                                fin = true;
                        }
                        else
                        {
                            fin = true;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Cette option est uniquement disponible avec la version étendue\n"
                    + "avec l'option WinBIZ.", "Licence insuffisante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}