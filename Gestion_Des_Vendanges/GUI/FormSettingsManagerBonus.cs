﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement du formulaire "Bonus"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class FormSettingsManagerBonus : FormSettingsManagerPesees
    {
        private int _annId
        {
            get { return Utilities.IdAnneeCourante; }
            set { Utilities.IdAnneeCourante = value; }
        }

        private ListBox _cbFiltre;
        private ComboBox _cbLieuProductionId;
        private ComboBox _cbCepageId;
        private ComboBox _cbAnnee;
        private TextBox _txtAnnId;
        private VAL_VALEUR_CEPAGETableAdapter _valTableAdapter;
        private DataGridView _bonusDataGridView;
        private BindingSource _bonusBindingSource;
        private DSPesees _dsPesees;
        private BON_BONUSTableAdapter _bonusTableAdapter;
        private DEG_DEGRE_OECHSLE_BRIXTableAdapter _degTableAdapter;
        private ViewBonusMaxTableAdapter _viewBonusMaxTableAdapter;
        private DSPesees _dsPeseesFiltre;
        private DataGridView _valDataGridView;
        private bool _isBonusDataGridViewEditMode;
        private ToolStripButton _btnCopyLieuDeProduction;
        private ToolStripButton _btnCopyCepage;
        private bool _isAddOneMode = false;
        private Label _lprix100;

        private void Init(DataGridView dataGridView,
                          DSPesees dsPesees,
                          ComboBox cbCepageId,
                          ComboBox cbLieuProductionId,
                          TextBox txtAnnId,
                          int annId,
                          VAL_VALEUR_CEPAGETableAdapter valTableAdapter,
                          DataGridView bonusDataGridView,
                          BindingSource bonusBindingSource,
                          BON_BONUSTableAdapter bonusTableAdapter,
                          ViewBonusMaxTableAdapter viewBonusMaxTableAdapter,
                          DSPesees dsPeseesFiltre,
                          ComboBox cbAnnee,
                          ToolStripButton btnCopyLieuProduction,
                          ToolStripButton btnCopyCepage,
                          Label lprix100)
        {
            _cbCepageId = cbCepageId;
            _cbLieuProductionId = cbLieuProductionId;
            _annId = annId;
            _valTableAdapter = valTableAdapter;
            _txtAnnId = txtAnnId;
            _bonusDataGridView = bonusDataGridView;
            _bonusDataGridView.CellBeginEdit += BonusDataGridViewBeginEdit;
            _bonusDataGridView.DataError += BON_BONUSDataGridView_DataError;
            _bonusDataGridView.CellEndEdit += BON_BONUSDataGridView_CellEndEdit;
            _bonusDataGridView.RowValidated += BONUSRowValidated;
            _bonusDataGridView.CellMouseDown += BON_BONUSDataGridView_CellMouseDown;
            _bonusBindingSource = bonusBindingSource;
            _dsPesees = dsPesees;
            _bonusTableAdapter = bonusTableAdapter;
            _viewBonusMaxTableAdapter = viewBonusMaxTableAdapter;
            _dsPeseesFiltre = dsPeseesFiltre;
            _valDataGridView = dataGridView;
            _cbAnnee = cbAnnee;
            _cbAnnee.SelectedIndexChanged += CbAnnee_SelectedIndexChanged;
            _degTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<DEG_DEGRE_OECHSLE_BRIXTableAdapter>();
            UpdateDataGrid();
            _bonusDataGridView.EnabledChanged += BonusDataGridEnabledChanged;
            _btnCopyLieuDeProduction = btnCopyLieuProduction;
            _btnCopyLieuDeProduction.Click += BtnCopyLieuProductionClick;
            _btnCopyCepage = btnCopyCepage;
            _btnCopyCepage.Click += BtnCopyCepageClick;
            _lprix100 = lprix100;
            UpdateLibellePrix100();
        }

        private void BonusDataGridEnabledChanged(object sender, EventArgs e)
        {
            if (_bonusDataGridView.Enabled) return;

            try
            {
                _bonusDataGridView.SelectedCells[0].Selected = false;
            }
            catch { }
        }

        private void SaveBonusDataGrid()
        {
            _bonusBindingSource.EndEdit();
            try
            {
                _bonusTableAdapter.Update(_dsPesees);
            }
            catch (SqlException) { }
        }

        protected override void BtnDelete_Click(object sender, EventArgs e)
        {
            base.BtnDelete_Click(sender, e);
            if (IsDeleted) UpdateDataGrid();
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    int valId = (int)_bonusDataGridView.CurrentRow.Cells["VAL_ID"].Value;
                    if (_bonusTableAdapter.GetNbOeIdendique(valId) > 0) throw new DegreOeIdentiqueGVException();
                }
                catch (NullReferenceException)
                {
                    //si aucun bonus n'a été saisi
                }
                int lieId = (int)_cbLieuProductionId.SelectedValue;
                int cepId = (int)_cbCepageId.SelectedValue;
                if (!IsNew || _valTableAdapter.GetNbByCepLieAnn(cepId, lieId, _annId) == 0)
                {
                    SaveBonusDataGrid();
                    base.BtnOk_Click(sender, e);
                    UpdateFilter(cepId, lieId);
                }
                else
                {
                    MessageBox.Show("Les bonus pour ce cépage et ce lieu de production existent déjà!\n" +
                                    "Veuillez choisir un autre cépage ou lieu de production.",
                                    "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                if (_isAddOneMode)
                {
                    Form.GetActiveForm().DialogResult = DialogResult.OK;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Veuillez sélectionner un cépage et un lieu de production.",
                                "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (DegreOeIdentiqueGVException)
            {
                MessageBox.Show("Erreur, les degrés Oe de certains bonus sont identiques.\n" +
                                "Veuillez corriger les valeurs.",
                                "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            _cbFiltre.Enabled = true;
            _bonusDataGridView.AllowUserToAddRows = false;
        }

        protected override void BtnCancel_Click(object sender, EventArgs e)
        {
            _bonusDataGridView.AllowUserToAddRows = false;
            base.BtnCancel_Click(sender, e);
            _cbFiltre.Enabled = true;
            if (_isAddOneMode) Form.GetActiveForm().DialogResult = DialogResult.Cancel;
        }

        private void BON_BONUSDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        /// <summary>
        /// Convertis le taux saisie en Oechsle ou en Brix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BON_BONUSDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // SI le taux saisie est en Oechsle, alors insert le taux convertis dans la colonne en Brix
                if (e.ColumnIndex == _bonusDataGridView.Columns["BON_OE"].Index)
                {
                    decimal oe = Convert.ToDecimal(_bonusDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    _bonusDataGridView.Rows[e.RowIndex].Cells["BON_BRIX"].Value = (oe == 0) ? 0 : _degTableAdapter.GetBrixByOechsle(oe);

                    ValideOe(e.ColumnIndex, e.RowIndex);
                }
                // SINON SI le taux saisie est en Brix, alors insert le taux convertis dans la colonne en Oechsle
                else if (e.ColumnIndex == _bonusDataGridView.Columns["BON_BRIX"].Index)
                {
                    decimal brix = Convert.ToDecimal(_bonusDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
                    _bonusDataGridView.Rows[e.RowIndex].Cells["BON_OE"].Value = (brix == 0) ? 0 : _degTableAdapter.GetOechsleByBrix(brix);

                    ValideOe(e.ColumnIndex, e.RowIndex);
                }
            }
            catch (InvalidCastException) { }
            catch (SqlException) { }
        }

        private void BONUSRowValidated(object sender, EventArgs e)
        {
            if (_isBonusDataGridViewEditMode)
            {
                _isBonusDataGridViewEditMode = false;
                SaveBonusDataGrid();
                _viewBonusMaxTableAdapter.Fill(_dsPesees.ViewBonusMax);
                _bonusDataGridView.Refresh();
                _bonusDataGridView.Sort(_bonusDataGridView.Columns["BON_OE"], ListSortDirection.Ascending);
            }
        }

        /// <summary>
        /// Teste si la valeur d'Oechsle saisie est existante dans la table de prix
        /// </summary>
        /// <param name="indexColumn"></param>
        /// <param name="indexRow"></param>
        /// <returns></returns>
        private bool ValideOe(int indexColumn, int indexRow)
        {
            bool res = true;
            try
            {
                int bonId = (int)_bonusDataGridView.Rows[indexRow].Cells["BON_ID"].Value;
                double bonOe = (double)_bonusDataGridView.Rows[indexRow].Cells["BON_OE"].Value;
                int valId = (int)_bonusDataGridView.Rows[indexRow].Cells["VAL_ID"].Value;
                int nbOeIdentique = (int)_bonusTableAdapter.GetNbOeIdentique(valId, bonId, bonOe);
                if (nbOeIdentique > 0)
                {
                    MessageBox.Show("Erreur, un bonus pour un degré Oe identique a déjà été saisi.\n" +
                                    "Veuillez en saisir un autre.",
                                    "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    _bonusDataGridView.Rows[indexRow].Cells[indexColumn].Value = DBNull.Value;
                    res = false;
                }
            }
            catch (InvalidCastException) { }
            catch (NullReferenceException) { }
            return res;
        }

        private void BON_BONUSDataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                _bonusDataGridView.CurrentCell = _bonusDataGridView.Rows[e.RowIndex].Cells[0];
        }

        public void NewTablePrix(int lieId, int cepId)
        {
            BtnNew_Click(null, null);
            _cbLieuProductionId.SelectedValue = lieId;
            _cbCepageId.SelectedValue = cepId;
            _cbLieuProductionId.Enabled = false;
            _cbCepageId.Enabled = false;
            _isAddOneMode = true;
        }

        private void CbAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_cbAnnee.SelectedValue != null && Convert.ToInt32(_cbAnnee.SelectedValue) != Utilities.IdAnneeCourante)
            {
                Utilities.IdAnneeCourante = Convert.ToInt32(_cbAnnee.SelectedValue);
                UpdateDataGrid();
                UpdateLibellePrix100();
            }
        }

        private void UpdateLibellePrix100()
        {
            _lprix100.Text = string.Concat("Valeur 100% (CHF/", Utilities.IsPrixKg ? "Kg)" : "Litre)");
        }
    }
}