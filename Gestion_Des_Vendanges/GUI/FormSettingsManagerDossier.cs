﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class FormSettingsManagerDossier : FormSettingsManagerMaster
    {
        private TextBox _txtNom;
        private TextBox _txtDescription;
        private Button _btnDelete;
        private BUSINESS.Installeur _installeur = new BUSINESS.Installeur();

        public FormSettingsManagerDossier(ICtrlSetting form, TextBox idTextBox, Control focusControl,
            Button btnNew, Button btnUpdate, Button btnDelete, DataGridView dataGridView,
            BindingSource bindingSource, DAO.DSMasterTableAdapters.TableAdapterManager tableAdapterManager, DSMaster dataSet, TextBox txtNom, TextBox txtDescription)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete,
                dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
            _txtDescription = txtDescription;
            _txtNom = txtNom;
            _btnDelete = btnDelete;
            _btnDelete.Visible = false;
        }

        protected override void ChangeEditMode(bool editMode)
        {
            base.ChangeEditMode(editMode);
            if (_btnDelete != null)
                _btnDelete.Visible = editMode;
        }

        protected override void DatagridSelectionChanged(object sender, EventArgs e)
        {
            base.DatagridSelectionChanged(sender, e);
            _btnDelete.Visible = false;
        }

        protected override void BtnUpdate_Click(object sender, EventArgs e)
        {
            base.BtnUpdate_Click(sender, e);
            _txtNom.ReadOnly = true;
        }

        protected override void BtnNew_Click(object sender, EventArgs e)
        {
            base.BtnNew_Click(sender, e);
            _txtNom.ReadOnly = false;
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_txtDescription.Text) || string.IsNullOrWhiteSpace(_txtNom.Text))
            {
                MessageBox.Show("Veuillez saisir un nom de base de données et une description.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (IsNew)
                {
                    try
                    {
                        _installeur.AjouterBd(_txtNom.Text, BUSINESS.Utilities.LicId);
                        base.BtnOk_Click(sender, e);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Une erreur s'est produite lors de la création de la base de données.\n" + ex.Message, "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    base.BtnOk_Click(sender, e);
                }
            }
        }
    }
}
