﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de sélection de la base de données
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmSelection : Form
    {
        public object Selection
        {
            get { return cbListe.SelectedItem; }
        }

        public object[] Choix
        {
            set
            {
                cbListe.Items.Clear();
                if (value.Length > 0)
                {
                    cbListe.Items.AddRange(value);
                    cbListe.SelectedItem = value[0];
                }
            }
        }

        public WfrmSelection(string titre, string label)
        {
            InitializeComponent();
            Text = titre;
            label1.Text = label;
        }

        public WfrmSelection(object[] choix, string titre, string label)
        {
            InitializeComponent();
            Text = titre;
            label1.Text = label;
            Choix = choix;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (cbListe.SelectedItem != null)
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show("Veuillez sélectionner un dossier.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void WfrmSelectionBD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOK.PerformClick();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}