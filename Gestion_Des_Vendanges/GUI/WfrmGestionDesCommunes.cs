﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des communes
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesCommunes : WfrmSetting
    {
        private static WfrmGestionDesCommunes _wfrm;
        private FormSettingsManagerPesees _manager;

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int comID = (int)cOM_COMMUNEDataGridView.CurrentRow.Cells["COM_ID"].Value;
                    resultat = (int)cOM_COMMUNETableAdapter.NbRef(comID) == 0;
                }
                catch
                {
                    resultat = true;
                }

                return resultat;
            }
        }

        public static WfrmGestionDesCommunes Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesCommunes();
                }
                return _wfrm;
            }
        }

        private WfrmGestionDesCommunes()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesCommunes_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            InitSpecificTableAdapters();

            cOM_COMMUNETableAdapter.Fill(dSPesees.COM_COMMUNE);
            _manager = new FormSettingsManagerPesees(this, cOM_IDTextBox, cOM_NOMTextBox, btnAddCommune, btnSaveCommune, btnSupprimer, cOM_COMMUNEDataGridView, cOM_COMMUNEBindingSource, tableAdapterManager, dSPesees);
            _manager.AddControl(cOM_NOMTextBox);
            _manager.AddControl(cOM_NUMEROTextBox);
            AddSpecificControls();
        }
    }
}