﻿using GSN.GDV.Data.Extensions;
namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSaisieAcquit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label aNN_IDLabel;
            System.Windows.Forms.Label pRO_IDLabel;
            System.Windows.Forms.Label aCQ_DATELabel;
            System.Windows.Forms.Label aCQ_SURFACELabel;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            this.cLA_IDLabel = new System.Windows.Forms.Label();
            this.cOU_IDLabel = new System.Windows.Forms.Label();
            this.cOM_IDLabel = new System.Windows.Forms.Label();
            this.aCQ_NUMEROLabel = new System.Windows.Forms.Label();
            this.aCQ_LITRESLabel = new System.Windows.Forms.Label();
            this.rEG_IDLabel = new System.Windows.Forms.Label();
            this.aCQ_NUM_COMPLLabel = new System.Windows.Forms.Label();
            this.dIS_IDLabel = new System.Windows.Forms.Label();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aCQ_ACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aCQ_IDTextBox = new System.Windows.Forms.TextBox();
            this.CouleurscontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CouleursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_IDComboBox = new System.Windows.Forms.ComboBox();
            this.CommunesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxCommunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProprietairesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PropriétairesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCQ_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.aCQ_DATEDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.aCQ_SURFACETextBox = new System.Windows.Forms.TextBox();
            this.aCQ_LITRESTextBox = new System.Windows.Forms.TextBox();
            this.lTitre = new System.Windows.Forms.Label();
            this.cmdValider = new System.Windows.Forms.Button();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.CategoriescontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxCatégoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLACLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.aNN_IDTextBox = new System.Windows.Forms.TextBox();
            this.txtAnnee = new System.Windows.Forms.TextBox();
            this.pAR_PARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.pAR_PARCELLEDataGridView = new System.Windows.Forms.DataGridView();
            this.CEP_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CEP_ACQ_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AUT_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LIE_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PAR_SURFACE_CEPAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_SURFACE_COMPTEE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DG_PAR_QUOTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_LITRES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kilos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZOT_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.zOTZONETRAVAILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PAR_SECTEUR_VISITE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pAR_SECTEUR_VISITEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PAR_MODE_CULTURE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnDeleteParcelle = new System.Windows.Forms.Button();
            this.btnModifParcelle = new System.Windows.Forms.Button();
            this.btnAddParcelle = new System.Windows.Forms.Button();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.lieuProductionMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.rEG_IDComboBox = new System.Windows.Forms.ComboBox();
            this.contextMenuRegion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolRegion = new System.Windows.Forms.ToolStripMenuItem();
            this.rEGREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rESRESPONSABLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rES_RESPONSABLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_RESPONSABLETableAdapter();
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.aCQ_NUMERO_COMPLTextBox = new System.Windows.Forms.TextBox();
            this.cLA_IDComboBox = new System.Windows.Forms.ComboBox();
            this.pRO_IDComboBox = new System.Windows.Forms.ComboBox();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOU_IDComboBox = new System.Windows.Forms.ComboBox();
            this.districtComboBox = new System.Windows.Forms.ComboBox();
            this.districtcontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxDistrictsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dISDISTRICTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dIS_DISTRICTTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter();
            this.pRO_ID_VENDANGEComboBox = new System.Windows.Forms.ComboBox();
            this.pRONOMCOMPLETBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dsPesees1 = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.prO_NOMCOMPLETTableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.cbRecu = new System.Windows.Forms.CheckBox();
            this.aCQ_RECEPTIONDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.zOT_ZONE_TRAVAILTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter();
            this.pAR_SECTEUR_VISITETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_SECTEUR_VISITETableAdapter();
            this.pCO_PAR_COMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pCO_PAR_COMTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PCO_PAR_COMTableAdapter();
            this.plI_PAR_LIETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PLI_PAR_LIETableAdapter();
            aNN_IDLabel = new System.Windows.Forms.Label();
            pRO_IDLabel = new System.Windows.Forms.Label();
            aCQ_DATELabel = new System.Windows.Forms.Label();
            aCQ_SURFACELabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).BeginInit();
            this.CouleurscontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            this.CommunesContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            this.ProprietairesContextMenuStrip.SuspendLayout();
            this.CategoriescontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_SECTEUR_VISITEBindingSource)).BeginInit();
            this.lieuProductionMenuStrip.SuspendLayout();
            this.contextMenuRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rESRESPONSABLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            this.districtcontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dISDISTRICTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCO_PAR_COMBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // aNN_IDLabel
            // 
            aNN_IDLabel.AutoSize = true;
            aNN_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            aNN_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aNN_IDLabel.Location = new System.Drawing.Point(13, 50);
            aNN_IDLabel.Name = "aNN_IDLabel";
            aNN_IDLabel.Size = new System.Drawing.Size(69, 17);
            aNN_IDLabel.TabIndex = 7;
            aNN_IDLabel.Text = "Année : ";
            // 
            // pRO_IDLabel
            // 
            pRO_IDLabel.AutoSize = true;
            pRO_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            pRO_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_IDLabel.Location = new System.Drawing.Point(473, 51);
            pRO_IDLabel.Name = "pRO_IDLabel";
            pRO_IDLabel.Size = new System.Drawing.Size(106, 17);
            pRO_IDLabel.TabIndex = 11;
            pRO_IDLabel.Text = "Propriétaire :";
            // 
            // aCQ_DATELabel
            // 
            aCQ_DATELabel.AutoSize = true;
            aCQ_DATELabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            aCQ_DATELabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aCQ_DATELabel.Location = new System.Drawing.Point(13, 207);
            aCQ_DATELabel.Name = "aCQ_DATELabel";
            aCQ_DATELabel.Size = new System.Drawing.Size(145, 17);
            aCQ_DATELabel.TabIndex = 15;
            aCQ_DATELabel.Text = "Date de l\'acquit : ";
            // 
            // aCQ_SURFACELabel
            // 
            aCQ_SURFACELabel.AutoSize = true;
            aCQ_SURFACELabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            aCQ_SURFACELabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aCQ_SURFACELabel.Location = new System.Drawing.Point(473, 183);
            aCQ_SURFACELabel.Name = "aCQ_SURFACELabel";
            aCQ_SURFACELabel.Size = new System.Drawing.Size(217, 17);
            aCQ_SURFACELabel.TabIndex = 17;
            aCQ_SURFACELabel.Text = "Surface totale de l\'acquit : ";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label5.Location = new System.Drawing.Point(473, 78);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(100, 17);
            label5.TabIndex = 171;
            label5.Text = "Producteur :";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label6.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label6.Location = new System.Drawing.Point(473, 158);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(179, 17);
            label6.TabIndex = 173;
            label6.Text = "Réception de l\'acquit :";
            // 
            // cLA_IDLabel
            // 
            this.cLA_IDLabel.AutoSize = true;
            this.cLA_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cLA_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cLA_IDLabel.Location = new System.Drawing.Point(12, 104);
            this.cLA_IDLabel.Name = "cLA_IDLabel";
            this.cLA_IDLabel.Size = new System.Drawing.Size(163, 17);
            this.cLA_IDLabel.TabIndex = 3;
            this.cLA_IDLabel.Text = "Classe (Catégorie) : ";
            // 
            // cOU_IDLabel
            // 
            this.cOU_IDLabel.AutoSize = true;
            this.cOU_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOU_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cOU_IDLabel.Location = new System.Drawing.Point(12, 78);
            this.cOU_IDLabel.Name = "cOU_IDLabel";
            this.cOU_IDLabel.Size = new System.Drawing.Size(80, 17);
            this.cOU_IDLabel.TabIndex = 5;
            this.cOU_IDLabel.Text = "Couleur : ";
            // 
            // cOM_IDLabel
            // 
            this.cOM_IDLabel.AutoSize = true;
            this.cOM_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOM_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cOM_IDLabel.Location = new System.Drawing.Point(13, 183);
            this.cOM_IDLabel.Name = "cOM_IDLabel";
            this.cOM_IDLabel.Size = new System.Drawing.Size(98, 17);
            this.cOM_IDLabel.TabIndex = 9;
            this.cOM_IDLabel.Text = "Commune : ";
            // 
            // aCQ_NUMEROLabel
            // 
            this.aCQ_NUMEROLabel.AutoSize = true;
            this.aCQ_NUMEROLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_NUMEROLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aCQ_NUMEROLabel.Location = new System.Drawing.Point(473, 105);
            this.aCQ_NUMEROLabel.Name = "aCQ_NUMEROLabel";
            this.aCQ_NUMEROLabel.Size = new System.Drawing.Size(161, 17);
            this.aCQ_NUMEROLabel.TabIndex = 13;
            this.aCQ_NUMEROLabel.Text = "Numéro de l\'acquit :";
            // 
            // aCQ_LITRESLabel
            // 
            this.aCQ_LITRESLabel.AutoSize = true;
            this.aCQ_LITRESLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_LITRESLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aCQ_LITRESLabel.Location = new System.Drawing.Point(473, 209);
            this.aCQ_LITRESLabel.Name = "aCQ_LITRESLabel";
            this.aCQ_LITRESLabel.Size = new System.Drawing.Size(121, 17);
            this.aCQ_LITRESLabel.TabIndex = 19;
            this.aCQ_LITRESLabel.Text = "Litres totaux : ";
            // 
            // rEG_IDLabel
            // 
            this.rEG_IDLabel.AutoSize = true;
            this.rEG_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rEG_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.rEG_IDLabel.Location = new System.Drawing.Point(13, 129);
            this.rEG_IDLabel.Name = "rEG_IDLabel";
            this.rEG_IDLabel.Size = new System.Drawing.Size(69, 17);
            this.rEG_IDLabel.TabIndex = 164;
            this.rEG_IDLabel.Text = "Région :";
            // 
            // aCQ_NUM_COMPLLabel
            // 
            this.aCQ_NUM_COMPLLabel.AutoSize = true;
            this.aCQ_NUM_COMPLLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_NUM_COMPLLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aCQ_NUM_COMPLLabel.Location = new System.Drawing.Point(473, 130);
            this.aCQ_NUM_COMPLLabel.Name = "aCQ_NUM_COMPLLabel";
            this.aCQ_NUM_COMPLLabel.Size = new System.Drawing.Size(324, 17);
            this.aCQ_NUM_COMPLLabel.TabIndex = 167;
            this.aCQ_NUM_COMPLLabel.Text = "Numéro complémentaire (acquit partiel) :";
            // 
            // dIS_IDLabel
            // 
            this.dIS_IDLabel.AutoSize = true;
            this.dIS_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dIS_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.dIS_IDLabel.Location = new System.Drawing.Point(13, 156);
            this.dIS_IDLabel.Name = "dIS_IDLabel";
            this.dIS_IDLabel.Size = new System.Drawing.Size(79, 17);
            this.dIS_IDLabel.TabIndex = 169;
            this.dIS_IDLabel.Text = "District : ";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aCQ_ACQUITBindingSource
            // 
            this.aCQ_ACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQ_ACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aCQ_IDTextBox
            // 
            this.aCQ_IDTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.aCQ_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ACQ_ID", true));
            this.aCQ_IDTextBox.Location = new System.Drawing.Point(215, 9);
            this.aCQ_IDTextBox.Name = "aCQ_IDTextBox";
            this.aCQ_IDTextBox.Size = new System.Drawing.Size(138, 22);
            this.aCQ_IDTextBox.TabIndex = 2;
            this.aCQ_IDTextBox.TabStop = false;
            this.aCQ_IDTextBox.Text = "idAcquit";
            // 
            // CouleurscontextMenuStrip
            // 
            this.CouleurscontextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CouleurscontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CouleursToolStripMenuItem});
            this.CouleurscontextMenuStrip.Name = "CouleurscontextMenuStrip";
            this.CouleurscontextMenuStrip.Size = new System.Drawing.Size(232, 28);
            // 
            // CouleursToolStripMenuItem
            // 
            this.CouleursToolStripMenuItem.Name = "CouleursToolStripMenuItem";
            this.CouleursToolStripMenuItem.Size = new System.Drawing.Size(231, 24);
            this.CouleursToolStripMenuItem.Text = "Accéder aux couleurs ...";
            this.CouleursToolStripMenuItem.Click += new System.EventHandler(this.CouleursToolStripMenuItem_Click);
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_IDComboBox
            // 
            this.cOM_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cOM_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cOM_IDComboBox.ContextMenuStrip = this.CommunesContextMenuStrip;
            this.cOM_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOM_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "COM_ID", true));
            this.cOM_IDComboBox.DataSource = this.cOMCOMMUNEBindingSource;
            this.cOM_IDComboBox.DisplayMember = "COM_NOM";
            this.cOM_IDComboBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cOM_IDComboBox.FormattingEnabled = true;
            this.cOM_IDComboBox.Location = new System.Drawing.Point(178, 178);
            this.cOM_IDComboBox.Name = "cOM_IDComboBox";
            this.cOM_IDComboBox.Size = new System.Drawing.Size(201, 24);
            this.cOM_IDComboBox.TabIndex = 4;
            this.cOM_IDComboBox.ValueMember = "COM_ID";
            // 
            // CommunesContextMenuStrip
            // 
            this.CommunesContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CommunesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxCommunesToolStripMenuItem});
            this.CommunesContextMenuStrip.Name = "CommunesContextMenuStrip";
            this.CommunesContextMenuStrip.Size = new System.Drawing.Size(249, 28);
            // 
            // accéderAuxCommunesToolStripMenuItem
            // 
            this.accéderAuxCommunesToolStripMenuItem.Name = "accéderAuxCommunesToolStripMenuItem";
            this.accéderAuxCommunesToolStripMenuItem.Size = new System.Drawing.Size(248, 24);
            this.accéderAuxCommunesToolStripMenuItem.Text = "Accéder aux communes ...";
            this.accéderAuxCommunesToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxCommunesToolStripMenuItem_Click);
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // ProprietairesContextMenuStrip
            // 
            this.ProprietairesContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ProprietairesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PropriétairesToolStripMenuItem});
            this.ProprietairesContextMenuStrip.Name = "ProprietairesContextMenuStrip";
            this.ProprietairesContextMenuStrip.Size = new System.Drawing.Size(351, 28);
            // 
            // PropriétairesToolStripMenuItem
            // 
            this.PropriétairesToolStripMenuItem.Name = "PropriétairesToolStripMenuItem";
            this.PropriétairesToolStripMenuItem.Size = new System.Drawing.Size(350, 24);
            this.PropriétairesToolStripMenuItem.Text = "Accéder aux propriétaires / producteurs...";
            this.PropriétairesToolStripMenuItem.Click += new System.EventHandler(this.ProprietairesToolStripMenuItem_Click);
            // 
            // aCQ_NUMEROTextBox
            // 
            this.aCQ_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ACQ_NUMERO", true));
            this.aCQ_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_NUMEROTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aCQ_NUMEROTextBox.Location = new System.Drawing.Point(776, 104);
            this.aCQ_NUMEROTextBox.Name = "aCQ_NUMEROTextBox";
            this.aCQ_NUMEROTextBox.Size = new System.Drawing.Size(201, 22);
            this.aCQ_NUMEROTextBox.TabIndex = 8;
            // 
            // aCQ_DATEDateTimePicker
            // 
            this.aCQ_DATEDateTimePicker.CustomFormat = "dd.MM.yyyy";
            this.aCQ_DATEDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.aCQ_ACQUITBindingSource, "ACQ_DATE", true));
            this.aCQ_DATEDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_DATEDateTimePicker.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aCQ_DATEDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.aCQ_DATEDateTimePicker.Location = new System.Drawing.Point(178, 206);
            this.aCQ_DATEDateTimePicker.Name = "aCQ_DATEDateTimePicker";
            this.aCQ_DATEDateTimePicker.Size = new System.Drawing.Size(201, 22);
            this.aCQ_DATEDateTimePicker.TabIndex = 5;
            // 
            // aCQ_SURFACETextBox
            // 
            this.aCQ_SURFACETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ACQ_SURFACE", true));
            this.aCQ_SURFACETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_SURFACETextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aCQ_SURFACETextBox.Location = new System.Drawing.Point(776, 183);
            this.aCQ_SURFACETextBox.Name = "aCQ_SURFACETextBox";
            this.aCQ_SURFACETextBox.ReadOnly = true;
            this.aCQ_SURFACETextBox.Size = new System.Drawing.Size(201, 22);
            this.aCQ_SURFACETextBox.TabIndex = 12;
            this.aCQ_SURFACETextBox.TabStop = false;
            // 
            // aCQ_LITRESTextBox
            // 
            this.aCQ_LITRESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ACQ_LITRES", true));
            this.aCQ_LITRESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_LITRESTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aCQ_LITRESTextBox.Location = new System.Drawing.Point(776, 209);
            this.aCQ_LITRESTextBox.Name = "aCQ_LITRESTextBox";
            this.aCQ_LITRESTextBox.ReadOnly = true;
            this.aCQ_LITRESTextBox.Size = new System.Drawing.Size(201, 22);
            this.aCQ_LITRESTextBox.TabIndex = 13;
            this.aCQ_LITRESTextBox.TabStop = false;
            // 
            // lTitre
            // 
            this.lTitre.AutoSize = true;
            this.lTitre.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lTitre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lTitre.Location = new System.Drawing.Point(12, 9);
            this.lTitre.Name = "lTitre";
            this.lTitre.Size = new System.Drawing.Size(197, 23);
            this.lTitre.TabIndex = 23;
            this.lTitre.Text = "Saisie d\'un acquit";
            // 
            // cmdValider
            // 
            this.cmdValider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdValider.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdValider.Location = new System.Drawing.Point(983, 592);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(104, 51);
            this.cmdValider.TabIndex = 15;
            this.cmdValider.Text = "Valider l\'acquit";
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Click += new System.EventHandler(this.CmdAjouter_Click);
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // CategoriescontextMenuStrip
            // 
            this.CategoriescontextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CategoriescontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxCatégoriesToolStripMenuItem});
            this.CategoriescontextMenuStrip.Name = "CategoriescontextMenuStrip";
            this.CategoriescontextMenuStrip.Size = new System.Drawing.Size(246, 28);
            // 
            // accéderAuxCatégoriesToolStripMenuItem
            // 
            this.accéderAuxCatégoriesToolStripMenuItem.Name = "accéderAuxCatégoriesToolStripMenuItem";
            this.accéderAuxCatégoriesToolStripMenuItem.Size = new System.Drawing.Size(245, 24);
            this.accéderAuxCatégoriesToolStripMenuItem.Text = "Accéder aux catégories ...";
            this.accéderAuxCatégoriesToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxCategoriesToolStripMenuItem_Click);
            // 
            // cLACLASSEBindingSource
            // 
            this.cLACLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLACLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // aNN_IDTextBox
            // 
            this.aNN_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ANN_ID", true));
            this.aNN_IDTextBox.Enabled = false;
            this.aNN_IDTextBox.Location = new System.Drawing.Point(359, 9);
            this.aNN_IDTextBox.Name = "aNN_IDTextBox";
            this.aNN_IDTextBox.Size = new System.Drawing.Size(100, 22);
            this.aNN_IDTextBox.TabIndex = 28;
            this.aNN_IDTextBox.TabStop = false;
            this.aNN_IDTextBox.Text = "idAnnee";
            // 
            // txtAnnee
            // 
            this.txtAnnee.Location = new System.Drawing.Point(178, 46);
            this.txtAnnee.Name = "txtAnnee";
            this.txtAnnee.ReadOnly = true;
            this.txtAnnee.Size = new System.Drawing.Size(201, 22);
            this.txtAnnee.TabIndex = 0;
            this.txtAnnee.TabStop = false;
            // 
            // pAR_PARCELLEBindingSource
            // 
            this.pAR_PARCELLEBindingSource.DataMember = "PAR_PARCELLE";
            this.pAR_PARCELLEBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // pAR_PARCELLEDataGridView
            // 
            this.pAR_PARCELLEDataGridView.AllowUserToAddRows = false;
            this.pAR_PARCELLEDataGridView.AllowUserToDeleteRows = false;
            this.pAR_PARCELLEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pAR_PARCELLEDataGridView.AutoGenerateColumns = false;
            this.pAR_PARCELLEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pAR_PARCELLEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pAR_PARCELLEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CEP_ID,
            this.CEP_ACQ_ID,
            this.PAR_ID,
            this.PAR_NUMERO,
            this.AUT_ID,
            this.LIE_ID,
            this.PAR_SURFACE_CEPAGE,
            this.PAR_SURFACE_COMPTEE,
            this.DG_PAR_QUOTA,
            this.PAR_LITRES,
            this.Kilos,
            this.ZOT_ID,
            this.PAR_SECTEUR_VISITE,
            this.PAR_MODE_CULTURE});
            this.pAR_PARCELLEDataGridView.DataSource = this.pAR_PARCELLEBindingSource;
            this.pAR_PARCELLEDataGridView.Location = new System.Drawing.Point(12, 258);
            this.pAR_PARCELLEDataGridView.MultiSelect = false;
            this.pAR_PARCELLEDataGridView.Name = "pAR_PARCELLEDataGridView";
            this.pAR_PARCELLEDataGridView.ReadOnly = true;
            this.pAR_PARCELLEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.pAR_PARCELLEDataGridView.Size = new System.Drawing.Size(965, 385);
            this.pAR_PARCELLEDataGridView.TabIndex = 162;
            this.pAR_PARCELLEDataGridView.TabStop = false;
            this.pAR_PARCELLEDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.PAR_PARCELLEDataGridView_CellFormatting);
            this.pAR_PARCELLEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PAR_PARCELLEDataGridView_ColumnHeaderMouseClick);
            // 
            // CEP_ID
            // 
            this.CEP_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.DataSource = this.cEPCEPAGEBindingSource;
            this.CEP_ID.DisplayMember = "CEP_NOM";
            this.CEP_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CEP_ID.HeaderText = "Cépage";
            this.CEP_ID.MinimumWidth = 100;
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CEP_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.CEP_ID.ValueMember = "CEP_ID";
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // CEP_ACQ_ID
            // 
            this.CEP_ACQ_ID.DataPropertyName = "ACQ_ID";
            this.CEP_ACQ_ID.HeaderText = "Acquit";
            this.CEP_ACQ_ID.Name = "CEP_ACQ_ID";
            this.CEP_ACQ_ID.ReadOnly = true;
            this.CEP_ACQ_ID.Visible = false;
            // 
            // PAR_ID
            // 
            this.PAR_ID.DataPropertyName = "PAR_ID";
            this.PAR_ID.HeaderText = "Id";
            this.PAR_ID.Name = "PAR_ID";
            this.PAR_ID.ReadOnly = true;
            this.PAR_ID.Visible = false;
            // 
            // PAR_NUMERO
            // 
            this.PAR_NUMERO.DataPropertyName = "PAR_NUMERO";
            this.PAR_NUMERO.HeaderText = "Numéro";
            this.PAR_NUMERO.Name = "PAR_NUMERO";
            this.PAR_NUMERO.ReadOnly = true;
            this.PAR_NUMERO.Width = 70;
            // 
            // AUT_ID
            // 
            this.AUT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AUT_ID.DataPropertyName = "AUT_ID";
            this.AUT_ID.DataSource = this.aUTAUTREMENTIONBindingSource;
            this.AUT_ID.DisplayMember = "AUT_NOM";
            this.AUT_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.AUT_ID.HeaderText = "Nom local";
            this.AUT_ID.MinimumWidth = 100;
            this.AUT_ID.Name = "AUT_ID";
            this.AUT_ID.ReadOnly = true;
            this.AUT_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AUT_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.AUT_ID.ValueMember = "AUT_ID";
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // LIE_ID
            // 
            this.LIE_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LIE_ID.DataPropertyName = "LIE_ID";
            this.LIE_ID.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.LIE_ID.DisplayMember = "LIE_NOM";
            this.LIE_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.LIE_ID.HeaderText = "Lieu de production";
            this.LIE_ID.MinimumWidth = 100;
            this.LIE_ID.Name = "LIE_ID";
            this.LIE_ID.ReadOnly = true;
            this.LIE_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LIE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.LIE_ID.ValueMember = "LIE_ID";
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // PAR_SURFACE_CEPAGE
            // 
            this.PAR_SURFACE_CEPAGE.DataPropertyName = "PAR_SURFACE_CEPAGE";
            this.PAR_SURFACE_CEPAGE.HeaderText = "Surface cépage";
            this.PAR_SURFACE_CEPAGE.Name = "PAR_SURFACE_CEPAGE";
            this.PAR_SURFACE_CEPAGE.ReadOnly = true;
            // 
            // PAR_SURFACE_COMPTEE
            // 
            this.PAR_SURFACE_COMPTEE.DataPropertyName = "PAR_SURFACE_COMPTEE";
            this.PAR_SURFACE_COMPTEE.HeaderText = "Surface comptée";
            this.PAR_SURFACE_COMPTEE.Name = "PAR_SURFACE_COMPTEE";
            this.PAR_SURFACE_COMPTEE.ReadOnly = true;
            // 
            // DG_PAR_QUOTA
            // 
            this.DG_PAR_QUOTA.DataPropertyName = "PAR_QUOTA";
            this.DG_PAR_QUOTA.HeaderText = "Litres/m2";
            this.DG_PAR_QUOTA.Name = "DG_PAR_QUOTA";
            this.DG_PAR_QUOTA.ReadOnly = true;
            this.DG_PAR_QUOTA.Width = 60;
            // 
            // PAR_LITRES
            // 
            this.PAR_LITRES.DataPropertyName = "PAR_LITRES";
            this.PAR_LITRES.HeaderText = "Litres";
            this.PAR_LITRES.Name = "PAR_LITRES";
            this.PAR_LITRES.ReadOnly = true;
            this.PAR_LITRES.Width = 80;
            // 
            // Kilos
            // 
            this.Kilos.HeaderText = "Kilos";
            this.Kilos.Name = "Kilos";
            this.Kilos.ReadOnly = true;
            // 
            // ZOT_ID
            // 
            this.ZOT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ZOT_ID.DataPropertyName = "ZOT_ID";
            this.ZOT_ID.DataSource = this.zOTZONETRAVAILBindingSource;
            this.ZOT_ID.DisplayMember = "ZOT_DESCRIPTION";
            this.ZOT_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ZOT_ID.HeaderText = "Zone";
            this.ZOT_ID.MinimumWidth = 20;
            this.ZOT_ID.Name = "ZOT_ID";
            this.ZOT_ID.ReadOnly = true;
            this.ZOT_ID.ValueMember = "ZOT_ID";
            this.ZOT_ID.Visible = false;
            // 
            // zOTZONETRAVAILBindingSource
            // 
            this.zOTZONETRAVAILBindingSource.DataMember = "ZOT_ZONE_TRAVAIL";
            this.zOTZONETRAVAILBindingSource.DataSource = this.dSPesees;
            // 
            // PAR_SECTEUR_VISITE
            // 
            this.PAR_SECTEUR_VISITE.DataPropertyName = "PAR_SECTEUR_VISITE";
            this.PAR_SECTEUR_VISITE.DataSource = this.pAR_SECTEUR_VISITEBindingSource;
            this.PAR_SECTEUR_VISITE.DisplayMember = "PAR_SECTEUR_VISITE";
            this.PAR_SECTEUR_VISITE.HeaderText = "Secteur de visite";
            this.PAR_SECTEUR_VISITE.Name = "PAR_SECTEUR_VISITE";
            this.PAR_SECTEUR_VISITE.ReadOnly = true;
            this.PAR_SECTEUR_VISITE.ValueMember = "PAR_SECTEUR_VISITE";
            this.PAR_SECTEUR_VISITE.Visible = false;
            // 
            // pAR_SECTEUR_VISITEBindingSource
            // 
            this.pAR_SECTEUR_VISITEBindingSource.DataMember = "PAR_SECTEUR_VISITE";
            this.pAR_SECTEUR_VISITEBindingSource.DataSource = this.dSPesees;
            // 
            // PAR_MODE_CULTURE
            // 
            this.PAR_MODE_CULTURE.DataPropertyName = "PAR_MODE_CULTURE";
            this.PAR_MODE_CULTURE.HeaderText = "Mode de culture";
            this.PAR_MODE_CULTURE.Name = "PAR_MODE_CULTURE";
            this.PAR_MODE_CULTURE.ReadOnly = true;
            this.PAR_MODE_CULTURE.Visible = false;
            // 
            // btnDeleteParcelle
            // 
            this.btnDeleteParcelle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteParcelle.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDeleteParcelle.Location = new System.Drawing.Point(983, 366);
            this.btnDeleteParcelle.Name = "btnDeleteParcelle";
            this.btnDeleteParcelle.Size = new System.Drawing.Size(104, 51);
            this.btnDeleteParcelle.TabIndex = 14;
            this.btnDeleteParcelle.Text = "Supprimer la parcelle";
            this.btnDeleteParcelle.UseVisualStyleBackColor = false;
            this.btnDeleteParcelle.Click += new System.EventHandler(this.BtnDeleteParcelle_Click);
            // 
            // btnModifParcelle
            // 
            this.btnModifParcelle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModifParcelle.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnModifParcelle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifParcelle.Location = new System.Drawing.Point(984, 310);
            this.btnModifParcelle.Margin = new System.Windows.Forms.Padding(2);
            this.btnModifParcelle.Name = "btnModifParcelle";
            this.btnModifParcelle.Size = new System.Drawing.Size(104, 51);
            this.btnModifParcelle.TabIndex = 13;
            this.btnModifParcelle.Text = "Modifier la parcelle";
            this.btnModifParcelle.UseVisualStyleBackColor = false;
            this.btnModifParcelle.Click += new System.EventHandler(this.BtnModifParcelle_Click);
            // 
            // btnAddParcelle
            // 
            this.btnAddParcelle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddParcelle.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddParcelle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddParcelle.Location = new System.Drawing.Point(984, 258);
            this.btnAddParcelle.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddParcelle.Name = "btnAddParcelle";
            this.btnAddParcelle.Size = new System.Drawing.Size(103, 48);
            this.btnAddParcelle.TabIndex = 12;
            this.btnAddParcelle.Text = "Ajouter une parcelle";
            this.btnAddParcelle.UseVisualStyleBackColor = false;
            this.btnAddParcelle.Click += new System.EventHandler(this.BtnAddParcelle_Click);
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // lieuProductionMenuStrip
            // 
            this.lieuProductionMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.lieuProductionMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.lieuProductionMenuStrip.Name = "CouleurscontextMenuStrip";
            this.lieuProductionMenuStrip.Size = new System.Drawing.Size(232, 28);
            this.lieuProductionMenuStrip.Click += new System.EventHandler(this.LieuProductionMenuStrip_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(231, 24);
            this.toolStripMenuItem1.Text = "Accéder aux couleurs ...";
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // rEG_IDComboBox
            // 
            this.rEG_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.rEG_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.rEG_IDComboBox.ContextMenuStrip = this.contextMenuRegion;
            this.rEG_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "REG_ID", true));
            this.rEG_IDComboBox.DataSource = this.rEGREGIONBindingSource;
            this.rEG_IDComboBox.DisplayMember = "REG_NAME";
            this.rEG_IDComboBox.FormattingEnabled = true;
            this.rEG_IDComboBox.Location = new System.Drawing.Point(178, 124);
            this.rEG_IDComboBox.Name = "rEG_IDComboBox";
            this.rEG_IDComboBox.Size = new System.Drawing.Size(201, 24);
            this.rEG_IDComboBox.TabIndex = 2;
            this.rEG_IDComboBox.ValueMember = "REG_ID";
            this.rEG_IDComboBox.SelectedIndexChanged += new System.EventHandler(this.REG_IDComboBox_SelectedIndexChanged);
            // 
            // contextMenuRegion
            // 
            this.contextMenuRegion.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuRegion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolRegion});
            this.contextMenuRegion.Name = "CouleurscontextMenuStrip";
            this.contextMenuRegion.Size = new System.Drawing.Size(226, 28);
            // 
            // toolRegion
            // 
            this.toolRegion.Name = "toolRegion";
            this.toolRegion.Size = new System.Drawing.Size(225, 24);
            this.toolRegion.Text = "Accéder aux régions ...";
            this.toolRegion.Click += new System.EventHandler(this.ToolRegion_Click);
            // 
            // rEGREGIONBindingSource
            // 
            this.rEGREGIONBindingSource.DataMember = "REG_REGION";
            this.rEGREGIONBindingSource.DataSource = this.dSPesees;
            // 
            // rESRESPONSABLEBindingSource
            // 
            this.rESRESPONSABLEBindingSource.DataMember = "RES_RESPONSABLE";
            this.rESRESPONSABLEBindingSource.DataSource = this.dSPesees;
            // 
            // rES_RESPONSABLETableAdapter
            // 
            this.rES_RESPONSABLETableAdapter.ClearBeforeFill = true;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // aCQ_NUMERO_COMPLTextBox
            // 
            this.aCQ_NUMERO_COMPLTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aCQ_ACQUITBindingSource, "ACQ_NUMERO_COMPL", true));
            this.aCQ_NUMERO_COMPLTextBox.Location = new System.Drawing.Point(776, 129);
            this.aCQ_NUMERO_COMPLTextBox.Name = "aCQ_NUMERO_COMPLTextBox";
            this.aCQ_NUMERO_COMPLTextBox.Size = new System.Drawing.Size(201, 22);
            this.aCQ_NUMERO_COMPLTextBox.TabIndex = 9;
            // 
            // cLA_IDComboBox
            // 
            this.cLA_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cLA_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cLA_IDComboBox.ContextMenuStrip = this.CategoriescontextMenuStrip;
            this.cLA_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "CLA_ID", true));
            this.cLA_IDComboBox.DataSource = this.cLACLASSEBindingSource;
            this.cLA_IDComboBox.DisplayMember = "CLA_NOM";
            this.cLA_IDComboBox.FormattingEnabled = true;
            this.cLA_IDComboBox.Location = new System.Drawing.Point(178, 99);
            this.cLA_IDComboBox.Name = "cLA_IDComboBox";
            this.cLA_IDComboBox.Size = new System.Drawing.Size(201, 24);
            this.cLA_IDComboBox.TabIndex = 1;
            this.cLA_IDComboBox.ValueMember = "CLA_ID";
            // 
            // pRO_IDComboBox
            // 
            this.pRO_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.pRO_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pRO_IDComboBox.ContextMenuStrip = this.ProprietairesContextMenuStrip;
            this.pRO_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "PRO_ID", true));
            this.pRO_IDComboBox.DataSource = this.pRONOMCOMPLETBindingSource;
            this.pRO_IDComboBox.DisplayMember = "NOMCOMPLET";
            this.pRO_IDComboBox.FormattingEnabled = true;
            this.pRO_IDComboBox.Location = new System.Drawing.Point(776, 49);
            this.pRO_IDComboBox.Name = "pRO_IDComboBox";
            this.pRO_IDComboBox.Size = new System.Drawing.Size(201, 24);
            this.pRO_IDComboBox.TabIndex = 6;
            this.pRO_IDComboBox.ValueMember = "PRO_ID";
            this.pRO_IDComboBox.SelectedIndexChanged += new System.EventHandler(this.PRO_IDComboBox_SelectedIndexChanged);
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // cOU_IDComboBox
            // 
            this.cOU_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cOU_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cOU_IDComboBox.ContextMenuStrip = this.CouleurscontextMenuStrip;
            this.cOU_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "COU_ID", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOU_IDComboBox.DataSource = this.cOUCOULEURBindingSource;
            this.cOU_IDComboBox.DisplayMember = "COU_NOM";
            this.cOU_IDComboBox.FormattingEnabled = true;
            this.cOU_IDComboBox.Location = new System.Drawing.Point(178, 71);
            this.cOU_IDComboBox.Name = "cOU_IDComboBox";
            this.cOU_IDComboBox.Size = new System.Drawing.Size(201, 24);
            this.cOU_IDComboBox.TabIndex = 0;
            this.cOU_IDComboBox.ValueMember = "COU_ID";
            // 
            // districtComboBox
            // 
            this.districtComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.districtComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.districtComboBox.ContextMenuStrip = this.districtcontextMenuStrip;
            this.districtComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.districtComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "COM_ID", true));
            this.districtComboBox.DataSource = this.dISDISTRICTBindingSource;
            this.districtComboBox.DisplayMember = "DIS_NOM";
            this.districtComboBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.districtComboBox.FormattingEnabled = true;
            this.districtComboBox.Location = new System.Drawing.Point(178, 151);
            this.districtComboBox.Name = "districtComboBox";
            this.districtComboBox.Size = new System.Drawing.Size(201, 24);
            this.districtComboBox.TabIndex = 3;
            this.districtComboBox.ValueMember = "DIS_ID";
            this.districtComboBox.SelectedIndexChanged += new System.EventHandler(this.DistrictComboBox_SelectedIndexChanged);
            // 
            // districtcontextMenuStrip
            // 
            this.districtcontextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.districtcontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxDistrictsToolStripMenuItem});
            this.districtcontextMenuStrip.Name = "districtcontextMenuStrip";
            this.districtcontextMenuStrip.Size = new System.Drawing.Size(224, 28);
            // 
            // accéderAuxDistrictsToolStripMenuItem
            // 
            this.accéderAuxDistrictsToolStripMenuItem.Name = "accéderAuxDistrictsToolStripMenuItem";
            this.accéderAuxDistrictsToolStripMenuItem.Size = new System.Drawing.Size(223, 24);
            this.accéderAuxDistrictsToolStripMenuItem.Text = "Accéder aux districts...";
            this.accéderAuxDistrictsToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxDistrictsToolStripMenuItem_Click);
            // 
            // dISDISTRICTBindingSource
            // 
            this.dISDISTRICTBindingSource.DataMember = "DIS_DISTRICT";
            this.dISDISTRICTBindingSource.DataSource = this.dSPesees;
            // 
            // dIS_DISTRICTTableAdapter
            // 
            this.dIS_DISTRICTTableAdapter.ClearBeforeFill = true;
            // 
            // pRO_ID_VENDANGEComboBox
            // 
            this.pRO_ID_VENDANGEComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.pRO_ID_VENDANGEComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pRO_ID_VENDANGEComboBox.ContextMenuStrip = this.ProprietairesContextMenuStrip;
            this.pRO_ID_VENDANGEComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.aCQ_ACQUITBindingSource, "PRO_ID_VENDANGE", true));
            this.pRO_ID_VENDANGEComboBox.DataSource = this.pRONOMCOMPLETBindingSource1;
            this.pRO_ID_VENDANGEComboBox.DisplayMember = "NOMCOMPLET";
            this.pRO_ID_VENDANGEComboBox.FormattingEnabled = true;
            this.pRO_ID_VENDANGEComboBox.Location = new System.Drawing.Point(776, 77);
            this.pRO_ID_VENDANGEComboBox.Name = "pRO_ID_VENDANGEComboBox";
            this.pRO_ID_VENDANGEComboBox.Size = new System.Drawing.Size(201, 24);
            this.pRO_ID_VENDANGEComboBox.TabIndex = 7;
            this.pRO_ID_VENDANGEComboBox.ValueMember = "PRO_ID";
            this.pRO_ID_VENDANGEComboBox.SelectedIndexChanged += new System.EventHandler(this.PRO_ID_VENDANGEComboBox_SelectedIndexChanged);
            // 
            // pRONOMCOMPLETBindingSource1
            // 
            this.pRONOMCOMPLETBindingSource1.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource1.DataSource = this.dsPesees1;
            // 
            // dsPesees1
            // 
            this.dsPesees1.DataSetName = "DSPesees";
            this.dsPesees1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // prO_NOMCOMPLETTableAdapter1
            // 
            this.prO_NOMCOMPLETTableAdapter1.ClearBeforeFill = true;
            // 
            // cbRecu
            // 
            this.cbRecu.AutoSize = true;
            this.cbRecu.Location = new System.Drawing.Point(776, 159);
            this.cbRecu.Name = "cbRecu";
            this.cbRecu.Size = new System.Drawing.Size(63, 21);
            this.cbRecu.TabIndex = 10;
            this.cbRecu.Text = "Reçu";
            this.cbRecu.UseVisualStyleBackColor = true;
            this.cbRecu.CheckedChanged += new System.EventHandler(this.CbRecu_CheckedChanged);
            // 
            // aCQ_RECEPTIONDateTimePicker
            // 
            this.aCQ_RECEPTIONDateTimePicker.CustomFormat = "dd.MM.yyyy";
            this.aCQ_RECEPTIONDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.aCQ_ACQUITBindingSource, "ACQ_RECEPTION", true));
            this.aCQ_RECEPTIONDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.aCQ_RECEPTIONDateTimePicker.Location = new System.Drawing.Point(834, 157);
            this.aCQ_RECEPTIONDateTimePicker.Name = "aCQ_RECEPTIONDateTimePicker";
            this.aCQ_RECEPTIONDateTimePicker.Size = new System.Drawing.Size(142, 22);
            this.aCQ_RECEPTIONDateTimePicker.TabIndex = 11;
            // 
            // zOT_ZONE_TRAVAILTableAdapter
            // 
            this.zOT_ZONE_TRAVAILTableAdapter.ClearBeforeFill = true;
            // 
            // pAR_SECTEUR_VISITETableAdapter
            // 
            this.pAR_SECTEUR_VISITETableAdapter.ClearBeforeFill = true;
            // 
            // pCO_PAR_COMBindingSource
            // 
            this.pCO_PAR_COMBindingSource.DataMember = "PCO_PAR_COM";
            this.pCO_PAR_COMBindingSource.DataSource = this.dSPesees;
            // 
            // pCO_PAR_COMTableAdapter
            // 
            this.pCO_PAR_COMTableAdapter.ClearBeforeFill = true;
            // 
            // plI_PAR_LIETableAdapter
            // 
            this.plI_PAR_LIETableAdapter.ClearBeforeFill = true;
            // 
            // WfrmSaisieAcquit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(1098, 655);
            this.Controls.Add(this.lTitre);
            this.Controls.Add(this.aCQ_LITRESLabel);
            this.Controls.Add(this.aCQ_SURFACETextBox);
            this.Controls.Add(aCQ_SURFACELabel);
            this.Controls.Add(this.aCQ_RECEPTIONDateTimePicker);
            this.Controls.Add(this.cbRecu);
            this.Controls.Add(label6);
            this.Controls.Add(this.aNN_IDTextBox);
            this.Controls.Add(this.aCQ_IDTextBox);
            this.Controls.Add(this.pRO_ID_VENDANGEComboBox);
            this.Controls.Add(label5);
            this.Controls.Add(this.aCQ_DATEDateTimePicker);
            this.Controls.Add(aCQ_DATELabel);
            this.Controls.Add(this.districtComboBox);
            this.Controls.Add(this.dIS_IDLabel);
            this.Controls.Add(this.pAR_PARCELLEDataGridView);
            this.Controls.Add(this.cOU_IDComboBox);
            this.Controls.Add(this.cLA_IDComboBox);
            this.Controls.Add(this.cmdValider);
            this.Controls.Add(this.btnDeleteParcelle);
            this.Controls.Add(this.btnModifParcelle);
            this.Controls.Add(this.pRO_IDComboBox);
            this.Controls.Add(this.aCQ_NUMERO_COMPLTextBox);
            this.Controls.Add(this.aCQ_NUM_COMPLLabel);
            this.Controls.Add(this.aCQ_LITRESTextBox);
            this.Controls.Add(this.aCQ_NUMEROTextBox);
            this.Controls.Add(this.rEG_IDComboBox);
            this.Controls.Add(this.rEG_IDLabel);
            this.Controls.Add(this.btnAddParcelle);
            this.Controls.Add(this.aCQ_NUMEROLabel);
            this.Controls.Add(this.txtAnnee);
            this.Controls.Add(pRO_IDLabel);
            this.Controls.Add(aNN_IDLabel);
            this.Controls.Add(this.cOU_IDLabel);
            this.Controls.Add(this.cOM_IDLabel);
            this.Controls.Add(this.cOM_IDComboBox);
            this.Controls.Add(this.cLA_IDLabel);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(1024, 600);
            this.Name = "WfrmSaisieAcquit";
            this.Text = "Gestion des acquits - ajout d\'un acquit";
            this.Load += new System.EventHandler(this.WfrmAddAcquit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).EndInit();
            this.CouleurscontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            this.CommunesContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            this.ProprietairesContextMenuStrip.ResumeLayout(false);
            this.CategoriescontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_SECTEUR_VISITEBindingSource)).EndInit();
            this.lieuProductionMenuStrip.ResumeLayout(false);
            this.contextMenuRegion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rESRESPONSABLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            this.districtcontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dISDISTRICTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCO_PAR_COMBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aCQ_ACQUITBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox aCQ_IDTextBox;
        private System.Windows.Forms.ComboBox cOM_IDComboBox;
        private System.Windows.Forms.TextBox aCQ_NUMEROTextBox;
        private System.Windows.Forms.DateTimePicker aCQ_DATEDateTimePicker;
        private System.Windows.Forms.TextBox aCQ_SURFACETextBox;
        private System.Windows.Forms.TextBox aCQ_LITRESTextBox;
        private System.Windows.Forms.Label lTitre;
        private System.Windows.Forms.Button cmdValider;
        private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.BindingSource cLACLASSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.TextBox aNN_IDTextBox;
        private System.Windows.Forms.TextBox txtAnnee;
        private System.Windows.Forms.BindingSource pAR_PARCELLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
        private System.Windows.Forms.DataGridView pAR_PARCELLEDataGridView;
        private System.Windows.Forms.Button btnDeleteParcelle;
        private System.Windows.Forms.Button btnModifParcelle;
        private System.Windows.Forms.Button btnAddParcelle;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        //    private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
        //  private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
        //   private System.Windows.Forms.DataGridViewComboBoxColumn CRU_ID;
        //     private System.Windows.Forms.DataGridViewComboBoxColumn APP_ID;
        private System.Windows.Forms.ContextMenuStrip CouleurscontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CouleursToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip CategoriescontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxCatégoriesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip CommunesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxCommunesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ProprietairesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem PropriétairesToolStripMenuItem;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        //     private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MET_MENTIONTXTTableAdapter mET_MENTIONTXTTableAdapter;
        private System.Windows.Forms.ContextMenuStrip lieuProductionMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        //    private System.Windows.Forms.DataGridViewTextBoxColumn PAR_QUOTA;
        private System.Windows.Forms.ComboBox rEG_IDComboBox;
        private System.Windows.Forms.BindingSource rESRESPONSABLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_RESPONSABLETableAdapter rES_RESPONSABLETableAdapter;
        private System.Windows.Forms.BindingSource rEGREGIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
        private System.Windows.Forms.TextBox aCQ_NUMERO_COMPLTextBox;
        private System.Windows.Forms.ComboBox cLA_IDComboBox;
        private System.Windows.Forms.ComboBox pRO_IDComboBox;
        private System.Windows.Forms.ComboBox cOU_IDComboBox;
        private System.Windows.Forms.ComboBox districtComboBox;
        private System.Windows.Forms.BindingSource dISDISTRICTBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter dIS_DISTRICTTableAdapter;
        private System.Windows.Forms.ContextMenuStrip districtcontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxDistrictsToolStripMenuItem;
        private System.Windows.Forms.ComboBox pRO_ID_VENDANGEComboBox;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource1;
        private Gestion_Des_Vendanges.DAO.DSPesees dsPesees1;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter prO_NOMCOMPLETTableAdapter1;
        private System.Windows.Forms.CheckBox cbRecu;
        private System.Windows.Forms.DateTimePicker aCQ_RECEPTIONDateTimePicker;
        private System.Windows.Forms.ContextMenuStrip contextMenuRegion;
        private System.Windows.Forms.ToolStripMenuItem toolRegion;
        private System.Windows.Forms.BindingSource zOTZONETRAVAILBindingSource;
        private DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter zOT_ZONE_TRAVAILTableAdapter;
        private System.Windows.Forms.BindingSource pAR_SECTEUR_VISITEBindingSource;
        private DAO.DSPeseesTableAdapters.PAR_SECTEUR_VISITETableAdapter pAR_SECTEUR_VISITETableAdapter;
        private System.Windows.Forms.DataGridViewComboBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEP_ACQ_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_NUMERO;
        private System.Windows.Forms.DataGridViewComboBoxColumn AUT_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn LIE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_SURFACE_CEPAGE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_SURFACE_COMPTEE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DG_PAR_QUOTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_LITRES;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kilos;
        private System.Windows.Forms.DataGridViewComboBoxColumn ZOT_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn PAR_SECTEUR_VISITE;
        private System.Windows.Forms.DataGridViewComboBoxColumn PAR_MODE_CULTURE;
        private System.Windows.Forms.Label cLA_IDLabel;
        private System.Windows.Forms.Label cOU_IDLabel;
        private System.Windows.Forms.Label cOM_IDLabel;
        private System.Windows.Forms.Label aCQ_NUMEROLabel;
        private System.Windows.Forms.Label aCQ_LITRESLabel;
        private System.Windows.Forms.Label rEG_IDLabel;
        private System.Windows.Forms.Label aCQ_NUM_COMPLLabel;
        private System.Windows.Forms.Label dIS_IDLabel;
        private System.Windows.Forms.BindingSource pCO_PAR_COMBindingSource;
        private DAO.DSPeseesTableAdapters.PCO_PAR_COMTableAdapter pCO_PAR_COMTableAdapter;
        private DAO.DSPeseesTableAdapters.PLI_PAR_LIETableAdapter plI_PAR_LIETableAdapter;
    }
}