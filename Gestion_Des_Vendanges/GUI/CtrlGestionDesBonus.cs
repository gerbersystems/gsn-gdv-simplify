﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Extensions;
using GSN.GDV.Data.Models.Data.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des tables de prix
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class CtrlGestionDesBonus : CtrlWfrm, ICtrlSetting
    {
        #region Fields

        private List<OechsleMoyenParCepageModel> _avgOechsleByGrapes;
        private FormSettingsManagerBonus _manager;
        private int _lieId;
        private int _cepId;

        #endregion Fields

        #region Properties

        public bool CanDelete => true;
        public override ToolStrip ToolStrip => toolStrip2;
        public override Panel OptionPanel => panel1;

        #endregion Properties

        #region Constructors

        public static CtrlGestionDesBonus GetInstance(int annId)
        {
            Utilities.IdAnneeCourante = annId;
            return GetInstance();
        }

        public static CtrlGestionDesBonus GetInstance()
        {
            // Check if licence level is valid
            if (Utilities.GetOption(1)) return new CtrlGestionDesBonus();

            MessageBox.Show("Vous n'avez pas la license pour utiliser cette option.\n" +
                            "Cette option est uniquement disponible avec la version étendue.",
                            "License", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return null;
        }

        private CtrlGestionDesBonus()
        {
            InitializeComponent();
            Text = "Gestion des bonus";

            //Modifie la vue Gestion des bonus selon le mode pourcent ou francs
            bool _isBonusUniteChf = (GlobalParam.Instance.BonusUnite == SettingExtension.BonusUniteEnum.VariableChf);
            bool _isDegreValeurOe = (GlobalParam.Instance.DegreValeur == SettingExtension.DegreValeur.Oechsle);
            OeMoyenTextBox.Visible = lblOeMoyen.Visible = _isBonusUniteChf;

            bON_BONUSDataGridView.Columns["BON_OE"].Visible = _isDegreValeurOe;
            bON_BONUSDataGridView.Columns["BON_OE_FIN"].Visible = _isDegreValeurOe;
            bON_BONUSDataGridView.Columns["BON_BRIX"].Visible = !_isDegreValeurOe;
            bON_BONUSDataGridView.Columns["BRIX_FIN"].Visible = !_isDegreValeurOe;

            if (_isBonusUniteChf)
            {
                bON_BONUSDataGridView.Columns["VAL_BONUS"].HeaderText = "Bonus | Malus (CHF)";

                //TODO BonusMalus | Get liste Oechsle Moyen
                using (var dBContext = new MainDataContext())
                {
                    // TODO Average Oechsle must be ("pondéré") weighted by quantity
                    // TODO Average Oechsle is by Grape and ProductionLocality
                    _avgOechsleByGrapes = OechsleMoyenParCepageExtension.ListOechsleMoyenParCepage(dBContext, Utilities.IdAnneeCourante);
                }
            }
        }

        private void WfrmGestionDesBonus_Load(object sender, EventArgs e)
        {
            InitTableAdapters();
            UpdateDonnees();

            InitFormSettingsManagerBonus();

            if (_lieId != 0 && _cepId != 0)
            {
                OptionPanel.Visible = true;
                splitContainer1.Panel1.Controls.Add(OptionPanel);
                splitContainer1.Panel1MinSize = 0;
                splitContainer1.SplitterDistance = 0;
                splitContainer1.FixedPanel = FixedPanel.Panel1;
                splitContainer1.IsSplitterFixed = true;
                ToolStrip.Hide();
                lIE_IDComboBox1.SelectedValue = _lieId;
                _manager.NewTablePrix(_lieId, _cepId);
                vAL_VALEURTextBox.Focus();
            }
        }

        private void InitTableAdapters()
        {
            ConnectionManager.Instance.AddGvTableAdapter(viewBonusMaxTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(bON_BONUSTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
        }

        private void InitFormSettingsManagerBonus()
        {
            _manager = new FormSettingsManagerBonus(form: this,
                                                    idTextBox: vAL_IDTextBox,
                                                    focusControl: cEP_IDComboBox,
                                                    btnNew: toolAdd,
                                                    btnUpdate: toolUpdate,
                                                    btnDelete: toolDelete,
                                                    btnOK: btnOK,
                                                    btnCancel: btnAnnuler,
                                                    dataGridView: vAL_VALEUR_CEPAGEDataGridView,
                                                    bindingSource: vAL_VALEUR_CEPAGEBindingSource,
                                                    tableAdapterManager: tableAdapterManager,
                                                    dsPesees: dSPesees,
                                                    cbCepageId: cEP_IDComboBox,
                                                    cbLieuProductionId: lIE_IDComboBox,
                                                    txtAnnId: aNN_IDTextBox,
                                                    annId: Utilities.IdAnneeCourante,
                                                    valTableAdapter: vAL_VALEUR_CEPAGETableAdapter,
                                                    bonusDataGridView: bON_BONUSDataGridView,
                                                    bonusBindingSource: bON_BONUSBindingSource,
                                                    bonusTableAdapter: bON_BONUSTableAdapter,
                                                    viewBonusMaxTableAdapter: viewBonusMaxTableAdapter,
                                                    cbFiltreLieuDeProduction: lIE_IDComboBox1,
                                                    filtreLieTableAdapter: liE_LIEU_DE_PRODUCTIONTableAdapter1,
                                                    dsPeseesFiltre: dsPesees1,
                                                    cbAnnee: cbAnnee,
                                                    btnCopyLieuProduction: toolCopy,
                                                    btnCopyCepage: toolBtnCopyCepage,
                                                    lprix100: lprix100);
            _manager.AddControl(vAL_VALEURTextBox);
            _manager.AddControl(cEP_IDComboBox);
            _manager.AddControl(lIE_IDComboBox);
            _manager.AddControl(bON_BONUSDataGridView);
        }

        #endregion Constructors

        #region Methods

        public void NewTable(int lieId, int cepId)
        {
            _lieId = lieId;
            _cepId = cepId;
        }

        public override void UpdateDonnees()
        {
            viewBonusMaxTableAdapter.Fill(dSPesees.ViewBonusMax);
            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
            bON_BONUSTableAdapter.Fill(dSPesees.BON_BONUS);
            cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
        }

        private void SupprimerLaLigneToolStripMenuItem_Click(object sender, EventArgs e) => bON_BONUSBindingSource.RemoveCurrent();

        private void VAL_VALEUR_CEPAGEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != 2) return;

            vAL_VALEUR_CEPAGEBindingSource.RemoveSort();
            vAL_VALEUR_CEPAGETableAdapter.FillByLieAnnOrderByCep(dSPesees.VAL_VALEUR_CEPAGE,
                                                                 (int)lIE_IDComboBox1.SelectedValue,
                                                                 Utilities.IdAnneeCourante);
        }

        private void BtnApercu_Click(object sender, EventArgs e)
        {
            try
            {
                int lieId = (int)lIE_IDComboBox1.SelectedValue;
                CtrlReportViewer.ShowTableDesPrix(Utilities.IdAnneeCourante, lieId);
            }
            catch
            {
                MessageBox.Show("Erreur lors de l'affichage des tables de prix.\n",
                                "Table de prix", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DataGridView_EnabledChanged(object sender, EventArgs e)
        {
            var dataGrid = (DataGridView)sender;
            dataGrid.BackgroundColor = (dataGrid.Enabled) ? Properties.Settings.Default.DataGridColor : Color.Gray;
        }

        #endregion Methods
    }
}