﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmKeyManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmKeyManager));
            this.aNN_IDLabel = new System.Windows.Forms.Label();
            this.lTitre = new System.Windows.Forms.Label();
            this.cmdActiver = new System.Windows.Forms.Button();
            this.cmdQuitter = new System.Windows.Forms.Button();
            this.txtCle1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCle2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCle4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCle3 = new System.Windows.Forms.TextBox();
            this.cmdPaste = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // aNN_IDLabel
            // 
            this.aNN_IDLabel.AutoSize = true;
            this.aNN_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aNN_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aNN_IDLabel.Location = new System.Drawing.Point(9, 58);
            this.aNN_IDLabel.Name = "aNN_IDLabel";
            this.aNN_IDLabel.Size = new System.Drawing.Size(121, 16);
            this.aNN_IDLabel.TabIndex = 17;
            this.aNN_IDLabel.Text = "Clé d\'activation :";
            // 
            // lTitre
            // 
            this.lTitre.AutoSize = true;
            this.lTitre.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lTitre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lTitre.Location = new System.Drawing.Point(12, 9);
            this.lTitre.Name = "lTitre";
            this.lTitre.Size = new System.Drawing.Size(197, 20);
            this.lTitre.TabIndex = 24;
            this.lTitre.Text = "Activation du logiciel";
            // 
            // cmdActiver
            // 
            this.cmdActiver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdActiver.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdActiver.Location = new System.Drawing.Point(296, 117);
            this.cmdActiver.Name = "cmdActiver";
            this.cmdActiver.Size = new System.Drawing.Size(75, 30);
            this.cmdActiver.TabIndex = 5;
            this.cmdActiver.Text = "Activer";
            this.cmdActiver.UseVisualStyleBackColor = false;
            this.cmdActiver.Click += new System.EventHandler(this.cmdActiver_Click);
            // 
            // cmdQuitter
            // 
            this.cmdQuitter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdQuitter.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdQuitter.Location = new System.Drawing.Point(377, 117);
            this.cmdQuitter.Name = "cmdQuitter";
            this.cmdQuitter.Size = new System.Drawing.Size(75, 30);
            this.cmdQuitter.TabIndex = 6;
            this.cmdQuitter.Text = "Quitter";
            this.cmdQuitter.UseVisualStyleBackColor = false;
            this.cmdQuitter.Click += new System.EventHandler(this.cmdQuitter_Click);
            // 
            // txtCle1
            // 
            this.txtCle1.Location = new System.Drawing.Point(136, 57);
            this.txtCle1.Name = "txtCle1";
            this.txtCle1.Size = new System.Drawing.Size(67, 20);
            this.txtCle1.TabIndex = 0;
            this.txtCle1.TextChanged += new System.EventHandler(this.txtCle1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(206, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "-";
            // 
            // txtCle2
            // 
            this.txtCle2.Location = new System.Drawing.Point(219, 57);
            this.txtCle2.Name = "txtCle2";
            this.txtCle2.Size = new System.Drawing.Size(67, 20);
            this.txtCle2.TabIndex = 2;
            this.txtCle2.TextChanged += new System.EventHandler(this.txtCle2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "-";
            // 
            // txtCle4
            // 
            this.txtCle4.Location = new System.Drawing.Point(385, 57);
            this.txtCle4.Name = "txtCle4";
            this.txtCle4.Size = new System.Drawing.Size(67, 20);
            this.txtCle4.TabIndex = 4;
            this.txtCle4.TextChanged += new System.EventHandler(this.txtCle4_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(372, 60);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "-";
            // 
            // txtCle3
            // 
            this.txtCle3.Location = new System.Drawing.Point(302, 57);
            this.txtCle3.Name = "txtCle3";
            this.txtCle3.Size = new System.Drawing.Size(67, 20);
            this.txtCle3.TabIndex = 3;
            this.txtCle3.TextChanged += new System.EventHandler(this.txtCle3_TextChanged);
            // 
            // cmdPaste
            // 
            this.cmdPaste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdPaste.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdPaste.Location = new System.Drawing.Point(12, 117);
            this.cmdPaste.Name = "cmdPaste";
            this.cmdPaste.Size = new System.Drawing.Size(75, 30);
            this.cmdPaste.TabIndex = 34;
            this.cmdPaste.Text = "Coller";
            this.cmdPaste.UseVisualStyleBackColor = false;
            this.cmdPaste.Click += new System.EventHandler(this.cmdPaste_Click);
            // 
            // WfrmKeyManager
            // 
            this.AcceptButton = this.cmdActiver;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(464, 159);
            this.Controls.Add(this.cmdPaste);
            this.Controls.Add(this.txtCle4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCle3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCle2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCle1);
            this.Controls.Add(this.cmdQuitter);
            this.Controls.Add(this.cmdActiver);
            this.Controls.Add(this.lTitre);
            this.Controls.Add(this.aNN_IDLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 197);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 197);
            this.Name = "WfrmKeyManager";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Gestion des vendanges - Activation";
            this.Activated += new System.EventHandler(this.WfrmKeyManager_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lTitre;
        private System.Windows.Forms.Button cmdActiver;
        private System.Windows.Forms.Label aNN_IDLabel;
        private System.Windows.Forms.Button cmdQuitter;
        private System.Windows.Forms.TextBox txtCle1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCle2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCle4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCle3;
        private System.Windows.Forms.Button cmdPaste;
    }
}