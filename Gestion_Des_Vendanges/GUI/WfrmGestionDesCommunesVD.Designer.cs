﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesCommunes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cOM_NOMLabel;
            System.Windows.Forms.Label cOM_NUMEROLabel;
            System.Windows.Forms.Label dIS_IDLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.cOM_COMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cOM_COMMUNEDataGridView = new System.Windows.Forms.DataGridView();
            this.COM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COM_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIS_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dISDISTRICTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_NOMTextBox = new System.Windows.Forms.TextBox();
            this.btnAddCommune = new System.Windows.Forms.Button();
            this.btnSaveCommune = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dIS_IDComboBox = new System.Windows.Forms.ComboBox();
            this.districtMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOM_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.cOM_IDTextBox = new System.Windows.Forms.TextBox();
            this.dIS_DISTRICTTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter();
            cOM_NOMLabel = new System.Windows.Forms.Label();
            cOM_NUMEROLabel = new System.Windows.Forms.Label();
            dIS_IDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOM_COMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOM_COMMUNEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISDISTRICTBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.districtMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // cOM_NOMLabel
            // 
            cOM_NOMLabel.AutoSize = true;
            cOM_NOMLabel.Location = new System.Drawing.Point(6, 33);
            cOM_NOMLabel.Name = "cOM_NOMLabel";
            cOM_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cOM_NOMLabel.TabIndex = 17;
            cOM_NOMLabel.Text = "Nom : ";
            // 
            // cOM_NUMEROLabel
            // 
            cOM_NUMEROLabel.AutoSize = true;
            cOM_NUMEROLabel.Location = new System.Drawing.Point(6, 63);
            cOM_NUMEROLabel.Name = "cOM_NUMEROLabel";
            cOM_NUMEROLabel.Size = new System.Drawing.Size(50, 13);
            cOM_NUMEROLabel.TabIndex = 17;
            cOM_NUMEROLabel.Text = "Numéro :";
            // 
            // dIS_IDLabel
            // 
            dIS_IDLabel.AutoSize = true;
            dIS_IDLabel.Location = new System.Drawing.Point(6, 91);
            dIS_IDLabel.Name = "dIS_IDLabel";
            dIS_IDLabel.Size = new System.Drawing.Size(45, 13);
            dIS_IDLabel.TabIndex = 18;
            dIS_IDLabel.Text = "District :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cOM_COMMUNEBindingSource
            // 
            this.cOM_COMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOM_COMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = this.cOM_COMMUNETableAdapter;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // cOM_COMMUNEDataGridView
            // 
            this.cOM_COMMUNEDataGridView.AllowUserToAddRows = false;
            this.cOM_COMMUNEDataGridView.AllowUserToDeleteRows = false;
            this.cOM_COMMUNEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cOM_COMMUNEDataGridView.AutoGenerateColumns = false;
            this.cOM_COMMUNEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.cOM_COMMUNEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cOM_COMMUNEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COM_ID,
            this.COM_NUMERO,
            this.dataGridViewTextBoxColumn4,
            this.DIS_ID});
            this.cOM_COMMUNEDataGridView.DataSource = this.cOM_COMMUNEBindingSource;
            this.cOM_COMMUNEDataGridView.Location = new System.Drawing.Point(411, 12);
            this.cOM_COMMUNEDataGridView.MultiSelect = false;
            this.cOM_COMMUNEDataGridView.Name = "cOM_COMMUNEDataGridView";
            this.cOM_COMMUNEDataGridView.ReadOnly = true;
            this.cOM_COMMUNEDataGridView.RowTemplate.Height = 24;
            this.cOM_COMMUNEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cOM_COMMUNEDataGridView.Size = new System.Drawing.Size(419, 342);
            this.cOM_COMMUNEDataGridView.TabIndex = 0;
            this.cOM_COMMUNEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.cOM_COMMUNEDataGridView_ColumnHeaderMouseClick);
            // 
            // COM_ID
            // 
            this.COM_ID.DataPropertyName = "COM_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.COM_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.COM_ID.HeaderText = "ID";
            this.COM_ID.Name = "COM_ID";
            this.COM_ID.ReadOnly = true;
            this.COM_ID.Visible = false;
            this.COM_ID.Width = 80;
            // 
            // COM_NUMERO
            // 
            this.COM_NUMERO.DataPropertyName = "COM_NUMERO";
            this.COM_NUMERO.HeaderText = "Numéro";
            this.COM_NUMERO.Name = "COM_NUMERO";
            this.COM_NUMERO.ReadOnly = true;
            this.COM_NUMERO.Width = 80;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "COM_NOM";
            this.dataGridViewTextBoxColumn4.HeaderText = "Commune";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // DIS_ID
            // 
            this.DIS_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DIS_ID.DataPropertyName = "DIS_ID";
            this.DIS_ID.DataSource = this.dISDISTRICTBindingSource;
            this.DIS_ID.DisplayMember = "DIS_NOM";
            this.DIS_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.DIS_ID.HeaderText = "District";
            this.DIS_ID.MinimumWidth = 100;
            this.DIS_ID.Name = "DIS_ID";
            this.DIS_ID.ReadOnly = true;
            this.DIS_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DIS_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.DIS_ID.ValueMember = "DIS_ID";
            // 
            // dISDISTRICTBindingSource
            // 
            this.dISDISTRICTBindingSource.DataMember = "DIS_DISTRICT";
            this.dISDISTRICTBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_NOMTextBox
            // 
            this.cOM_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cOM_COMMUNEBindingSource, "COM_NOM", true));
            this.cOM_NOMTextBox.Location = new System.Drawing.Point(131, 30);
            this.cOM_NOMTextBox.Name = "cOM_NOMTextBox";
            this.cOM_NOMTextBox.Size = new System.Drawing.Size(256, 19);
            this.cOM_NOMTextBox.TabIndex = 0;
            // 
            // btnAddCommune
            // 
            this.btnAddCommune.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddCommune.Location = new System.Drawing.Point(150, 134);
            this.btnAddCommune.Name = "btnAddCommune";
            this.btnAddCommune.Size = new System.Drawing.Size(75, 30);
            this.btnAddCommune.TabIndex = 3;
            this.btnAddCommune.Text = "Nouveau";
            this.btnAddCommune.UseVisualStyleBackColor = false;
            // 
            // btnSaveCommune
            // 
            this.btnSaveCommune.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveCommune.Location = new System.Drawing.Point(231, 134);
            this.btnSaveCommune.Name = "btnSaveCommune";
            this.btnSaveCommune.Size = new System.Drawing.Size(75, 30);
            this.btnSaveCommune.TabIndex = 4;
            this.btnSaveCommune.Text = "Modifier";
            this.btnSaveCommune.UseVisualStyleBackColor = false;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(312, 134);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 5;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dIS_IDComboBox);
            this.groupBox1.Controls.Add(dIS_IDLabel);
            this.groupBox1.Controls.Add(cOM_NUMEROLabel);
            this.groupBox1.Controls.Add(this.cOM_NUMEROTextBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnSaveCommune);
            this.groupBox1.Controls.Add(this.btnAddCommune);
            this.groupBox1.Controls.Add(cOM_NOMLabel);
            this.groupBox1.Controls.Add(this.cOM_NOMTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(393, 170);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des communes";
            // 
            // dIS_IDComboBox
            // 
            this.dIS_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.dIS_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.dIS_IDComboBox.ContextMenuStrip = this.districtMenuStrip;
            this.dIS_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cOM_COMMUNEBindingSource, "DIS_ID", true));
            this.dIS_IDComboBox.DataSource = this.dISDISTRICTBindingSource;
            this.dIS_IDComboBox.DisplayMember = "DIS_NOM";
            this.dIS_IDComboBox.FormattingEnabled = true;
            this.dIS_IDComboBox.Location = new System.Drawing.Point(131, 85);
            this.dIS_IDComboBox.Name = "dIS_IDComboBox";
            this.dIS_IDComboBox.Size = new System.Drawing.Size(256, 21);
            this.dIS_IDComboBox.TabIndex = 2;
            this.dIS_IDComboBox.ValueMember = "DIS_ID";
            // 
            // districtMenuStrip
            // 
            this.districtMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem});
            this.districtMenuStrip.Name = "districtMenuStrip";
            this.districtMenuStrip.Size = new System.Drawing.Size(187, 26);
            // 
            // accéderAuMenuGestionDesCommunesToolStripMenuItem
            // 
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem.Name = "accéderAuMenuGestionDesCommunesToolStripMenuItem";
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem.Text = "Accéder aux districts...";
            this.accéderAuMenuGestionDesCommunesToolStripMenuItem.Click += new System.EventHandler(this.accederAuMenuGestionDesCommunesToolStripMenuItem_Click);
            // 
            // cOM_NUMEROTextBox
            // 
            this.cOM_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cOM_COMMUNEBindingSource, "COM_NUMERO", true));
            this.cOM_NUMEROTextBox.Location = new System.Drawing.Point(131, 57);
            this.cOM_NUMEROTextBox.Name = "cOM_NUMEROTextBox";
            this.cOM_NUMEROTextBox.Size = new System.Drawing.Size(256, 19);
            this.cOM_NUMEROTextBox.TabIndex = 1;
            // 
            // cOM_IDTextBox
            // 
            this.cOM_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.cOM_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cOM_COMMUNEBindingSource, "COM_ID", true));
            this.cOM_IDTextBox.Enabled = false;
            this.cOM_IDTextBox.Location = new System.Drawing.Point(542, 286);
            this.cOM_IDTextBox.Name = "cOM_IDTextBox";
            this.cOM_IDTextBox.Size = new System.Drawing.Size(145, 19);
            this.cOM_IDTextBox.TabIndex = 1;
            // 
            // dIS_DISTRICTTableAdapter
            // 
            this.dIS_DISTRICTTableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesCommunes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(842, 366);
            this.Controls.Add(this.cOM_COMMUNEDataGridView);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cOM_IDTextBox);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(700, 400);
            this.Name = "WfrmGestionDesCommunes";
            this.Text = "Gestion des Communes";
            this.Load += new System.EventHandler(this.WfrmGestionDesCommunes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOM_COMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOM_COMMUNEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISDISTRICTBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.districtMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource cOM_COMMUNEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView cOM_COMMUNEDataGridView;
        private System.Windows.Forms.TextBox cOM_NOMTextBox;
        private System.Windows.Forms.Button btnAddCommune;
        private System.Windows.Forms.Button btnSaveCommune;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.GroupBox groupBox1;
       // private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
     //   private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox cOM_IDTextBox;
        private System.Windows.Forms.ComboBox dIS_IDComboBox;
        private System.Windows.Forms.TextBox cOM_NUMEROTextBox;
        private System.Windows.Forms.BindingSource dISDISTRICTBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.DIS_DISTRICTTableAdapter dIS_DISTRICTTableAdapter;
        private System.Windows.Forms.ContextMenuStrip districtMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuMenuGestionDesCommunesToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn COM_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn COM_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn DIS_ID;
    }
}