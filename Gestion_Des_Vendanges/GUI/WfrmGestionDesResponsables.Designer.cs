﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesResponsables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label aDR_IDLabel;
            System.Windows.Forms.Label rES_NOMLabel;
            System.Windows.Forms.Label rES_PRENOMLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.rES_RESPONSABLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rES_RESPONSABLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_RESPONSABLETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aDR_ADRESSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter();
            this.rES_RESPONSABLEDataGridView = new System.Windows.Forms.DataGridView();
            this.RES_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADR_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.adresseCompletBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RES_DEFAULT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.aDRADRESSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rES_IDTextBox = new System.Windows.Forms.TextBox();
            this.rES_NOMTextBox = new System.Windows.Forms.TextBox();
            this.rES_PRENOMTextBox = new System.Windows.Forms.TextBox();
            this.aDR_IDComboBox = new System.Windows.Forms.ComboBox();
            this.AdressesontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AdresseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aDRADRESSEBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rES_DEFAULTCheckBox = new System.Windows.Forms.CheckBox();
            this.adresseCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter();
            aDR_IDLabel = new System.Windows.Forms.Label();
            rES_NOMLabel = new System.Windows.Forms.Label();
            rES_PRENOMLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rES_RESPONSABLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rES_RESPONSABLEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).BeginInit();
            this.AdressesontextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // aDR_IDLabel
            // 
            aDR_IDLabel.AutoSize = true;
            aDR_IDLabel.Location = new System.Drawing.Point(6, 88);
            aDR_IDLabel.Name = "aDR_IDLabel";
            aDR_IDLabel.Size = new System.Drawing.Size(54, 13);
            aDR_IDLabel.TabIndex = 23;
            aDR_IDLabel.Text = "Adresse : ";
            // 
            // rES_NOMLabel
            // 
            rES_NOMLabel.AutoSize = true;
            rES_NOMLabel.Location = new System.Drawing.Point(6, 34);
            rES_NOMLabel.Name = "rES_NOMLabel";
            rES_NOMLabel.Size = new System.Drawing.Size(38, 13);
            rES_NOMLabel.TabIndex = 25;
            rES_NOMLabel.Text = "Nom : ";
            // 
            // rES_PRENOMLabel
            // 
            rES_PRENOMLabel.AutoSize = true;
            rES_PRENOMLabel.Location = new System.Drawing.Point(6, 62);
            rES_PRENOMLabel.Name = "rES_PRENOMLabel";
            rES_PRENOMLabel.Size = new System.Drawing.Size(52, 13);
            rES_PRENOMLabel.TabIndex = 27;
            rES_PRENOMLabel.Text = "Prénom : ";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 110);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(50, 13);
            label1.TabIndex = 28;
            label1.Text = "Default : ";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rES_RESPONSABLEBindingSource
            // 
            this.rES_RESPONSABLEBindingSource.DataMember = "RES_RESPONSABLE";
            this.rES_RESPONSABLEBindingSource.DataSource = this.dSPesees;
            // 
            // rES_RESPONSABLETableAdapter
            // 
            this.rES_RESPONSABLETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = this.aDR_ADRESSETableAdapter;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = this.rES_RESPONSABLETableAdapter;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aDR_ADRESSETableAdapter
            // 
            this.aDR_ADRESSETableAdapter.ClearBeforeFill = true;
            // 
            // rES_RESPONSABLEDataGridView
            // 
            this.rES_RESPONSABLEDataGridView.AllowUserToAddRows = false;
            this.rES_RESPONSABLEDataGridView.AllowUserToDeleteRows = false;
            this.rES_RESPONSABLEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rES_RESPONSABLEDataGridView.AutoGenerateColumns = false;
            this.rES_RESPONSABLEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.rES_RESPONSABLEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rES_RESPONSABLEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RES_ID,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.ADR_ID,
            this.RES_DEFAULT});
            this.rES_RESPONSABLEDataGridView.DataSource = this.rES_RESPONSABLEBindingSource;
            this.rES_RESPONSABLEDataGridView.Location = new System.Drawing.Point(12, 186);
            this.rES_RESPONSABLEDataGridView.Name = "rES_RESPONSABLEDataGridView";
            this.rES_RESPONSABLEDataGridView.ReadOnly = true;
            this.rES_RESPONSABLEDataGridView.RowTemplate.Height = 24;
            this.rES_RESPONSABLEDataGridView.Size = new System.Drawing.Size(709, 266);
            this.rES_RESPONSABLEDataGridView.TabIndex = 19;
            this.rES_RESPONSABLEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.rES_RESPONSABLEDataGridView_ColumnHeaderMouseClick);
            // 
            // RES_ID
            // 
            this.RES_ID.DataPropertyName = "RES_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.RES_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.RES_ID.HeaderText = "Id";
            this.RES_ID.Name = "RES_ID";
            this.RES_ID.ReadOnly = true;
            this.RES_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "RES_NOM";
            this.dataGridViewTextBoxColumn3.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "RES_PRENOM";
            this.dataGridViewTextBoxColumn4.HeaderText = "Prénom";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // ADR_ID
            // 
            this.ADR_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ADR_ID.DataPropertyName = "ADR_ID";
            this.ADR_ID.DataSource = this.adresseCompletBindingSource;
            this.ADR_ID.DisplayMember = "ADR_COMPLET";
            this.ADR_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ADR_ID.HeaderText = "Adresse";
            this.ADR_ID.MinimumWidth = 150;
            this.ADR_ID.Name = "ADR_ID";
            this.ADR_ID.ReadOnly = true;
            this.ADR_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ADR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ADR_ID.ValueMember = "ADR_ID";
            // 
            // adresseCompletBindingSource
            // 
            this.adresseCompletBindingSource.DataMember = "AdresseComplet";
            this.adresseCompletBindingSource.DataSource = this.dSPesees;
            // 
            // RES_DEFAULT
            // 
            this.RES_DEFAULT.DataPropertyName = "RES_DEFAULT";
            this.RES_DEFAULT.FalseValue = "0";
            this.RES_DEFAULT.HeaderText = "Default";
            this.RES_DEFAULT.Name = "RES_DEFAULT";
            this.RES_DEFAULT.ReadOnly = true;
            this.RES_DEFAULT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RES_DEFAULT.TrueValue = "1";
            // 
            // aDRADRESSEBindingSource
            // 
            this.aDRADRESSEBindingSource.DataMember = "ADR_ADRESSE";
            this.aDRADRESSEBindingSource.DataSource = this.dSPesees;
            // 
            // rES_IDTextBox
            // 
            this.rES_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.rES_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rES_RESPONSABLEBindingSource, "RES_ID", true));
            this.rES_IDTextBox.Enabled = false;
            this.rES_IDTextBox.Location = new System.Drawing.Point(453, 268);
            this.rES_IDTextBox.Name = "rES_IDTextBox";
            this.rES_IDTextBox.Size = new System.Drawing.Size(125, 19);
            this.rES_IDTextBox.TabIndex = 22;
            // 
            // rES_NOMTextBox
            // 
            this.rES_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rES_RESPONSABLEBindingSource, "RES_NOM", true));
            this.rES_NOMTextBox.Location = new System.Drawing.Point(81, 31);
            this.rES_NOMTextBox.Name = "rES_NOMTextBox";
            this.rES_NOMTextBox.Size = new System.Drawing.Size(238, 19);
            this.rES_NOMTextBox.TabIndex = 0;
            // 
            // rES_PRENOMTextBox
            // 
            this.rES_PRENOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rES_RESPONSABLEBindingSource, "RES_PRENOM", true));
            this.rES_PRENOMTextBox.Location = new System.Drawing.Point(81, 59);
            this.rES_PRENOMTextBox.Name = "rES_PRENOMTextBox";
            this.rES_PRENOMTextBox.Size = new System.Drawing.Size(238, 19);
            this.rES_PRENOMTextBox.TabIndex = 1;
            // 
            // aDR_IDComboBox
            // 
            this.aDR_IDComboBox.ContextMenuStrip = this.AdressesontextMenuStrip;
            this.aDR_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.rES_RESPONSABLEBindingSource, "ADR_ID", true));
            this.aDR_IDComboBox.DataSource = this.adresseCompletBindingSource;
            this.aDR_IDComboBox.DisplayMember = "ADR_COMPLET";
            this.aDR_IDComboBox.FormattingEnabled = true;
            this.aDR_IDComboBox.Location = new System.Drawing.Point(81, 85);
            this.aDR_IDComboBox.Name = "aDR_IDComboBox";
            this.aDR_IDComboBox.Size = new System.Drawing.Size(238, 21);
            this.aDR_IDComboBox.TabIndex = 2;
            this.aDR_IDComboBox.ValueMember = "ADR_ID";
            // 
            // AdressesontextMenuStrip
            // 
            this.AdressesontextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.AdressesontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdresseMenuItem});
            this.AdressesontextMenuStrip.Name = "AdressesontextMenuStrip";
            this.AdressesontextMenuStrip.Size = new System.Drawing.Size(186, 26);
            this.AdressesontextMenuStrip.Click += new System.EventHandler(this.AdressesontextMenuStrip_Click);
            // 
            // AdresseMenuItem
            // 
            this.AdresseMenuItem.Name = "AdresseMenuItem";
            this.AdresseMenuItem.Size = new System.Drawing.Size(185, 22);
            this.AdresseMenuItem.Text = "Accéder aux adresses";
            // 
            // aDRADRESSEBindingSource1
            // 
            this.aDRADRESSEBindingSource1.DataMember = "ADR_ADRESSE";
            this.aDRADRESSEBindingSource1.DataSource = this.dSPesees;
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnNew.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnNew.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnNew.Location = new System.Drawing.Point(81, 132);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 30);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "Nouveau";
            this.btnNew.UseVisualStyleBackColor = false;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnSupprimer.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnSupprimer.Location = new System.Drawing.Point(244, 132);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 6;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(163, 132);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rES_DEFAULTCheckBox);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.aDR_IDComboBox);
            this.groupBox1.Controls.Add(aDR_IDLabel);
            this.groupBox1.Controls.Add(rES_NOMLabel);
            this.groupBox1.Controls.Add(this.rES_NOMTextBox);
            this.groupBox1.Controls.Add(rES_PRENOMLabel);
            this.groupBox1.Controls.Add(this.rES_PRENOMTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(325, 168);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des responsables";
            // 
            // rES_DEFAULTCheckBox
            // 
            this.rES_DEFAULTCheckBox.AutoSize = true;
            this.rES_DEFAULTCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.rES_RESPONSABLEBindingSource, "RES_DEFAULT", true));
            this.rES_DEFAULTCheckBox.Location = new System.Drawing.Point(81, 111);
            this.rES_DEFAULTCheckBox.Name = "rES_DEFAULTCheckBox";
            this.rES_DEFAULTCheckBox.Size = new System.Drawing.Size(15, 14);
            this.rES_DEFAULTCheckBox.TabIndex = 29;
            this.rES_DEFAULTCheckBox.UseVisualStyleBackColor = true;
            // 
            // adresseCompletTableAdapter
            // 
            this.adresseCompletTableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesResponsables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(734, 464);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rES_IDTextBox);
            this.Controls.Add(this.rES_RESPONSABLEDataGridView);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(750, 503);
            this.Name = "WfrmGestionDesResponsables";
            this.Text = "Gestion des responsables";
            this.Load += new System.EventHandler(this.WfrmGestionDesResponsables_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rES_RESPONSABLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rES_RESPONSABLEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).EndInit();
            this.AdressesontextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource rES_RESPONSABLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.RES_RESPONSABLETableAdapter rES_RESPONSABLETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView rES_RESPONSABLEDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter aDR_ADRESSETableAdapter;
        private System.Windows.Forms.BindingSource aDRADRESSEBindingSource;
        private System.Windows.Forms.TextBox rES_IDTextBox;
        private System.Windows.Forms.TextBox rES_NOMTextBox;
        private System.Windows.Forms.TextBox rES_PRENOMTextBox;
        private System.Windows.Forms.ComboBox aDR_IDComboBox;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.BindingSource aDRADRESSEBindingSource1;
        private System.Windows.Forms.ContextMenuStrip AdressesontextMenuStrip;
        private System.Windows.Forms.BindingSource adresseCompletBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter adresseCompletTableAdapter;
		private System.Windows.Forms.ToolStripMenuItem AdresseMenuItem;
		private System.Windows.Forms.CheckBox rES_DEFAULTCheckBox;
		private System.Windows.Forms.DataGridViewTextBoxColumn RES_ID;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewComboBoxColumn ADR_ID;
		private System.Windows.Forms.DataGridViewCheckBoxColumn RES_DEFAULT;
    }
}