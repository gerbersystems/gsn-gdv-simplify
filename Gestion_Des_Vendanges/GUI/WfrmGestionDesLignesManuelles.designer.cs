﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesLignesManuelles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label cEP_NOMLabel;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIM_LIGNE_PAIEMENT_MANUELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.pAI_PAIEMENTTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAI_PAIEMENTTableAdapter();
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAI_TEXTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lIM_IDTextBox = new System.Windows.Forms.TextBox();
            this.lIM_TEXTETextBox = new System.Windows.Forms.TextBox();
            this.lIM_MONTANTTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMontantTotal = new System.Windows.Forms.TextBox();
            this.txtPmtNumero = new System.Windows.Forms.TextBox();
            this.pAI_IDTextBox = new System.Windows.Forms.TextBox();
            this.numPourcentage = new System.Windows.Forms.NumericUpDown();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pAIPAIEMENTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lipTable = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIP_LIGNE_PAIEMENT_PESEETableAdapter();
            label1 = new System.Windows.Forms.Label();
            cEP_NOMLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAIPAIEMENTBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(5, 67);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(40, 13);
            label1.TabIndex = 18;
            label1.Text = "Texte :";
            // 
            // cEP_NOMLabel
            // 
            cEP_NOMLabel.AutoSize = true;
            cEP_NOMLabel.Location = new System.Drawing.Point(5, 119);
            cEP_NOMLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            cEP_NOMLabel.Name = "cEP_NOMLabel";
            cEP_NOMLabel.Size = new System.Drawing.Size(74, 13);
            cEP_NOMLabel.TabIndex = 16;
            cEP_NOMLabel.Text = "Pourcentage :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(5, 145);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(52, 13);
            label2.TabIndex = 20;
            label2.Text = "Montant :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(5, 92);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(75, 13);
            label3.TabIndex = 22;
            label3.Text = "Montant total :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(5, 40);
            label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(70, 13);
            label4.TabIndex = 24;
            label4.Text = "Paiement n° :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.Locale = new System.Globalization.CultureInfo("fr-CH");
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lIM_LIGNE_PAIEMENT_MANUELLEBindingSource
            // 
            this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource.DataMember = "LIM_LIGNE_PAIEMENT_MANUELLE";
            this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource.DataSource = this.dSPesees;
            // 
            // lIM_LIGNE_PAIEMENT_MANUELLETableAdapter
            // 
            this.lIM_LIGNE_PAIEMENT_MANUELLETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = this.lIM_LIGNE_PAIEMENT_MANUELLETableAdapter;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = this.pAI_PAIEMENTTableAdapter;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // pAI_PAIEMENTTableAdapter
            // 
            this.pAI_PAIEMENTTableAdapter.ClearBeforeFill = true;
            // 
            // lIM_LIGNE_PAIEMENT_MANUELLEDataGridView
            // 
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.AllowUserToAddRows = false;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.AllowUserToDeleteRows = false;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.AutoGenerateColumns = false;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.PAI_TEXTE});
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.DataSource = this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.Location = new System.Drawing.Point(383, 12);
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.Name = "lIM_LIGNE_PAIEMENT_MANUELLEDataGridView";
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.ReadOnly = true;
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.Size = new System.Drawing.Size(353, 286);
            this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "LIM_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "LIM_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PAI_ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "PAI_ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // PAI_TEXTE
            // 
            this.PAI_TEXTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAI_TEXTE.DataPropertyName = "LIM_TEXTE";
            this.PAI_TEXTE.HeaderText = "Texte";
            this.PAI_TEXTE.MinimumWidth = 150;
            this.PAI_TEXTE.Name = "PAI_TEXTE";
            this.PAI_TEXTE.ReadOnly = true;
            // 
            // lIM_IDTextBox
            // 
            this.lIM_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource, "LIM_ID", true));
            this.lIM_IDTextBox.Location = new System.Drawing.Point(145, 10);
            this.lIM_IDTextBox.Name = "lIM_IDTextBox";
            this.lIM_IDTextBox.Size = new System.Drawing.Size(86, 20);
            this.lIM_IDTextBox.TabIndex = 3;
            this.lIM_IDTextBox.Text = "LIM_ID";
            // 
            // lIM_TEXTETextBox
            // 
            this.lIM_TEXTETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource, "LIM_TEXTE", true));
            this.lIM_TEXTETextBox.Location = new System.Drawing.Point(115, 63);
            this.lIM_TEXTETextBox.Name = "lIM_TEXTETextBox";
            this.lIM_TEXTETextBox.Size = new System.Drawing.Size(244, 20);
            this.lIM_TEXTETextBox.TabIndex = 5;
            // 
            // lIM_MONTANTTextBox
            // 
            this.lIM_MONTANTTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource, "LIM_MONTANT_HT", true));
            this.lIM_MONTANTTextBox.Location = new System.Drawing.Point(115, 141);
            this.lIM_MONTANTTextBox.Name = "lIM_MONTANTTextBox";
            this.lIM_MONTANTTextBox.Size = new System.Drawing.Size(244, 20);
            this.lIM_MONTANTTextBox.TabIndex = 7;
            this.lIM_MONTANTTextBox.Validated += new System.EventHandler(this.LIM_MONTANTTextBox_Validated);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMontantTotal);
            this.groupBox1.Controls.Add(this.txtPmtNumero);
            this.groupBox1.Controls.Add(this.pAI_IDTextBox);
            this.groupBox1.Controls.Add(this.numPourcentage);
            this.groupBox1.Controls.Add(label4);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.lIM_IDTextBox);
            this.groupBox1.Controls.Add(this.lIM_TEXTETextBox);
            this.groupBox1.Controls.Add(this.lIM_MONTANTTextBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(cEP_NOMLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 224);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des lignes manuelles";
            // 
            // txtMontantTotal
            // 
            this.txtMontantTotal.Location = new System.Drawing.Point(115, 88);
            this.txtMontantTotal.Name = "txtMontantTotal";
            this.txtMontantTotal.ReadOnly = true;
            this.txtMontantTotal.Size = new System.Drawing.Size(244, 20);
            this.txtMontantTotal.TabIndex = 29;
            // 
            // txtPmtNumero
            // 
            this.txtPmtNumero.Location = new System.Drawing.Point(115, 37);
            this.txtPmtNumero.Name = "txtPmtNumero";
            this.txtPmtNumero.ReadOnly = true;
            this.txtPmtNumero.Size = new System.Drawing.Size(244, 20);
            this.txtPmtNumero.TabIndex = 28;
            // 
            // pAI_IDTextBox
            // 
            this.pAI_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource, "PAI_ID", true));
            this.pAI_IDTextBox.Location = new System.Drawing.Point(237, 10);
            this.pAI_IDTextBox.Name = "pAI_IDTextBox";
            this.pAI_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.pAI_IDTextBox.TabIndex = 27;
            // 
            // numPourcentage
            // 
            this.numPourcentage.DecimalPlaces = 2;
            this.numPourcentage.Location = new System.Drawing.Point(115, 115);
            this.numPourcentage.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPourcentage.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numPourcentage.Name = "numPourcentage";
            this.numPourcentage.Size = new System.Drawing.Size(244, 20);
            this.numPourcentage.TabIndex = 26;
            this.numPourcentage.ValueChanged += new System.EventHandler(this.NumPourcentage_ValueChanged);
            this.numPourcentage.EnabledChanged += new System.EventHandler(this.LIM_MONTANTTextBox_Validated);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(285, 189);
            this.btnSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(206, 189);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAdd.Location = new System.Drawing.Point(127, 189);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 30);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Nouveau";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // pAIPAIEMENTBindingSource
            // 
            this.pAIPAIEMENTBindingSource.DataMember = "PAI_PAIEMENT";
            this.pAIPAIEMENTBindingSource.DataSource = this.dSPesees;
            // 
            // lipTable
            // 
            this.lipTable.ClearBeforeFill = true;
            // 
            // WfrmGestionDesLignesManuelles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 310);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView);
            this.MinimumSize = new System.Drawing.Size(656, 344);
            this.Name = "WfrmGestionDesLignesManuelles";
            this.Text = "Gestion des lignes manuelles";
            this.Load += new System.EventHandler(this.WfrmGestionDesLignesManuelles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIM_LIGNE_PAIEMENT_MANUELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIM_LIGNE_PAIEMENT_MANUELLEDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAIPAIEMENTBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource lIM_LIGNE_PAIEMENT_MANUELLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter lIM_LIGNE_PAIEMENT_MANUELLETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView lIM_LIGNE_PAIEMENT_MANUELLEDataGridView;
        private System.Windows.Forms.TextBox lIM_IDTextBox;
        private System.Windows.Forms.TextBox lIM_TEXTETextBox;
        private System.Windows.Forms.TextBox lIM_MONTANTTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAI_PAIEMENTTableAdapter pAI_PAIEMENTTableAdapter;
        private System.Windows.Forms.BindingSource pAIPAIEMENTBindingSource;
        private System.Windows.Forms.NumericUpDown numPourcentage;
        private System.Windows.Forms.TextBox pAI_IDTextBox;
        private System.Windows.Forms.TextBox txtMontantTotal;
        private System.Windows.Forms.TextBox txtPmtNumero;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIP_LIGNE_PAIEMENT_PESEETableAdapter lipTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_TEXTE;
        //private System.Windows.Forms.DataGridViewTextBoxColumn PAI_MONTANT;
    }
}