﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesRegions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cRU_NOMLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.rEG_REGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.rEG_REGIONDataGridView = new System.Windows.Forms.DataGridView();
            this.REG_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rEG_IDTextBox = new System.Windows.Forms.TextBox();
            this.rEG_NAMETextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            cRU_NOMLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEG_REGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEG_REGIONDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cRU_NOMLabel
            // 
            cRU_NOMLabel.AutoSize = true;
            cRU_NOMLabel.Location = new System.Drawing.Point(6, 43);
            cRU_NOMLabel.Name = "cRU_NOMLabel";
            cRU_NOMLabel.Size = new System.Drawing.Size(96, 13);
            cRU_NOMLabel.TabIndex = 19;
            cRU_NOMLabel.Text = "Nom de la région : ";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rEG_REGIONBindingSource
            // 
            this.rEG_REGIONBindingSource.DataMember = "REG_REGION";
            this.rEG_REGIONBindingSource.DataSource = this.dSPesees;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = this.rEG_REGIONTableAdapter;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // rEG_REGIONDataGridView
            // 
            this.rEG_REGIONDataGridView.AllowUserToAddRows = false;
            this.rEG_REGIONDataGridView.AllowUserToDeleteRows = false;
            this.rEG_REGIONDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rEG_REGIONDataGridView.AutoGenerateColumns = false;
            this.rEG_REGIONDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.rEG_REGIONDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rEG_REGIONDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.REG_ID,
            this.dataGridViewTextBoxColumn2});
            this.rEG_REGIONDataGridView.DataSource = this.rEG_REGIONBindingSource;
            this.rEG_REGIONDataGridView.Location = new System.Drawing.Point(396, 12);
            this.rEG_REGIONDataGridView.Name = "rEG_REGIONDataGridView";
            this.rEG_REGIONDataGridView.ReadOnly = true;
            this.rEG_REGIONDataGridView.RowTemplate.Height = 24;
            this.rEG_REGIONDataGridView.Size = new System.Drawing.Size(260, 313);
            this.rEG_REGIONDataGridView.TabIndex = 19;
            // 
            // REG_ID
            // 
            this.REG_ID.DataPropertyName = "REG_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.REG_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.REG_ID.HeaderText = "Id";
            this.REG_ID.Name = "REG_ID";
            this.REG_ID.ReadOnly = true;
            this.REG_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "REG_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Région";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // rEG_IDTextBox
            // 
            this.rEG_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.rEG_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rEG_REGIONBindingSource, "REG_ID", true));
            this.rEG_IDTextBox.Enabled = false;
            this.rEG_IDTextBox.Location = new System.Drawing.Point(462, 198);
            this.rEG_IDTextBox.Name = "rEG_IDTextBox";
            this.rEG_IDTextBox.Size = new System.Drawing.Size(118, 19);
            this.rEG_IDTextBox.TabIndex = 21;
            // 
            // rEG_NAMETextBox
            // 
            this.rEG_NAMETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rEG_REGIONBindingSource, "REG_NAME", true));
            this.rEG_NAMETextBox.Location = new System.Drawing.Point(135, 40);
            this.rEG_NAMETextBox.Name = "rEG_NAMETextBox";
            this.rEG_NAMETextBox.Size = new System.Drawing.Size(237, 19);
            this.rEG_NAMETextBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(cRU_NOMLabel);
            this.groupBox1.Controls.Add(this.rEG_NAMETextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 124);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des régions";
            // 
            // btnNew
            // 
            this.btnNew.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnNew.Location = new System.Drawing.Point(135, 88);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 30);
            this.btnNew.TabIndex = 1;
            this.btnNew.Text = "Nouveau";
            this.btnNew.UseVisualStyleBackColor = false;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(297, 88);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(216, 88);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // WfrmGestionDesRegions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(668, 337);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rEG_IDTextBox);
            this.Controls.Add(this.rEG_REGIONDataGridView);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(620, 300);
            this.Name = "WfrmGestionDesRegions";
            this.Text = "Gestion des régions";
            this.Load += new System.EventHandler(this.WfrmGestionDesRegions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEG_REGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEG_REGIONDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource rEG_REGIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView rEG_REGIONDataGridView;
        private System.Windows.Forms.TextBox rEG_IDTextBox;
        private System.Windows.Forms.TextBox rEG_NAMETextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn REG_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}