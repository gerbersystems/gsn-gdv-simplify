﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Data;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class FormSettingsManagerAnnee : FormSettingsManagerRefAdresse
    {
        private MOC_MODE_COMPTABILISATIONTableAdapter _mocTable;
        private PAI_PAIEMENTTableAdapter _paiTable;
        private RadioButton _optLitre;
        private RadioButton _optKg;
        private TextBox _idTexteBox;
        private ANN_ANNEETableAdapter _annTable;
        private ComboBox _cbModeComptabilisation;

        private int _AnnId
        {
            get { return Convert.ToInt32(_idTexteBox.Text); }
        }

        public FormSettingsManagerAnnee(WfrmSetting form,
                                        TextBox idTextBox,
                                        Control focusControl,
                                        Button btnNew,
                                        Button btnUpdate,
                                        Button btnDelete,
                                        DataGridView dataGridView,
                                        BindingSource bindingSource,
                                        TableAdapterManager tableAdapterManager,
                                        DSPesees dsPesees,
                                        MOC_MODE_COMPTABILISATIONTableAdapter mocTable,
                                        ANN_ANNEETableAdapter annTable,
                                        ComboBox cbModeComptabilisation,
                                        RadioButton optKg,
                                        RadioButton optLitre,
                                        AdresseCompletTableAdapter adrCompletTable)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dsPesees, adrCompletTable)
        {
            _idTexteBox = idTextBox;
            _mocTable = mocTable;
            _annTable = annTable;
            _paiTable = ConnectionManager.Instance.CreateGvTableAdapter<PAI_PAIEMENTTableAdapter>();
            DataGridView.SelectionChanged += dataGridViewSelectionChanged;
            _cbModeComptabilisation = cbModeComptabilisation;
            _optKg = optKg;
            _optLitre = optLitre;
            _optKg.CheckedChanged += optKg_CheckedChanged;
        }

        private void dataGridViewSelectionChanged(object sender, EventArgs e)
        {
            int? mocId = _annTable.GetMocId(_AnnId);

            _mocTable.FillByAnnId(dataTable: DataSet.MOC_MODE_COMPTABILISATION,
                                 ANN_ID: (_AnnId <= 0) ? Convert.ToInt32(_annTable.GetIdMaxAn()) : _AnnId);

            bool found = false;
            for (int i = 0; i < _cbModeComptabilisation.Items.Count && !found; i++)
            {
                var row = (DSPesees.MOC_MODE_COMPTABILISATIONRow)((DataRowView)_cbModeComptabilisation.Items[i]).Row;

                if (mocId == row.MOC_ID)
                {
                    _cbModeComptabilisation.SelectedItem = row;
                    _cbModeComptabilisation.SelectedValue = mocId;
                    found = true;
                }
            }
        }

        protected override void ChangeEditMode(bool editMode)
        {
            base.ChangeEditMode(editMode);
            if (editMode && (int)_paiTable.GetNbByAnnId(_AnnId) != 0)
            {
                _optLitre.Enabled = false;
                _optKg.Enabled = false;
            }
        }

        private void optKg_CheckedChanged(object sender, EventArgs e)
        {
            _optLitre.Checked = !_optKg.Checked;
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                int idMaxAn = 0;
                if (IsNew)
                {
                    try
                    {
                        idMaxAn = Convert.ToInt32(_annTable.GetIdMaxAn());
                    }
                    catch
                    {
                    }
                }

                base.BtnOk_Click(sender, e);

                if (IsNew)
                {
                    CopieAcquits(idMaxAn);
                    CopieBonus(idMaxAn);
                    CopieModeComptabilisation(idMaxAn);
                }

                UpdateIsPrixKG();
            }
            catch (Exception)
            {
                MessageBox.Show("Erreur lors de l'enregistrement.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void UpdateIsPrixKG()
        {
            _annTable.SetIsPrixKg(_optKg.Checked, _AnnId);
            DataGridView.SelectionChanged -= dataGridViewSelectionChanged;
            _annTable.Fill(DataSet.ANN_ANNEE);
            DataGridView.SelectionChanged += dataGridViewSelectionChanged;
        }

        private void CopieModeComptabilisation(int idMaxAn)
        {
            var mocTable = ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
            mocTable.FillByAnnId(DataSet.MOC_MODE_COMPTABILISATION, idMaxAn);
            foreach (DSPesees.MOC_MODE_COMPTABILISATIONRow mocRow in mocTable.GetDataByAnnId(idMaxAn))
            {
                mocTable.Insert(MOC_ID_WINBIZ: mocRow.MOC_ID_WINBIZ,
                                MOC_TEXTE: mocRow.MOC_TEXTE,
                                MOC_TAUX_TVA: mocRow.MOC_TAUX_TVA,
                                MOD_ID: mocRow.MOD_ID,
                                ANN_ID: _AnnId,
                                MOC_NO_COMPTE_COMPTABLE: mocRow.MOC_NO_COMPTE_COMPTABLE,
                                MOC_DEBIT_CREDIT: mocRow.MOC_DEBIT_CREDIT);
            }

            CopieModeComptablisationCouleur(idMaxAn, mocTable);

            var mceTable = ConnectionManager.Instance.CreateGvTableAdapter<MCE_MOC_CEPTableAdapter>();
            mceTable.FillByAnnId(DataSet.MCE_MOC_CEP, idMaxAn);

            foreach (DSPesees.MCE_MOC_CEPRow mceRow in mceTable.GetDataByAnnId(idMaxAn))
            {
                int mocId = Convert.ToInt32(mocTable.GetMocId(Convert.ToInt32(mocTable.GetMocIdWinBiz(mceRow.MOC_ID)), _AnnId));
                mceTable.Insert(MOC_ID: mocId,
                                CEP_ID: mceRow.CEP_ID);
            }

            //Ajustement du mode de comptabilisation de la nouvelle année
            var mocIdOld = Convert.ToInt32(_annTable.GetMocId(_AnnId));
            if (mocIdOld > 0)
            {
                int mocIdNew = Convert.ToInt32(mocTable.GetMocId(Convert.ToInt32(mocTable.GetMocIdWinBiz(mocIdOld)), _AnnId));
                _annTable.SetMocId(MOC_IDD: mocIdNew,
                                   ANN_ID: _AnnId);
            }
        }

        private void CopieBonus(int idMaxAn)
        {
            var valTable = ConnectionManager.Instance.CreateGvTableAdapter<VAL_VALEUR_CEPAGETableAdapter>();
            valTable.FillByYear(DataSet.VAL_VALEUR_CEPAGE, idMaxAn);

            foreach (DSPesees.VAL_VALEUR_CEPAGERow valRow in valTable.GetDataByYear(idMaxAn))
            {
                valTable.Insert(CEP_ID: valRow.CEP_ID,
                                ANN_ID: _AnnId,
                                VAL_VALEUR: (valRow.IsVAL_VALEURNull() ? 0 : valRow.VAL_VALEUR),
                                LIE_ID: valRow.LIE_ID,
                                VAL_OECHSLE_MOYEN: (valRow.IsVAL_OECHSLE_MOYENNull() ? 0 : valRow.VAL_OECHSLE_MOYEN),
                                VAL_BON_TYPE: valRow.VAL_BON_TYPE,
                                CLA_ID: valRow.CLA_ID);

                InsertBonus(valeurCepageMaxId: (int)valTable.GetMaxValId(), valeurCepageId: valRow.VAL_ID);
            }
        }

        private void InsertBonus(int valeurCepageMaxId, int valeurCepageId)
        {
            var bonusTable = ConnectionManager.Instance.CreateGvTableAdapter<BON_BONUSTableAdapter>();
            bonusTable.FillByVal(DataSet.BON_BONUS, valeurCepageId);

            foreach (DSPesees.BON_BONUSRow bonRow in bonusTable.GetDataByVal(valeurCepageId))
            {
                bonusTable.Insert(VAL_ID: valeurCepageMaxId,
                                  BON_OE: bonRow.BON_OE,
                                  BON_BRIX: bonRow.BON_BRIX,
                                  BON_BONUS: bonRow.BON_BONUS);
            }
        }
    }
}