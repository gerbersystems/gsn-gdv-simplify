﻿namespace Gestion_Des_Vendanges.GUI
{
	partial class CtrlGestionDesAcquits
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.aCQ_ACQUITDataGridView = new System.Windows.Forms.DataGridView();
            this.ACQ_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.PRO_ID_VENDANGE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pRONOMCOMPLETBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.CLA_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cLACLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.REG_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.rEGREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.COM_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.COU_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cOUCOULEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ACQ_SURFACE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_LITRES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kilos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_NUMERO_COMPL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACQ_RECEPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aCQ_ACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_PARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.cOU_COULEURTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter();
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.rEG_REGIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.acquitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesAcquitsDeVendangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrintProduction = new System.Windows.Forms.ToolStripButton();
            this.toolVisit = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "Gestion des acquits";
            // 
            // aCQ_ACQUITDataGridView
            // 
            this.aCQ_ACQUITDataGridView.AllowUserToAddRows = false;
            this.aCQ_ACQUITDataGridView.AllowUserToDeleteRows = false;
            this.aCQ_ACQUITDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aCQ_ACQUITDataGridView.AutoGenerateColumns = false;
            this.aCQ_ACQUITDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aCQ_ACQUITDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aCQ_ACQUITDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACQ_ID,
            this.ANN_ID,
            this.ACQ_NUMERO,
            this.ACQ_DATE,
            this.PRO_ID,
            this.PRO_ID_VENDANGE,
            this.CLA_ID,
            this.REG_ID,
            this.COM_ID,
            this.COU_ID,
            this.ACQ_SURFACE,
            this.ACQ_LITRES,
            this.Kilos,
            this.ACQ_NUMERO_COMPL,
            this.ACQ_RECEPTION});
            this.aCQ_ACQUITDataGridView.DataSource = this.aCQ_ACQUITBindingSource;
            this.aCQ_ACQUITDataGridView.Location = new System.Drawing.Point(3, 55);
            this.aCQ_ACQUITDataGridView.MultiSelect = false;
            this.aCQ_ACQUITDataGridView.Name = "aCQ_ACQUITDataGridView";
            this.aCQ_ACQUITDataGridView.ReadOnly = true;
            this.aCQ_ACQUITDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.aCQ_ACQUITDataGridView.Size = new System.Drawing.Size(885, 412);
            this.aCQ_ACQUITDataGridView.TabIndex = 5;
            this.aCQ_ACQUITDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ACQ_ACQUITDataGridView_CellFormatting);
            this.aCQ_ACQUITDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ACQ_ACQUITDataGridView_ColumnHeaderMouseClick);
            // 
            // ACQ_ID
            // 
            this.ACQ_ID.DataPropertyName = "ACQ_ID";
            this.ACQ_ID.FillWeight = 40F;
            this.ACQ_ID.HeaderText = "ID";
            this.ACQ_ID.Name = "ACQ_ID";
            this.ACQ_ID.ReadOnly = true;
            this.ACQ_ID.Visible = false;
            this.ACQ_ID.Width = 40;
            // 
            // ANN_ID
            // 
            this.ANN_ID.DataPropertyName = "ANN_ID";
            this.ANN_ID.HeaderText = "Année";
            this.ANN_ID.Name = "ANN_ID";
            this.ANN_ID.ReadOnly = true;
            this.ANN_ID.Visible = false;
            // 
            // ACQ_NUMERO
            // 
            this.ACQ_NUMERO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACQ_NUMERO.DataPropertyName = "ACQ_NUMERO";
            this.ACQ_NUMERO.FillWeight = 6.191064F;
            this.ACQ_NUMERO.HeaderText = "Numéro";
            this.ACQ_NUMERO.Name = "ACQ_NUMERO";
            this.ACQ_NUMERO.ReadOnly = true;
            // 
            // ACQ_DATE
            // 
            this.ACQ_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACQ_DATE.DataPropertyName = "ACQ_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.ACQ_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.ACQ_DATE.FillWeight = 9.985587F;
            this.ACQ_DATE.HeaderText = "Date";
            this.ACQ_DATE.Name = "ACQ_DATE";
            this.ACQ_DATE.ReadOnly = true;
            // 
            // PRO_ID
            // 
            this.PRO_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_ID.DataPropertyName = "PRO_ID";
            this.PRO_ID.DataSource = this.pRONOMCOMPLETBindingSource;
            this.PRO_ID.DisplayMember = "NOMCOMPLET";
            this.PRO_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PRO_ID.FillWeight = 13.87997F;
            this.PRO_ID.HeaderText = "Propriétaire";
            this.PRO_ID.Name = "PRO_ID";
            this.PRO_ID.ReadOnly = true;
            this.PRO_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PRO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PRO_ID.ValueMember = "PRO_ID";
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PRO_ID_VENDANGE
            // 
            this.PRO_ID_VENDANGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_ID_VENDANGE.DataPropertyName = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.DataSource = this.pRONOMCOMPLETBindingSource1;
            this.PRO_ID_VENDANGE.DisplayMember = "NOMCOMPLET";
            this.PRO_ID_VENDANGE.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PRO_ID_VENDANGE.FillWeight = 13.87997F;
            this.PRO_ID_VENDANGE.HeaderText = "Producteur";
            this.PRO_ID_VENDANGE.Name = "PRO_ID_VENDANGE";
            this.PRO_ID_VENDANGE.ReadOnly = true;
            this.PRO_ID_VENDANGE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PRO_ID_VENDANGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PRO_ID_VENDANGE.ValueMember = "PRO_ID";
            // 
            // pRONOMCOMPLETBindingSource1
            // 
            this.pRONOMCOMPLETBindingSource1.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource1.DataSource = this.dSPesees;
            // 
            // CLA_ID
            // 
            this.CLA_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CLA_ID.DataPropertyName = "CLA_ID";
            this.CLA_ID.DataSource = this.cLACLASSEBindingSource;
            this.CLA_ID.DisplayMember = "CLA_NOM";
            this.CLA_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CLA_ID.FillWeight = 16.91559F;
            this.CLA_ID.HeaderText = "Classe";
            this.CLA_ID.Name = "CLA_ID";
            this.CLA_ID.ReadOnly = true;
            this.CLA_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CLA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CLA_ID.ValueMember = "CLA_ID";
            // 
            // cLACLASSEBindingSource
            // 
            this.cLACLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLACLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // REG_ID
            // 
            this.REG_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.REG_ID.DataPropertyName = "REG_ID";
            this.REG_ID.DataSource = this.rEGREGIONBindingSource;
            this.REG_ID.DisplayMember = "REG_NAME";
            this.REG_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.REG_ID.FillWeight = 13.87997F;
            this.REG_ID.HeaderText = "Région";
            this.REG_ID.Name = "REG_ID";
            this.REG_ID.ReadOnly = true;
            this.REG_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REG_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.REG_ID.ValueMember = "REG_ID";
            // 
            // rEGREGIONBindingSource
            // 
            this.rEGREGIONBindingSource.DataMember = "REG_REGION";
            this.rEGREGIONBindingSource.DataSource = this.dSPesees;
            // 
            // COM_ID
            // 
            this.COM_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.COM_ID.DataPropertyName = "COM_ID";
            this.COM_ID.DataSource = this.cOMCOMMUNEBindingSource;
            this.COM_ID.DisplayMember = "COM_NOM";
            this.COM_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.COM_ID.FillWeight = 13.87997F;
            this.COM_ID.HeaderText = "Commune";
            this.COM_ID.Name = "COM_ID";
            this.COM_ID.ReadOnly = true;
            this.COM_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.COM_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.COM_ID.ValueMember = "COM_ID";
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // COU_ID
            // 
            this.COU_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.COU_ID.DataPropertyName = "COU_ID";
            this.COU_ID.DataSource = this.cOUCOULEURBindingSource;
            this.COU_ID.DisplayMember = "COU_NOM";
            this.COU_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.COU_ID.FillWeight = 7.289479F;
            this.COU_ID.HeaderText = "Couleur";
            this.COU_ID.Name = "COU_ID";
            this.COU_ID.ReadOnly = true;
            this.COU_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.COU_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.COU_ID.ValueMember = "COU_ID";
            // 
            // cOUCOULEURBindingSource
            // 
            this.cOUCOULEURBindingSource.DataMember = "COU_COULEUR";
            this.cOUCOULEURBindingSource.DataSource = this.dSPesees;
            // 
            // ACQ_SURFACE
            // 
            this.ACQ_SURFACE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACQ_SURFACE.DataPropertyName = "ACQ_SURFACE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = "0";
            this.ACQ_SURFACE.DefaultCellStyle = dataGridViewCellStyle2;
            this.ACQ_SURFACE.FillWeight = 7.888614F;
            this.ACQ_SURFACE.HeaderText = "Surface";
            this.ACQ_SURFACE.Name = "ACQ_SURFACE";
            this.ACQ_SURFACE.ReadOnly = true;
            // 
            // ACQ_LITRES
            // 
            this.ACQ_LITRES.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACQ_LITRES.DataPropertyName = "ACQ_LITRES";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = "0";
            this.ACQ_LITRES.DefaultCellStyle = dataGridViewCellStyle3;
            this.ACQ_LITRES.FillWeight = 6.790199F;
            this.ACQ_LITRES.HeaderText = "Droit(L)";
            this.ACQ_LITRES.Name = "ACQ_LITRES";
            this.ACQ_LITRES.ReadOnly = true;
            // 
            // Kilos
            // 
            this.Kilos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Kilos.DefaultCellStyle = dataGridViewCellStyle4;
            this.Kilos.FillWeight = 6.959606F;
            this.Kilos.HeaderText = "Droit(Kg)";
            this.Kilos.Name = "Kilos";
            this.Kilos.ReadOnly = true;
            // 
            // ACQ_NUMERO_COMPL
            // 
            this.ACQ_NUMERO_COMPL.DataPropertyName = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.HeaderText = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.Name = "ACQ_NUMERO_COMPL";
            this.ACQ_NUMERO_COMPL.ReadOnly = true;
            this.ACQ_NUMERO_COMPL.Visible = false;
            // 
            // ACQ_RECEPTION
            // 
            this.ACQ_RECEPTION.DataPropertyName = "ACQ_RECEPTION";
            dataGridViewCellStyle5.Format = "Oui";
            dataGridViewCellStyle5.NullValue = "Non";
            this.ACQ_RECEPTION.DefaultCellStyle = dataGridViewCellStyle5;
            this.ACQ_RECEPTION.HeaderText = "Reçu";
            this.ACQ_RECEPTION.Name = "ACQ_RECEPTION";
            this.ACQ_RECEPTION.ReadOnly = true;
            this.ACQ_RECEPTION.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ACQ_RECEPTION.Width = 45;
            // 
            // aCQ_ACQUITBindingSource
            // 
            this.aCQ_ACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQ_ACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_PARCELLEBindingSource
            // 
            this.pAR_PARCELLEBindingSource.DataMember = "FK_PAR_PARC_CONTIENT_ACQ_ACQU";
            this.pAR_PARCELLEBindingSource.DataSource = this.aCQ_ACQUITBindingSource;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = this.aCQ_ACQUITTableAdapter;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = this.pAR_PARCELLETableAdapter;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // cOU_COULEURTableAdapter
            // 
            this.cOU_COULEURTableAdapter.ClearBeforeFill = true;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // rEG_REGIONTableAdapter
            // 
            this.rEG_REGIONTableAdapter.ClearBeforeFill = true;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbAnnee);
            this.panel1.Location = new System.Drawing.Point(99, 149);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 211);
            this.panel1.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 24);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            this.cbAnnee.SelectedIndexChanged += new System.EventHandler(this.CbAnnee_SelectedIndexChanged);
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // toolAdd
            // 
            this.toolAdd.Image = global::Gestion_Des_Vendanges.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(82, 24);
            this.toolAdd.Text = "Ajouter";
            this.toolAdd.Click += new System.EventHandler(this.BtnAddAcquit_Click);
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = global::Gestion_Des_Vendanges.Properties.Resources.open;
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(90, 24);
            this.toolUpdate.Text = "Modifier";
            this.toolUpdate.Click += new System.EventHandler(this.BtnModifyAcquit_Click);
            // 
            // toolDelete
            // 
            this.toolDelete.Image = global::Gestion_Des_Vendanges.Properties.Resources.delete;
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(102, 24);
            this.toolDelete.Text = "Supprimer";
            this.toolDelete.Click += new System.EventHandler(this.BtnDeleteAcquit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acquitsToolStripMenuItem,
            this.listeDesAcquitsDeVendangeToolStripMenuItem,
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem});
            this.toolStripButton1.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(97, 24);
            this.toolStripButton1.Text = "Acquits";
            // 
            // acquitsToolStripMenuItem
            // 
            this.acquitsToolStripMenuItem.Name = "acquitsToolStripMenuItem";
            this.acquitsToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.acquitsToolStripMenuItem.Text = "Acquits";
            this.acquitsToolStripMenuItem.Click += new System.EventHandler(this.AcquitsToolStripMenuItem_Click);
            // 
            // listeDesAcquitsDeVendangeToolStripMenuItem
            // 
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Name = "listeDesAcquitsDeVendangeToolStripMenuItem";
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Text = "Liste des acquits de vendange";
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Click += new System.EventHandler(this.ListeDesAcquitsDeVendangeToolStripMenuItem_Click);
            // 
            // listeDesAcquitsDeVendangeParCépageToolStripMenuItem
            // 
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Name = "listeDesAcquitsDeVendangeParCépageToolStripMenuItem";
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Size = new System.Drawing.Size(361, 26);
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Text = "Liste des acquits de vendange par cépage";
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Click += new System.EventHandler(this.ListeDesAcquitsDeVendangeParCépageToolStripMenuItem_Click);
            // 
            // toolPrintProduction
            // 
            this.toolPrintProduction.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintProduction.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintProduction.Name = "toolPrintProduction";
            this.toolPrintProduction.Size = new System.Drawing.Size(171, 24);
            this.toolPrintProduction.Text = "Droits de production";
            this.toolPrintProduction.Click += new System.EventHandler(this.BtnDroitsProduction_Click);
            // 
            // toolVisit
            // 
            this.toolVisit.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolVisit.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolVisit.Name = "toolVisit";
            this.toolVisit.Size = new System.Drawing.Size(142, 24);
            this.toolVisit.Text = "Visite des vignes";
            this.toolVisit.Click += new System.EventHandler(this.BtnVisiteVignes_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.toolPrintProduction,
            this.toolVisit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(891, 27);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // CtrlGestionDesAcquits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.aCQ_ACQUITDataGridView);
            this.Controls.Add(this.label1);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CtrlGestionDesAcquits";
            this.Size = new System.Drawing.Size(891, 470);
            this.Load += new System.EventHandler(this.WfrmGestionDesAcquits_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cLACLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGREGIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOUCOULEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQ_ACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
	  //  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
	   // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
	   // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
		//  private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
	   // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
	   // private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
	  //  private System.Windows.Forms.BindingSource litres_Cat1_by_AcquitBindingSource2;
	   // private System.Windows.Forms.BindingSource litres_Cat2_by_AcquitBindingSource2;
		private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
		private System.Windows.Forms.BindingSource aCQ_ACQUITBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
		private System.Windows.Forms.DataGridView aCQ_ACQUITDataGridView;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
		private System.Windows.Forms.BindingSource pAR_PARCELLEBindingSource;
	 //   private System.Windows.Forms.BindingSource maxAnneeBindingSource;
		//    private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MaxAnneeTableAdapter maxAnneeTableAdapter;
		private System.Windows.Forms.BindingSource cLACLASSEBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
		private System.Windows.Forms.BindingSource cOUCOULEURBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COU_COULEURTableAdapter cOU_COULEURTableAdapter;
		private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
	   // private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
		private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
	 //   private System.Windows.Forms.BindingSource aPPAPPELLATIONBindingSource;
		//   private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
		private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
		private System.Windows.Forms.BindingSource rEGREGIONBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.REG_REGIONTableAdapter rEG_REGIONTableAdapter;
		private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox cbAnnee;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
		private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_DATE;
        private System.Windows.Forms.DataGridViewComboBoxColumn PRO_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn PRO_ID_VENDANGE;
        private System.Windows.Forms.DataGridViewComboBoxColumn CLA_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn REG_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn COM_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn COU_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_SURFACE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_LITRES;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kilos;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_NUMERO_COMPL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_RECEPTION;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem acquitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesAcquitsDeVendangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesAcquitsDeVendangeParCépageToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolPrintProduction;
        private System.Windows.Forms.ToolStripButton toolVisit;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}