﻿using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Data.Extensions;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des pesées
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class CtrlGestionDesPesees : CtrlWfrm
    {
        #region properties

        public override ToolStrip ToolStrip
        {
            get { return toolStrip1; }
        }

        public override Panel OptionPanel
        {
            get { return panel1; }
        }

        #endregion properties

        #region constructor

        private static CtrlGestionDesPesees _instance;

        public static CtrlGestionDesPesees GetInstance(bool firstLoad) => _instance = new CtrlGestionDesPesees(firstLoad);

        public static CtrlGestionDesPesees GetCurrentInstance(bool firstLoad) => _instance ?? new CtrlGestionDesPesees(firstLoad);

        private CtrlGestionDesPesees(bool firstLoad)
        {
            InitializeComponent();
            if (!firstLoad) WfrmGestionDesPesees_Load(null, null);
        }

        private void WfrmGestionDesPesees_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pEE_PESEE_ENTETETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rES_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pEE_PRODUCTEUR_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pEE_TOTAL_QUANTITETableAdapter);
            UpdateDonnees();

            pEE_PESEE_ENTETEDataGridView.MouseDoubleClick += BtnModifierPesee_Click;
            pEE_PESEE_ENTETEDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        #endregion constructor

        public override void UpdateDonnees()
        {
            try
            {
                aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
                cbAnnee.SelectedValue = BUSINESS.Utilities.IdAnneeCourante;
                aCQ_ACQUITTableAdapter.FillByAnnee(dSPesees.ACQ_ACQUIT, BUSINESS.Utilities.IdAnneeCourante);
                pEE_PESEE_ENTETETableAdapter.FillByYear(dSPesees.PEE_PESEE_ENTETE, BUSINESS.Utilities.IdAnneeCourante);
                lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
                aUT_AUTRE_MENTIONTableAdapter.Fill(dSPesees.AUT_AUTRE_MENTION);
                rES_NOMCOMPLETTableAdapter.Fill(dSPesees.RES_NOMCOMPLET);
                pEE_PRODUCTEUR_NOMCOMPLETTableAdapter.Fill(dSPesees.PEE_PRODUCTEUR_NOMCOMPLET);
                pEE_TOTAL_QUANTITETableAdapter.Fill(dSPesees.PEE_TOTAL_QUANTITE);

                PEE_ID_LITRES.Visible = BUSINESS.GlobalParam.Instance.ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre;
                PEE_ID_KG.Visible = PEE_ID_LITRES.Visible == false;

                switch (BUSINESS.GlobalParam.Instance.Canton)
                {
                    case SettingExtension.CantonEnum.Neuchâtel:
                        PEE_GRANDCRU.Visible = PEE_ID_LITRES.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private BUSINESS.EntetePesee DataGridViewCellCollectionToEntetePesee(DataGridViewCellCollection row)
        {
            BUSINESS.EntetePesee entetePesee = new BUSINESS.EntetePesee();
            entetePesee.AcquitId = (int)row["ACQ_ID"].Value;
            try
            {
                entetePesee.Date = (DateTime)row["PEE_DATE"].Value;
            }
            catch { }

            entetePesee.Lieu = row["PEE_LIEU"].Value.ToString();
            entetePesee.ResponsableId = (int)row["RES_ID"].Value;
            entetePesee.Id = (int)row["PEE_ID"].Value;
            entetePesee.AutreMentionId = (int)row["AUT_ID"].Value;
            entetePesee.Remark = row["PEE_REMARQUES"].Value.ToString();
            entetePesee.Divers1 = row["PEE_DIVERS1"].Value.ToString();
            entetePesee.Divers2 = row["PEE_DIVERS2"].Value.ToString();
            entetePesee.Divers3 = row["PEE_DIVERS3"].Value.ToString();

            switch (BUSINESS.GlobalParam.Instance.Canton)
            {
                case SettingExtension.CantonEnum.Vaud:
                    entetePesee.LieuProductionId = (int)row["LIE_ID"].Value;
                    try
                    {
                        entetePesee.GrandCru = (bool)row["PEE_GRANDCRU"].Value;
                    }
                    catch
                    {
                        entetePesee.GrandCru = false;
                    }
                    break;
            }
            return entetePesee;
        }

        public void UpdateEntetePesee(BUSINESS.EntetePesee entetePesee)
        {
            int id = -1;
            int idCurrentRow;
            UpdateDonnees();
            try
            {
                idCurrentRow = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
            }
            catch
            {
                idCurrentRow = -1;
            }
            if (entetePesee.Id != idCurrentRow)
            {
                for (int i = 0; i < pEE_PESEE_ENTETEDataGridView.RowCount && id < 0; i++)
                {
                    if (entetePesee.Id == (int)pEE_PESEE_ENTETEDataGridView["PEE_ID", i].Value)
                        id = i;
                }
                if (id >= 0 && id < pEE_PESEE_ENTETEDataGridView.Rows.Count)
                    UpdateRowAcquit(pEE_PESEE_ENTETEDataGridView.Rows[id].Cells, entetePesee);
            }
            else
            {
                UpdateRowAcquit(pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells, entetePesee);
            }
            SaveDataGrid();
        }

        private void SaveDataGrid()
        {
            Validate();
            pEE_PESEE_ENTETEBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(dSPesees);
        }

        private void UpdateRowAcquit(DataGridViewCellCollection row, BUSINESS.EntetePesee entetePesee)
        {
            row["ACQ_ID"].Value = entetePesee.AcquitId;
            row["PEE_DATE"].Value = entetePesee.Date;
            row["PEE_LIEU"].Value = entetePesee.Lieu;
            row["RES_ID"].Value = entetePesee.ResponsableId;
            row["PEE_GRANDCRU"].Value = entetePesee.GrandCru;
            row["AUT_ID"].Value = entetePesee.AutreMentionId;
            row["LIE_ID"].Value = entetePesee.LieuProductionId;
            row["PEE_REMARQUES"].Value = entetePesee.Remark;
            row["PEE_DIVERS1"].Value = entetePesee.Divers1;
            row["PEE_DIVERS2"].Value = entetePesee.Divers2;
            row["PEE_DIVERS3"].Value = entetePesee.Divers3;
        }
        private void PEE_PESEE_ENTETEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool isSorted = true;
            int idAn = BUSINESS.Utilities.IdAnneeCourante;

            switch (e.ColumnIndex)
            {
                case 2:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderAcq(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 3:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderPro(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 4:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderRes(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 5:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderLie(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 6:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderAut(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 7:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderKg(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                case 8:
                    pEE_PESEE_ENTETEBindingSource.RemoveSort();
                    pEE_PESEE_ENTETETableAdapter.FillOrderLitres(dSPesees.PEE_PESEE_ENTETE, idAn);
                    break;

                default:
                    isSorted = false;
                    break;
            }
            if (isSorted)
            {
                foreach (DataGridViewColumn column in pEE_PESEE_ENTETEDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                pEE_PESEE_ENTETEDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void CbAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAnnee.SelectedValue != null && Convert.ToInt32(cbAnnee.SelectedValue) != BUSINESS.Utilities.IdAnneeCourante)
            {
                BUSINESS.Utilities.IdAnneeCourante = Convert.ToInt32(cbAnnee.SelectedValue);
                UpdateDonnees();
            }
        }

        private void AvecDroitEnLitresDeMoûtDeRaisinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                int peeId = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                CtrlReportViewer.ShowApportsProducteurMoutRaisin(BUSINESS.Utilities.IdAnneeCourante, peeId);
            }
        }

        #region Button Events

        private void BtnAddAcquit_Click(object sender, EventArgs e)
        {
            WfrmSaisieEntetePesee formAddEntetePesee = new WfrmSaisieEntetePesee(this);
            formAddEntetePesee.WfrmShowDialog();
        }

        private void BtnModifierPesee_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEDataGridView.CurrentRow != null)
            {
                WfrmSaisieEntetePesee formEntetePesee = new WfrmSaisieEntetePesee(this, DataGridViewCellCollectionToEntetePesee(pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells));
                formEntetePesee.WfrmShowDialog();
            }
        }

        private void BtnDeleteAcquit_Click(object sender, EventArgs e)
        {
            try
            {
                int peeId = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                if (pEE_PESEE_ENTETETableAdapter.NbRef(peeId) > 0)
                {
                    MessageBox.Show("Vous ne pouvez pas supprimer cette attestation de contrôle car des pesées ont été payées.\nVeuillez supprimer les paiements pour dévérouiller l'attestation.", "Attestation vérouillée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    if (pEE_PESEE_ENTETEBindingSource.Current != null)
                    {
                        if (MessageBox.Show("Etes vous sûr de vouloir supprimer cette attestation de contrôle?", "Suppression d'une attestation de contrôle", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            pEE_PESEE_ENTETEBindingSource.RemoveCurrent();
                            SaveDataGrid();
                        }
                    }
                }
            }
            catch (NullReferenceException) { }
        }

        #endregion Button Events

        #region Button Attestation

        private void AttestationDeContrôleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                int id = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                CtrlReportViewer.ShowAttestationControle(id);
            }
        }

        private void AttestationDeContrôleV2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                if (aCQACQUITBindingSource.Current != null)
                { }

                int id = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                int producteurID = 0;
                producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);

                CtrlReportViewer.ShowAttestationControleV2(peeID: id, proID: producteurID, annID: BUSINESS.Utilities.IdAnneeCourante);
            }
        }

        private void AttestationDeContrôleAvecSoldeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                if (aCQACQUITBindingSource.Current != null)
                { }

                int id = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                int producteurID = 0;
                producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);

                CtrlReportViewer.ShowAttestationControleAvecSolde(peeID: id, proID: producteurID, annID: BUSINESS.Utilities.IdAnneeCourante);
            }
        }

        private void AttestationDeContrôleAvecSoldeParAcquitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                if (aCQACQUITBindingSource.Current != null)
                {
                    int id = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                    int acqid = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["ACQ_ID"].Value;
                    int producteurID = 0;

                    producteurID = Gestion_Des_Vendanges_Reports.BUSINESS.Reports_Utilities.getProducteurID(id);

                    CtrlReportViewer.ShowAttestationControleAvecSoldeParAcquit(peeID: id, proID: producteurID, acqID: acqid, annID: BUSINESS.Utilities.IdAnneeCourante);
                    //ShowAttestationControleVendange(peeID: id, taux: 0.8F, proID: producteurID, annID: BUSINESS.Utilities.IdAnneeCourante);
                }
            }
        }

        #endregion Button Attestation

        #region Button VendangeLivree

        private void BtnAfficherVendangesLivreesParProducteur(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParProducteur(BUSINESS.Utilities.IdAnneeCourante);
        }

        private void BtnAfficherVendangesLivreesParDate(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeVendangesLivreesParDateEnKilos(BUSINESS.Utilities.IdAnneeCourante);
        }

        #endregion Button VendangeLivree

        #region Button Apports

        private void BtnAfficherApportsProducteur_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                int peeId = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                CtrlReportViewer.ShowApportsProducteur(BUSINESS.Utilities.IdAnneeCourante, peeId);
            }
        }

        private void BtnAfficherApportsProducteurGrpOe_Click(object sender, EventArgs e)
        {
            if (pEE_PESEE_ENTETEBindingSource.Current != null)
            {
                int peeId = (int)pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value;
                CtrlReportViewer.ShowApportsProducteurGrpByOe(BUSINESS.Utilities.IdAnneeCourante, peeId);
            }
        }

        #endregion Button Apports
        #region Button Quittance

        private int _selectedPeeId
        {
            get
            {
                if (pEE_PESEE_ENTETEBindingSource.Current == null) return -1;

                var peeId = pEE_PESEE_ENTETEDataGridView.CurrentRow.Cells["PEE_ID"].Value as int?;
                return peeId ?? -1;
            }
        }

        private void BtnAfficherQuittance_Click(object sender, EventArgs e)
        {
            if (_selectedPeeId > -1) CtrlReportViewer.ShowQuittancePesee(_selectedPeeId);
        }

        private void BtnAfficherQuittanceV2_Click(object sender, EventArgs e)
        {
            if (_selectedPeeId > -1) CtrlReportViewer.ShowQuittanceV2Pesee(_selectedPeeId);
        }

        #endregion Button Quittance
    }
}