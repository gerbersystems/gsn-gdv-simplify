﻿using Gestion_Des_Vendanges.BUSINESS;
using GSN.GDV.Reports;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmMenu
    {
        private void EncavageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(Utilities.IdAnneeCourante);
        }

        private void RegionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesRegions.Wfrm);
        }

        #region DeclarationEncavage events

        private void declarationDencavageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(BUSINESS.Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void apercuDeclarationEncavage_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(Utilities.IdAnneeCourante, FormatReport.Apercu);
        }

        private void pdfDeclarationEncavage_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void xlsDeclarationEncavage_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void docDeclarationEncavage_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowDeclarationEncavage(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion DeclarationEncavage events

        private void gestionDesDistrictsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesDistricts.Wfrm);
        }

        private void gestionDesResponsablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesResponsables.Wfrm);
        }

        private void gestionDesPressoirsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesPressoirs.Wfrm);
        }

        private void zonesDeTravailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowWfrmSettings(WfrmGestionDesZonesDeTravail.Wfrm);
        }

        #region ListeParcellesVisite events

        private void aperçuListeParcellesVisiteVignesMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeParcellesVisiteVignes(Utilities.IdAnneeCourante);
        }

        private void pdfListeParcellesVisiteVignesMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeParcellesVisiteVignes(Utilities.IdAnneeCourante, FormatReport.PDF);
        }

        private void xlsListeParcellesVisiteVignesMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeParcellesVisiteVignes(Utilities.IdAnneeCourante, FormatReport.Excel);
        }

        private void wordListeParcellesVisiteVignesMenuItem_Click(object sender, EventArgs e)
        {
            CtrlReportViewer.ShowListeParcellesVisiteVignes(Utilities.IdAnneeCourante, FormatReport.Word);
        }

        #endregion ListeParcellesVisite events
    }
}