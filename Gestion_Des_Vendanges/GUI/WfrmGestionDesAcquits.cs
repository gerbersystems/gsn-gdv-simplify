﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*  
*  Description: Formulaire permettant la gestion des acquits
*  Date de création: 
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */ 
    partial class WfrmGestionDesAcquits : WfrmMenu
    {
        private static WfrmGestionDesAcquits _wfrm;
        public static WfrmGestionDesAcquits Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesAcquits();
                }
                return _wfrm;
            }
        }
        private WfrmGestionDesAcquits()
        {
            InitializeComponent();
        }
        private void WfrmGestionDesAcquits_Load(object sender, EventArgs e)
        {

            UpdateDataGrid();
            aCQ_ACQUITDataGridView.MouseDoubleClick += btnModifyAcquit_Click;
        }
        public void UpdateDataGrid()
        {
            try
            {
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.REG_REGION'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
                this.rEG_REGIONTableAdapter.Fill(this.dSPesees.REG_REGION);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.APP_APPELLATION'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
               // this.aPP_APPELLATIONTableAdapter.Fill(this.dSPesees.APP_APPELLATION);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.COM_COMMUNE'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
                this.cOM_COMMUNETableAdapter.Fill(this.dSPesees.COM_COMMUNE);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.COU_COULEUR'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
                this.cOU_COULEURTableAdapter.Fill(this.dSPesees.COU_COULEUR);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.CLA_CLASSE'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
                this.cLA_CLASSETableAdapter.Fill(this.dSPesees.CLA_CLASSE);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.PRO_PROPRIETAIRE'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
               // this.pRO_PROPRIETAIRETableAdapter.Fill(this.dSPesees.PRO_PROPRIETAIRE);
                // TODO : cette ligne de code charge les données dans la table 'dSPesees.PRO_NOMCOMPLET'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
                this.pRO_NOMCOMPLETTableAdapter.Fill(this.dSPesees.PRO_NOMCOMPLET);
                this.aCQ_ACQUITTableAdapter.FillByAnnee(this.dSPesees.ACQ_ACQUIT, new System.Nullable<int>(BUSINESS.Utilities.IdAnneeCourante));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void WfrmGestionDesAcquits_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }
        private void saveDataGrid()
        {
            this.Validate();
            this.aCQ_ACQUITBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dSPesees);
        }
        public void UpdateAcquit(BUSINESS.Acquit acquit)
        {
            int id = -1;
            int idCurrentRow;
            UpdateDataGrid();
            try
            {
                idCurrentRow = (int)this.aCQ_ACQUITDataGridView.CurrentRow.Cells["ACQ_ID"].Value;
            }
            catch
            {
                idCurrentRow = -1;
            }
            if (acquit.Id != idCurrentRow)
            {
                for (int i = 0; i < this.aCQ_ACQUITDataGridView.RowCount && id < 0; i++)
                {
                    if (acquit.Id == (int)this.aCQ_ACQUITDataGridView["ACQ_ID", i].Value)
                    {
                        id = i;
                    }
                }
                updateRowAcquit(this.aCQ_ACQUITDataGridView.Rows[id].Cells, acquit);
            }
            else
            {
                updateRowAcquit(this.aCQ_ACQUITDataGridView.CurrentRow.Cells, acquit);
            }
            
            saveDataGrid();
        }
        private BUSINESS.Acquit dataGridViewCellCollectionToAcquit(DataGridViewCellCollection row)
        {
            BUSINESS.Acquit acquit = new BUSINESS.Acquit();
            acquit.Id = (int) row["ACQ_ID"].Value;
            acquit.ClasseId = (int)row["CLA_ID"].Value;
            acquit.CouleurId = (int)row["COU_ID"].Value;
            acquit.AnneeId = (int)row["ANN_ID"].Value;
            acquit.CommuneId = (int)row["COM_ID"].Value;
            acquit.ProprietaireId = (int)row["PRO_ID"].Value;
            acquit.Numero = (int)row["ACQ_NUMERO"].Value;
            try
            {
                acquit.Date = (DateTime)row["ACQ_DATE"].Value;
            }
            catch
            {
            }
            acquit.Surface = decimal.Parse(row["ACQ_SURFACE"].Value.ToString());
            acquit.Litres = decimal.Parse(row["ACQ_LITRES"].Value.ToString());
            acquit.RegionId = (int)row["REG_ID"].Value;
            try
            {
                acquit.NumeroComplementaire = (int)row["ACQ_NUMERO_COMPL"].Value;
            }
            catch { }
            acquit.ProprietaireVendangeId = (int)row["PRO_ID_VENDANGE"].Value;
            return acquit;
        }
        private void updateRowAcquit(DataGridViewCellCollection row, BUSINESS.Acquit acquit)
        {
            row["CLA_ID"].Value = acquit.ClasseId;
            row["COU_ID"].Value = acquit.CouleurId;
            row["ANN_ID"].Value = acquit.AnneeId; 
            row["COM_ID"].Value = acquit.CommuneId;
            row["PRO_ID"].Value = acquit.ProprietaireId;
            row["ACQ_NUMERO"].Value = acquit.Numero;
            row["ACQ_DATE"].Value = acquit.Date;
            row["ACQ_SURFACE"].Value = acquit.Surface ;
            row["ACQ_LITRES"].Value = acquit.Litres;
            row["REG_ID"].Value = acquit.RegionId;
            if (acquit.NumeroComplementaire != null)
            {
                row["ACQ_NUMERO_COMPL"].Value = acquit.NumeroComplementaire;
            }
            row["PRO_ID_VENDANGE"].Value = acquit.ProprietaireVendangeId;
        }
        private void btnAddAcquit_Click(object sender, EventArgs e)
        {
            WfrmSaisieAcquit formAcquit = new WfrmSaisieAcquit(this);
            formAcquit.WfrmShowDialog();
  
        }
        private void btnModifyAcquit_Click(object sender, EventArgs e)
        {
            /* int i;
             int id = -1;
             int currentId = (int) this.aCQ_ACQUITDataGridView.CurrentRow.Cells[0].Value;
             for(i = 0; dSPesees.ACQ_ACQUIT.Rows.Count > i && id != currentId; i++)
             {
                 id = (int) dSPesees.ACQ_ACQUIT.Rows[i][0];
             }
             formGestion.newAddAcquit(this, this.dSPesees.ACQ_ACQUIT.Rows[i]);*/
            if(aCQ_ACQUITDataGridView.CurrentRow != null)
            {
                WfrmSaisieAcquit formAddAcquit = new WfrmSaisieAcquit(this, this.dataGridViewCellCollectionToAcquit(this.aCQ_ACQUITDataGridView.CurrentRow.Cells));
                formAddAcquit.WfrmShowDialog();
            }
        }
        private void btnDeleteAcquit_Click(object sender, EventArgs e)
        {
            if (this.aCQ_ACQUITDataGridView.CurrentRow != null)
            {
                int? acqId = (int?) aCQ_ACQUITDataGridView.CurrentRow.Cells["ACQ_ID"].Value;
                if (aCQ_ACQUITTableAdapter.NbRef(acqId) == 0)
                {
                    if (MessageBox.Show("Etes vous sûr de vouloir supprimer cet acquit?", "Suppression d'un acquit", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        this.aCQ_ACQUITBindingSource.RemoveCurrent();
                        saveDataGrid();
                    }
                }
                else
                {
                    MessageBox.Show("Suppression de l'acquit impossible.\n" +
                    "Des pesées ont été enregistrées pour cet acquit!", "Suppression impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        
                }
            }
        }
        private void cmdReportAcquits_Click(object sender, EventArgs e)
        {
            WfrmReportViewer formReport = new WfrmReportViewer();
            formReport.ShowListeAcquits(BUSINESS.Utilities.IdAnneeCourante);
        }
        private void btnDroitsProduction_Click(object sender, EventArgs e)
        {
            new WfrmReportViewer().ShowDroitsProduction(BUSINESS.Utilities.IdAnneeCourante);
           
        }
        private void aCQ_ACQUITDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            bool isSorted = true;
            int idAn = BUSINESS.Utilities.IdAnneeCourante;
            switch (e.ColumnIndex)
            {
                case 5:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderPro(dSPesees.ACQ_ACQUIT, idAn);
                    break;
                case 7:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderReg(dSPesees.ACQ_ACQUIT, idAn);
                    break;
                case 8:
                    aCQ_ACQUITBindingSource.RemoveSort();
                    aCQ_ACQUITTableAdapter.FillOrderCom(dSPesees.ACQ_ACQUIT, idAn);
                    break;
                default:
                    isSorted = false;
                    break;

            }
            if (isSorted)
            {
                foreach (DataGridViewColumn column in aCQ_ACQUITDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                aCQ_ACQUITDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }
       /* private void sortDataGrid(DataGridView dataGrid, int coloumnIndex)
        {
            bool isMin = true;
            object valeurLigneA = dataGrid.Rows[0].Cells[coloumnIndex].FormattedValue;
            for (int i = 0; i < dataGrid.Rows.Count && isMin; i++)
            {
                object valeurLigneB = dataGrid.Rows[i].Cells[coloumnIndex].FormattedValue;
                if (valeurLigneA.ToString().CompareTo(valeurLigneB) > 0)
                {
                    DataGridViewRow rowTmp= dataGrid.Rows[0];
                    dataGrid.Rows[i].SetValues(getValues(dataGrid.Rows[i]));
                    dataGrid.Rows[i].SetValues(getValues(rowTmp));
                    dataGrid.Rows[i].Cells.
                    isMin = false;
                }
            }
        }
        private object[] getValues(DataGridViewRow row)
        {
            List<object> values = new List<object>();
            foreach (DataGridViewCell cell in row.Cells)
            {
                values.Add(cell.Value);
            }
            return values.ToArray();
        }*/
    }
}
