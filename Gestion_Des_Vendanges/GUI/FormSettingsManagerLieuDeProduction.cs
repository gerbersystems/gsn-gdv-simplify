﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class FormSettingsManagerLieuDeProduction : FormSettingsManagerPesees
    {
        private class Commune
        {
            private COM_COMMUNETableAdapter _table;

            public string Name { get; private set; }
            public int Id { get; private set; }

            public Commune(int id, COM_COMMUNETableAdapter table)
            {
                _table = table;
                Id = id;
                Name = _table.GetComNomByComID(id);
            }

            public override string ToString()
            {
                return Name;
            }
        }

        private TextBox _txtLieId;
        private Button _btnAddLic;
        private Button _btnRemoveLic;
        private ComboBox _cbCommune;
        private ListBox _listLic;
        private LIC_LIEU_COMMUNETableAdapter _licTable;
        private COM_COMMUNETableAdapter _comTable;
        private List<int> _communeIds = new List<int>();

        public FormSettingsManagerLieuDeProduction(WfrmSetting form, TextBox idTextBox, Control focusControl,
            Button btnNew, Button btnUpdate, Button btnDelete, DataGridView dataGridView,
            BindingSource bindingSource, TableAdapterManager tableAdapterManager, DSPesees dsPesees,
            Button btnAddLic, Button btnRemoveLic, ComboBox cbCommune, ListBox listLic, LIC_LIEU_COMMUNETableAdapter licTable, TextBox txtLieId, COM_COMMUNETableAdapter comTable) :
            base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete,
            dataGridView, bindingSource, tableAdapterManager, dsPesees)
        {
            _btnAddLic = btnAddLic;
            _btnRemoveLic = btnRemoveLic;
            _cbCommune = cbCommune;
            _listLic = listLic;
            _licTable = licTable;
            _btnAddLic.Click += OnBtnAddLic_Click;
            _btnRemoveLic.Click += OnBtnRemoveLic_Click;
            _txtLieId = txtLieId;
            _comTable = comTable;
        }

        private void OnBtnAddLic_Click(object sender, EventArgs e)
        {
            int comId = (int)_cbCommune.SelectedValue;
            var commune = new Commune(comId, _comTable);
            if (!_communeIds.Contains(comId))
            {
                _listLic.Items.Add(commune);
                _communeIds.Add(comId);
            }
        }

        private void OnBtnRemoveLic_Click(object sender, EventArgs e)
        {
            Commune commune = (Commune)_listLic.SelectedItem;
            if (commune != null)
            {
                _communeIds.Remove(commune.Id);
                _listLic.Items.Remove(commune);
            }
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            base.BtnOk_Click(sender, e);
            int lieID = int.Parse(_txtLieId.Text);
            _licTable.DeleteByLieId(lieID);
            foreach (int comId in _communeIds)
            {
                try
                {
                    _licTable.Insert(comId, lieID);
                }
                catch
                {
                }
            }
        }

        protected override void DatagridSelectionChanged(object sender, EventArgs e)
        {
            base.DatagridSelectionChanged(sender, e);
            _listLic.Items.Clear();
            _communeIds.Clear();
            try
            {
                int lieID = int.Parse(_txtLieId.Text);
                _licTable.FillByLieId(DataSet.LIC_LIEU_COMMUNE, lieID);
                foreach (var lic in _licTable.GetDataByLieId(lieID))
                {
                    _listLic.Items.Add(new Commune(lic.COM_ID, _comTable));
                    _communeIds.Add(lic.COM_ID);
                }
            }
            catch
            {
            }
        }
    }
}