﻿using System;
namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesPaiements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (ArgumentOutOfRangeException)
            {
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pAI_PAIEMENTDataGridView = new System.Windows.Forms.DataGridView();
            this.PAI_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAI_ID_WINBIZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.PAI_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAI_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAI_ACOMPTE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PAI_TEXTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAI_PAIEMENTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.pAI_PAIEMENTTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAI_PAIEMENTTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolPrintFacture = new System.Windows.Forms.ToolStripDropDownButton();
            this.factureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.factureDetailBonusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.FactureRapportToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolWinBIZFolder = new System.Windows.Forms.ToolStripButton();
            this.toolExportToErp = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pAI_PAIEMENTDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAI_PAIEMENTBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pAI_PAIEMENTDataGridView
            // 
            this.pAI_PAIEMENTDataGridView.AllowUserToAddRows = false;
            this.pAI_PAIEMENTDataGridView.AllowUserToDeleteRows = false;
            this.pAI_PAIEMENTDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pAI_PAIEMENTDataGridView.AutoGenerateColumns = false;
            this.pAI_PAIEMENTDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pAI_PAIEMENTDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pAI_PAIEMENTDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAI_ID,
            this.PAI_ID_WINBIZ,
            this.ANN_ID,
            this.PAI_NUMERO,
            this.PAI_DATE,
            this.PAI_ACOMPTE,
            this.PAI_TEXTE,
            this.PRO_ID});
            this.pAI_PAIEMENTDataGridView.DataSource = this.pAI_PAIEMENTBindingSource;
            this.pAI_PAIEMENTDataGridView.Location = new System.Drawing.Point(3, 55);
            this.pAI_PAIEMENTDataGridView.MinimumSize = new System.Drawing.Size(500, 320);
            this.pAI_PAIEMENTDataGridView.MultiSelect = false;
            this.pAI_PAIEMENTDataGridView.Name = "pAI_PAIEMENTDataGridView";
            this.pAI_PAIEMENTDataGridView.ReadOnly = true;
            this.pAI_PAIEMENTDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.pAI_PAIEMENTDataGridView.Size = new System.Drawing.Size(862, 385);
            this.pAI_PAIEMENTDataGridView.TabIndex = 2;
            this.pAI_PAIEMENTDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PAI_PAIEMENTDataGridView_ColumnHeaderMouseClick);
            // 
            // PAI_ID
            // 
            this.PAI_ID.DataPropertyName = "PAI_ID";
            this.PAI_ID.HeaderText = "PAI_ID";
            this.PAI_ID.Name = "PAI_ID";
            this.PAI_ID.ReadOnly = true;
            this.PAI_ID.Visible = false;
            // 
            // PAI_ID_WINBIZ
            // 
            this.PAI_ID_WINBIZ.DataPropertyName = "PAI_ID_WINBIZ";
            this.PAI_ID_WINBIZ.HeaderText = "PAI_ID_WINBIZ";
            this.PAI_ID_WINBIZ.Name = "PAI_ID_WINBIZ";
            this.PAI_ID_WINBIZ.ReadOnly = true;
            this.PAI_ID_WINBIZ.Visible = false;
            // 
            // ANN_ID
            // 
            this.ANN_ID.DataPropertyName = "ANN_ID";
            this.ANN_ID.DataSource = this.aNNANNEEBindingSource;
            this.ANN_ID.DisplayMember = "ANN_AN";
            this.ANN_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ANN_ID.HeaderText = "Année";
            this.ANN_ID.Name = "ANN_ID";
            this.ANN_ID.ReadOnly = true;
            this.ANN_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ANN_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ANN_ID.ValueMember = "ANN_ID";
            this.ANN_ID.Width = 60;
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PAI_NUMERO
            // 
            this.PAI_NUMERO.DataPropertyName = "PAI_NUMERO";
            this.PAI_NUMERO.HeaderText = "Numéro";
            this.PAI_NUMERO.Name = "PAI_NUMERO";
            this.PAI_NUMERO.ReadOnly = true;
            this.PAI_NUMERO.Width = 70;
            // 
            // PAI_DATE
            // 
            this.PAI_DATE.DataPropertyName = "PAI_DATE";
            dataGridViewCellStyle1.Format = "d";
            dataGridViewCellStyle1.NullValue = null;
            this.PAI_DATE.DefaultCellStyle = dataGridViewCellStyle1;
            this.PAI_DATE.HeaderText = "Date";
            this.PAI_DATE.Name = "PAI_DATE";
            this.PAI_DATE.ReadOnly = true;
            this.PAI_DATE.Width = 90;
            // 
            // PAI_ACOMPTE
            // 
            this.PAI_ACOMPTE.DataPropertyName = "PAI_ACOMPTE";
            this.PAI_ACOMPTE.HeaderText = "Acompte";
            this.PAI_ACOMPTE.Name = "PAI_ACOMPTE";
            this.PAI_ACOMPTE.ReadOnly = true;
            this.PAI_ACOMPTE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PAI_ACOMPTE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PAI_ACOMPTE.Width = 55;
            // 
            // PAI_TEXTE
            // 
            this.PAI_TEXTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAI_TEXTE.DataPropertyName = "PAI_TEXTE";
            this.PAI_TEXTE.HeaderText = "Texte";
            this.PAI_TEXTE.MinimumWidth = 100;
            this.PAI_TEXTE.Name = "PAI_TEXTE";
            this.PAI_TEXTE.ReadOnly = true;
            // 
            // PRO_ID
            // 
            this.PRO_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRO_ID.DataPropertyName = "PRO_ID";
            this.PRO_ID.DataSource = this.pRONOMCOMPLETBindingSource;
            this.PRO_ID.DisplayMember = "NOMCOMPLET";
            this.PRO_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.PRO_ID.HeaderText = "Producteur";
            this.PRO_ID.MinimumWidth = 100;
            this.PRO_ID.Name = "PRO_ID";
            this.PRO_ID.ReadOnly = true;
            this.PRO_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PRO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.PRO_ID.ValueMember = "PRO_ID";
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // pAI_PAIEMENTBindingSource
            // 
            this.pAI_PAIEMENTBindingSource.DataMember = "PAI_PAIEMENT";
            this.pAI_PAIEMENTBindingSource.DataSource = this.dSPesees;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 21);
            this.label1.TabIndex = 26;
            this.label1.Text = "Gestion des paiements";
            // 
            // pAI_PAIEMENTTableAdapter
            // 
            this.pAI_PAIEMENTTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = this.aNN_ANNEETableAdapter;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = this.pAI_PAIEMENTTableAdapter;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = this.pRO_NOMCOMPLETTableAdapter;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolPrintFacture,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator1,
            this.FactureRapportToolStripButton,
            this.toolStripSeparator2,
            this.toolWinBIZFolder,
            this.toolExportToErp});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(868, 25);
            this.toolStrip1.TabIndex = 29;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = global::Gestion_Des_Vendanges.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(66, 22);
            this.toolAdd.Text = "Ajouter";
            this.toolAdd.Click += new System.EventHandler(this.BtnAddPmt_Click);
            // 
            // toolPrintFacture
            // 
            this.toolPrintFacture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.factureToolStripMenuItem,
            this.factureDetailBonusToolStripMenuItem});
            this.toolPrintFacture.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintFacture.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintFacture.Name = "toolPrintFacture";
            this.toolPrintFacture.Size = new System.Drawing.Size(75, 22);
            this.toolPrintFacture.Text = "Facture";
            // 
            // factureToolStripMenuItem
            // 
            this.factureToolStripMenuItem.Name = "factureToolStripMenuItem";
            this.factureToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.factureToolStripMenuItem.Text = "Facture";
            this.factureToolStripMenuItem.Click += new System.EventHandler(this.FactureToolStripMenuItem_Click);
            // 
            // factureDetailBonusToolStripMenuItem
            // 
            this.factureDetailBonusToolStripMenuItem.Name = "factureDetailBonusToolStripMenuItem";
            this.factureDetailBonusToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.factureDetailBonusToolStripMenuItem.Text = "Détail bonus";
            this.factureDetailBonusToolStripMenuItem.Click += new System.EventHandler(this.FactureDetailBonusToolStripMenuItem_Click);
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = global::Gestion_Des_Vendanges.Properties.Resources.open;
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(163, 22);
            this.toolUpdate.Text = "Modifier lignes manuelles";
            this.toolUpdate.Visible = false;
            this.toolUpdate.Click += new System.EventHandler(this.BtnModifierLIM_Click);
            // 
            // toolDelete
            // 
            this.toolDelete.Image = global::Gestion_Des_Vendanges.Properties.Resources.delete;
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(82, 22);
            this.toolDelete.Text = "Supprimer";
            this.toolDelete.Click += new System.EventHandler(this.BtnDeletePmt_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // FactureRapportToolStripButton
            // 
            this.FactureRapportToolStripButton.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.FactureRapportToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.FactureRapportToolStripButton.Name = "FactureRapportToolStripButton";
            this.FactureRapportToolStripButton.Size = new System.Drawing.Size(69, 22);
            this.FactureRapportToolStripButton.Text = "Rapport";
            this.FactureRapportToolStripButton.Click += new System.EventHandler(this.FactureRapportToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolWinBIZFolder
            // 
            this.toolWinBIZFolder.Image = global::Gestion_Des_Vendanges.Properties.Resources.version;
            this.toolWinBIZFolder.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolWinBIZFolder.Name = "toolWinBIZFolder";
            this.toolWinBIZFolder.Size = new System.Drawing.Size(106, 22);
            this.toolWinBIZFolder.Text = "Dossier WinBIZ";
            this.toolWinBIZFolder.Click += new System.EventHandler(this.ToolWinBIZFolder_Click);
            // 
            // toolExportToErp
            // 
            this.toolExportToErp.Image = global::Gestion_Des_Vendanges.Properties.Resources.newdoc;
            this.toolExportToErp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.toolExportToErp.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolExportToErp.Name = "toolExportToErp";
            this.toolExportToErp.Size = new System.Drawing.Size(169, 22);
            this.toolExportToErp.Text = "Exporter factures dans l\'erp";
            this.toolExportToErp.ToolTipText = "Exporte les factures de l\'erp sélectionné dans la configuration.";
            this.toolExportToErp.Click += new System.EventHandler(this.BtnExportToErp_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbAnnee);
            this.panel1.Location = new System.Drawing.Point(334, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 211);
            this.panel1.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 21);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            this.cbAnnee.SelectedIndexChanged += new System.EventHandler(this.CbAnnee_SelectedIndexChanged);
            // 
            // CtrlGestionDesPaiements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pAI_PAIEMENTDataGridView);
            this.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Name = "CtrlGestionDesPaiements";
            this.Size = new System.Drawing.Size(868, 443);
            this.Load += new System.EventHandler(this.WfrmGestionDesPaiements_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pAI_PAIEMENTDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAI_PAIEMENTBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pAI_PAIEMENTBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAI_PAIEMENTTableAdapter pAI_PAIEMENTTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView pAI_PAIEMENTDataGridView;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolExportToErp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.ToolStripButton toolWinBIZFolder;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_ID_WINBIZ;
        private System.Windows.Forms.DataGridViewComboBoxColumn ANN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_DATE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PAI_ACOMPTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAI_TEXTE;
        private System.Windows.Forms.DataGridViewComboBoxColumn PRO_ID;
        private System.Windows.Forms.ToolStripDropDownButton toolPrintFacture;
        private System.Windows.Forms.ToolStripMenuItem factureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem factureDetailBonusToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton FactureRapportToolStripButton;
    }
}