﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmCopieTablesPrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lIE_IDLabel;
            System.Windows.Forms.Label label1;
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.vAL_VALEUR_CEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vAL_VALEUR_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.VAL_VALEUR_CEPAGETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbDestination = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSourceDest = new System.Windows.Forms.BindingSource(this.components);
            this.dsPeseesDest = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.vAL_VALEUR_CEPAGEDataGridView = new System.Windows.Forms.DataGridView();
            this.CEP_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SelectToCopy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.VAL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAL_VALEUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCopierLie = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.tableAdapterDest = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            lIE_IDLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSourceDest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPeseesDest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lIE_IDLabel
            // 
            lIE_IDLabel.AutoSize = true;
            lIE_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lIE_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lIE_IDLabel.Location = new System.Drawing.Point(10, 110);
            lIE_IDLabel.Name = "lIE_IDLabel";
            lIE_IDLabel.Size = new System.Drawing.Size(193, 16);
            lIE_IDLabel.TabIndex = 2;
            lIE_IDLabel.Text = "Lieu de production source :";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label1.Location = new System.Drawing.Point(10, 189);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(223, 16);
            label1.TabIndex = 4;
            label1.Text = "Lieu de production destination :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vAL_VALEUR_CEPAGEBindingSource
            // 
            this.vAL_VALEUR_CEPAGEBindingSource.DataMember = "VAL_VALEUR_CEPAGE";
            this.vAL_VALEUR_CEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // vAL_VALEUR_CEPAGETableAdapter
            // 
            this.vAL_VALEUR_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = this.vAL_VALEUR_CEPAGETableAdapter;
            // 
            // cmbSource
            // 
            this.cmbSource.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.cmbSource.DisplayMember = "LIE_NOM";
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Location = new System.Drawing.Point(30, 129);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(243, 21);
            this.cmbSource.TabIndex = 3;
            this.cmbSource.ValueMember = "LIE_ID";
            this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.lIE_IDComboBox_SelectedIndexChanged);
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // cmbDestination
            // 
            this.cmbDestination.DataSource = this.lIELIEUDEPRODUCTIONBindingSourceDest;
            this.cmbDestination.DisplayMember = "LIE_NOM";
            this.cmbDestination.FormattingEnabled = true;
            this.cmbDestination.Location = new System.Drawing.Point(30, 208);
            this.cmbDestination.Name = "cmbDestination";
            this.cmbDestination.Size = new System.Drawing.Size(243, 21);
            this.cmbDestination.TabIndex = 5;
            this.cmbDestination.ValueMember = "LIE_ID";
            // 
            // lIELIEUDEPRODUCTIONBindingSourceDest
            // 
            this.lIELIEUDEPRODUCTIONBindingSourceDest.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSourceDest.DataSource = this.dsPeseesDest;
            // 
            // dsPeseesDest
            // 
            this.dsPeseesDest.DataSetName = "DSPesees";
            this.dsPeseesDest.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vAL_VALEUR_CEPAGEDataGridView
            // 
            this.vAL_VALEUR_CEPAGEDataGridView.AllowUserToAddRows = false;
            this.vAL_VALEUR_CEPAGEDataGridView.AllowUserToDeleteRows = false;
            this.vAL_VALEUR_CEPAGEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.vAL_VALEUR_CEPAGEDataGridView.AutoGenerateColumns = false;
            this.vAL_VALEUR_CEPAGEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.vAL_VALEUR_CEPAGEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vAL_VALEUR_CEPAGEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CEP_ID,
            this.SelectToCopy,
            this.VAL_ID,
            this.ANN_ID,
            this.VAL_VALEUR,
            this.LIE_ID});
            this.vAL_VALEUR_CEPAGEDataGridView.DataSource = this.vAL_VALEUR_CEPAGEBindingSource;
            this.vAL_VALEUR_CEPAGEDataGridView.Location = new System.Drawing.Point(17, 19);
            this.vAL_VALEUR_CEPAGEDataGridView.Name = "vAL_VALEUR_CEPAGEDataGridView";
            this.vAL_VALEUR_CEPAGEDataGridView.Size = new System.Drawing.Size(296, 366);
            this.vAL_VALEUR_CEPAGEDataGridView.TabIndex = 5;
            this.vAL_VALEUR_CEPAGEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.vAL_VALEUR_CEPAGEDataGridView_ColumnHeaderMouseClick);
            // 
            // CEP_ID
            // 
            this.CEP_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.DataSource = this.cEPCEPAGEBindingSource;
            this.CEP_ID.DisplayMember = "CEP_NOM";
            this.CEP_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CEP_ID.HeaderText = "Cépage";
            this.CEP_ID.MinimumWidth = 100;
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CEP_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.CEP_ID.ValueMember = "CEP_ID";
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // SelectToCopy
            // 
            this.SelectToCopy.HeaderText = "Sélectionner";
            this.SelectToCopy.Name = "SelectToCopy";
            this.SelectToCopy.Width = 70;
            // 
            // VAL_ID
            // 
            this.VAL_ID.DataPropertyName = "VAL_ID";
            this.VAL_ID.HeaderText = "VAL_ID";
            this.VAL_ID.Name = "VAL_ID";
            this.VAL_ID.ReadOnly = true;
            this.VAL_ID.Visible = false;
            // 
            // ANN_ID
            // 
            this.ANN_ID.DataPropertyName = "ANN_ID";
            this.ANN_ID.HeaderText = "ANN_ID";
            this.ANN_ID.Name = "ANN_ID";
            this.ANN_ID.Visible = false;
            // 
            // VAL_VALEUR
            // 
            this.VAL_VALEUR.DataPropertyName = "VAL_VALEUR";
            this.VAL_VALEUR.HeaderText = "VAL_VALEUR";
            this.VAL_VALEUR.Name = "VAL_VALEUR";
            this.VAL_VALEUR.Visible = false;
            // 
            // LIE_ID
            // 
            this.LIE_ID.DataPropertyName = "LIE_ID";
            this.LIE_ID.HeaderText = "LIE_ID";
            this.LIE_ID.Name = "LIE_ID";
            this.LIE_ID.Visible = false;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.vAL_VALEUR_CEPAGEDataGridView);
            this.groupBox1.Location = new System.Drawing.Point(288, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 391);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sélection des cépages à copier";
            // 
            // btnCopierLie
            // 
            this.btnCopierLie.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnCopierLie.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnCopierLie.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.btnCopierLie.Location = new System.Drawing.Point(176, 257);
            this.btnCopierLie.Margin = new System.Windows.Forms.Padding(2);
            this.btnCopierLie.Name = "btnCopierLie";
            this.btnCopierLie.Size = new System.Drawing.Size(97, 43);
            this.btnCopierLie.TabIndex = 30;
            this.btnCopierLie.Text = "Copier les tables de prix";
            this.btnCopierLie.UseVisualStyleBackColor = false;
            this.btnCopierLie.Click += new System.EventHandler(this.btnCopierLie_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label2.Location = new System.Drawing.Point(26, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(441, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Copie des tables de prix d\'un lieu de production";
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterDest
            // 
            this.tableAdapterDest.ClearBeforeFill = true;
            // 
            // WfrmCopieTablesPrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 476);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCopierLie);
            this.Controls.Add(label1);
            this.Controls.Add(lIE_IDLabel);
            this.Controls.Add(this.cmbSource);
            this.Controls.Add(this.cmbDestination);
            this.MinimumSize = new System.Drawing.Size(639, 508);
            this.Name = "WfrmCopieTablesPrix";
            this.Text = "Copie des tables de prix";
            this.Load += new System.EventHandler(this.WfrmCopieBonus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSourceDest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPeseesDest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource vAL_VALEUR_CEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.VAL_VALEUR_CEPAGETableAdapter vAL_VALEUR_CEPAGETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ComboBox cmbSource;
        private System.Windows.Forms.ComboBox cmbDestination;
        private System.Windows.Forms.DataGridView vAL_VALEUR_CEPAGEDataGridView;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCopierLie;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSourceDest;
        private System.Windows.Forms.DataGridViewComboBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SelectToCopy;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANN_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_VALEUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIE_ID;
        private DAO.DSPesees dsPeseesDest;
        private DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter tableAdapterDest;
    }
}