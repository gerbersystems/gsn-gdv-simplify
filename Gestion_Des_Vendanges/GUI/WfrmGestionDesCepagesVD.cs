﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesCepages
    {
        private void InitManager(bool optionWinBiz)
        {
            _manager = new FormSettingsManagerCepage(this,
                                                     cEP_IDTextBox,
                                                     cEP_NUMEROTextBox,
                                                     btnAddCepage,
                                                     btnSaveCepage,
                                                     btnSupprimer,
                                                     cEP_CEPAGEDataGridView,
                                                     cEP_CEPAGEBindingSource,
                                                     tableAdapterManager,
                                                     dSPesees,
                                                     mOC_IDComboBox,
                                                     gbModeComtabilisation,
                                                     optSelonCouleur,
                                                     optSpecifique,
                                                     cEP_CEPAGETableAdapter,
                                                     optionWinBiz,
                                                     cOU_IDComboBox,
                                                     cEP_NOMTextBox);

            _manager.AddControl(cOU_IDComboBox);
            _manager.AddControl(optSelonCouleur);
        }

        private void InitSepecificTableAdapters()
        {
        }
    }
}