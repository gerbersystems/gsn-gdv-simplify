﻿using System.Windows.Forms;
namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesVendanges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmGestionDesVendanges));
            this.splitPage = new Gestion_Des_Vendanges.GUI.GVSplitContainer();
            this.splitLeft = new Gestion_Des_Vendanges.GUI.GVSplitContainer();
            this.btnCockpit = new System.Windows.Forms.Button();
            this.btnAcquit = new System.Windows.Forms.Button();
            this.btnPesee = new System.Windows.Forms.Button();
            this.btnArticle = new System.Windows.Forms.Button();
            this.btnBonus = new System.Windows.Forms.Button();
            this.btnPaiement = new System.Windows.Forms.Button();
            this.btnRapport = new System.Windows.Forms.Button();
            this.backgroundWorkerDoUpdate = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.splitPage)).BeginInit();
            this.splitPage.Panel1.SuspendLayout();
            this.splitPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitLeft)).BeginInit();
            this.splitLeft.Panel2.SuspendLayout();
            this.splitLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitPage
            // 
            this.splitPage.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.splitPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitPage.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitPage.Location = new System.Drawing.Point(0, 24);
            this.splitPage.Name = "splitPage";
            // 
            // splitPage.Panel1
            // 
            this.splitPage.Panel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitPage.Panel1.Controls.Add(this.splitLeft);
            this.splitPage.Panel1.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.splitPage.Panel1MinSize = 195;
            // 
            // splitPage.Panel2
            // 
            this.splitPage.Panel2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitPage.Size = new System.Drawing.Size(1264, 657);
            this.splitPage.SplitterDistance = 203;
            this.splitPage.TabIndex = 1;
            // 
            // splitLeft
            // 
            this.splitLeft.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.splitLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitLeft.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitLeft.Location = new System.Drawing.Point(0, 0);
            this.splitLeft.Name = "splitLeft";
            this.splitLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitLeft.Panel1
            // 
            this.splitLeft.Panel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitLeft.Panel1MinSize = 195;
            // 
            // splitLeft.Panel2
            // 
            this.splitLeft.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(235)))), ((int)(((byte)(130)))));
            this.splitLeft.Panel2.Controls.Add(this.btnCockpit);
            this.splitLeft.Panel2.Controls.Add(this.btnAcquit);
            this.splitLeft.Panel2.Controls.Add(this.btnPesee);
            this.splitLeft.Panel2.Controls.Add(this.btnArticle);
            this.splitLeft.Panel2.Controls.Add(this.btnBonus);
            this.splitLeft.Panel2.Controls.Add(this.btnPaiement);
            this.splitLeft.Panel2.Controls.Add(this.btnRapport);
            this.splitLeft.Size = new System.Drawing.Size(203, 657);
            this.splitLeft.SplitterDistance = 392;
            this.splitLeft.TabIndex = 0;
            // 
            // btnCockpit
            // 
            this.btnCockpit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnCockpit.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnCockpit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCockpit.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnCockpit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCockpit.Image = global::Gestion_Des_Vendanges.Properties.Resources.version;
            this.btnCockpit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCockpit.Location = new System.Drawing.Point(0, 2);
            this.btnCockpit.Name = "btnCockpit";
            this.btnCockpit.Size = new System.Drawing.Size(203, 37);
            this.btnCockpit.TabIndex = 1;
            this.btnCockpit.Text = "  Cockpit";
            this.btnCockpit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCockpit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCockpit.UseVisualStyleBackColor = false;
            this.btnCockpit.Click += new System.EventHandler(this.BtnCockpit_Click);
            // 
            // btnAcquit
            // 
            this.btnAcquit.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAcquit.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnAcquit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAcquit.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnAcquit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAcquit.Image = global::Gestion_Des_Vendanges.Properties.Resources.acquit;
            this.btnAcquit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAcquit.Location = new System.Drawing.Point(0, 39);
            this.btnAcquit.Name = "btnAcquit";
            this.btnAcquit.Size = new System.Drawing.Size(203, 37);
            this.btnAcquit.TabIndex = 2;
            this.btnAcquit.Text = "Acquits";
            this.btnAcquit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAcquit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAcquit.UseVisualStyleBackColor = false;
            this.btnAcquit.Click += new System.EventHandler(this.BtnAcquit_Click);
            // 
            // btnPesee
            // 
            this.btnPesee.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPesee.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnPesee.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnPesee.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPesee.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnPesee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPesee.Image = global::Gestion_Des_Vendanges.Properties.Resources.balance24;
            this.btnPesee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesee.Location = new System.Drawing.Point(0, 76);
            this.btnPesee.Name = "btnPesee";
            this.btnPesee.Size = new System.Drawing.Size(203, 37);
            this.btnPesee.TabIndex = 3;
            this.btnPesee.Text = "Pesées";
            this.btnPesee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPesee.UseVisualStyleBackColor = false;
            this.btnPesee.Click += new System.EventHandler(this.BtnPesee_Click);
            // 
            // btnArticle
            // 
            this.btnArticle.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnArticle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnArticle.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnArticle.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnArticle.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnArticle.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnArticle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnArticle.Image = global::Gestion_Des_Vendanges.Properties.Resources.bottle_ho_yeah;
            this.btnArticle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnArticle.Location = new System.Drawing.Point(0, 113);
            this.btnArticle.Name = "btnArticle";
            this.btnArticle.Size = new System.Drawing.Size(203, 37);
            this.btnArticle.TabIndex = 4;
            this.btnArticle.Text = "Articles";
            this.btnArticle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnArticle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnArticle.UseVisualStyleBackColor = false;
            this.btnArticle.Click += new System.EventHandler(this.BtnArticle_Click);
            // 
            // btnBonus
            // 
            this.btnBonus.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnBonus.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnBonus.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnBonus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnBonus.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnBonus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnBonus.Image = global::Gestion_Des_Vendanges.Properties.Resources.calc24;
            this.btnBonus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBonus.Location = new System.Drawing.Point(0, 150);
            this.btnBonus.Name = "btnBonus";
            this.btnBonus.Size = new System.Drawing.Size(203, 37);
            this.btnBonus.TabIndex = 5;
            this.btnBonus.Text = "Bonus";
            this.btnBonus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBonus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBonus.UseVisualStyleBackColor = false;
            this.btnBonus.Click += new System.EventHandler(this.BtnBonus_Click);
            // 
            // btnPaiement
            // 
            this.btnPaiement.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPaiement.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnPaiement.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnPaiement.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnPaiement.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnPaiement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPaiement.Image = global::Gestion_Des_Vendanges.Properties.Resources.bundle_24x24x32b;
            this.btnPaiement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPaiement.Location = new System.Drawing.Point(0, 187);
            this.btnPaiement.Name = "btnPaiement";
            this.btnPaiement.Size = new System.Drawing.Size(203, 37);
            this.btnPaiement.TabIndex = 6;
            this.btnPaiement.Text = "Paiements";
            this.btnPaiement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPaiement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPaiement.UseVisualStyleBackColor = false;
            this.btnPaiement.Click += new System.EventHandler(this.BtnPaiement_Click);
            // 
            // btnRapport
            // 
            this.btnRapport.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnRapport.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Boutons", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnRapport.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.btnRapport.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnRapport.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.btnRapport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnRapport.Image = global::Gestion_Des_Vendanges.Properties.Resources.report;
            this.btnRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRapport.Location = new System.Drawing.Point(0, 224);
            this.btnRapport.Name = "btnRapport";
            this.btnRapport.Size = new System.Drawing.Size(203, 37);
            this.btnRapport.TabIndex = 7;
            this.btnRapport.Text = "Rapports";
            this.btnRapport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRapport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRapport.UseVisualStyleBackColor = false;
            this.btnRapport.Click += new System.EventHandler(this.BtnRapport_Click);
            // 
            // WfrmGestionDesVendanges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.splitPage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "WfrmGestionDesVendanges";
            this.Text = "Gestion des vendanges";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.WfrmGestionDesVendanges_Load);
            this.Shown += new System.EventHandler(this.WfrmGestionDesVendanges_Shown);
            this.Controls.SetChildIndex(this.splitPage, 0);
            this.splitPage.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPage)).EndInit();
            this.splitPage.ResumeLayout(false);
            this.splitLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitLeft)).EndInit();
            this.splitLeft.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAcquit;
        private System.Windows.Forms.Button btnPesee;
        private System.Windows.Forms.Button btnArticle;
        private System.Windows.Forms.Button btnBonus;
        private System.Windows.Forms.Button btnPaiement;
        private System.Windows.Forms.Button btnRapport;
        private System.ComponentModel.BackgroundWorker backgroundWorkerDoUpdate;
        private GVSplitContainer splitPage;
        private GVSplitContainer splitLeft;
        private Button btnCockpit;
    }
}