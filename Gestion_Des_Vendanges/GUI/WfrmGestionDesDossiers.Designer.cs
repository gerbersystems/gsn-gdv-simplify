﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesDossiers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dAT_NOMLabel;
            System.Windows.Forms.Label dAT_DESCRIPTIONLabel;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dAT_DESCRIPTIONTextBox = new System.Windows.Forms.TextBox();
            this.dAT_DATABASEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSMaster = new Gestion_Des_Vendanges.DAO.DSMaster();
            this.dAT_NOMTextBox = new System.Windows.Forms.TextBox();
            this.dAT_IDTextBox = new System.Windows.Forms.TextBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.dAT_DATABASETableAdapter = new Gestion_Des_Vendanges.DAO.DSMasterTableAdapters.DAT_DATABASETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSMasterTableAdapters.TableAdapterManager();
            this.dAT_DATABASEDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            dAT_NOMLabel = new System.Windows.Forms.Label();
            dAT_DESCRIPTIONLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dAT_DATABASEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAT_DATABASEDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dAT_NOMLabel
            // 
            dAT_NOMLabel.AutoSize = true;
            dAT_NOMLabel.Location = new System.Drawing.Point(6, 41);
            dAT_NOMLabel.Name = "dAT_NOMLabel";
            dAT_NOMLabel.Size = new System.Drawing.Size(35, 13);
            dAT_NOMLabel.TabIndex = 5;
            dAT_NOMLabel.Text = "Nom :";
            // 
            // dAT_DESCRIPTIONLabel
            // 
            dAT_DESCRIPTIONLabel.AutoSize = true;
            dAT_DESCRIPTIONLabel.Location = new System.Drawing.Point(6, 73);
            dAT_DESCRIPTIONLabel.Name = "dAT_DESCRIPTIONLabel";
            dAT_DESCRIPTIONLabel.Size = new System.Drawing.Size(66, 13);
            dAT_DESCRIPTIONLabel.TabIndex = 7;
            dAT_DESCRIPTIONLabel.Text = "Description :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(dAT_DESCRIPTIONLabel);
            this.groupBox1.Controls.Add(this.dAT_DESCRIPTIONTextBox);
            this.groupBox1.Controls.Add(dAT_NOMLabel);
            this.groupBox1.Controls.Add(this.dAT_NOMTextBox);
            this.groupBox1.Controls.Add(this.dAT_IDTextBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 147);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gestion des dossiers";
            // 
            // dAT_DESCRIPTIONTextBox
            // 
            this.dAT_DESCRIPTIONTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dAT_DATABASEBindingSource, "DAT_DESCRIPTION", true));
            this.dAT_DESCRIPTIONTextBox.Location = new System.Drawing.Point(125, 70);
            this.dAT_DESCRIPTIONTextBox.Name = "dAT_DESCRIPTIONTextBox";
            this.dAT_DESCRIPTIONTextBox.Size = new System.Drawing.Size(237, 20);
            this.dAT_DESCRIPTIONTextBox.TabIndex = 1;
            // 
            // dAT_DATABASEBindingSource
            // 
            this.dAT_DATABASEBindingSource.DataMember = "DAT_DATABASE";
            this.dAT_DATABASEBindingSource.DataSource = this.dSMaster;
            // 
            // dSMaster
            // 
            this.dSMaster.DataSetName = "DSMaster";
            this.dSMaster.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dAT_NOMTextBox
            // 
            this.dAT_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dAT_DATABASEBindingSource, "DAT_NOM", true));
            this.dAT_NOMTextBox.Location = new System.Drawing.Point(125, 38);
            this.dAT_NOMTextBox.Name = "dAT_NOMTextBox";
            this.dAT_NOMTextBox.Size = new System.Drawing.Size(237, 20);
            this.dAT_NOMTextBox.TabIndex = 0;
            // 
            // dAT_IDTextBox
            // 
            this.dAT_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dAT_DATABASEBindingSource, "DAT_ID", true));
            this.dAT_IDTextBox.Location = new System.Drawing.Point(257, 19);
            this.dAT_IDTextBox.Name = "dAT_IDTextBox";
            this.dAT_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.dAT_IDTextBox.TabIndex = 5;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(287, 111);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 4;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnNew
            // 
            this.btnNew.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnNew.Location = new System.Drawing.Point(125, 111);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 30);
            this.btnNew.TabIndex = 2;
            this.btnNew.Text = "Nouveau";
            this.btnNew.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(206, 111);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // dAT_DATABASETableAdapter
            // 
            this.dAT_DATABASETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DAT_DATABASETableAdapter = this.dAT_DATABASETableAdapter;
            this.tableAdapterManager.LIC_LICENSETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSMasterTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // dAT_DATABASEDataGridView
            // 
            this.dAT_DATABASEDataGridView.AllowUserToAddRows = false;
            this.dAT_DATABASEDataGridView.AllowUserToDeleteRows = false;
            this.dAT_DATABASEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dAT_DATABASEDataGridView.AutoGenerateColumns = false;
            this.dAT_DATABASEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.dAT_DATABASEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dAT_DATABASEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dAT_DATABASEDataGridView.DataSource = this.dAT_DATABASEBindingSource;
            this.dAT_DATABASEDataGridView.Location = new System.Drawing.Point(386, 12);
            this.dAT_DATABASEDataGridView.Name = "dAT_DATABASEDataGridView";
            this.dAT_DATABASEDataGridView.ReadOnly = true;
            this.dAT_DATABASEDataGridView.Size = new System.Drawing.Size(322, 309);
            this.dAT_DATABASEDataGridView.TabIndex = 22;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DAT_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DAT_DESCRIPTION";
            this.dataGridViewTextBoxColumn3.HeaderText = "Description";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // WfrmGestionDesDossiers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 333);
            this.Controls.Add(this.dAT_DATABASEDataGridView);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(736, 371);
            this.Name = "WfrmGestionDesDossiers";
            this.Text = "Gestion des vendanges - Gestion des dossiers";
            this.Load += new System.EventHandler(this.WfrmGestionDesDossiers_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dAT_DATABASEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dAT_DATABASEDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnUpdate;
        private DAO.DSMaster dSMaster;
        private System.Windows.Forms.BindingSource dAT_DATABASEBindingSource;
        private DAO.DSMasterTableAdapters.DAT_DATABASETableAdapter dAT_DATABASETableAdapter;
        private DAO.DSMasterTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox dAT_IDTextBox;
        private System.Windows.Forms.DataGridView dAT_DATABASEDataGridView;
        private System.Windows.Forms.TextBox dAT_NOMTextBox;
        private System.Windows.Forms.TextBox dAT_DESCRIPTIONTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}