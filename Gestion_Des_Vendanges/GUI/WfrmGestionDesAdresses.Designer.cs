﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesAdresses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label aDR_ADRESSE1Label;
            System.Windows.Forms.Label aDR_ADRESSE2Label;
            System.Windows.Forms.Label aDR_ADRESSE3Label;
            System.Windows.Forms.Label aDR_NPALabel;
            System.Windows.Forms.Label aDR_VILLELabel;
            System.Windows.Forms.Label aDR_TELLabel;
            System.Windows.Forms.Label aDR_FAXLabel;
            System.Windows.Forms.Label aDR_EMAILLabel;
            System.Windows.Forms.Label aDR_PAYSLabel;
            System.Windows.Forms.Label aDR_NATELLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.aDR_ADRESSEDataGridView = new System.Windows.Forms.DataGridView();
            this.ADR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADR_ID_WINBIZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aDR_ADRESSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aDR_ADRESSE1TextBox = new System.Windows.Forms.TextBox();
            this.aDR_ADRESSE2TextBox = new System.Windows.Forms.TextBox();
            this.aDR_ADRESSE3TextBox = new System.Windows.Forms.TextBox();
            this.aDR_NPATextBox = new System.Windows.Forms.TextBox();
            this.aDR_TELTextBox = new System.Windows.Forms.TextBox();
            this.aDR_FAXTextBox = new System.Windows.Forms.TextBox();
            this.aDR_EMAILTextBox = new System.Windows.Forms.TextBox();
            this.aDR_PAYSTextBox = new System.Windows.Forms.TextBox();
            this.btnAddAdress = new System.Windows.Forms.Button();
            this.btnNewAdresse = new System.Windows.Forms.Button();
            this.aDR_IDTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtErpId = new System.Windows.Forms.TextBox();
            this.aDR_NATELTextBox = new System.Windows.Forms.TextBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.cbVille = new System.Windows.Forms.ComboBox();
            this.cOACOMMUNEADRESSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aDR_ADRESSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cOA_COMMUNE_ADRESSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COA_COMMUNE_ADRESSETableAdapter();
            aDR_ADRESSE1Label = new System.Windows.Forms.Label();
            aDR_ADRESSE2Label = new System.Windows.Forms.Label();
            aDR_ADRESSE3Label = new System.Windows.Forms.Label();
            aDR_NPALabel = new System.Windows.Forms.Label();
            aDR_VILLELabel = new System.Windows.Forms.Label();
            aDR_TELLabel = new System.Windows.Forms.Label();
            aDR_FAXLabel = new System.Windows.Forms.Label();
            aDR_EMAILLabel = new System.Windows.Forms.Label();
            aDR_PAYSLabel = new System.Windows.Forms.Label();
            aDR_NATELLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aDR_ADRESSEDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDR_ADRESSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cOACOMMUNEADRESSEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // aDR_ADRESSE1Label
            // 
            aDR_ADRESSE1Label.AutoSize = true;
            aDR_ADRESSE1Label.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_ADRESSE1Label.Location = new System.Drawing.Point(5, 28);
            aDR_ADRESSE1Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_ADRESSE1Label.Name = "aDR_ADRESSE1Label";
            aDR_ADRESSE1Label.Size = new System.Drawing.Size(67, 15);
            aDR_ADRESSE1Label.TabIndex = 10;
            aDR_ADRESSE1Label.Text = "Adresse 1 :";
            // 
            // aDR_ADRESSE2Label
            // 
            aDR_ADRESSE2Label.AutoSize = true;
            aDR_ADRESSE2Label.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_ADRESSE2Label.Location = new System.Drawing.Point(5, 57);
            aDR_ADRESSE2Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_ADRESSE2Label.Name = "aDR_ADRESSE2Label";
            aDR_ADRESSE2Label.Size = new System.Drawing.Size(67, 15);
            aDR_ADRESSE2Label.TabIndex = 12;
            aDR_ADRESSE2Label.Text = "Adresse 2 :";
            // 
            // aDR_ADRESSE3Label
            // 
            aDR_ADRESSE3Label.AutoSize = true;
            aDR_ADRESSE3Label.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_ADRESSE3Label.Location = new System.Drawing.Point(5, 86);
            aDR_ADRESSE3Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_ADRESSE3Label.Name = "aDR_ADRESSE3Label";
            aDR_ADRESSE3Label.Size = new System.Drawing.Size(70, 15);
            aDR_ADRESSE3Label.TabIndex = 14;
            aDR_ADRESSE3Label.Text = "Adresse 3 : ";
            // 
            // aDR_NPALabel
            // 
            aDR_NPALabel.AutoSize = true;
            aDR_NPALabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_NPALabel.Location = new System.Drawing.Point(5, 115);
            aDR_NPALabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_NPALabel.Name = "aDR_NPALabel";
            aDR_NPALabel.Size = new System.Drawing.Size(77, 15);
            aDR_NPALabel.TabIndex = 16;
            aDR_NPALabel.Text = "Code postal :";
            // 
            // aDR_VILLELabel
            // 
            aDR_VILLELabel.AutoSize = true;
            aDR_VILLELabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_VILLELabel.Location = new System.Drawing.Point(5, 144);
            aDR_VILLELabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_VILLELabel.Name = "aDR_VILLELabel";
            aDR_VILLELabel.Size = new System.Drawing.Size(40, 15);
            aDR_VILLELabel.TabIndex = 18;
            aDR_VILLELabel.Text = "Ville : ";
            // 
            // aDR_TELLabel
            // 
            aDR_TELLabel.AutoSize = true;
            aDR_TELLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_TELLabel.Location = new System.Drawing.Point(293, 58);
            aDR_TELLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_TELLabel.Name = "aDR_TELLabel";
            aDR_TELLabel.Size = new System.Drawing.Size(74, 15);
            aDR_TELLabel.TabIndex = 20;
            aDR_TELLabel.Text = "Téléphone : ";
            // 
            // aDR_FAXLabel
            // 
            aDR_FAXLabel.AutoSize = true;
            aDR_FAXLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_FAXLabel.Location = new System.Drawing.Point(293, 86);
            aDR_FAXLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_FAXLabel.Name = "aDR_FAXLabel";
            aDR_FAXLabel.Size = new System.Drawing.Size(35, 15);
            aDR_FAXLabel.TabIndex = 22;
            aDR_FAXLabel.Text = "Fax : ";
            // 
            // aDR_EMAILLabel
            // 
            aDR_EMAILLabel.AutoSize = true;
            aDR_EMAILLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_EMAILLabel.Location = new System.Drawing.Point(584, 28);
            aDR_EMAILLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_EMAILLabel.Name = "aDR_EMAILLabel";
            aDR_EMAILLabel.Size = new System.Drawing.Size(98, 15);
            aDR_EMAILLabel.TabIndex = 26;
            aDR_EMAILLabel.Text = "Adresse e-mail : ";
            // 
            // aDR_PAYSLabel
            // 
            aDR_PAYSLabel.AutoSize = true;
            aDR_PAYSLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_PAYSLabel.Location = new System.Drawing.Point(293, 28);
            aDR_PAYSLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_PAYSLabel.Name = "aDR_PAYSLabel";
            aDR_PAYSLabel.Size = new System.Drawing.Size(40, 15);
            aDR_PAYSLabel.TabIndex = 28;
            aDR_PAYSLabel.Text = "Pays : ";
            // 
            // aDR_NATELLabel
            // 
            aDR_NATELLabel.AutoSize = true;
            aDR_NATELLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_NATELLabel.Location = new System.Drawing.Point(293, 115);
            aDR_NATELLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            aDR_NATELLabel.Name = "aDR_NATELLabel";
            aDR_NATELLabel.Size = new System.Drawing.Size(54, 15);
            aDR_NATELLabel.TabIndex = 30;
            aDR_NATELLabel.Text = "Mobile : ";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label1.Location = new System.Drawing.Point(584, 57);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(48, 15);
            label1.TabIndex = 32;
            label1.Text = "Erp Id : ";
            // 
            // aDR_ADRESSEDataGridView
            // 
            this.aDR_ADRESSEDataGridView.AllowUserToAddRows = false;
            this.aDR_ADRESSEDataGridView.AllowUserToDeleteRows = false;
            this.aDR_ADRESSEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_ADRESSEDataGridView.AutoGenerateColumns = false;
            this.aDR_ADRESSEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aDR_ADRESSEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aDR_ADRESSEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ADR_ID,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.ADR_ID_WINBIZ});
            this.aDR_ADRESSEDataGridView.DataSource = this.aDR_ADRESSEBindingSource;
            this.aDR_ADRESSEDataGridView.Location = new System.Drawing.Point(12, 198);
            this.aDR_ADRESSEDataGridView.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_ADRESSEDataGridView.MultiSelect = false;
            this.aDR_ADRESSEDataGridView.Name = "aDR_ADRESSEDataGridView";
            this.aDR_ADRESSEDataGridView.ReadOnly = true;
            this.aDR_ADRESSEDataGridView.RowTemplate.Height = 24;
            this.aDR_ADRESSEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.aDR_ADRESSEDataGridView.Size = new System.Drawing.Size(1140, 471);
            this.aDR_ADRESSEDataGridView.TabIndex = 4;
            // 
            // ADR_ID
            // 
            this.ADR_ID.DataPropertyName = "ADR_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.ADR_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.ADR_ID.HeaderText = "Id";
            this.ADR_ID.Name = "ADR_ID";
            this.ADR_ID.ReadOnly = true;
            this.ADR_ID.Visible = false;
            this.ADR_ID.Width = 70;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ADR_ADRESSE1";
            this.dataGridViewTextBoxColumn4.HeaderText = "Adresse";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 95;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ADR_ADRESSE2";
            this.dataGridViewTextBoxColumn5.HeaderText = "Adresse2";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 80;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "ADR_ADRESSE3";
            this.dataGridViewTextBoxColumn6.HeaderText = "Adresse3";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 80;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ADR_NPA";
            this.dataGridViewTextBoxColumn7.HeaderText = "Npa";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ADR_VILLE";
            this.dataGridViewTextBoxColumn8.HeaderText = "Ville";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 95;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ADR_TEL";
            this.dataGridViewTextBoxColumn9.HeaderText = "Téléphone";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 95;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "ADR_FAX";
            this.dataGridViewTextBoxColumn10.HeaderText = "Fax";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 95;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ADR_NATEL";
            this.dataGridViewTextBoxColumn11.HeaderText = "Mobile";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 95;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "ADR_EMAIL";
            this.dataGridViewTextBoxColumn12.HeaderText = "E-mail";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 95;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ADR_PAYS";
            this.dataGridViewTextBoxColumn13.HeaderText = "Pays";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Visible = false;
            this.dataGridViewTextBoxColumn13.Width = 80;
            // 
            // ADR_ID_WINBIZ
            // 
            this.ADR_ID_WINBIZ.DataPropertyName = "ADR_ID_WINBIZ";
            this.ADR_ID_WINBIZ.HeaderText = "Erp Id";
            this.ADR_ID_WINBIZ.Name = "ADR_ID_WINBIZ";
            this.ADR_ID_WINBIZ.ReadOnly = true;
            // 
            // aDR_ADRESSEBindingSource
            // 
            this.aDR_ADRESSEBindingSource.DataMember = "ADR_ADRESSE";
            this.aDR_ADRESSEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aDR_ADRESSE1TextBox
            // 
            this.aDR_ADRESSE1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_ADRESSE1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_ADRESSE1", true));
            this.aDR_ADRESSE1TextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_ADRESSE1TextBox.Location = new System.Drawing.Point(85, 25);
            this.aDR_ADRESSE1TextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_ADRESSE1TextBox.Name = "aDR_ADRESSE1TextBox";
            this.aDR_ADRESSE1TextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_ADRESSE1TextBox.TabIndex = 0;
            // 
            // aDR_ADRESSE2TextBox
            // 
            this.aDR_ADRESSE2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_ADRESSE2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_ADRESSE2", true));
            this.aDR_ADRESSE2TextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_ADRESSE2TextBox.Location = new System.Drawing.Point(85, 54);
            this.aDR_ADRESSE2TextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_ADRESSE2TextBox.Name = "aDR_ADRESSE2TextBox";
            this.aDR_ADRESSE2TextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_ADRESSE2TextBox.TabIndex = 1;
            // 
            // aDR_ADRESSE3TextBox
            // 
            this.aDR_ADRESSE3TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_ADRESSE3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_ADRESSE3", true));
            this.aDR_ADRESSE3TextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_ADRESSE3TextBox.Location = new System.Drawing.Point(85, 83);
            this.aDR_ADRESSE3TextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_ADRESSE3TextBox.Name = "aDR_ADRESSE3TextBox";
            this.aDR_ADRESSE3TextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_ADRESSE3TextBox.TabIndex = 2;
            // 
            // aDR_NPATextBox
            // 
            this.aDR_NPATextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_NPATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_NPA", true));
            this.aDR_NPATextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_NPATextBox.Location = new System.Drawing.Point(85, 112);
            this.aDR_NPATextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_NPATextBox.Name = "aDR_NPATextBox";
            this.aDR_NPATextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_NPATextBox.TabIndex = 3;
            this.aDR_NPATextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aDR_NPATextBox_KeyPress);
            this.aDR_NPATextBox.Validated += new System.EventHandler(this.aDR_NPATextBox_Validated);
            // 
            // aDR_TELTextBox
            // 
            this.aDR_TELTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_TELTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_TEL", true));
            this.aDR_TELTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_TELTextBox.Location = new System.Drawing.Point(373, 54);
            this.aDR_TELTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_TELTextBox.Name = "aDR_TELTextBox";
            this.aDR_TELTextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_TELTextBox.TabIndex = 6;
            this.aDR_TELTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aDR_NPATextBox_KeyPress);
            // 
            // aDR_FAXTextBox
            // 
            this.aDR_FAXTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_FAXTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_FAX", true));
            this.aDR_FAXTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_FAXTextBox.Location = new System.Drawing.Point(373, 83);
            this.aDR_FAXTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_FAXTextBox.Name = "aDR_FAXTextBox";
            this.aDR_FAXTextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_FAXTextBox.TabIndex = 7;
            this.aDR_FAXTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aDR_NPATextBox_KeyPress);
            // 
            // aDR_EMAILTextBox
            // 
            this.aDR_EMAILTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_EMAILTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_EMAIL", true));
            this.aDR_EMAILTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_EMAILTextBox.Location = new System.Drawing.Point(680, 25);
            this.aDR_EMAILTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_EMAILTextBox.Name = "aDR_EMAILTextBox";
            this.aDR_EMAILTextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_EMAILTextBox.TabIndex = 9;
            // 
            // aDR_PAYSTextBox
            // 
            this.aDR_PAYSTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_PAYSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_PAYS", true));
            this.aDR_PAYSTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_PAYSTextBox.Location = new System.Drawing.Point(373, 25);
            this.aDR_PAYSTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_PAYSTextBox.Name = "aDR_PAYSTextBox";
            this.aDR_PAYSTextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_PAYSTextBox.TabIndex = 5;
            this.aDR_PAYSTextBox.Text = "Suisse";
            // 
            // btnAddAdress
            // 
            this.btnAddAdress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAdress.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddAdress.Location = new System.Drawing.Point(973, 139);
            this.btnAddAdress.Name = "btnAddAdress";
            this.btnAddAdress.Size = new System.Drawing.Size(75, 30);
            this.btnAddAdress.TabIndex = 11;
            this.btnAddAdress.Text = "Modifier";
            this.btnAddAdress.UseVisualStyleBackColor = false;
            // 
            // btnNewAdresse
            // 
            this.btnNewAdresse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewAdresse.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnNewAdresse.Location = new System.Drawing.Point(892, 139);
            this.btnNewAdresse.Name = "btnNewAdresse";
            this.btnNewAdresse.Size = new System.Drawing.Size(75, 30);
            this.btnNewAdresse.TabIndex = 10;
            this.btnNewAdresse.Text = "Nouveau";
            this.btnNewAdresse.UseVisualStyleBackColor = false;
            // 
            // aDR_IDTextBox
            // 
            this.aDR_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.aDR_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_ID", true));
            this.aDR_IDTextBox.Enabled = false;
            this.aDR_IDTextBox.Location = new System.Drawing.Point(99, 12);
            this.aDR_IDTextBox.Name = "aDR_IDTextBox";
            this.aDR_IDTextBox.Size = new System.Drawing.Size(124, 23);
            this.aDR_IDTextBox.TabIndex = 32;
            this.aDR_IDTextBox.Text = "Id adresse";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtErpId);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(aDR_NATELLabel);
            this.groupBox1.Controls.Add(this.aDR_NATELTextBox);
            this.groupBox1.Controls.Add(this.aDR_EMAILTextBox);
            this.groupBox1.Controls.Add(aDR_FAXLabel);
            this.groupBox1.Controls.Add(aDR_EMAILLabel);
            this.groupBox1.Controls.Add(aDR_TELLabel);
            this.groupBox1.Controls.Add(aDR_VILLELabel);
            this.groupBox1.Controls.Add(this.aDR_FAXTextBox);
            this.groupBox1.Controls.Add(this.aDR_TELTextBox);
            this.groupBox1.Controls.Add(this.aDR_PAYSTextBox);
            this.groupBox1.Controls.Add(this.aDR_NPATextBox);
            this.groupBox1.Controls.Add(aDR_PAYSLabel);
            this.groupBox1.Controls.Add(aDR_NPALabel);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnNewAdresse);
            this.groupBox1.Controls.Add(this.btnAddAdress);
            this.groupBox1.Controls.Add(aDR_ADRESSE1Label);
            this.groupBox1.Controls.Add(this.aDR_ADRESSE1TextBox);
            this.groupBox1.Controls.Add(aDR_ADRESSE2Label);
            this.groupBox1.Controls.Add(this.aDR_ADRESSE2TextBox);
            this.groupBox1.Controls.Add(aDR_ADRESSE3Label);
            this.groupBox1.Controls.Add(this.aDR_ADRESSE3TextBox);
            this.groupBox1.Controls.Add(this.cbVille);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1140, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie d\'adresse";
            // 
            // txtErpId
            // 
            this.txtErpId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtErpId.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_ID_WINBIZ", true));
            this.txtErpId.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.txtErpId.Location = new System.Drawing.Point(680, 54);
            this.txtErpId.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtErpId.Name = "txtErpId";
            this.txtErpId.Size = new System.Drawing.Size(200, 23);
            this.txtErpId.TabIndex = 10;
            // 
            // aDR_NATELTextBox
            // 
            this.aDR_NATELTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aDR_NATELTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_NATEL", true));
            this.aDR_NATELTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_NATELTextBox.Location = new System.Drawing.Point(373, 112);
            this.aDR_NATELTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.aDR_NATELTextBox.Name = "aDR_NATELTextBox";
            this.aDR_NATELTextBox.Size = new System.Drawing.Size(200, 23);
            this.aDR_NATELTextBox.TabIndex = 8;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(1054, 139);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 12;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // cbVille
            // 
            this.cbVille.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbVille.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbVille.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbVille.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aDR_ADRESSEBindingSource, "ADR_VILLE", true));
            this.cbVille.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cbVille.FormattingEnabled = true;
            this.cbVille.Location = new System.Drawing.Point(85, 141);
            this.cbVille.Name = "cbVille";
            this.cbVille.Size = new System.Drawing.Size(200, 23);
            this.cbVille.TabIndex = 4;
            this.cbVille.Validated += new System.EventHandler(this.cbVille_Validated);
            // 
            // cOACOMMUNEADRESSEBindingSource
            // 
            this.cOACOMMUNEADRESSEBindingSource.DataMember = "COA_COMMUNE_ADRESSE";
            this.cOACOMMUNEADRESSEBindingSource.DataSource = this.dSPesees;
            // 
            // aDR_ADRESSETableAdapter
            // 
            this.aDR_ADRESSETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = this.aDR_ADRESSETableAdapter;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // cOA_COMMUNE_ADRESSETableAdapter
            // 
            this.cOA_COMMUNE_ADRESSETableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesAdresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(1164, 681);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.aDR_IDTextBox);
            this.Controls.Add(this.aDR_ADRESSEDataGridView);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MinimumSize = new System.Drawing.Size(1180, 720);
            this.Name = "WfrmGestionDesAdresses";
            this.Text = "Gestion des adresses";
            this.Load += new System.EventHandler(this.WfrmGestionDesAdresses_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aDR_ADRESSEDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDR_ADRESSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cOACOMMUNEADRESSEBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aDR_ADRESSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter aDR_ADRESSETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView aDR_ADRESSEDataGridView;
        private System.Windows.Forms.TextBox aDR_ADRESSE1TextBox;
        private System.Windows.Forms.TextBox aDR_ADRESSE2TextBox;
        private System.Windows.Forms.TextBox aDR_ADRESSE3TextBox;
        private System.Windows.Forms.TextBox aDR_NPATextBox;
        private System.Windows.Forms.TextBox aDR_TELTextBox;
        private System.Windows.Forms.TextBox aDR_FAXTextBox;
        private System.Windows.Forms.TextBox aDR_EMAILTextBox;
        private System.Windows.Forms.TextBox aDR_PAYSTextBox;
        private System.Windows.Forms.Button btnAddAdress;
        private System.Windows.Forms.Button btnNewAdresse;
        private System.Windows.Forms.TextBox aDR_IDTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.ComboBox cbVille;
        private System.Windows.Forms.BindingSource cOACOMMUNEADRESSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COA_COMMUNE_ADRESSETableAdapter cOA_COMMUNE_ADRESSETableAdapter;
        private System.Windows.Forms.TextBox aDR_NATELTextBox;
        private System.Windows.Forms.TextBox txtErpId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADR_ID_WINBIZ;
    }
}