﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmGlobalSettings : WfrmBase
    {
        #region Properties

        private static WfrmGlobalSettings _wfrm;

        public static WfrmGlobalSettings Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed) _wfrm = new WfrmGlobalSettings();
                return _wfrm;
            }
        }

        #endregion Properties

        #region Constructor

        public WfrmGlobalSettings()
        {
            InitializeComponent();

            Load += (s, e) => InitializeSettings();
            Shown += (s, e) => InitializeEvents();
        }

        private void InitializeSettings()
        {
            // Canton
            CantonComboBox.DataSource = Enum.GetValues(typeof(CantonEnum));
            CantonComboBox.SelectedItem = GlobalParam.Instance.Canton;

            if (!GlobalParam.Instance.CanSetCanton()) CantonComboBox.Enabled = false;

            // Erp
            ErpComboBox.DataSource = Enum.GetValues(typeof(ErpEnum));
            ErpComboBox.SelectedItem = GlobalParam.Instance.Erp;

            // SyncContact
            syncContactRadioButton.Checked = GlobalParam.Instance.SyncContacts == SyncContactsEnum.ProhibitModifyLocalContacts;
            unSyncContactRadioButton.Checked = GlobalParam.Instance.SyncContacts == SyncContactsEnum.AllowModifyLocalContacts;

            // RateConversionVinClair
            RougeVinClairNumericUpDown.Value = GlobalParam.Instance.TauxConversionRouge;
            BlancVinClairNumericUpDown.Value = GlobalParam.Instance.TauxConversionBlanc;

            // RateConversionMoutRaisin
            RougeMoutRaisinNumericUpDown.Value = GlobalParam.Instance.TauxConversionRougeMout ?? GlobalParam.Instance.TauxConversionRouge;
            BlancMoutRaisinNumericUpDown.Value = GlobalParam.Instance.TauxConversionBlancMout ?? GlobalParam.Instance.TauxConversionBlanc;

            // SondageUnite
            sondageBricksRadioButton.Checked = GlobalParam.Instance.ShowSondageBrix;
            sondageOechsleRadioButton.Checked = GlobalParam.Instance.ShowSondageOechsle;

            // ExceedQuota (boolean)
            TrueExceedQuotaRadioButton.Checked = GlobalParam.Instance.AcceptExceedingQuota == true;
            rdbFalseExceedQuota.Checked = GlobalParam.Instance.AcceptExceedingQuota == false;

            // Type de table de bonus (integer)
            var bonusUnites = new[] {
                new { Id = BonusUniteEnum.FixePourcent, Name = "Valeur FIXE en %" },
                new { Id = BonusUniteEnum.VariableChf, Name = "Valeur VARIABLE (sondage moyen) en CHF" },
                new { Id = BonusUniteEnum.VariablePourcent, Name = "Valeur VARIABLE (sondage moyen) en %"}
            };

            typeTableBonusComboBox.DataSource = bonusUnites;
            typeTableBonusComboBox.DisplayMember = "Name";
            typeTableBonusComboBox.ValueMember = "Id";
            typeTableBonusComboBox.SelectedIndex = (int)GlobalParam.Instance.BonusUnite;

            // DegreValeur (integer)
            OechsleRadioButton.Checked = (GlobalParam.Instance.DegreValeur == DegreValeur.Oechsle);
            BrixRadioButton.Checked = (GlobalParam.Instance.DegreValeur == DegreValeur.Brix);

            // Unite (integer)
            KiloRadioButton.Checked = GlobalParam.Instance.ApplicationUnite == ApplicationUniteEnum.Kilo;
            LitresRadioButton.Checked = GlobalParam.Instance.ApplicationUnite == ApplicationUniteEnum.Litre;

            // Type de facture
            var factureType = new[]
            {
                new { Id = FactureTypeEnum.Simple, Name = "Facture simple" },
                new { Id = FactureTypeEnum.LitresMout, Name = "Facture simple (litres de moût)" },
                new { Id = FactureTypeEnum.AvecBonus, Name = "Facture avec bonus" },
                new { Id = FactureTypeEnum.AvecBonusV2, Name = "Facture avec bonus - V2" },
            };

            typeFactureComboBox.DataSource = factureType;
            typeFactureComboBox.DisplayMember = "Name";
            typeFactureComboBox.ValueMember = "Id";
            typeFactureComboBox.SelectedIndex = (int)GlobalParam.Instance.FactureType;
        }

        private void InitializeEvents()
        {
            // Canton (integer)
            CantonComboBox.SelectedIndexChanged += (s, e) => GlobalParam.Instance.Canton = (CantonEnum)CantonComboBox.SelectedIndex;

            // Erp (integer)
            ErpComboBox.SelectedIndexChanged += (s, e) => GlobalParam.Instance.Erp = (ErpEnum)ErpComboBox.SelectedIndex;

            // SyncContact (integer)
            syncContactRadioButton.CheckedChanged += (s, e) =>
            {
                if (syncContactRadioButton.Checked) GlobalParam.Instance.SyncContacts = SyncContactsEnum.ProhibitModifyLocalContacts;
            };
            unSyncContactRadioButton.CheckedChanged += (s, e) =>
            {
                if (unSyncContactRadioButton.Checked) GlobalParam.Instance.SyncContacts = SyncContactsEnum.AllowModifyLocalContacts;
            };

            // RateConversionVinClair
            RougeVinClairNumericUpDown.ValueChanged += (s, e) => GlobalParam.Instance.TauxConversionRouge = RougeVinClairNumericUpDown.Value;
            BlancVinClairNumericUpDown.ValueChanged += (s, e) => GlobalParam.Instance.TauxConversionBlanc = BlancVinClairNumericUpDown.Value;

            // RateConversionMoutRaisin
            RougeMoutRaisinNumericUpDown.ValueChanged += (s, e) => GlobalParam.Instance.TauxConversionRougeMout = RougeMoutRaisinNumericUpDown.Value;
            BlancMoutRaisinNumericUpDown.ValueChanged += (s, e) => GlobalParam.Instance.TauxConversionBlancMout = BlancMoutRaisinNumericUpDown.Value;

            // SondageUnite (boolean)
            sondageOechsleRadioButton.CheckedChanged += (s, e) =>
            {
                GlobalParam.Instance.ShowSondageBrix = sondageBricksRadioButton.Checked;
                GlobalParam.Instance.ShowSondageOechsle = sondageOechsleRadioButton.Checked;
            };

            // ExceedQuota (boolean)
            TrueExceedQuotaRadioButton.CheckedChanged +=
                (s, e) => GlobalParam.Instance.AcceptExceedingQuota = TrueExceedQuotaRadioButton.Checked;

            // Type de table de bonus (integer)
            typeTableBonusComboBox.SelectedIndexChanged +=
                (s, e) => GlobalParam.Instance.BonusUnite = (BonusUniteEnum)typeTableBonusComboBox.SelectedIndex;

            // DegreValeur (integer)
            OechsleRadioButton.CheckedChanged += (s, e) =>
            {
                if (OechsleRadioButton.Checked) GlobalParam.Instance.DegreValeur = DegreValeur.Oechsle;
            };
            BrixRadioButton.CheckedChanged += (s, e) =>
            {
                if (BrixRadioButton.Checked) GlobalParam.Instance.DegreValeur = DegreValeur.Brix;
            };

            // Unite (integer)
            KiloRadioButton.CheckedChanged += (s, e) =>
            {
                if (KiloRadioButton.Checked) GlobalParam.Instance.ApplicationUnite = ApplicationUniteEnum.Kilo;
            };
            LitresRadioButton.CheckedChanged += (s, e) =>
            {
                if (LitresRadioButton.Checked) GlobalParam.Instance.ApplicationUnite = ApplicationUniteEnum.Litre;
            };

            // Type de facture (integer)
            typeFactureComboBox.SelectedIndexChanged +=
                (s, e) => GlobalParam.Instance.FactureType = (FactureTypeEnum)typeFactureComboBox.SelectedIndex;
        }

        #endregion Constructor
    }
}