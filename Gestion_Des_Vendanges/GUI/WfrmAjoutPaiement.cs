﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: 1er Formulaire de création des paiements
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmAjoutPaiement : WfrmBase
    {
        #region Fields

        private ParametrePaiement _parametrePaiement;

        #endregion Fields

        #region Constructors

        private WfrmAjoutPaiement(ParametrePaiement parametrePaiement)
        {
            InitializeComponent();
            _parametrePaiement = parametrePaiement;
        }

        public static WfrmAjoutPaiement GetInstance(ParametrePaiement parametrePaiement)
        {
            var _wfrm = new WfrmAjoutPaiement(parametrePaiement);

            switch (GlobalParam.Instance.Erp)
            {
                case ErpEnum.Winbiz:
                    try
                    {
                        List<WinBizExercice> exercices = new WinBizControleur(Utilities.IdAnneeCourante).GetExercices();
                        foreach (WinBizExercice exercice in exercices)
                        {
                            _wfrm.ExerciceComptableComboBox.Items.Add(exercice);
                        }
                    }
                    catch (DirectoryNotFoundException)
                    {
                        MessageBox.Show("Erreur, le dossier WinBIZ est incorrect!",
                                        "Erreur dossier WinBIZ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        WfrmWinBizFolder.Wfrm.WfrmShowDialog();
                        _wfrm = null;
                    }

                    break;

                case ErpEnum.XpertFIN:
                    _wfrm.ExerciceComptableComboBox.DataSource = _wfrm.aNNANNEEBindingSource;
                    _wfrm.ExerciceComptableComboBox.DisplayMember = "ANN_AN";
                    break;

                default:
                    throw new NotImplementedException("This erp type does not exist.");
            }

            return _wfrm;
        }

        #endregion Constructors

        #region Methods

        private void OptAcompte_CheckedChanged(object sender, EventArgs e) => UpdateTxtTitre();

        private void BtnSuivant_Click(object sender, EventArgs e)
        {
            try
            {
                int annId = (int)cbAnnId.SelectedValue;
                if ((!optAcompte.Checked && !optFinal.Checked) || txtTitre.Text == "" || txtNumeroPmt.Text == "")
                {
                    MessageBox.Show("Veuillez saisir tous les champs avant de continuer.",
                                    "Saisie incorrect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    _parametrePaiement.AnnId = annId;
                    _parametrePaiement.IsAcompte = optAcompte.Checked;
                    _parametrePaiement.Texte = txtTitre.Text;
                    _parametrePaiement.Date = dtpDate.Value;
                    _parametrePaiement.Code = txtNumeroPmt.Text;
                    DialogResult = DialogResult.OK;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Veuillez saisir tous les champs avant de continuer.",
                                "Saisie incorrect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void OptFinal_CheckedChanged(object sender, EventArgs e) => UpdateTxtTitre();

        private void UpdateTxtTitre() => txtTitre.Text = ((optAcompte.Checked) ? "Acompte " : "Décompte final ") + cbAnnId.Text;

        private void WfrmAjoutPaiement_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
            try
            {
                cbAnnId.SelectedValue = (_parametrePaiement.AnnId > 0) ? _parametrePaiement.AnnId : Utilities.IdAnneeCourante;
            }
            catch
            {
                cbAnnId.SelectedValue = Utilities.IdAnneeCourante;
            }

            try
            {
                if (_parametrePaiement.Date >= dtpDate.MinDate && _parametrePaiement.Date <= dtpDate.MaxDate) dtpDate.Value = _parametrePaiement.Date;

                if (_parametrePaiement.IsAcompte)
                {
                    optAcompte.Checked = true;
                }
                else
                {
                    optFinal.Checked = true;
                }
            }
            catch
            { }

            txtNumeroPmt.Text = _parametrePaiement.Code;
            txtTitre.Text = _parametrePaiement.Texte ?? txtTitre.Text;
            bool fin = false;
            int an = (_parametrePaiement.Date != DateTime.MinValue) ? _parametrePaiement.Date.Year : Utilities.AnneeCourante;

            //TODO implement autoSelect exercise comptable
            if (GlobalParam.Instance.Erp != ErpEnum.Winbiz) return;

            for (int i = 0; i < ExerciceComptableComboBox.Items.Count && !fin; i++)
            {
                if (((WinBizExercice)ExerciceComptableComboBox.Items[i]).Annee == an)
                {
                    fin = true;
                    ExerciceComptableComboBox.SelectedIndex = i;
                }
            }
        }

        private void BtnAnnuler_Click(object sender, EventArgs e) => DialogResult = DialogResult.Cancel;

        private void CbExerciceWinBIZ_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GlobalParam.Instance.Erp != ErpEnum.Winbiz) return;

            dtpDate.MaxDate = ((WinBizExercice)ExerciceComptableComboBox.SelectedItem).DateFin;
            dtpDate.MinDate = ((WinBizExercice)ExerciceComptableComboBox.SelectedItem).DateDebut;
        }

        #endregion Methods
    }
}