﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesZonesDeTravail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label valeurLabel;
            System.Windows.Forms.Label nomLabel;
            this.ZoneTravailGroupBox = new System.Windows.Forms.GroupBox();
            this.mOC_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.label1 = new System.Windows.Forms.Label();
            this.zOT_DESCRIPTIONTextBox = new System.Windows.Forms.TextBox();
            this.zOTZONETRAVAILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnAddZoneTravail = new System.Windows.Forms.Button();
            this.btnSaveZoneTravail = new System.Windows.Forms.Button();
            this.zOT_VALEURTextBox = new System.Windows.Forms.TextBox();
            this.zOT_ZONE_TRAVAILDataGridView = new System.Windows.Forms.DataGridView();
            this.ZOT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zOTDESCRIPTIONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zOTVALEURDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zOT_ZONE_TRAVAILTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter();
            this.zOT_IDTextBox = new System.Windows.Forms.TextBox();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            valeurLabel = new System.Windows.Forms.Label();
            nomLabel = new System.Windows.Forms.Label();
            this.ZoneTravailGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOT_ZONE_TRAVAILDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // valeurLabel
            // 
            valeurLabel.AutoSize = true;
            valeurLabel.Location = new System.Drawing.Point(6, 59);
            valeurLabel.Name = "valeurLabel";
            valeurLabel.Size = new System.Drawing.Size(37, 13);
            valeurLabel.TabIndex = 15;
            valeurLabel.Text = "Valeur";
            // 
            // nomLabel
            // 
            nomLabel.AutoSize = true;
            nomLabel.Location = new System.Drawing.Point(6, 31);
            nomLabel.Name = "nomLabel";
            nomLabel.Size = new System.Drawing.Size(29, 13);
            nomLabel.TabIndex = 15;
            nomLabel.Text = "Nom";
            // 
            // ZoneTravailGroupBox
            // 
            this.ZoneTravailGroupBox.Controls.Add(this.mOC_IDComboBox);
            this.ZoneTravailGroupBox.Controls.Add(this.label1);
            this.ZoneTravailGroupBox.Controls.Add(nomLabel);
            this.ZoneTravailGroupBox.Controls.Add(this.zOT_DESCRIPTIONTextBox);
            this.ZoneTravailGroupBox.Controls.Add(this.btnSupprimer);
            this.ZoneTravailGroupBox.Controls.Add(this.btnAddZoneTravail);
            this.ZoneTravailGroupBox.Controls.Add(valeurLabel);
            this.ZoneTravailGroupBox.Controls.Add(this.btnSaveZoneTravail);
            this.ZoneTravailGroupBox.Controls.Add(this.zOT_VALEURTextBox);
            this.ZoneTravailGroupBox.Location = new System.Drawing.Point(10, 11);
            this.ZoneTravailGroupBox.Name = "ZoneTravailGroupBox";
            this.ZoneTravailGroupBox.Size = new System.Drawing.Size(417, 188);
            this.ZoneTravailGroupBox.TabIndex = 21;
            this.ZoneTravailGroupBox.TabStop = false;
            this.ZoneTravailGroupBox.Text = "Saisie des zones de travail";
            // 
            // mOC_IDComboBox
            // 
            this.mOC_IDComboBox.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.mOC_IDComboBox.DisplayMember = "MOC_TEXTE";
            this.mOC_IDComboBox.FormattingEnabled = true;
            this.mOC_IDComboBox.Location = new System.Drawing.Point(225, 118);
            this.mOC_IDComboBox.Name = "mOC_IDComboBox";
            this.mOC_IDComboBox.Size = new System.Drawing.Size(186, 21);
            this.mOC_IDComboBox.TabIndex = 17;
            this.mOC_IDComboBox.ValueMember = "MOC_ID";
            this.mOC_IDComboBox.Visible = false;
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Mode de comptabilisation par défaut :";
            this.label1.Visible = false;
            // 
            // zOT_DESCRIPTIONTextBox
            // 
            this.zOT_DESCRIPTIONTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zOTZONETRAVAILBindingSource, "ZOT_DESCRIPTION", true));
            this.zOT_DESCRIPTIONTextBox.Location = new System.Drawing.Point(225, 25);
            this.zOT_DESCRIPTIONTextBox.Name = "zOT_DESCRIPTIONTextBox";
            this.zOT_DESCRIPTIONTextBox.Size = new System.Drawing.Size(186, 19);
            this.zOT_DESCRIPTIONTextBox.TabIndex = 0;
            // 
            // zOTZONETRAVAILBindingSource
            // 
            this.zOTZONETRAVAILBindingSource.DataMember = "ZOT_ZONE_TRAVAIL";
            this.zOTZONETRAVAILBindingSource.DataSource = this.dSPesees;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(214, 152);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 5;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnAddZoneTravail
            // 
            this.btnAddZoneTravail.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddZoneTravail.Location = new System.Drawing.Point(52, 152);
            this.btnAddZoneTravail.Name = "btnAddZoneTravail";
            this.btnAddZoneTravail.Size = new System.Drawing.Size(75, 30);
            this.btnAddZoneTravail.TabIndex = 3;
            this.btnAddZoneTravail.Text = "Nouveau";
            this.btnAddZoneTravail.UseVisualStyleBackColor = false;
            // 
            // btnSaveZoneTravail
            // 
            this.btnSaveZoneTravail.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveZoneTravail.Location = new System.Drawing.Point(133, 152);
            this.btnSaveZoneTravail.Name = "btnSaveZoneTravail";
            this.btnSaveZoneTravail.Size = new System.Drawing.Size(75, 30);
            this.btnSaveZoneTravail.TabIndex = 4;
            this.btnSaveZoneTravail.Text = "Modifier";
            this.btnSaveZoneTravail.UseVisualStyleBackColor = false;
            // 
            // zOT_VALEURTextBox
            // 
            this.zOT_VALEURTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zOTZONETRAVAILBindingSource, "ZOT_VALEUR", true));
            this.zOT_VALEURTextBox.Location = new System.Drawing.Point(225, 53);
            this.zOT_VALEURTextBox.Name = "zOT_VALEURTextBox";
            this.zOT_VALEURTextBox.Size = new System.Drawing.Size(186, 19);
            this.zOT_VALEURTextBox.TabIndex = 1;
            // 
            // zOT_ZONE_TRAVAILDataGridView
            // 
            this.zOT_ZONE_TRAVAILDataGridView.AllowUserToOrderColumns = true;
            this.zOT_ZONE_TRAVAILDataGridView.AutoGenerateColumns = false;
            this.zOT_ZONE_TRAVAILDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.zOT_ZONE_TRAVAILDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.zOT_ZONE_TRAVAILDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.zOT_ZONE_TRAVAILDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ZOT_ID,
            this.zOTDESCRIPTIONDataGridViewTextBoxColumn,
            this.zOTVALEURDataGridViewTextBoxColumn});
            this.zOT_ZONE_TRAVAILDataGridView.DataSource = this.zOTZONETRAVAILBindingSource;
            this.zOT_ZONE_TRAVAILDataGridView.Location = new System.Drawing.Point(433, 11);
            this.zOT_ZONE_TRAVAILDataGridView.Name = "zOT_ZONE_TRAVAILDataGridView";
            this.zOT_ZONE_TRAVAILDataGridView.Size = new System.Drawing.Size(360, 188);
            this.zOT_ZONE_TRAVAILDataGridView.TabIndex = 23;
            // 
            // ZOT_ID
            // 
            this.ZOT_ID.DataPropertyName = "ZOT_ID";
            this.ZOT_ID.HeaderText = "ZOT_ID";
            this.ZOT_ID.Name = "ZOT_ID";
            this.ZOT_ID.ReadOnly = true;
            this.ZOT_ID.Visible = false;
            // 
            // zOTDESCRIPTIONDataGridViewTextBoxColumn
            // 
            this.zOTDESCRIPTIONDataGridViewTextBoxColumn.DataPropertyName = "ZOT_DESCRIPTION";
            this.zOTDESCRIPTIONDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.zOTDESCRIPTIONDataGridViewTextBoxColumn.Name = "zOTDESCRIPTIONDataGridViewTextBoxColumn";
            // 
            // zOTVALEURDataGridViewTextBoxColumn
            // 
            this.zOTVALEURDataGridViewTextBoxColumn.DataPropertyName = "ZOT_VALEUR";
            this.zOTVALEURDataGridViewTextBoxColumn.HeaderText = "Valeur";
            this.zOTVALEURDataGridViewTextBoxColumn.Name = "zOTVALEURDataGridViewTextBoxColumn";
            // 
            // zOT_ZONE_TRAVAILTableAdapter
            // 
            this.zOT_ZONE_TRAVAILTableAdapter.ClearBeforeFill = true;
            // 
            // zOT_IDTextBox
            // 
            this.zOT_IDTextBox.Location = new System.Drawing.Point(473, 113);
            this.zOT_IDTextBox.Name = "zOT_IDTextBox";
            this.zOT_IDTextBox.Size = new System.Drawing.Size(100, 19);
            this.zOT_IDTextBox.TabIndex = 16;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = this.zOT_ZONE_TRAVAILTableAdapter;
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesZonesDeTravail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(802, 211);
            this.Controls.Add(this.zOT_IDTextBox);
            this.Controls.Add(this.ZoneTravailGroupBox);
            this.Controls.Add(this.zOT_ZONE_TRAVAILDataGridView);
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(660, 250);
            this.Name = "WfrmGestionDesZonesDeTravail";
            this.Text = "Gestion des zones de travail";
            this.Load += new System.EventHandler(this.WfrmGestionDesZonesDeTravail_Load);
            this.ZoneTravailGroupBox.ResumeLayout(false);
            this.ZoneTravailGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOT_ZONE_TRAVAILDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ZoneTravailGroupBox;
        private System.Windows.Forms.TextBox zOT_DESCRIPTIONTextBox;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnAddZoneTravail;
        private System.Windows.Forms.Button btnSaveZoneTravail;
        private System.Windows.Forms.TextBox zOT_VALEURTextBox;
        private System.Windows.Forms.DataGridView zOT_ZONE_TRAVAILDataGridView;
        private DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource zOTZONETRAVAILBindingSource;
        private DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter zOT_ZONE_TRAVAILTableAdapter;
        private System.Windows.Forms.TextBox zOT_IDTextBox;
        private DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZOT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn zOTDESCRIPTIONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zOTVALEURDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox mOC_IDComboBox;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
    }
}