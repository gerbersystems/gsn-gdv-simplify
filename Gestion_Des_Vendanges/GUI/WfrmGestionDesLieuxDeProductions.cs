﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire des lieux de productions
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesLieuxDeProductions : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesLieuxDeProductions _wfrm;

        public static WfrmGestionDesLieuxDeProductions Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesLieuxDeProductions();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int lieId = (int)lIE_LIEU_DE_PRODUCTIONDataGridView.CurrentRow.Cells["LIE_ID"].Value;
                    resultat = (lIE_LIEU_DE_PRODUCTIONTableAdapter.NbRef(lieId) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesLieuxDeProductions()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesLieuxDeProductions_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rEG_REGIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIC_LIEU_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            cOM_COMMUNETableAdapter.Fill(this.dSPesees.COM_COMMUNE);
            rEG_REGIONTableAdapter.Fill(this.dSPesees.REG_REGION);
            lIC_LIEU_COMMUNETableAdapter.Fill(this.dSPesees.LIC_LIEU_COMMUNE);
            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(this.dSPesees.LIE_LIEU_DE_PRODUCTION);
            _manager = new FormSettingsManagerLieuDeProduction(this, lIE_IDTextBox, lIE_NOMTextBox, btnAddLie, btnSaveLie, btnSupprimer, lIE_LIEU_DE_PRODUCTIONDataGridView,
                lIE_LIEU_DE_PRODUCTIONBindingSource, tableAdapterManager, dSPesees, cmdAddLIC, cmdDeleteLIC, cOM_IDComboBox, listLic, lIC_LIEU_COMMUNETableAdapter, lIE_IDTextBox, cOM_COMMUNETableAdapter);
            _manager.AddControl(lIE_NOMTextBox);
            _manager.AddControl(rEG_IDComboBox);
            _manager.AddControl(lIE_NUMEROTextBox);
            _manager.AddControl(cOM_IDComboBox);
            _manager.AddControl(cmdAddLIC);
            _manager.AddControl(cmdDeleteLIC);
            _manager.AddControl(listLic);
        }

        private void lIE_LIEU_DE_PRODUCTIONDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == lIE_LIEU_DE_PRODUCTIONDataGridView.Columns["REG_ID"].Index)
            {
                lIE_LIEU_DE_PRODUCTIONBindingSource.RemoveSort();
                lIE_LIEU_DE_PRODUCTIONTableAdapter.FillOrdeByRegNom(dSPesees.LIE_LIEU_DE_PRODUCTION);
                lIE_LIEU_DE_PRODUCTIONDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void regionMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesRegions.Wfrm.WfrmShowDialog();
            rEG_REGIONTableAdapter.Fill(dSPesees.REG_REGION);
        }

        private void communeMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCommunes.Wfrm.WfrmShowDialog();
            cOM_COMMUNETableAdapter.Fill(dSPesees.COM_COMMUNE);
        }
    }
}