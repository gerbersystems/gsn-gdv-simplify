﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmConnectionStringMaster : WfrmBase
    {
        private readonly string _connexionString;

        public string ConnexionString
        {
            get { return txtConnexionString.Text; }
        }

        public WfrmConnectionStringMaster(string connexionString)
        {
            InitializeComponent();
            _connexionString = connexionString;
            txtConnexionString.Text = _connexionString;
        }

        private void OnBtnModifierClick(object sender, EventArgs e)
        {
            txtConnexionString.Enabled = true;
            btnAnnuler.Enabled = true;
            btnModifier.Enabled = false;
        }

        private void OnBtnAnnulerClick(object sender, EventArgs e)
        {
            txtConnexionString.Text = _connexionString;
            txtConnexionString.Enabled = true;
            btnModifier.Enabled = true;
            btnAnnuler.Enabled = false;
        }

        private void OnBtnOKClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}