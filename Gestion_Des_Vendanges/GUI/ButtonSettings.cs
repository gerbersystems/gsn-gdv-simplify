﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal interface IButtonSettings
    {
        string Text { get; set; }

        void Enabled();

        void Disabled();

        void AddClickEvent(EventHandler click);

        void RemoveClickEvent(EventHandler click);
    }

    internal class ButtonSettings : IButtonSettings
    {
        private Button _button;

        public string Text
        {
            get
            {
                return _button.Text;
            }
            set
            {
                _button.Text = value;
            }
        }

        public ButtonSettings(Button button)
        {
            _button = button;
        }

        public void Enabled()
        {
            _button.Show();
        }

        public void Disabled()
        {
            _button.Hide();
        }

        public void AddClickEvent(EventHandler click)
        {
            _button.Click += click;
        }

        public void RemoveClickEvent(EventHandler click)
        {
            _button.Click -= click;
        }
    }

    internal class ToolStripButtonSettings : IButtonSettings
    {
        private ToolStripButton _button;

        public string Text
        {
            get
            {
                return _button.Text;
            }
            set
            {
                _button.Text = value;
            }
        }

        public ToolStripButtonSettings(ToolStripButton button)
        {
            _button = button;
        }

        public void Enabled()
        {
            _button.Enabled = true;
        }

        public void Disabled()
        {
            _button.Enabled = false;
        }

        public void AddClickEvent(EventHandler click)
        {
            _button.Click += click;
        }

        public void RemoveClickEvent(EventHandler click)
        {
            _button.Click -= click;
        }
    }
}