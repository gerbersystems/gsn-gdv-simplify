﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using GSN.GDV.Data.Extensions;
using GSN.GDV.GUI.Common.Helpers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de saisie des pesées
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 06.11.2009 - Modification de la logique de conversion L/Kg
    *   JGA - 13.06.2014 - Ajout empêcher dépassement des pesées selon quota de l'acquit
    * */

    partial class WfrmSaisiePesee : WfrmBase
    {
        #region Properties

        private BUSINESS.Pesee _pesee;
        private WfrmSaisieEntetePesee _formEntetePesee;
        private readonly bool _newPesee;
        private decimal _litresPesees;
        private bool _loadingInProgress = true;
        private DEG_DEGRE_OECHSLE_BRIXTableAdapter dEGTableAdapter;
        private decimal conversionRate = 1;
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(WfrmSaisiePesee));
        private ErrorProvider soldeErrorProvider = null;
        private readonly SettingExtension.ApplicationUniteEnum ApplicationUnite;
        private bool limitationAlreadyRetrieved = false;
        //private List<Control> _lstControlsToShowNeuchatel;

        #endregion Properties

        #region Constructor

        // Constructor when NEW pesee
        public WfrmSaisiePesee(WfrmSaisieEntetePesee formEntetePesee, BUSINESS.EntetePesee entete)
        {
            _pesee = new Gestion_Des_Vendanges.BUSINESS.Pesee();

            InitializeComponent();
            _formEntetePesee = formEntetePesee;

            _pesee.Entete = entete;
            _pesee.CepageId = entete.CepageId ?? 0;
            _newPesee = true;
            _litresPesees = 0;
            //pED_LOTTextBox.Text = _pesee.Lot.ToString();
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                ApplicationUnite = GlobalParam.Instance.ApplicationUnite;
            }
            LogIt(_pesee, prefix: "WfrmSaisiePesee(WfrmSaisieEntetePesee formEntetePesee, BUSINESS.EntetePesee entete)");

            //HACK LIE |
            //_lstControlsToShowNeuchatel = new List<Control>
            //{
            //    cOM_IDCombobox, cOM_IDLabel
            //};
        }

        // Constructor when MODIFY pesee
        public WfrmSaisiePesee(WfrmSaisieEntetePesee formEntetePesee, BUSINESS.Pesee pesee)
        {
            _pesee = pesee;
            InitializeComponent();
            _formEntetePesee = formEntetePesee;

            _newPesee = false;
            lTitre.Text = "Modifier une pesée";
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                ApplicationUnite = GlobalParam.Instance.ApplicationUnite;
            }
            LogIt(_pesee, prefix: "(WfrmSaisieEntetePesee formEntetePesee, BUSINESS.Pesee pesee).");

            //HACK LIE
            //_lstControlsToShowNeuchatel = new List<Control>
            //{
            //    cOM_IDCombobox, cOM_IDLabel
            //};
        }

        private void OnLoad(object sender, EventArgs e)
        {
            pED_SONDAGE_OETextBox.Validated += pED_SONDAGE_OETextBox_Validated;

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                try
                {
                    switch (BUSINESS.GlobalParam.Instance.Canton)
                    {
                        case SettingExtension.CantonEnum.Neuchâtel:

                            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
                            ConnectionManager.Instance.AddGvTableAdapter(peL_PED_LIETableAdapter);

                            cOM_COMMUNETableAdapter.Fill(dSPesees.COM_COMMUNE);
                            peL_PED_LIETableAdapter.Fill(dSPesees.PEL_PED_LIE);
                            cEP_IDComboBox.Enabled = true;
                            break;

                        case SettingExtension.CantonEnum.Vaud:
                            cEP_IDComboBox.Enabled = false;
                            break;
                    }

                    ConnectionManager.Instance.AddGvTableAdapter(pED_PESEE_DETAILTableAdapter);
                    ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
                    ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
                    ConnectionManager.Instance.AddGvTableAdapter(pAM_PARAMETRESTableAdapter);
                    ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
                    dEGTableAdapter = ConnectionManager.Instance.CreateGvTableAdapter<DEG_DEGRE_OECHSLE_BRIXTableAdapter>();
                    ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);

                    aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
                    pAM_PARAMETRESTableAdapter.Fill(dSPesees.PAM_PARAMETRES);
                    cEP_CEPAGETableAdapter.FillByAcqId(dSPesees.CEP_CEPAGE, _pesee.Entete.AcquitId);
                    pED_PESEE_DETAILTableAdapter.Fill(dSPesees.PED_PESEE_DETAIL);
                    lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message, "Erreur d'accès à la base de données.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                }

                var selected = pED_PESEE_DETAILBindingSource.AddNew();
                if (selected != null && selected is System.Data.DataRowView)
                {
                    var drv = selected as System.Data.DataRowView;
                    var dr = drv.Row as DSPesees.PED_PESEE_DETAILRow;
                    if (dr != null)
                    {
                        try
                        {
                            dr.PEE_ID = ConverterHelper.To(pEE_IDTextBox.Text, _pesee.Entete.Id);
                            dr.PED_SONDAGE_OE = Convert.ToDouble(Math.Max(pED_SONDAGE_OETextBox.Value, _pesee.SondageOE));
                            dr.PED_SONDAGE_BRIKS = Convert.ToDouble(Math.Max(pED_SONDAGE_BRIKSTextBox.Value, _pesee.SondageBriks));
                            Log.DebugFormat("dr.PED_SONDAGE_BRIKS={0}, dr.PED_SONDAGE_OE={1}", dr.PED_SONDAGE_BRIKS, dr.PED_SONDAGE_OE);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("OnLoad", ex);
                            throw ex;
                        }
                    }
                }
            }

            StyleBuilderExtension.ActivateStyle(StyleBuilderExtension.TextBoxStyleEnum.ReadOnly,
                pED_QUANTITE_KGTextBox,
                pED_QUANTITE_LITRESTextBox,
                //pED_SONDAGE_BRIKSTextBox,
                //pED_SONDAGE_OETextBox,
                txtKilosAdmis,
                txtKilosSaisis,
                txtConversionLKg,
                txtLitresAdmis,
                txtLitresSaisis
            );

            //ValidationBuilderExtension.ApplyValidation(pED_LOTTextBox, type:ValidationBuilderExtension.ValidationTypeEnum.Integer);
            //ValidationBuilderExtension.ApplyValidation(pED_SONDAGE_OETextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            //ValidationBuilderExtension.ApplyValidation(pED_QUANTITE_KGTextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            //ValidationBuilderExtension.ApplyValidation(pED_QUANTITE_LITRESTextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);

            pED_IDTextBox.Hide();
            pEE_IDTextBox.Text = _pesee.Entete.Id + "";
            pEE_IDTextBox.Hide();

            txtLitresAdmis.ReadOnly = true;
            txtLitresSaisis.ReadOnly = true;

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                txtProducteur.Text = string.Concat(pED_PESEE_DETAILTableAdapter.GetProducteurPrenomNom(_pesee.Entete.Id));

            if (!_newPesee)
            {
                setChamps();
                _litresPesees = decimal.Parse(pED_QUANTITE_LITRESTextBox.Text);
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                    if (pED_PESEE_DETAILTableAdapter.NbRef(_pesee.Id) > 0)
                    {
                        MessageBox.Show("Cette pesée est verrouillée car un ou plusieurs acomptes ont été payés.\nVeuillez annuler ces paiements pour pouvoir modifier cette pesée",
                            "Pesée verrouillée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        optBlocageConversion.Enabled = false;
                        optBlocageKg.Enabled = false;
                        optBlocageLitre.Enabled = false;
                        pED_QUANTITE_KGTextBox.ReadOnly = true;
                        pED_QUANTITE_LITRESTextBox.ReadOnly = true;
                        pED_SONDAGE_BRIKSTextBox.ReadOnly = true;
                        pED_SONDAGE_OETextBox.ReadOnly = true;
                        txtConversionLKg.ReadOnly = true;
                        cEP_IDComboBox.Enabled = false;
                        cmdOK.Text = "OK";
                    }
                /// <TODO>Je ne suis pas sure de cette partie!!!</TODO>
                _ActivateCepageDependantControls(needBeActivated: _pesee.CepageId > 0);
            }
            else
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                using (var db = ConnectionManager.Instance.CreateMainDataContext())
                {
                    _pesee.Lot = PeseeDetailExtension.NextLot(db, Year: Utilities.AnneeCourante);
                }
                pED_LOTTextBox.Text = _pesee.Lot.ToString();
                _ActivateCepageDependantControls(needBeActivated: _pesee.CepageId > 0);
            }
            _loadingInProgress = false;
            cEP_IDComboBox.SelectedValue = _pesee.CepageId;

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                limitationLitresParCepageGroupBox.Visible = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre;
                limitationKilogrammesParCepageGroupBox.Visible = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Kilo;

                pED_QUANTITE_KGTextBox.ReadOnly = limitationLitresParCepageGroupBox.Visible == false;
                pED_QUANTITE_LITRESTextBox.ReadOnly = limitationKilogrammesParCepageGroupBox.Visible == false;

                optBlocageKg.Enabled = pED_QUANTITE_KGTextBox.ReadOnly == false;
                optBlocageLitre.Enabled = pED_QUANTITE_LITRESTextBox.ReadOnly == false;

                sondageBrixGroupBox.Visible = (GlobalParam.Instance.ShowSondageBrix);
                sondageOxelGroupBox.Visible = sondageBrixGroupBox.Visible == false;
            }

            updateLimiteLitres();
            optBlocageKg_CheckedChanged(sender: sender, e: e);
            LogIt(_pesee, prefix: "OnLoad(object sender, EventArgs e)");
        }

        #endregion Constructor

        private void LogIt(BUSINESS.Pesee v, string prefix = null)
        {
            Log.DebugFormat(@"{0}{1}", prefix, v);
        }

        private void setChamps()
        {
            cEP_IDComboBox.SelectedValue = _pesee.CepageId;
            pED_LOTTextBox.Text = _pesee.Lot.ToString();
            pED_SONDAGE_BRIKSTextBox.Value = Math.Max(pED_SONDAGE_BRIKSTextBox.Minimum, (_pesee.SondageBriks > 30) ? Convert.ToDecimal((double)dEGTableAdapter.GetBrixByOechsle(pED_SONDAGE_OETextBox.Value)) : _pesee.SondageBriks);
            pED_SONDAGE_OETextBox.Value = Math.Max(pED_SONDAGE_OETextBox.Minimum, _pesee.SondageOE);
            pED_IDTextBox.Text = (_pesee.Entete == null ? 0 : _pesee.Entete.Id).ToString();
            pED_QUANTITE_KGTextBox.Text = string.Format("{0:0.00}", _pesee.QuantiteKg);
            pED_QUANTITE_LITRESTextBox.Text = string.Format("{0:0.00}", _pesee.QuantiteLitres);

            //HACK LIE | To implement with Lieu de production
            //switch (BUSINESS.GlobalParam.Instance.Canton)
            //{
            //    case SettingExtension.CantonEnum.Neuchâtel:
            //        cOM_IDCombobox.SelectedValue = _pesee.Commune;
            //        break;
            //}

            updateTauxConversion();
        }

        private bool updatePesee()
        {
            bool result = true;
            try
            {
                _pesee.CepageId = (int)cEP_IDComboBox.SelectedValue;
                _pesee.SondageBriks = pED_SONDAGE_BRIKSTextBox.Value;
                _pesee.SondageOE = pED_SONDAGE_OETextBox.Value;
                _pesee.Lot = Convert.ToInt32(pED_LOTTextBox.Text);
                _pesee.QuantiteKg = ConverterHelper.To(pED_QUANTITE_KGTextBox.Text, _pesee.QuantiteKg);
                _pesee.QuantiteLitres = ConverterHelper.To(pED_QUANTITE_LITRESTextBox.Text, _pesee.QuantiteLitres);

                //HACK LIE | To implement with Lieu de production
                //switch (BUSINESS.GlobalParam.Instance.Canton)
                //{
                //    case SettingExtension.CantonEnum.Neuchâtel:
                //        _pesee.Commune = (int)cOM_IDCombobox.SelectedValue;
                //        break;
                //}
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void _ActivateCepageDependantControls(bool needBeActivated)
        {
            ControlHelper.ActiveIt(needBeActivated,
                pED_LOTTextBox,
                pED_SONDAGE_BRIKSTextBox,
                pED_SONDAGE_OETextBox,
                pED_QUANTITE_KGTextBox
            );
        }

        private bool chkSaisie()
        {
            var messageList = new System.Collections.Specialized.StringCollection();
            bool result = true;
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    ValidationBuilderExtension.Validate(cEP_IDComboBox, cEP_IDComboBox.SelectedValue, type: ValidationBuilderExtension.ValidationTypeEnum.Integer);
                    ValidationBuilderExtension.Validate(pED_LOTTextBox, pED_LOTTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);

                    if (sondageOxelGroupBox.Visible)
                        ValidationBuilderExtension.Validate(pED_SONDAGE_OETextBox, pED_SONDAGE_OETextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
                    if (limitationKilogrammesParCepageGroupBox.Visible)
                        ValidationBuilderExtension.Validate(pED_QUANTITE_KGTextBox, pED_QUANTITE_KGTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);

                    if (limitationLitresParCepageGroupBox.Visible)
                        ValidationBuilderExtension.Validate(pED_QUANTITE_LITRESTextBox, pED_QUANTITE_LITRESTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);

                    if (sondageOxelGroupBox.Visible)
                        ValidationBuilderExtension.Validate(pED_SONDAGE_BRIKSTextBox, pED_SONDAGE_BRIKSTextBox.Text, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);

                    //float testFloat;
                    //testInt = (int)cEP_IDComboBox.SelectedValue;
                    //testFloat = float.Parse(pED_SONDAGE_OETextBox.Text);
                    //testFloat = float.Parse(pED_SONDAGE_BRIKSTextBox.Text);

                    //float testFloat;
                    if (pED_QUANTITE_KGTextBox.ReadOnly == false && pED_QUANTITE_KGTextBox.Enabled == true && ConverterHelper.To(pED_QUANTITE_KGTextBox.Text, 0.0) <= 0)
                    {
                        var description = ("Une pesée doit être plus grande que 0 kg.");
                        ValidationBuilderExtension.Validate(pED_QUANTITE_KGTextBox, isValid: false, description: description);
                        messageList.Add(description);
                        result = false;
                    }
                    else
                        ValidationBuilderExtension.Validate(pED_QUANTITE_KGTextBox, isValid: true);

                    if (pED_QUANTITE_KGTextBox.ReadOnly == false && pED_QUANTITE_KGTextBox.Enabled == true && ConverterHelper.To(pED_QUANTITE_LITRESTextBox.Text, 0.0) <= 0)
                    {
                        var description = ("Une pesée doit contenir plus de 0 litre.");
                        ValidationBuilderExtension.Validate(pED_QUANTITE_KGTextBox, isValid: false, description: description);
                        messageList.Add(description);
                        result = false;
                    }
                    else
                        ValidationBuilderExtension.Validate(pED_QUANTITE_KGTextBox, isValid: true);
                    int testInt;
                    using (var db = ConnectionManager.Instance.CreateMainDataContext())
                    {
                        if (int.TryParse(System.Text.RegularExpressions.Regex.Replace(pED_LOTTextBox.Text, "[^0-9]", ""), out testInt) == false)
                        {
                            var description = "Le numéro de lot n'est pas valide";
                            ValidationBuilderExtension.Validate(pED_LOTTextBox, isValid: false, description: description);
                            messageList.Add(description);
                            result = false;
                        }
                        else if (PeseeDetailExtension.IsUniqueLot(db, Lot: testInt, ExcludedId: _pesee.Id) == false)
                        {
                            var description = "Le numéro de lot " + testInt + " est déja utilisé";
                            ValidationBuilderExtension.Validate(pED_LOTTextBox, isValid: false, description: description);
                            messageList.Add(description);
                            result = false;
                        }
                        else
                            ValidationBuilderExtension.Validate(pED_LOTTextBox, isValid: true);
                    }

                    switch (BUSINESS.GlobalParam.Instance.Canton)
                    {
                        case SettingExtension.CantonEnum.Neuchâtel:
                            testInt = (int)cOM_IDCombobox.SelectedValue;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log.Warn("chkSaisie", ex);
                    messageList.Add("Veuillez saisir tous les champs correctement.");
                    result = false;
                }
            if (result == false && messageList.Count > 0)
            {
                var message = new StringBuilder();
                foreach (var v in messageList)
                    message.Append("- ").AppendLine(v);
                MessageBox.Show(message.ToString(), "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return result;
        }

        private void savePesee()
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                Validate();
                pED_PESEE_DETAILBindingSource.EndEdit();
                updatePesee();
                tableAdapterManager.UpdateAll(this.dSPesees);

                switch (BUSINESS.GlobalParam.Instance.Canton)
                {
                    case SettingExtension.CantonEnum.Neuchâtel:
                        //HACK LIE | pEC_PED_COMTableAdapter.Insert(Convert.ToInt32(pED_IDTextBox.Text), (int)cOM_IDCombobox.SelectedValue);
                        peL_PED_LIETableAdapter.Insert(Convert.ToInt32(pED_IDTextBox.Text), (int)cOM_IDCombobox.SelectedValue);
                        break;
                }
            }
        }

        private void updateTauxConversion()
        {
            try
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var PeseeEnteteId = ConverterHelper.To(pED_IDTextBox.Text, 0);

                    var quantiteKilogrammes = ConverterHelper.To(pED_QUANTITE_KGTextBox.Text, 0.0d);
                    var quantiteLites = ConverterHelper.To(pED_QUANTITE_LITRESTextBox.Text, 0.0d);
                    double taux = Math.Round(quantiteLites / (quantiteKilogrammes == 0 ? 1 : quantiteKilogrammes), 4);
                    txtConversionLKg.Text = string.Format("{0:0.00}", taux);

                    if (taux > Properties.Settings.Default.maximumLegalRateAllowedLitersKilos)
                    {
                        var description = string.Format("Attention le taux de conversion {0:0.00} litre/kg dépasse la limite {1:0.00} acceptée par la loi!", taux, Properties.Settings.Default.maximumLegalRateAllowedLitersKilos);
                        MessageBox.Show(
                            description,
                            "Taux de conversion",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation
                        );
                        ValidationBuilderExtension.Validate(txtConversionLKg, isValid: false, description: description);
                    }
                    else
                    {
                        ValidationBuilderExtension.Validate(txtConversionLKg, isValid: true);
                    }
                    if (double.IsNaN(taux) || double.IsInfinity(taux))
                    {
                        txtConversionLKg.Text = string.Format("{0:0.00}", Properties.Settings.Default.maximumLegalRateAllowedLitersKilos);
                    }
                    else
                    {
                        txtConversionLKg.Text = string.Format("{0:0.00}", taux);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Warn("updateTauxConversion", ex);
            }
        }

        private bool updateFormPesee()
        {
            var txtSaisis = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresSaisis : txtKilosSaisis;
            var txtAdmins = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresAdmis : txtKilosAdmis;
            var txtSolde = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? soldeLitresTextBox : soldeKilosTextBox;

            decimal valeurSaisis = Math.Round(ConverterHelper.To(txtSaisis.Text, 0m), 2);
            decimal valeurAdmis = Math.Round(ConverterHelper.To(txtAdmins.Text, 0m), 2);
            decimal valeurSolde = valeurAdmis - valeurSaisis;
            bool ok = true;
            if (valeurSolde < 0)
            {
                if (
                    MessageBox.Show(string.Format(
                            "Attention, vous avez saisi {0:0.00} la limite est {1:0.0} vous avez dépassé la limite par cépage qui est {2:0.0} .",
                            valeurSaisis,
                            valeurAdmis,
                            valeurSolde
                        ),
                    "Limite dépassée",
                    MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Exclamation) == DialogResult.Cancel || GlobalParam.Instance.AcceptExceedingQuota == false)
                {
                    ok = false;
                }
            }
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                if (ok)
                {
                    if (_newPesee)
                    {
                        savePesee();
                        _formEntetePesee.UpdateDetailPesee();
                    }
                    else
                    {
                        updatePesee();
                        _formEntetePesee.UpdateCurrentRow(_pesee);
                    }
                    this.Close();
                }
            return ok;
        }

        private void updateLimiteLitres()
        {
            if (_loadingInProgress)
                return;
            //var parcelleTableAdapter = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PAR_PARCELLETableAdapter>();
            var PeseeEnteteId = ConverterHelper.To(pEE_IDTextBox.Text, 0);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    int CepageId = ConverterHelper.To(cEP_IDComboBox.SelectedValue, 0);
                    double valeurAdmis = 0;
                    double valeurSaisiGlobalment = 0;

                    var txtAdmis = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresAdmis : txtKilosAdmis;
                    var txtSaisisGlobalment = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresSaisis : txtKilosSaisis;
                    var txtSolde = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? soldeLitresTextBox : soldeKilosTextBox;
                    var txtSaisis = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? pED_QUANTITE_LITRESTextBox : pED_QUANTITE_KGTextBox;

                    //var v = parcelleTableAdapter.GetSumLitresByCepageAndPee(CepageId, PeseeEnteteId);

                    //double.TryParse("" + v, out litresAdmis);
                    if (limitationAlreadyRetrieved == false)
                    {
                        var ExcludedId = _pesee.Id;
                        var LieuId = _pesee.Entete.LieuProductionId;
                        var AcquitId = 0;
                        using (var db = ConnectionManager.Instance.CreateMainDataContext())
                        {
                            AcquitId = (from t in db.PeseeEnteteSet where t.Id == PeseeEnteteId select t.AcquitId).FirstOrDefault() ?? 0;
                        }
                        if (AcquitId > 0 && LieuId > 0 && CepageId > 0)
                        {
                            valeurAdmis = GlobalParam.LimitationProduction.GetMaximumByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, UniteType: ApplicationUnite) ?? 0;
                            valeurSaisiGlobalment = GlobalParam.LimitationProduction.GetSaisiByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedId, UniteType: ApplicationUnite) ?? 0;
                        }
                        txtAdmis.Text = string.Format("{0:0.00}", Math.Round(valeurAdmis, 2));
                        txtSaisisGlobalment.Text = string.Format("{0:0.00}", Math.Round(valeurSaisiGlobalment, 2));
                        limitationAlreadyRetrieved = true;
                    }
                    else
                    {
                        valeurAdmis = ConverterHelper.To(txtAdmis.Text, valeurAdmis);
                        valeurSaisiGlobalment = ConverterHelper.To(txtAdmis.Text, valeurSaisiGlobalment);
                    }

                    //!Modifier méthode de calcul pour kilos admis****************************************************************
                    double valeurSaisis = ConverterHelper.To(txtSaisis.Text, 0.0);
                    //************************************************************************************************************

                    txtSaisis.Text = string.Format("{0:0.00}", Math.Round(valeurSaisis, 2));
                    txtAdmis.Text = string.Format("{0:0.00}", Math.Round(valeurAdmis, 2));

                    try
                    {
                        /*
                         SELECT SUM(PED.PED_QUANTITE_LITRES) FROM PED_PESEE_DETAIL AS PED, PEE_PESEE_ENTETE AS PEE
                        WHERE PED.CEP_ID = @CEP_ID AND PED.PEE_ID = PEE.PEE_ID AND
                        PEE.LIE_ID IN (SELECT LIE_ID FROM PEE_PESEE_ENTETE WHERE PEE_ID = @PEE_ID)
                        AND PEE.ACQ_ID IN (SELECT ACQ_ID FROM ACQ_ACQUIT WHERE
                        CLA_ID IN
                        (SELECT CLA_ID FROM ACQ_ACQUIT WHERE ACQ_ID IN (SELECT ACQ_ID FROM PEE_PESEE_ENTETE WHERE PEE_ID = @PEE_ID))
                        AND ANN_ID IN
                        (SELECT ANN_ID FROM ACQ_ACQUIT WHERE ACQ_ID IN (SELECT ACQ_ID FROM PEE_PESEE_ENTETE WHERE PEE_ID = @PEE_ID)))
                         */
                        //v = pED_PESEE_DETAILTableAdapter.GetSumLitresByCepageAndPEE(CepageId, PeseeEnteteId);

                        ConnectionManager.Instance.AddGvTableAdapter(pEE_TOTAL_QUANTITETableAdapter);
                        pEE_TOTAL_QUANTITETableAdapter.Fill(dSPesees.PEE_TOTAL_QUANTITE);

                        _RecalculateSolde();
                    }
                    catch (NullReferenceException)
                    {
                    }
                }
                catch (NullReferenceException)
                {
                }
        }

        private void _RecalculateSolde()
        {
            if (_loadingInProgress)
                return;

            double litresAdmis = 0;
            double litresSaisi = 0;
            double kilosSaisi = 0;
            double kilosAdmis = 0;
            double litreParCepageEtPesee = 0;
            double kilosParCepageEtPesee = 0;

            double.TryParse(txtLitresSaisis.Text, out litreParCepageEtPesee);
            double.TryParse(txtKilosSaisis.Text, out kilosParCepageEtPesee);
            double.TryParse(txtLitresAdmis.Text, out litresAdmis);
            double.TryParse(txtKilosAdmis.Text, out kilosAdmis);

            double.TryParse(pED_QUANTITE_LITRESTextBox.Text, out litresSaisi);
            double.TryParse(pED_QUANTITE_KGTextBox.Text, out kilosSaisi);

            var litresCourrant = (litresSaisi + litreParCepageEtPesee);
            var kilosCourrant = (kilosSaisi + kilosParCepageEtPesee);

            var soldeLitres = litresAdmis - litresCourrant;
            var soldeKilos = kilosAdmis - kilosCourrant;

            soldeLitresTextBox.Text = string.Format("{0:0.00}", Math.Round(soldeLitres, 2));
            soldeKilosTextBox.Text = string.Format("{0:0.00}", Math.Round(soldeKilos, 2));

            var txtSaisis = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresSaisis : txtKilosSaisis;
            var txtAdmins = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? txtLitresAdmis : txtKilosAdmis;
            var txtSolde = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? soldeLitresTextBox : soldeKilosTextBox;

            var quantiteAdmis = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? litresAdmis : kilosAdmis;
            var quantiteSaisie = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? litresSaisi : kilosSaisi;
            var solde = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre ? soldeLitres : soldeKilos;

            if (soldeErrorProvider == null)
                soldeErrorProvider = ValidationBuilderExtension.CreateErrorProvider(txtSolde, this);
            if (solde < 0)
            {
                var message = string.Format("Vous avez saisi {0:0.00} la limite est de {1:0.00} le solde est de {2:0.00}", quantiteSaisie, quantiteAdmis, solde);
                soldeErrorProvider.SetError(txtSolde, message);
            }
            else
            {
                soldeErrorProvider.SetError(txtSolde, string.Empty);
            }
        }

        private void changeConversion()
        {
            try
            {
                if (optBlocageLitre.Checked)
                {
                    if (txtConversionLKg.Text == "0" || txtConversionLKg.Text == "")
                    {
                        txtConversionLKg.Text = string.Format("{0:0.00}", conversionRate);
                    }
                    pED_QUANTITE_LITRESTextBox.Text = string.Format("{0:0.00}", (Math.Round(decimal.Parse(pED_QUANTITE_KGTextBox.Text) * decimal.Parse(txtConversionLKg.Text), 2)));
                }
                else if (optBlocageKg.Checked)
                {
                    var litre = decimal.Parse(pED_QUANTITE_LITRESTextBox.Text);
                    var conversionLitreKg = conversionRate;
                    if (decimal.TryParse(txtConversionLKg.Text, out conversionLitreKg) == false || conversionLitreKg == 0)
                        conversionLitreKg = conversionRate;

                    pED_QUANTITE_KGTextBox.Text = conversionLitreKg == 0 ? "0" : (Math.Round(litre / conversionLitreKg, 2)).ToString();
                }
                else
                {
                    if (pED_QUANTITE_KGTextBox.Text == "0" || txtConversionLKg.Text == "")
                    {
                        txtConversionLKg.Text = "0";
                    }
                    else
                    {
                        txtConversionLKg.Text = (Math.Round(decimal.Parse(pED_QUANTITE_LITRESTextBox.Text) / decimal.Parse(pED_QUANTITE_KGTextBox.Text), 4)).ToString();
                    }
                }
                //var limiteTauxDeConversion=GlobalParam.Instance.GetTauxConversion()
                //if (double.Parse(txtConversionLKg.Text) > 0.8)
                if (decimal.Parse(txtConversionLKg.Text) > conversionRate)
                {
                    if (txtConversionLKg.BackColor != Color.Red)
                    {
                        MessageBox.Show("Attention le taux de conversion litres/kg dépasse la limite acceptée par la loi!", "Taux de conversion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        Log.InfoFormat("Le taux de conversion {0} dépasse la limite {1}", txtConversionLKg.Text, conversionRate);
                        txtConversionLKg.BackColor = Color.Red;
                    }
                }
                else
                {
                    if (optBlocageConversion.Checked)
                    {
                        txtConversionLKg.BackColor = Color.FromKnownColor(KnownColor.Control);
                    }
                    else
                    {
                        txtConversionLKg.BackColor = Color.White;
                    }
                }
                updateLimiteLitres();
            }
            catch
            {
            }
        }

        private void showControls(List<Control> lstControlsToShow)
        {
            if (lstControlsToShow.Count <= 0) return;

            foreach (Control c in lstControlsToShow)
            {
                c.Visible = true;
            }
        }

        #region Button Event

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPrintQuittance_Click(object sender, EventArgs e)
        {
            if (chkSaisie())
            {
                if (updateFormPesee())
                {
                    CtrlReportViewer.ShowQuittanceV2Pesee(_pesee.Entete.Id);
                }
            }
        }

        private void accederAuxCepagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCepages.Wfrm.WfrmShowDialog();
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                cEP_CEPAGETableAdapter.FillByAutLieAcq(dSPesees.CEP_CEPAGE, _pesee.Entete.AutreMentionId, _pesee.Entete.AcquitId, _pesee.Entete.LieuProductionId);
        }

        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                if (chkSaisie())
                    updateFormPesee();
            }
        }

        private void pED_PESEE_DETAILBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pED_PESEE_DETAILBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dSPesees);
        }

        #endregion Button Event

        #region Events

        private void pED_QUANTITE_KGTextBox_TextChanged(object sender, EventArgs e)
        {
            _RecalculateSolde();
        }

        private void pED_QUANTITE_LITRESTextBox_TextChanged(object sender, EventArgs e)
        {
            _RecalculateSolde();
        }

        private void optBlocageKg_CheckedChanged(object sender, EventArgs e)
        {
            txtConversionLKg.ReadOnly = optBlocageConversion.Checked;
            pED_QUANTITE_KGTextBox.ReadOnly = optBlocageKg.Checked;
            pED_QUANTITE_LITRESTextBox.ReadOnly = optBlocageLitre.Checked;

            if (ApplicationUnite == SettingExtension.ApplicationUniteEnum.Kilo && pED_QUANTITE_KGTextBox.ReadOnly && txtConversionLKg.ReadOnly)
                pED_QUANTITE_KGTextBox.ReadOnly = false;

            if (ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre && pED_QUANTITE_LITRESTextBox.ReadOnly && txtConversionLKg.ReadOnly)
                pED_QUANTITE_LITRESTextBox.ReadOnly = false;

            //txtConversionLKg.BackColor = txtConversionLKg.ReadOnly ? Color.LightYellow : Color.FromKnownColor(KnownColor.Control);
            //pED_QUANTITE_LITRESTextBox.BackColor = pED_QUANTITE_LITRESTextBox.ReadOnly ? Color.LightYellow : Color.FromKnownColor(KnownColor.Control);
            //pED_QUANTITE_KGTextBox.BackColor = pED_QUANTITE_LITRESTextBox.ReadOnly ? Color.LightYellow : Color.FromKnownColor(KnownColor.Control);

            optBlocageKg.Enabled = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Kilo;
            optBlocageLitre.Enabled = ApplicationUnite == SettingExtension.ApplicationUniteEnum.Litre;
        }

        private void ConversionChanged(object sender, EventArgs e)
        {
            changeConversion();
        }

        private void pED_SONDAGE_BRIKSTextBox_Validated(object sender, EventArgs e)
        {
            try
            {
                decimal brix = pED_SONDAGE_BRIKSTextBox.Value;
                pED_SONDAGE_OETextBox.Value = decimal.Parse(dEGTableAdapter.GetOechsleByBrix(brix) + " ");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("La valeur du sondage est en dehors des limites.", "Valeur du sondage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                ex.ToString();
            }
            catch
            {
                pED_SONDAGE_OETextBox.Value = (pED_SONDAGE_OETextBox.Minimum);
            }
        }

        private void pED_SONDAGE_OETextBox_Validated(object sender, EventArgs e)
        {
            try
            {
                pED_SONDAGE_BRIKSTextBox.Value = Convert.ToDecimal((double)dEGTableAdapter.GetBrixByOechsle(pED_SONDAGE_OETextBox.Value));
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("La valeur du sondage est en dehors des limites.", "Valeur du sondage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                pED_SONDAGE_BRIKSTextBox.Value = pED_SONDAGE_BRIKSTextBox.Minimum;
            }
        }

        private void cEP_IDComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            limitationAlreadyRetrieved = false;
            if (_loadingInProgress)
                return;

            _pesee.CepageId = ConverterHelper.To(cEP_IDComboBox.SelectedValue, _pesee.CepageId);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                if (_pesee.CepageId > 0)
                {
                    CEP_CEPAGETableAdapter cepTableAdapter = DAO.ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();

                    AnneeExtension.AnneeCouleurVinEnum couleur = AnneeExtension.ToAnneeCouleurVinEnum(cepTableAdapter.GetCouleurIdById(_pesee.CepageId) ?? 1);
                    conversionRate = GlobalParam.Instance.GetTauxConversion(Annee: BUSINESS.Utilities.AnneeCourante, Couleur: couleur);

                    _ActivateCepageDependantControls(needBeActivated: true);
                    changeConversion();
                }
            updateLimiteLitres();
        }

        #endregion Events

        //private void txtConversionLKg_Validated(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        double litres = Math.Round(double.Parse(pED_QUANTITE_KGTextBox.Text) * double.Parse(txtConversionLKg.Text), 2);
        //        if (double.IsNaN(litres))
        //        {
        //            pED_QUANTITE_LITRESTextBox.Text = "0";
        //        }
        //        else
        //        {
        //            pED_QUANTITE_LITRESTextBox.Text = litres.ToString();
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}
    }
}