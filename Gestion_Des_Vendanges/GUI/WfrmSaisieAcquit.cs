﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Data.Extensions;
using log4net;
using System;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public enum ClasseAcquit
    {
        Classe1_AOC_GrandCru = 1,
        Classe1_PremierGrandCru = 2,
        Classe2_VinDePays = 3,
        Classe3_VinDeTable = 10
    }

    /* *
    *  Description: Formulaire de saisie des acquits
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmSaisieAcquit : WfrmSetting
    {
        #region Properties

        private ILog _log;
        private CtrlGestionDesAcquits _formAcquits;
        private CtrlCockpit _formCockpit;
        private Acquit _acquit;
        private bool _newAcquit = true;
        public int CurrentAcquitID => _acquit.Id;

        #endregion Properties

        #region Constructor

        public WfrmSaisieAcquit(CtrlGestionDesAcquits formAcquits)
        {
            InitializeComponent();
            _formAcquits = formAcquits;
        }

        public WfrmSaisieAcquit(CtrlGestionDesAcquits formAcquits, Acquit acquit)
            : this(formAcquits)
        {
            _acquit = acquit;
            _newAcquit = false;
        }

        public WfrmSaisieAcquit(CtrlCockpit cockpit)
        {
            InitializeComponent();
            _formAcquits = CtrlGestionDesAcquits.GetCurrentInstance();
            _formAcquits.FromCockpit = true;
            _formCockpit = cockpit;
        }

        public WfrmSaisieAcquit(CtrlCockpit cockpit, Acquit acquit)
            : this(cockpit)
        {
            _acquit = acquit;
            _newAcquit = false;
        }

        private void WfrmAddAcquit_Load(object sender, EventArgs e)
        {
            _log = LogManager.GetLogger(GetType());

            ConnectionManager.Instance.AddGvTableAdapter(prO_NOMCOMPLETTableAdapter1);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rEG_REGIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cLA_CLASSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(dIS_DISTRICTTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(zOT_ZONE_TRAVAILTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAR_SECTEUR_VISITETableAdapter);

            UpdateDonnees();
            pAR_PARCELLEDataGridView.MouseDoubleClick += BtnModifParcelle_Click;

            if (_newAcquit)
            {
                cbRecu.Checked = true;
                aCQ_RECEPTIONDateTimePicker.Value = DateTime.Today;
                aCQ_DATEDateTimePicker.Value = DateTime.Today;
                aCQ_ACQUITBindingSource.AddNew();
                aCQ_LITRESTextBox.Text = "0";
                aCQ_SURFACETextBox.Text = "0";
                districtComboBox.Enabled = false;
                cOM_IDComboBox.Enabled = false;

                if (_formCockpit?.ExploitantId > 0) pRO_ID_VENDANGEComboBox.SelectedValue = _formCockpit.ExploitantId;
            }
            else
            {
                SetChamps();
                UpdateDataGrid();
                UpdateEtatComboBox();
            }
            aNN_IDTextBox.Text = Utilities.IdAnneeCourante.ToString();
            aNN_IDTextBox.Visible = false;
            txtAnnee.Text = Utilities.AnneeCourante.ToString();
            aCQ_IDTextBox.Visible = false;

            cLA_IDComboBox.SelectedValue = _acquit?.ClasseId != null ? _acquit.ClasseId : (int)ClasseAcquit.Classe1_AOC_GrandCru;
        }

        #endregion Constructor

        #region Methods

        private void UpdateDonnees()
        {
            try
            {
                prO_NOMCOMPLETTableAdapter1.FillProducteurs(dsPesees1.PRO_NOMCOMPLET);
                pRO_NOMCOMPLETTableAdapter.FillProprietaires(dSPesees.PRO_NOMCOMPLET);
                rEG_REGIONTableAdapter.Fill(dSPesees.REG_REGION);
                aUT_AUTRE_MENTIONTableAdapter.Fill(dSPesees.AUT_AUTRE_MENTION);
                lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
                cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
                aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
                cLA_CLASSETableAdapter.Fill(dSPesees.CLA_CLASSE);
                cOU_COULEURTableAdapter.FillCouCepage(dSPesees.COU_COULEUR);
                aCQ_ACQUITTableAdapter.Fill(dSPesees.ACQ_ACQUIT);
                cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
                zOT_ZONE_TRAVAILTableAdapter.Fill(dSPesees.ZOT_ZONE_TRAVAIL);
                pAR_SECTEUR_VISITETableAdapter.Fill(dSPesees.PAR_SECTEUR_VISITE);
            }
            catch (ConstraintException ex)
            {
                _log.Error($"Error when Fill data in method {MethodBase.GetCurrentMethod().Name}", ex);
            }
            catch (Exception ex)
            {
                _log.Error($"Error in {MethodBase.GetCurrentMethod().Name}", ex);
                throw;
            }
        }

        public void UpdateDataGrid()
        {
            pAR_PARCELLETableAdapter.FillByAcquit(dSPesees.PAR_PARCELLE, new Nullable<int>(Int32.Parse(aCQ_IDTextBox.Text)));
            aUT_AUTRE_MENTIONTableAdapter.Fill(dSPesees.AUT_AUTRE_MENTION);
        }

        private void SetChamps()
        {
            cLA_IDComboBox.SelectedValue = _acquit.ClasseId;
            cOM_IDComboBox.SelectedValue = _acquit.CommuneId;
            cOU_IDComboBox.SelectedValue = _acquit.CouleurId;
            try
            {
                aCQ_DATEDateTimePicker.Value = _acquit.Date;
            }
            catch
            {
                aCQ_DATEDateTimePicker.Value = DateTime.Today;
            }
            if (_acquit.DateReception == null)
            {
                aCQ_RECEPTIONDateTimePicker.Value = DateTime.Today;
                cbRecu.Checked = false;
                aCQ_RECEPTIONDateTimePicker.Enabled = false;
            }
            else
            {
                aCQ_RECEPTIONDateTimePicker.Value = (DateTime)_acquit.DateReception;
                cbRecu.Checked = true;
                aCQ_RECEPTIONDateTimePicker.Enabled = true;
            }
            aCQ_LITRESTextBox.Text = _acquit.Litres.ToString();
            aCQ_NUMEROTextBox.Text = _acquit.Numero.ToString();
            pRO_IDComboBox.SelectedValue = _acquit.ProprietaireId;
            aCQ_SURFACETextBox.Text = _acquit.Surface.ToString();
            aCQ_IDTextBox.Text = _acquit.Id.ToString();
            rEG_IDComboBox.SelectedValue = _acquit.RegionId;
            aCQ_NUMERO_COMPLTextBox.Text = _acquit.NumeroComplementaire.ToString();
            try
            {
                dIS_DISTRICTTableAdapter.FillByReg(dSPesees.DIS_DISTRICT, (int)rEG_IDComboBox.SelectedValue);
                districtComboBox.SelectedValue = dIS_DISTRICTTableAdapter.GetDisIdByComId(_acquit.CommuneId);
                cOM_COMMUNETableAdapter.FillByRegDis(dSPesees.COM_COMMUNE, (int)rEG_IDComboBox.SelectedValue, (int)districtComboBox.SelectedValue);
                cOM_IDComboBox.SelectedValue = _acquit.CommuneId;
            }
            catch
            {
                districtComboBox.Enabled = false;
                cOM_IDComboBox.Enabled = false;
            }
            pRO_ID_VENDANGEComboBox.SelectedValue = _acquit.ProprietaireVendangeId;
        }

        public bool UpdateAcquit()
        {
            bool result = true;
            try
            {
                if (_acquit == null)
                {
                    _acquit = new Acquit()
                    {
                        Id = Int32.Parse(aCQ_IDTextBox.Text),
                        AnneeId = Int32.Parse(aNN_IDTextBox.Text)
                    };
                }

                try
                {
                    _acquit.Date = (DateTime)aCQ_DATEDateTimePicker.Value;
                }
                catch
                {
                    _acquit.Date = DateTime.Today;
                }

                if (cbRecu.Checked)
                {
                    try
                    {
                        _acquit.DateReception = (DateTime)aCQ_RECEPTIONDateTimePicker.Value;
                    }
                    catch
                    {
                        _acquit.DateReception = DateTime.Today;
                    }
                }
                else
                {
                    _acquit.DateReception = null;
                }

                _acquit.Numero = Int32.Parse(aCQ_NUMEROTextBox.Text);
                _acquit.ProprietaireId = (int)pRO_IDComboBox.SelectedValue;
                _acquit.Surface = decimal.Parse(aCQ_SURFACETextBox.Text);
                _acquit.ProprietaireVendangeId = (int)pRO_ID_VENDANGEComboBox.SelectedValue;

                switch (GlobalParam.Instance.Canton)
                {
                    case SettingExtension.CantonEnum.Vaud:
                        _acquit.ClasseId = (int)cLA_IDComboBox.SelectedValue;
                        _acquit.CommuneId = (int)cOM_IDComboBox.SelectedValue;
                        _acquit.CouleurId = (int)cOU_IDComboBox.SelectedValue;
                        _acquit.Litres = decimal.Parse(aCQ_LITRESTextBox.Text);
                        _acquit.RegionId = (int)rEG_IDComboBox.SelectedValue;
                        try
                        {
                            _acquit.NumeroComplementaire = int.Parse(aCQ_NUMERO_COMPLTextBox.Text);
                        }
                        catch { }
                        break;
                    //TODO | Modifier les valeurs par défaut OU tenter valeur DBnull.value OU GetFirstOrDefault de chaque champs, sinon risque d'avoir une erreur lors de l'enregistrement : index non présent
                    case SettingExtension.CantonEnum.Neuchâtel:
                        _acquit.ClasseId = 1;
                        _acquit.CommuneId = 138;
                        _acquit.CouleurId = 1;
                        _acquit.Litres = 0;
                        _acquit.RegionId = 1;
                        _acquit.NumeroComplementaire = null;
                        break;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void Save_Acquits()
        {
            this.Validate();
            this.aCQ_ACQUITBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dSPesees);

            _acquit.Id = int.Parse(aCQ_IDTextBox.Text);
            _formAcquits?.UpdateAcquit(_acquit);
            _formCockpit?.UpdateAcquit();
        }

        private void CmdAjouter_Click(object sender, EventArgs e)
        {
            if (UpdateAcquit())
            {
                if (_newAcquit)
                {
                    Save_Acquits();
                }
                else
                {
                    _formAcquits?.UpdateAcquit(_acquit);
                    _formCockpit?.UpdateAcquit();
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Veuillez saisir tous les champs correctement.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool AjouterAcquit()
        {
            bool resultat = true;
            if (UpdateAcquit())
            {
                this.Save_Acquits();
                this.SetChamps();
                /* this.updateAcquit();
                 formAcquits.UpdateDataGrid();*/
                _newAcquit = false;
            }
            else
            {
                resultat = false;
                MessageBox.Show("Veuillez saisir tous les champs de l'entête correctement\navant d'ajouter une pesée.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return resultat;
        }

        private void UpdateTotaux()
        {
            int acqID = Int32.Parse(aCQ_IDTextBox.Text);

            aCQ_LITRESTextBox.Text = pAR_PARCELLETableAdapter.GetSumLitresByAcquit(acqID).ToString();
            aCQ_SURFACETextBox.Text = pAR_PARCELLETableAdapter.GetSumSurfaceByAcquit(acqID).ToString();

            if (!decimal.TryParse(aCQ_LITRESTextBox.Text, out decimal test)) aCQ_LITRESTextBox.Text = "0";
            if (!decimal.TryParse(aCQ_SURFACETextBox.Text, out test)) aCQ_SURFACETextBox.Text = "0";

            if (UpdateAcquit()) _formAcquits?.UpdateAcquit(_acquit);
        }

        private Parcelle DataGridViewCellCollectionToParcelle(DataGridViewCellCollection row)
        {
            var parcelle = new Parcelle()
            {
                Id = (int)row["PAR_ID"].Value,
                Acquit = (int)row["CEP_ACQ_ID"].Value,
                Cepage = (int)row["CEP_ID"].Value
            };

            if (decimal.TryParse(row["PAR_LITRES"].Value.ToString(), NumberStyles.Any, new CultureInfo("fr-CH"), out decimal parLitresConvertedToDecimal))
                parcelle.Litres = parLitresConvertedToDecimal;

            if (decimal.TryParse(row["Kilos"].Value == null ? @"0" : row["Kilos"].Value.ToString(), NumberStyles.Any, new CultureInfo("fr-CH"), out decimal parKilosConvertedToDecimal))
                parcelle.Kilos = parKilosConvertedToDecimal;

            parcelle.Quota = decimal.Parse(row["DG_PAR_QUOTA"].Value.ToString());
            parcelle.SurfaceCepage = decimal.Parse(row["PAR_SURFACE_CEPAGE"].Value.ToString());
            parcelle.SurfaceComptee = decimal.Parse(row["PAR_SURFACE_COMPTEE"].Value.ToString());
            parcelle.Numero = (int)row["PAR_NUMERO"].Value;
            parcelle.AutreMention = (int)row["AUT_ID"].Value;

            if (!string.IsNullOrEmpty(row["ZOT_ID"].Value.ToString()))
                parcelle.ZoneTravail = (int)row["ZOT_ID"].Value;
            if (!string.IsNullOrEmpty(row["PAR_SECTEUR_VISITE"].Value.ToString()))
                parcelle.SecteurVisite = row["PAR_SECTEUR_VISITE"].Value.ToString();
            if (!string.IsNullOrEmpty(row["PAR_MODE_CULTURE"].Value.ToString()))
                parcelle.ModeCulture = row["PAR_MODE_CULTURE"].Value.ToString();

            parcelle.LieuProduction = (int)row["LIE_ID"].Value;

            return parcelle;
        }

        public void UpdateCurrentRow(Parcelle parcelle)
        {
            UpdateRowParcelle(this.pAR_PARCELLEDataGridView.CurrentRow.Cells, parcelle);
            SaveDataGrid();
        }

        private void UpdateRowParcelle(DataGridViewCellCollection row, Parcelle parcelle)
        {
            row["CEP_ID"].Value = parcelle.Cepage;
            row["PAR_NUMERO"].Value = parcelle.Numero;
            row["PAR_LITRES"].Value = parcelle.Litres;
            row["DG_PAR_QUOTA"].Value = parcelle.Quota;
            row["PAR_SURFACE_CEPAGE"].Value = parcelle.SurfaceCepage;
            row["PAR_SURFACE_COMPTEE"].Value = parcelle.SurfaceComptee;
            row["LIE_ID"].Value = parcelle.LieuProduction;
            row["AUT_ID"].Value = parcelle.AutreMention;

            if (parcelle.ZoneTravail.HasValue)
                row["ZOT_ID"].Value = parcelle.ZoneTravail;
            if (parcelle.SecteurVisite != null)
                row["PAR_SECTEUR_VISITE"].Value = parcelle.SecteurVisite;
            if (parcelle.ModeCulture != null)
                row["PAR_MODE_CULTURE"].Value = parcelle.ModeCulture;
        }

        private void SaveDataGrid()
        {
            this.Validate();
            this.pAR_PARCELLEBindingSource.EndEdit();
            this.pAR_PARCELLETableAdapter.Update(dSPesees);
        }

        private void UpdateEtatComboBox()
        {
            bool value = (pAR_PARCELLETableAdapter.GetNbParcellesByAcquit(Int32.Parse(aCQ_IDTextBox.Text)) == 0);
            cOU_IDComboBox.Enabled = value;
            cOM_IDComboBox.Enabled = value;
            rEG_IDComboBox.Enabled = value;
            districtComboBox.Enabled = value;
        }

        #endregion Methods

        #region Button Event

        private void BtnModifParcelle_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateAcquit();
                _formAcquits?.UpdateAcquit(_acquit);
                _formCockpit?.UpdateAcquit();
                var formSaisieParcelle = new WfrmSaisieParcelle(this,
                                                                DataGridViewCellCollectionToParcelle(pAR_PARCELLEDataGridView.CurrentRow.Cells),
                                                                (int)cOM_IDComboBox.SelectedValue)
                {
                    ShowInTaskbar = false
                };
                formSaisieParcelle.ShowDialog();
                UpdateTotaux();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Aucune parcelle n'est sélectionnée !", "Modifier parcelle", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BtnDeleteParcelle_Click(object sender, EventArgs e)
        {
            if (this.pAR_PARCELLEBindingSource.Current != null)
            {
                if (MessageBox.Show("Êtes vous sûr de vouloir supprimer cette parcelle ?", "Suppression d'une parcelle", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    this.pAR_PARCELLEBindingSource.RemoveCurrent();
                    SaveDataGrid();
                    UpdateEtatComboBox();
                    UpdateTotaux();
                }
            }
        }

        private void BtnAddParcelle_Click(object sender, EventArgs e)
        {
            try
            {
                AddParcelle();
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message, "Erreur du logiciel", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void AddParcelle()
        {
            if (IsNewAcquit())
            {
                var formParcelle = new WfrmSaisieParcelle(this, (int)cOM_IDComboBox.SelectedValue)
                {
                    ShowInTaskbar = false
                };
                formParcelle.ShowDialog();
                UpdateTotaux();
                UpdateEtatComboBox();
            }
        }

        private bool IsNewAcquit()
        {
            if (_newAcquit && !AjouterAcquit())
            {
                return false;
            }
            else
            {
                if (UpdateAcquit())
                {
                    _formAcquits?.UpdateAcquit(_acquit);
                    return true;
                }
                else
                {
                    MessageBox.Show("Veuillez saisir tous les champs de l'acquit correctement\navant d'ajouter une parcelle.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
        }

        #endregion Button Event

        #region Right Button Event

        private void CouleursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCouleurs.Wfrm.WfrmShowDialog();
            cOU_COULEURTableAdapter.FillCouCepage(dSPesees.COU_COULEUR);
        }

        private void AccederAuxCategoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesClasses.Wfrm.WfrmShowDialog();
            this.cLA_CLASSETableAdapter.Fill(this.dSPesees.CLA_CLASSE);
        }

        private void AccederAuxCommunesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCommunes.Wfrm.WfrmShowDialog();
            try
            {
                cOM_COMMUNETableAdapter.FillByReg(dSPesees.COM_COMMUNE, (int)rEG_IDComboBox.SelectedValue);
            }
            catch { }
        }

        private void ProprietairesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesProprietaires.Wfrm.WfrmShowDialog();

            pRO_NOMCOMPLETTableAdapter.FillProprietaires(dSPesees.PRO_NOMCOMPLET);
            prO_NOMCOMPLETTableAdapter1.FillProducteurs(dsPesees1.PRO_NOMCOMPLET);
        }

        private void LieuProductionMenuStrip_Click(object sender, EventArgs e)
        {
            WfrmGestionDesLieuxDeProductions.Wfrm.WfrmShowDialog();
            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
        }

        private void ToolRegion_Click(object sender, EventArgs e)
        {
            WfrmGestionDesRegions.Wfrm.WfrmShowDialog();
            rEG_REGIONTableAdapter.Fill(dSPesees.REG_REGION);
        }

        private void AccederAuxDistrictsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesDistricts.Wfrm.WfrmShowDialog();
            try
            {
                dIS_DISTRICTTableAdapter.FillByReg(dSPesees.DIS_DISTRICT, (int)rEG_IDComboBox.SelectedValue);
            }
            catch { }
        }

        #endregion Right Button Event

        #region Events

        private void REG_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                districtComboBox.Enabled = true;
                dIS_DISTRICTTableAdapter.FillByReg(dSPesees.DIS_DISTRICT, (int)rEG_IDComboBox.SelectedValue);
                DistrictComboBox_SelectedIndexChanged(null, null);
                districtComboBox.Focus();
            }
            catch
            {
                districtComboBox.Enabled = false;
                cOM_IDComboBox.Enabled = false;
            }
        }

        private void PAR_PARCELLEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool isSorted = true;
            int acqId = int.Parse(aCQ_IDTextBox.Text);
            switch (e.ColumnIndex)
            {
                case 3:
                    pAR_PARCELLEBindingSource.RemoveSort();
                    pAR_PARCELLETableAdapter.FillOrderCep(dSPesees.PAR_PARCELLE, acqId);
                    break;

                case 4:
                    pAR_PARCELLEBindingSource.RemoveSort();
                    pAR_PARCELLETableAdapter.FillOrderAut(dSPesees.PAR_PARCELLE, acqId);
                    break;

                case 5:
                    pAR_PARCELLEBindingSource.RemoveSort();
                    pAR_PARCELLETableAdapter.FillOrderLie(dSPesees.PAR_PARCELLE, acqId);
                    break;

                default:
                    isSorted = false;
                    break;
            }
            if (isSorted)
            {
                foreach (DataGridViewColumn column in pAR_PARCELLEDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                pAR_PARCELLEDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void DistrictComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cOM_IDComboBox.Enabled = true;
                cOM_COMMUNETableAdapter.FillByRegDis(dSPesees.COM_COMMUNE, (int)rEG_IDComboBox.SelectedValue, (int)districtComboBox.SelectedValue);
            }
            catch
            {
                cOM_IDComboBox.Enabled = false;
            }
        }

        private void PRO_IDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pRO_ID_VENDANGEComboBox.SelectedValue == null && pRO_IDComboBox.SelectedValue != null)
                pRO_ID_VENDANGEComboBox.SelectedValue = pRO_IDComboBox.SelectedValue;
        }

        private void PRO_ID_VENDANGEComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pRO_ID_VENDANGEComboBox.SelectedValue == null || pRO_IDComboBox.SelectedValue != null)
                return;

            pRO_IDComboBox.SelectedValue = pRO_ID_VENDANGEComboBox.SelectedValue;
        }

        private void CbRecu_CheckedChanged(object sender, EventArgs e)
        {
            aCQ_RECEPTIONDateTimePicker.Enabled = cbRecu.Checked;
        }

        private void PAR_PARCELLEDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == pAR_PARCELLEDataGridView.Columns["Kilos"].Index)
                SetKiloValue_ParParcelleDataGridView(e);
        }

        private void SetKiloValue_ParParcelleDataGridView(DataGridViewCellFormattingEventArgs e)
        {
            int couleurId = -1;
            decimal rateConv = -1;
            if (pAR_PARCELLEDataGridView.Rows[e.RowIndex].Cells["CEP_ACQ_ID"].Value != null)
            {
                int id = (int)pAR_PARCELLEDataGridView.Rows[e.RowIndex].Cells["CEP_ACQ_ID"].Value;
                couleurId = (int)aCQ_ACQUITTableAdapter.GetCouleurIdByACQ_ID(id);
            }

            switch (couleurId)
            {
                case 1: //BLANC
                    rateConv = GlobalParam.Instance.TauxConversionBlanc;
                    break;

                case 2: //ROUGE
                    rateConv = GlobalParam.Instance.TauxConversionRouge;
                    break;

                default:
                    rateConv = -1;
                    break;
            }

            e.FormattingApplied = true;
            DataGridViewRow row = pAR_PARCELLEDataGridView.Rows[e.RowIndex];
            decimal kilo = -1;
            if (row.Cells["PAR_LITRES"].Value != null)
            {
                kilo = Convert.ToDecimal(row.Cells["PAR_LITRES"].Value) / rateConv;
            }
            e.Value = kilo.ToString("0");
        }

        #endregion Events
    }
}