﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSaisieParcelle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cEP_IDLabel;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label aUT_IDLabel;
            System.Windows.Forms.Label lblZoneTravail;
            System.Windows.Forms.Label lblSecteurVisite;
            System.Windows.Forms.Label lblModeCulture;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WfrmSaisieParcelle));
            this.cOM_IDLabel = new System.Windows.Forms.Label();
            this.aCQ_NUMEROLabel = new System.Windows.Forms.Label();
            this.lIE_IDLabel = new System.Windows.Forms.Label();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pAR_PARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cEP_IDComboBox = new System.Windows.Forms.ComboBox();
            this.CepagesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxCépagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_SURFACE_CEPAGETextBox = new System.Windows.Forms.TextBox();
            this.pAR_SURFACE_COMPTEETextBox = new System.Windows.Forms.TextBox();
            this.pAR_QUOTATextBox = new System.Windows.Forms.TextBox();
            this.pAR_LITRESTextBox = new System.Windows.Forms.TextBox();
            this.lTitre = new System.Windows.Forms.Label();
            this.pAR_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.cmdOK = new System.Windows.Forms.Button();
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.pAR_IDTextBox = new System.Windows.Forms.TextBox();
            this.aCQ_IDComboBox = new System.Windows.Forms.ComboBox();
            this.aCQACQUITBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aCQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.aUTAUTREMENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.aUT_IDComboBox = new System.Windows.Forms.ComboBox();
            this.autMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxMentionsParticulièresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lIE_IDComboBox = new System.Windows.Forms.ComboBox();
            this.contextLieuProduction = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolLieuProduction = new System.Windows.Forms.ToolStripMenuItem();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.zOT_ZONE_TRAVAILcomboBox = new System.Windows.Forms.ComboBox();
            this.zOTZONETRAVAILBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zOT_ZONE_TRAVAILTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter();
            this.pAR_SECTEUR_VISITEcombobox = new System.Windows.Forms.ComboBox();
            this.pAR_SECTEUR_VISITEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pARPARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAR_SECTEUR_VISITETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_SECTEUR_VISITETableAdapter();
            this.pAR_MODE_CULTUREcombobox = new System.Windows.Forms.ComboBox();
            this.cOM_IDCombobox = new System.Windows.Forms.ComboBox();
            this.cOMCOMMUNEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cOM_COMMUNETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter();
            this.pCOPARCOMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pCO_PAR_COMTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PCO_PAR_COMTableAdapter();
            this.plI_PAR_LIETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PLI_PAR_LIETableAdapter();
            this.pAR_KILOSTextBox = new System.Windows.Forms.TextBox();
            this.lblTauxQuota = new System.Windows.Forms.Label();
            this.lblLitresOrKilos = new System.Windows.Forms.Label();
            cEP_IDLabel = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            aUT_IDLabel = new System.Windows.Forms.Label();
            lblZoneTravail = new System.Windows.Forms.Label();
            lblSecteurVisite = new System.Windows.Forms.Label();
            lblModeCulture = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).BeginInit();
            this.CepagesContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).BeginInit();
            this.autMenuStrip.SuspendLayout();
            this.contextLieuProduction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_SECTEUR_VISITEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pARPARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCOPARCOMBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // cEP_IDLabel
            // 
            cEP_IDLabel.AutoSize = true;
            cEP_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            cEP_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            cEP_IDLabel.Location = new System.Drawing.Point(36, 199);
            cEP_IDLabel.Name = "cEP_IDLabel";
            cEP_IDLabel.Size = new System.Drawing.Size(68, 16);
            cEP_IDLabel.TabIndex = 10;
            cEP_IDLabel.Text = "Cépage :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label4.Location = new System.Drawing.Point(36, 175);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(120, 16);
            label4.TabIndex = 35;
            label4.Text = "Surface cépage :";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label5.Location = new System.Drawing.Point(36, 277);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(129, 16);
            label5.TabIndex = 36;
            label5.Text = "Surface comptée :";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            label3.Location = new System.Drawing.Point(36, 96);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(70, 16);
            label3.TabIndex = 41;
            label3.Text = "Numéro :";
            // 
            // aUT_IDLabel
            // 
            aUT_IDLabel.AutoSize = true;
            aUT_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            aUT_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aUT_IDLabel.Location = new System.Drawing.Point(36, 121);
            aUT_IDLabel.Name = "aUT_IDLabel";
            aUT_IDLabel.Size = new System.Drawing.Size(89, 16);
            aUT_IDLabel.TabIndex = 47;
            aUT_IDLabel.Text = "Nom local : ";
            // 
            // lblZoneTravail
            // 
            lblZoneTravail.AutoSize = true;
            lblZoneTravail.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblZoneTravail.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblZoneTravail.Location = new System.Drawing.Point(37, 359);
            lblZoneTravail.Name = "lblZoneTravail";
            lblZoneTravail.Size = new System.Drawing.Size(119, 16);
            lblZoneTravail.TabIndex = 53;
            lblZoneTravail.Text = "Zone de travail :";
            lblZoneTravail.Visible = false;
            // 
            // lblSecteurVisite
            // 
            lblSecteurVisite.AutoSize = true;
            lblSecteurVisite.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblSecteurVisite.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblSecteurVisite.Location = new System.Drawing.Point(36, 329);
            lblSecteurVisite.Name = "lblSecteurVisite";
            lblSecteurVisite.Size = new System.Drawing.Size(129, 16);
            lblSecteurVisite.TabIndex = 54;
            lblSecteurVisite.Text = "Secteur de visite :";
            // 
            // lblModeCulture
            // 
            lblModeCulture.AutoSize = true;
            lblModeCulture.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            lblModeCulture.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            lblModeCulture.Location = new System.Drawing.Point(36, 305);
            lblModeCulture.Name = "lblModeCulture";
            lblModeCulture.Size = new System.Drawing.Size(117, 16);
            lblModeCulture.TabIndex = 57;
            lblModeCulture.Text = "Mode de culture";
            // 
            // cOM_IDLabel
            // 
            this.cOM_IDLabel.AutoSize = true;
            this.cOM_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cOM_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cOM_IDLabel.Location = new System.Drawing.Point(37, 386);
            this.cOM_IDLabel.Name = "cOM_IDLabel";
            this.cOM_IDLabel.Size = new System.Drawing.Size(83, 16);
            this.cOM_IDLabel.TabIndex = 58;
            this.cOM_IDLabel.Text = "Commune :";
            this.cOM_IDLabel.Visible = false;
            // 
            // aCQ_NUMEROLabel
            // 
            this.aCQ_NUMEROLabel.AutoSize = true;
            this.aCQ_NUMEROLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.aCQ_NUMEROLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.aCQ_NUMEROLabel.Location = new System.Drawing.Point(36, 69);
            this.aCQ_NUMEROLabel.Name = "aCQ_NUMEROLabel";
            this.aCQ_NUMEROLabel.Size = new System.Drawing.Size(60, 16);
            this.aCQ_NUMEROLabel.TabIndex = 45;
            this.aCQ_NUMEROLabel.Text = "Acquit :";
            // 
            // lIE_IDLabel
            // 
            this.lIE_IDLabel.AutoSize = true;
            this.lIE_IDLabel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lIE_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lIE_IDLabel.Location = new System.Drawing.Point(36, 148);
            this.lIE_IDLabel.Name = "lIE_IDLabel";
            this.lIE_IDLabel.Size = new System.Drawing.Size(144, 16);
            this.lIE_IDLabel.TabIndex = 50;
            this.lIE_IDLabel.Text = "Lieu de production :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pAR_PARCELLEBindingSource
            // 
            this.pAR_PARCELLEBindingSource.DataMember = "PAR_PARCELLE";
            this.pAR_PARCELLEBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = this.pAR_PARCELLETableAdapter;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // cEP_IDComboBox
            // 
            this.cEP_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cEP_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cEP_IDComboBox.ContextMenuStrip = this.CepagesContextMenuStrip;
            this.cEP_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pAR_PARCELLEBindingSource, "CEP_ID", true));
            this.cEP_IDComboBox.DataSource = this.cEPCEPAGEBindingSource;
            this.cEP_IDComboBox.DisplayMember = "CEP_NOM";
            this.cEP_IDComboBox.FormattingEnabled = true;
            this.cEP_IDComboBox.Location = new System.Drawing.Point(204, 199);
            this.cEP_IDComboBox.Name = "cEP_IDComboBox";
            this.cEP_IDComboBox.Size = new System.Drawing.Size(201, 21);
            this.cEP_IDComboBox.TabIndex = 6;
            this.cEP_IDComboBox.ValueMember = "CEP_ID";
            this.cEP_IDComboBox.SelectedValueChanged += new System.EventHandler(this.CEP_IDComboBox_SelectedValueChanged);
             // 
            // CepagesContextMenuStrip
            // 
            this.CepagesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxCépagesToolStripMenuItem});
            this.CepagesContextMenuStrip.Name = "CepagesContextMenuStrip";
            this.CepagesContextMenuStrip.Size = new System.Drawing.Size(197, 26);
            // 
            // accéderAuxCépagesToolStripMenuItem
            // 
            this.accéderAuxCépagesToolStripMenuItem.Name = "accéderAuxCépagesToolStripMenuItem";
            this.accéderAuxCépagesToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.accéderAuxCépagesToolStripMenuItem.Text = "Accéder aux cépages ...";
            this.accéderAuxCépagesToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxCepagesToolStripMenuItem_Click);
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_SURFACE_CEPAGETextBox
            // 
            this.pAR_SURFACE_CEPAGETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_SURFACE_CEPAGE", true));
            this.pAR_SURFACE_CEPAGETextBox.Location = new System.Drawing.Point(204, 174);
            this.pAR_SURFACE_CEPAGETextBox.Name = "pAR_SURFACE_CEPAGETextBox";
            this.pAR_SURFACE_CEPAGETextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_SURFACE_CEPAGETextBox.TabIndex = 5;
            this.pAR_SURFACE_CEPAGETextBox.Validated += new System.EventHandler(this.PAR_SURFACE_CEPAGETextBox_Validated);
            // 
            // pAR_SURFACE_COMPTEETextBox
            // 
            this.pAR_SURFACE_COMPTEETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_SURFACE_COMPTEE", true));
            this.pAR_SURFACE_COMPTEETextBox.Location = new System.Drawing.Point(204, 276);
            this.pAR_SURFACE_COMPTEETextBox.Name = "pAR_SURFACE_COMPTEETextBox";
            this.pAR_SURFACE_COMPTEETextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_SURFACE_COMPTEETextBox.TabIndex = 9;
            // 
            // pAR_QUOTATextBox
            // 
            this.pAR_QUOTATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_QUOTA", true));
            this.pAR_QUOTATextBox.Location = new System.Drawing.Point(204, 226);
            this.pAR_QUOTATextBox.Name = "pAR_QUOTATextBox";
            this.pAR_QUOTATextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_QUOTATextBox.TabIndex = 7;
            // 
            // pAR_LITRESTextBox
            // 
            this.pAR_LITRESTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_LITRES", true));
            this.pAR_LITRESTextBox.Location = new System.Drawing.Point(204, 251);
            this.pAR_LITRESTextBox.Name = "pAR_LITRESTextBox";
            this.pAR_LITRESTextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_LITRESTextBox.TabIndex = 8;
            this.pAR_LITRESTextBox.Visible = false;
            // 
            // lTitre
            // 
            this.lTitre.AutoSize = true;
            this.lTitre.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lTitre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lTitre.Location = new System.Drawing.Point(35, 23);
            this.lTitre.Name = "lTitre";
            this.lTitre.Size = new System.Drawing.Size(165, 20);
            this.lTitre.TabIndex = 39;
            this.lTitre.Text = "Nouvelle parcelle";
            // 
            // pAR_NUMEROTextBox
            // 
            this.pAR_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_NUMERO", true));
            this.pAR_NUMEROTextBox.Location = new System.Drawing.Point(204, 95);
            this.pAR_NUMEROTextBox.Name = "pAR_NUMEROTextBox";
            this.pAR_NUMEROTextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_NUMEROTextBox.TabIndex = 2;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdOK.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cmdOK.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cmdOK.Location = new System.Drawing.Point(249, 422);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 30);
            this.cmdOK.TabIndex = 13;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.CmdAjouter_Click);
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // pAR_IDTextBox
            // 
            this.pAR_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_ID", true));
            this.pAR_IDTextBox.Location = new System.Drawing.Point(293, 17);
            this.pAR_IDTextBox.Name = "pAR_IDTextBox";
            this.pAR_IDTextBox.Size = new System.Drawing.Size(100, 19);
            this.pAR_IDTextBox.TabIndex = 43;
            this.pAR_IDTextBox.Text = "ID";
            // 
            // aCQ_IDComboBox
            // 
            this.aCQ_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pAR_PARCELLEBindingSource, "ACQ_ID", true));
            this.aCQ_IDComboBox.DataSource = this.aCQACQUITBindingSource;
            this.aCQ_IDComboBox.DisplayMember = "ACQ_NUMERO";
            this.aCQ_IDComboBox.Enabled = false;
            this.aCQ_IDComboBox.FormattingEnabled = true;
            this.aCQ_IDComboBox.Location = new System.Drawing.Point(204, 68);
            this.aCQ_IDComboBox.Name = "aCQ_IDComboBox";
            this.aCQ_IDComboBox.Size = new System.Drawing.Size(201, 21);
            this.aCQ_IDComboBox.TabIndex = 0;
            this.aCQ_IDComboBox.ValueMember = "ACQ_ID";
            // 
            // aCQACQUITBindingSource
            // 
            this.aCQACQUITBindingSource.DataMember = "ACQ_ACQUIT";
            this.aCQACQUITBindingSource.DataSource = this.dSPesees;
            // 
            // aCQ_ACQUITTableAdapter
            // 
            this.aCQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // aUTAUTREMENTIONBindingSource
            // 
            this.aUTAUTREMENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUTAUTREMENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // aUT_IDComboBox
            // 
            this.aUT_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.aUT_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.aUT_IDComboBox.ContextMenuStrip = this.autMenuStrip;
            this.aUT_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pAR_PARCELLEBindingSource, "AUT_ID", true));
            this.aUT_IDComboBox.DataSource = this.aUTAUTREMENTIONBindingSource;
            this.aUT_IDComboBox.DisplayMember = "AUT_NOM";
            this.aUT_IDComboBox.FormattingEnabled = true;
            this.aUT_IDComboBox.Location = new System.Drawing.Point(204, 120);
            this.aUT_IDComboBox.Name = "aUT_IDComboBox";
            this.aUT_IDComboBox.Size = new System.Drawing.Size(201, 21);
            this.aUT_IDComboBox.TabIndex = 3;
            this.aUT_IDComboBox.ValueMember = "AUT_ID";
            // 
            // autMenuStrip
            // 
            this.autMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxMentionsParticulièresToolStripMenuItem});
            this.autMenuStrip.Name = "contextMenuStrip1";
            this.autMenuStrip.Size = new System.Drawing.Size(259, 26);
            this.autMenuStrip.Click += new System.EventHandler(this.AutMenuStrip_Click_1);
            // 
            // accéderAuxMentionsParticulièresToolStripMenuItem
            // 
            this.accéderAuxMentionsParticulièresToolStripMenuItem.Name = "accéderAuxMentionsParticulièresToolStripMenuItem";
            this.accéderAuxMentionsParticulièresToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.accéderAuxMentionsParticulièresToolStripMenuItem.Text = "Accéder aux mentions particulières";
            // 
            // lIE_IDComboBox
            // 
            this.lIE_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lIE_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lIE_IDComboBox.ContextMenuStrip = this.contextLieuProduction;
            this.lIE_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pAR_PARCELLEBindingSource, "LIE_ID", true));
            this.lIE_IDComboBox.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.lIE_IDComboBox.DisplayMember = "LIE_NOM";
            this.lIE_IDComboBox.FormattingEnabled = true;
            this.lIE_IDComboBox.Location = new System.Drawing.Point(204, 147);
            this.lIE_IDComboBox.Name = "lIE_IDComboBox";
            this.lIE_IDComboBox.Size = new System.Drawing.Size(201, 21);
            this.lIE_IDComboBox.TabIndex = 4;
            this.lIE_IDComboBox.ValueMember = "LIE_ID";
            // 
            // contextLieuProduction
            // 
            this.contextLieuProduction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLieuProduction});
            this.contextLieuProduction.Name = "CepagesContextMenuStrip";
            this.contextLieuProduction.Size = new System.Drawing.Size(253, 26);
            // 
            // toolLieuProduction
            // 
            this.toolLieuProduction.Name = "toolLieuProduction";
            this.toolLieuProduction.Size = new System.Drawing.Size(252, 22);
            this.toolLieuProduction.Text = "Accéder aux lieux de production...";
            this.toolLieuProduction.Click += new System.EventHandler(this.ToolLieuProduction_Click);
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // cmdCancel
            // 
            this.cmdCancel.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.cmdCancel.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cmdCancel.Location = new System.Drawing.Point(330, 422);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 30);
            this.cmdCancel.TabIndex = 14;
            this.cmdCancel.Text = "Annuler";
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.CmdCancel_Click);
            // 
            // zOT_ZONE_TRAVAILcomboBox
            // 
            this.zOT_ZONE_TRAVAILcomboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.zOT_ZONE_TRAVAILcomboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.zOT_ZONE_TRAVAILcomboBox.ContextMenuStrip = this.contextLieuProduction;
            this.zOT_ZONE_TRAVAILcomboBox.DataSource = this.zOTZONETRAVAILBindingSource;
            this.zOT_ZONE_TRAVAILcomboBox.DisplayMember = "ZOT_DESCRIPTION";
            this.zOT_ZONE_TRAVAILcomboBox.FormattingEnabled = true;
            this.zOT_ZONE_TRAVAILcomboBox.Location = new System.Drawing.Point(204, 354);
            this.zOT_ZONE_TRAVAILcomboBox.Name = "zOT_ZONE_TRAVAILcomboBox";
            this.zOT_ZONE_TRAVAILcomboBox.Size = new System.Drawing.Size(201, 21);
            this.zOT_ZONE_TRAVAILcomboBox.TabIndex = 12;
            this.zOT_ZONE_TRAVAILcomboBox.ValueMember = "ZOT_ID";
            this.zOT_ZONE_TRAVAILcomboBox.Visible = false;
            // 
            // zOTZONETRAVAILBindingSource
            // 
            this.zOTZONETRAVAILBindingSource.DataMember = "ZOT_ZONE_TRAVAIL";
            this.zOTZONETRAVAILBindingSource.DataSource = this.dSPesees;
            // 
            // zOT_ZONE_TRAVAILTableAdapter
            // 
            this.zOT_ZONE_TRAVAILTableAdapter.ClearBeforeFill = true;
            // 
            // pAR_SECTEUR_VISITEcombobox
            // 
            this.pAR_SECTEUR_VISITEcombobox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_SECTEUR_VISITE", true));
            this.pAR_SECTEUR_VISITEcombobox.DataSource = this.pAR_SECTEUR_VISITEBindingSource;
            this.pAR_SECTEUR_VISITEcombobox.DisplayMember = "PAR_SECTEUR_VISITE";
            this.pAR_SECTEUR_VISITEcombobox.FormattingEnabled = true;
            this.pAR_SECTEUR_VISITEcombobox.Location = new System.Drawing.Point(204, 327);
            this.pAR_SECTEUR_VISITEcombobox.Name = "pAR_SECTEUR_VISITEcombobox";
            this.pAR_SECTEUR_VISITEcombobox.Size = new System.Drawing.Size(201, 21);
            this.pAR_SECTEUR_VISITEcombobox.TabIndex = 11;
            this.pAR_SECTEUR_VISITEcombobox.ValueMember = "PAR_SECTEUR_VISITE";
            // 
            // pAR_SECTEUR_VISITEBindingSource
            // 
            this.pAR_SECTEUR_VISITEBindingSource.DataMember = "PAR_SECTEUR_VISITE";
            this.pAR_SECTEUR_VISITEBindingSource.DataSource = this.dSPesees;
            // 
            // pARPARCELLEBindingSource
            // 
            this.pARPARCELLEBindingSource.DataMember = "PAR_PARCELLE";
            this.pARPARCELLEBindingSource.DataSource = this.dSPesees;
            // 
            // pAR_SECTEUR_VISITETableAdapter
            // 
            this.pAR_SECTEUR_VISITETableAdapter.ClearBeforeFill = true;
            // 
            // pAR_MODE_CULTUREcombobox
            // 
            this.pAR_MODE_CULTUREcombobox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_MODE_CULTURE", true));
            this.pAR_MODE_CULTUREcombobox.FormattingEnabled = true;
            this.pAR_MODE_CULTUREcombobox.Location = new System.Drawing.Point(204, 300);
            this.pAR_MODE_CULTUREcombobox.Name = "pAR_MODE_CULTUREcombobox";
            this.pAR_MODE_CULTUREcombobox.Size = new System.Drawing.Size(201, 21);
            this.pAR_MODE_CULTUREcombobox.TabIndex = 10;
            // 
            // cOM_IDCombobox
            // 
            this.cOM_IDCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cOM_IDCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cOM_IDCombobox.ContextMenuStrip = this.contextLieuProduction;
            this.cOM_IDCombobox.DataSource = this.cOMCOMMUNEBindingSource;
            this.cOM_IDCombobox.DisplayMember = "COM_NOM";
            this.cOM_IDCombobox.FormattingEnabled = true;
            this.cOM_IDCombobox.Location = new System.Drawing.Point(204, 381);
            this.cOM_IDCombobox.Name = "cOM_IDCombobox";
            this.cOM_IDCombobox.Size = new System.Drawing.Size(201, 21);
            this.cOM_IDCombobox.TabIndex = 59;
            this.cOM_IDCombobox.ValueMember = "COM_ID";
            this.cOM_IDCombobox.Visible = false;
            // 
            // cOMCOMMUNEBindingSource
            // 
            this.cOMCOMMUNEBindingSource.DataMember = "COM_COMMUNE";
            this.cOMCOMMUNEBindingSource.DataSource = this.dSPesees;
            // 
            // cOM_COMMUNETableAdapter
            // 
            this.cOM_COMMUNETableAdapter.ClearBeforeFill = true;
            // 
            // pCOPARCOMBindingSource
            // 
            this.pCOPARCOMBindingSource.DataMember = "PCO_PAR_COM";
            this.pCOPARCOMBindingSource.DataSource = this.dSPesees;
            // 
            // pCO_PAR_COMTableAdapter
            // 
            this.pCO_PAR_COMTableAdapter.ClearBeforeFill = true;
            // 
            // plI_PAR_LIETableAdapter
            // 
            this.plI_PAR_LIETableAdapter.ClearBeforeFill = true;
            // 
            // pAR_KILOSTextBox
            // 
            this.pAR_KILOSTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pAR_PARCELLEBindingSource, "PAR_KILOS", true));
            this.pAR_KILOSTextBox.Location = new System.Drawing.Point(204, 251);
            this.pAR_KILOSTextBox.Name = "pAR_KILOSTextBox";
            this.pAR_KILOSTextBox.Size = new System.Drawing.Size(201, 19);
            this.pAR_KILOSTextBox.TabIndex = 60;
            this.pAR_KILOSTextBox.Visible = false;
            // 
            // lblTauxQuota
            // 
            this.lblTauxQuota.AutoSize = true;
            this.lblTauxQuota.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblTauxQuota.Location = new System.Drawing.Point(36, 226);
            this.lblTauxQuota.Name = "lblTauxQuota";
            this.lblTauxQuota.Size = new System.Drawing.Size(83, 16);
            this.lblTauxQuota.TabIndex = 61;
            this.lblTauxQuota.Text = "Litres/m2 :";
            // 
            // lblLitresOrKilos
            // 
            this.lblLitresOrKilos.AutoSize = true;
            this.lblLitresOrKilos.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblLitresOrKilos.Location = new System.Drawing.Point(36, 252);
            this.lblLitresOrKilos.Name = "lblLitresOrKilos";
            this.lblLitresOrKilos.Size = new System.Drawing.Size(55, 16);
            this.lblLitresOrKilos.TabIndex = 62;
            this.lblLitresOrKilos.Text = "Litres :";
            // 
            // WfrmSaisieParcelle
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(440, 466);
            this.Controls.Add(this.lblLitresOrKilos);
            this.Controls.Add(this.lblTauxQuota);
            this.Controls.Add(this.pAR_KILOSTextBox);
            this.Controls.Add(this.cOM_IDCombobox);
            this.Controls.Add(this.cOM_IDLabel);
            this.Controls.Add(lblModeCulture);
            this.Controls.Add(this.pAR_MODE_CULTUREcombobox);
            this.Controls.Add(this.pAR_SECTEUR_VISITEcombobox);
            this.Controls.Add(lblSecteurVisite);
            this.Controls.Add(this.zOT_ZONE_TRAVAILcomboBox);
            this.Controls.Add(lblZoneTravail);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.lTitre);
            this.Controls.Add(this.pAR_SURFACE_COMPTEETextBox);
            this.Controls.Add(this.aCQ_NUMEROLabel);
            this.Controls.Add(this.aCQ_IDComboBox);
            this.Controls.Add(this.lIE_IDComboBox);
            this.Controls.Add(this.aUT_IDComboBox);
            this.Controls.Add(aUT_IDLabel);
            this.Controls.Add(this.lIE_IDLabel);
            this.Controls.Add(this.pAR_IDTextBox);
            this.Controls.Add(label3);
            this.Controls.Add(this.pAR_NUMEROTextBox);
            this.Controls.Add(this.pAR_QUOTATextBox);
            this.Controls.Add(cEP_IDLabel);
            this.Controls.Add(this.cEP_IDComboBox);
            this.Controls.Add(this.pAR_SURFACE_CEPAGETextBox);
            this.Controls.Add(label4);
            this.Controls.Add(this.pAR_LITRESTextBox);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(label5);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(412, 393);
            this.Name = "WfrmSaisieParcelle";
            this.Text = "Gestion des acquits - ajouter une parcelle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WfrmAddParcelle_FormClosing);
            this.Load += new System.EventHandler(this.WfrmAddParcelle_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).EndInit();
            this.CepagesContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aCQACQUITBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUTAUTREMENTIONBindingSource)).EndInit();
            this.autMenuStrip.ResumeLayout(false);
            this.contextLieuProduction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zOTZONETRAVAILBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_SECTEUR_VISITEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pARPARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cOMCOMMUNEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pCOPARCOMBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pAR_PARCELLEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.ComboBox cEP_IDComboBox;
        private System.Windows.Forms.TextBox pAR_SURFACE_CEPAGETextBox;
        private System.Windows.Forms.TextBox pAR_SURFACE_COMPTEETextBox;
        private System.Windows.Forms.TextBox pAR_QUOTATextBox;
        private System.Windows.Forms.TextBox pAR_LITRESTextBox;
        private System.Windows.Forms.Label lTitre;
        private System.Windows.Forms.TextBox pAR_NUMEROTextBox;
        private System.Windows.Forms.Button cmdOK;
     //   private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CRU_CRUTableAdapter cRU_CRUTableAdapter;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
       // private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.APP_APPELLATIONTableAdapter aPP_APPELLATIONTableAdapter;
        private System.Windows.Forms.TextBox pAR_IDTextBox;
        private System.Windows.Forms.ComboBox aCQ_IDComboBox;
        private System.Windows.Forms.BindingSource aCQACQUITBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter aCQ_ACQUITTableAdapter;
        private System.Windows.Forms.ContextMenuStrip CepagesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxCépagesToolStripMenuItem;
        private System.Windows.Forms.BindingSource aUTAUTREMENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private System.Windows.Forms.ComboBox aUT_IDComboBox;
        private System.Windows.Forms.ComboBox lIE_IDComboBox;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.ContextMenuStrip autMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxMentionsParticulièresToolStripMenuItem;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.ContextMenuStrip contextLieuProduction;
        private System.Windows.Forms.ToolStripMenuItem toolLieuProduction;
        private System.Windows.Forms.ComboBox zOT_ZONE_TRAVAILcomboBox;
        private System.Windows.Forms.BindingSource zOTZONETRAVAILBindingSource;
        private DAO.DSPeseesTableAdapters.ZOT_ZONE_TRAVAILTableAdapter zOT_ZONE_TRAVAILTableAdapter;
        private System.Windows.Forms.ComboBox pAR_SECTEUR_VISITEcombobox;
        private System.Windows.Forms.BindingSource pARPARCELLEBindingSource;
        private DAO.DSPeseesTableAdapters.PAR_SECTEUR_VISITETableAdapter pAR_SECTEUR_VISITETableAdapter;
        private System.Windows.Forms.BindingSource pAR_SECTEUR_VISITEBindingSource;
        private System.Windows.Forms.ComboBox pAR_MODE_CULTUREcombobox;
        private System.Windows.Forms.Label aCQ_NUMEROLabel;
        private System.Windows.Forms.Label lIE_IDLabel;
        private System.Windows.Forms.ComboBox cOM_IDCombobox;
        private System.Windows.Forms.BindingSource cOMCOMMUNEBindingSource;
        private DAO.DSPeseesTableAdapters.COM_COMMUNETableAdapter cOM_COMMUNETableAdapter;
        private System.Windows.Forms.Label cOM_IDLabel;
        private System.Windows.Forms.BindingSource pCOPARCOMBindingSource;
        private DAO.DSPeseesTableAdapters.PCO_PAR_COMTableAdapter pCO_PAR_COMTableAdapter;
        private DAO.DSPeseesTableAdapters.PLI_PAR_LIETableAdapter plI_PAR_LIETableAdapter;
        private System.Windows.Forms.TextBox pAR_KILOSTextBox;
        private System.Windows.Forms.Label lblTauxQuota;
        private System.Windows.Forms.Label lblLitresOrKilos;


    }
}