﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des adresses des pressoirs
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesPressoirs : WfrmSetting
    {
        private static WfrmGestionDesPressoirs _wfrm;
        private FormSettingsManagerRefAdresse _manager;

        public static WfrmGestionDesPressoirs Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesPressoirs();
                }
                return _wfrm;
            }
        }

        private WfrmGestionDesPressoirs()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesPressoirs_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(pRE_PRESSOIRTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(adresseCompletTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
            pRE_PRESSOIRTableAdapter.FillOrderAdr(dSPesees.PRE_PRESSOIR);
            _manager = new FormSettingsManagerRefAdresse(this, pRE_IDTextBox, aDR_IDComboBox, btnAddCepage,
                btnSaveCepage, btnSupprimer, pRE_PRESSOIRDataGridView, pRE_PRESSOIRBindingSource,
                tableAdapterManager, dSPesees, adresseCompletTableAdapter);
            _manager.AddControl(aDR_IDComboBox);
        }

        private void toolAddress_Click(object sender, EventArgs e)
        {
            WfrmGestionDesAdresses.Wfrm.WfrmShowDialog();
            adresseCompletTableAdapter.Fill(dSPesees.AdresseComplet);
        }
    }
}