﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesBonus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cRU_NOMLabel;
            System.Windows.Forms.Label lIE_IDLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtrlGestionDesBonus));
            this.lprix100 = new System.Windows.Forms.Label();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.vAL_VALEUR_CEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vAL_VALEUR_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.VAL_VALEUR_CEPAGETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cEPCEPAGEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cEP_CEPAGETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter();
            this.lIE_IDComboBox = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.bON_BONUSDataGridView = new System.Windows.Forms.DataGridView();
            this.BON_OE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BON_OE_FIN = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.viewBonusMaxBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BON_BRIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRIX_FIN = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.VAL_BONUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BON_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bonusDatagridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.supprimerLaLigneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bON_BONUSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vAL_VALEURTextBox = new System.Windows.Forms.TextBox();
            this.cEP_IDComboBox = new System.Windows.Forms.ComboBox();
            this.bON_BONUSTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.BON_BONUSTableAdapter();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.aNN_ANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.viewBonusMaxTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ViewBonusMaxTableAdapter();
            this.lIELIEUDEPRODUCTIONBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dsPesees1 = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.liE_LIEU_DE_PRODUCTIONTableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lIE_IDComboBox1 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.vAL_VALEUR_CEPAGEDataGridView = new System.Windows.Forms.DataGridView();
            this.DGVAL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CEP_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAL_OECHSLE_MOYEN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VAL_BON_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vAL_IDTextBox = new System.Windows.Forms.TextBox();
            this.OeMoyenTextBox = new System.Windows.Forms.TextBox();
            this.lblOeMoyen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.aNN_IDTextBox = new System.Windows.Forms.TextBox();
            this.valTypeTextBox = new System.Windows.Forms.TextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBtnCopyCepage = new System.Windows.Forms.ToolStripButton();
            this.toolCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolExportWinBIZ = new System.Windows.Forms.ToolStripButton();
            cRU_NOMLabel = new System.Windows.Forms.Label();
            lIE_IDLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewBonusMaxBindingSource)).BeginInit();
            this.bonusDatagridContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEDataGridView)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cRU_NOMLabel
            // 
            cRU_NOMLabel.AutoSize = true;
            cRU_NOMLabel.Location = new System.Drawing.Point(3, 78);
            cRU_NOMLabel.Name = "cRU_NOMLabel";
            cRU_NOMLabel.Size = new System.Drawing.Size(53, 13);
            cRU_NOMLabel.TabIndex = 19;
            cRU_NOMLabel.Text = "Cépage : ";
            // 
            // lIE_IDLabel
            // 
            lIE_IDLabel.AutoSize = true;
            lIE_IDLabel.Location = new System.Drawing.Point(3, 51);
            lIE_IDLabel.Name = "lIE_IDLabel";
            lIE_IDLabel.Size = new System.Drawing.Size(101, 13);
            lIE_IDLabel.TabIndex = 27;
            lIE_IDLabel.Text = "Lieu de production :";
            // 
            // lprix100
            // 
            this.lprix100.AutoSize = true;
            this.lprix100.Location = new System.Drawing.Point(3, 105);
            this.lprix100.Name = "lprix100";
            this.lprix100.Size = new System.Drawing.Size(122, 13);
            this.lprix100.TabIndex = 27;
            this.lprix100.Text = "Valeur 100% (CHF/ kg) :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vAL_VALEUR_CEPAGEBindingSource
            // 
            this.vAL_VALEUR_CEPAGEBindingSource.DataMember = "VAL_VALEUR_CEPAGE";
            this.vAL_VALEUR_CEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // vAL_VALEUR_CEPAGETableAdapter
            // 
            this.vAL_VALEUR_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = this.vAL_VALEUR_CEPAGETableAdapter;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // cEPCEPAGEBindingSource
            // 
            this.cEPCEPAGEBindingSource.DataMember = "CEP_CEPAGE";
            this.cEPCEPAGEBindingSource.DataSource = this.dSPesees;
            // 
            // cEP_CEPAGETableAdapter
            // 
            this.cEP_CEPAGETableAdapter.ClearBeforeFill = true;
            // 
            // lIE_IDComboBox
            // 
            this.lIE_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lIE_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lIE_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lIE_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.vAL_VALEUR_CEPAGEBindingSource, "LIE_ID", true));
            this.lIE_IDComboBox.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.lIE_IDComboBox.DisplayMember = "LIE_NOM";
            this.lIE_IDComboBox.FormattingEnabled = true;
            this.lIE_IDComboBox.Location = new System.Drawing.Point(172, 51);
            this.lIE_IDComboBox.Name = "lIE_IDComboBox";
            this.lIE_IDComboBox.Size = new System.Drawing.Size(246, 21);
            this.lIE_IDComboBox.TabIndex = 0;
            this.lIE_IDComboBox.ValueMember = "LIE_ID";
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(356, 503);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 4;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnOK.Location = new System.Drawing.Point(276, 503);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            // 
            // bON_BONUSDataGridView
            // 
            this.bON_BONUSDataGridView.AllowUserToAddRows = false;
            this.bON_BONUSDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bON_BONUSDataGridView.AutoGenerateColumns = false;
            this.bON_BONUSDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.bON_BONUSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bON_BONUSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BON_OE,
            this.BON_OE_FIN,
            this.BON_BRIX,
            this.BRIX_FIN,
            this.VAL_BONUS,
            this.BON_ID,
            this.VAL_ID});
            this.bON_BONUSDataGridView.ContextMenuStrip = this.bonusDatagridContextMenuStrip;
            this.bON_BONUSDataGridView.DataSource = this.bON_BONUSBindingSource;
            this.bON_BONUSDataGridView.Location = new System.Drawing.Point(3, 156);
            this.bON_BONUSDataGridView.MultiSelect = false;
            this.bON_BONUSDataGridView.Name = "bON_BONUSDataGridView";
            this.bON_BONUSDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.bON_BONUSDataGridView.Size = new System.Drawing.Size(428, 340);
            this.bON_BONUSDataGridView.TabIndex = 27;
            this.bON_BONUSDataGridView.EnabledChanged += new System.EventHandler(this.DataGridView_EnabledChanged);
            // 
            // BON_OE
            // 
            this.BON_OE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_OE.DataPropertyName = "BON_OE";
            this.BON_OE.HeaderText = "°Oe (début)";
            this.BON_OE.Name = "BON_OE";
            // 
            // BON_OE_FIN
            // 
            this.BON_OE_FIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_OE_FIN.DataPropertyName = "BON_ID";
            this.BON_OE_FIN.DataSource = this.viewBonusMaxBindingSource;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.BON_OE_FIN.DefaultCellStyle = dataGridViewCellStyle1;
            this.BON_OE_FIN.DisplayMember = "BON_OE_FIN_WITHSIGN";
            this.BON_OE_FIN.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.BON_OE_FIN.HeaderText = "°Oe (fin)";
            this.BON_OE_FIN.Name = "BON_OE_FIN";
            this.BON_OE_FIN.ReadOnly = true;
            this.BON_OE_FIN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BON_OE_FIN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.BON_OE_FIN.ValueMember = "BON_ID";
            // 
            // viewBonusMaxBindingSource
            // 
            this.viewBonusMaxBindingSource.DataMember = "ViewBonusMax";
            this.viewBonusMaxBindingSource.DataSource = this.dSPesees;
            // 
            // BON_BRIX
            // 
            this.BON_BRIX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_BRIX.DataPropertyName = "BON_BRIX";
            this.BON_BRIX.HeaderText = "%Brix (début)";
            this.BON_BRIX.Name = "BON_BRIX";
            // 
            // BRIX_FIN
            // 
            this.BRIX_FIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BRIX_FIN.DataPropertyName = "BON_ID";
            this.BRIX_FIN.DataSource = this.viewBonusMaxBindingSource;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.BRIX_FIN.DefaultCellStyle = dataGridViewCellStyle2;
            this.BRIX_FIN.DisplayMember = "BON_BRIX_FIN_WITHSIGN";
            this.BRIX_FIN.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.BRIX_FIN.HeaderText = "%Brix (fin)";
            this.BRIX_FIN.Name = "BRIX_FIN";
            this.BRIX_FIN.ReadOnly = true;
            this.BRIX_FIN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BRIX_FIN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.BRIX_FIN.ValueMember = "BON_ID";
            // 
            // VAL_BONUS
            // 
            this.VAL_BONUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.VAL_BONUS.DataPropertyName = "BON_BONUS";
            this.VAL_BONUS.HeaderText = "Bonus / Malus (%)";
            this.VAL_BONUS.Name = "VAL_BONUS";
            // 
            // BON_ID
            // 
            this.BON_ID.DataPropertyName = "BON_ID";
            this.BON_ID.HeaderText = "BON_ID";
            this.BON_ID.Name = "BON_ID";
            this.BON_ID.ReadOnly = true;
            this.BON_ID.Visible = false;
            // 
            // VAL_ID
            // 
            this.VAL_ID.DataPropertyName = "VAL_ID";
            this.VAL_ID.HeaderText = "VAL_ID";
            this.VAL_ID.Name = "VAL_ID";
            this.VAL_ID.Visible = false;
            // 
            // bonusDatagridContextMenuStrip
            // 
            this.bonusDatagridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supprimerLaLigneToolStripMenuItem});
            this.bonusDatagridContextMenuStrip.Name = "bonusDatagridContextMenuStrip";
            this.bonusDatagridContextMenuStrip.Size = new System.Drawing.Size(171, 26);
            // 
            // supprimerLaLigneToolStripMenuItem
            // 
            this.supprimerLaLigneToolStripMenuItem.Name = "supprimerLaLigneToolStripMenuItem";
            this.supprimerLaLigneToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.supprimerLaLigneToolStripMenuItem.Text = "Supprimer la ligne";
            this.supprimerLaLigneToolStripMenuItem.Click += new System.EventHandler(this.SupprimerLaLigneToolStripMenuItem_Click);
            // 
            // bON_BONUSBindingSource
            // 
            this.bON_BONUSBindingSource.DataMember = "FK_BON_BONUS_VAL_VALEUR_CEPAGE";
            this.bON_BONUSBindingSource.DataSource = this.vAL_VALEUR_CEPAGEBindingSource;
            // 
            // vAL_VALEURTextBox
            // 
            this.vAL_VALEURTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vAL_VALEURTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vAL_VALEUR_CEPAGEBindingSource, "VAL_VALEUR", true));
            this.vAL_VALEURTextBox.Location = new System.Drawing.Point(172, 105);
            this.vAL_VALEURTextBox.MaximumSize = new System.Drawing.Size(88, 20);
            this.vAL_VALEURTextBox.Name = "vAL_VALEURTextBox";
            this.vAL_VALEURTextBox.Size = new System.Drawing.Size(20, 20);
            this.vAL_VALEURTextBox.TabIndex = 2;
            // 
            // cEP_IDComboBox
            // 
            this.cEP_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cEP_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cEP_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cEP_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.vAL_VALEUR_CEPAGEBindingSource, "CEP_ID", true));
            this.cEP_IDComboBox.DataSource = this.cEPCEPAGEBindingSource;
            this.cEP_IDComboBox.DisplayMember = "CEP_NOM";
            this.cEP_IDComboBox.Enabled = false;
            this.cEP_IDComboBox.FormattingEnabled = true;
            this.cEP_IDComboBox.Location = new System.Drawing.Point(172, 78);
            this.cEP_IDComboBox.Name = "cEP_IDComboBox";
            this.cEP_IDComboBox.Size = new System.Drawing.Size(246, 21);
            this.cEP_IDComboBox.TabIndex = 1;
            this.cEP_IDComboBox.ValueMember = "CEP_ID";
            // 
            // bON_BONUSTableAdapter
            // 
            this.bON_BONUSTableAdapter.ClearBeforeFill = true;
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // aNN_ANNEEBindingSource
            // 
            this.aNN_ANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNN_ANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // viewBonusMaxTableAdapter
            // 
            this.viewBonusMaxTableAdapter.ClearBeforeFill = true;
            // 
            // lIELIEUDEPRODUCTIONBindingSource1
            // 
            this.lIELIEUDEPRODUCTIONBindingSource1.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource1.DataSource = this.dsPesees1;
            // 
            // dsPesees1
            // 
            this.dsPesees1.DataSetName = "DSPesees";
            this.dsPesees1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // liE_LIEU_DE_PRODUCTIONTableAdapter1
            // 
            this.liE_LIEU_DE_PRODUCTIONTableAdapter1.ClearBeforeFill = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.vAL_VALEUR_CEPAGEDataGridView);
            this.splitContainer1.Panel1.Controls.Add(this.vAL_IDTextBox);
            this.splitContainer1.Panel1.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.splitContainer1.Panel2.Controls.Add(this.OeMoyenTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.lblOeMoyen);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(lIE_IDLabel);
            this.splitContainer1.Panel2.Controls.Add(this.lprix100);
            this.splitContainer1.Panel2.Controls.Add(this.bON_BONUSDataGridView);
            this.splitContainer1.Panel2.Controls.Add(cRU_NOMLabel);
            this.splitContainer1.Panel2.Controls.Add(this.lIE_IDComboBox);
            this.splitContainer1.Panel2.Controls.Add(this.vAL_VALEURTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Panel2.Controls.Add(this.cEP_IDComboBox);
            this.splitContainer1.Panel2.Controls.Add(this.btnAnnuler);
            this.splitContainer1.Panel2.Controls.Add(this.aNN_IDTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.valTypeTextBox);
            this.splitContainer1.Panel2.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", global::Gestion_Des_Vendanges.Properties.Settings.Default, "BackColor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.splitContainer1.Size = new System.Drawing.Size(891, 543);
            this.splitContainer1.SplitterDistance = 449;
            this.splitContainer1.TabIndex = 37;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lIE_IDComboBox1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cbAnnee);
            this.panel1.Location = new System.Drawing.Point(157, 179);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 211);
            this.panel1.TabIndex = 53;
            // 
            // lIE_IDComboBox1
            // 
            this.lIE_IDComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lIE_IDComboBox1.DataSource = this.lIELIEUDEPRODUCTIONBindingSource1;
            this.lIE_IDComboBox1.DisplayMember = "LIE_NOM";
            this.lIE_IDComboBox1.FormattingEnabled = true;
            this.lIE_IDComboBox1.Location = new System.Drawing.Point(6, 99);
            this.lIE_IDComboBox1.MaximumSize = new System.Drawing.Size(500, 200);
            this.lIE_IDComboBox1.Name = "lIE_IDComboBox1";
            this.lIE_IDComboBox1.Size = new System.Drawing.Size(188, 108);
            this.lIE_IDComboBox1.TabIndex = 45;
            this.lIE_IDComboBox1.ValueMember = "LIE_ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label6.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label6.Location = new System.Drawing.Point(3, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 15);
            this.label6.TabIndex = 44;
            this.label6.Text = "Lieu de production :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label5.Location = new System.Drawing.Point(3, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNN_ANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 21);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label4.Location = new System.Drawing.Point(10, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 21);
            this.label4.TabIndex = 52;
            this.label4.Text = "Gestion des bonus";
            // 
            // vAL_VALEUR_CEPAGEDataGridView
            // 
            this.vAL_VALEUR_CEPAGEDataGridView.AllowUserToAddRows = false;
            this.vAL_VALEUR_CEPAGEDataGridView.AllowUserToDeleteRows = false;
            this.vAL_VALEUR_CEPAGEDataGridView.AutoGenerateColumns = false;
            this.vAL_VALEUR_CEPAGEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.vAL_VALEUR_CEPAGEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vAL_VALEUR_CEPAGEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DGVAL_ID,
            this.dataGridViewTextBoxColumn5,
            this.CEP_ID,
            this.dataGridViewTextBoxColumn4,
            this.VAL_OECHSLE_MOYEN,
            this.VAL_BON_TYPE,
            this.dataGridViewTextBoxColumn3});
            this.vAL_VALEUR_CEPAGEDataGridView.DataSource = this.vAL_VALEUR_CEPAGEBindingSource;
            this.vAL_VALEUR_CEPAGEDataGridView.Location = new System.Drawing.Point(0, 50);
            this.vAL_VALEUR_CEPAGEDataGridView.Name = "vAL_VALEUR_CEPAGEDataGridView";
            this.vAL_VALEUR_CEPAGEDataGridView.ReadOnly = true;
            this.vAL_VALEUR_CEPAGEDataGridView.Size = new System.Drawing.Size(449, 493);
            this.vAL_VALEUR_CEPAGEDataGridView.TabIndex = 41;
            this.vAL_VALEUR_CEPAGEDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.VAL_VALEUR_CEPAGEDataGridView_ColumnHeaderMouseClick);
            this.vAL_VALEUR_CEPAGEDataGridView.EnabledChanged += new System.EventHandler(this.DataGridView_EnabledChanged);
            // 
            // DGVAL_ID
            // 
            this.DGVAL_ID.DataPropertyName = "VAL_ID";
            this.DGVAL_ID.HeaderText = "VAL_ID";
            this.DGVAL_ID.Name = "DGVAL_ID";
            this.DGVAL_ID.ReadOnly = true;
            this.DGVAL_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LIE_ID";
            this.dataGridViewTextBoxColumn5.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.dataGridViewTextBoxColumn5.DisplayMember = "LIE_NOM";
            this.dataGridViewTextBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.dataGridViewTextBoxColumn5.HeaderText = "Lieu de production";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn5.ValueMember = "LIE_ID";
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 120;
            // 
            // CEP_ID
            // 
            this.CEP_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.DataSource = this.cEPCEPAGEBindingSource;
            this.CEP_ID.DisplayMember = "CEP_NOM";
            this.CEP_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.CEP_ID.FillWeight = 200F;
            this.CEP_ID.HeaderText = "Cépage";
            this.CEP_ID.MinimumWidth = 20;
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CEP_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.CEP_ID.ValueMember = "CEP_ID";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "VAL_VALEUR";
            this.dataGridViewTextBoxColumn4.HeaderText = "Valeur (100%)";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 20;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // VAL_OECHSLE_MOYEN
            // 
            this.VAL_OECHSLE_MOYEN.DataPropertyName = "VAL_OECHSLE_MOYEN";
            this.VAL_OECHSLE_MOYEN.HeaderText = "Oechsle Moyen";
            this.VAL_OECHSLE_MOYEN.Name = "VAL_OECHSLE_MOYEN";
            this.VAL_OECHSLE_MOYEN.ReadOnly = true;
            // 
            // VAL_BON_TYPE
            // 
            this.VAL_BON_TYPE.DataPropertyName = "VAL_BON_TYPE";
            this.VAL_BON_TYPE.HeaderText = "VAL_BON_TYPE";
            this.VAL_BON_TYPE.Name = "VAL_BON_TYPE";
            this.VAL_BON_TYPE.ReadOnly = true;
            this.VAL_BON_TYPE.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ANN_ID";
            this.dataGridViewTextBoxColumn3.HeaderText = "ANN_ID";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // vAL_IDTextBox
            // 
            this.vAL_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vAL_VALEUR_CEPAGEBindingSource, "VAL_ID", true));
            this.vAL_IDTextBox.Enabled = false;
            this.vAL_IDTextBox.Location = new System.Drawing.Point(62, 175);
            this.vAL_IDTextBox.Name = "vAL_IDTextBox";
            this.vAL_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.vAL_IDTextBox.TabIndex = 40;
            this.vAL_IDTextBox.Text = "VAL_ID";
            // 
            // OeMoyenTextBox
            // 
            this.OeMoyenTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OeMoyenTextBox.Cursor = System.Windows.Forms.Cursors.No;
            this.OeMoyenTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vAL_VALEUR_CEPAGEBindingSource, "VAL_OECHSLE_MOYEN", true));
            this.OeMoyenTextBox.Location = new System.Drawing.Point(332, 105);
            this.OeMoyenTextBox.MaximumSize = new System.Drawing.Size(88, 20);
            this.OeMoyenTextBox.Name = "OeMoyenTextBox";
            this.OeMoyenTextBox.ReadOnly = true;
            this.OeMoyenTextBox.Size = new System.Drawing.Size(20, 20);
            this.OeMoyenTextBox.TabIndex = 55;
            // 
            // lblOeMoyen
            // 
            this.lblOeMoyen.AutoSize = true;
            this.lblOeMoyen.Location = new System.Drawing.Point(270, 108);
            this.lblOeMoyen.Name = "lblOeMoyen";
            this.lblOeMoyen.Size = new System.Drawing.Size(56, 13);
            this.lblOeMoyen.TabIndex = 54;
            this.lblOeMoyen.Text = "Oe Moyen";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 21);
            this.label3.TabIndex = 53;
            this.label3.Text = "Saisie bonus";
            // 
            // aNN_IDTextBox
            // 
            this.aNN_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vAL_VALEUR_CEPAGEBindingSource, "ANN_ID", true));
            this.aNN_IDTextBox.Location = new System.Drawing.Point(135, 303);
            this.aNN_IDTextBox.Name = "aNN_IDTextBox";
            this.aNN_IDTextBox.ReadOnly = true;
            this.aNN_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.aNN_IDTextBox.TabIndex = 37;
            this.aNN_IDTextBox.Text = "ANN_ID";
            // 
            // valTypeTextBox
            // 
            this.valTypeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.vAL_VALEUR_CEPAGEBindingSource, "VAL_BON_TYPE", true));
            this.valTypeTextBox.Location = new System.Drawing.Point(82, 215);
            this.valTypeTextBox.Name = "valTypeTextBox";
            this.valTypeTextBox.Size = new System.Drawing.Size(100, 20);
            this.valTypeTextBox.TabIndex = 56;
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator1,
            this.toolBtnCopyCepage,
            this.toolCopy,
            this.toolStripSeparator2,
            this.toolExportWinBIZ});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(891, 25);
            this.toolStrip2.TabIndex = 40;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolAdd.Image")));
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(66, 22);
            this.toolAdd.Text = "Ajouter";
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolUpdate.Image")));
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(72, 22);
            this.toolUpdate.Text = "Modifier";
            // 
            // toolDelete
            // 
            this.toolDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolDelete.Image")));
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(82, 22);
            this.toolDelete.Text = "Supprimer";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolBtnCopyCepage
            // 
            this.toolBtnCopyCepage.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnCopyCepage.Image")));
            this.toolBtnCopyCepage.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolBtnCopyCepage.Name = "toolBtnCopyCepage";
            this.toolBtnCopyCepage.Size = new System.Drawing.Size(115, 22);
            this.toolBtnCopyCepage.Text = "Copier le cépage";
            // 
            // toolCopy
            // 
            this.toolCopy.Image = ((System.Drawing.Image)(resources.GetObject("toolCopy.Image")));
            this.toolCopy.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolCopy.Name = "toolCopy";
            this.toolCopy.Size = new System.Drawing.Size(174, 22);
            this.toolCopy.Text = "Copier le lieu de production";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolExportWinBIZ
            // 
            this.toolExportWinBIZ.Image = ((System.Drawing.Image)(resources.GetObject("toolExportWinBIZ.Image")));
            this.toolExportWinBIZ.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolExportWinBIZ.Name = "toolExportWinBIZ";
            this.toolExportWinBIZ.Size = new System.Drawing.Size(65, 22);
            this.toolExportWinBIZ.Text = "Aperçu";
            this.toolExportWinBIZ.Click += new System.EventHandler(this.BtnApercu_Click);
            // 
            // CtrlGestionDesBonus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip2);
            this.Name = "CtrlGestionDesBonus";
            this.Size = new System.Drawing.Size(891, 568);
            this.Load += new System.EventHandler(this.WfrmGestionDesBonus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEPCEPAGEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewBonusMaxBindingSource)).EndInit();
            this.bonusDatagridContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNN_ANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPesees1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vAL_VALEUR_CEPAGEDataGridView)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource vAL_VALEUR_CEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.VAL_VALEUR_CEPAGETableAdapter vAL_VALEUR_CEPAGETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource cEPCEPAGEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CEP_CEPAGETableAdapter cEP_CEPAGETableAdapter;
        private System.Windows.Forms.TextBox vAL_VALEURTextBox;
        private System.Windows.Forms.ComboBox cEP_IDComboBox;
        private System.Windows.Forms.BindingSource bON_BONUSBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.BON_BONUSTableAdapter bON_BONUSTableAdapter;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.ComboBox lIE_IDComboBox;
        private System.Windows.Forms.BindingSource aNN_ANNEEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private System.Windows.Forms.DataGridView bON_BONUSDataGridView;
        private System.Windows.Forms.BindingSource viewBonusMaxBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ViewBonusMaxTableAdapter viewBonusMaxTableAdapter;
        private System.Windows.Forms.ContextMenuStrip bonusDatagridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem supprimerLaLigneToolStripMenuItem;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource1;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter liE_LIEU_DE_PRODUCTIONTableAdapter1;
        private Gestion_Des_Vendanges.DAO.DSPesees dsPesees1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView vAL_VALEUR_CEPAGEDataGridView;
        private System.Windows.Forms.TextBox vAL_IDTextBox;
        private System.Windows.Forms.TextBox aNN_IDTextBox;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolExportWinBIZ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.ToolStripButton toolBtnCopyCepage;
        private System.Windows.Forms.ListBox lIE_IDComboBox1;
        private System.Windows.Forms.Label lprix100;
        private System.Windows.Forms.DataGridViewTextBoxColumn BON_OE;
        private System.Windows.Forms.DataGridViewComboBoxColumn BON_OE_FIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn BON_BRIX;
        private System.Windows.Forms.DataGridViewComboBoxColumn BRIX_FIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_BONUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn BON_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_ID;
        private System.Windows.Forms.Label lblOeMoyen;
        private System.Windows.Forms.TextBox OeMoyenTextBox;
        private System.Windows.Forms.TextBox valTypeTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn DGVAL_ID;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_OECHSLE_MOYEN;
        private System.Windows.Forms.DataGridViewTextBoxColumn VAL_BON_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}