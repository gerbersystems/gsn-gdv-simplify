﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Gère le comportement du formulaire "Article"
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class FormSettingsManagerArticle : FormSettingsManagerPesees
    {
        private ART_ARTICLETableAdapter _artTable;
        private DSPesees _dsPesees;
        private TextBox _txtDesignationLongue;
        private ToolStripButton _btnGenererArticle;
        private ToolStripButton _btnGrouper;
        private ToolStripButton _btnDissocier;
        private ToolStripButton _btnExportWinBiz;
        private ToolStripButton _btnUpdate;
        private ToolStripButton _btnDelete;
        private ToolStripButton _toolOk;
        private ToolStripButton _toolCancel;
        private ComboBox _cbAnnee;
        private DAO.BDVendangeControleur _bdVendangeControleur;
        private List<int> _artToGroup;
        private ViewArticleTableAdapter _vartTable;

        /*private PED_PESEE_DETAILTableAdapter _PedTable
        {
            get
            {
                _pedTable.Connection.ConnectionString = DAO.ConnectionManager.Instance._gvConnectionString;
                return _pedTable;
            }
            set
            {
                _pedTable = value;
            }
        }*/

        public FormSettingsManagerArticle(ICtrlSetting ctrlSetting, TextBox idTextBox, Control focusControl,
            ToolStripButton btnNew, ToolStripButton btnUpdate, ToolStripButton btnDelete, Button btnOK, Button btnCancel, DataGridView dataGridView,
            BindingSource bindingSource, TableAdapterManager tableAdapterManager, DSPesees dsPesees,
            ART_ARTICLETableAdapter articleTableAdapter, TextBox txtDesignationLongue, ToolStripButton btnGenererArticle,
            ToolStripButton btnGrouper, ToolStripButton btnDissocier, ToolStripButton btnExportWinBiz, ComboBox cbAnnee, ToolStripButton toolOk, ToolStripButton toolCancel)
            : base(ctrlSetting, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, btnOK, btnCancel, dataGridView, bindingSource,
                tableAdapterManager, dsPesees)
        {
            _txtDesignationLongue = txtDesignationLongue;
            _artTable = articleTableAdapter;
            _dsPesees = dsPesees;
            _btnGenererArticle = btnGenererArticle;
            _btnGenererArticle.Click += btnGenereArticles_Click;
            _btnGrouper = btnGrouper;
            _btnGrouper.Click += btnGrouper_Click;
            _btnDissocier = btnDissocier;
            _btnDissocier.Click += btnDissocier_Click;
            _btnExportWinBiz = btnExportWinBiz;
            _toolCancel = toolCancel;
            _toolOk = toolOk;
            if (BUSINESS.Utilities.GetOption(2))
            {
                _btnExportWinBiz.Click += btnWinBizExport_Click;
            }
            else
            {
                _btnExportWinBiz.Enabled = false;
            }
            _bdVendangeControleur = new BDVendangeControleur();
            _cbAnnee = cbAnnee;
            _cbAnnee.SelectedIndexChanged += cbAnnee_SelectedIndexChanged;
            _artToGroup = new List<int>();
            DataGridView.CellContentClick += dataGridView_CellContentClick;
            _vartTable = ConnectionManager.Instance.CreateGvTableAdapter<ViewArticleTableAdapter>();
            updateStateBtnGenerer();
            updateDataGridViewSelection();
            _toolCancel.Click += toolCancel_Click;
            _toolOk.Click += toolOk_Click;
            _btnDelete = btnDelete;
            _btnUpdate = btnUpdate;
        }

        protected override void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (!_txtDesignationLongue.Focused || e.KeyCode != Keys.Enter)
            {
                base.FormKeyDown(sender, e);
            }
        }

        protected override void BtnDelete_Click(object sender, EventArgs e)
        {
            if (!DataGridView.Columns["SelectArticle"].Visible && DataGridView.CurrentRow != null)
            {
                int artId = (int)DataGridView.CurrentRow.Cells["ART_ID"].Value;
                if (base.Form.CanDelete)
                {
                    setNullArtId(artId);
                    base.BtnDelete_Click(sender, e);
                    _btnGenererArticle.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Cet article a été transféré dans WinBIZ et ne peut être supprimé."
                        , "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        protected override void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (!DataGridView.Columns["SelectArticle"].Visible && DataGridView.CurrentRow != null)
                base.BtnUpdate_Click(sender, e);
        }

        protected override void DatagridSelectionChanged(object sender, EventArgs e)
        {
            base.DatagridSelectionChanged(sender, e);
            _btnDissocier.Enabled = (DataGridView.SelectedRows.Count == 1
                    && DataGridView.SelectedRows[0].Cells["ART_ID_WINBIZ"].Value.ToString() == string.Empty
                    && Convert.ToInt32(DataGridView.SelectedRows[0].Cells["ART_VERSION"].Value) != 1
                    && !DataGridView.Columns["SelectArticle"].Visible);
            if (DataGridView.Columns["SelectArticle"].Visible)
            {
                _btnUpdate.Enabled = false;
                _btnDelete.Enabled = false;
            }
        }

        private void btnWinBizExport_Click(object sender, EventArgs e)
        {
            try
            {
                DAO.WinBizControleur controleur = new Gestion_Des_Vendanges.DAO.WinBizControleur(BUSINESS.Utilities.IdAnneeCourante);
                ViewWinBizExportArticleTableAdapter vWbArticleTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ViewWinBizExportArticleTableAdapter>();
                controleur.ExportArticle(vWbArticleTable);
                //articleTable.UpdateEtatApresTransfert(BUSINESS.Utilities.IdAnneeCourante);
                MessageBox.Show("Articles exportés avec succès!", "Exportation WinBiz", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                MessageBox.Show("Connexion impossible, le dossier de l'année " + BUSINESS.Utilities.AnneeCourante + " n'existe pas.\n"
                    + "Veuillez vérifier son existence ainsi que la validité du chemin du dossier.", "Connexion impossible", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (System.Data.Odbc.OdbcException)
            {
                MessageBox.Show("Connexion impossible.\nVeuillez fermer toutes les connexions à WinBIZ et vérifier le chemin du dossier WinBIZ.\nSi cette erreur persiste, veuillez contacter Gerber Systems & Networks", "Erreur de connexion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            _artTable.FillByAn(_dsPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
            updateDataGridViewSelection();
        }

        private void btnGenereArticles_Click(object sender, EventArgs e)
        {
            if (new WfrmGroupageArticle().ShowDialog() == DialogResult.OK)
            {
                _bdVendangeControleur.GenererArticles(BUSINESS.Utilities.IdAnneeCourante);
                _artTable.FillByAn(_dsPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
                _btnGenererArticle.Enabled = false;
                updateDataGridViewSelection();
            }
        }

        private void toolOk_Click(object sender, EventArgs e)
        {
            new DAO.BDVendangeControleur().GrouperArticles(_artToGroup);
            _artTable.FillByAn(_dsPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
            updateDataGridViewSelection();
            showGroupement(false);
        }

        private void toolCancel_Click(object sender, EventArgs e)
        {
            showGroupement(false);
            foreach (DataGridViewRow row in DataGridView.Rows)
            {
                row.Cells["SelectArticle"].Value = false;
            }
            updateDataGridViewSelection();
        }

        private void showGroupement(bool isShow)
        {
            _btnExportWinBiz.Enabled = !isShow;
            _btnGrouper.Enabled = !isShow;
            if (isShow)
            {
                _btnDissocier.Enabled = (DataGridView.SelectedRows.Count == 1
                   && DataGridView.SelectedRows[0].Cells["ART_ID_WINBIZ"].Value.ToString() == string.Empty
                   && Convert.ToInt32(DataGridView.SelectedRows[0].Cells["ART_VERSION"].Value) != 1
                   && !DataGridView.Columns["SelectArticle"].Visible);
            }
            else
            {
                _btnDissocier.Enabled = false;
            }

            _toolCancel.Visible = isShow;
            _toolOk.Visible = isShow;
            _toolOk.Enabled = false;
            _btnUpdate.Enabled = !isShow;
            _btnDelete.Enabled = !isShow;
            DataGridView.Columns["SelectArticle"].Visible = isShow;
            if (isShow)
            {
                _btnGenererArticle.Enabled = false;
            }
            else
            {
                updateStateBtnGenerer();
            }
        }

        private void btnDissocier_Click(object sender, EventArgs e)
        {
            new DAO.BDVendangeControleur().DissocierArticle((int)DataGridView.SelectedRows[0].Cells["ART_ID"].Value);
            _artTable.FillByAn(_dsPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
            updateDataGridViewSelection();
        }

        private void btnGrouper_Click(object sender, EventArgs e)
        {
            showGroupement(true);
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView.EndEdit();
            updateDataGridViewSelection();
        }

        private void cbAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_cbAnnee.SelectedValue != null && Convert.ToInt32(_cbAnnee.SelectedValue) != BUSINESS.Utilities.IdAnneeCourante)
            {
                BUSINESS.Utilities.IdAnneeCourante = Convert.ToInt32(_cbAnnee.SelectedValue);
                _artTable.FillByAn(_dsPesees.ART_ARTICLE, BUSINESS.Utilities.IdAnneeCourante);
                _cbAnnee.SelectedValue = BUSINESS.Utilities.IdAnneeCourante;
                updateStateBtnGenerer();
                updateDataGridViewSelection();
            }
        }

        private void updateStateBtnGenerer()
        {
            _btnGenererArticle.Enabled = _vartTable.GetNbNonGenerer(BUSINESS.Utilities.IdAnneeCourante) > 0;
        }

        private List<int> genererArtToGroup()
        {
            _artToGroup.Clear();
            foreach (DataGridViewRow row in DataGridView.Rows)
            {
                if ((bool?)row.Cells["SelectArticle"].Value == true)
                {
                    _artToGroup.Add((int)row.Cells["ART_ID"].Value);
                }
            }
            return _artToGroup;
        }

        private void updateDataGridViewSelection()
        {
            genererArtToGroup();
            if (_artToGroup.Count == 0)
            {
                _toolOk.Enabled = false;
                //_btnGrouper.Enabled = false;
                foreach (DataGridViewRow row in DataGridView.Rows)
                {
                    if (row.Cells["ART_ID_WINBIZ"].Value.ToString() != string.Empty
                        || Convert.ToInt32(row.Cells["ART_VERSION"].Value) == 1)
                    {
                        row.Cells["SelectArticle"].ReadOnly = true;
                        row.Cells["SelectArticle"].Style.BackColor = System.Drawing.Color.LightGray;
                        row.Cells["SelectArticle"].Style.SelectionBackColor = System.Drawing.Color.LightGray;
                    }
                    else
                    {
                        row.Cells["SelectArticle"].ReadOnly = false;
                        row.Cells["SelectArticle"].Style.BackColor = System.Drawing.Color.LightGreen;
                        row.Cells["SelectArticle"].Style.SelectionBackColor = System.Drawing.Color.LightGreen;
                    }
                }
            }
            else
            {
                int couId = (int)_artTable.GetCouId(_artToGroup[0]);
                foreach (DataGridViewRow row in DataGridView.Rows)
                {
                    if ((int)row.Cells["COU_ID"].Value != couId || row.Cells["ART_ID_WINBIZ"].Value.ToString() != string.Empty
                        || Convert.ToInt32(row.Cells["ART_VERSION"].Value) == 1)
                    {
                        row.Cells["SelectArticle"].ReadOnly = true;
                        row.Cells["SelectArticle"].Style.BackColor = System.Drawing.Color.LightGray;
                        row.Cells["SelectArticle"].Style.SelectionBackColor = System.Drawing.Color.LightGray;
                    }
                    else
                    {
                        row.Cells["SelectArticle"].ReadOnly = false;
                        row.Cells["SelectArticle"].Style.BackColor = System.Drawing.Color.LightGreen;
                        row.Cells["SelectArticle"].Style.SelectionBackColor = System.Drawing.Color.LightGreen;
                    }
                }
                _toolOk.Enabled = _artToGroup.Count > 1;
            }
        }
    }
}