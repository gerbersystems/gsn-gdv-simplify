﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiementMode
    {
        private void SetModePaiementSpecifique()
        {
            int lieId = (int)cbLieuProduction.SelectedValue;

            _parametrePaiement.SetModePaiementCepage(lieId: lieId
                                                  , pourcentage: Math.Round(numPourcentage.Value, 2)
                                                  , isLitreMoutRaisinChecked: chkLitreMoutRaisin.Checked
                                                  , isGestionBonusActived: chkBonus.Checked
                                                  , isZoneDifficulteChecked: chkZoneTravail.Checked);
        }

        private void AddTableAdapter()
        {
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
        }

        private void FillTableAdapter()
        {
            lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByBonusAnnId(dSPesees.LIE_LIEU_DE_PRODUCTION, Utilities.IdAnneeCourante);
            mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(dSPesees.MOC_MODE_COMPTABILISATION, Utilities.IdAnneeCourante);
        }

        private void InitComboBoxValue()
        {
            try
            {
                if (_parametrePaiement.ModePaiement != null)
                    cbLieuProduction.SelectedValue = ((ModePaiementParCepage)_parametrePaiement.ModePaiement).LieId;
            }
            catch (NullReferenceException) { }

            if (GlobalParam.Instance.BonusUnite == BonusUniteEnum.VariablePourcent)
            {
                optPrixFixe.Enabled = optPrixCepage.Enabled = optPrixCepLieTablePrix.Enabled = false;
                optPrixCepLie.Select();
            }
        }
    }
}