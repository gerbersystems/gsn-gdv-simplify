﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesZonesDeTravail : WfrmSetting
    {
        private static WfrmGestionDesZonesDeTravail _wfrm;
        private FormSettingsManagerZoneDeTravail _manager;

        public override bool CanDelete
        {
            get
            {
                try
                {
                    int _zotId = (int)zOT_ZONE_TRAVAILDataGridView.CurrentRow.Cells["ZOT_ID"].Value;
                    return ((int)zOT_ZONE_TRAVAILTableAdapter.NbRef(_zotId) == 0);
                }
                catch
                {
                    return true;
                }
            }
        }

        public static WfrmGestionDesZonesDeTravail Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                    _wfrm = new WfrmGestionDesZonesDeTravail();

                return _wfrm;
            }
        }

        private WfrmGestionDesZonesDeTravail()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesZonesDeTravail_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(zOT_ZONE_TRAVAILTableAdapter);

            this.mOC_MODE_COMPTABILISATIONTableAdapter.Fill(this.dSPesees.MOC_MODE_COMPTABILISATION);
            this.zOT_ZONE_TRAVAILTableAdapter.Fill(this.dSPesees.ZOT_ZONE_TRAVAIL);

            bool blnModeComptaActivated = BUSINESS.Utilities.GetOption(2);
            if (blnModeComptaActivated)
                mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(this.dSPesees.MOC_MODE_COMPTABILISATION, BUSINESS.Utilities.IdAnneeCourante);

            _manager = new FormSettingsManagerZoneDeTravail(form: this, idTextBox: zOT_IDTextBox
                                                                      , focusControl: zOT_DESCRIPTIONTextBox
                                                                      , btnNew: btnAddZoneTravail
                                                                      , btnUpdate: btnSaveZoneTravail
                                                                      , btnDelete: btnSupprimer
                                                                      , dataGridView: zOT_ZONE_TRAVAILDataGridView
                                                                      , bindingSource: zOTZONETRAVAILBindingSource
                                                                      , tableAdapterManager: tableAdapterManager
                                                                      , dataSet: dSPesees
                                                                      , cbModeComptabilisation: mOC_IDComboBox);

            _manager.AddControl(this.zOT_DESCRIPTIONTextBox);
            _manager.AddControl(this.zOT_VALEURTextBox);

            if (blnModeComptaActivated)
            {
                _manager.AddControl(mOC_IDComboBox);
                try
                {
                    new BDVendangeControleur().UpdateModeComptabilisations();
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    MessageBox.Show("Au moins un dossier WinBIZ est indisponible.\n" +
                        "Les modes de comptabilisation de ce(s) dossier(s) n'ont pas été synchronisés."
                        , "Gestion des vendanges"
                        , MessageBoxButtons.OK
                        , MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                mOC_IDComboBox.Hide();
            }
        }
    }
}