﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class FormSettingsManagerAnnee : FormSettingsManagerRefAdresse
    {
        private void CopieAcquits(int idMaxAn)
        {
            ACQ_ACQUITTableAdapter acqTable = ConnectionManager.Instance.CreateGvTableAdapter<ACQ_ACQUITTableAdapter>();
            PAR_PARCELLETableAdapter parTable = ConnectionManager.Instance.CreateGvTableAdapter<PAR_PARCELLETableAdapter>();

            acqTable.FillByAnnee(DataSet.ACQ_ACQUIT, idMaxAn);
            foreach (DSPesees.ACQ_ACQUITRow acqRow in acqTable.GetDataByAnnee(idMaxAn))
            {
                try
                {
                    acqTable.Insert(CLA_ID: acqRow.CLA_ID,
                                    COU_ID: acqRow.COU_ID,
                                    ANN_ID: _AnnId,
                                    COM_ID: acqRow.COM_ID,
                                    PRO_ID: acqRow.PRO_ID,
                                    ACQ_NUMERO: acqRow.ACQ_NUMERO,
                                    ACQ_DATE: acqRow.ACQ_DATE,
                                    ACQ_SURFACE: acqRow.ACQ_SURFACE,
                                    ACQ_LITRES: acqRow.ACQ_LITRES,
                                    REG_ID: acqRow.REG_ID,
                                    ACQ_NUMERO_COMPL: (acqRow.IsACQ_NUMERO_COMPLNull() ? (int?)null : acqRow.ACQ_NUMERO_COMPL),
                                    PRO_ID_VENDANGE: acqRow.PRO_ID_VENDANGE,
                                    ACQ_RECEPTION: null);
                }
                catch (System.Data.StrongTypingException)
                {
                    throw;
                }

                parTable.FillByAcquit((DataSet).PAR_PARCELLE, acqRow.ACQ_ID);
                foreach (DSPesees.PAR_PARCELLERow parRow in parTable.GetDataByAcquit(acqRow.ACQ_ID))
                {
                    try
                    {
                        parTable.Insert(CEP_ID: parRow.CEP_ID,
                                        ACQ_ID: acqTable.GetMaxAcqId(),
                                        PAR_FOLIO: (parRow.IsPAR_FOLIONull() ? null : parRow.PAR_FOLIO),
                                        PAR_NUMERO: parRow.PAR_NUMERO,
                                        PAR_SURFACE_CEPAGE: parRow.PAR_SURFACE_CEPAGE,
                                        PAR_QUOTA: parRow.PAR_QUOTA,
                                        PAR_LITRES: parRow.PAR_LITRES,
                                        PAR_SURFACE_COMPTEE: parRow.PAR_SURFACE_COMPTEE,
                                        LIE_ID: parRow.LIE_ID,
                                        AUT_ID: parRow.AUT_ID,
                                        ZOT_ID: (parRow.IsZOT_IDNull() ? (int?)null : parRow.ZOT_ID),
                                        PAR_SECTEUR_VISITE: parRow.PAR_SECTEUR_VISITE,
                                        PAR_MODE_CULTURE: (parRow.IsPAR_MODE_CULTURENull() ? null : parRow.PAR_MODE_CULTURE),
                                        PAR_KILOS: (parRow.IsPAR_KILOSNull() ? 0 : parRow.PAR_KILOS));
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        private void CopieModeComptablisationCouleur(int idMaxAn, MOC_MODE_COMPTABILISATIONTableAdapter mocTable)
        {
            var mcoTable = ConnectionManager.Instance.CreateGvTableAdapter<MCO_MOC_COUTableAdapter>();
            mcoTable.FillByAnnId(DataSet.MCO_MOC_COU, idMaxAn);

            foreach (DSPesees.MCO_MOC_COURow mcoRow in mcoTable.GetDataByAnnId(idMaxAn))
            {
                int mocId = Convert.ToInt32(mocTable.GetMocId(Convert.ToInt32(mocTable.GetMocIdWinBiz(mcoRow.MOC_ID)), _AnnId));
                mcoTable.Insert(MOC_ID: mocId,
                                COU_ID: mcoRow.COU_ID);
            }
        }

        protected override void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                base.BtnDelete_Click(sender, e);
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Erreur lors de la suppression de l'année, veuillez effectuer la suppression manuellement ou contacter Gerber Systems & Networks.",
                                                     "Erreur base de données",
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
            }
        }
    }
}