﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des lignes manuelles
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesLignesManuelles : WfrmSetting
    {
        private decimal _montantTotal = 0m;
        private bool _pourcentageChange;
        private FormSettingsManagerLigneManuelle _manager;
        private int _paiId;
        private static WfrmGestionDesLignesManuelles _wfrm;

        private WfrmGestionDesLignesManuelles(int paiId)
        {
            InitializeComponent();
            _paiId = paiId;
            _pourcentageChange = true;
        }

        public static WfrmGestionDesLignesManuelles GetWfrm(int paiId)
        {
            if (_wfrm == null || _wfrm.IsDisposed)
            {
                _wfrm = new WfrmGestionDesLignesManuelles(paiId);
            }
            else
            {
                _wfrm._paiId = paiId;
                _wfrm.WfrmGestionDesLignesManuelles_Load(null, null);
            }

            return _wfrm;
        }

        private void WfrmGestionDesLignesManuelles_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(lIM_LIGNE_PAIEMENT_MANUELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            lIM_LIGNE_PAIEMENT_MANUELLETableAdapter.FillByPai(this.dSPesees.LIM_LIGNE_PAIEMENT_MANUELLE, _paiId);

            _manager = new FormSettingsManagerLigneManuelle(this
                                                          , lIM_IDTextBox
                                                          , pAI_IDTextBox
                                                          , _paiId
                                                          , lIM_TEXTETextBox
                                                          , btnAdd
                                                          , btnUpdate
                                                          , btnSupprimer
                                                          , lIM_LIGNE_PAIEMENT_MANUELLEDataGridView
                                                          , lIM_LIGNE_PAIEMENT_MANUELLEBindingSource
                                                          , tableAdapterManager, dSPesees);
            _manager.AddControl(lIM_TEXTETextBox);
            _manager.AddControl(lIM_MONTANTTextBox);
            _manager.AddControl(numPourcentage);

            txtPmtNumero.Text = pAI_PAIEMENTTableAdapter.GetNumeroById(_paiId).ToString();

            /*    if (Convert.ToInt32(pAI_PAIEMENTTableAdapter.GetModeTVA(_paiId)) == 2)
                {
                    _montantTotal = Convert.ToDecimal(lipTable.GetTotalPaiementTTC(_paiId));
                }
                else
                {
                    _montantTotal = Convert.ToDecimal(lipTable.GetTotalPaiementHT(_paiId));
                }*/
            throw new NotImplementedException();
            //txtMontantTotal.Text = _montantTotal.ToString();
        }

        private void NumPourcentage_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (_pourcentageChange)
                {
                    decimal montant = (numPourcentage.Value * _montantTotal) / 100;
                    montant = Math.Round(montant * 2, 1) / 2;
                    lIM_MONTANTTextBox.Text = montant.ToString();
                }
            }
            catch
            {
                lIM_MONTANTTextBox.Text = "0";
            }
        }

        private void LIM_MONTANTTextBox_Validated(object sender, EventArgs e)
        {
            try
            {
                decimal montant = decimal.Parse(lIM_MONTANTTextBox.Text);
                decimal pourcentage = (montant / _montantTotal) * 100;
                pourcentage = Math.Round(pourcentage, 2);
                _pourcentageChange = false;
                numPourcentage.Value = pourcentage;
                _pourcentageChange = true;
            }
            catch
            {
                numPourcentage.Value = 0;
            }
        }

        /*  private void WfrmGestionDesLignesManuelles_FormClosing(object sender, FormClosingEventArgs e)
          {
              UpdateDonnees();
             // throw new Exception();
          }*/
    }
}