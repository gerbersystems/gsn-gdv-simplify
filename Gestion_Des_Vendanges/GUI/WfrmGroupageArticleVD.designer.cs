﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGroupageArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lSelectionArticle = new System.Windows.Forms.Label();
            this.chkLie = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkAut = new System.Windows.Forms.CheckBox();
            this.chkCom = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.optCep = new System.Windows.Forms.RadioButton();
            this.optCou = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPro = new System.Windows.Forms.CheckBox();
            this.chkCla = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuler.Location = new System.Drawing.Point(367, 214);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 5;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnOK.Location = new System.Drawing.Point(286, 214);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lSelectionArticle
            // 
            this.lSelectionArticle.AutoSize = true;
            this.lSelectionArticle.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lSelectionArticle.Location = new System.Drawing.Point(8, 9);
            this.lSelectionArticle.Name = "lSelectionArticle";
            this.lSelectionArticle.Size = new System.Drawing.Size(204, 20);
            this.lSelectionArticle.TabIndex = 3;
            this.lSelectionArticle.Text = "Groupage des articles";
            // 
            // chkLie
            // 
            this.chkLie.AutoSize = true;
            this.chkLie.Location = new System.Drawing.Point(16, 57);
            this.chkLie.Name = "chkLie";
            this.chkLie.Size = new System.Drawing.Size(114, 17);
            this.chkLie.TabIndex = 1;
            this.chkLie.Text = "Lieu de production";
            this.chkLie.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkAut);
            this.groupBox1.Controls.Add(this.chkCom);
            this.groupBox1.Controls.Add(this.chkLie);
            this.groupBox1.Location = new System.Drawing.Point(12, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 123);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lieu";
            // 
            // chkAut
            // 
            this.chkAut.AutoSize = true;
            this.chkAut.Location = new System.Drawing.Point(16, 80);
            this.chkAut.Name = "chkAut";
            this.chkAut.Size = new System.Drawing.Size(63, 17);
            this.chkAut.TabIndex = 2;
            this.chkAut.Text = "Parchet";
            this.chkAut.UseVisualStyleBackColor = true;
            // 
            // chkCom
            // 
            this.chkCom.AutoSize = true;
            this.chkCom.Location = new System.Drawing.Point(16, 34);
            this.chkCom.Name = "chkCom";
            this.chkCom.Size = new System.Drawing.Size(73, 17);
            this.chkCom.TabIndex = 0;
            this.chkCom.Text = "Commune";
            this.chkCom.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.optCep);
            this.groupBox2.Controls.Add(this.optCou);
            this.groupBox2.Location = new System.Drawing.Point(157, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(139, 123);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cépage";
            // 
            // optCep
            // 
            this.optCep.AutoSize = true;
            this.optCep.Location = new System.Drawing.Point(15, 57);
            this.optCep.Name = "optCep";
            this.optCep.Size = new System.Drawing.Size(62, 17);
            this.optCep.TabIndex = 1;
            this.optCep.TabStop = true;
            this.optCep.Text = "Cépage";
            this.optCep.UseVisualStyleBackColor = true;
            // 
            // optCou
            // 
            this.optCou.AutoSize = true;
            this.optCou.Location = new System.Drawing.Point(15, 33);
            this.optCou.Name = "optCou";
            this.optCou.Size = new System.Drawing.Size(61, 17);
            this.optCou.TabIndex = 0;
            this.optCou.TabStop = true;
            this.optCou.Text = "Couleur";
            this.optCou.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPro);
            this.groupBox3.Controls.Add(this.chkCla);
            this.groupBox3.Location = new System.Drawing.Point(302, 61);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(140, 123);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Autres groupements";
            // 
            // chkPro
            // 
            this.chkPro.AutoSize = true;
            this.chkPro.Location = new System.Drawing.Point(16, 57);
            this.chkPro.Name = "chkPro";
            this.chkPro.Size = new System.Drawing.Size(78, 17);
            this.chkPro.TabIndex = 1;
            this.chkPro.Text = "Producteur";
            this.chkPro.UseVisualStyleBackColor = true;
            // 
            // chkCla
            // 
            this.chkCla.AutoSize = true;
            this.chkCla.Location = new System.Drawing.Point(16, 34);
            this.chkCla.Name = "chkCla";
            this.chkCla.Size = new System.Drawing.Size(57, 17);
            this.chkCla.TabIndex = 0;
            this.chkCla.Text = "Classe";
            this.chkCla.UseVisualStyleBackColor = true;
            // 
            // WfrmGroupageArticle
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 256);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lSelectionArticle);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnAnnuler);
            this.MaximumSize = new System.Drawing.Size(470, 294);
            this.MinimumSize = new System.Drawing.Size(470, 294);
            this.Name = "WfrmGroupageArticle";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Gestion des vendanges - Séparation des articles";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lSelectionArticle;
        private System.Windows.Forms.CheckBox chkLie;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkAut;
        private System.Windows.Forms.CheckBox chkCom;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton optCep;
        private System.Windows.Forms.RadioButton optCou;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkPro;
        private System.Windows.Forms.CheckBox chkCla;
    }
}