﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des mentions
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesAutresMentions : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesAutresMentions _wfrm;

        public static WfrmGestionDesAutresMentions Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesAutresMentions();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int autId = (int)aUT_AUTRE_MENTIONDataGridView.CurrentRow.Cells["AUT_ID"].Value;
                    resultat = (int)aUT_AUTRE_MENTIONTableAdapter.NbRef(autId) == 0;
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesAutresMentions()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesAutresMentions_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            //INFO | Cette ligne de code charge les données dans la table 'dSPesees.AUT_AUTRE_MENTION'. Vous pouvez la déplacer ou la supprimer selon vos besoins.
            this.aUT_AUTRE_MENTIONTableAdapter.Fill(this.dSPesees.AUT_AUTRE_MENTION);
            _manager = new FormSettingsManagerPesees(this, aUT_IDTextBox, aUT_NOMTextBox, btnAddClass, btnSaveClass, btnSupprimer, aUT_AUTRE_MENTIONDataGridView, aUT_AUTRE_MENTIONBindingSource, tableAdapterManager, dSPesees);
            _manager.AddControl(aUT_NOMTextBox);
        }
    }
}