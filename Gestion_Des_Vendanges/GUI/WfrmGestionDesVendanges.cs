﻿using AutoMapper;
using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.Helpers;
using Gestion_Des_Vendanges.Properties;
using Gestion_Des_Vendanges.Services;
using Gestion_Des_Vendanges.XpertFINServiceReference;
using GSN.GDV.Data.Extensions;
using GSN.GDV.GUI.Common;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmGestionDesVendanges : WfrmMenu
    {
        #region Fields

        private CtrlWfrm _controlForm;
        private Panel _currentOptionPanel;
        private ToolStrip _currentToolStrip;
        private bool _cancelClick = false;
        private SplashScreen _splash;

        #endregion Fields

        #region Constructors

        public WfrmGestionDesVendanges()
        {
            using (new WorkingGlass(this))
            {
                new Installeur().CheckInstall();
                ConnectionManager.Init();
                ConnectionManager.Instance.InitGvConnectionString(Utilities.LicId);
            }
            CreateMainDataContext();
            ConfigureAutoMapper();

            InitializeSplashScreen();
            CheckLicense();
            CheckUpdates();
            InitializeComponent();
            InitializeButtonEvents();
            SetButtonsWithUserRightsLevel();
            ShowMainWindow();
            StartDefaultMenu();
        }

        private void CreateMainDataContext()
        {
            using (new WorkingGlass(this))
            {
                using (var db = ConnectionManager.Instance.CreateMainDataContext())
                {
                    SettingExtension.InitializeSettings(db, autoSave: true);
                }
            }
        }

        private void ConfigureAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<XpertFINpaymentHeader, XFINInterfacesIIHWriteInputData>();
                cfg.CreateMap<XpertFINpaymentRow, XFINInterfacesIIDWriteInputData>();
            });
        }

        private void InitializeSplashScreen()
        {
            _splash = new SplashScreen();
            _splash.LostFocus += SplashLostFocus;
            _splash.Show();
            _splash.TopLevel = true;
            TopMost = false;
        }

        private void CheckLicense()
        {
            _splash.Message = "Vérification des licences...";
            if (!LICENSE.LicenseManager.CheckLicense())
                Close();
        }

        private void CheckUpdates()
        {
            _splash.Message = "Vérification des mises à jour...";
            if (LICENSE.LicenseManager.NeedUpdate)
            {
                _splash.Message = "Téléchargement des mises à jour...";
                backgroundWorkerDoUpdate.DoWork += BackgroundWorkerDoUpdate_DoWork;
                backgroundWorkerDoUpdate.RunWorkerAsync();
            }
        }

        private void InitializeButtonEvents()
        {
            btnCockpit.Click += SelectMenu;
            btnAcquit.Click += SelectMenu;
            btnArticle.Click += SelectMenu;
            btnBonus.Click += SelectMenu;
            btnPaiement.Click += SelectMenu;
            btnPesee.Click += SelectMenu;
            btnRapport.Click += SelectMenu;
        }

        private void SetButtonsWithUserRightsLevel()
        {
            menuStrip.Visible = SettingsService.UserSettings.Rights > 0;
            btnAcquit.Visible = SettingsService.UserSettings.Rights > 0;
            btnArticle.Visible = SettingsService.UserSettings.Rights > 0;
            btnPaiement.Visible = SettingsService.UserSettings.Rights > 0;
            btnBonus.Visible = SettingsService.UserSettings.Rights > 0;
            btnRapport.Visible = SettingsService.UserSettings.Rights > 0;
            btnPesee.Visible = SettingsService.UserSettings.Rights > 0;
            btnCockpit.Visible = SettingsService.UserSettings.Rights > 0;
        }

        private void StartDefaultMenu()
        {
            _splash.Message = "Démarrage de l'application...";
            _controlForm = new CtrlWfrm();
            UpdateCtrWfrm(CtrlCockpit.GetInstance());
            SelectMenu(btnCockpit, null);
            btnCockpit.Focus();
        }

        private void ShowMainWindow()
        {
            _splash.LostFocus -= SplashLostFocus;
            Activate();
        }

        private void WfrmGestionDesVendanges_Load(object sender, EventArgs e) => UpdateTitre();

        private void WfrmGestionDesVendanges_Shown(object sender, EventArgs e) => _splash.SplashClose();

        #endregion Constructors

        #region MenuButton

        private void SelectMenu(object sender, EventArgs e)
        {
            if (sender is Button)
                SetColorToSelectedButton((sender as Button));
        }

        private void SetColorToSelectedButton(Button selectedButton)
        {
            if (selectedButton == null) return;

            if (_cancelClick)
            {
                _cancelClick = false;
                return;
            }

            var buttons = new List<Button>
            {
                btnCockpit,
                btnAcquit,
                btnPesee,
                btnArticle,
                btnBonus,
                btnPaiement,
                btnRapport
            };

            SetNormalBackColor(buttons);
            SetSelectedBackcolor((selectedButton));
        }

        private void SetNormalBackColor(IEnumerable<Button> buttons)
        {
            foreach (var button in buttons)
                button.BackColor = Settings.Default.Boutons;
        }

        private void SetSelectedBackcolor(Button button)
        {
            button.BackColor = Settings.Default.SelectedButton;
        }

        private void BtnCockpit_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlCockpit.GetInstance());

        private void BtnAcquit_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlGestionDesAcquits.GetInstance());

        private void BtnPesee_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlGestionDesPesees.GetCurrentInstance(true));

        private void BtnArticle_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlGestionDesArticles.GetInstance());

        private void BtnBonus_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlGestionDesBonus.GetInstance());

        private void BtnPaiement_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlGestionDesPaiements.GetInstance());

        private void BtnRapport_Click(object sender, EventArgs e) => UpdateCtrWfrm(CtrlReportViewer.GetInstance());

        #endregion MenuButton

        #region Methods

        private void BackgroundWorkerDoUpdate_DoWork(object sender, EventArgs e) => LICENSE.LicenseManager.DoUpdate();

        private void UpdateCtrWfrm(CtrlWfrm newCtrWfrm)
        {
            if (_controlForm == null || _controlForm.GetType() == newCtrWfrm.GetType())
            {
                _cancelClick = true;
                return;
            }
            splitPage.Panel2.Controls.Remove(_controlForm);
            _controlForm = newCtrWfrm;
            _controlForm.Dock = DockStyle.Fill;
            _controlForm.Size = splitPage.Panel2.Size;
            UpdateToolStrip();
            UpdateOptionPanel();
            splitPage.Panel2.Controls.Add(_controlForm);
        }

        private void UpdateToolStrip()
        {
            if (_currentToolStrip != null)
            {
                _currentToolStrip.Items.Clear();
                while (_controlForm?.ToolStrip?.Items.Count > 0)
                {
                    _currentToolStrip.Items.Add(_controlForm.ToolStrip.Items[0]);
                }
                _controlForm?.ToolStrip?.Hide();
            }
            else
            {
                Controls.Remove(menuStrip);
                _currentToolStrip = _controlForm.ToolStrip;
                Controls.Add(_currentToolStrip);
                Controls.Add(menuStrip);
            }
        }

        private void UpdateOptionPanel()
        {
            if (_currentOptionPanel != null)
            {
                splitLeft.Panel1.Controls.Remove(_currentOptionPanel);
            }
            if (_controlForm.OptionPanel == null) return;
            _currentOptionPanel = _controlForm.OptionPanel;
            _currentOptionPanel.Dock = DockStyle.Fill;
            splitLeft.Panel1.Controls.Add(_currentOptionPanel);
        }

        protected override void UpdateDonnees()
        {
            _controlForm.UpdateDonnees();
        }

        private void SplashLostFocus(object sender, EventArgs e)
        {
            if (Application.OpenForms.Count > 1)
                _splash.Hide();
        }

        #endregion Methods
    }
}