﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesClasses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cLA_NOMLabel;
            System.Windows.Forms.Label cLA_NUMEROLabel;
            System.Windows.Forms.Label cLA_NUMERO_CSCVLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cLA_CLASSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.cLA_IDTextBox = new System.Windows.Forms.TextBox();
            this.cLA_NOMTextBox = new System.Windows.Forms.TextBox();
            this.btnAddClass = new System.Windows.Forms.Button();
            this.btnSaveClass = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cLA_NUMERO_CSCVTextBox = new System.Windows.Forms.TextBox();
            this.cLA_NUMEROTextBox = new System.Windows.Forms.TextBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.cLA_CLASSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.cLA_CLASSEDataGridView = new System.Windows.Forms.DataGridView();
            this.CLA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLA_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            cLA_NOMLabel = new System.Windows.Forms.Label();
            cLA_NUMEROLabel = new System.Windows.Forms.Label();
            cLA_NUMERO_CSCVLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cLA_CLASSEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLA_CLASSEDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cLA_NOMLabel
            // 
            cLA_NOMLabel.AutoSize = true;
            cLA_NOMLabel.Location = new System.Drawing.Point(6, 59);
            cLA_NOMLabel.Name = "cLA_NOMLabel";
            cLA_NOMLabel.Size = new System.Drawing.Size(38, 13);
            cLA_NOMLabel.TabIndex = 15;
            cLA_NOMLabel.Text = "Nom : ";
            // 
            // cLA_NUMEROLabel
            // 
            cLA_NUMEROLabel.AutoSize = true;
            cLA_NUMEROLabel.Location = new System.Drawing.Point(6, 31);
            cLA_NUMEROLabel.Name = "cLA_NUMEROLabel";
            cLA_NUMEROLabel.Size = new System.Drawing.Size(50, 13);
            cLA_NUMEROLabel.TabIndex = 15;
            cLA_NUMEROLabel.Text = "Numéro :";
            // 
            // cLA_NUMERO_CSCVLabel
            // 
            cLA_NUMERO_CSCVLabel.AutoSize = true;
            cLA_NUMERO_CSCVLabel.Location = new System.Drawing.Point(6, 86);
            cLA_NUMERO_CSCVLabel.Name = "cLA_NUMERO_CSCVLabel";
            cLA_NUMERO_CSCVLabel.Size = new System.Drawing.Size(69, 13);
            cLA_NUMERO_CSCVLabel.TabIndex = 15;
            cLA_NUMERO_CSCVLabel.Text = "Code CSCV :";
            // 
            // cLA_CLASSEBindingSource
            // 
            this.cLA_CLASSEBindingSource.DataMember = "CLA_CLASSE";
            this.cLA_CLASSEBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cLA_IDTextBox
            // 
            this.cLA_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.cLA_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cLA_CLASSEBindingSource, "CLA_ID", true));
            this.cLA_IDTextBox.Enabled = false;
            this.cLA_IDTextBox.Location = new System.Drawing.Point(401, 95);
            this.cLA_IDTextBox.Name = "cLA_IDTextBox";
            this.cLA_IDTextBox.Size = new System.Drawing.Size(123, 19);
            this.cLA_IDTextBox.TabIndex = 14;
            // 
            // cLA_NOMTextBox
            // 
            this.cLA_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cLA_CLASSEBindingSource, "CLA_NOM", true));
            this.cLA_NOMTextBox.Location = new System.Drawing.Point(133, 56);
            this.cLA_NOMTextBox.Name = "cLA_NOMTextBox";
            this.cLA_NOMTextBox.Size = new System.Drawing.Size(156, 19);
            this.cLA_NOMTextBox.TabIndex = 1;
            // 
            // btnAddClass
            // 
            this.btnAddClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddClass.Location = new System.Drawing.Point(52, 152);
            this.btnAddClass.Name = "btnAddClass";
            this.btnAddClass.Size = new System.Drawing.Size(75, 30);
            this.btnAddClass.TabIndex = 3;
            this.btnAddClass.Text = "Nouveau";
            this.btnAddClass.UseVisualStyleBackColor = false;
            // 
            // btnSaveClass
            // 
            this.btnSaveClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveClass.Location = new System.Drawing.Point(133, 152);
            this.btnSaveClass.Name = "btnSaveClass";
            this.btnSaveClass.Size = new System.Drawing.Size(75, 30);
            this.btnSaveClass.TabIndex = 4;
            this.btnSaveClass.Text = "Modifier";
            this.btnSaveClass.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(cLA_NUMERO_CSCVLabel);
            this.groupBox1.Controls.Add(this.cLA_NUMERO_CSCVTextBox);
            this.groupBox1.Controls.Add(cLA_NUMEROLabel);
            this.groupBox1.Controls.Add(this.cLA_NUMEROTextBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnAddClass);
            this.groupBox1.Controls.Add(cLA_NOMLabel);
            this.groupBox1.Controls.Add(this.btnSaveClass);
            this.groupBox1.Controls.Add(this.cLA_NOMTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 188);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des classes";
            // 
            // cLA_NUMERO_CSCVTextBox
            // 
            this.cLA_NUMERO_CSCVTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cLA_CLASSEBindingSource, "CLA_NUMERO_CSCV1", true));
            this.cLA_NUMERO_CSCVTextBox.Location = new System.Drawing.Point(133, 83);
            this.cLA_NUMERO_CSCVTextBox.Name = "cLA_NUMERO_CSCVTextBox";
            this.cLA_NUMERO_CSCVTextBox.Size = new System.Drawing.Size(156, 19);
            this.cLA_NUMERO_CSCVTextBox.TabIndex = 2;
            // 
            // cLA_NUMEROTextBox
            // 
            this.cLA_NUMEROTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cLA_CLASSEBindingSource, "CLA_NUMERO", true));
            this.cLA_NUMEROTextBox.Location = new System.Drawing.Point(133, 28);
            this.cLA_NUMEROTextBox.Name = "cLA_NUMEROTextBox";
            this.cLA_NUMEROTextBox.Size = new System.Drawing.Size(156, 19);
            this.cLA_NUMEROTextBox.TabIndex = 0;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(214, 152);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 5;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // cLA_CLASSETableAdapter
            // 
            this.cLA_CLASSETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = this.cLA_CLASSETableAdapter;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // cLA_CLASSEDataGridView
            // 
            this.cLA_CLASSEDataGridView.AllowUserToAddRows = false;
            this.cLA_CLASSEDataGridView.AllowUserToDeleteRows = false;
            this.cLA_CLASSEDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cLA_CLASSEDataGridView.AutoGenerateColumns = false;
            this.cLA_CLASSEDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.cLA_CLASSEDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cLA_CLASSEDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CLA_ID,
            this.CLA_NUMERO,
            this.dataGridViewTextBoxColumn2});
            this.cLA_CLASSEDataGridView.DataSource = this.cLA_CLASSEBindingSource;
            this.cLA_CLASSEDataGridView.Location = new System.Drawing.Point(313, 15);
            this.cLA_CLASSEDataGridView.MultiSelect = false;
            this.cLA_CLASSEDataGridView.Name = "cLA_CLASSEDataGridView";
            this.cLA_CLASSEDataGridView.ReadOnly = true;
            this.cLA_CLASSEDataGridView.RowTemplate.Height = 24;
            this.cLA_CLASSEDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cLA_CLASSEDataGridView.Size = new System.Drawing.Size(360, 188);
            this.cLA_CLASSEDataGridView.TabIndex = 20;
            // 
            // CLA_ID
            // 
            this.CLA_ID.DataPropertyName = "CLA_ID";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Lavender;
            this.CLA_ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.CLA_ID.HeaderText = "Id";
            this.CLA_ID.Name = "CLA_ID";
            this.CLA_ID.ReadOnly = true;
            this.CLA_ID.Visible = false;
            this.CLA_ID.Width = 150;
            // 
            // CLA_NUMERO
            // 
            this.CLA_NUMERO.DataPropertyName = "CLA_NUMERO";
            this.CLA_NUMERO.HeaderText = "N°";
            this.CLA_NUMERO.Name = "CLA_NUMERO";
            this.CLA_NUMERO.ReadOnly = true;
            this.CLA_NUMERO.Width = 35;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CLA_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "Classe";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 175;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // WfrmGestionDesClasses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(685, 217);
            this.Controls.Add(this.cLA_CLASSEDataGridView);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cLA_IDTextBox);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(660, 250);
            this.Name = "WfrmGestionDesClasses";
            this.Text = "Gestion des classes";
            this.Load += new System.EventHandler(this.WfrmGestionDesClasses_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cLA_CLASSEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLA_CLASSEDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource cLA_CLASSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CLA_CLASSETableAdapter cLA_CLASSETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox cLA_IDTextBox;
        private System.Windows.Forms.TextBox cLA_NOMTextBox;
        private System.Windows.Forms.Button btnAddClass;
        private System.Windows.Forms.Button btnSaveClass;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.TextBox cLA_NUMEROTextBox;
        private System.Windows.Forms.DataGridView cLA_CLASSEDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLA_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.TextBox cLA_NUMERO_CSCVTextBox;
    }
}