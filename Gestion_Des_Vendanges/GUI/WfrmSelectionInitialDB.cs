﻿using System.Collections.Generic;
using System.Linq;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmSelectionInitialDB : WfrmBase
    {
        private Dictionary<int, string> _dbList;

        public WfrmSelectionInitialDB(Dictionary<int, string> dbList)
        {
            InitializeComponent();
            _dbList = new Dictionary<int, string>();
            _dbList = dbList;
            setComboboxListDataBases();
        }

        private void setComboboxListDataBases()
        {
            cmbDataBases.Items.AddRange(_dbList.Values.ToArray());
        }
    }
}