﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesCommunes
    {
        private void InitSpecificTableAdapters()
        {
            ConnectionManager.Instance.AddGvTableAdapter(dIS_DISTRICTTableAdapter);
            dIS_DISTRICTTableAdapter.Fill(dSPesees.DIS_DISTRICT);
        }

        private void AddSpecificControls()
        {
            _manager.AddControl(dIS_IDComboBox);
        }

        /*    private void cmdLieProd_Click(object sender, EventArgs e)
            {
                WfrmGestionDesLieuxDeProductions.Wfrm.WfrmShowDialog();
            }*/

        private void cOM_COMMUNEDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                cOM_COMMUNEBindingSource.RemoveSort();
                cOM_COMMUNETableAdapter.FillOrderDis(dSPesees.COM_COMMUNE);
                foreach (DataGridViewColumn column in cOM_COMMUNEDataGridView.Columns)
                {
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                cOM_COMMUNEDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void accederAuMenuGestionDesCommunesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesDistricts.Wfrm.WfrmShowDialog();
            dIS_DISTRICTTableAdapter.Fill(dSPesees.DIS_DISTRICT);
        }
    }
}