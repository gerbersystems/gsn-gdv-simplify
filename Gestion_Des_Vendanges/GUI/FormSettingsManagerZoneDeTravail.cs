﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class FormSettingsManagerZoneDeTravail : FormSettingsManagerPesees
    {
        private ComboBox _cbModeComptabilisation;
        private PAM_PARAMETRESTableAdapter _pamTable;

        public FormSettingsManagerZoneDeTravail(WfrmSetting form, TextBox idTextBox, Control focusControl, Button btnNew, Button btnUpdate, Button btnDelete, DataGridView dataGridView, BindingSource bindingSource, TableAdapterManager tableAdapterManager, DSPesees dataSet, ComboBox cbModeComptabilisation)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dataSet)
        {
            _pamTable = ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();

            _cbModeComptabilisation = cbModeComptabilisation;
            _cbModeComptabilisation.SelectedValue = (int)(_pamTable.GetDefaultZoneDeTravailMocId() ?? 0);
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            base.BtnOk_Click(sender, e);

            if (_cbModeComptabilisation.SelectedValue != null)
                _pamTable.UpdateDefaultZoneTravailMocId((int)_cbModeComptabilisation.SelectedValue);
        }
    }
}