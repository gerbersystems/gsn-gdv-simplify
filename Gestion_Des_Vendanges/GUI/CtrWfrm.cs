﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal class CtrlWfrm : UserControl
    {
        private string _text;

        public Form GetActiveForm()
        {
            return ParentForm;
        }

        public override string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        virtual public ToolStrip ToolStrip
        {
            get { throw new NotImplementedException(); }
        }

        virtual public Panel OptionPanel
        {
            get { return new Panel(); }
        }

        virtual public void UpdateDonnees()
        {
        }
    }
}