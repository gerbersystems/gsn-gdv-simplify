﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement des formulaires "PaiementManuel"
    *  Date de création:
    *  Modifications:
    *  ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class FormSettingsManagerPaiementManuel : FormSettingsManagerPesees
    {
        #region Properties

        private WfrmAjoutPaiementLigneManuelle _form;
        private DataGridView _dataGridView;
        private List<PaiementLigneManuelle> _lignesManuelles;
        private bool _isNew;
        private int _indexRow;

        public PaiementLigneManuelle[] LignesManuelles
        {
            get { return _lignesManuelles.ToArray(); }
        }

        #endregion Properties

        public FormSettingsManagerPaiementManuel(WfrmAjoutPaiementLigneManuelle form
                                               , Control focusControl
                                               , Button btnNew
                                               , Button btnUpdate
                                               , Button btnDelete
                                               , DataGridView dataGridView
                                               , PaiementLigneManuelle[] ligneManuelles)
            : base(form, new TextBox(), focusControl, btnNew, btnUpdate, btnDelete, dataGridView, new BindingSource(), new TableAdapterManager(), new DSPesees())
        {
            _form = form;
            _dataGridView = dataGridView;
            _lignesManuelles = new List<PaiementLigneManuelle>();

            foreach (PaiementLigneManuelle ligne in ligneManuelles)
                addLigne(ligne);
        }

        #region Button events

        protected override void BtnNew_Click(object sender, EventArgs e)
        {
            ChangeEditMode(true);
            resetControlsValues();
            _isNew = true;
            _form.CheckedOption();
        }

        protected override void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _indexRow = _dataGridView.SelectedRows[0].Index;
                _form.SetChamps(_lignesManuelles[_indexRow]);
                ChangeEditMode(true);
                _isNew = false;
                _form.CheckedOption();
            }
            catch { }
        }

        protected override void BtnOk_Click(object sender, EventArgs e)
        {
            if (_isNew)
            {
                try
                {
                    addLigne(newLigneManuelle: _form.GetLigneManuelle());
                }
                catch { }
            }
            else
            {
                try
                {
                    updateLigne(newLigneManuelle: _form.GetLigneManuelle());
                }
                catch { }
            }
        }

        protected override void BtnCancel_Click(object sender, EventArgs e)
        {
            ChangeEditMode(false);
            resetControlsValues();
        }

        protected override void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int idList = (int)_dataGridView.SelectedRows[0].Cells["idList"].Value;
                _indexRow = _dataGridView.SelectedRows[0].Index;

                DialogResult result = MessageBox.Show("Voulez-vous supprimer cette ligne manuelle?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    _dataGridView.Rows.RemoveAt(_indexRow);
                    _lignesManuelles.RemoveAt(idList);
                }
            }
            catch { }
        }

        #endregion Button events

        #region Add LigneManuelle

        private void addLigne(PaiementLigneManuelle newLigneManuelle)
        {
            _lignesManuelles.Add(newLigneManuelle);
            addLastLigneDataGridView();
            ChangeEditMode(false);
        }

        private void addLastLigneDataGridView()
        {
            int idLigne = _lignesManuelles.Count - 1;
            PaiementLigneManuelle ligneManuelle = _lignesManuelles[idLigne];
            object[] values = new object[5];
            values[0] = idLigne;
            values[1] = ligneManuelle.Producteur;
            values[2] = ligneManuelle.Texte;
            values[3] = ligneManuelle.MontantIsFixe;
            values[4] = ligneManuelle.Valeur;
            _dataGridView.Rows.Add(values);
        }

        #endregion Add LigneManuelle

        #region Update LigneManuelle

        private void updateLigne(PaiementLigneManuelle newLigneManuelle)
        {
            _lignesManuelles[_indexRow] = newLigneManuelle;
            updateLigneDataGridView(newLigneManuelle);
            ChangeEditMode(false);
        }

        private void updateLigneDataGridView(PaiementLigneManuelle newligneManuelle)
        {
            int i;
            bool trouver = false;

            for (i = 0; i < _dataGridView.Rows.Count && !trouver; i++)
            {
                if ((int)_dataGridView.Rows[i].Cells["idList"].Value == _indexRow)
                    trouver = true;
            }

            if (trouver)
            {
                DataGridViewCellCollection cells = _dataGridView.Rows[i - 1].Cells;
                cells["producteur"].Value = newligneManuelle.Producteur;
                cells["texte"].Value = newligneManuelle.Texte;
                cells["montantIsFixe"].Value = newligneManuelle.MontantIsFixe;
                cells["valeur"].Value = newligneManuelle.Valeur;
            }
            else
            {
                throw new InvalidOperationException("Aucune ligne ne correspond");
            }
        }

        #endregion Update LigneManuelle

        private void resetControlsValues()
        {
            foreach (Control control in Controls)
            {
                if (control is TextBox)
                {
                    control.Text = "";
                }
                else if (control is RadioButton)
                {
                    RadioButton rd = (RadioButton)control;
                    rd.Checked = false;
                }
                else if (control is NumericUpDown)
                {
                    NumericUpDown numUD = (NumericUpDown)control;
                    numUD.Value = 0;
                }
            }
        }

        internal void ExportCsvFileTo(ExportCsvType exportCsvType, string directoryName)
        {
            string fileName = $@"{directoryName}\Gdv_LignesManuelles-{exportCsvType.ToString()}.csv";
            ImportExportLigneManuelleBase exportLigneManuelle;

            switch (exportCsvType)
            {
                case ExportCsvType.Standard:
                    exportLigneManuelle = new ImportExportLigneManuelleStandard(_form.GetProducteurIdList(), fileName);
                    break;

                case ExportCsvType.ZoneTravail:
                    exportLigneManuelle = new ImportExportLigneManuelleZoneTravail(_form.GetProducteurIdList(), fileName);
                    break;

                case ExportCsvType.ModeComptabilisation:
                    exportLigneManuelle = new ImportExportLigneManuelleModeComptabilisation(_form.GetProducteurIdList(), fileName);
                    break;

                default:
                    throw new ArgumentException("Le type de format d'export n'existe pas.");
            }

            var filePathCreated = exportLigneManuelle.CreateFichierCsvProducteurs(directoryName, exportCsvType);

            if (filePathCreated == string.Empty || !File.Exists(filePathCreated))
                MessageBox.Show("Une erreur est survenue durant l'exportation." + filePathCreated, "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                MessageBox.Show("L'exportation du fichier\n" + Path.GetFileName(filePathCreated) + "\ndans\n" + Path.GetDirectoryName(filePathCreated) + "\na été effectuée avec succès !", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        internal void ImportCsvFileFrom(ExportCsvType exportType, string filePath, int modeComptabilisationId, string typeFormatImport)
        {
            ImportExportLigneManuelleBase _importLigneManuelle;
            switch (exportType)
            {
                case ExportCsvType.Standard:
                    _importLigneManuelle = new ImportExportLigneManuelleStandard(producteurIdList: _form.GetProducteurIdList(), fichier: filePath);
                    break;

                case ExportCsvType.ZoneTravail:
                    _importLigneManuelle = new ImportExportLigneManuelleZoneTravail(producteurIdList: _form.GetProducteurIdList(), fichier: filePath);
                    break;

                case ExportCsvType.ModeComptabilisation:
                    _importLigneManuelle = new ImportExportLigneManuelleModeComptabilisation(producteurIdList: _form.GetProducteurIdList(), fichier: filePath);
                    break;

                default:
                    throw new ArgumentException("Le type de format d'import n'existe pas.");
            }

            CreateImportedLignesManuelles(_importLigneManuelle.GetFichierCsvProducteurs(filePath), modeComptabilisationId, exportType);

            if (_lignesManuelles.Count > 0)
                MessageBox.Show("L'importation de " + _lignesManuelles.Count + " ligne(s) manuelle(s) " + "\na été effectuée avec succès !", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Le format du fichier\n" + Path.GetFileName(filePath) + "\nne correspond pas au format " + typeFormatImport + "\nou\nCertaines valeurs sont manquantes.", "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CreateImportedLignesManuelles(string[,] tableauLignesManuellesParProducteur, int modeComptabilisationId, ExportCsvType exportType)
        {
            var producers = _form.GetProducteurIdList();
            for (int i = 1; i < tableauLignesManuellesParProducteur.GetLength(0); i++)
            {
                if (!IsProducerIdPresent(tableauLignesManuellesParProducteur[i, 0], producers))
                    continue;

                if (!Decimal.TryParse(tableauLignesManuellesParProducteur[i, GetIndexColumn(exportType)].Replace(",", "."),
                                      NumberStyles.Any, CultureInfo.InvariantCulture, out decimal valeur))
                    continue;

                var descriptionLigneManuelle = SetDescription(exportType, tableauLignesManuellesParProducteur, i);

                var ligneManuelle = new PaiementLigneManuelle(idProducteur: Convert.ToInt32(tableauLignesManuellesParProducteur[i, 0]),
                                                              texte: descriptionLigneManuelle,
                                                              valeur: valeur,
                                                              montantIsFixe: true);

                switch (exportType)
                {
                    case ExportCsvType.ModeComptabilisation:

                        if (int.TryParse(tableauLignesManuellesParProducteur[i, 5], out int mocId))
                            ligneManuelle.MocId = mocId;
                        break;

                    default:
                        ligneManuelle.MocId = modeComptabilisationId;
                        break;
                }

                addLigne(ligneManuelle);
            }
        }

        private bool IsProducerIdPresent(string producerId, string[,] producers)
        {
            for (int i = 0; i < producers.GetLength(0); i++)
            {
                if (producers[i, 0].Equals(producerId))
                    return true;
            }

            return false;
        }

        private static string SetDescription(ExportCsvType exportType, string[,] tableauLignesManuellesParProducteur, int index)
        {
            switch (exportType)
            {
                case ExportCsvType.Standard:
                    return $"{tableauLignesManuellesParProducteur[index, 2]}";

                case ExportCsvType.ZoneTravail:
                    return $"{tableauLignesManuellesParProducteur[index, 2]} : {tableauLignesManuellesParProducteur[index, 3]} m2 x {tableauLignesManuellesParProducteur[index, 4]} chf";

                case ExportCsvType.ModeComptabilisation:
                    return $"{tableauLignesManuellesParProducteur[index, 3]}";

                default:
                    throw new ArgumentNullException();
            }
        }

        private static int GetIndexColumn(ExportCsvType exportType)
        {
            switch (exportType)
            {
                case ExportCsvType.Standard:
                    return 3;

                case ExportCsvType.ZoneTravail:
                    return 5;

                case ExportCsvType.ModeComptabilisation:
                    return 4;

                default:
                    return 0;
            }
        }
    }
}