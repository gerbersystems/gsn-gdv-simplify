﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlCockpit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtrlCockpit));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.aNN_ANNEETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter();
            this.pRO_NOMCOMPLETTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pAR_PARCELLETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.pAR_PARCELLEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aNNANNEEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOMCOMPLETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pnlYear = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.lblCockpit = new System.Windows.Forms.Label();
            this.cbxExploitant = new System.Windows.Forms.ComboBox();
            this.cockpitAcquitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cockpitAcquitsTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CockpitAcquitsTableAdapter();
            this.acQ_ACQUITTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter();
            this.btnAddAcq = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolAdd = new System.Windows.Forms.ToolStripButton();
            this.toolUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.acquitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesAcquitsDeVendangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPrintProduction = new System.Windows.Forms.ToolStripButton();
            this.toolVisit = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.TotalLabel = new System.Windows.Forms.Label();
            this.lblAcquits = new System.Windows.Forms.Label();
            this.cockpitAcquitsDataGridView = new System.Windows.Forms.DataGridView();
            this.ACQ_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acquitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lieuProductionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COM_NOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Couleur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomLocalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surfaceM2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cépageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.litrem2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kgLivréDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.difDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNewWeighing = new System.Windows.Forms.DataGridViewButtonColumn();
            this.PAR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CEP_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.TotalSurfaceAreaLabel = new System.Windows.Forms.Label();
            this.TotalLitersQuotaLabel = new System.Windows.Forms.Label();
            this.TotalKilosDeliveredLabel = new System.Windows.Forms.Label();
            this.cockpitWeighingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cockpitWeighingsTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.CockpitWeighingsTableAdapter();
            this.cockpitWeighingsDataGridView = new System.Windows.Forms.DataGridView();
            this.PED_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_QUANTITE_KG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblWeighings = new System.Windows.Forms.Label();
            this.peE_PESEE_ENTETETableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter();
            this.peD_PESEE_DETAILTableAdapter1 = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter();
            this.ShowR_ApportsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).BeginInit();
            this.pnlYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitAcquitsBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitAcquitsDataGridView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitWeighingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitWeighingsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // aNN_ANNEETableAdapter
            // 
            this.aNN_ANNEETableAdapter.ClearBeforeFill = true;
            // 
            // pRO_NOMCOMPLETTableAdapter
            // 
            this.pRO_NOMCOMPLETTableAdapter.ClearBeforeFill = true;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pAR_PARCELLETableAdapter
            // 
            this.pAR_PARCELLETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = this.pAR_PARCELLETableAdapter;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // aNNANNEEBindingSource
            // 
            this.aNNANNEEBindingSource.DataMember = "ANN_ANNEE";
            this.aNNANNEEBindingSource.DataSource = this.dSPesees;
            // 
            // pRONOMCOMPLETBindingSource
            // 
            this.pRONOMCOMPLETBindingSource.DataMember = "PRO_NOMCOMPLET";
            this.pRONOMCOMPLETBindingSource.DataSource = this.dSPesees;
            // 
            // pnlYear
            // 
            this.pnlYear.Controls.Add(this.label2);
            this.pnlYear.Controls.Add(this.cbAnnee);
            this.pnlYear.Location = new System.Drawing.Point(768, 448);
            this.pnlYear.Margin = new System.Windows.Forms.Padding(2);
            this.pnlYear.Name = "pnlYear";
            this.pnlYear.Size = new System.Drawing.Size(150, 171);
            this.pnlYear.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(3, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DataSource = this.aNNANNEEBindingSource;
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Margin = new System.Windows.Forms.Padding(2);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(144, 23);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            // 
            // lblCockpit
            // 
            this.lblCockpit.AutoSize = true;
            this.lblCockpit.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lblCockpit.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.lblCockpit.Location = new System.Drawing.Point(-3, 9);
            this.lblCockpit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCockpit.Name = "lblCockpit";
            this.lblCockpit.Size = new System.Drawing.Size(141, 21);
            this.lblCockpit.TabIndex = 11;
            this.lblCockpit.Text = "Cockpit vigneron";
            // 
            // cbxExploitant
            // 
            this.cbxExploitant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxExploitant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxExploitant.DataSource = this.pRONOMCOMPLETBindingSource;
            this.cbxExploitant.DisplayMember = "NOMCOMPLET";
            this.cbxExploitant.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.cbxExploitant.FormattingEnabled = true;
            this.cbxExploitant.Location = new System.Drawing.Point(147, 10);
            this.cbxExploitant.Margin = new System.Windows.Forms.Padding(2);
            this.cbxExploitant.Name = "cbxExploitant";
            this.cbxExploitant.Size = new System.Drawing.Size(499, 23);
            this.cbxExploitant.TabIndex = 12;
            this.cbxExploitant.ValueMember = "PRO_ID";
            // 
            // cockpitAcquitsBindingSource
            // 
            this.cockpitAcquitsBindingSource.DataMember = "CockpitAcquits";
            this.cockpitAcquitsBindingSource.DataSource = this.dSPesees;
            // 
            // cockpitAcquitsTableAdapter
            // 
            this.cockpitAcquitsTableAdapter.ClearBeforeFill = true;
            // 
            // acQ_ACQUITTableAdapter
            // 
            this.acQ_ACQUITTableAdapter.ClearBeforeFill = true;
            // 
            // btnAddAcq
            // 
            this.btnAddAcq.AutoSize = true;
            this.btnAddAcq.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnAddAcq.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddAcq.Location = new System.Drawing.Point(105, 44);
            this.btnAddAcq.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddAcq.Name = "btnAddAcq";
            this.btnAddAcq.Size = new System.Drawing.Size(109, 25);
            this.btnAddAcq.TabIndex = 16;
            this.btnAddAcq.Text = "+ Acquit-parcelle";
            this.btnAddAcq.UseVisualStyleBackColor = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolAdd,
            this.toolUpdate,
            this.toolDelete,
            this.toolStripSeparator1,
            this.toolStripButton1,
            this.toolPrintProduction,
            this.toolVisit,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(1013, 27);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolAdd
            // 
            this.toolAdd.Image = global::Gestion_Des_Vendanges.Properties.Resources.add;
            this.toolAdd.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolAdd.Name = "toolAdd";
            this.toolAdd.Size = new System.Drawing.Size(152, 24);
            this.toolAdd.Text = "Ajouter acquit-parcelle";
            // 
            // toolUpdate
            // 
            this.toolUpdate.Image = global::Gestion_Des_Vendanges.Properties.Resources.open;
            this.toolUpdate.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolUpdate.Name = "toolUpdate";
            this.toolUpdate.Size = new System.Drawing.Size(158, 24);
            this.toolUpdate.Text = "Modifier acquit-parcelle";
            // 
            // toolDelete
            // 
            this.toolDelete.Image = global::Gestion_Des_Vendanges.Properties.Resources.delete;
            this.toolDelete.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(86, 24);
            this.toolDelete.Text = "Supprimer";
            this.toolDelete.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acquitsToolStripMenuItem,
            this.listeDesAcquitsDeVendangeToolStripMenuItem,
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem});
            this.toolStripButton1.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(83, 24);
            this.toolStripButton1.Text = "Acquits";
            this.toolStripButton1.Visible = false;
            // 
            // acquitsToolStripMenuItem
            // 
            this.acquitsToolStripMenuItem.Name = "acquitsToolStripMenuItem";
            this.acquitsToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.acquitsToolStripMenuItem.Text = "Acquits";
            // 
            // listeDesAcquitsDeVendangeToolStripMenuItem
            // 
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Name = "listeDesAcquitsDeVendangeToolStripMenuItem";
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.listeDesAcquitsDeVendangeToolStripMenuItem.Text = "Liste des acquits de vendange";
            // 
            // listeDesAcquitsDeVendangeParCépageToolStripMenuItem
            // 
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Name = "listeDesAcquitsDeVendangeParCépageToolStripMenuItem";
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Size = new System.Drawing.Size(292, 22);
            this.listeDesAcquitsDeVendangeParCépageToolStripMenuItem.Text = "Liste des acquits de vendange par cépage";
            // 
            // toolPrintProduction
            // 
            this.toolPrintProduction.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolPrintProduction.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolPrintProduction.Name = "toolPrintProduction";
            this.toolPrintProduction.Size = new System.Drawing.Size(140, 24);
            this.toolPrintProduction.Text = "Droits de production";
            this.toolPrintProduction.Visible = false;
            // 
            // toolVisit
            // 
            this.toolVisit.Image = global::Gestion_Des_Vendanges.Properties.Resources.apercu;
            this.toolVisit.ImageTransparentColor = System.Drawing.Color.Black;
            this.toolVisit.Name = "toolVisit";
            this.toolVisit.Size = new System.Drawing.Size(117, 24);
            this.toolVisit.Text = "Visite des vignes";
            this.toolVisit.Visible = false;
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Visible = false;
            // 
            // TotalLabel
            // 
            this.TotalLabel.AutoSize = true;
            this.TotalLabel.Location = new System.Drawing.Point(3, 0);
            this.TotalLabel.Name = "TotalLabel";
            this.TotalLabel.Padding = new System.Windows.Forms.Padding(5);
            this.TotalLabel.Size = new System.Drawing.Size(44, 25);
            this.TotalLabel.TabIndex = 18;
            this.TotalLabel.Text = "Total";
            this.TotalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAcquits
            // 
            this.lblAcquits.AutoSize = true;
            this.lblAcquits.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblAcquits.Location = new System.Drawing.Point(-3, 49);
            this.lblAcquits.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAcquits.Name = "lblAcquits";
            this.lblAcquits.Padding = new System.Windows.Forms.Padding(1);
            this.lblAcquits.Size = new System.Drawing.Size(104, 17);
            this.lblAcquits.TabIndex = 15;
            this.lblAcquits.Text = "Acquits-parcelles";
            // 
            // cockpitAcquitsDataGridView
            // 
            this.cockpitAcquitsDataGridView.AllowUserToAddRows = false;
            this.cockpitAcquitsDataGridView.AllowUserToDeleteRows = false;
            this.cockpitAcquitsDataGridView.AllowUserToResizeRows = false;
            this.cockpitAcquitsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cockpitAcquitsDataGridView.AutoGenerateColumns = false;
            this.cockpitAcquitsDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.cockpitAcquitsDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.cockpitAcquitsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.cockpitAcquitsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cockpitAcquitsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACQ_ID,
            this.acquitDataGridViewTextBoxColumn,
            this.lieuProductionDataGridViewTextBoxColumn,
            this.COM_NOM,
            this.Couleur,
            this.nomLocalDataGridViewTextBoxColumn,
            this.surfaceM2DataGridViewTextBoxColumn,
            this.cépageDataGridViewTextBoxColumn,
            this.classeDataGridViewTextBoxColumn,
            this.litrem2DataGridViewTextBoxColumn,
            this.kgLivréDataGridViewTextBoxColumn,
            this.difDataGridViewTextBoxColumn,
            this.Diff,
            this.btnNewWeighing,
            this.PAR_ID,
            this.CEP_ID});
            this.cockpitAcquitsDataGridView.DataSource = this.cockpitAcquitsBindingSource;
            this.cockpitAcquitsDataGridView.Location = new System.Drawing.Point(1, 109);
            this.cockpitAcquitsDataGridView.MaximumSize = new System.Drawing.Size(10000, 400);
            this.cockpitAcquitsDataGridView.MinimumSize = new System.Drawing.Size(500, 200);
            this.cockpitAcquitsDataGridView.MultiSelect = false;
            this.cockpitAcquitsDataGridView.Name = "cockpitAcquitsDataGridView";
            this.cockpitAcquitsDataGridView.ReadOnly = true;
            this.cockpitAcquitsDataGridView.RowHeadersVisible = false;
            this.cockpitAcquitsDataGridView.RowHeadersWidth = 10;
            this.cockpitAcquitsDataGridView.RowTemplate.Height = 24;
            this.cockpitAcquitsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cockpitAcquitsDataGridView.Size = new System.Drawing.Size(1013, 200);
            this.cockpitAcquitsDataGridView.TabIndex = 15;
            // 
            // ACQ_ID
            // 
            this.ACQ_ID.DataPropertyName = "ACQ_ID";
            this.ACQ_ID.HeaderText = "ACQ_ID";
            this.ACQ_ID.Name = "ACQ_ID";
            this.ACQ_ID.ReadOnly = true;
            this.ACQ_ID.Visible = false;
            // 
            // acquitDataGridViewTextBoxColumn
            // 
            this.acquitDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.acquitDataGridViewTextBoxColumn.DataPropertyName = "No_ acquit";
            this.acquitDataGridViewTextBoxColumn.HeaderText = "Acquit";
            this.acquitDataGridViewTextBoxColumn.Name = "acquitDataGridViewTextBoxColumn";
            this.acquitDataGridViewTextBoxColumn.ReadOnly = true;
            this.acquitDataGridViewTextBoxColumn.Width = 67;
            // 
            // lieuProductionDataGridViewTextBoxColumn
            // 
            this.lieuProductionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.lieuProductionDataGridViewTextBoxColumn.DataPropertyName = "Lieu Production";
            this.lieuProductionDataGridViewTextBoxColumn.HeaderText = "Lieu";
            this.lieuProductionDataGridViewTextBoxColumn.Name = "lieuProductionDataGridViewTextBoxColumn";
            this.lieuProductionDataGridViewTextBoxColumn.ReadOnly = true;
            this.lieuProductionDataGridViewTextBoxColumn.Width = 54;
            // 
            // COM_NOM
            // 
            this.COM_NOM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.COM_NOM.DataPropertyName = "COM_NOM";
            this.COM_NOM.HeaderText = "Commune";
            this.COM_NOM.Name = "COM_NOM";
            this.COM_NOM.ReadOnly = true;
            this.COM_NOM.Width = 89;
            // 
            // Couleur
            // 
            this.Couleur.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Couleur.DataPropertyName = "Couleur";
            this.Couleur.HeaderText = "Couleur";
            this.Couleur.Name = "Couleur";
            this.Couleur.ReadOnly = true;
            this.Couleur.Width = 74;
            // 
            // nomLocalDataGridViewTextBoxColumn
            // 
            this.nomLocalDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomLocalDataGridViewTextBoxColumn.DataPropertyName = "Nom Local";
            this.nomLocalDataGridViewTextBoxColumn.FillWeight = 1000F;
            this.nomLocalDataGridViewTextBoxColumn.HeaderText = "Nom Local";
            this.nomLocalDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.nomLocalDataGridViewTextBoxColumn.Name = "nomLocalDataGridViewTextBoxColumn";
            this.nomLocalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // surfaceM2DataGridViewTextBoxColumn
            // 
            this.surfaceM2DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.surfaceM2DataGridViewTextBoxColumn.DataPropertyName = "Surface m2";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "# ##0 m2";
            this.surfaceM2DataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.surfaceM2DataGridViewTextBoxColumn.HeaderText = "Surface";
            this.surfaceM2DataGridViewTextBoxColumn.Name = "surfaceM2DataGridViewTextBoxColumn";
            this.surfaceM2DataGridViewTextBoxColumn.ReadOnly = true;
            this.surfaceM2DataGridViewTextBoxColumn.Width = 71;
            // 
            // cépageDataGridViewTextBoxColumn
            // 
            this.cépageDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cépageDataGridViewTextBoxColumn.DataPropertyName = "Cépage";
            this.cépageDataGridViewTextBoxColumn.HeaderText = "Cépage";
            this.cépageDataGridViewTextBoxColumn.Name = "cépageDataGridViewTextBoxColumn";
            this.cépageDataGridViewTextBoxColumn.ReadOnly = true;
            this.cépageDataGridViewTextBoxColumn.Width = 72;
            // 
            // classeDataGridViewTextBoxColumn
            // 
            this.classeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.classeDataGridViewTextBoxColumn.DataPropertyName = "Classe";
            this.classeDataGridViewTextBoxColumn.DividerWidth = 1;
            this.classeDataGridViewTextBoxColumn.HeaderText = "Classe";
            this.classeDataGridViewTextBoxColumn.Name = "classeDataGridViewTextBoxColumn";
            this.classeDataGridViewTextBoxColumn.ReadOnly = true;
            this.classeDataGridViewTextBoxColumn.Width = 66;
            // 
            // litrem2DataGridViewTextBoxColumn
            // 
            this.litrem2DataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.litrem2DataGridViewTextBoxColumn.DataPropertyName = "Litre/m2";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.litrem2DataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.litrem2DataGridViewTextBoxColumn.HeaderText = "L/m2";
            this.litrem2DataGridViewTextBoxColumn.Name = "litrem2DataGridViewTextBoxColumn";
            this.litrem2DataGridViewTextBoxColumn.ReadOnly = true;
            this.litrem2DataGridViewTextBoxColumn.Width = 60;
            // 
            // kgLivréDataGridViewTextBoxColumn
            // 
            this.kgLivréDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.kgLivréDataGridViewTextBoxColumn.DataPropertyName = "Kg livré";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "# ##0 kg";
            this.kgLivréDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.kgLivréDataGridViewTextBoxColumn.HeaderText = "Kg livré";
            this.kgLivréDataGridViewTextBoxColumn.Name = "kgLivréDataGridViewTextBoxColumn";
            this.kgLivréDataGridViewTextBoxColumn.ReadOnly = true;
            this.kgLivréDataGridViewTextBoxColumn.Width = 71;
            // 
            // difDataGridViewTextBoxColumn
            // 
            this.difDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.difDataGridViewTextBoxColumn.DataPropertyName = "dif %";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#0 \\%";
            this.difDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.difDataGridViewTextBoxColumn.DividerWidth = 1;
            this.difDataGridViewTextBoxColumn.HeaderText = "% livré";
            this.difDataGridViewTextBoxColumn.Name = "difDataGridViewTextBoxColumn";
            this.difDataGridViewTextBoxColumn.ReadOnly = true;
            this.difDataGridViewTextBoxColumn.Width = 68;
            // 
            // Diff
            // 
            this.Diff.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Diff.DataPropertyName = "Diff";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Format = "# ### kg";
            this.Diff.DefaultCellStyle = dataGridViewCellStyle5;
            this.Diff.HeaderText = "Diff Kg";
            this.Diff.Name = "Diff";
            this.Diff.ReadOnly = true;
            this.Diff.Width = 68;
            // 
            // btnNewWeighing
            // 
            this.btnNewWeighing.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnNewWeighing.DefaultCellStyle = dataGridViewCellStyle6;
            this.btnNewWeighing.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNewWeighing.HeaderText = "";
            this.btnNewWeighing.Name = "btnNewWeighing";
            this.btnNewWeighing.ReadOnly = true;
            this.btnNewWeighing.Text = "+Pesée";
            this.btnNewWeighing.UseColumnTextForButtonValue = true;
            this.btnNewWeighing.Width = 5;
            // 
            // PAR_ID
            // 
            this.PAR_ID.DataPropertyName = "PAR_ID";
            this.PAR_ID.HeaderText = "PAR_ID";
            this.PAR_ID.Name = "PAR_ID";
            this.PAR_ID.ReadOnly = true;
            this.PAR_ID.Visible = false;
            // 
            // CEP_ID
            // 
            this.CEP_ID.DataPropertyName = "CEP_ID";
            this.CEP_ID.HeaderText = "CEP_ID";
            this.CEP_ID.Name = "CEP_ID";
            this.CEP_ID.ReadOnly = true;
            this.CEP_ID.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.TotalLabel);
            this.flowLayoutPanel1.Controls.Add(this.TotalSurfaceAreaLabel);
            this.flowLayoutPanel1.Controls.Add(this.TotalLitersQuotaLabel);
            this.flowLayoutPanel1.Controls.Add(this.TotalKilosDeliveredLabel);
            this.flowLayoutPanel1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 77);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(156, 29);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // TotalSurfaceAreaLabel
            // 
            this.TotalSurfaceAreaLabel.AutoSize = true;
            this.TotalSurfaceAreaLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TotalSurfaceAreaLabel.Location = new System.Drawing.Point(53, 0);
            this.TotalSurfaceAreaLabel.Name = "TotalSurfaceAreaLabel";
            this.TotalSurfaceAreaLabel.Padding = new System.Windows.Forms.Padding(5);
            this.TotalSurfaceAreaLabel.Size = new System.Drawing.Size(62, 27);
            this.TotalSurfaceAreaLabel.TabIndex = 19;
            this.TotalSurfaceAreaLabel.Text = "Surface";
            // 
            // TotalLitersQuotaLabel
            // 
            this.TotalLitersQuotaLabel.AutoSize = true;
            this.TotalLitersQuotaLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TotalLitersQuotaLabel.Location = new System.Drawing.Point(121, 0);
            this.TotalLitersQuotaLabel.Name = "TotalLitersQuotaLabel";
            this.TotalLitersQuotaLabel.Padding = new System.Windows.Forms.Padding(5);
            this.TotalLitersQuotaLabel.Size = new System.Drawing.Size(12, 27);
            this.TotalLitersQuotaLabel.TabIndex = 20;
            // 
            // TotalKilosDeliveredLabel
            // 
            this.TotalKilosDeliveredLabel.AutoSize = true;
            this.TotalKilosDeliveredLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TotalKilosDeliveredLabel.Location = new System.Drawing.Point(139, 0);
            this.TotalKilosDeliveredLabel.Name = "TotalKilosDeliveredLabel";
            this.TotalKilosDeliveredLabel.Padding = new System.Windows.Forms.Padding(5);
            this.TotalKilosDeliveredLabel.Size = new System.Drawing.Size(12, 27);
            this.TotalKilosDeliveredLabel.TabIndex = 21;
            // 
            // cockpitWeighingsBindingSource
            // 
            this.cockpitWeighingsBindingSource.DataMember = "CockpitWeighings";
            this.cockpitWeighingsBindingSource.DataSource = this.dSPesees;
            // 
            // cockpitWeighingsTableAdapter
            // 
            this.cockpitWeighingsTableAdapter.ClearBeforeFill = true;
            // 
            // cockpitWeighingsDataGridView
            // 
            this.cockpitWeighingsDataGridView.AllowUserToAddRows = false;
            this.cockpitWeighingsDataGridView.AllowUserToDeleteRows = false;
            this.cockpitWeighingsDataGridView.AllowUserToResizeRows = false;
            this.cockpitWeighingsDataGridView.AutoGenerateColumns = false;
            this.cockpitWeighingsDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.cockpitWeighingsDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.cockpitWeighingsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.cockpitWeighingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cockpitWeighingsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PED_ID,
            this.PED_QUANTITE_KG,
            this.PEE_ID,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.cockpitWeighingsDataGridView.DataSource = this.cockpitWeighingsBindingSource;
            this.cockpitWeighingsDataGridView.Location = new System.Drawing.Point(1, 345);
            this.cockpitWeighingsDataGridView.MultiSelect = false;
            this.cockpitWeighingsDataGridView.Name = "cockpitWeighingsDataGridView";
            this.cockpitWeighingsDataGridView.ReadOnly = true;
            this.cockpitWeighingsDataGridView.RowHeadersVisible = false;
            this.cockpitWeighingsDataGridView.RowTemplate.Height = 24;
            this.cockpitWeighingsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cockpitWeighingsDataGridView.Size = new System.Drawing.Size(578, 198);
            this.cockpitWeighingsDataGridView.TabIndex = 19;
            // 
            // PED_ID
            // 
            this.PED_ID.DataPropertyName = "PED_ID";
            this.PED_ID.HeaderText = "PED_ID";
            this.PED_ID.Name = "PED_ID";
            this.PED_ID.ReadOnly = true;
            this.PED_ID.Visible = false;
            // 
            // PED_QUANTITE_KG
            // 
            this.PED_QUANTITE_KG.DataPropertyName = "PED_QUANTITE_KG";
            this.PED_QUANTITE_KG.HeaderText = "PED_QUANTITE_KG";
            this.PED_QUANTITE_KG.Name = "PED_QUANTITE_KG";
            this.PED_QUANTITE_KG.ReadOnly = true;
            this.PED_QUANTITE_KG.Visible = false;
            // 
            // PEE_ID
            // 
            this.PEE_ID.DataPropertyName = "PEE_ID";
            this.PEE_ID.HeaderText = "PEE_ID";
            this.PEE_ID.Name = "PEE_ID";
            this.PEE_ID.ReadOnly = true;
            this.PEE_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PEE_DATE";
            this.dataGridViewTextBoxColumn1.HeaderText = "Date - heure";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 97;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "PED_QUANTITE_KG";
            this.dataGridViewTextBoxColumn2.HeaderText = "Quantité en kg";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PED_SONDAGE_OE";
            this.dataGridViewTextBoxColumn3.HeaderText = "Deg Oechslé";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 98;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ACQ_PAR_ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "Acquit";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 67;
            // 
            // lblWeighings
            // 
            this.lblWeighings.AutoSize = true;
            this.lblWeighings.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lblWeighings.Location = new System.Drawing.Point(1, 322);
            this.lblWeighings.Name = "lblWeighings";
            this.lblWeighings.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblWeighings.Size = new System.Drawing.Size(78, 15);
            this.lblWeighings.TabIndex = 20;
            this.lblWeighings.Text = "lblWeighings";
            // 
            // peE_PESEE_ENTETETableAdapter1
            // 
            this.peE_PESEE_ENTETETableAdapter1.ClearBeforeFill = true;
            // 
            // peD_PESEE_DETAILTableAdapter1
            // 
            this.peD_PESEE_DETAILTableAdapter1.ClearBeforeFill = true;
            // 
            // ShowR_ApportsButton
            // 
            this.ShowR_ApportsButton.AutoSize = true;
            this.ShowR_ApportsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ShowR_ApportsButton.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.ShowR_ApportsButton.Location = new System.Drawing.Point(650, 8);
            this.ShowR_ApportsButton.Margin = new System.Windows.Forms.Padding(2);
            this.ShowR_ApportsButton.Name = "ShowR_ApportsButton";
            this.ShowR_ApportsButton.Size = new System.Drawing.Size(105, 25);
            this.ShowR_ApportsButton.TabIndex = 21;
            this.ShowR_ApportsButton.Text = "Liste des apports";
            this.ShowR_ApportsButton.UseVisualStyleBackColor = false;
            // 
            // CtrlCockpit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.Controls.Add(this.ShowR_ApportsButton);
            this.Controls.Add(this.lblAcquits);
            this.Controls.Add(this.lblWeighings);
            this.Controls.Add(this.cockpitWeighingsDataGridView);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.cockpitAcquitsDataGridView);
            this.Controls.Add(this.pnlYear);
            this.Controls.Add(this.btnAddAcq);
            this.Controls.Add(this.cbxExploitant);
            this.Controls.Add(this.lblCockpit);
            this.Controls.Add(this.toolStrip1);
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "CtrlCockpit";
            this.Size = new System.Drawing.Size(1013, 855);
            this.Load += new System.EventHandler(this.CtrlCockpit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAR_PARCELLEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aNNANNEEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOMCOMPLETBindingSource)).EndInit();
            this.pnlYear.ResumeLayout(false);
            this.pnlYear.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitAcquitsBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitAcquitsDataGridView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitWeighingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cockpitWeighingsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DAO.DSPeseesTableAdapters.ANN_ANNEETableAdapter aNN_ANNEETableAdapter;
        private DAO.DSPeseesTableAdapters.PRO_NOMCOMPLETTableAdapter pRO_NOMCOMPLETTableAdapter;
        private DAO.DSPesees dSPesees;
        private DAO.DSPeseesTableAdapters.PAR_PARCELLETableAdapter pAR_PARCELLETableAdapter;
        private DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource pAR_PARCELLEBindingSource;
        private System.Windows.Forms.BindingSource aNNANNEEBindingSource;
        private System.Windows.Forms.BindingSource pRONOMCOMPLETBindingSource;
        private System.Windows.Forms.Panel pnlYear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbAnnee;
        private System.Windows.Forms.Label lblCockpit;
        private System.Windows.Forms.ComboBox cbxExploitant;
        private System.Windows.Forms.BindingSource cockpitAcquitsBindingSource;
        private DAO.DSPeseesTableAdapters.CockpitAcquitsTableAdapter cockpitAcquitsTableAdapter;
        private DAO.DSPeseesTableAdapters.ACQ_ACQUITTableAdapter acQ_ACQUITTableAdapter;
        private System.Windows.Forms.Button btnAddAcq;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolAdd;
        private System.Windows.Forms.ToolStripButton toolUpdate;
        private System.Windows.Forms.ToolStripButton toolDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSplitButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem acquitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesAcquitsDeVendangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesAcquitsDeVendangeParCépageToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolPrintProduction;
        private System.Windows.Forms.ToolStripButton toolVisit;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Label TotalLabel;
        private System.Windows.Forms.Label lblAcquits;
        private System.Windows.Forms.DataGridView cockpitAcquitsDataGridView;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label TotalSurfaceAreaLabel;
        private System.Windows.Forms.Label TotalLitersQuotaLabel;
        private System.Windows.Forms.Label TotalKilosDeliveredLabel;
        private System.Windows.Forms.BindingSource cockpitWeighingsBindingSource;
        private DAO.DSPeseesTableAdapters.CockpitWeighingsTableAdapter cockpitWeighingsTableAdapter;
        private System.Windows.Forms.DataGridView cockpitWeighingsDataGridView;
        private System.Windows.Forms.Label lblWeighings;
        private DAO.DSPeseesTableAdapters.PEE_PESEE_ENTETETableAdapter peE_PESEE_ENTETETableAdapter1;
        private DAO.DSPeseesTableAdapters.PED_PESEE_DETAILTableAdapter peD_PESEE_DETAILTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn droitProdLitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACQ_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn acquitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lieuProductionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn COM_NOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Couleur;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomLocalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn surfaceM2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cépageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn classeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn litrem2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kgLivréDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn difDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diff;
        private System.Windows.Forms.DataGridViewButtonColumn btnNewWeighing;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CEP_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_QUANTITE_KG;
        private System.Windows.Forms.DataGridViewTextBoxColumn PEE_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Button ShowR_ApportsButton;
    }
}
