﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesAutresMentions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cLA_NOMLabel;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.aUT_NOMTextBox = new System.Windows.Forms.TextBox();
            this.aUT_AUTRE_MENTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.aUT_IDTextBox = new System.Windows.Forms.TextBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnAddClass = new System.Windows.Forms.Button();
            this.btnSaveClass = new System.Windows.Forms.Button();
            this.aUT_AUTRE_MENTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.aUT_AUTRE_MENTIONDataGridView = new System.Windows.Forms.DataGridView();
            this.AUT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            cLA_NOMLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aUT_AUTRE_MENTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUT_AUTRE_MENTIONDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cLA_NOMLabel
            // 
            cLA_NOMLabel.AutoSize = true;
            cLA_NOMLabel.Location = new System.Drawing.Point(7, 41);
            cLA_NOMLabel.Name = "cLA_NOMLabel";
            cLA_NOMLabel.Size = new System.Drawing.Size(179, 13);
            cLA_NOMLabel.TabIndex = 15;
            cLA_NOMLabel.Text = "Nom local / Désignation cadastrale :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.aUT_NOMTextBox);
            this.groupBox1.Controls.Add(this.aUT_IDTextBox);
            this.groupBox1.Controls.Add(this.btnSupprimer);
            this.groupBox1.Controls.Add(this.btnAddClass);
            this.groupBox1.Controls.Add(cLA_NOMLabel);
            this.groupBox1.Controls.Add(this.btnSaveClass);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 118);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des mentions particulières";
            // 
            // aUT_NOMTextBox
            // 
            this.aUT_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aUT_AUTRE_MENTIONBindingSource, "AUT_NOM", true));
            this.aUT_NOMTextBox.Location = new System.Drawing.Point(207, 38);
            this.aUT_NOMTextBox.Name = "aUT_NOMTextBox";
            this.aUT_NOMTextBox.Size = new System.Drawing.Size(162, 20);
            this.aUT_NOMTextBox.TabIndex = 0;
            // 
            // aUT_AUTRE_MENTIONBindingSource
            // 
            this.aUT_AUTRE_MENTIONBindingSource.DataMember = "AUT_AUTRE_MENTION";
            this.aUT_AUTRE_MENTIONBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // aUT_IDTextBox
            // 
            this.aUT_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aUT_AUTRE_MENTIONBindingSource, "AUT_ID", true));
            this.aUT_IDTextBox.Location = new System.Drawing.Point(192, 12);
            this.aUT_IDTextBox.Name = "aUT_IDTextBox";
            this.aUT_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.aUT_IDTextBox.TabIndex = 16;
            this.aUT_IDTextBox.Text = "AUTID";
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSupprimer.Location = new System.Drawing.Point(294, 82);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 30);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = false;
            // 
            // btnAddClass
            // 
            this.btnAddClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddClass.Location = new System.Drawing.Point(132, 82);
            this.btnAddClass.Name = "btnAddClass";
            this.btnAddClass.Size = new System.Drawing.Size(75, 30);
            this.btnAddClass.TabIndex = 1;
            this.btnAddClass.Text = "Nouveau";
            this.btnAddClass.UseVisualStyleBackColor = false;
            // 
            // btnSaveClass
            // 
            this.btnSaveClass.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSaveClass.Location = new System.Drawing.Point(213, 82);
            this.btnSaveClass.Name = "btnSaveClass";
            this.btnSaveClass.Size = new System.Drawing.Size(75, 30);
            this.btnSaveClass.TabIndex = 2;
            this.btnSaveClass.Text = "Modifier";
            this.btnSaveClass.UseVisualStyleBackColor = false;
            // 
            // aUT_AUTRE_MENTIONTableAdapter
            // 
            this.aUT_AUTRE_MENTIONTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = this.aUT_AUTRE_MENTIONTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = null;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            // 
            // aUT_AUTRE_MENTIONDataGridView
            // 
            this.aUT_AUTRE_MENTIONDataGridView.AllowUserToAddRows = false;
            this.aUT_AUTRE_MENTIONDataGridView.AllowUserToDeleteRows = false;
            this.aUT_AUTRE_MENTIONDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aUT_AUTRE_MENTIONDataGridView.AutoGenerateColumns = false;
            this.aUT_AUTRE_MENTIONDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.aUT_AUTRE_MENTIONDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aUT_AUTRE_MENTIONDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AUT_ID,
            this.dataGridViewTextBoxColumn2});
            this.aUT_AUTRE_MENTIONDataGridView.DataSource = this.aUT_AUTRE_MENTIONBindingSource;
            this.aUT_AUTRE_MENTIONDataGridView.Location = new System.Drawing.Point(409, 12);
            this.aUT_AUTRE_MENTIONDataGridView.Name = "aUT_AUTRE_MENTIONDataGridView";
            this.aUT_AUTRE_MENTIONDataGridView.ReadOnly = true;
            this.aUT_AUTRE_MENTIONDataGridView.Size = new System.Drawing.Size(260, 291);
            this.aUT_AUTRE_MENTIONDataGridView.TabIndex = 21;
            // 
            // AUT_ID
            // 
            this.AUT_ID.DataPropertyName = "AUT_ID";
            this.AUT_ID.HeaderText = "AUT_ID";
            this.AUT_ID.Name = "AUT_ID";
            this.AUT_ID.ReadOnly = true;
            this.AUT_ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AUT_ID.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "AUT_NOM";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom local / Désignation cadastrale";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 200;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // WfrmGestionDesAutresMentions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 315);
            this.Controls.Add(this.aUT_AUTRE_MENTIONDataGridView);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(650, 220);
            this.Name = "WfrmGestionDesAutresMentions";
            this.Text = "Gestion des vendanges - Gestion des mentions particulières";
            this.Load += new System.EventHandler(this.WfrmGestionDesAutresMentions_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aUT_AUTRE_MENTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aUT_AUTRE_MENTIONDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnAddClass;
        private System.Windows.Forms.Button btnSaveClass;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource aUT_AUTRE_MENTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AUT_AUTRE_MENTIONTableAdapter aUT_AUTRE_MENTIONTableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView aUT_AUTRE_MENTIONDataGridView;
        private System.Windows.Forms.TextBox aUT_NOMTextBox;
        private System.Windows.Forms.TextBox aUT_IDTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn AUT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}