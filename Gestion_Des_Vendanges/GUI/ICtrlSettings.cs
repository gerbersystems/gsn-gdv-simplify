﻿using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    internal interface ICtrlSetting : System.ComponentModel.IComponent
    {
        bool CanDelete
        {
            get;
        }

        bool Validate();
        Form GetActiveForm();
    }
}