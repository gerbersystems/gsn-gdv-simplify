﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des regions
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesRegions : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesRegions _wfrm;

        public static WfrmGestionDesRegions Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesRegions();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int regId = (int)rEG_REGIONDataGridView.CurrentRow.Cells["REG_ID"].Value;
                    resultat = ((int)rEG_REGIONTableAdapter.NbRef(regId) == 0);
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        private WfrmGestionDesRegions()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesRegions_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(rEG_REGIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            rEG_REGIONTableAdapter.Fill(this.dSPesees.REG_REGION);
            _manager = new FormSettingsManagerPesees(this, rEG_IDTextBox, rEG_NAMETextBox, btnNew, btnUpdate, btnSupprimer, rEG_REGIONDataGridView, rEG_REGIONBindingSource, tableAdapterManager, dSPesees);
            _manager.AddControl(rEG_NAMETextBox);
        }
    }
}