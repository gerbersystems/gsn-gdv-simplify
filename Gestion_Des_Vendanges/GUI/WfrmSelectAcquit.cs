﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmSelectAcquit : WfrmBase
    {
        public readonly log4net.ILog Log;

        public int AcquitId
        {
            get
            {
                try
                {
                    return (int)this.aCQ_ACQUITDataGridView.CurrentRow.Cells[this.ACQ_ID.Name].Value;
                }
                catch
                {
                    return -1;
                }
            }
        }

        public WfrmSelectAcquit()
        {
            Log = log4net.LogManager.GetLogger(GetType());
            InitializeComponent();

            _CustomInitializeComponent();
        }

        private void _CustomInitializeComponent()
        {
            this.chkCepageIgnore.Checked = true;
            this.chkLieuIgnore.Checked = true;
            this.chkProducteurIgnore.Checked = true;
            this.chkAutreMentionIgnore.Checked = true;
            _UpdateFilterStatus();
        }

        private void _UpdateFilterStatus(object sender = null)
        {
            if (sender == null || (sender == this.chkProducteurIgnore && this.chkProducteurIgnore.Checked == true))
                this.chkCepageIgnore.Checked = true;

            if (sender == null || (sender == this.chkCepageIgnore && this.chkCepageIgnore.Checked == true))
                this.chkLieuIgnore.Checked = true;

            if (sender == null || (sender == this.chkLieuIgnore && this.chkLieuIgnore.Checked == true))
                this.chkAutreMentionIgnore.Checked = true;

            this.cbFiltreProducteur.Enabled = this.chkProducteurIgnore.Checked == false;

            this.chkCepageIgnore.Enabled = this.chkProducteurIgnore.Checked == false && cbFiltreProducteur.Items.Count > 0;
            this.cbCepage.Enabled = this.cbFiltreProducteur.Enabled && this.chkCepageIgnore.Checked == false;

            this.chkLieuIgnore.Enabled = this.chkCepageIgnore.Checked == false && cbCepage.Items.Count > 0;
            this.cbLieu.Enabled = this.cbCepage.Enabled && this.chkLieuIgnore.Checked == false;

            this.chkAutreMentionIgnore.Enabled = this.chkLieuIgnore.Checked == false && cbLieu.Items.Count > 0;
            this.cbAutreMention.Enabled = this.cbLieu.Enabled && this.chkAutreMentionIgnore.Checked == false;
        }

        private void WfrmSelectAcquit_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            ConnectionManager.Instance.AddGvTableAdapter(cLA_CLASSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(cOM_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_PROPRIETAIRETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(rEG_REGIONTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(prO_NOMCOMPLETTableAdapter1);

            ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIC_LIEU_COMMUNETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);

            ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);

            ConnectionManager.Instance.AddGvTableAdapter(peE_PESEE_ENTETETableAdapter1);
            peE_PESEE_ENTETETableAdapter1.Fill(this.dsPesees1.PEE_PESEE_ENTETE);

            pRO_NOMCOMPLETTableAdapter.Fill(this.dSPesees.PRO_NOMCOMPLET);
            rEG_REGIONTableAdapter.Fill(this.dSPesees.REG_REGION);
            pRO_PROPRIETAIRETableAdapter.Fill(this.dSPesees.PRO_PROPRIETAIRE);
            cOM_COMMUNETableAdapter.Fill(this.dSPesees.COM_COMMUNE);
            cOU_COULEURTableAdapter.Fill(this.dSPesees.COU_COULEUR);
            cLA_CLASSETableAdapter.Fill(this.dSPesees.CLA_CLASSE);
            aCQ_ACQUITTableAdapter.FillByAnnee(this.dSPesees.ACQ_ACQUIT, BUSINESS.Utilities.IdAnneeCourante);
            prO_NOMCOMPLETTableAdapter1.FillProducteurByAcquitAnn(this.dsPesees1.PRO_NOMCOMPLET, BUSINESS.Utilities.IdAnneeCourante);

            pAR_PARCELLETableAdapter.Fill(dsPesees1.PAR_PARCELLE);
            cOM_COMMUNETableAdapter.Fill(this.dsPesees1.COM_COMMUNE);
            lIC_LIEU_COMMUNETableAdapter.Fill(dsPesees1.LIC_LIEU_COMMUNE);

            lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dsPesees1.LIE_LIEU_DE_PRODUCTION);
            pAR_PARCELLETableAdapter.Fill(dsPesees1.PAR_PARCELLE);
            aUT_AUTRE_MENTIONTableAdapter.Fill(dsPesees1.AUT_AUTRE_MENTION);

            var dtc = new System.Data.DataTable[]{
				this.dSPesees.PRO_NOMCOMPLET,
				this.dSPesees.REG_REGION,
				this.dSPesees.PRO_PROPRIETAIRE,
				this.dSPesees.COM_COMMUNE,
				this.dSPesees.COU_COULEUR,
				this.dSPesees.CLA_CLASSE,
				this.dSPesees.ACQ_ACQUIT,
				this.dsPesees1.PRO_NOMCOMPLET,
				this.dsPesees1.PAR_PARCELLE,
				this.dsPesees1.COM_COMMUNE,
				this.dsPesees1.LIC_LIEU_COMMUNE,
				this.dsPesees1.LIE_LIEU_DE_PRODUCTION,
				this.dsPesees1.PAR_PARCELLE,
				this.dsPesees1.AUT_AUTRE_MENTION,
			};
            foreach (var dt in dtc)
            {
                try
                {
                    Log.DebugFormat("{0}.Rows.Count:{1}", dt.TableName, dt.Rows.Count);
                }
                catch (Exception ex)
                {
                    Log.Debug(string.Format("{0}.Rows.Count:{1}", dt.TableName, "Failled!!!"), ex);
                }
            }
            this.aCQ_ACQUITDataGridView.Columns[droitLitresTextBoxColumn.Name].Visible = GlobalParam.Instance.ApplicationUnite == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre;
            this.aCQ_ACQUITDataGridView.Columns[droitKilogrammesTextBoxColumn.Name].Visible = GlobalParam.Instance.ApplicationUnite == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Kilo;
        }

        private void btnSelectAcquit_Click(object sender, EventArgs e)
        {
            selectAcquit();
        }

        private void aCQ_ACQUITDataGridView_CellContentDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            selectAcquit();
        }

        private void selectAcquit()
        {
            DialogResult = DialogResult.OK;
        }

        private void chkProducteurIgnore_CheckedChanged(object sender, EventArgs e)
        {
            //cbFiltreProducteur.Enabled = !chkTous.Checked;
            _UpdateFilterStatus(sender: sender);
            updateProducteur();
        }

        private void chkAutreMentionIgnore_CheckedChanged(object sender, EventArgs e)
        {
            _UpdateFilterStatus(sender: sender);
            if (chkLieuIgnore.Checked == false)
            {
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                var cepageId = Convert.ToInt32(cbCepage.SelectedValue);
                var lieuId = Convert.ToInt32(cbLieu.SelectedValue);
                updateAutreMention(producteurId: producteurId, cepageId: cepageId, lieuId: lieuId);
                cbAutreMention_SelectionChangeCommitted(sender, e);
            }
        }

        private void chkLieuIgnore_CheckedChanged(object sender, EventArgs e)
        {
            _UpdateFilterStatus(sender: sender);
            if (chkLieuIgnore.Checked == false)
            {
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                var cepageId = Convert.ToInt32(cbCepage.SelectedValue);
                updateLieu(producteurId: producteurId, cepageId: cepageId);
                cbLieu_SelectionChangeCommitted(sender, e);
            }
        }

        private void chkCepageIgnore_CheckedChanged(object sender, EventArgs e)
        {
            _UpdateFilterStatus(sender: sender);
            if (chkCepageIgnore.Checked == false)
            {
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                updateCepage(producteurId: producteurId, annId: BUSINESS.Utilities.IdAnneeCourante);
                cbCepage_SelectionChangeCommitted(sender, e);
            }
        }

        private void cbFiltreProducteur_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateProducteur();
            _UpdateFilterStatus(sender: sender);
        }

        private void cbCepage_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbCepage.SelectedValue != null && chkCepageIgnore.Checked == false)
            {
                var cepageId = Convert.ToInt32(cbCepage.SelectedValue);
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);

                if (chkLieuIgnore.Checked == false)
                    updateLieu(producteurId: producteurId, cepageId: Convert.ToInt32(cepageId));
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                    aCQ_ACQUITTableAdapter.FillByAnnProVenCepage(dSPesees.ACQ_ACQUIT, ANN_ID: BUSINESS.Utilities.IdAnneeCourante, producteurId: producteurId, cepageId: cepageId);
            }
            _UpdateFilterStatus(sender: sender);
        }

        private void cbLieu_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbCepage.SelectedValue != null && cbLieu.SelectedValue != null && chkLieuIgnore.Checked == false)
            {
                var cepageId = Convert.ToInt32(cbCepage.SelectedValue);
                var lieuId = Convert.ToInt32(cbLieu.SelectedValue);
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                if (chkAutreMentionIgnore.Checked == false)
                    updateAutreMention(producteurId: producteurId, cepageId: cepageId, lieuId: lieuId);

                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                    aCQ_ACQUITTableAdapter.FillByAnnProVenCepageLieu(dSPesees.ACQ_ACQUIT, ANN_ID: BUSINESS.Utilities.IdAnneeCourante, producteurId: producteurId, cepageId: cepageId, lieuId: lieuId);
            }
            _UpdateFilterStatus(sender: sender);
        }

        private void cbAutreMention_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbLieu.SelectedValue != null && cbAutreMention.SelectedValue != null && chkAutreMentionIgnore.Checked == false)
            {
                var autreMentionId = Convert.ToInt32(cbAutreMention.SelectedValue);
                var cepageId = Convert.ToInt32(cbCepage.SelectedValue);
                var lieuId = Convert.ToInt32(cbLieu.SelectedValue);
                var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                    aCQ_ACQUITTableAdapter.FillByAnnProVenCepageLieuAutreMention(dSPesees.ACQ_ACQUIT, ANN_ID: BUSINESS.Utilities.IdAnneeCourante, producteurId: producteurId, cepageId: cepageId, lieuId: lieuId, autreMentionId: autreMentionId);
            }
            _UpdateFilterStatus(sender: sender);
        }

        private void updateProducteur()
        {
            if (chkProducteurIgnore.Checked)
            {
                aCQ_ACQUITTableAdapter.FillByAnnee(dSPesees.ACQ_ACQUIT, BUSINESS.Utilities.IdAnneeCourante);
            }
            else
            {
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var producteurId = Convert.ToInt32(cbFiltreProducteur.SelectedValue);
                    aCQ_ACQUITTableAdapter.FillByAnnProVen(dSPesees.ACQ_ACQUIT, BUSINESS.Utilities.IdAnneeCourante, producteurId);
                    if (chkCepageIgnore.Checked != true)
                        updateCepage(producteurId: producteurId, annId: BUSINESS.Utilities.IdAnneeCourante);
                }
            }
        }

        private void updateCepage(int producteurId, int annId)
        {
            try
            {
                cEP_CEPAGETableAdapter.FillByProducteurAnnee(dsPesees1.CEP_CEPAGE, ANN_ID: annId, PRO_ID: producteurId);
                aCQ_ACQUITTableAdapter.FillByAnnProVen(dsPesees1.ACQ_ACQUIT, ANN_ID: annId, PRO_ID_VENDANGE: producteurId);

                Program.Log.InfoFormat("dsPesees1.ACQ_ACQUIT.Count:{0}", dsPesees1.ACQ_ACQUIT.Count);
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var q = (
                            from cep in dsPesees1.CEP_CEPAGE
                            from acq in dsPesees1.ACQ_ACQUIT
                            from par in dsPesees1.PAR_PARCELLE
                            where
                                cep.COU_ID == acq.COU_ID &&
                                acq.ANN_ID == annId &&
                                acq.PRO_ID_VENDANGE == producteurId
                            orderby cep.CEP_NOM, cep.CEP_NUMERO
                            select new { ValueMember = cep.CEP_ID, DisplayMember = cep.CEP_NOM, CepageId = cep.CEP_ID }
                        ).Distinct();
                    cbCepage.Text = string.Empty;
                    cbCepage.DataSource = q.ToList();
                    cbCepage.DisplayMember = "DisplayMember";
                    cbCepage.ValueMember = "ValueMember";

                    if (cbCepage.SelectedValue != null && chkLieuIgnore.Checked != true)
                    {
                        updateLieu(producteurId, Convert.ToInt32(cbCepage.SelectedValue));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug("updateCepa", ex);
            }
        }

        private void updateLieu(int producteurId, int cepageId)
        {
            try
            {
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var q = (
                            from acq in dsPesees1.ACQ_ACQUIT
                            from par in dsPesees1.PAR_PARCELLE
                            from lie in dsPesees1.LIE_LIEU_DE_PRODUCTION
                            where
                                lie.LIE_ID == par.LIE_ID &&
                                par.ACQ_ID == acq.ACQ_ID &&
                                par.CEP_ID == cepageId &&
                                acq.PRO_ID_VENDANGE == producteurId
                            select new { ValueMember = par.LIE_ID, DisplayMember = string.Concat(lie.LIE_NOM), LieuId = lie.LIE_ID, CepageId = cepageId }
                        ).Distinct();
                    cbLieu.Text = string.Empty;
                    cbLieu.DataSource = q.ToList();
                    cbLieu.DisplayMember = "DisplayMember";
                    cbLieu.ValueMember = "ValueMember";

                    if (cbLieu.SelectedValue != null && chkAutreMentionIgnore.Checked != true)
                    {
                        var lieuId = Convert.ToInt32(cbLieu.SelectedValue);
                        updateAutreMention(producteurId: producteurId, cepageId: cepageId, lieuId: lieuId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug("updateLieu", ex);
            }
        }

        private void updateAutreMention(int producteurId, int cepageId, int lieuId)
        {
            try
            {
                using (var w = new GSN.GDV.GUI.Common.WorkingGlass(this))
                {
                    var q = (
                            from acq in dsPesees1.ACQ_ACQUIT
                            from par in dsPesees1.PAR_PARCELLE
                            from aut in dsPesees1.AUT_AUTRE_MENTION
                            where
                                aut.AUT_ID == par.AUT_ID &&
                                par.LIE_ID == lieuId &&
                                par.CEP_ID == cepageId &&
                                par.ACQ_ID == acq.ACQ_ID &&
                                acq.PRO_ID_VENDANGE == producteurId
                            select new { ValueMember = par.AUT_ID, DisplayMember = aut.AUT_NOM, AutreMentionId = par.AUT_ID, LieuId = lieuId, CepageId = cepageId }
                        ).Distinct();
                    cbAutreMention.Text = string.Empty;
                    cbAutreMention.DataSource = q.ToList();
                    cbAutreMention.DisplayMember = "DisplayMember";
                    cbAutreMention.ValueMember = "ValueMember";
                }
            }
            catch (Exception ex)
            {
                Log.Debug("updateAutreMention", ex);
            }
        }
    }
}