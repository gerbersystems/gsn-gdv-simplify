﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesProprietaires
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label aDR_IDLabel;
            System.Windows.Forms.Label pRO_NOMLabel;
            System.Windows.Forms.Label pRO_PRENOMLabel;
            System.Windows.Forms.Label pRO_TITRELabel;
            System.Windows.Forms.Label pRO_SCTELabel;
            System.Windows.Forms.Label pRO_NUMERO_TVALabel;
            System.Windows.Forms.Label ErpIdLabel;
            this.mOC_IDLabel = new System.Windows.Forms.Label();
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.pRO_PROPRIETAIREBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRO_PROPRIETAIRETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter();
            this.tableAdapterManager = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager();
            this.pRO_PROPRIETAIREDataGridView = new System.Windows.Forms.DataGridView();
            this.PRO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_TITRE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRO_SCTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADR_ID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.adresseCompletBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PRO_IS_PROPRIETAIRE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PRO_IS_PRODUCTEUR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ADR_ID_WINBIZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aDRADRESSEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aDR_ADRESSETableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter();
            this.pRO_IDTextBox = new System.Windows.Forms.TextBox();
            this.aDR_IDComboBox = new System.Windows.Forms.ComboBox();
            this.AdressescontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AdressestoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pRO_NOMTextBox = new System.Windows.Forms.TextBox();
            this.pRO_PRENOMTextBox = new System.Windows.Forms.TextBox();
            this.btnAddProprio = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.pRO_TITRETextBox = new System.Windows.Forms.TextBox();
            this.pRO_SCTETextBox = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mOD_IDComboBox = new System.Windows.Forms.ComboBox();
            this.mODMODETVABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRO_NUMERO_TVATextBox = new System.Windows.Forms.TextBox();
            this.chkProducteur = new System.Windows.Forms.CheckBox();
            this.chkProprietaire = new System.Windows.Forms.CheckBox();
            this.lgenre = new System.Windows.Forms.Label();
            this.ErpIdTextBox = new System.Windows.Forms.TextBox();
            this.btnImportWinBiz = new System.Windows.Forms.Button();
            this.adresseCompletTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter();
            this.mOD_MODE_TVATableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOD_MODE_TVATableAdapter();
            aDR_IDLabel = new System.Windows.Forms.Label();
            pRO_NOMLabel = new System.Windows.Forms.Label();
            pRO_PRENOMLabel = new System.Windows.Forms.Label();
            pRO_TITRELabel = new System.Windows.Forms.Label();
            pRO_SCTELabel = new System.Windows.Forms.Label();
            pRO_NUMERO_TVALabel = new System.Windows.Forms.Label();
            ErpIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).BeginInit();
            this.AdressescontextMenuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mODMODETVABindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // aDR_IDLabel
            // 
            aDR_IDLabel.AutoSize = true;
            aDR_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            aDR_IDLabel.Location = new System.Drawing.Point(4, 140);
            aDR_IDLabel.Name = "aDR_IDLabel";
            aDR_IDLabel.Size = new System.Drawing.Size(60, 15);
            aDR_IDLabel.TabIndex = 21;
            aDR_IDLabel.Text = "Adresse : ";
            // 
            // pRO_NOMLabel
            // 
            pRO_NOMLabel.AutoSize = true;
            pRO_NOMLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_NOMLabel.Location = new System.Drawing.Point(6, 55);
            pRO_NOMLabel.Name = "pRO_NOMLabel";
            pRO_NOMLabel.Size = new System.Drawing.Size(43, 15);
            pRO_NOMLabel.TabIndex = 23;
            pRO_NOMLabel.Text = "Nom : ";
            // 
            // pRO_PRENOMLabel
            // 
            pRO_PRENOMLabel.AutoSize = true;
            pRO_PRENOMLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_PRENOMLabel.Location = new System.Drawing.Point(6, 83);
            pRO_PRENOMLabel.Name = "pRO_PRENOMLabel";
            pRO_PRENOMLabel.Size = new System.Drawing.Size(60, 15);
            pRO_PRENOMLabel.TabIndex = 25;
            pRO_PRENOMLabel.Text = "Prénom : ";
            // 
            // pRO_TITRELabel
            // 
            pRO_TITRELabel.AutoSize = true;
            pRO_TITRELabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_TITRELabel.Location = new System.Drawing.Point(6, 29);
            pRO_TITRELabel.Name = "pRO_TITRELabel";
            pRO_TITRELabel.Size = new System.Drawing.Size(43, 15);
            pRO_TITRELabel.TabIndex = 27;
            pRO_TITRELabel.Text = "Titre : ";
            // 
            // pRO_SCTELabel
            // 
            pRO_SCTELabel.AutoSize = true;
            pRO_SCTELabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_SCTELabel.Location = new System.Drawing.Point(6, 111);
            pRO_SCTELabel.Name = "pRO_SCTELabel";
            pRO_SCTELabel.Size = new System.Drawing.Size(58, 15);
            pRO_SCTELabel.TabIndex = 28;
            pRO_SCTELabel.Text = "Société : ";
            // 
            // pRO_NUMERO_TVALabel
            // 
            pRO_NUMERO_TVALabel.AutoSize = true;
            pRO_NUMERO_TVALabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            pRO_NUMERO_TVALabel.Location = new System.Drawing.Point(542, 58);
            pRO_NUMERO_TVALabel.Name = "pRO_NUMERO_TVALabel";
            pRO_NUMERO_TVALabel.Size = new System.Drawing.Size(101, 15);
            pRO_NUMERO_TVALabel.TabIndex = 29;
            pRO_NUMERO_TVALabel.Text = "Numéro de TVA :";
            // 
            // ErpIdLabel
            // 
            ErpIdLabel.AutoSize = true;
            ErpIdLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            ErpIdLabel.Location = new System.Drawing.Point(542, 116);
            ErpIdLabel.Name = "ErpIdLabel";
            ErpIdLabel.Size = new System.Drawing.Size(45, 15);
            ErpIdLabel.TabIndex = 32;
            ErpIdLabel.Text = "Erp Id :";
            // 
            // mOC_IDLabel
            // 
            this.mOC_IDLabel.AutoSize = true;
            this.mOC_IDLabel.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.mOC_IDLabel.Location = new System.Drawing.Point(542, 86);
            this.mOC_IDLabel.Name = "mOC_IDLabel";
            this.mOC_IDLabel.Size = new System.Drawing.Size(70, 15);
            this.mOC_IDLabel.TabIndex = 30;
            this.mOC_IDLabel.Text = "Mode TVA :";
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.Locale = new System.Globalization.CultureInfo("fr-CH");
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRO_PROPRIETAIREBindingSource
            // 
            this.pRO_PROPRIETAIREBindingSource.DataMember = "PRO_PROPRIETAIRE";
            this.pRO_PROPRIETAIREBindingSource.DataSource = this.dSPesees;
            // 
            // pRO_PROPRIETAIRETableAdapter
            // 
            this.pRO_PROPRIETAIRETableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ACQ_ACQUITTableAdapter = null;
            this.tableAdapterManager.ADR_ADRESSETableAdapter = null;
            this.tableAdapterManager.AdresseCompletTableAdapter = null;
            this.tableAdapterManager.ANN_ANNEETableAdapter = null;
            this.tableAdapterManager.ART_ARTICLETableAdapter = null;
            this.tableAdapterManager.ASS_ASSOCIATION_ARTICLETableAdapter = null;
            this.tableAdapterManager.AUT_AUTRE_MENTIONTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BON_BONUSTableAdapter = null;
            this.tableAdapterManager.CEP_CEPAGETableAdapter = null;
            this.tableAdapterManager.CLA_CLASSETableAdapter = null;
            this.tableAdapterManager.COA_COMMUNE_ADRESSETableAdapter = null;
            this.tableAdapterManager.COM_COMMUNETableAdapter = null;
            this.tableAdapterManager.COU_COULEURTableAdapter = null;
            this.tableAdapterManager.DEG_DEGRE_OECHSLE_BRIXTableAdapter = null;
            this.tableAdapterManager.DIS_DISTRICTTableAdapter = null;
            this.tableAdapterManager.HAS_HASHTableAdapter = null;
            this.tableAdapterManager.LIC_LIEU_COMMUNETableAdapter = null;
            this.tableAdapterManager.LIE_LIEU_DE_PRODUCTIONTableAdapter = null;
            this.tableAdapterManager.LIM_LIGNE_PAIEMENT_MANUELLETableAdapter = null;
            this.tableAdapterManager.LIP_LIGNE_PAIEMENT_PESEETableAdapter = null;
            this.tableAdapterManager.MCE_MOC_CEPTableAdapter = null;
            this.tableAdapterManager.MCO_MOC_COUTableAdapter = null;
            this.tableAdapterManager.MOC_MODE_COMPTABILISATIONTableAdapter = null;
            this.tableAdapterManager.MOD_MODE_TVATableAdapter = null;
            this.tableAdapterManager.PAI_PAIEMENTTableAdapter = null;
            this.tableAdapterManager.PAM_PARAMETRESTableAdapter = null;
            this.tableAdapterManager.PAR_PARCELLETableAdapter = null;
            this.tableAdapterManager.PCO_PAR_COMTableAdapter = null;
            this.tableAdapterManager.PEC_PED_COMTableAdapter = null;
            this.tableAdapterManager.PED_PESEE_DETAILTableAdapter = null;
            this.tableAdapterManager.PEE_PESEE_ENTETETableAdapter = null;
            this.tableAdapterManager.PEL_PED_LIETableAdapter = null;
            this.tableAdapterManager.PLI_PAR_LIETableAdapter = null;
            this.tableAdapterManager.PRE_PRESSOIRTableAdapter = null;
            this.tableAdapterManager.PRO_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.PRO_PROPRIETAIRETableAdapter = this.pRO_PROPRIETAIRETableAdapter;
            this.tableAdapterManager.REG_REGIONTableAdapter = null;
            this.tableAdapterManager.REP_REPORTTableAdapter = null;
            this.tableAdapterManager.RES_NOMCOMPLETTableAdapter = null;
            this.tableAdapterManager.RES_RESPONSABLETableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VAL_VALEUR_CEPAGETableAdapter = null;
            this.tableAdapterManager.ZOT_ZONE_TRAVAILTableAdapter = null;
            // 
            // pRO_PROPRIETAIREDataGridView
            // 
            this.pRO_PROPRIETAIREDataGridView.AllowUserToAddRows = false;
            this.pRO_PROPRIETAIREDataGridView.AllowUserToDeleteRows = false;
            this.pRO_PROPRIETAIREDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pRO_PROPRIETAIREDataGridView.AutoGenerateColumns = false;
            this.pRO_PROPRIETAIREDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.pRO_PROPRIETAIREDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pRO_PROPRIETAIREDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PRO_ID,
            this.PRO_TITRE,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.PRO_SCTE,
            this.ADR_ID,
            this.PRO_IS_PROPRIETAIRE,
            this.PRO_IS_PRODUCTEUR,
            this.ADR_ID_WINBIZ});
            this.pRO_PROPRIETAIREDataGridView.DataSource = this.pRO_PROPRIETAIREBindingSource;
            this.pRO_PROPRIETAIREDataGridView.Location = new System.Drawing.Point(12, 190);
            this.pRO_PROPRIETAIREDataGridView.MinimumSize = new System.Drawing.Size(834, 300);
            this.pRO_PROPRIETAIREDataGridView.Name = "pRO_PROPRIETAIREDataGridView";
            this.pRO_PROPRIETAIREDataGridView.RowTemplate.Height = 24;
            this.pRO_PROPRIETAIREDataGridView.Size = new System.Drawing.Size(1140, 479);
            this.pRO_PROPRIETAIREDataGridView.TabIndex = 18;
            this.pRO_PROPRIETAIREDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.pRO_PROPRIETAIREDataGridView_ColumnHeaderMouseClick);
            // 
            // PRO_ID
            // 
            this.PRO_ID.DataPropertyName = "PRO_ID";
            this.PRO_ID.HeaderText = "Id";
            this.PRO_ID.Name = "PRO_ID";
            this.PRO_ID.ReadOnly = true;
            this.PRO_ID.Visible = false;
            this.PRO_ID.Width = 70;
            // 
            // PRO_TITRE
            // 
            this.PRO_TITRE.DataPropertyName = "PRO_TITRE";
            this.PRO_TITRE.HeaderText = "Titre";
            this.PRO_TITRE.Name = "PRO_TITRE";
            this.PRO_TITRE.ReadOnly = true;
            this.PRO_TITRE.Visible = false;
            this.PRO_TITRE.Width = 110;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PRO_NOM";
            this.dataGridViewTextBoxColumn3.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PRO_PRENOM";
            this.dataGridViewTextBoxColumn4.HeaderText = "Prénom";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // PRO_SCTE
            // 
            this.PRO_SCTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PRO_SCTE.DataPropertyName = "PRO_SCTE";
            this.PRO_SCTE.FillWeight = 300F;
            this.PRO_SCTE.HeaderText = "Société";
            this.PRO_SCTE.MinimumWidth = 100;
            this.PRO_SCTE.Name = "PRO_SCTE";
            this.PRO_SCTE.ReadOnly = true;
            // 
            // ADR_ID
            // 
            this.ADR_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ADR_ID.DataPropertyName = "ADR_ID";
            this.ADR_ID.DataSource = this.adresseCompletBindingSource;
            this.ADR_ID.DisplayMember = "ADR_COMPLET";
            this.ADR_ID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.ADR_ID.HeaderText = "Adresse";
            this.ADR_ID.MinimumWidth = 100;
            this.ADR_ID.Name = "ADR_ID";
            this.ADR_ID.ReadOnly = true;
            this.ADR_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ADR_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.ADR_ID.ValueMember = "ADR_ID";
            // 
            // adresseCompletBindingSource
            // 
            this.adresseCompletBindingSource.DataMember = "AdresseComplet";
            this.adresseCompletBindingSource.DataSource = this.dSPesees;
            // 
            // PRO_IS_PROPRIETAIRE
            // 
            this.PRO_IS_PROPRIETAIRE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PRO_IS_PROPRIETAIRE.DataPropertyName = "PRO_IS_PROPRIETAIRE";
            this.PRO_IS_PROPRIETAIRE.HeaderText = "Propriétaire";
            this.PRO_IS_PROPRIETAIRE.Name = "PRO_IS_PROPRIETAIRE";
            this.PRO_IS_PROPRIETAIRE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PRO_IS_PROPRIETAIRE.Width = 93;
            // 
            // PRO_IS_PRODUCTEUR
            // 
            this.PRO_IS_PRODUCTEUR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.PRO_IS_PRODUCTEUR.DataPropertyName = "PRO_IS_PRODUCTEUR";
            this.PRO_IS_PRODUCTEUR.HeaderText = "Producteur";
            this.PRO_IS_PRODUCTEUR.Name = "PRO_IS_PRODUCTEUR";
            this.PRO_IS_PRODUCTEUR.ReadOnly = true;
            this.PRO_IS_PRODUCTEUR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PRO_IS_PRODUCTEUR.Width = 91;
            // 
            // ADR_ID_WINBIZ
            // 
            this.ADR_ID_WINBIZ.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ADR_ID_WINBIZ.DataPropertyName = "ADR_ID_WINBIZ";
            this.ADR_ID_WINBIZ.HeaderText = "N° ERP";
            this.ADR_ID_WINBIZ.Name = "ADR_ID_WINBIZ";
            this.ADR_ID_WINBIZ.ReadOnly = true;
            this.ADR_ID_WINBIZ.Width = 69;
            // 
            // aDRADRESSEBindingSource
            // 
            this.aDRADRESSEBindingSource.DataMember = "ADR_ADRESSE";
            this.aDRADRESSEBindingSource.DataSource = this.dSPesees;
            // 
            // aDR_ADRESSETableAdapter
            // 
            this.aDR_ADRESSETableAdapter.ClearBeforeFill = true;
            // 
            // pRO_IDTextBox
            // 
            this.pRO_IDTextBox.BackColor = System.Drawing.Color.Lavender;
            this.pRO_IDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_ID", true));
            this.pRO_IDTextBox.Enabled = false;
            this.pRO_IDTextBox.Location = new System.Drawing.Point(474, -12);
            this.pRO_IDTextBox.Name = "pRO_IDTextBox";
            this.pRO_IDTextBox.Size = new System.Drawing.Size(132, 23);
            this.pRO_IDTextBox.TabIndex = 20;
            // 
            // aDR_IDComboBox
            // 
            this.aDR_IDComboBox.ContextMenuStrip = this.AdressescontextMenuStrip;
            this.aDR_IDComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRO_PROPRIETAIREBindingSource, "ADR_ID", true));
            this.aDR_IDComboBox.DataSource = this.adresseCompletBindingSource;
            this.aDR_IDComboBox.DisplayMember = "ADR_COMPLET";
            this.aDR_IDComboBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.aDR_IDComboBox.FormattingEnabled = true;
            this.aDR_IDComboBox.Location = new System.Drawing.Point(106, 137);
            this.aDR_IDComboBox.Name = "aDR_IDComboBox";
            this.aDR_IDComboBox.Size = new System.Drawing.Size(400, 23);
            this.aDR_IDComboBox.TabIndex = 4;
            this.aDR_IDComboBox.ValueMember = "ADR_ID";
            this.aDR_IDComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aDR_IDComboBox_KeyPress);
            // 
            // AdressescontextMenuStrip
            // 
            this.AdressescontextMenuStrip.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.AdressescontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdressestoolStripMenuItem});
            this.AdressescontextMenuStrip.Name = "AdressescontextMenuStrip";
            this.AdressescontextMenuStrip.Size = new System.Drawing.Size(195, 26);
            // 
            // AdressestoolStripMenuItem
            // 
            this.AdressestoolStripMenuItem.Name = "AdressestoolStripMenuItem";
            this.AdressestoolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.AdressestoolStripMenuItem.Text = "Accéder aux adresses...";
            this.AdressestoolStripMenuItem.Click += new System.EventHandler(this.AdressestoolStripMenuItem_Click);
            // 
            // pRO_NOMTextBox
            // 
            this.pRO_NOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_NOM", true));
            this.pRO_NOMTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.pRO_NOMTextBox.Location = new System.Drawing.Point(106, 52);
            this.pRO_NOMTextBox.Name = "pRO_NOMTextBox";
            this.pRO_NOMTextBox.Size = new System.Drawing.Size(400, 23);
            this.pRO_NOMTextBox.TabIndex = 1;
            this.pRO_NOMTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aDR_IDComboBox_KeyPress);
            // 
            // pRO_PRENOMTextBox
            // 
            this.pRO_PRENOMTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_PRENOM", true));
            this.pRO_PRENOMTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.pRO_PRENOMTextBox.Location = new System.Drawing.Point(106, 80);
            this.pRO_PRENOMTextBox.Name = "pRO_PRENOMTextBox";
            this.pRO_PRENOMTextBox.Size = new System.Drawing.Size(400, 23);
            this.pRO_PRENOMTextBox.TabIndex = 2;
            // 
            // btnAddProprio
            // 
            this.btnAddProprio.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAddProprio.Location = new System.Drawing.Point(812, 136);
            this.btnAddProprio.Name = "btnAddProprio";
            this.btnAddProprio.Size = new System.Drawing.Size(75, 30);
            this.btnAddProprio.TabIndex = 9;
            this.btnAddProprio.Text = "Nouveau";
            this.btnAddProprio.UseVisualStyleBackColor = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnUpdate.Location = new System.Drawing.Point(893, 136);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 30);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Modifier";
            this.btnUpdate.UseVisualStyleBackColor = false;
            // 
            // pRO_TITRETextBox
            // 
            this.pRO_TITRETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_TITRE", true));
            this.pRO_TITRETextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.pRO_TITRETextBox.Location = new System.Drawing.Point(106, 26);
            this.pRO_TITRETextBox.Name = "pRO_TITRETextBox";
            this.pRO_TITRETextBox.Size = new System.Drawing.Size(400, 23);
            this.pRO_TITRETextBox.TabIndex = 0;
            // 
            // pRO_SCTETextBox
            // 
            this.pRO_SCTETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_SCTE", true));
            this.pRO_SCTETextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.pRO_SCTETextBox.Location = new System.Drawing.Point(106, 108);
            this.pRO_SCTETextBox.Name = "pRO_SCTETextBox";
            this.pRO_SCTETextBox.Size = new System.Drawing.Size(400, 23);
            this.pRO_SCTETextBox.TabIndex = 3;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnDelete.Location = new System.Drawing.Point(974, 136);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "Supprimer";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mOC_IDLabel);
            this.groupBox1.Controls.Add(this.mOD_IDComboBox);
            this.groupBox1.Controls.Add(pRO_NUMERO_TVALabel);
            this.groupBox1.Controls.Add(this.pRO_NUMERO_TVATextBox);
            this.groupBox1.Controls.Add(this.chkProducteur);
            this.groupBox1.Controls.Add(this.chkProprietaire);
            this.groupBox1.Controls.Add(this.lgenre);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.pRO_IDTextBox);
            this.groupBox1.Controls.Add(pRO_SCTELabel);
            this.groupBox1.Controls.Add(this.pRO_SCTETextBox);
            this.groupBox1.Controls.Add(pRO_TITRELabel);
            this.groupBox1.Controls.Add(this.pRO_TITRETextBox);
            this.groupBox1.Controls.Add(aDR_IDLabel);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.aDR_IDComboBox);
            this.groupBox1.Controls.Add(this.btnAddProprio);
            this.groupBox1.Controls.Add(pRO_NOMLabel);
            this.groupBox1.Controls.Add(this.pRO_NOMTextBox);
            this.groupBox1.Controls.Add(pRO_PRENOMLabel);
            this.groupBox1.Controls.Add(this.pRO_PRENOMTextBox);
            this.groupBox1.Controls.Add(ErpIdLabel);
            this.groupBox1.Controls.Add(this.ErpIdTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1055, 172);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisie des propriétaires";
            // 
            // mOD_IDComboBox
            // 
            this.mOD_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.mOD_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.mOD_IDComboBox.DataSource = this.mODMODETVABindingSource;
            this.mOD_IDComboBox.DisplayMember = "MOD_NOM";
            this.mOD_IDComboBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.mOD_IDComboBox.Location = new System.Drawing.Point(683, 83);
            this.mOD_IDComboBox.Name = "mOD_IDComboBox";
            this.mOD_IDComboBox.Size = new System.Drawing.Size(204, 23);
            this.mOD_IDComboBox.TabIndex = 8;
            this.mOD_IDComboBox.ValueMember = "MOD_ID";
            // 
            // mODMODETVABindingSource
            // 
            this.mODMODETVABindingSource.DataMember = "MOD_MODE_TVA";
            this.mODMODETVABindingSource.DataSource = this.dSPesees;
            // 
            // pRO_NUMERO_TVATextBox
            // 
            this.pRO_NUMERO_TVATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "PRO_NUMERO_TVA", true));
            this.pRO_NUMERO_TVATextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.pRO_NUMERO_TVATextBox.HideSelection = false;
            this.pRO_NUMERO_TVATextBox.Location = new System.Drawing.Point(683, 54);
            this.pRO_NUMERO_TVATextBox.Name = "pRO_NUMERO_TVATextBox";
            this.pRO_NUMERO_TVATextBox.Size = new System.Drawing.Size(204, 23);
            this.pRO_NUMERO_TVATextBox.TabIndex = 7;
            // 
            // chkProducteur
            // 
            this.chkProducteur.AutoSize = true;
            this.chkProducteur.Checked = true;
            this.chkProducteur.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProducteur.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.pRO_PROPRIETAIREBindingSource, "PRO_IS_PRODUCTEUR", true));
            this.chkProducteur.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.chkProducteur.Location = new System.Drawing.Point(792, 31);
            this.chkProducteur.Name = "chkProducteur";
            this.chkProducteur.Size = new System.Drawing.Size(89, 19);
            this.chkProducteur.TabIndex = 6;
            this.chkProducteur.Text = "Producteur";
            this.chkProducteur.UseVisualStyleBackColor = true;
            // 
            // chkProprietaire
            // 
            this.chkProprietaire.AutoSize = true;
            this.chkProprietaire.Checked = true;
            this.chkProprietaire.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProprietaire.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.pRO_PROPRIETAIREBindingSource, "PRO_IS_PROPRIETAIRE", true));
            this.chkProprietaire.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.chkProprietaire.Location = new System.Drawing.Point(682, 31);
            this.chkProprietaire.Name = "chkProprietaire";
            this.chkProprietaire.Size = new System.Drawing.Size(93, 19);
            this.chkProprietaire.TabIndex = 5;
            this.chkProprietaire.Text = "Propriétaire";
            this.chkProprietaire.UseVisualStyleBackColor = true;
            // 
            // lgenre
            // 
            this.lgenre.AutoSize = true;
            this.lgenre.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lgenre.Location = new System.Drawing.Point(542, 32);
            this.lgenre.Name = "lgenre";
            this.lgenre.Size = new System.Drawing.Size(48, 15);
            this.lgenre.TabIndex = 29;
            this.lgenre.Text = "Genre :";
            // 
            // ErpIdTextBox
            // 
            this.ErpIdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRO_PROPRIETAIREBindingSource, "ADR_ID_WINBIZ", true));
            this.ErpIdTextBox.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.ErpIdTextBox.HideSelection = false;
            this.ErpIdTextBox.Location = new System.Drawing.Point(683, 112);
            this.ErpIdTextBox.Name = "ErpIdTextBox";
            this.ErpIdTextBox.Size = new System.Drawing.Size(125, 23);
            this.ErpIdTextBox.TabIndex = 9;
            // 
            // btnImportWinBiz
            // 
            this.btnImportWinBiz.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnImportWinBiz.Location = new System.Drawing.Point(1077, 149);
            this.btnImportWinBiz.Name = "btnImportWinBiz";
            this.btnImportWinBiz.Size = new System.Drawing.Size(75, 30);
            this.btnImportWinBiz.TabIndex = 10;
            this.btnImportWinBiz.Text = "Importer";
            this.btnImportWinBiz.UseVisualStyleBackColor = false;
            this.btnImportWinBiz.Click += new System.EventHandler(this.btnImportWinBiz_Click);
            // 
            // adresseCompletTableAdapter
            // 
            this.adresseCompletTableAdapter.ClearBeforeFill = true;
            // 
            // mOD_MODE_TVATableAdapter
            // 
            this.mOD_MODE_TVATableAdapter.ClearBeforeFill = true;
            // 
            // WfrmGestionDesProprietaires
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.ClientSize = new System.Drawing.Size(1164, 681);
            this.Controls.Add(this.pRO_PROPRIETAIREDataGridView);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnImportWinBiz);
            this.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.MinimumSize = new System.Drawing.Size(1180, 720);
            this.Name = "WfrmGestionDesProprietaires";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion des propriétaires";
            this.Load += new System.EventHandler(this.WfrmGestionDesProprietaires_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRO_PROPRIETAIREDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adresseCompletBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDRADRESSEBindingSource)).EndInit();
            this.AdressescontextMenuStrip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mODMODETVABindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource pRO_PROPRIETAIREBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.PRO_PROPRIETAIRETableAdapter pRO_PROPRIETAIRETableAdapter;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView pRO_PROPRIETAIREDataGridView;
        private System.Windows.Forms.BindingSource aDRADRESSEBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.ADR_ADRESSETableAdapter aDR_ADRESSETableAdapter;
        private System.Windows.Forms.TextBox pRO_IDTextBox;
        private System.Windows.Forms.ComboBox aDR_IDComboBox;
        private System.Windows.Forms.TextBox pRO_NOMTextBox;
        private System.Windows.Forms.TextBox pRO_PRENOMTextBox;
        private System.Windows.Forms.Button btnAddProprio;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox pRO_TITRETextBox;
        private System.Windows.Forms.TextBox pRO_SCTETextBox;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ContextMenuStrip AdressescontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem AdressestoolStripMenuItem;
        private System.Windows.Forms.BindingSource adresseCompletBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.AdresseCompletTableAdapter adresseCompletTableAdapter;
        private System.Windows.Forms.Button btnImportWinBiz;
        private System.Windows.Forms.CheckBox chkProducteur;
        private System.Windows.Forms.CheckBox chkProprietaire;
        private System.Windows.Forms.Label lgenre;
        private System.Windows.Forms.TextBox pRO_NUMERO_TVATextBox;
        private System.Windows.Forms.ComboBox mOD_IDComboBox;
        private System.Windows.Forms.Label mOC_IDLabel;
        private System.Windows.Forms.BindingSource mODMODETVABindingSource;
        private DAO.DSPeseesTableAdapters.MOD_MODE_TVATableAdapter mOD_MODE_TVATableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_TITRE;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRO_SCTE;
        private System.Windows.Forms.DataGridViewComboBoxColumn ADR_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PRO_IS_PROPRIETAIRE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PRO_IS_PRODUCTEUR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADR_ID_WINBIZ;
        private System.Windows.Forms.TextBox ErpIdTextBox;
    }
}