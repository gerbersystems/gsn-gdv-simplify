﻿using Gestion_Des_Vendanges.DAO;
using GSN.GDV.Data.Extensions;
using GSN.GDV.GUI.Extensions;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de saisie des parcelles
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmSaisieParcelle : WfrmBase
    {
        #region Properties

        private WfrmSaisieAcquit _formAcquit;
        private BUSINESS.Parcelle _parcelle;
        private int _comId;
        private bool _newParcelle;
        private bool initializing;

        public enum Par_ModeCulture
        {
            Fils = 1,
            Gobelet = 2,
            Autre = 3
        }

        #endregion Properties

        #region Constructor

        public WfrmSaisieParcelle(WfrmSaisieAcquit formAcquit, int comId)
        {
            InitWfrmSaisieParcelle(formAcquit, comId);
            _newParcelle = true;
        }

        public WfrmSaisieParcelle(WfrmSaisieAcquit formAcquit, BUSINESS.Parcelle parcelle, int comId)
        {
            InitWfrmSaisieParcelle(formAcquit, comId);

            _parcelle = parcelle;
            _newParcelle = false;
            lTitre.Text = "Modifier parcelle";
            this.Text = "Gestion des acquits - modifier la parcelle";
        }

        private void InitWfrmSaisieParcelle(WfrmSaisieAcquit formAcquit, int comId)
        {
            initializing = true;
            InitializeComponent();

            _formAcquit = formAcquit;
            _comId = comId;

            pAR_SURFACE_COMPTEETextBox.TextChanged += new EventHandler(this.CalculDroitProduction);
            pAR_QUOTATextBox.TextChanged += new EventHandler(this.CalculDroitProduction);
            pAR_LITRESTextBox.Visible = true;
            lblTauxQuota.Text = "Litres/m2 :";
            lblLitresOrKilos.Text = "Litres :";
        }

        private void WfrmAddParcelle_Load(object sender, EventArgs e)
        {
            try
            {
                WfrmAddParcelle_Loader(sender, e);
            }
            catch (InvalidEnumArgumentException ex)
            {
                MessageBox.Show("Veuillez vérifier les paramètres.\nLe canton sélectionné n'est pas pris en charge.\n" + ex.Message, "Erreur de paramètrage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void WfrmAddParcelle_Loader(object sender, EventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                ConnectionManager.Instance.AddGvTableAdapter(lIE_LIEU_DE_PRODUCTIONTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(aUT_AUTRE_MENTIONTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(aCQ_ACQUITTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(cEP_CEPAGETableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(pAR_PARCELLETableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(zOT_ZONE_TRAVAILTableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(pAR_SECTEUR_VISITETableAdapter);
                ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            }

            pAR_MODE_CULTUREcombobox.DataSource = Enum.GetValues(typeof(Par_ModeCulture));

            pAR_LITRESTextBox.Validated += NumerictextBox_Validated;
            pAR_QUOTATextBox.Validated += NumerictextBox_Validated;
            pAR_SURFACE_CEPAGETextBox.Validated += NumerictextBox_Validated;
            pAR_SURFACE_COMPTEETextBox.Validated += NumerictextBox_Validated;

            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
            {
                lIE_LIEU_DE_PRODUCTIONTableAdapter.Fill(dSPesees.LIE_LIEU_DE_PRODUCTION);
                aUT_AUTRE_MENTIONTableAdapter.Fill(dSPesees.AUT_AUTRE_MENTION);
                aCQ_ACQUITTableAdapter.Fill(dSPesees.ACQ_ACQUIT);
                cEP_CEPAGETableAdapter.Fill(dSPesees.CEP_CEPAGE);
                pAR_PARCELLETableAdapter.Fill(dSPesees.PAR_PARCELLE);
                zOT_ZONE_TRAVAILTableAdapter.Fill(dSPesees.ZOT_ZONE_TRAVAIL);
                pAR_SECTEUR_VISITETableAdapter.Fill(dSPesees.PAR_SECTEUR_VISITE);
            }

            ValidationBuilderExtension.ApplyValidation(pAR_NUMEROTextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            ValidationBuilderExtension.ApplyValidation(pAR_SURFACE_CEPAGETextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            ValidationBuilderExtension.ApplyValidation(pAR_SURFACE_COMPTEETextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            ValidationBuilderExtension.ApplyValidation(pAR_QUOTATextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            ValidationBuilderExtension.ApplyValidation(pAR_LITRESTextBox, type: ValidationBuilderExtension.ValidationTypeEnum.Numeric);
            if (_newParcelle)
            {
                this.pAR_PARCELLEBindingSource.AddNew();
                switch (BUSINESS.GlobalParam.Instance.Canton)
                {
                    case SettingExtension.CantonEnum.Neuchâtel:
                        //TODO LIE | A QUOI IL SERT ??????????????
                        this.pCOPARCOMBindingSource.AddNew();
                        break;
                }
                aUT_IDComboBox.SelectedText = "";
            }
            else
            {
                SetChamps();
            }

            aCQ_IDComboBox.SelectedValue = _formAcquit.CurrentAcquitID;
            cEP_CEPAGETableAdapter.FillByCouleurFromAcquiId(dSPesees.CEP_CEPAGE, (int)aCQ_IDComboBox.SelectedValue);
            try
            {
                cEP_IDComboBox.SelectedValue = _parcelle.Cepage;
                lIE_IDComboBox.SelectedValue = _parcelle.LieuProduction;
                aUT_IDComboBox.SelectedValue = _parcelle.AutreMention;
            }
            catch (NullReferenceException)
            {
                //TODO Si nouvelle parcelle
            }
            this.pAR_IDTextBox.Hide();

            if (cEP_IDComboBox.SelectedValue != null && (string.IsNullOrEmpty(pAR_QUOTATextBox.Text) || pAR_QUOTATextBox.Text.Equals("0")))
                SetQuotaLitres();

            initializing = false;
        }

        #endregion Constructor

        #region Methods

        private void SetQuotaLitres()
        {
            using (var context = ConnectionManager.Instance.CreateMainDataContext())
            {
                string idCepage = cEP_IDComboBox.SelectedValue.ToString();
                string selectedAcquit = aCQ_IDComboBox.SelectedValue.ToString();
                int idRegion = (int)aCQ_ACQUITTableAdapter.GetRegionByAcquiId(Int32.Parse(selectedAcquit));
                int idClasse = (int)aCQ_ACQUITTableAdapter.GetClaIdByAcqId(Int32.Parse(selectedAcquit));
                try
                {
                    pAR_QUOTATextBox.Text = QuotaProductionExtension.GetTaux(context, Int32.Parse(idCepage), idRegion, BUSINESS.Utilities.AnneeCourante, idClasse).ToString();
                }
                catch (Exception)
                {
                    //TODO Implement exception
                    throw;
                }
            }
        }

        private void SetChamps()
        {
            aCQ_IDComboBox.SelectedValue = _parcelle.Acquit;
            cEP_IDComboBox.SelectedValue = _parcelle.Cepage;
            lIE_IDComboBox.SelectedValue = _parcelle.LieuProduction;
            pAR_LITRESTextBox.Text = _parcelle.Kilos.ToString();
            pAR_NUMEROTextBox.Text = _parcelle.Numero.ToString();
            pAR_QUOTATextBox.Text = _parcelle.Quota.ToString();
            pAR_SURFACE_CEPAGETextBox.Text = _parcelle.SurfaceCepage.ToString();
            pAR_SURFACE_COMPTEETextBox.Text = _parcelle.SurfaceComptee.ToString();
            aUT_IDComboBox.SelectedValue = _parcelle.AutreMention;

            if (_parcelle.ZoneTravail.HasValue)
                zOT_ZONE_TRAVAILcomboBox.SelectedValue = _parcelle.ZoneTravail;

            if (!string.IsNullOrWhiteSpace(_parcelle.SecteurVisite))
                pAR_SECTEUR_VISITEcombobox.SelectedValue = _parcelle.SecteurVisite;

            pAR_MODE_CULTUREcombobox.Text = _parcelle.ModeCulture;
        }

        private bool UpdateParcelle()
        {
            bool result = true;
            try
            {
                _parcelle.Acquit = (int)aCQ_IDComboBox.SelectedValue;
                _parcelle.Cepage = (int)cEP_IDComboBox.SelectedValue;
                _parcelle.Litres = decimal.Parse(pAR_LITRESTextBox.Text);
                _parcelle.Numero = Int32.Parse(pAR_NUMEROTextBox.Text);
                _parcelle.Quota = decimal.Parse(pAR_QUOTATextBox.Text);
                _parcelle.SurfaceCepage = decimal.Parse(pAR_SURFACE_CEPAGETextBox.Text);
                _parcelle.SurfaceComptee = decimal.Parse(pAR_SURFACE_COMPTEETextBox.Text);
                _parcelle.AutreMention = (int)aUT_IDComboBox.SelectedValue;

                if (zOT_ZONE_TRAVAILcomboBox.SelectedValue != null)
                    _parcelle.ZoneTravail = (int)zOT_ZONE_TRAVAILcomboBox.SelectedValue;

                if (pAR_SECTEUR_VISITEcombobox.Text == "")
                    _parcelle.SecteurVisite = SetSecteurVisiteToNone(_parcelle.SecteurVisite);
                else
                    _parcelle.SecteurVisite = pAR_SECTEUR_VISITEcombobox.Text;

                _parcelle.ModeCulture = pAR_MODE_CULTUREcombobox.SelectedValue?.ToString();

                if (decimal.TryParse(pAR_KILOSTextBox.Text, out decimal kilos))
                    _parcelle.Kilos = kilos;

                _parcelle.LieuProduction = (int)lIE_IDComboBox.SelectedValue;
            }
            catch (InvalidEnumArgumentException ex)
            {
                MessageBox.Show("Veuillez vérifier les paramètres.\nLe canton sélectionné n'est pas pris en charge.\n" + ex.Message, "Erreur de paramètrage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                result = false;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void Save_Parcelles()
        {
            Validate();
            pAR_PARCELLEBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.dSPesees);
        }

        private bool ChkSaisie()
        {
            bool result = true;
            try
            {
                int testInt;
                float testFloat;

                testInt = (int)aCQ_IDComboBox.SelectedValue;
                testInt = Int32.Parse(pAR_NUMEROTextBox.Text);
                testInt = (int)cEP_IDComboBox.SelectedValue;
                testFloat = float.Parse(pAR_SURFACE_CEPAGETextBox.Text);
                testFloat = float.Parse(pAR_SURFACE_COMPTEETextBox.Text);
                testFloat = float.Parse(pAR_QUOTATextBox.Text);
                testInt = (int)aUT_IDComboBox.SelectedValue;
                if (pAR_SECTEUR_VISITEcombobox.Text == "")
                    pAR_SECTEUR_VISITEcombobox.Text = SetSecteurVisiteToNone(pAR_SECTEUR_VISITEcombobox.Text);
                testInt = pAR_MODE_CULTUREcombobox.SelectedValue == null ? 0 : (int)pAR_MODE_CULTUREcombobox.SelectedValue;
                testInt = (int)lIE_IDComboBox.SelectedValue;
                testFloat = float.Parse(pAR_LITRESTextBox.Text);
            }
            catch (InvalidEnumArgumentException ex)
            {
                MessageBox.Show("Veuillez vérifier les paramètres.\nLe canton sélectionné n'est pas pris en charge.\n" + ex.Message, "Erreur de paramètrage", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                result = false;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        private void WfrmAddParcelle_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formAcquit.UpdateDataGrid();
        }

        //ATH - 27.07.09 - Met à jour les données (clic-droit)
        private void CalculDroitProduction(object sender, EventArgs e)
        {
            decimal quota, surface;
            try
            {
                quota = decimal.Parse(pAR_QUOTATextBox.Text);
                surface = decimal.Parse(pAR_SURFACE_COMPTEETextBox.Text);
                pAR_LITRESTextBox.Text = Math.Round(quota * surface, 2).ToString();
            }
            catch { }
        }

        private string SetSecteurVisiteToNone(string strStringToSet)
        {
            return strStringToSet = "Aucun";
        }

        #endregion Methods

        #region Right Button Event

        private void AutMenuStrip_Click_1(object sender, EventArgs e)
        {
            WfrmGestionDesAutresMentions.Wfrm.WfrmShowDialog();
            aUT_AUTRE_MENTIONTableAdapter.Fill(dSPesees.AUT_AUTRE_MENTION);
            aUT_IDComboBox.DataSource = dSPesees.AUT_AUTRE_MENTION;
        }

        private void ToolLieuProduction_Click(object sender, EventArgs e)
        {
            WfrmGestionDesLieuxDeProductions.Wfrm.WfrmShowDialog();
            lIE_LIEU_DE_PRODUCTIONTableAdapter.FillByComId(dSPesees.LIE_LIEU_DE_PRODUCTION, _comId);
        }

        private void AccederAuxCepagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WfrmGestionDesCepages.Wfrm.WfrmShowDialog();
            this.cEP_CEPAGETableAdapter.FillByCouleurFromAcquiId(this.dSPesees.CEP_CEPAGE, (int)this.aCQ_IDComboBox.SelectedValue);
        }

        #endregion Right Button Event

        #region Button Event

        private void CmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CmdAjouter_Click(object sender, EventArgs e)
        {
            try
            {
                if (_newParcelle)
                {
                    if (ChkSaisie())
                    {
                        Save_Parcelles();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Veuillez saisir tous les champs correctement.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (UpdateParcelle())
                    {
                        _formAcquit.UpdateCurrentRow(_parcelle);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Veuillez saisir tous les champs correctement.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur interne du logiciel.\n\n" + ex.ToString(), "Exception non prise en charge", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion Button Event

        #region Events

        private void NumerictextBox_Validated(object sender, EventArgs e)
        {
            if (!(sender is TextBox)) return;

            if (double.TryParse(((TextBox)sender).Text, out double valeur))
            {
                valeur = Math.Round(valeur, 2);
                ((TextBox)sender).Text = valeur.ToString();
            }
            else
            {
                ((TextBox)sender).Text = string.Empty;
            }
        }

        private void PAR_SURFACE_CEPAGETextBox_Validated(object sender, EventArgs e)
        {
            pAR_SURFACE_COMPTEETextBox.Text = pAR_SURFACE_CEPAGETextBox.Text;
            NumerictextBox_Validated(pAR_SURFACE_COMPTEETextBox, e);
        }

        private void CEP_IDComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (initializing) return;

            SetQuotaLitres();
        }

        #endregion Events
    }
}