﻿using System;

namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmGestionDesDossiers : WfrmSetting
    {
        private FormSettingsManagerDossier _manager;
        private static WfrmGestionDesDossiers _wfrm;

        public static WfrmGestionDesDossiers Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesDossiers();
                }
                return _wfrm;
            }
        }

        public override bool CanDelete
        {
            get
            {
                return false;
            }
        }

        private WfrmGestionDesDossiers()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesDossiers_Load(object sender, EventArgs e)
        {
            dAT_DATABASETableAdapter.Connection.ConnectionString = DAO.ConnectionManager.Instance.MasterConnectionString;
            tableAdapterManager.Connection.ConnectionString = DAO.ConnectionManager.Instance.MasterConnectionString;
            dAT_DATABASETableAdapter.Fill(this.dSMaster.DAT_DATABASE);
            _manager = new FormSettingsManagerDossier(this, dAT_IDTextBox, dAT_NOMTextBox, btnNew, btnUpdate, btnSupprimer,
                dAT_DATABASEDataGridView, dAT_DATABASEBindingSource, tableAdapterManager, dSMaster, dAT_NOMTextBox, dAT_DESCRIPTIONTextBox);
            _manager.AddControl(dAT_NOMTextBox);
            _manager.AddControl(dAT_DESCRIPTIONTextBox);
        }
    }
}