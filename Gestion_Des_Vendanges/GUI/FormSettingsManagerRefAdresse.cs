﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Gère le comportement des fromulaires utilisant des adresses
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class FormSettingsManagerRefAdresse : FormSettingsManagerPesees
    {
        private ADR_ADRESSETableAdapter _adrTable;
        private AdresseCompletTableAdapter _adrCompletTable;

        public FormSettingsManagerRefAdresse(WfrmSetting form,
                                             TextBox idTextBox,
                                             Control focusControl,
                                             Button btnNew,
                                             Button btnUpdate,
                                             Button btnDelete,
                                             DataGridView dataGridView,
                                             BindingSource bindingSource,
                                             TableAdapterManager tableAdapterManager,
                                             DSPesees dsPesees,
                                             AdresseCompletTableAdapter adrCompletTable)
            : base(form, idTextBox, focusControl, btnNew, btnUpdate, btnDelete, dataGridView, bindingSource, tableAdapterManager, dsPesees)
        {
            _adrTable = ConnectionManager.Instance.CreateGvTableAdapter<ADR_ADRESSETableAdapter>();
            _adrCompletTable = adrCompletTable;
        }

        protected override void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int adrId = (int)DataGridView.CurrentRow.Cells["ADR_ID"].Value;

                base.BtnDelete_Click(sender, e);

                if (IsDeleted && (int)_adrTable.NbRef(adrId) == 0)
                {
                    _adrTable.DeleteByAdrId(adrId);
                    _adrCompletTable.Fill(DataSet.AdresseComplet);
                }
            }
            catch (NullReferenceException)
            {
                base.BtnDelete_Click(sender, e);
            }
            catch (InvalidCastException)
            {
                base.BtnDelete_Click(sender, e);
            }
        }
    }
}