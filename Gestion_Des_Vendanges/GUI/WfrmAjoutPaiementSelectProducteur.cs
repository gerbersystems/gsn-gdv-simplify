﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de sélection des producteurs
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class WfrmAjoutPaiementSelectProducteur : WfrmBase
    {
        //private static WfrmAjoutPaiementSelectProducteur _wfrm;
        private BUSINESS.ParametrePaiement _parametrePaiement;

        public WfrmAjoutPaiementSelectProducteur(BUSINESS.ParametrePaiement parametrePaiement)
        {
            InitializeComponent();
            _parametrePaiement = parametrePaiement;
        }

        private void WfrmAjoutPaiementSelectProprietaire_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(adresseCompletTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aDR_ADRESSETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pRO_PROPRIETAIRETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);

            this.adresseCompletTableAdapter.Fill(this.dSPesees.AdresseComplet);
            this.aDR_ADRESSETableAdapter.Fill(this.dSPesees.ADR_ADRESSE);
            this.pRO_PROPRIETAIRETableAdapter.FillProducteurWithOpenPesee(dSPesees.PRO_PROPRIETAIRE, _parametrePaiement.AnnId);

            if (_parametrePaiement.CountProId > 0)
            {
                int[] proIds = _parametrePaiement.ProIds;
                foreach (DataGridViewRow row in pRO_PROPRIETAIREDataGridView.Rows)
                {
                    for (int i = 0; i < proIds.Length; i++)
                    {
                        if (proIds[i] == (int)row.Cells["PRO_ID"].Value)
                        {
                            row.Cells["SELECT"].Value = true;
                            i = proIds.Length;
                        }
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow row in pRO_PROPRIETAIREDataGridView.Rows)
                    row.Cells["SELECT"].Value = true;
            }
        }

        private bool SaveDonnees(bool sansEchec)
        {
            _parametrePaiement.ClearProprietaire();
            foreach (DataGridViewRow row in pRO_PROPRIETAIREDataGridView.Rows)
            {
                if (row.Cells["SELECT"].Value != null)
                {
                    if ((bool)row.Cells["SELECT"].Value)
                        _parametrePaiement.AddProprietaire((int)row.Cells["PRO_ID"].Value);
                }
            }
            if (_parametrePaiement.CountProId == 0)
            {
                if (!sansEchec)
                    MessageBox.Show("Veuillez sélectionner au moins un producteur.", "Sélection incorrect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                return false;
            }
            else
            {
                return true;
            }
        }

        private void BtnSuivant_Click(object sender, EventArgs e)
        {
            if (SaveDonnees(false))
                DialogResult = DialogResult.OK;
        }

        private void PRO_PROPRIETAIREDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == pRO_PROPRIETAIREDataGridView.Columns["select"].Index)
            {
                bool allSelect = true;
                pRO_PROPRIETAIREDataGridView.CurrentCell = pRO_PROPRIETAIREDataGridView.CurrentRow.Cells["PRO_NOM"];
                for (int i = 0; i < pRO_PROPRIETAIREDataGridView.Rows.Count && allSelect; i++)
                {
                    allSelect = (pRO_PROPRIETAIREDataGridView.Rows[i].Cells["select"].Value == null) ? false : (bool)pRO_PROPRIETAIREDataGridView.Rows[i].Cells["select"].Value;
                }
                foreach (DataGridViewRow row in pRO_PROPRIETAIREDataGridView.Rows)
                {
                    row.Cells["select"].Value = !allSelect;
                }
            }
        }

        private void BtnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtnPrecedent_Click(object sender, EventArgs e)
        {
            SaveDonnees(true);
            DialogResult = DialogResult.Retry;
        }
    }
}