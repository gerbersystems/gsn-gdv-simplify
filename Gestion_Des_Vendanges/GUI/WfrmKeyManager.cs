﻿using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire permettant le changement de la clé d'activation
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmKeyManager : Form
    {
        private Key _cleDepart;

        public WfrmKeyManager()
        {
            InitializeComponent();
            _cleDepart = new Key("");
            if (Application.OpenForms[0] is SplashScreen)
            {
                Application.OpenForms[0].Hide();
            }
        }

        public WfrmKeyManager(string cleActivation)
        {
            InitializeComponent();
            _cleDepart = new Key(cleActivation);
            initTxtCle(cleActivation);
        }

        private void initTxtCle(string cleActivation)
        {
            Key cle = new Key(cleActivation);
            txtCle1.Text = cle.SplitValue[0];
            txtCle2.Text = cle.SplitValue[1];
            txtCle3.Text = cle.SplitValue[2];
            txtCle4.Text = cle.SplitValue[3];
        }

        private void cmdActiver_Click(object sender, EventArgs e)
        {
            string[] cle = new string[4];
            cle[0] = txtCle1.Text;
            cle[1] = txtCle2.Text;
            cle[2] = txtCle3.Text;
            cle[3] = txtCle4.Text;
            Key newCle = new Key(cle);

            if (_cleDepart.Value != newCle.Value)
            {
                DAO.DSMasterTableAdapters.LIC_LICENSETableAdapter licTable = new DAO.DSMasterTableAdapters.LIC_LICENSETableAdapter();
                licTable.Connection.ConnectionString = DAO.ConnectionManager.Instance.MasterConnectionString;
                if (LICENSE.LicenseManager.CheckCle(newCle.Value))
                {
                    licTable.SetCle(newCle.Value, BUSINESS.Utilities.LicId);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Erreur, votre clé n'est pas valide.\nVeuillez saisir à nouveau votre clé d'activation\nSi le problème persiste, veuillez contacter Gerber Systems and Networks", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void cmdQuitter_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void txtCle1_TextChanged(object sender, EventArgs e)
        {
            controlSaisieTxtCle(txtCle1, txtCle2);
        }

        private void controlSaisieTxtCle(TextBox txtCle, TextBox txtCleNext)
        {
            if (txtCle.TextLength == 8)
            {
                txtCleNext.Focus();
            }
            else if (txtCle.TextLength > 8)
            {
                txtCle.Text = txtCle.Text.Substring(0, 8);
            }
        }

        private void txtCle2_TextChanged(object sender, EventArgs e)
        {
            controlSaisieTxtCle(txtCle2, txtCle3);
        }

        private void txtCle3_TextChanged(object sender, EventArgs e)
        {
            controlSaisieTxtCle(txtCle3, txtCle4);
        }

        private void txtCle4_TextChanged(object sender, EventArgs e)
        {
            if (txtCle4.TextLength > 8)
            {
                txtCle4.Text = txtCle4.Text.Substring(0, 8);
            }
        }

        private void cmdPaste_Click(object sender, EventArgs e)
        {
            try
            {
                initTxtCle(Clipboard.GetText());
            }
            catch
            {
            }
        }

        private void WfrmKeyManager_Activated(object sender, EventArgs e)
        {
            cmdPaste.Enabled = Clipboard.ContainsText();
        }
    }
}