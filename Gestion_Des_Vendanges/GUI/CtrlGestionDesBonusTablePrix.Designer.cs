﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class CtrlGestionDesBonusTablePrix
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lIE_IDLabel;
            System.Windows.Forms.Label cRU_NOMLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbAnnee = new System.Windows.Forms.ComboBox();
            this.gvSplitContainer1 = new Gestion_Des_Vendanges.GUI.GVSplitContainer();
            this.OechsleMoyenParCepageDataGridView = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lprix100 = new System.Windows.Forms.Label();
            this.bON_BONUSDataGridView = new System.Windows.Forms.DataGridView();
            this.BON_OE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BON_OE_FIN = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BON_BRIX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRIX_FIN = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lIE_IDComboBox = new System.Windows.Forms.ComboBox();
            this.vAL_VALEURTextBox = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.cEP_IDComboBox = new System.Windows.Forms.ComboBox();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.aNN_IDTextBox = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            lIE_IDLabel = new System.Windows.Forms.Label();
            cRU_NOMLabel = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvSplitContainer1)).BeginInit();
            this.gvSplitContainer1.Panel1.SuspendLayout();
            this.gvSplitContainer1.Panel2.SuspendLayout();
            this.gvSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OechsleMoyenParCepageDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cbAnnee);
            this.panel2.Location = new System.Drawing.Point(58, 161);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 211);
            this.panel2.TabIndex = 54;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Année :";
            // 
            // cbAnnee
            // 
            this.cbAnnee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAnnee.DisplayMember = "ANN_AN";
            this.cbAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnnee.FormattingEnabled = true;
            this.cbAnnee.Location = new System.Drawing.Point(6, 35);
            this.cbAnnee.Name = "cbAnnee";
            this.cbAnnee.Size = new System.Drawing.Size(191, 21);
            this.cbAnnee.TabIndex = 0;
            this.cbAnnee.ValueMember = "ANN_ID";
            // 
            // gvSplitContainer1
            // 
            this.gvSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvSplitContainer1.Location = new System.Drawing.Point(0, 25);
            this.gvSplitContainer1.Name = "gvSplitContainer1";
            // 
            // gvSplitContainer1.Panel1
            // 
            this.gvSplitContainer1.Panel1.Controls.Add(this.panel2);
            this.gvSplitContainer1.Panel1.Controls.Add(this.label4);
            this.gvSplitContainer1.Panel1.Controls.Add(this.OechsleMoyenParCepageDataGridView);
            // 
            // gvSplitContainer1.Panel2
            // 
            this.gvSplitContainer1.Panel2.Controls.Add(this.label3);
            this.gvSplitContainer1.Panel2.Controls.Add(lIE_IDLabel);
            this.gvSplitContainer1.Panel2.Controls.Add(cRU_NOMLabel);
            this.gvSplitContainer1.Panel2.Controls.Add(this.lprix100);
            this.gvSplitContainer1.Panel2.Controls.Add(this.bON_BONUSDataGridView);
            this.gvSplitContainer1.Panel2.Controls.Add(this.lIE_IDComboBox);
            this.gvSplitContainer1.Panel2.Controls.Add(this.vAL_VALEURTextBox);
            this.gvSplitContainer1.Panel2.Controls.Add(this.btnOK);
            this.gvSplitContainer1.Panel2.Controls.Add(this.cEP_IDComboBox);
            this.gvSplitContainer1.Panel2.Controls.Add(this.btnAnnuler);
            this.gvSplitContainer1.Panel2.Controls.Add(this.aNN_IDTextBox);
            this.gvSplitContainer1.Size = new System.Drawing.Size(891, 543);
            this.gvSplitContainer1.SplitterDistance = 443;
            this.gvSplitContainer1.TabIndex = 55;
            // 
            // OechsleMoyenParCepageDataGridView
            // 
            this.OechsleMoyenParCepageDataGridView.AllowUserToAddRows = false;
            this.OechsleMoyenParCepageDataGridView.AllowUserToDeleteRows = false;
            this.OechsleMoyenParCepageDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.OechsleMoyenParCepageDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OechsleMoyenParCepageDataGridView.Location = new System.Drawing.Point(3, 51);
            this.OechsleMoyenParCepageDataGridView.Name = "OechsleMoyenParCepageDataGridView";
            this.OechsleMoyenParCepageDataGridView.ReadOnly = true;
            this.OechsleMoyenParCepageDataGridView.Size = new System.Drawing.Size(437, 489);
            this.OechsleMoyenParCepageDataGridView.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label4.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label4.Location = new System.Drawing.Point(10, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 20);
            this.label4.TabIndex = 53;
            this.label4.Text = "Gestion des bonus";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresForms", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label3.Location = new System.Drawing.Point(8, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 20);
            this.label3.TabIndex = 64;
            this.label3.Text = "Saisie bonus";
            // 
            // lIE_IDLabel
            // 
            lIE_IDLabel.AutoSize = true;
            lIE_IDLabel.Location = new System.Drawing.Point(8, 49);
            lIE_IDLabel.Name = "lIE_IDLabel";
            lIE_IDLabel.Size = new System.Drawing.Size(101, 13);
            lIE_IDLabel.TabIndex = 60;
            lIE_IDLabel.Text = "Lieu de production :";
            // 
            // cRU_NOMLabel
            // 
            cRU_NOMLabel.AutoSize = true;
            cRU_NOMLabel.Location = new System.Drawing.Point(8, 76);
            cRU_NOMLabel.Name = "cRU_NOMLabel";
            cRU_NOMLabel.Size = new System.Drawing.Size(53, 13);
            cRU_NOMLabel.TabIndex = 59;
            cRU_NOMLabel.Text = "Cépage : ";
            // 
            // lprix100
            // 
            this.lprix100.AutoSize = true;
            this.lprix100.Location = new System.Drawing.Point(8, 103);
            this.lprix100.Name = "lprix100";
            this.lprix100.Size = new System.Drawing.Size(122, 13);
            this.lprix100.TabIndex = 61;
            this.lprix100.Text = "Valeur 100% (CHF/ kg) :";
            // 
            // bON_BONUSDataGridView
            // 
            this.bON_BONUSDataGridView.AllowUserToAddRows = false;
            this.bON_BONUSDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bON_BONUSDataGridView.BackgroundColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.DataGridColor;
            this.bON_BONUSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bON_BONUSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BON_OE,
            this.BON_OE_FIN,
            this.BON_BRIX,
            this.BRIX_FIN});
            this.bON_BONUSDataGridView.Location = new System.Drawing.Point(8, 137);
            this.bON_BONUSDataGridView.MultiSelect = false;
            this.bON_BONUSDataGridView.Name = "bON_BONUSDataGridView";
            this.bON_BONUSDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.bON_BONUSDataGridView.Size = new System.Drawing.Size(432, 333);
            this.bON_BONUSDataGridView.TabIndex = 62;
            // 
            // BON_OE
            // 
            this.BON_OE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_OE.DataPropertyName = "BON_OE";
            this.BON_OE.HeaderText = "°Oe (début)";
            this.BON_OE.Name = "BON_OE";
            // 
            // BON_OE_FIN
            // 
            this.BON_OE_FIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_OE_FIN.DataPropertyName = "BON_ID";
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.White;
            this.BON_OE_FIN.DefaultCellStyle = dataGridViewCellStyle11;
            this.BON_OE_FIN.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.BON_OE_FIN.HeaderText = "°Oe (fin)";
            this.BON_OE_FIN.Name = "BON_OE_FIN";
            this.BON_OE_FIN.ReadOnly = true;
            this.BON_OE_FIN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BON_OE_FIN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // BON_BRIX
            // 
            this.BON_BRIX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BON_BRIX.DataPropertyName = "BON_BRIX";
            this.BON_BRIX.HeaderText = "%Brix (début)";
            this.BON_BRIX.Name = "BON_BRIX";
            // 
            // BRIX_FIN
            // 
            this.BRIX_FIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BRIX_FIN.DataPropertyName = "BON_ID";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.BRIX_FIN.DefaultCellStyle = dataGridViewCellStyle12;
            this.BRIX_FIN.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.BRIX_FIN.HeaderText = "%Brix (fin)";
            this.BRIX_FIN.Name = "BRIX_FIN";
            this.BRIX_FIN.ReadOnly = true;
            this.BRIX_FIN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BRIX_FIN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // lIE_IDComboBox
            // 
            this.lIE_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lIE_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lIE_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lIE_IDComboBox.DisplayMember = "LIE_NOM";
            this.lIE_IDComboBox.FormattingEnabled = true;
            this.lIE_IDComboBox.Location = new System.Drawing.Point(177, 49);
            this.lIE_IDComboBox.Name = "lIE_IDComboBox";
            this.lIE_IDComboBox.Size = new System.Drawing.Size(249, 21);
            this.lIE_IDComboBox.TabIndex = 54;
            this.lIE_IDComboBox.ValueMember = "LIE_ID";
            // 
            // vAL_VALEURTextBox
            // 
            this.vAL_VALEURTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vAL_VALEURTextBox.Location = new System.Drawing.Point(177, 103);
            this.vAL_VALEURTextBox.Name = "vAL_VALEURTextBox";
            this.vAL_VALEURTextBox.Size = new System.Drawing.Size(249, 20);
            this.vAL_VALEURTextBox.TabIndex = 56;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnOK.Location = new System.Drawing.Point(284, 476);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 30);
            this.btnOK.TabIndex = 57;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            // 
            // cEP_IDComboBox
            // 
            this.cEP_IDComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cEP_IDComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cEP_IDComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cEP_IDComboBox.DisplayMember = "CEP_NOM";
            this.cEP_IDComboBox.Enabled = false;
            this.cEP_IDComboBox.FormattingEnabled = true;
            this.cEP_IDComboBox.Location = new System.Drawing.Point(177, 76);
            this.cEP_IDComboBox.Name = "cEP_IDComboBox";
            this.cEP_IDComboBox.Size = new System.Drawing.Size(249, 21);
            this.cEP_IDComboBox.TabIndex = 55;
            this.cEP_IDComboBox.ValueMember = "CEP_ID";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(365, 476);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 58;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            // 
            // aNN_IDTextBox
            // 
            this.aNN_IDTextBox.Location = new System.Drawing.Point(211, 301);
            this.aNN_IDTextBox.Name = "aNN_IDTextBox";
            this.aNN_IDTextBox.ReadOnly = true;
            this.aNN_IDTextBox.Size = new System.Drawing.Size(100, 20);
            this.aNN_IDTextBox.TabIndex = 63;
            this.aNN_IDTextBox.Text = "ANN_ID";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(891, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // CtrlGestionDesBonusTablePrix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.BackColor;
            this.Controls.Add(this.gvSplitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "CtrlGestionDesBonusTablePrix";
            this.Size = new System.Drawing.Size(891, 568);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gvSplitContainer1.Panel1.ResumeLayout(false);
            this.gvSplitContainer1.Panel1.PerformLayout();
            this.gvSplitContainer1.Panel2.ResumeLayout(false);
            this.gvSplitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvSplitContainer1)).EndInit();
            this.gvSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OechsleMoyenParCepageDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bON_BONUSDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAnnee;
        private GVSplitContainer gvSplitContainer1;
        private System.Windows.Forms.DataGridView OechsleMoyenParCepageDataGridView;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lprix100;
        private System.Windows.Forms.DataGridView bON_BONUSDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn BON_OE;
        private System.Windows.Forms.DataGridViewComboBoxColumn BON_OE_FIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn BON_BRIX;
        private System.Windows.Forms.DataGridViewComboBoxColumn BRIX_FIN;
        private System.Windows.Forms.ComboBox lIE_IDComboBox;
        private System.Windows.Forms.TextBox vAL_VALEURTextBox;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox cEP_IDComboBox;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.TextBox aNN_IDTextBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}
