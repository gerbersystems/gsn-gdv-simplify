﻿using Gestion_Des_Vendanges.DAO;
using System;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des districts
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesDistricts : WfrmSetting
    {
        private FormSettingsManagerPesees _manager;
        private static WfrmGestionDesDistricts _wfrm;

        public override bool CanDelete
        {
            get
            {
                bool resutlat;
                try
                {
                    int disID = (int)dIS_DISTRICTDataGridView.CurrentRow.Cells["DIS_ID"].Value;
                    resutlat = (dIS_DISTRICTTableAdapter.NbRef(disID) == 0);
                }
                catch
                {
                    resutlat = true;
                }
                return resutlat;
            }
        }

        public static WfrmGestionDesDistricts Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesDistricts();
                }
                return _wfrm;
            }
        }

        public WfrmGestionDesDistricts()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesDistricts_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(dIS_DISTRICTTableAdapter);
            this.dIS_DISTRICTTableAdapter.Fill(this.dSPesees.DIS_DISTRICT);
            _manager = new FormSettingsManagerPesees(this, dIS_IDTextBox, dIS_NOMTextBox, btnAddClass, btnSaveClass, btnSupprimer,
                dIS_DISTRICTDataGridView, dIS_DISTRICTBindingSource, tableAdapterManager, dSPesees);
            _manager.AddControl(dIS_NOMTextBox);
        }
    }
}