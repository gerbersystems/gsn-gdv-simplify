﻿using Gestion_Des_Vendanges.DAO;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /*
*  Description: Formulaire de gestion des couleurs
*  Date de création:
*  Modifications:
*   ATH - 02.11.2009 - Application des conventions de programmation
* */

    partial class WfrmGestionDesCouleurs : WfrmSetting
    {
        private static WfrmGestionDesCouleurs _wfrm;
        private FormSettingsManagerCouleur _manager;

        public override bool CanDelete
        {
            get
            {
                bool resultat;
                try
                {
                    int couId = (int)cOU_COULEURDataGridView.CurrentRow.Cells["COU_ID"].Value;
                    resultat = (int)cOU_COULEURTableAdapter.NbRef(couId) == 0;
                }
                catch
                {
                    resultat = true;
                }
                return resultat;
            }
        }

        public static WfrmGestionDesCouleurs Wfrm
        {
            get
            {
                if (_wfrm == null || _wfrm.IsDisposed)
                {
                    _wfrm = new WfrmGestionDesCouleurs();
                }
                return _wfrm;
            }
        }

        private WfrmGestionDesCouleurs()
        {
            InitializeComponent();
        }

        private void WfrmGestionDesCouleurs_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            bool optionWinBiz = BUSINESS.Utilities.GetOption(2);
            if (optionWinBiz)
            {
                try
                {
                    new DAO.BDVendangeControleur().UpdateModeComptabilisations(BUSINESS.Utilities.IdAnneeCourante);
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    MessageBox.Show("Attention le dossier WinBIZ est indisponible.\n" +
                        "Les modes de comptabilisation ne sont pas synchronisés.", "Gestion des vendanges", MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
                ConnectionManager.Instance.AddGvTableAdapter(mOC_MODE_COMPTABILISATIONTableAdapter);
                mOC_MODE_COMPTABILISATIONTableAdapter.FillByAnnId(this.dSPesees.MOC_MODE_COMPTABILISATION, BUSINESS.Utilities.IdAnneeCourante);
            }
            ConnectionManager.Instance.AddGvTableAdapter(cOU_COULEURTableAdapter);
            cOU_COULEURTableAdapter.Fill(dSPesees.COU_COULEUR);
            _manager = new FormSettingsManagerCouleur(this, cOU_IDTextBox, cOU_NOMTextBox,
                btnAddCouleur, btnUpdate, btnSupprimer, cOU_COULEURDataGridView,
                cOU_COULEURBindingSource, tableAdapterManager, dSPesees, cOU_CEPAGECheckBox, mOC_IDComboBox, optionWinBiz);
            _manager.AddControl(cOU_NOMTextBox);
            _manager.AddControl(cOU_CEPAGECheckBox);
            _manager.AddControl(mOC_IDComboBox);
        }
    }
}