﻿namespace Gestion_Des_Vendanges.GUI
{
    partial class WfrmAjoutPaiementMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.optPrixFixe = new System.Windows.Forms.RadioButton();
            this.gbPrixFixe = new System.Windows.Forms.GroupBox();
            this.lPrixFixe = new System.Windows.Forms.Label();
            this.txtPrixFixe = new System.Windows.Forms.TextBox();
            this.gbCepage = new System.Windows.Forms.GroupBox();
            this.contextBonus = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.accéderAuxTablesDePrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbLieuProduction = new System.Windows.Forms.ComboBox();
            this.lIELIEUDEPRODUCTIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dSPesees = new Gestion_Des_Vendanges.DAO.DSPesees();
            this.label2 = new System.Windows.Forms.Label();
            this.optPrixCepage = new System.Windows.Forms.RadioButton();
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optPrixCepLieTablePrix = new System.Windows.Forms.RadioButton();
            this.optPrixCepLie = new System.Windows.Forms.RadioButton();
            this.gbCalcul = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkBonus = new System.Windows.Forms.CheckBox();
            this.numPourcentage = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.mOCMODECOMPTABILISATIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnSuivant = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPrecedent = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.mOC_MODE_COMPTABILISATIONTableAdapter = new Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbModeCompta = new System.Windows.Forms.ComboBox();
            this.optSelectModeCompta = new System.Windows.Forms.RadioButton();
            this.optDefaultModeCompta = new System.Windows.Forms.RadioButton();
            this.chkZoneTravail = new System.Windows.Forms.CheckBox();
            this.grbPaiementLitresOptions = new System.Windows.Forms.GroupBox();
            this.chkLitreMoutRaisin = new System.Windows.Forms.CheckBox();
            this.lblLitreMoutRaisin = new System.Windows.Forms.Label();
            this.gbPrixFixe.SuspendLayout();
            this.gbCepage.SuspendLayout();
            this.contextBonus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.gbCalcul.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grbPaiementLitresOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // optPrixFixe
            // 
            this.optPrixFixe.AutoSize = true;
            this.optPrixFixe.Location = new System.Drawing.Point(36, 59);
            this.optPrixFixe.Name = "optPrixFixe";
            this.optPrixFixe.Size = new System.Drawing.Size(14, 13);
            this.optPrixFixe.TabIndex = 0;
            this.optPrixFixe.TabStop = true;
            this.optPrixFixe.UseVisualStyleBackColor = true;
            this.optPrixFixe.CheckedChanged += new System.EventHandler(this.OptPrix_CheckedChanged);
            // 
            // gbPrixFixe
            // 
            this.gbPrixFixe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPrixFixe.Controls.Add(this.lPrixFixe);
            this.gbPrixFixe.Controls.Add(this.txtPrixFixe);
            this.gbPrixFixe.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gbPrixFixe.Enabled = false;
            this.gbPrixFixe.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.gbPrixFixe.Location = new System.Drawing.Point(67, 27);
            this.gbPrixFixe.Name = "gbPrixFixe";
            this.gbPrixFixe.Size = new System.Drawing.Size(608, 55);
            this.gbPrixFixe.TabIndex = 4;
            this.gbPrixFixe.TabStop = false;
            this.gbPrixFixe.Text = "Prix fixe";
            // 
            // lPrixFixe
            // 
            this.lPrixFixe.AutoSize = true;
            this.lPrixFixe.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.lPrixFixe.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.lPrixFixe.Location = new System.Drawing.Point(24, 30);
            this.lPrixFixe.Name = "lPrixFixe";
            this.lPrixFixe.Size = new System.Drawing.Size(93, 16);
            this.lPrixFixe.TabIndex = 0;
            this.lPrixFixe.Text = "Prix par kg :";
            // 
            // txtPrixFixe
            // 
            this.txtPrixFixe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrixFixe.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPrixFixe.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.txtPrixFixe.Location = new System.Drawing.Point(329, 29);
            this.txtPrixFixe.Name = "txtPrixFixe";
            this.txtPrixFixe.Size = new System.Drawing.Size(258, 19);
            this.txtPrixFixe.TabIndex = 1;
            // 
            // gbCepage
            // 
            this.gbCepage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCepage.ContextMenuStrip = this.contextBonus;
            this.gbCepage.Controls.Add(this.cbLieuProduction);
            this.gbCepage.Controls.Add(this.label2);
            this.gbCepage.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gbCepage.Enabled = false;
            this.gbCepage.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.gbCepage.Location = new System.Drawing.Point(67, 87);
            this.gbCepage.Name = "gbCepage";
            this.gbCepage.Size = new System.Drawing.Size(608, 55);
            this.gbCepage.TabIndex = 4;
            this.gbCepage.TabStop = false;
            this.gbCepage.Text = "Prix par cépage";
            // 
            // contextBonus
            // 
            this.contextBonus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accéderAuxTablesDePrixToolStripMenuItem});
            this.contextBonus.Name = "contextBonus";
            this.contextBonus.Size = new System.Drawing.Size(220, 26);
            // 
            // accéderAuxTablesDePrixToolStripMenuItem
            // 
            this.accéderAuxTablesDePrixToolStripMenuItem.Name = "accéderAuxTablesDePrixToolStripMenuItem";
            this.accéderAuxTablesDePrixToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.accéderAuxTablesDePrixToolStripMenuItem.Text = "Accéder aux tables de prix...";
            this.accéderAuxTablesDePrixToolStripMenuItem.Click += new System.EventHandler(this.AccederAuxTablesDePrixToolStripMenuItem_Click);
            // 
            // cbLieuProduction
            // 
            this.cbLieuProduction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbLieuProduction.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLieuProduction.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLieuProduction.ContextMenuStrip = this.contextBonus;
            this.cbLieuProduction.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "Normal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbLieuProduction.DataSource = this.lIELIEUDEPRODUCTIONBindingSource;
            this.cbLieuProduction.DisplayMember = "LIE_NOM";
            this.cbLieuProduction.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.Normal;
            this.cbLieuProduction.FormattingEnabled = true;
            this.cbLieuProduction.Location = new System.Drawing.Point(329, 23);
            this.cbLieuProduction.Name = "cbLieuProduction";
            this.cbLieuProduction.Size = new System.Drawing.Size(258, 21);
            this.cbLieuProduction.TabIndex = 1;
            this.cbLieuProduction.ValueMember = "LIE_ID";
            // 
            // lIELIEUDEPRODUCTIONBindingSource
            // 
            this.lIELIEUDEPRODUCTIONBindingSource.DataMember = "LIE_LIEU_DE_PRODUCTION";
            this.lIELIEUDEPRODUCTIONBindingSource.DataSource = this.dSPesees;
            // 
            // dSPesees
            // 
            this.dSPesees.DataSetName = "DSPesees";
            this.dSPesees.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label2.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.label2.Location = new System.Drawing.Point(24, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Table de prix :";
            // 
            // optPrixCepage
            // 
            this.optPrixCepage.AutoSize = true;
            this.optPrixCepage.Location = new System.Drawing.Point(36, 114);
            this.optPrixCepage.Name = "optPrixCepage";
            this.optPrixCepage.Size = new System.Drawing.Size(14, 13);
            this.optPrixCepage.TabIndex = 2;
            this.optPrixCepage.TabStop = true;
            this.optPrixCepage.UseVisualStyleBackColor = true;
            this.optPrixCepage.CheckedChanged += new System.EventHandler(this.OptPrix_CheckedChanged);
            // 
            // lIE_LIEU_DE_PRODUCTIONTableAdapter
            // 
            this.lIE_LIEU_DE_PRODUCTIONTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.optPrixCepLieTablePrix);
            this.groupBox3.Controls.Add(this.optPrixCepLie);
            this.groupBox3.Controls.Add(this.gbCepage);
            this.groupBox3.Controls.Add(this.optPrixCepage);
            this.groupBox3.Controls.Add(this.gbPrixFixe);
            this.groupBox3.Controls.Add(this.optPrixFixe);
            this.groupBox3.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.groupBox3.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.groupBox3.Location = new System.Drawing.Point(12, 38);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(706, 218);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mode de paiement";
            // 
            // optPrixCepLieTablePrix
            // 
            this.optPrixCepLieTablePrix.AutoSize = true;
            this.optPrixCepLieTablePrix.ContextMenuStrip = this.contextBonus;
            this.optPrixCepLieTablePrix.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.optPrixCepLieTablePrix.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.optPrixCepLieTablePrix.Location = new System.Drawing.Point(36, 188);
            this.optPrixCepLieTablePrix.Name = "optPrixCepLieTablePrix";
            this.optPrixCepLieTablePrix.Size = new System.Drawing.Size(447, 20);
            this.optPrixCepLieTablePrix.TabIndex = 5;
            this.optPrixCepLieTablePrix.TabStop = true;
            this.optPrixCepLieTablePrix.Text = "    Prix par cépage, lieu de production et table de prix CIVCV ";
            this.optPrixCepLieTablePrix.UseVisualStyleBackColor = true;
            this.optPrixCepLieTablePrix.CheckedChanged += new System.EventHandler(this.OptPrix_CheckedChanged);
            // 
            // optPrixCepLie
            // 
            this.optPrixCepLie.AutoSize = true;
            this.optPrixCepLie.ContextMenuStrip = this.contextBonus;
            this.optPrixCepLie.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.optPrixCepLie.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.optPrixCepLie.Location = new System.Drawing.Point(36, 155);
            this.optPrixCepLie.Name = "optPrixCepLie";
            this.optPrixCepLie.Size = new System.Drawing.Size(299, 20);
            this.optPrixCepLie.TabIndex = 3;
            this.optPrixCepLie.TabStop = true;
            this.optPrixCepLie.Text = "    Prix par cépage et lieu de production";
            this.optPrixCepLie.UseVisualStyleBackColor = true;
            this.optPrixCepLie.CheckedChanged += new System.EventHandler(this.OptPrix_CheckedChanged);
            // 
            // gbCalcul
            // 
            this.gbCalcul.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCalcul.Controls.Add(this.label4);
            this.gbCalcul.Controls.Add(this.chkBonus);
            this.gbCalcul.Controls.Add(this.numPourcentage);
            this.gbCalcul.Controls.Add(this.label3);
            this.gbCalcul.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gbCalcul.Enabled = false;
            this.gbCalcul.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.gbCalcul.Location = new System.Drawing.Point(12, 261);
            this.gbCalcul.Name = "gbCalcul";
            this.gbCalcul.Size = new System.Drawing.Size(706, 96);
            this.gbCalcul.TabIndex = 1;
            this.gbCalcul.TabStop = false;
            this.gbCalcul.Text = "Pourcentage";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Bonus/Malus";
            // 
            // chkBonus
            // 
            this.chkBonus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBonus.AutoSize = true;
            this.chkBonus.Checked = true;
            this.chkBonus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBonus.Location = new System.Drawing.Point(396, 28);
            this.chkBonus.Name = "chkBonus";
            this.chkBonus.Size = new System.Drawing.Size(176, 20);
            this.chkBonus.TabIndex = 2;
            this.chkBonus.Text = "Selon degrés Oe/Briks";
            this.chkBonus.UseVisualStyleBackColor = true;
            // 
            // numPourcentage
            // 
            this.numPourcentage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numPourcentage.DecimalPlaces = 2;
            this.numPourcentage.Location = new System.Drawing.Point(396, 61);
            this.numPourcentage.Name = "numPourcentage";
            this.numPourcentage.Size = new System.Drawing.Size(258, 26);
            this.numPourcentage.TabIndex = 3;
            this.numPourcentage.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Pourcentage payé :";
            // 
            // mOCMODECOMPTABILISATIONBindingSource
            // 
            this.mOCMODECOMPTABILISATIONBindingSource.DataMember = "MOC_MODE_COMPTABILISATION";
            this.mOCMODECOMPTABILISATIONBindingSource.DataSource = this.dSPesees;
            // 
            // btnSuivant
            // 
            this.btnSuivant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSuivant.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnSuivant.Location = new System.Drawing.Point(562, 550);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(75, 30);
            this.btnSuivant.TabIndex = 2;
            this.btnSuivant.Text = "Suivant";
            this.btnSuivant.UseVisualStyleBackColor = false;
            this.btnSuivant.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.label5.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresForms;
            this.label5.Location = new System.Drawing.Point(22, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(304, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Gestion des modes de paiements";
            // 
            // btnPrecedent
            // 
            this.btnPrecedent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrecedent.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnPrecedent.Location = new System.Drawing.Point(481, 550);
            this.btnPrecedent.Name = "btnPrecedent";
            this.btnPrecedent.Size = new System.Drawing.Size(75, 30);
            this.btnPrecedent.TabIndex = 3;
            this.btnPrecedent.Text = "Précédent";
            this.btnPrecedent.UseVisualStyleBackColor = false;
            this.btnPrecedent.Click += new System.EventHandler(this.BtnPrecedent_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnnuler.BackColor = global::Gestion_Des_Vendanges.Properties.Settings.Default.Boutons;
            this.btnAnnuler.Location = new System.Drawing.Point(643, 550);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 30);
            this.btnAnnuler.TabIndex = 4;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.BtnAnnuler_Click);
            // 
            // mOC_MODE_COMPTABILISATIONTableAdapter
            // 
            this.mOC_MODE_COMPTABILISATIONTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbModeCompta);
            this.groupBox1.Controls.Add(this.optSelectModeCompta);
            this.groupBox1.Controls.Add(this.optDefaultModeCompta);
            this.groupBox1.Controls.Add(this.chkZoneTravail);
            this.groupBox1.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.groupBox1.Location = new System.Drawing.Point(12, 422);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(706, 120);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zone de travail";
            this.groupBox1.Visible = false;
            // 
            // cbModeCompta
            // 
            this.cbModeCompta.DataSource = this.mOCMODECOMPTABILISATIONBindingSource;
            this.cbModeCompta.DisplayMember = "MOC_TEXTE";
            this.cbModeCompta.Enabled = false;
            this.cbModeCompta.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.cbModeCompta.FormattingEnabled = true;
            this.cbModeCompta.Location = new System.Drawing.Point(396, 86);
            this.cbModeCompta.Name = "cbModeCompta";
            this.cbModeCompta.Size = new System.Drawing.Size(242, 21);
            this.cbModeCompta.TabIndex = 8;
            this.cbModeCompta.ValueMember = "MOC_ID";
            // 
            // optSelectModeCompta
            // 
            this.optSelectModeCompta.AutoSize = true;
            this.optSelectModeCompta.Enabled = false;
            this.optSelectModeCompta.Location = new System.Drawing.Point(111, 85);
            this.optSelectModeCompta.Name = "optSelectModeCompta";
            this.optSelectModeCompta.Size = new System.Drawing.Size(241, 20);
            this.optSelectModeCompta.TabIndex = 7;
            this.optSelectModeCompta.TabStop = true;
            this.optSelectModeCompta.Text = "Autre mode de comptabilisation";
            this.optSelectModeCompta.UseVisualStyleBackColor = true;
            // 
            // optDefaultModeCompta
            // 
            this.optDefaultModeCompta.AutoSize = true;
            this.optDefaultModeCompta.Enabled = false;
            this.optDefaultModeCompta.Location = new System.Drawing.Point(111, 55);
            this.optDefaultModeCompta.Name = "optDefaultModeCompta";
            this.optDefaultModeCompta.Size = new System.Drawing.Size(275, 20);
            this.optDefaultModeCompta.TabIndex = 6;
            this.optDefaultModeCompta.TabStop = true;
            this.optDefaultModeCompta.Text = "Mode de comptabilisation par défaut";
            this.optDefaultModeCompta.UseVisualStyleBackColor = true;
            this.optDefaultModeCompta.CheckedChanged += new System.EventHandler(this.ChkZoneTravail_CheckedChanged);
            // 
            // chkZoneTravail
            // 
            this.chkZoneTravail.AutoSize = true;
            this.chkZoneTravail.Location = new System.Drawing.Point(77, 26);
            this.chkZoneTravail.Name = "chkZoneTravail";
            this.chkZoneTravail.Size = new System.Drawing.Size(278, 20);
            this.chkZoneTravail.TabIndex = 5;
            this.chkZoneTravail.Text = "Prendre en compte la zone de travail";
            this.chkZoneTravail.UseVisualStyleBackColor = true;
            this.chkZoneTravail.CheckedChanged += new System.EventHandler(this.ChkZoneTravail_CheckedChanged);
            // 
            // grbPaiementLitresOptions
            // 
            this.grbPaiementLitresOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbPaiementLitresOptions.Controls.Add(this.chkLitreMoutRaisin);
            this.grbPaiementLitresOptions.Controls.Add(this.lblLitreMoutRaisin);
            this.grbPaiementLitresOptions.DataBindings.Add(new System.Windows.Forms.Binding("Font", global::Gestion_Des_Vendanges.Properties.Settings.Default, "TitresLabels", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.grbPaiementLitresOptions.Enabled = false;
            this.grbPaiementLitresOptions.Font = global::Gestion_Des_Vendanges.Properties.Settings.Default.TitresLabels;
            this.grbPaiementLitresOptions.Location = new System.Drawing.Point(12, 363);
            this.grbPaiementLitresOptions.Name = "grbPaiementLitresOptions";
            this.grbPaiementLitresOptions.Size = new System.Drawing.Size(706, 54);
            this.grbPaiementLitresOptions.TabIndex = 6;
            this.grbPaiementLitresOptions.TabStop = false;
            this.grbPaiementLitresOptions.Text = "Paiement en litres";
            // 
            // chkLitreMoutRaisin
            // 
            this.chkLitreMoutRaisin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkLitreMoutRaisin.AutoSize = true;
            this.chkLitreMoutRaisin.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLitreMoutRaisin.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.chkLitreMoutRaisin.Location = new System.Drawing.Point(396, 24);
            this.chkLitreMoutRaisin.Name = "chkLitreMoutRaisin";
            this.chkLitreMoutRaisin.Size = new System.Drawing.Size(274, 20);
            this.chkLitreMoutRaisin.TabIndex = 5;
            this.chkLitreMoutRaisin.Text = "Moût de raisin (par défaut vins clair)";
            this.chkLitreMoutRaisin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkLitreMoutRaisin.UseVisualStyleBackColor = true;
            // 
            // lblLitreMoutRaisin
            // 
            this.lblLitreMoutRaisin.AutoSize = true;
            this.lblLitreMoutRaisin.Location = new System.Drawing.Point(74, 25);
            this.lblLitreMoutRaisin.Name = "lblLitreMoutRaisin";
            this.lblLitreMoutRaisin.Size = new System.Drawing.Size(268, 16);
            this.lblLitreMoutRaisin.TabIndex = 4;
            this.lblLitreMoutRaisin.Text = "Base de calcul du paiement en litres  :";
            // 
            // WfrmAjoutPaiementMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 591);
            this.Controls.Add(this.grbPaiementLitresOptions);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnPrecedent);
            this.Controls.Add(this.btnSuivant);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.gbCalcul);
            this.Controls.Add(this.groupBox3);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(746, 650);
            this.MinimumSize = new System.Drawing.Size(746, 477);
            this.Name = "WfrmAjoutPaiementMode";
            this.Text = "Gestion des paiements - Mode de paiement";
            this.Load += new System.EventHandler(this.WfrmAjoutPaiementMode_Load);
            this.gbPrixFixe.ResumeLayout(false);
            this.gbPrixFixe.PerformLayout();
            this.gbCepage.ResumeLayout(false);
            this.gbCepage.PerformLayout();
            this.contextBonus.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lIELIEUDEPRODUCTIONBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSPesees)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbCalcul.ResumeLayout(false);
            this.gbCalcul.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPourcentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mOCMODECOMPTABILISATIONBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbPaiementLitresOptions.ResumeLayout(false);
            this.grbPaiementLitresOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton optPrixFixe;
        private System.Windows.Forms.GroupBox gbPrixFixe;
        private System.Windows.Forms.Label lPrixFixe;
        private System.Windows.Forms.TextBox txtPrixFixe;
        private System.Windows.Forms.GroupBox gbCepage;
        private System.Windows.Forms.ComboBox cbLieuProduction;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton optPrixCepage;
        private Gestion_Des_Vendanges.DAO.DSPesees dSPesees;
        private System.Windows.Forms.BindingSource lIELIEUDEPRODUCTIONBindingSource;
        private Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters.LIE_LIEU_DE_PRODUCTIONTableAdapter lIE_LIEU_DE_PRODUCTIONTableAdapter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton optPrixCepLie;
        private System.Windows.Forms.GroupBox gbCalcul;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numPourcentage;
        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.CheckBox chkBonus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ContextMenuStrip contextBonus;
        private System.Windows.Forms.ToolStripMenuItem accéderAuxTablesDePrixToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPrecedent;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.RadioButton optPrixCepLieTablePrix;
        private System.Windows.Forms.BindingSource mOCMODECOMPTABILISATIONBindingSource;
        private DAO.DSPeseesTableAdapters.MOC_MODE_COMPTABILISATIONTableAdapter mOC_MODE_COMPTABILISATIONTableAdapter;
        //private GV_KeyManager.GVKeyManagerDataSetTableAdapters.CLI_CLIENTTableAdapter clI_CLIENTTableAdapter1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbModeCompta;
        private System.Windows.Forms.RadioButton optSelectModeCompta;
        private System.Windows.Forms.RadioButton optDefaultModeCompta;
        private System.Windows.Forms.CheckBox chkZoneTravail;
        private System.Windows.Forms.GroupBox grbPaiementLitresOptions;
        private System.Windows.Forms.CheckBox chkLitreMoutRaisin;
        private System.Windows.Forms.Label lblLitreMoutRaisin;
    }
}