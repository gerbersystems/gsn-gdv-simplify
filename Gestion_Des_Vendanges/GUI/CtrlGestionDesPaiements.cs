﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.Services;
using GSN.GDV.Data.Extensions;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.GUI
{
    /* *
    *  Description: Formulaire de gestion des paiements
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class CtrlGestionDesPaiements : CtrlWfrm, ICtrlSetting
    {
        #region Properties

        public bool CanDelete => true;
        public override ToolStrip ToolStrip => toolStrip1;
        public override Panel OptionPanel => panel1;

        #endregion Properties

        #region Constructors

        private CtrlGestionDesPaiements()
        {
            InitializeComponent();
        }

        public static CtrlGestionDesPaiements GetInstance()
        {
            if (Utilities.GetOption(1))
            {
                return new CtrlGestionDesPaiements();
            }
            else
            {
                MessageBox.Show("Vous n'avez pas la licence pour utiliser cette option.\n" +
                                "Cette option est uniquement disponible avec la version étendue.",
                                "Licence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private void WfrmGestionDesPaiements_Load(object sender, EventArgs e)
        {
            ConnectionManager.Instance.AddGvTableAdapter(pRO_NOMCOMPLETTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(aNN_ANNEETableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(pAI_PAIEMENTTableAdapter);
            ConnectionManager.Instance.AddGvTableAdapter(tableAdapterManager);
            UpdateDonnees();
        }

        #endregion Constructors

        #region Methods

        private void PAI_PAIEMENTBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            pAI_PAIEMENTBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(dSPesees);
        }

        // INFO | #paiements - 1. Bouton ajouter paiement
        private void BtnAddPmt_Click(object sender, EventArgs e)
        {
            try
            {
                new PaiementManager().AjoutPaiement();
                UpdateDonnees();
            }
            catch (ModeComptabilisationNotSetGVException)
            {
                MessageBox.Show("Le mode de comptablisation n'est pas défini dans les paramètres de l'année.\n" +
                                "Veuillez le définir avant de créer les paiements.",
                                "Gestion des vendanges", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BtnDeletePmt_Click(object sender, EventArgs e)
        {
            try
            {
                string message = (pAI_PAIEMENTDataGridView.SelectedRows[0].Cells["PAI_ID_WINBIZ"].Value == null) ?
                                 "Etes vous sûr de vouloir supprimer ce paiement?" :
                                 "Ce paiement a été exporté.\n" +
                                 "En cas de suppression, il ne sera pas effacer de l'erp.\n\n" +
                                 "Etes-vous sûr de vouloir supprimer ce paiement?";

                DialogResult result = MessageBox.Show(message,
                                                      "Suppression d'un paiement",
                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (result == DialogResult.No) return;

                pAI_PAIEMENTBindingSource.RemoveCurrent();
                Validate();
                pAI_PAIEMENTBindingSource.EndEdit();
                tableAdapterManager.UpdateAll(dSPesees);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        public override void UpdateDonnees()
        {
            pRO_NOMCOMPLETTableAdapter.Fill(dSPesees.PRO_NOMCOMPLET);
            aNN_ANNEETableAdapter.Fill(dSPesees.ANN_ANNEE);
            cbAnnee.SelectedValue = Utilities.IdAnneeCourante;
            pAI_PAIEMENTTableAdapter.FillByAnnId(dSPesees.PAI_PAIEMENT, Utilities.IdAnneeCourante);
        }

        private void BtnModifierLIM_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = DialogResult.OK;
                if (pAI_PAIEMENTDataGridView.CurrentRow.Cells["PAI_ID_WINBIZ"].Value != null)
                {
                    result = MessageBox.Show("Attention ce paiement a été exporté.\n" +
                                             "Supprimez le document de WinBIZ et relancez l'exportation pour appliquer les modifications.",
                                             "Paiement exporté", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
                if (result == DialogResult.OK)
                {
                    int paiId = (int)pAI_PAIEMENTDataGridView.CurrentRow.Cells["PAI_ID"].Value;
                    WfrmGestionDesLignesManuelles.GetWfrm(paiId).WfrmShowDialog();
                }
            }
            catch (NullReferenceException) { }
        }

        private async void BtnExportToErp_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utilities.GetOption(2))
                {
                    bool result;
                    switch (GlobalParam.Instance.Erp)
                    {
                        case SettingExtension.ErpEnum.Winbiz:
                            result = new PaiementExport().Execute();

                            break;

                        case SettingExtension.ErpEnum.XpertFIN:
                            result = await new XpertFINService().ExportInvoices();
                            break;

                        default:
                            throw new ArgumentException();
                    }
                    //INFO | #exportWinbiz - 1. Call : BUSINESS.PaiementExport().Execute()
                    if (result)
                        MessageBox.Show("Exportation terminée avec succès!",
                                        "Exportation réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Vous n'avez pas la license pour utiliser cette option.\n" +
                                    "Commander l'option \"Export WinBiz\" pour pouvoir exporter vos paiements.",
                                    "License", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (ModelAdresseWinBIZNotFoundGVException ex)
            {
                MessageBox.Show($"Erreur, le model d'adresse '{ex.Model}' n'est pas défini dans WinBIZ.\n" +
                                "Veuillez le créer puis relancer l'exportation.",
                                "Erreur importation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CbAnnee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAnnee.SelectedValue != null &&
                Convert.ToInt32(cbAnnee.SelectedValue) != Utilities.IdAnneeCourante)
            {
                Utilities.IdAnneeCourante = Convert.ToInt32(cbAnnee.SelectedValue);
                UpdateDonnees();
            }
        }

        private void ToolWinBIZFolder_Click(object sender, EventArgs e)
        {
            new WfrmWinBizFolder().WfrmShowDialog();
        }

        private void PAI_PAIEMENTDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            bool isSorted = true;
            int idAn = Utilities.IdAnneeCourante;
            switch (e.ColumnIndex)
            {
                case 7:
                    pAI_PAIEMENTBindingSource.RemoveSort();
                    pAI_PAIEMENTTableAdapter.FillOrderByPro(dSPesees.PAI_PAIEMENT, Utilities.IdAnneeCourante);
                    break;

                default:
                    isSorted = false;
                    break;
            }
            if (isSorted)
            {
                foreach (DataGridViewColumn column in pAI_PAIEMENTDataGridView.Columns)
                    column.HeaderCell.SortGlyphDirection = SortOrder.None;

                pAI_PAIEMENTDataGridView.Columns[e.ColumnIndex]
                                        .HeaderCell
                                        .SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void FactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pAI_PAIEMENTDataGridView.SelectedRows.Count <= 0) return;

            try
            {
                switch (GlobalParam.Instance.FactureType)
                {
                    case SettingExtension.FactureTypeEnum.Simple:
                        CtrlReportViewer.ShowPaiement((int)pAI_PAIEMENTDataGridView.SelectedRows[0]
                                                                                   .Cells["PAI_ID"].Value);
                        break;

                    case SettingExtension.FactureTypeEnum.LitresMout:
                        CtrlReportViewer.ShowPaiementFacture((int)pAI_PAIEMENTDataGridView.SelectedRows[0]
                                                                                          .Cells["PAI_ID"].Value);
                        break;

                    case SettingExtension.FactureTypeEnum.AvecBonus:
                        CtrlReportViewer.ShowPaiementBonus((int)pAI_PAIEMENTDataGridView.SelectedRows[0]
                                                                                        .Cells["PAI_ID"].Value);
                        break;

                    case SettingExtension.FactureTypeEnum.AvecBonusV2:
                        CtrlReportViewer.ShowPaiementBonusV2((int)pAI_PAIEMENTDataGridView.SelectedRows[0]
                                                                                          .Cells["PAI_ID"].Value);
                        break;

                    default:
                        throw new ArgumentException();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erreur lors de la création du rapport.\n{ex.Message}",
                                "Erreur rapport", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void FactureDetailBonusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (pAI_PAIEMENTDataGridView.SelectedRows.Count <= 0) return;

                CtrlReportViewer.ShowPaiementDetailBonus((int)pAI_PAIEMENTDataGridView.SelectedRows[0]
                                                                                      .Cells["PAI_ID"].Value);
            }
            catch { }
        }

        private void FactureRapportToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                CtrlReportViewer.ShowPaiementRapport();
            }
            catch { }
        }

        #endregion Methods
    }
}