﻿using System;

namespace Gestion_Des_Vendanges.GUI
{
    public partial class WfrmGroupageArticle : WfrmBase
    {
        private DAO.DSPeseesTableAdapters.ASS_ASSOCIATION_ARTICLETableAdapter _assTable;

        public WfrmGroupageArticle()
        {
            InitializeComponent();
            _assTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.ASS_ASSOCIATION_ARTICLETableAdapter>();
            initCheckBox();
        }

        private void initCheckBox()
        {
            chkAut.Checked = (bool)_assTable.GetSelection("AUT_ID");
            chkCla.Checked = (bool)_assTable.GetSelection("CLA_ID");
            chkCom.Checked = (bool)_assTable.GetSelection("COM_ID");
            chkLie.Checked = (bool)_assTable.GetSelection("LIE_ID");
            chkPro.Checked = (bool)_assTable.GetSelection("PRO_ID_VENDANGE");
            optCep.Checked = (bool)_assTable.GetSelection("CEP_ID");
            SetCouChecked();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _assTable.SetSelection(chkCom.Checked, "COM_ID");
            _assTable.SetSelection(chkLie.Checked, "LIE_ID");
            _assTable.SetSelection(chkAut.Checked, "AUT_ID");
            _assTable.SetSelection(chkCla.Checked, "CLA_ID");
            _assTable.SetSelection(chkPro.Checked, "PRO_ID_VENDANGE");
            _assTable.SetSelection(optCep.Checked, "CEP_ID");
            SetSelectionSpecific();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}