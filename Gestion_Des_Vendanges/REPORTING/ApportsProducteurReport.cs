﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ApportsProducteurReport : ReportInfo
    {
        public ApportsProducteurReport(FormatReport format, int annId, int peeId = 0)
            : base(format, "ApportsProducteur")
        {
            DAO.DSPeseesTableAdapters.R_ApportsProducteurTableAdapter quiTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_ApportsProducteurTableAdapter>();
            if (peeId == 0)
            {
                quiTable.FillByAnn(DsPesees.R_ApportsProducteur, annId);
                AddDataSource("ViewQuittance", (DataTable)quiTable.GetDataByAnn(annId));
            }
            else
            {
                quiTable.FillByAnnPee(DsPesees.R_ApportsProducteur, annId, peeId);
                AddDataSource("ViewQuittance", (DataTable)quiTable.GetDataByAnnPee(annId, peeId));
            }
        }
    }
}