﻿using GSN.GDV.Reports;
using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ReportDroitsProduction : Report
    {
        protected override byte _numberLicenceLevel
        {
            get { return 0; }
        }

        public override bool isRDLC
        {
            get { return false; }
        }

        public override bool needProducteurFilter
        {
            get { return false; }
        }

        public override bool needDateFilter
        {
            get { return false; }
        }

        public override bool isReportAutoLoaded
        {
            get { return true; }
        }

        public override IReportInfo GetReportInfo(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
        {
            return new DroitsProductionReport(format: format, annId: annId); ;
        }
    }
}