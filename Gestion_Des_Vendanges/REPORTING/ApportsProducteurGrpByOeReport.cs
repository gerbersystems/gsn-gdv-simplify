﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ApportsProducteurGrpByOeReport : ReportInfo
    {
        public ApportsProducteurGrpByOeReport(FormatReport format, int annId, int peeId = 0)
            : base(format, "ApportsProducteurGrpByOe")
        {
            DAO.DSPeseesTableAdapters.R_ApportsProducteurGrpByOeTableAdapter quiTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_ApportsProducteurGrpByOeTableAdapter>();
            //quiTable.Connection.ConnectionString = DAO.ConnectionManager.Instance._gvConnectionString;
            if (peeId == 0)
            {
                quiTable.FillByAnn(DsPesees.R_ApportsProducteurGrpByOe, annId);
                AddDataSource("ViewQuittance", (DataTable)quiTable.GetDataByAnn(annId));
            }
            else
            {
                quiTable.FillByAnnPee(DsPesees.R_ApportsProducteurGrpByOe, annId, peeId);
                AddDataSource("ViewQuittance", (DataTable)quiTable.GetDataAnnPee(annId, peeId));
            }
        }
    }
}