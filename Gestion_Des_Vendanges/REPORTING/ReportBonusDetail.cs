﻿using Gestion_Des_Vendanges_Reports.REPORTING;
using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ReportBonusDetail : Report
    {
        protected override byte _numberLicenceLevel
        {
            get { return 1; }
        }

        public override bool isRDLC
        {
            get { return true; }
        }

        public override bool needProducteurFilter
        {
            get { return true; }
        }

        public override bool needDateFilter
        {
            get { return true; }
        }

        public override bool isReportAutoLoaded
        {
            get { return false; }
        }

        public override GSN.GDV.Reports.IReportInfo GetReportInfo(GSN.GDV.Reports.FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
        {
            return new R_PaiementDetailBonusReport(format: format, annId: annId, proId: proId, dateRangeFrom: dateRangeFrom, dateRangeTo: dateRangeTo);
        }
    }
}