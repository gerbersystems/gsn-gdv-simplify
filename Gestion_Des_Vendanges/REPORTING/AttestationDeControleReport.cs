﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class AttestationDeControleReport : ReportInfo
    {
        public AttestationDeControleReport(FormatReport format, int annId = 0, int peeId = 0)
            : base(format, "AttestationsDeControle")
        {
            DAO.DSPeseesTableAdapters.R_AttestationDeControleTableAdapter _attTable =
                DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_AttestationDeControleTableAdapter>();
            if (peeId > 0)
            {
                _attTable.FillByPee(new DAO.DSPesees().R_AttestationDeControle, peeId);
                AddDataSource("Pesee", (DataTable)_attTable.GetDataByPee(peeId));
            }
            else
            {
                if (annId == 0)
                {
                    annId = BUSINESS.Utilities.IdAnneeCourante;
                }
                _attTable.FillAnn(new DAO.DSPesees().R_AttestationDeControle, annId);
                AddDataSource("Pesee", (DataTable)_attTable.GetDataAnn(annId));
            }
        }
    }
}