﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class SondageMoyenReport : ReportInfo
    {
        public SondageMoyenReport(FormatReport format, int annId = 0)
            : base(format, "SondageMoyen")
        {
            DAO.DSPeseesTableAdapters.R_SondageMoyenTableAdapter _sonTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_SondageMoyenTableAdapter>();
            DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter _encTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter>();
            if (annId == 0)
            {
                annId = BUSINESS.Utilities.IdAnneeCourante;
            }
            _sonTable.FillAnn(DsPesees.R_SondageMoyen, annId);
            _encTable.FillByAn(DsPesees.R_Encaveur, annId);
            AddDataSource("OEMoyen", (DataTable)_sonTable.GetDataAnn(annId));
            AddDataSource("Encaveur", (DataTable)_encTable.GetDataByAn(annId));
        }
    }
}