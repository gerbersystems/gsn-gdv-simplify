﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class DeclarationEncavageReport : ReportInfo
    {
        public DeclarationEncavageReport(FormatReport format, int annId)
            : base(format, "DeclarationEncavage")
        {
            DAO.DSPeseesTableAdapters.R_DeclarationEncavageTableAdapter decTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_DeclarationEncavageTableAdapter>(); ;
            DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter encTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter>();
            DAO.DSPeseesTableAdapters.R_PressoirTableAdapter preTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_PressoirTableAdapter>();
            DAO.DSPeseesTableAdapters.R_DeclarationEncavageRecapTableAdapter derTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_DeclarationEncavageRecapTableAdapter>();

            decTable.FillByAn(DsPesees.R_DeclarationEncavage, annId);
            encTable.FillByAn(DsPesees.R_Encaveur, annId);
            preTable.Fill(DsPesees.R_Pressoir);
            derTable.FillByAn(DsPesees.R_DeclarationEncavageRecap, annId);
            AddDataSource("Encaveur", encTable.GetDataByAn(annId));
            AddDataSource("TableaDeclarationEncavage", decTable.GetDataByAn(annId));
            AddDataSource("Pressoirs", preTable.GetData());
            AddSubReportDataSource("ViewRecap", derTable.GetDataAn(annId));
        }
    }
}