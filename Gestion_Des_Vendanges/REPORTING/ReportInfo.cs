﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges_Reports.DAO;
using GSN.GDV.Reports;
using Microsoft.Reporting.WinForms;
using System.Collections.Generic;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    public abstract class ReportInfo : IReportInfo
    {
        private List<ReportDataSource> _dataSources;
        private List<ReportDataSource> _subReportDataSources;
        private FormatReport _format;
        private string _name;
        private DSPesees _dsPessees;
        private DSReports _dsReports;

        protected DSPesees DsPesees
        {
            get { return _dsPessees; }
        }

        protected DSReports DsReports
        {
            get { return _dsReports; }
        }

        public string Name
        {
            get { return _name; }
        }

        public FormatReport Format
        {
            get { return _format; }
        }

        public List<ReportDataSource> DataSources
        {
            get { return _dataSources; }
        }

        public List<ReportDataSource> SubReportDataSources
        {
            get { return _subReportDataSources; }
        }

        public ReportInfo(FormatReport format, string name)
        {
            _dsPessees = new DSPesees();
            _format = format;
            _name = name;
            _dataSources = new List<ReportDataSource>();
            _subReportDataSources = new List<ReportDataSource>();
        }

        public ReportInfo(FormatReport format, string name, bool isDSReport)
        {
            if (isDSReport == true)
                _dsReports = new DSReports();
            else
                _dsPessees = new DSPesees();

            _format = format;
            _name = name;
            _dataSources = new List<ReportDataSource>();
            _subReportDataSources = new List<ReportDataSource>();
        }

        protected void AddDataSource(string name, DataTable table)
        {
            _dataSources.Add(new ReportDataSource(name, table));
        }

        protected void AddSubReportDataSource(string name, DataTable table)
        {
            _subReportDataSources.Add(new ReportDataSource(name, table));
        }
    }
}