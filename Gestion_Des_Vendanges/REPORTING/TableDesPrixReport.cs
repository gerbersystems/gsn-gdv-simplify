﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class TableDesPrixReport : ReportInfo
    {
        public TableDesPrixReport(FormatReport format, int annId, int lieId = 0)
            : base(format, "TableDesPrix")
        {
            DAO.DSPeseesTableAdapters.R_TablePrixTableAdapter tabTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_TablePrixTableAdapter>();
            if (lieId > 0)
            {
                tabTable.FillByAnLie(DsPesees.R_TablePrix, annId, lieId);
                AddDataSource("Donnees", (DataTable)tabTable.GetDataByAnLie(annId, lieId));
            }
            else
            {
                tabTable.FillByAn(DsPesees.R_TablePrix, annId);
                AddDataSource("Donnees", (DataTable)tabTable.GetDataByAn(annId));
            }
        }
    }
}