﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class PaiementAvecBonusReport : ReportInfo
    {
        public PaiementAvecBonusReport(FormatReport format, int annId = 0, int paiId = 0, int proId = 0)
            : base(format, "PaiementAvecBonus")
        {
            DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter encTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter>();
            DAO.DSPeseesTableAdapters.R_PaiementTableAdapter paiTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_PaiementTableAdapter>();

            if (paiId > 0)
            {
                annId = BUSINESS.Utilities.IdAnneeCourante;
                paiTable.FillByPai(DsPesees.R_Paiement, paiId);
                AddDataSource("PaiementAvecBonus", (DataTable)paiTable.GetDataByPai(paiId));
            }
            else
            {
                if (annId <= 0)
                {
                    annId = BUSINESS.Utilities.IdAnneeCourante;
                }
                if (proId > 0)
                {
                    paiTable.FillByAnPro(DsPesees.R_Paiement, annId, proId);
                    AddDataSource("PaiementAvecBonus", (DataTable)paiTable.GetDataByAnPro(annId, proId));
                }
                else
                {
                    paiTable.FillByAn(DsPesees.R_Paiement, annId);
                    AddDataSource("PaiementAvecBonus", (DataTable)paiTable.GetDataByAn(annId));
                }
            }
            //encTable.FillByAn(DsPesees.R_Encaveur, annId);
            //AddDataSource("Encaveur", (DataTable)encTable.GetDataByAn(annId));
        }
    }
}