﻿using Gestion_Des_Vendanges.BUSINESS;
using GSN.GDV.Data.Extensions;
using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ReportFactory
    {
        private static Report _currentSelectedReport;

        static public Report Create(string selectedRadioButton)
        {
            switch (selectedRadioButton)
            {
                case "optListeAcquit":
                    _currentSelectedReport = new ReportListeAcquits();
                    break;

                case "optDroitsProduction":
                    _currentSelectedReport = new ReportDroitsDeProduction();
                    break;

                case "optDeclarationEncavage":
                    _currentSelectedReport = new ReportDeclarationEncavage();
                    break;

                case "optAttestationControle":
                    _currentSelectedReport = new ReportAttestationControle();
                    break;

                case "optAttestationControleAvecSolde":
                    _currentSelectedReport = new ReportAttestationControleAvecSolde();
                    break;

                case "optAttestationControleAvecSoldeParAcquit":
                    _currentSelectedReport = new ReportAttestationControleAvecSoldeParAcquit();
                    break;

                case "optPaiements":
                    switch (GlobalParam.Instance.FactureType)
                    {
                        case SettingExtension.FactureTypeEnum.Simple:
                            _currentSelectedReport = new ReportPaiement();
                            break;

                        case SettingExtension.FactureTypeEnum.LitresMout:
                            _currentSelectedReport = new ReportPaiementFacture();
                            break;

                        case SettingExtension.FactureTypeEnum.AvecBonus:
                            _currentSelectedReport = new ReportPaiementsLigneGroupeParCepageLieuProduction();
                            break;

                        case SettingExtension.FactureTypeEnum.AvecBonusV2:
                            _currentSelectedReport = new ReportPaiementsLigneGroupeParCepageLieuProductionV2();
                            break;

                        default:
                            throw new ArgumentException();
                    }
                    break;

                case "optBonusDetail":
                    _currentSelectedReport = new ReportBonusDetail();
                    break;

                case "optTablesPrix":
                    _currentSelectedReport = new ReportTablesPrix();
                    break;

                case "optListeParcelles":
                    _currentSelectedReport = new ReportListeParcellesVisiteVigne();
                    break;

                case "optApportsProducteur":
                    _currentSelectedReport = new ReportApportsProducteur();
                    break;

                case "optApportsProducteurMout":
                    _currentSelectedReport = new ReportApportsProducteurMout();
                    break;

                case "optAttestationsFiscales":
                    _currentSelectedReport = new ReportAttestationsFiscales();
                    break;
            }
            return _currentSelectedReport ?? new ReportListeAcquits();
        }
    }
}