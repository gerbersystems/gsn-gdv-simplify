﻿using GSN.GDV.Reports;
using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal abstract class Report
    {
        protected abstract byte _numberLicenceLevel { get; }
        public abstract bool isRDLC { get; }
        public abstract bool needProducteurFilter { get; }
        public abstract bool needDateFilter { get; }
        public abstract bool isReportAutoLoaded { get; }

        public abstract IReportInfo GetReportInfo(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null);

        public bool haveProperLicenceLevel()
        {
            if (_numberLicenceLevel.Equals(0) || BUSINESS.Utilities.GetOption(_numberLicenceLevel))
                return true;
            else
                throw new AccessViolationException();
        }
    }
}