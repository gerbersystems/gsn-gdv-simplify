﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class DroitsProductionReport : ReportInfo
    {
        public DroitsProductionReport(FormatReport format, int annId)
            : base(format, "DroitsProduction")
        {
            DAO.DSPeseesTableAdapters.R_DroitsProductionTableAdapter droTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_DroitsProductionTableAdapter>();
            DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter encTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_EncaveurTableAdapter>();
            droTable.FillByAn(DsPesees.R_DroitsProduction, annId);
            encTable.FillByAn(DsPesees.R_Encaveur, annId);
            AddDataSource("Encaveur", (DataTable)encTable.GetDataByAn(annId));
            AddDataSource("Donnees", (DataTable)droTable.GetDataByAn(annId));
        }
    }
}