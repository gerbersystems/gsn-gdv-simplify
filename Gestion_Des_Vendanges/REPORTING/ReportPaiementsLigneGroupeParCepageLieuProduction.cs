﻿using Gestion_Des_Vendanges_Reports.REPORTING;
using GSN.GDV.Reports;
using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ReportPaiementsLigneGroupeParCepageLieuProduction : Report
    {
        protected override byte _numberLicenceLevel
        {
            get { return 1; }
        }

        public override bool isRDLC
        {
            get { return true; }
        }

        public override bool needProducteurFilter
        {
            get { return true; }
        }

        public override bool needDateFilter
        {
            get { return true; }
        }

        public override bool isReportAutoLoaded
        {
            get { return false; }
        }

        public override IReportInfo GetReportInfo(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
        {
            return new R_PaiementReport(format: FormatReport.Apercu, annId: annId, proId: proId, dateRangeFrom: dateRangeFrom, dateRangeTo: dateRangeTo);
        }
    }
}