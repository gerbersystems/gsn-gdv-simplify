﻿using System;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ReportAttestationControle : Report
    {
        protected override byte _numberLicenceLevel
        {
            get { return 1; }
        }

        public override bool isRDLC
        {
            get { return false; }
        }

        public override bool needProducteurFilter
        {
            get { return false; }
        }

        public override bool needDateFilter
        {
            get { return false; }
        }

        public override bool isReportAutoLoaded
        {
            get { return true; }
        }

        public override GSN.GDV.Reports.IReportInfo GetReportInfo(GSN.GDV.Reports.FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
        {
            return new AttestationDeControleReport(format: format, annId: annId);
        }
    }
}