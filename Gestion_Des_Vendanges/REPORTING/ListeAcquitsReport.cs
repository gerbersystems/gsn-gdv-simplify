﻿using Gestion_Des_Vendanges.DAO;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using GSN.GDV.Reports;
using System;
using System.Windows;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class ListeAcquitsReport : ReportInfo
    {
        private log4net.ILog _log = log4net.LogManager.GetLogger(typeof(ListeAcquitsReport));
        private const string REPORT_NAME = "ListeAcquits";

        public ListeAcquitsReport(FormatReport format, int annId)
            : base(format, REPORT_NAME)
        {
            try
            {
                AddDataSource("Acquits", ConnectionManager.Instance
                                             .CreateGvTableAdapter<R_AcquitsTableAdapter>()
                                             .GetDataByAnnId(annId));
            }
            catch (Exception ex)
            {
                LogAndInform("Acquits", ex);
            }

            try
            {
                AddDataSource("Encaveur", ConnectionManager.Instance
                                              .CreateGvTableAdapter<R_EncaveurTableAdapter>()
                                              .GetDataByAn(annId));
            }
            catch (Exception ex)
            {
                LogAndInform("Encaveur", ex);
            }
        }

        private void LogAndInform(string who, Exception ex)
        {
            _log.Warn(ex.ToString(), ex);
            MessageBox.Show($"[ {REPORT_NAME}:{who} ]{Environment.NewLine}{ex.ToString()}", $"Report error: {REPORT_NAME}",
                            MessageBoxButton.OK,
                            MessageBoxImage.Warning);
        }
    }
}