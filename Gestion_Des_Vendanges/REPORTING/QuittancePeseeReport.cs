﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges.REPORTING
{
    internal class QuittancePeseeReport : ReportInfo
    {
        public QuittancePeseeReport(FormatReport format, int peeId)
            : base(format, "QuittancePesee")
        {
            DAO.DSPeseesTableAdapters.R_QuittancePeseeTableAdapter quiTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<DAO.DSPeseesTableAdapters.R_QuittancePeseeTableAdapter>();
            quiTable.FillByPee(DsPesees.R_QuittancePesee, peeId);
            AddDataSource("ViewQuittance", (DataTable)quiTable.GetDataByPee(peeId));
        }
    }
}