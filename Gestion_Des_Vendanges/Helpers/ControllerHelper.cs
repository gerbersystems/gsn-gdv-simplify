﻿using GSN.GDV.Data.Contextes;

namespace GSN.GDV.GUI.Controllers
{
    public class ControllerHelper : IControllerHelper
    {
        public static IControllerHelper Create()
        {
            return new ControllerHelper();
        }

        GSN.GDV.Business.IConnectionManager IControllerHelper.GetConnectionManager()
        {
            return Gestion_Des_Vendanges.DAO.ConnectionManager.Instance;
        }

        GSN.GDV.Business.IGlobalParameterProvider IControllerHelper.GetGlobalParameterProvider()
        {
            return Gestion_Des_Vendanges.BUSINESS.GlobalParam.Instance;
        }

        IMainDataContext IControllerHelper.CreateMainDataContext()
        {
            IControllerHelper me = this;
            return me.GetConnectionManager().CreateMainDataContext();
        }
    }
}