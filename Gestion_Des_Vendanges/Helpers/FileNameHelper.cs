﻿using System.IO;

namespace Gestion_Des_Vendanges.Helpers
{
    internal static class FileNameHelper
    {
        internal static string GetUniqueFullName(string fileFullName)
        {
            while (File.Exists(fileFullName))
                fileFullName = fileFullName.AppendToFileName("_NEW");

            return fileFullName;
        }
    }
}