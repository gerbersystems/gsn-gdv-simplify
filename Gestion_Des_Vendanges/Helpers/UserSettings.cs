namespace Gestion_Des_Vendanges.Helpers
{
    public class UserSettings
    {
        // 1 = superuser = full rights
        public int Rights { get; set; } = 1;
        //Default RefractometerNb in weighing attestation de contr�le
        public string RefractometerNb { get; set; } = string.Empty;
        //Default ScaleNb in weighing attestation de contr�le
        public string ScaleNb { get; set; } = string.Empty;
        //Default PlaceOfDelivery in weighing attestation de contr�le
        public string PlaceOfDelivery { get; set; } = string.Empty;
        //Default ResponsableId in weighing attestation de contr�le
        public string ResponsableId { get; set; } = "-1";
    }
}