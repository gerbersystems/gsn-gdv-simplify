﻿using System.IO;

namespace Gestion_Des_Vendanges.Helpers
{
    internal static class Extensions
    {
        public static string AppendToFileName(this string fileFullName, string characterToAppend)
        {
            return Path.Combine(Path.GetDirectoryName(fileFullName),
                                string.Concat(Path.GetFileNameWithoutExtension(fileFullName)
                                            , characterToAppend
                                            , Path.GetExtension(fileFullName)));
        }

        public static string Truncate(this string value, int maxLength = 50)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }
}