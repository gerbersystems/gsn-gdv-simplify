﻿namespace Gestion_Des_Vendanges.Helpers
{
    using Newtonsoft.Json;
    using System;
    using System.IO;

    public class SettingsService
    {
        private static string UserSettingsFileName = "userSettings.json";
        private static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\GSN\GestionDesVendanges\Settings\";
        public static UserSettings UserSettings { get; } = UserSettings ?? GetSettings();

        /// <summary>
        /// Reads the config file and sets the static variable to provide user's settings to all project
        /// </summary>
        private static UserSettings GetSettings()
        {
            try
            {
                return JsonConvert.DeserializeObject<UserSettings>(File.ReadAllText(Path + "\\" + UserSettingsFileName));
            }
            catch (Exception)
            {
                return new UserSettings();
            }
        }
    }
}