﻿using System.Reflection;
using System.Runtime.InteropServices;

// Les informations générales relatives à un assembly dépendent de
// l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
// associées à un assembly.
[assembly: AssemblyTitle("Gestion_Des_Vendanges")]
[assembly: AssemblyDescription("Logiciel pour la gestion des vendanges.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Gerber Systems & Networks")]
[assembly: AssemblyProduct("Gestion_Des_Vendanges")]
[assembly: AssemblyCopyright("Copyright © Gerber Systems && Networks 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// L'affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly
// aux composants COM. Si vous devez accéder à un type dans cet assembly à partir de
// COM, affectez la valeur true à l'attribut ComVisible sur ce type.
[assembly: ComVisible(false)]

// Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
[assembly: Guid("49620aae-5e11-4f18-bf66-b09150751cc9")]

// Les informations de version pour un assembly se composent des quatre valeurs suivantes :
//
//      Version principale
//      Version secondaire
//      Numéro de build
//      Révision
//
// Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut
// en utilisant '*', comme indiqué ci-dessous :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.1.3.15")]
[assembly: AssemblyFileVersion("3.1.3.15")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.cfg.xml", Watch = true)]