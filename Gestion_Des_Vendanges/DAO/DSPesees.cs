﻿using System.Data.SqlClient;

namespace Gestion_Des_Vendanges.DAO
{
}

namespace Gestion_Des_Vendanges.DAO
{
}

namespace Gestion_Des_Vendanges.DAO
{
}

namespace Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters
{
    partial class AUT_AUTRE_MENTIONTableAdapter
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AUT_AUTRE_MENTIONTableAdapter));

        public virtual SqlCommand CommandByAcquitIdLieuIdCepageId(int? AcquitId, int? LieuId, int? CepageId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT AUT_ID, AUT_NOM ",
                " FROM AUT_AUTRE_MENTION  ",
                " WHERE (AUT_ID IN ( ",
                "   SELECT AUT_ID FROM PAR_PARCELLE WHERE ",
                AcquitId == null ? "" : "    (ACQ_ID = ", AcquitId, ") AND  ",
                LieuId == null ? "" : "   (LIE_ID = ", LieuId, ") AND  ",
                CepageId == null ? "" : "   (CEP_ID = ", CepageId, ") AND ",
                "   ( 1 = 1 )",
                " )) ",
                " ORDER BY AUT_NOM "
            );

            return cmd;
        }

        public virtual int FillByAcquitIdLieuIdCepageId(DSPesees.AUT_AUTRE_MENTIONDataTable dataTable, int? AcquitId, int? LieuId, int? CepageId)
        {
            Adapter.SelectCommand = CommandByAcquitIdLieuIdCepageId(AcquitId: AcquitId, LieuId: LieuId, CepageId: CepageId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }
    }

    partial class PAM_PARAMETRESTableAdapter
    {
        private int OpenConnection(SqlCommand selectCommand)
        {
            System.Data.ConnectionState previousConnectionState = selectCommand.Connection.State;
            if (((selectCommand.Connection.State & System.Data.ConnectionState.Open)
                        != System.Data.ConnectionState.Open))
            {
                selectCommand.Connection.Open();
            }
            int returnValue;
            try
            {
                returnValue = selectCommand.ExecuteNonQuery();
            }
            finally
            {
                if ((previousConnectionState == System.Data.ConnectionState.Closed))
                {
                    selectCommand.Connection.Close();
                }
            }
            return returnValue;
        }
    }

    partial class RES_NOMCOMPLETTableAdapter
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ACQ_ACQUITTableAdapter));

        public virtual SqlCommand CommandOrderedByDefault()
        {
            var cmd = this.Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT     RES_ID, RES_NOM + \' \' + RES_PRENOM AS NOMCOMPLET\r\n",
                "FROM         RES_RES",
                "PONSABLE\r\n",
                "ORDER BY RES_DEFAULT desc, RES_NOM, RES_PRENOM"
            );

            return cmd;
        }

        public virtual int FillOrderedByDefault(DSPesees.RES_NOMCOMPLETDataTable dataTable)
        {
            Adapter.SelectCommand = CommandOrderedByDefault();

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }

        public virtual SqlCommand CommandByAnnProVenCepageLieux(int ANN_ID, int producteurId, int cepageId, int lieuxId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT DISTINCT acq.* ",
                " FROM ",
                " ACQ_ACQUIT acq,",
                " PAR_PARCELLE par",
                " WHERE ",
                " acq.ACQ_ID=par.ACQ_ID AND",
                " par.CEP_ID=", cepageId, " AND",
                " par.LIE_ID=", lieuxId, " AND",
                " acq.ANN_ID = ", ANN_ID, " AND ",
                " acq.PRO_ID_VENDANGE = ", producteurId,
                " ORDER BY ACQ_NUMERO"
            );

            return cmd;
        }
    }

    partial class ACQ_ACQUITTableAdapter
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ACQ_ACQUITTableAdapter));

        public virtual SqlCommand CommandByAnnProVenCepage(int ANN_ID, int producteurId, int cepageId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT DISTINCT acq.* ",
                " FROM ",
                " ACQ_ACQUIT acq,",
                " PAR_PARCELLE par",
                " WHERE ",
                " acq.ACQ_ID=par.ACQ_ID AND",
                " par.CEP_ID=", cepageId, " AND",
                " acq.ANN_ID = ", ANN_ID, " AND ",
                " acq.PRO_ID_VENDANGE = ", producteurId,
                " ORDER BY ACQ_NUMERO"
            );

            return cmd;
        }

        public virtual int FillByAnnProVenCepage(DSPesees.ACQ_ACQUITDataTable dataTable, int ANN_ID, int producteurId, int cepageId)
        {
            Adapter.SelectCommand = CommandByAnnProVenCepage(ANN_ID: ANN_ID, producteurId: producteurId, cepageId: cepageId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = Adapter.Fill(dataTable);
            return returnValue;
        }

        public virtual SqlCommand CommandByAnnProVenCepageLieux(int ANN_ID, int producteurId, int cepageId, int lieuxId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT DISTINCT acq.* ",
                " FROM ",
                " ACQ_ACQUIT acq,",
                " PAR_PARCELLE par",
                " WHERE ",
                " acq.ACQ_ID=par.ACQ_ID AND",
                " par.CEP_ID=", cepageId, " AND",
                " par.LIE_ID=", lieuxId, " AND",
                " acq.ANN_ID = ", ANN_ID, " AND ",
                " acq.PRO_ID_VENDANGE = ", producteurId,
                " ORDER BY ACQ_NUMERO"
            );

            return cmd;
        }

        public virtual int FillByAnnProVenCepageLieu(DSPesees.ACQ_ACQUITDataTable dataTable, int ANN_ID, int producteurId, int cepageId, int lieuId)
        {
            Adapter.SelectCommand = CommandByAnnProVenCepageLieux(ANN_ID: ANN_ID, producteurId: producteurId, cepageId: cepageId, lieuxId: lieuId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }

        public virtual SqlCommand CommandByAnnProVenCepageLieuxAutreMention(int ANN_ID, int producteurId, int cepageId, int lieuxId, int autreMentionId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT DISTINCT acq.* ",
                " FROM ",
                " ACQ_ACQUIT acq,",
                " PAR_PARCELLE par",
                " WHERE ",
                " acq.ACQ_ID=par.ACQ_ID AND",
                " par.AUT_ID=", autreMentionId, " AND",
                " par.CEP_ID=", cepageId, " AND",
                " par.LIE_ID=", lieuxId, " AND",
                " acq.ANN_ID = ", ANN_ID, " AND ",
                " acq.PRO_ID_VENDANGE = ", producteurId,
                " ORDER BY ACQ_NUMERO"
            );

            return cmd;
        }

        public virtual int FillByAnnProVenCepageLieuAutreMention(DSPesees.ACQ_ACQUITDataTable dataTable, int ANN_ID, int producteurId, int cepageId, int lieuId, int autreMentionId)
        {
            Adapter.SelectCommand = CommandByAnnProVenCepageLieuxAutreMention(ANN_ID: ANN_ID, producteurId: producteurId, cepageId: cepageId, lieuxId: lieuId, autreMentionId: autreMentionId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }
    }

    partial class CEP_CEPAGETableAdapter
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(CEP_CEPAGETableAdapter));

        public virtual SqlCommand CommandByCouleurFromAcquiId(int AcquiId)
        {
            var cmd = this.Connection.CreateCommand();
            cmd.CommandText = string.Concat(
            "select distinct cep.* ",
            " from  ",
            "	cep_cepage cep,  ",
            "	ACQ_ACQUIT acq  ",
            " where  ",
            "	cep.COU_ID=acq.COU_ID and  ",
            "	acq.ACQ_ID=", AcquiId, "  ",
            "order by cep.CEP_NOM, cep.CEP_NUMERO"
            );

            return cmd;
        }

        public virtual int FillByCouleurFromAcquiId(DSPesees.CEP_CEPAGEDataTable dataTable, int AcquiId)
        {
            Adapter.SelectCommand = CommandByCouleurFromAcquiId(AcquiId: AcquiId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }

        public virtual SqlCommand CommandByProducteurAnnee(int ANN_ID, int PRO_ID)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat("SELECT distinct cep.*",
                " FROM ",
                " ACQ_ACQUIT acq,",
                " CEP_CEPAGE cep,",
                " PAR_PARCELLE par",
                " WHERE",
                " cep.CEP_ID=par.CEP_ID AND",
                " par.ACQ_ID = acq.ACQ_ID AND",
                " acq.PRO_ID_VENDANGE = ", PRO_ID, " AND",
                " acq.ANN_ID = ", ANN_ID,
                " order by ",
                " cep.CEP_NOM, cep.CEP_NUMERO"
            );

            return cmd;
        }

        public virtual int FillByProducteurAnnee(DSPesees.CEP_CEPAGEDataTable dataTable, int ANN_ID, int PRO_ID)
        {
            Adapter.SelectCommand = CommandByProducteurAnnee(ANN_ID: ANN_ID, PRO_ID: PRO_ID);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }
    }

    partial class COU_COULEURTableAdapter
    {
    }

    partial class LIP_LIGNE_PAIEMENT_PESEETableAdapter
    {
    }

    public partial class PED_PESEE_DETAIL1TableAdapter
    {
    }

    public partial class LIE_LIEU_DE_PRODUCTIONTableAdapter
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(CEP_CEPAGETableAdapter));

        public virtual SqlCommand CommandByAcquitEtCepage(int acquitId, int cepageId)
        {
            var cmd = Connection.CreateCommand();
            cmd.CommandText = string.Concat(
                "SELECT ",
                " LIE_ID, ",
                " LIE_NOM,  ",
                " LIE_NUMERO,  ",
                " REG_ID  ",
                " FROM LIE_LIEU_DE_PRODUCTION  ",
                " WHERE ( ",
                " LIE_ID IN ( ",
                    " SELECT LIE_ID  ",
                    " FROM PAR_PARCELLE  ",
                    " WHERE (ACQ_ID = ", acquitId, ") AND ",
                    " (  CEP_ID = ", cepageId, " )",
                    " ) ",
                " ) ",
                " ORDER BY LIE_NOM");

            return cmd;
        }

        public int FillByAcquitEtCepage(DSPesees.LIE_LIEU_DE_PRODUCTIONDataTable dataTable, int acquitId, int cepageId)
        {
            Adapter.SelectCommand = CommandByAcquitEtCepage(acquitId: acquitId, cepageId: cepageId);

            if ((ClearBeforeFill == true))
            {
                dataTable.Clear();
            }
            Log.InfoFormat("Ready to execute: {0}", this.Adapter.SelectCommand.CommandText);
            int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }
    }
}
namespace Gestion_Des_Vendanges.DAO
{


    public partial class DSPesees
    {
    }
}
