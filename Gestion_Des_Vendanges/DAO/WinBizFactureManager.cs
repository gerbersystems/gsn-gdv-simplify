﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Gère les documents dans WinBIZ
    *  Date de création: 03.11.2009
    *  Modifications:
    * */

    public enum AbType
    {
        posteBvRouge = 10,
        posteBvrOrange = 11,
        posteBvrOrangeEuro = 12,
        banqueBvRouge = 20,
        banqueBvrOrange = 21,
        banqueSuisse = 30,
        banqueInternational = 31
    }

    internal class WinBizFactureManager
    {
        private OleDbConnection _connection;
        private int _annId;

        public WinBizFactureManager(OleDbConnection connection, int annId)
        {
            _connection = connection;
            _annId = annId;
        }

        public void CreateFactures(List<FactureWinBIZ> factures)
        {
            foreach (FactureWinBIZ facture in factures)
            {
                initFacture(facture);
            }

            foreach (FactureWinBIZ facture in factures)
            {
                if (!facture.IsInserted)
                    insertFacture(facture);
            }
        }

        public List<ModeComptabilisation> GetModeComptabilisations()
        {
            List<ModeComptabilisation> modeComptabilisations = new List<ModeComptabilisation>();
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                //Compte de charges / Taux TCA / TVA Incl / TypeTVA
                command.CommandText = "SELECT AC_NUMERO, AC_TEXTE, AC_TVATXA, AC_TVATYPA, AC_TVAINCA FROM ART_CPT;";
                using (OleDbDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ModeComptabilisationWinBiz modeComptabilisation = new ModeComptabilisationWinBiz();
                        modeComptabilisation.IdWinbiz = Convert.ToInt32(dataReader.GetDecimal(0));
                        modeComptabilisation.Texte = dataReader.GetString(1);
                        modeComptabilisation.TauxTVA = Convert.ToDouble(dataReader.GetDecimal(2));
                        modeComptabilisation.SetModeTVA(dataReader.GetDecimal(3), dataReader.GetDecimal(4));
                        modeComptabilisation.AnnId = _annId;
                        modeComptabilisations.Add(modeComptabilisation);
                    }
                }
            }
            finally
            {
                _connection.Close();
            }

            return modeComptabilisations;
        }

        private void initFacture(FactureWinBIZ facture)
        {
            facture.IsInserted = false;
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                if (facture.DocumentID > 0)
                {
                    command.CommandText = "SELECT COUNT(*) FROM DOCUMENT WHERE DO_NUMERO = " + facture.DocumentID + ";";

                    if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                        facture.IsInserted = true;
                }

                if (!facture.IsInserted)
                {
                    //Donnees bancaires
                    command.CommandText = string.Concat("SELECT AB_NUMERO, AB_TYPE, AB_CCP, AB_ADHERCP, AB_CLEARIN, AB_CPTBANQ, AB_IBAN, AB_BANCCP, AB_BANCCP2, AB_SWIFT ",
                                                        "FROM AD_BANQU ",
                                                        "WHERE AB_ADRESSE = " + facture.AdrIdWinBIZ /*,
                                                             " AND (AB_TYPE = 10 OR AB_TYPE = 30 OR AB_TYPE = 11);"*/);

                    OleDbDataReader dataReader = command.ExecuteReader();

                    // In Progress : Test to select default bank when payement is "virement"
                    //if (dataReader.RecordsAffected < 1)
                    //{
                    //    command.CommandText = string.Concat("SELECT AB_NUMERO, AB_TYPE, AB_CCP, AB_CLEARIN"
                    //                                        , ", AB_CPTBANQ, AB_IBAN, AB_SWIFT "
                    //                                        , "FROM AD_BANQU "
                    //                                        , "WHERE AB_ADRESSE = " + facture.AdrIdWinBIZ
                    //                                        , " AND (AB_TYPE = 20);");

                    //    dataReader = command.ExecuteReader();
                    //}

                    if (dataReader.Read())
                    {
                        SetMethodOfPayment(facture, dataReader);

                        //if (dataReader.GetDecimal(1) == 10)
                        //{
                        //    facture.CompteCCP = dataReader.GetString(2);
                        //}
                        //else
                        //{
                        //    facture.Clearing = dataReader.GetString(3);
                        //    facture.CompteBanque = dataReader.GetString(4);
                        //    facture.Iban = dataReader.GetString(5);
                        //    facture.Swift = dataReader.GetString(6);
                        //}
                    }
                    dataReader.Close();
                    //Compte creanciers
                    command.CommandText = "SELECT AD_CPT_CRE FROM ADRESSES WHERE " +
                        "AD_NUMERO = " + facture.AdrIdWinBIZ + ";";
                    facture.CompteFournisseur = Convert.ToString(command.ExecuteScalar());

                    //Echance (en nombre de jours)
                    command.CommandText = "SELECT PM_JOURS1 FROM ADRESSES, CONDPMT WHERE " +
                        "AD_CNDPMT2 = PM_NUMERO AND AD_NUMERO = " + facture.AdrIdWinBIZ + ";";
                    facture.NbJourEcheance = Utilities.ConvertToDecimal(command.ExecuteScalar());

                    //Compte de charges / Taux TCA / TVA Incl / TypeTVA
                    /*  command.CommandText = "SELECT AC_CPTACH, AC_TVATXA, AC_TVAINCA, AC_TVATYPA, AC_TVACPTA, AC_TVABNA, AC_TVA_PT, AC_TVAARRA FROM ART_CPT, ADRESSES WHERE " +
                          "AC_NUMERO = AD_CREMCPT AND AD_NUMERO = " + facture.AdrIdWinBIZ + ";";*/
                    foreach (LigneDetailWinBIZ ligne in facture.LignesDetails)
                    {
                        command.CommandText = "SELECT AC_CPTACH, AC_TVATXA, AC_TVAINCA, AC_TVATYPA, AC_TVACPTA, AC_TVABNA, AC_TVA_PT, AC_TVAARRA FROM ART_CPT WHERE " +
                            "AC_NUMERO = " + ligne.MocIdWinBiz + ";";
                        dataReader.Close();
                        dataReader = command.ExecuteReader();
                        if (dataReader.Read())
                        {
                            ligne.CompteCharge = dataReader.GetString(0);
                            //facture.SetInfoComptesNull(dataReader.GetString(0), dataReader.GetDecimal(1), dataReader.GetDecimal(2));
                            // facture.TypeTVA = dataReader.GetDecimal(3);
                            ligne.CompteTVA = dataReader.GetString(4);
                            ligne.BnTVA = dataReader.GetDecimal(5);
                            ligne.PtTVA = dataReader.GetDecimal(6);
                            ligne.ArrTVA = dataReader.GetDecimal(7);
                        }
                    }
                }
            }
            finally
            {
                _connection.Close();
            }
        }

        private static void SetMethodOfPayment(FactureWinBIZ facture, OleDbDataReader dataReader)
        {
            decimal _adrBanqueId;
            decimal.TryParse(dataReader["AB_NUMERO"].ToString(), out _adrBanqueId);
            facture.AdrBanqueId = _adrBanqueId;

            int methodOfPayment;
            if (int.TryParse(dataReader["AB_TYPE"].ToString(), out methodOfPayment))
            {
                switch (methodOfPayment)
                {
                    case (int)AbType.posteBvRouge:
                        facture.CompteCCP = dataReader["AB_CCP"].ToString();
                        break;

                    case (int)AbType.posteBvrOrange:
                        facture.CompteCCP = dataReader["AB_ADHERCP"].ToString();
                        break;

                    case (int)AbType.posteBvrOrangeEuro:
                        throw new NotImplementedException("Le paiement par poste avec bvr en Euro, n'est pas implémenté.");

                    case (int)AbType.banqueBvRouge:
                        facture.Clearing = dataReader["AB_CLEARIN"].ToString().Trim();
                        facture.CompteBanque = dataReader["AB_CPTBANQ"].ToString().Trim();
                        facture.CompteCCP = dataReader["AB_BANCCP2"].ToString().Trim();
                        facture.Iban = dataReader["AB_IBAN"].ToString().Trim();
                        facture.Swift = dataReader["AB_SWIFT"].ToString().Trim();
                        break;

                    case (int)AbType.banqueBvrOrange:
                        facture.Clearing = dataReader["AB_CLEARIN"].ToString();
                        facture.CompteBanque = dataReader["AB_CPTBANQ"].ToString();
                        facture.CompteCCP = dataReader["AB_BANCCP"].ToString();
                        facture.Iban = dataReader["AB_IBAN"].ToString();
                        facture.Swift = dataReader["AB_SWIFT"].ToString();
                        break;

                    case (int)AbType.banqueSuisse:
                        facture.Clearing = dataReader["AB_CLEARIN"].ToString();
                        facture.CompteBanque = dataReader["AB_CPTBANQ"].ToString();
                        facture.Iban = dataReader["AB_IBAN"].ToString();
                        facture.Swift = dataReader["AB_SWIFT"].ToString();
                        break;

                    case (int)AbType.banqueInternational:
                        facture.CompteBanque = dataReader["AB_CPTBANQ"].ToString();
                        facture.Iban = dataReader["AB_IBAN"].ToString();
                        facture.Swift = dataReader["AB_SWIFT"].ToString();
                        break;

                    default:
                        throw new NotImplementedException("Cette méthode de paiement n'est pas implémentée.");
                }
            }
        }

        private void insertFacture(FactureWinBIZ facture)
        {
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                WinBizControleur.SetNullOff(command);

                SetDocumentId(facture, command);
                CreateDocumentWinbiz(facture, command);

                if (facture.HasPaiementMode)
                    CreateDonneesPaiementWinbiz(facture, command);

                CreateLigneDetailWinbiz(facture, command);
            }
            finally
            {
                _connection.Close();
            }
        }

        private static void CreateLigneDetailWinbiz(FactureWinBIZ facture, OleDbCommand command)
        {
            int numLigne = 1;
            foreach (LigneDetailWinBIZ ligne in facture.LignesDetails)
            {
                command.CommandText = "INSERT INTO DETAIL(DL_NUMERO, DL_NOLIGNE, " +
                    "DL_QTE1, DL_QTE2, DL_PRIX, DL_UNITE, DL_MONTANT, DL_COMPTE, " +
                    "DL_TVA_MNT, DL_TVA_TX, DL_TVA_TYP, DL_TVA_INC, DL_DESC, " +
                    "DL_DATE1, DL_TVA_CPT, DL_TVA_BN, DL_TVA_PT, DL_TVA_ARR, " +
                    "DL_NUMER2, DL_DETTYP, DL_ARTICLE, DL_QTE3, DL_QTE4, DL_STOCK, DL_STCKUPD, " +
                    "DL_CONDIT, DL_RAB_TX, DL_RAB_HTX, DL_RAB_MNT, DL_MONMNT, DL_PRIXMON, " +
                    "DL_TVA_MOD, DL_HORSTVA, DL_ACOMPTE, DL_BLOCPRT, DL_EMBALLA, DL_EMBAUTO, DL_ENTREP, " +
                    "DL_PCENT1, DL_VENDEUR, DL_REF2,  DL_COMSYS, DL_ANA, DL_DELAI, " +
                    "DL_GRDQTE, DL_RECAP, DL_PROJECT, DL_PJTELM, DL_QTETYPE, DL_TBLPRIX, DL_QTE5, " +
                    "DL_QTE6, DL_SPLADR, DL_SPLTX) " +
                    "VALUES(" + facture.DocumentID + ", " + numLigne + ", " +
                    ligne.Quantite + ", " + ligne.Quantite + ", " + ligne.PrixUnitaire +
                    ", '" + Utilities.FormatStringToSQL(ligne.Unite) + "', " + ligne.Montant + ", '" + ligne.CompteCharge +
                    "', " + ligne.MontantTVA + ", " + ligne.TauxTVA + ", " + ligne.TypeTVA +
                    ", " + ligne.TVAIncl + ", '" + Utilities.FormatStringToSQL(ligne.DescriptionArticle) +
                    "', " + Utilities.FormatDateToFoxPro(facture.DateDocument) + ", '" + ligne.CompteTVA +
                    "', " + ligne.BnTVA + ", " + ligne.PtTVA + ", " + ligne.ArrTVA + ", " +
                    "0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, " +
                    "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, " +
                    "0, 0, 0);";
                command.ExecuteNonQuery();
                numLigne++;
            }
        }

        private static void CreateDonneesPaiementWinbiz(FactureWinBIZ facture, OleDbCommand command)
        {
            command.CommandText = "INSERT INTO CRE_PMT(PC_NUMERO, PC_CCP, PC_CPTBANQ, PC_CLEARIN, "
                + "PC_IBAN, PC_SWIFT, PC_TAXE, PC_URGENT, PC_MOTIF1) VALUES("
                + facture.DocumentID + ", '" + Utilities.FormatStringToSQL(facture.CompteCCP) + "', '" + Utilities.FormatStringToSQL(facture.CompteBanque)
                + "', '" + Utilities.FormatStringToSQL(facture.Clearing) + "', '" + Utilities.FormatStringToSQL(facture.Iban) + "', '" + Utilities.FormatStringToSQL(facture.Swift)
                + "', " + facture.Taxe + ", " + facture.Urgent + ", '" + Utilities.FormatStringToSQL(facture.RefFacture) + "');";
            command.ExecuteNonQuery();
        }

        private static void CreateDocumentWinbiz(FactureWinBIZ facture, OleDbCommand command)
        {
            command.CommandText =
                string.Concat("INSERT INTO DOCUMENT(DO_NUMERO, DO_TYPE, DO_NODOC, ",
                                                   "DO_DATE1, DO_DATE2, ",
                                                   "DO_ADR1, DO_REF3, ",
                                                   "DO_REF1, DO_TVA, ",
                                                   "DO_NET, DO_MONTANT, DO_PMT_TYP, ",
                                                   "DO_BAN_ADR, DO_COMPTE, ",
                                                   "DO_NUMER2, DO_NUMER3, DO_NUMER4, DO_NUMER5, DO_PERIOD, DO_TIME, ",
                                                   "DO_CATEGO, DO_ADR2, DO_ADR3, DO_REF2, DO_CAISSE, DO_POS_SES, DO_RABAIS, ",
                                                   "DO_RAB_HTX, DO_TVAEXCL, DO_TVA_RED, DO_ESC_MNT, DO_PMT, DO_PMTSTAT, DO_PAYE, ",
                                                   "DO_EXPEDIT, DO_VENDEUR, DO_MESSAGE, DO_TBLPRIX, DO_IMPRIME, DO_PRTCODE, DO_SELECT, ",
                                                   "DO_CPTSNO, DO_CPTSMOD, DO_JOURS1, DO_RAPPELS, DO_RAPSEND, DO_PI_NBR, DO_BAN_PAY, ",
                                                   "DO_LSV, DO_DOCPRES, DO_BENEVOL, DO_ROUND, DO_MONNAI, DO_COURS, DO_MONMNT, ",
                                                   "DO_MONQTE, DO_MONPMT, DO_MONESC, DO_EMBAUTO, DO_CNDPMT1, DO_ESCOM1, DO_ECHEAN1, ",
                                                   "DO_ECHEAN2, DO_ESCMETH, DO_LANGUE, DO_ETAT, DO_STATCOM, DO_PCENT1, DO_PMTTYPE, ",
                                                   "DO_ESC2MNT, DO_ESC2MON, DO_ESC2TX, DO_ESC2TM, DO_ESC2MET, DO_RAB2MNT, DO_RAB2MON, ",
                                                   "DO_RAB2TX, DO_RAB2TM, DO_RAB2MET, DO_ESC_MET, DO_EXCDIFF, DO_DELAI, DO_CLASSIF, ",
                                                   "DO_DOCLOT, DO_PMTPREC, DO_EBSTAT, DO_PROJECT, DO_BCL, DO_FOLDER, DO_PROJCT) " +
                              "VALUES(" + facture.DocumentID + ", " + facture.TypeDocument + ", " + GetNumeroDocumentType(facture, command) + ", " +
                                       Utilities.FormatDateToFoxPro(facture.DateDocument) + ", " + Utilities.FormatDateToFoxPro(facture.DateEcheance) + ", " +
                                       facture.AdrIdWinBIZ + ", '" + Utilities.FormatStringToSQL(facture.RefFacture) + "', '" +
                                       Utilities.FormatStringToSQL(facture.TexteFacture) + "', " + facture.MontantTVA + ", " +
                                       facture.MontantNet + ", " + facture.MontantBrut + ", " + facture.PaiementType + ", " +
                                       facture.AdrBanqueId + ", '" + facture.CompteFournisseur + "', " +
                                      "0, 0, 0, 0, 0, {00:00:00}, " +
                                      "0, 0, 0, 0, 0, 0, 0, ",
                                      "0, 0, 0, 0, 0, 0, 0, ",
                                      "0, 0, 0, 1, 0, 1, 0, ",
                                      "0, 0, 0, 0, 0, 0, " + facture.BanPay + ", ",
                                      "0, 0, 1, 2, 0, 0, 0, ",
                                      "0, 0, 0, 0, 1, 0, " + facture.NbJourEcheance + ", ",
                                      "0, 0, 0, 0, 0, 0, 0, ",
                                      "0, 0, 0, 0, 1, 0, 0, ",
                                      "0, 0, 1, 0, 0, 0, 0, ",
                                      "0, 0, 0, 0, 0, 0, 0);");
            command.ExecuteNonQuery();

            #region backup copy insert into document

            //command.CommandText = "INSERT INTO DOCUMENT(DO_NUMERO, DO_TYPE, DO_NODOC, " +
            //    "DO_DATE1, DO_DATE2, DO_ADR1, DO_REF3, DO_REF1, DO_TVA, DO_NET, DO_MONTANT, " +
            //    "DO_PMT_TYP, DO_BAN_ADR, DO_COMPTE, DO_NUMER2, DO_NUMER3, DO_NUMER4, DO_NUMER5, " +
            //    "DO_PERIOD, DO_TIME, DO_CATEGO, DO_ADR2, DO_ADR3, DO_REF2, DO_CAISSE, DO_POS_SES, " +
            //    "DO_RABAIS, DO_RAB_HTX, DO_TVAEXCL, DO_TVA_RED, DO_ESC_MNT, DO_PMT, DO_PMTSTAT, " +
            //    "DO_PAYE, DO_EXPEDIT, DO_VENDEUR, DO_MESSAGE, DO_TBLPRIX, DO_IMPRIME, " +
            //    "DO_PRTCODE, DO_SELECT, DO_CPTSNO, DO_CPTSMOD, DO_JOURS1, DO_RAPPELS, DO_RAPSEND, " +
            //    "DO_PI_NBR, DO_BAN_PAY, DO_LSV, DO_DOCPRES, DO_BENEVOL, DO_ROUND, DO_MONNAI, " +
            //    "DO_COURS, DO_MONMNT, DO_MONQTE, DO_MONPMT, DO_MONESC, DO_EMBAUTO, DO_CNDPMT1, " +
            //    "DO_ESCOM1, DO_ECHEAN1, DO_ECHEAN2, DO_ESCMETH, DO_LANGUE, DO_ETAT, DO_STATCOM, " +
            //    "DO_PCENT1, DO_PMTTYPE, DO_ESC2MNT, DO_ESC2MON, DO_ESC2TX, DO_ESC2TM, DO_ESC2MET, " +
            //    "DO_RAB2MNT, DO_RAB2MON, DO_RAB2TX, DO_RAB2TM, DO_RAB2MET, DO_ESC_MET, DO_EXCDIFF, " +
            //    "DO_DELAI, DO_CLASSIF, DO_DOCLOT, DO_PMTPREC, DO_EBSTAT, DO_PROJECT, " +
            //    "DO_BCL, DO_FOLDER, DO_PROJCT) " +
            //    "VALUES(" + facture.DocumentID + ", " + facture.TypeDocument + ", " + numDocumentType + ", " +
            //    Utilities.FormatDateToFoxPro(facture.DateDocument) + ", " + Utilities.FormatDateToFoxPro(facture.DateEcheance) + ", "
            //    + facture.AdrIdWinBIZ + ", '" +
            //    Utilities.FormatStringToSQL(facture.RefFacture) + "', '" + Utilities.FormatStringToSQL(facture.TexteFacture) + "', " + facture.MontantTVA + ", " +
            //    facture.MontantNet + ", " + facture.MontantBrut + ", " + facture.PaiementType + ", " + facture.AdrBanqueId + ", '" +
            //    facture.CompteFournisseur + "', " +
            //    "0, 0, 0, 0, 0  , {00:00:00}, " +
            //    "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " +
            //    "0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, " + facture.BanPay + ", " +
            //    "0, 0, 1, 2, 0, 0, 0, " +
            //    "0, 0, 0, 0, 1, 0, " + facture.NbJourEcheance + ", " +
            //    "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, " +
            //    "0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);";
            //command.ExecuteNonQuery();

            #endregion backup copy insert into document
        }

        private static decimal GetNumeroDocumentType(FactureWinBIZ facture, OleDbCommand command)
        {
            command.CommandText = "SELECT MAX(DO_NODOC) FROM DOCUMENT WHERE DO_TYPE = " + facture.TypeDocument + ";";
            return Utilities.ConvertToDecimal(command.ExecuteScalar()) + 1;
        }

        private static void SetDocumentId(FactureWinBIZ facture, OleDbCommand command)
        {
            command.CommandText = "SELECT MAX(DO_NUMERO) FROM DOCUMENT;";
            facture.DocumentID = Utilities.ConvertToDecimal(command.ExecuteScalar()) + 1;
        }
    }
}