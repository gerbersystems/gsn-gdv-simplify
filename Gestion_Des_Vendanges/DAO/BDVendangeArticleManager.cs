﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.DAO
{
    /*
    *  Description: Applique la synchronisation des adresses dans la Gestion des vendanges
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    partial class BDVendangeArticleManager
    {
        private ART_ARTICLETableAdapter _artTable;
        private ANN_ANNEETableAdapter _annTable;
        private CEP_CEPAGETableAdapter _cepTable;
        private LIE_LIEU_DE_PRODUCTIONTableAdapter _lieTable;
        private AUT_AUTRE_MENTIONTableAdapter _autTable;
        private COM_COMMUNETableAdapter _comTable;
        private CLA_CLASSETableAdapter _claTable;
        private COU_COULEURTableAdapter _couTable;
        private PEE_PESEE_ENTETETableAdapter _peeTable;
        private ACQ_ACQUITTableAdapter _acqTable;
        private MOC_MODE_COMPTABILISATIONTableAdapter _mocTable;
        private ASS_ASSOCIATION_ARTICLETableAdapter _assTable;
        private PRO_NOMCOMPLETTableAdapter _proTable;
        private ViewArticleTableAdapter _viewArtTable;
        private DSPesees _dsPesees;

        public BDVendangeArticleManager()
        {
            _artTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ART_ARTICLETableAdapter>();
            _annTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ANN_ANNEETableAdapter>();
            _cepTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<CEP_CEPAGETableAdapter>();
            _lieTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<LIE_LIEU_DE_PRODUCTIONTableAdapter>();
            _autTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<AUT_AUTRE_MENTIONTableAdapter>();
            _comTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<COM_COMMUNETableAdapter>();
            _claTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<CLA_CLASSETableAdapter>();
            _couTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<COU_COULEURTableAdapter>();
            _peeTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PEE_PESEE_ENTETETableAdapter>();
            _acqTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ACQ_ACQUITTableAdapter>();
            _mocTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
            _assTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ASS_ASSOCIATION_ARTICLETableAdapter>();
            _viewArtTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ViewArticleTableAdapter>();
            _proTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PRO_NOMCOMPLETTableAdapter>();
            _dsPesees = new DSPesees();
            InitSpecificTableAdapters();
        }

        public void Generer(int annId)
        {
            List<PreArticle> articles = getPreArticles(annId);
            articles = grouperPreArticles(articles);
            insertArticles(articles);
        }

        private List<PreArticle> grouperPreArticles(List<PreArticle> preArticles)
        {
            List<PreArticle> articlesGrouper = new List<PreArticle>();
            while (preArticles.Count > 0)
            {
                for (int i = 1; i < preArticles.Count; i++)
                {
                    if (preArticles[0].Ajouter(preArticles[i]))
                    {
                        preArticles.RemoveAt(i);
                        i--;
                    }
                }
                articlesGrouper.Add(preArticles[0]);
                preArticles.RemoveAt(0);
            }
            return articlesGrouper;
        }

        private void insertArticles(List<PreArticle> articles)
        {
            foreach (PreArticle article in articles)
            {
                insertArticle(article);
            }
        }

        private string getStringCourt(string valeurLong, int nbChar)
        {
            return (valeurLong.Length > nbChar) ? valeurLong.Remove(nbChar) : valeurLong;
        }

        public void UpdateWinBizArticleIds(List<int> winbizArticlesIds, int annId)
        {
            _artTable.FillByAn(_dsPesees.ART_ARTICLE, annId);
            foreach (DSPesees.ART_ARTICLERow artRow in _artTable.GetDataByAn(annId))
            {
                if (!artRow.IsART_ID_WINBIZNull())
                {
                    bool trouve = false;
                    for (int i = 0; i < winbizArticlesIds.Count && !trouve; i++)
                    {
                        if (winbizArticlesIds[i] == artRow.ART_ID_WINBIZ)
                        {
                            trouve = true;
                            winbizArticlesIds.RemoveAt(i);
                        }
                    }
                    if (!trouve)
                        _artTable.SetWinBizId(null, artRow.ART_ID);
                }
            }
        }
    }
}