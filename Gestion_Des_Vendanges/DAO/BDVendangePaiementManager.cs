﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Gère les données de paiements dans la gestion des vendanges
    *  Date de création: 03.11.2009
    *  Modifications:
    * */

    partial class BDVendangePaiementManager
    {
        #region Properties

        private PAI_PAIEMENTTableAdapter _paiTable;
        private ADR_ADRESSETableAdapter _adrTable;
        private PRO_PROPRIETAIRETableAdapter _proTable;
        private LIM_LIGNE_PAIEMENT_MANUELLETableAdapter _limTable;
        private LIP_LIGNE_PAIEMENT_PESEETableAdapter _lipTable;
        private PEE_PESEE_ENTETETableAdapter _peeTable;
        private LIA_LIGNE_ACOMPTETableAdapter _liaTable;
        private MOC_MODE_COMPTABILISATIONTableAdapter _mocTable;
        private ANN_ANNEETableAdapter _annTable;
        private MCE_MOC_CEPTableAdapter _mceTable;
        private ART_ARTICLETableAdapter _artTable;
        private DSPesees _dsPesees;
        private int _annId;

        #endregion Properties

        public BDVendangePaiementManager(int annId)
        {
            _paiTable = ConnectionManager.Instance.CreateGvTableAdapter<PAI_PAIEMENTTableAdapter>();
            _adrTable = ConnectionManager.Instance.CreateGvTableAdapter<ADR_ADRESSETableAdapter>();
            _proTable = ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
            _limTable = ConnectionManager.Instance.CreateGvTableAdapter<LIM_LIGNE_PAIEMENT_MANUELLETableAdapter>();
            _lipTable = ConnectionManager.Instance.CreateGvTableAdapter<LIP_LIGNE_PAIEMENT_PESEETableAdapter>();
            _peeTable = ConnectionManager.Instance.CreateGvTableAdapter<PEE_PESEE_ENTETETableAdapter>();
            _mocTable = ConnectionManager.Instance.CreateGvTableAdapter<MOC_MODE_COMPTABILISATIONTableAdapter>();
            _liaTable = ConnectionManager.Instance.CreateGvTableAdapter<LIA_LIGNE_ACOMPTETableAdapter>();
            _annTable = ConnectionManager.Instance.CreateGvTableAdapter<ANN_ANNEETableAdapter>();
            _mceTable = ConnectionManager.Instance.CreateGvTableAdapter<MCE_MOC_CEPTableAdapter>();
            _artTable = ConnectionManager.Instance.CreateGvTableAdapter<ART_ARTICLETableAdapter>();

            _annId = annId;
            _dsPesees = new DSPesees();
            InitSpecificTableAdapters();
        }

        public void UpdateWinBizPaiIds(List<FactureWinBIZ> paiements)
        {
            foreach (FactureWinBIZ paiement in paiements)
                _paiTable.UpdateIdWinBiz(Convert.ToInt32(paiement.DocumentID), paiement.PaiId);
        }

        public void UpdateModeComptabilisations(List<ModeComptabilisation> modeComptabilisations, int annId)
        {
            Dictionary<int, ModeComptabilisation> mocGDV = new Dictionary<int, ModeComptabilisation>();
            _mocTable.FillByAnnId(_dsPesees.MOC_MODE_COMPTABILISATION, annId);
            //Charge la table de la base GDV
            foreach (DSPesees.MOC_MODE_COMPTABILISATIONRow mocRow in _mocTable.GetDataByAnnId(annId))
            {
                ModeComptabilisation modeCompta = new ModeComptabilisation();
                modeCompta.AnnId = mocRow.ANN_ID;
                modeCompta.ModId = mocRow.MOD_ID;
                modeCompta.TauxTVA = mocRow.MOC_TAUX_TVA;
                modeCompta.Texte = mocRow.MOC_TEXTE;
                modeCompta.IdWinbiz = mocRow.MOC_ID_WINBIZ;
                mocGDV.Add(mocRow.MOC_ID_WINBIZ, modeCompta);
            }
            //Met à jour la table GDV
            foreach (ModeComptabilisation modeComptabilisation in modeComptabilisations)
            {
                ModeComptabilisation modeComptaGdv;
                if (mocGDV.TryGetValue(modeComptabilisation.IdWinbiz, out modeComptaGdv))
                {
                    if (!modeComptabilisation.Compare(modeComptaGdv))
                    {
                        _mocTable.UpdateMod(modeComptabilisation.Texte, modeComptabilisation.TauxTVA, modeComptabilisation.ModId, modeComptabilisation.IdWinbiz,
                            modeComptabilisation.AnnId);
                    }
                    mocGDV.Remove(modeComptabilisation.IdWinbiz);
                }
                else
                {
                    _mocTable.Insert(modeComptabilisation.IdWinbiz,
                                     modeComptabilisation.Texte,
                                     modeComptabilisation.TauxTVA,
                                     modeComptabilisation.ModId,
                                     modeComptabilisation.AnnId,
                                     modeComptabilisation.NoCompteComptable,
                                     modeComptabilisation.DebitCredit);
                }
            }
            //Supprime les éléments non-trouvés
            foreach (KeyValuePair<int, ModeComptabilisation> modeComptaGdv in mocGDV)
            {
                int mocId = (int)_mocTable.GetMocId(modeComptaGdv.Key, modeComptaGdv.Value.AnnId);

                if (_annTable.GetMocId(modeComptaGdv.Value.AnnId) == mocId)
                    _annTable.SetMocId(null, modeComptaGdv.Value.AnnId);

                _mceTable.DeleteByMocId(mocId);
                DeleteSpecificMocReferences(mocId);

                _artTable.SetNullMocId(mocId);
                _limTable.SetNullMocId(mocId);
                _lipTable.SetNullMocId(mocId);

                _mocTable.DeleteByIdWinBiz(modeComptaGdv.Key, modeComptaGdv.Value.AnnId);
            }
        }

        public void UpdateMontantPaiement(int paiId)
        {
            const int tvaExclue = 3;

            double montantLimNet = 0;
            double montantLimTTC = 0;
            double montantLipNet = 0;
            double montantLipTTC = 0;
            double solde = 0;

            //Calcul : Lignes Manuelles
            _limTable.FillByPai(_dsPesees.LIM_LIGNE_PAIEMENT_MANUELLE, paiId);

            foreach (DSPesees.LIM_LIGNE_PAIEMENT_MANUELLERow limRow in _limTable.GetDataByPai(paiId))
            {
                montantLimNet += (limRow.MOD_ID == tvaExclue) ? limRow.LIM_MONTANT_HT : limRow.LIM_MONTANT_TTC;

                montantLimTTC += limRow.LIM_MONTANT_TTC;
            }

            //Calcul : Lignes pesées
            _lipTable.FillByPai(_dsPesees.LIP_LIGNE_PAIEMENT_PESEE, paiId);
            foreach (DSPesees.LIP_LIGNE_PAIEMENT_PESEERow lipRow in _lipTable.GetDataByPai(paiId))
            {
                montantLipNet += (lipRow.MOD_ID == tvaExclue) ? lipRow.LIP_MONTANT_HT : lipRow.LIP_MONTANT_TTC;

                montantLipTTC += lipRow.LIP_MONTANT_TTC;
            }

            //Calcul : Solde
            if (_paiTable.GetPaiAcompte(paiId) == false)
            {
                solde = montantLimTTC + montantLipTTC;
                _paiTable.FillAcomptesByPaiId(_dsPesees.PAI_PAIEMENT, paiId);

                foreach (DSPesees.PAI_PAIEMENTRow paiRow in _paiTable.GetAcomptesByPaiId(paiId))
                    solde -= (paiRow.PAI_MONTANT_LIM_TTC + paiRow.PAI_MONTANT_LIP_TTC);
            }

            _paiTable.UpdatePaiMontant(montantLimNet, montantLimTTC, montantLipNet, montantLipTTC, solde, paiId);
        }

        public List<FactureWinBIZ> GetPaiements()
        {
            List<FactureWinBIZ> paiements = new List<FactureWinBIZ>();
            _paiTable.FillByAnnId(_dsPesees.PAI_PAIEMENT, _annId);
            foreach (DSPesees.PAI_PAIEMENTRow rowPai in _paiTable.GetDataByAnnId(_annId))
            {
                FactureWinBIZ paiement = new FactureWinBIZ();
                _limTable.FillByPai(_dsPesees.LIM_LIGNE_PAIEMENT_MANUELLE, rowPai.PAI_ID);
                _lipTable.FillByPai(_dsPesees.LIP_LIGNE_PAIEMENT_PESEE, rowPai.PAI_ID);
                paiement.DateDocument = rowPai.PAI_DATE;
                paiement.RefFacture = rowPai.PAI_NUMERO;
                paiement.TexteFacture = rowPai.PAI_TEXTE;
                paiement.PaiId = rowPai.PAI_ID;
                try
                {
                    paiement.AdrIdWinBIZ = (int)_proTable.GetAdrIdWinBIZ(rowPai.PRO_ID);
                }
                catch (NullReferenceException)
                {
                }
                catch (InvalidOperationException)
                {
                }
                try
                {
                    paiement.DocumentID = rowPai.PAI_ID_WINBIZ;
                }
                catch
                {
                }

                paiement.ProId = rowPai.PRO_ID;
                /* if (rowPai.MOD_ID == 1)
                 {
                     paiement.TVAIncl = true;
                     paiement.TauxTVA = 0;
                     paiement.TypeTVA = 1;
                 }
                 else
                 {
                     paiement.TauxTVA = Convert.ToDecimal(rowPai.PAI_TAUX_TVA);
                     paiement.TypeTVA = 3;
                     paiement.TVAIncl = (rowPai.MOD_ID == 2);
                 }*/
                //Ajout des lignes de pesées
                AddLignesPesees(rowPai, paiement);
                //Déduction des acomptes
                if (!rowPai.PAI_ACOMPTE)
                {
                    PAI_PAIEMENTTableAdapter acompteTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PAI_PAIEMENTTableAdapter>();
                    acompteTable.FillAcomptesByPaiId(_dsPesees.PAI_PAIEMENT, paiement.PaiId);
                    foreach (DSPesees.PAI_PAIEMENTRow acompteRow in acompteTable.GetAcomptesByPaiId(paiement.PaiId))
                    {
                        _liaTable.FillByPaiId(_dsPesees.LIA_LIGNE_ACOMPTE, acompteRow.PAI_ID);
                        //Lignes d'acomptes groupées par mode de comptabilisation
                        foreach (DSPesees.LIA_LIGNE_ACOMPTERow liaRow in _liaTable.GetDataByPaiId(acompteRow.PAI_ID))
                        {
                            LigneDetailWinBIZ deductionAcompte = new LigneDetailWinBIZ();
                            deductionAcompte.MontantHT = -Convert.ToDecimal(liaRow.MONTANT_HT);
                            deductionAcompte.MontantTTC = -Convert.ToDecimal(liaRow.MONTANT_TTC);
                            deductionAcompte.Quantite = 1;
                            deductionAcompte.DescriptionArticle = liaRow.PAI_TEXTE;
                            if (liaRow.MOD_ID == 1)
                            {
                                deductionAcompte.TVAInclBool = true;
                                deductionAcompte.TauxTVA = 0;
                                deductionAcompte.TypeTVA = 1;
                            }
                            else
                            {
                                deductionAcompte.TauxTVA = Convert.ToDecimal(liaRow.TAUX_TVA);
                                deductionAcompte.TypeTVA = 3;
                                deductionAcompte.TVAInclBool = (liaRow.MOD_ID == 2);
                            }
                            deductionAcompte.MocIdWinBiz = Convert.ToInt32(_mocTable.GetMocIdWinBiz(liaRow.MOC_ID));
                            paiement.AddLigneDetail(deductionAcompte);
                        }
                    }
                }

                //Ajout des lignes manuelles
                foreach (DSPesees.LIM_LIGNE_PAIEMENT_MANUELLERow rowLim in _limTable.GetDataByPai(rowPai.PAI_ID))
                {
                    LigneDetailWinBIZ lignePaiement = new LigneDetailWinBIZ();
                    lignePaiement.MontantHT = Convert.ToDecimal(rowLim.LIM_MONTANT_HT);
                    lignePaiement.MontantTTC = Convert.ToDecimal(rowLim.LIM_MONTANT_TTC);
                    lignePaiement.Quantite = 1;
                    lignePaiement.DescriptionArticle = rowLim.LIM_TEXTE;
                    if (rowLim.MOD_ID == 1)
                    {
                        lignePaiement.TVAInclBool = true;
                        lignePaiement.TauxTVA = 0;
                        lignePaiement.TypeTVA = 1;
                    }
                    else
                    {
                        lignePaiement.TauxTVA = Convert.ToDecimal(rowLim.LIM_TAUX_TVA);
                        lignePaiement.TypeTVA = 3;
                        lignePaiement.TVAInclBool = (rowLim.MOD_ID == 2);
                    }
                    lignePaiement.MocIdWinBiz = Convert.ToInt32(_mocTable.GetMocIdWinBiz(rowLim.MOC_ID));
                    paiement.AddLigneDetail(lignePaiement);
                }
                paiements.Add(paiement);
            }
            return paiements;
        }

        private string GetDescriptionArticle(DSPesees.PEE_PESEE_ENTETERow rowPee, string an, string cepage)
        {
            string cepageCourt = (cepage.Length > 15) ? cepage.Remove(15) : cepage;
            string lieuProd = Utilities.GetLieuProduction(rowPee.LIE_ID);
            string lieuProdCourt = (lieuProd.Length > 15) ? lieuProd.Remove(15) : lieuProd;
            string autMention = Utilities.GetAutreMention(rowPee.AUT_ID);
            string designationArticle = cepageCourt + " - " + an + " - " + lieuProdCourt + " - " + autMention;

            if (designationArticle.Length > 40)
                designationArticle = designationArticle.Remove(40);

            return Utilities.RemoveAccents(designationArticle);
        }
    }
}