﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Contrôle les classes utilisant la base de données "GestionVendanges"
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class BDVendangeControleur
    {
        #region properties

        private BDVendangeUpdateManager _updateManager;

        private BDVendangeUpdateManager _UpdateManager
        {
            get
            {
                if (_updateManager == null)
                {
                    _updateManager = new BDVendangeUpdateManager();
                }
                return _updateManager;
            }
        }

        private BDVendangeAdresseManager _adresseManager;

        private BDVendangeAdresseManager _AdresseManager
        {
            get
            {
                if (_adresseManager == null)
                {
                    _adresseManager = new BDVendangeAdresseManager();
                }
                return _adresseManager;
            }
        }

        private BDVendangePaiementManager _paiementManager;

        private BDVendangePaiementManager _PaiementManager
        {
            get
            {
                if (_paiementManager == null)
                {
                    _paiementManager = new BDVendangePaiementManager(Utilities.IdAnneeCourante);
                }
                return _paiementManager;
            }
        }

        private BDVendangeSauvegardeManager _sauvegardeManager;

        private BDVendangeSauvegardeManager _SauvegardeManager
        {
            get
            {
                if (_sauvegardeManager == null)
                {
                    _sauvegardeManager = new BDVendangeSauvegardeManager();
                }
                return _sauvegardeManager;
            }
        }

        private BDVendangeArticleManager _articleManager;

        private BDVendangeArticleManager _ArticleManager
        {
            get
            {
                if (_articleManager == null)
                {
                    _articleManager = new BDVendangeArticleManager();
                }
                return _articleManager;
            }
        }

        #endregion properties

        public BDVendangeControleur()
        {
        }

        public void UpdateBD()
        {
            _UpdateManager.UpdateBD();
        }

        public void ImporterAdresses(List<Adresse> adresses)
        {
            _AdresseManager.ImporterAdresses(adresses);
        }

        public List<FactureWinBIZ> GetPaiements()
        {
            return _PaiementManager.GetPaiements();
        }

        /*public List<int> GetExercices()
        {
            return _PaiementManager.GetExercices();
        }*/

        public List<Adresse> GetProducteurs()
        {
            return _AdresseManager.GetProducteurs();
        }

        public void InsertWinBizAdrIds(CoupleIdsGvWinBIZ[] ids)
        {
            _AdresseManager.InsertWinBizAdrIds(ids);
        }

        public void InsertWinBizPaiIds(List<FactureWinBIZ> paiements)
        {
            _PaiementManager.UpdateWinBizPaiIds(paiements);
        }

        public void Sauvegarder(string path)
        {
            _SauvegardeManager.Sauvegarder(path);
        }

        public void Restaurer(string path)
        {
            _SauvegardeManager.Restaurer(path);
            UpdateBD();
        }

        public void RestoreToNewDb(string pathSauvegarde, string pathDB, string nomDB, bool isMaster = false)
        {
            _SauvegardeManager.RestaurerToNewDB(pathSauvegarde, pathDB, nomDB, isMaster);
        }

        public void UpdateModeComptabilisations()
        {
            Exception exception = null;
            foreach (int annee in BUSINESS.Utilities.AnneeIds)
            {
                try
                {
                    UpdateModeComptabilisations(annee);
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                    exception = ex;
                }
            }
            if (exception != null)
                throw exception;
        }

        public void UpdateModeComptabilisations(int annId)
        {
            _PaiementManager.UpdateModeComptabilisations(new WinBizControleur(annId).GetModeComptabilisations(), annId);
        }

        public void UpdateMontantPaiement(int paiId)
        {
            _PaiementManager.UpdateMontantPaiement(paiId);
        }

        public void GenererArticles(int annId)
        {
            _ArticleManager.Generer(annId);
        }

        public void GrouperArticles(List<int> artIds)
        {
            _ArticleManager.Grouper(artIds);
        }

        public void DissocierArticle(int artId)
        {
            _ArticleManager.Dissocier(artId);
        }

        public void UpdateWinBizArticleIds(List<int> winbizArticleIds, int annId)
        {
            _ArticleManager.UpdateWinBizArticleIds(winbizArticleIds, annId);
        }
    }
}