﻿using System.Data.SqlClient;
using System.IO;

namespace Gestion_Des_Vendanges.DAO
{
    internal class BDMasterManager
    {
        public static bool IsDataBaseExist(string connectionString)
        {
            bool result;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "SELECT COUNT(*) FROM DAT_DATABASE";
                result = (int)command.ExecuteScalar() > 0;
            }
            catch
            {
                result = false;
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        public static void CreateDataBase(string connectionString, string path = null)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand();
            try
            {
                connection.Open();
                string bdName = "GV_MASTER_" + BUSINESS.Utilities.Version;
                command.Connection = connection;
                //Création de la base de données
                command.CommandText = "CREATE DATABASE " + bdName;
                if (path != null)
                {
                    //TEST MDIFICATION SIZE DATABASE
                    //command.CommandText += " ON  PRIMARY ( NAME = N'" + bdName + "', FILENAME = N'" + Path.Combine(path, "GV_MASTER_VD.mdf") + "' , SIZE = 3072KB , FILEGROWTH = 1024KB ) " +
                    //"LOG ON  " +
                    //"( NAME = N'" + bdName + "_log', FILENAME = N'" + Path.Combine(path, "GV_MASTER_VD_log.ldf") + "' , SIZE = 1024KB , FILEGROWTH = 10%)";

                    command.CommandText += " ON  PRIMARY ( NAME = N'" + bdName + "', FILENAME = N'" + Path.Combine(path, "GV_MASTER_VD.mdf") + "'  ) " +
                   "LOG ON  " +
                   "( NAME = N'" + bdName + "_log', FILENAME = N'" + Path.Combine(path, "GV_MASTER_VD_log.ldf") + "' )";
                }
                command.ExecuteNonQuery();
                //Création de la table "DAT_DATABASE"
                command.CommandText = "USE [GV_MASTER_" + BUSINESS.Utilities.Version + "] " +
                    "SET ANSI_NULLS ON " +
                    "SET QUOTED_IDENTIFIER ON " +
                    "SET ANSI_PADDING ON " +
                    "CREATE TABLE [dbo].[DAT_DATABASE]( " +
                    "[DAT_ID] [int] IDENTITY(1,1) NOT NULL, " +
                    "[DAT_NOM] [varchar](50) NOT NULL, " +
                    "[DAT_DESCRIPTION] [varchar](50) NULL, " +
                    "CONSTRAINT [PK_DAT_ID] PRIMARY KEY CLUSTERED " +
                    "( " +
                    "[DAT_ID] ASC " +
                    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                    ") ON [PRIMARY] " +
                    "SET ANSI_PADDING OFF;";
                command.ExecuteNonQuery();
                //Création de la table "LIC_LICENSE"
                command.CommandText = "USE [GV_MASTER_" + BUSINESS.Utilities.Version + "] " +
                    "SET ANSI_NULLS ON " +
                    "SET QUOTED_IDENTIFIER ON " +
                    "SET ANSI_PADDING ON " +
                    "CREATE TABLE [dbo].[LIC_LICENSE]( " +
                    "[LIC_ID] [int] IDENTITY(1,1) NOT NULL, " +
                    "[LIC_CLE] [varchar](50) NULL, " +
                    "[LIC_HASH_MACHINE] [varchar](50) NULL, " +
                    "[LIC_NUM_ENCAVEUR] [int] NULL, " +
                    "[LIC_EXPIRATION] [varbinary](50) NULL, " +
                    "[LIC_LAST_LICENSE] [int] NULL, " +
                     "[LIC_HASH_VERSION] [varchar](50) NULL, " +
                     "[LIC_EXPIRATION_UPDATE] [varbinary](50) NULL, " +
                    "[LIC_HECTO_LITRE] [varbinary](50) NULL, " +
                    "[LIC_NB_PRODUCTEUR] [varbinary](50) NULL, " +
                    "[LIC_NOM_MACHINE] [varchar](50) NULL, " +
                    "[DAT_ID] [int] NULL, " +
                    "[LIC_LAST_ANNEE] [int] NULL, " +
                    "[LIC_PATH_BD] [varchar](50) NULL, " +
                    "CONSTRAINT [PK_LIC_LICENSE] PRIMARY KEY CLUSTERED " +
                    "( " +
                    "	[LIC_ID] ASC " +
                    ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] " +
                    ") ON [PRIMARY] " +
                    "SET ANSI_PADDING OFF;";
                command.ExecuteNonQuery();
                //Ajout de la clé étrangère
                command.CommandText = "USE [GV_MASTER_" + BUSINESS.Utilities.Version + "] " +
                    "ALTER TABLE [dbo].[LIC_LICENSE]  WITH CHECK ADD  CONSTRAINT [FK_LIC_LICENSE_DAT_DATABASE] FOREIGN KEY([DAT_ID]) " +
                    "REFERENCES [dbo].[DAT_DATABASE] ([DAT_ID]);";
                command.ExecuteNonQuery();
                command.CommandText = "USE [GV_MASTER_" + BUSINESS.Utilities.Version + "] " +
                   "ALTER TABLE [dbo].[LIC_LICENSE] CHECK CONSTRAINT [FK_LIC_LICENSE_DAT_DATABASE];";
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public static void RecuperationLicense(string masterBdNom, string gvBdNom)
        {
            SqlConnection connection = new SqlConnection(ConnectionManager.Instance.MasterConnectionString.Replace(ConnectionManager.Instance.MasterDbName, ""));
            SqlCommand command = new SqlCommand();
            try
            {
                connection.Open();
                command.Connection = connection;
                //Suppression de la première ligne vide
                command.CommandText = "delete from " + masterBdNom + ".dbo.LIC_LICENSE;";
                command.ExecuteNonQuery();
                //Création de la base de données
                command.CommandText = "insert into " + masterBdNom + ".dbo.LIC_LICENSE (LIC_CLE, LIC_HASH_MACHINE, LIC_NUM_ENCAVEUR, LIC_EXPIRATION, LIC_LAST_LICENSE, " +
                    "LIC_HASH_VERSION, LIC_EXPIRATION_UPDATE, LIC_NB_PRODUCTEUR, LIC_NOM_MACHINE) " +
                    "SELECT PAM_CLE, PAM_HASH_MACHINE, PAM_NUM_ENCAVEUR, PAM_EXPIRATION, PAM_LAST_LICENSE, PAM_HASH_VERSION, " +
                    "PAM_EXPIRATION_UPDATE, PAM_NB_PRODUCTEUR, PAM_MACHINE_NAME FROM " + gvBdNom + ".dbo.PAM_PARAMETRES;";
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
    }
}