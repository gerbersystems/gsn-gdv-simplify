﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.DAO
{
    /*
    *  Description: Applique la synchronisation des adresses dans la Gestion des vendanges
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class BDVendangeAdresseManager
    {
        private ADR_ADRESSETableAdapter _adrTable;
        private PRO_PROPRIETAIRETableAdapter _proTable;

        //private MPR_MOC_PROTableAdapter _mprTable;
        private DSPesees _dsPesees;

        public BDVendangeAdresseManager()
        {
            _adrTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ADR_ADRESSETableAdapter>();
            _proTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PRO_PROPRIETAIRETableAdapter>();
            //_mprTable = new MPR_MOC_PROTableAdapter();
            _dsPesees = new DSPesees();
        }

        public void ImporterAdresses(List<Adresse> adresses)
        {
            setNullReferenceDeleted(adresses);
            foreach (Adresse adresse in adresses)
            {
                int? adrId = _adrTable.GetAdrIdByAdrIdWinBiz(adresse.Numero);
                if (adrId != null)
                    updateAdresse((int)adrId, adresse);
                else
                    insertAdresse(adresse);
            }
        }

        private void setNullReferenceDeleted(List<Adresse> adresses)
        {
            _adrTable.FillWinBIZId(new DSPesees.ADR_ADRESSEDataTable());
            _proTable.FillWinBIZId(new DSPesees.PRO_PROPRIETAIREDataTable());
            foreach (DSPesees.ADR_ADRESSERow row in _adrTable.GetDataWinBIZId())
            {
                bool find = false;
                for (int i = 0; i < adresses.Count && !find; i++)
                {
                    if (row.ADR_ID_WINBIZ == adresses[i].Numero)
                        find = true;
                }
                if (!find)
                {
                    _adrTable.SetNullAdrIdWinBiz(row.ADR_ID);
                    _proTable.SetNullWinbizID(row.ADR_ID_WINBIZ);
                }
            }
        }

        private void updateAdresse(int adrId, Adresse adresse)
        {
            _adrTable.UpdateByAdrId(adresse.Rue1, adresse.Rue2, string.Empty, adresse.Npa, adresse.Ville, adresse.Telephone, adresse.Fax, adresse.Mobile, adresse.Email, adresse.Pays, adresse.Numero, adrId);
            if (_proTable.UpdateByAdrIdWinBIZ(adresse.Nom, adresse.Prenom, adresse.Titre, adresse.Societe, adresse.NoTVA, adresse.ModeTVA, adresse.Numero) == 0)
            {
                _proTable.Insert(adrId, adresse.Nom, adresse.Prenom, adresse.Titre, adresse.Societe, true, true, adresse.NoTVA, adresse.Numero, adresse.ModeTVA);
            }
        }

        private void insertAdresse(Adresse adresse)
        {
            _adrTable.Insert(adresse.Rue1, adresse.Rue2, string.Empty, adresse.Npa, adresse.Ville, adresse.Telephone, adresse.Fax, adresse.Mobile, adresse.Email, adresse.Pays, adresse.Numero);
            int adrId = (int)_adrTable.GetMaxAdrId();
            _proTable.Insert(adrId, adresse.Nom, adresse.Prenom, adresse.Titre, adresse.Societe, true, true, adresse.NoTVA, adresse.Numero, adresse.ModeTVA);
        }

        public List<Adresse> GetProducteurs()
        {
            List<Adresse> adresses = new List<Adresse>();
            _proTable.FillProducteurByAnnId(_dsPesees.PRO_PROPRIETAIRE, Utilities.IdAnneeCourante);
            foreach (DSPesees.PRO_PROPRIETAIRERow proRow in _proTable.GetProducteurByAnnId(Utilities.IdAnneeCourante))
            {
                Adresse adresse = new Adresse();
                _adrTable.FillByAdrId(_dsPesees.ADR_ADRESSE, proRow.ADR_ID);
                DSPesees.ADR_ADRESSERow adrRow = _adrTable.GetDataByAdrId(proRow.ADR_ID)[0];
                try
                {
                    adresse.NumeroDecimal = proRow.ADR_ID_WINBIZ;
                }
                catch { }
                adresse.IdAdrGv = proRow.ADR_ID;
                adresse.IdProGv = proRow.PRO_ID;
                try
                {
                    adresse.Email = adrRow.ADR_EMAIL;
                }
                catch { }
                try
                {
                    adresse.Fax = adrRow.ADR_FAX;
                }
                catch { }
                try
                {
                    adresse.Mobile = adrRow.ADR_NATEL;
                }
                catch { }
                try
                {
                    adresse.Nom = proRow.PRO_NOM;
                }
                catch { }
                try
                {
                    adresse.Npa = adrRow.ADR_NPA;
                }
                catch { }
                try
                {
                    adresse.Pays = adrRow.ADR_PAYS;
                }
                catch { }
                try
                {
                    adresse.Prenom = proRow.PRO_PRENOM;
                }
                catch { }
                try
                {
                    adresse.Rue1 = adrRow.ADR_ADRESSE1;
                }
                catch { }
                try
                {
                    adresse.Rue2 = adrRow.ADR_ADRESSE2;
                }
                catch { }
                try
                {
                    adresse.Societe = proRow.PRO_SCTE;
                }
                catch { }
                try
                {
                    adresse.Telephone = adrRow.ADR_TEL;
                }
                catch { }
                try
                {
                    adresse.Titre = proRow.PRO_TITRE;
                }
                catch { }
                try
                {
                    adresse.Ville = adrRow.ADR_VILLE;
                }
                catch { }
                try
                {
                    adresse.NoTVA = proRow.PRO_NUMERO_TVA;
                }
                catch { }
                adresses.Add(adresse);
            }
            return adresses;
        }

        public void InsertWinBizAdrIds(CoupleIdsGvWinBIZ[] ids)
        {
            foreach (CoupleIdsGvWinBIZ coupleId in ids)
            {
                int adrId = (int)_proTable.GetAdrId(coupleId.ProId);
                int? adrIdWinBIZ = _adrTable.GetAdrIdWinBIZ(adrId);
                //int? winbizId = _proTable.GetAdrIdWinBIZ(coupleId.ProId);
                //Duplique l'adresse si le winbizId de l'adresse ne correspond pas à celui du propriétaire
                //Ainsi les adresses sont liées à un seul enregistrement dans WinBIZ
                if (adrIdWinBIZ != null && adrIdWinBIZ != coupleId.AdrIdWinBiz)
                {
                    _adrTable.CopyAdresse(adrId);
                    adrId = (int)_adrTable.GetMaxAdrId();
                    _proTable.UpdateAdrId(adrId, coupleId.ProId);
                }
                _adrTable.SetAdrIdWinBIZ(coupleId.AdrIdWinBiz, adrId);
                _proTable.SetAdrIdWinBIZ(coupleId.AdrIdWinBiz, coupleId.ProId);
            }
        }
    }
}