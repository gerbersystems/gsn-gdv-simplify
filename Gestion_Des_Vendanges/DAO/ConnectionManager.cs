﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using Gestion_Des_Vendanges.GUI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using CustomConnectionStringBuilder = System.Data.SqlClient.SqlConnectionStringBuilder;

namespace Gestion_Des_Vendanges.DAO
{
    /*
    *  Description: Gère le changement de base de données
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    * */

    public class ConnectionManager : GSN.GDV.Business.IConnectionManager
    {
        private log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ConnectionManager));

        private interface ITableAdapter
        {
            System.Data.SqlClient.SqlConnection Connection { get; }
        }

        private List<object> _gvTableAdapters = new List<object>();

        static ConnectionManager()
        {
            if (Instance == null)
                Init();
        }

        public DSMasterTableAdapters.DAT_DATABASETableAdapter DatTable { get; private set; }
        public DSMasterTableAdapters.LIC_LICENSETableAdapter LicTable { get; private set; }
        public string MasterConnectionString { get; private set; }
        public string GVConnectionString { get; private set; }
        public static ConnectionManager Instance { get; private set; }

        public string GvDBName
        {
            get { return NameByConnectionString(GVConnectionString); }
        }

        public string GvDBDescription
        {
            get { return DatTable.GetDescriptionByNom(GvDBName); }
        }

        public string MasterDbName
        {
            get { return NameByConnectionString(MasterConnectionString); }
        }

        private ConnectionManager()
        {
            Log.Info("ConnectionManager()");
            if (!ConfigFileManager.FileExists)
            {
                Log.Info("!ConfigFileManager.FileExists, using ConfigFileManager.DefaultValue ");
                WfrmConnectionStringMaster frmConnection = new WfrmConnectionStringMaster(ConfigFileManager.DefaultValue);
                frmConnection.ShowDialog();
                ConfigFileManager.ServerAdress = frmConnection.ConnexionString;
            }
            //Log.InfoFormat("ConfigFileManager.ServerAdress:{0}", ConfigFileManager.ServerAdress);
            MasterConnectionString = ConfigFileManager.ServerAdress;
            DatTable = new DSMasterTableAdapters.DAT_DATABASETableAdapter();
            DatTable.Connection.ConnectionString = MasterConnectionString;

            LicTable = new DSMasterTableAdapters.LIC_LICENSETableAdapter();
            LicTable.Connection.ConnectionString = MasterConnectionString;

            Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.Init(this);
        }

        private static object initLocker = new object();

        public static void Init()
        {
            lock (initLocker)
            {
                Instance = new ConnectionManager();
            }
        }

        public void ChangeDBGv(string name)
        {
            Log.InfoFormat("ChangeDBGv({0})", name);
            //GVConnectionString = MasterConnectionString.Replace(
            //    MasterConnectionString.Substring(
            //        MasterConnectionString.ToUpper().IndexOf("INITIAL CATALOG="))
            //        .Split(';')[0]
            //        .Split('=')[1], name);
            var csb = new CustomConnectionStringBuilder(MasterConnectionString)
            {
                InitialCatalog = name,
                PersistSecurityInfo = true
            };
            GVConnectionString = csb.ToString();
            LicTable.SetDatId(Utilities.LicId, name);
            GSN.GDV.Data.Contextes.MainDataContext.AvailableConnectionString = GVConnectionString;
            foreach (var tableAdapter in _gvTableAdapters)
                SetConnectionStringToGvTableAdapter(tableAdapter);
        }

        public T CreateGvTableAdapter<T>()
            where T : new()
        {
            var tableAdapter = new T();
            AddGvTableAdapter(tableAdapter);
            return tableAdapter;
        }

        public void AddGvTableAdapter(object tableAdapter)
        {
            Log.InfoFormat("AddGvTableAdapter({0})", tableAdapter);
            SetConnectionStringToGvTableAdapter(tableAdapter);
            _gvTableAdapters.Add(tableAdapter);
        }

        private void SetConnectionStringToGvTableAdapter(object tableAdapter)
        {
            Type t = tableAdapter.GetType();
            var method = t.GetMethod("get_Connection", BindingFlags.Instance | BindingFlags.NonPublic);
            if (method == null)
                method = t.GetMethod("get_Connection");
            var cnn = ((SqlConnection)method.Invoke(tableAdapter, null));
            cnn.ConnectionString = GVConnectionString;
            var log = log4net.LogManager.GetLogger(tableAdapter.GetType());
            cnn.InfoMessage += delegate (object sender, SqlInfoMessageEventArgs e)
            {
                log.InfoFormat("InfoMessage({0}, {1})", sender, e);
            };
            //Log.InfoFormat("SetConnectionStringToGvTableAdapter({0}):{1}", tableAdapter, cnn.ConnectionString);
        }

        public void InitGvConnectionString(int licId)
        {
            var licTable = new DSMasterTableAdapters.LIC_LICENSETableAdapter();
            var datTable = new DSMasterTableAdapters.DAT_DATABASETableAdapter();
            licTable.Connection.ConnectionString = MasterConnectionString;
            datTable.Connection.ConnectionString = MasterConnectionString;
            string nom = licTable.GetDatNom(licId);
            if (nom == null)
            {
                nom = datTable.GetFirstNom();
            }
            ChangeDBGv(nom);
            Log.InfoFormat("InitGvConnectionString({0}):{1}", licId, nom);
            //**************************
            if (!this.IsAccessible())
            {
                var dataBasesAvailable = datTable.GetData();

                foreach (var o in dataBasesAvailable)
                {
                    if (o == null || string.IsNullOrWhiteSpace(o.DAT_NOM))
                        continue;
                    ChangeDBGv(o.DAT_NOM.Trim());
                    if (this.IsAccessible())
                        break;
                }
                //WfrmSelectionInitialDB selectionDossier = new WfrmSelectionInitialDB(_dataBasesList);
                //selectionDossier.Show();
            }
            //**************************
        }

        private bool IsAccessible()
        {
            try
            {
                SqlConnection _connection = new SqlConnection(ConnectionManager.Instance.GVConnectionString);
                PAM_PARAMETRESTableAdapter _pamTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();
                _connection.Open();
                int currentVersion = (int)_pamTable.GetVersionBD();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string NameByConnectionString(string connectionString)
        {
            //return connectionString.Substring(connectionString.ToUpper().IndexOf("INITIAL CATALOG=")).Split(';')[0].Split('=')[1];
            var csb = new CustomConnectionStringBuilder(connectionString);
            return csb.InitialCatalog;
        }

        public GSN.GDV.Data.Contextes.MainDataContext CreateMainDataContext()
        {
            var r = new GSN.GDV.Data.Contextes.MainDataContext(GVConnectionString);
            var log = log4net.LogManager.GetLogger(typeof(GSN.GDV.Data.Contextes.MainDataContext));
            r.Database.Log = (message) => { log.Info(message); };
            return r;
        }
    }
}