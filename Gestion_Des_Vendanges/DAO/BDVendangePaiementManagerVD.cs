﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;

namespace Gestion_Des_Vendanges.DAO
{
    partial class BDVendangePaiementManager
    {
        private PED_PESEE_DETAILTableAdapter _pedTable;
        private MCO_MOC_COUTableAdapter _mcoTable;

        private void InitSpecificTableAdapters()
        {
            _pedTable = ConnectionManager.Instance.CreateGvTableAdapter<PED_PESEE_DETAILTableAdapter>();
            _mcoTable = ConnectionManager.Instance.CreateGvTableAdapter<MCO_MOC_COUTableAdapter>();
        }

        private void AddLignesPesees(DSPesees.PAI_PAIEMENTRow rowPai, FactureWinBIZ paiement)
        {
            foreach (DSPesees.LIP_LIGNE_PAIEMENT_PESEERow rowLip in _lipTable.GetDataByPai(rowPai.PAI_ID))
            {
                _pedTable.FillByPed(_dsPesees.PED_PESEE_DETAIL, rowLip.PED_ID);
                DSPesees.PED_PESEE_DETAILRow rowPed = _pedTable.GetDataByPed(rowLip.PED_ID)[0];
                LigneDetailWinBIZ lignePaiement = new LigneDetailWinBIZ();
                ViewArticleTableAdapter viewArticle = DAO.ConnectionManager.Instance.CreateGvTableAdapter<ViewArticleTableAdapter>();
                lignePaiement.PrixUnitaire = Convert.ToDecimal(rowLip.LIP_PRIX_KG);

                if (Utilities.IsPrixKg)
                {
                    lignePaiement.Quantite = Convert.ToDecimal(rowPed.PED_QUANTITE_KG);
                    lignePaiement.Unite = "Kg";
                }
                else
                {
                    lignePaiement.Quantite = Convert.ToDecimal(rowPed.PED_QUANTITE_LITRES);
                    lignePaiement.Unite = "Litre";
                }
                lignePaiement.MontantHT = Convert.ToDecimal(rowLip.LIP_MONTANT_HT);
                lignePaiement.MontantTTC = Convert.ToDecimal(rowLip.LIP_MONTANT_TTC);
                lignePaiement.DescriptionArticle = getDescriptionArticle(rowPed);

                lignePaiement.MocIdWinBiz = Convert.ToInt32(_mocTable.GetMocIdWinBiz(rowLip.MOC_ID));

                if (rowLip.MOD_ID == 1)
                {
                    lignePaiement.TVAInclBool = true;
                    lignePaiement.TauxTVA = 0;
                    lignePaiement.TypeTVA = 1;
                }
                else
                {
                    lignePaiement.TauxTVA = Convert.ToDecimal(rowLip.LIP_TAUX_TVA);
                    lignePaiement.TypeTVA = 3;
                    lignePaiement.TVAInclBool = (rowLip.MOD_ID == 2);
                }

                paiement.AddLigneDetail(lignePaiement);
            }
        }

        private string getDescriptionArticle(DSPesees.PED_PESEE_DETAILRow rowPED)
        {
            _peeTable.FillByPed(_dsPesees.PEE_PESEE_ENTETE, rowPED.PED_ID);
            DSPesees.PEE_PESEE_ENTETERow rowPee = _peeTable.GetDataByPed(rowPED.PED_ID)[0];
            string an = BUSINESS.Utilities.AnneeCourante.ToString();
            string cepage = BUSINESS.Utilities.GetCepage(rowPED.CEP_ID);
            return GetDescriptionArticle(rowPee, an, cepage);
        }

        private void DeleteSpecificMocReferences(int mocId)
        {
            _mcoTable.DeleteByMocId(mocId);
        }
    }
}