﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;

namespace Gestion_Des_Vendanges.DAO
{
    internal class WinBizExerciceManager
    {
        private OleDbConnection _sysConnection;
        private OleDbConnection _connection;

        public WinBizExerciceManager(string pathCurrentFolder)
        {
            try
            {
                _sysConnection = new OleDbConnection();
                _connection = new OleDbConnection();
                DirectoryInfo directory = new DirectoryInfo(pathCurrentFolder);
                directory = directory.Parent;
                _connection.ConnectionString = WinBizControleur.GetConnectionString(directory.FullName);
                directory = directory.Parent.Parent.GetDirectories("SYS", SearchOption.TopDirectoryOnly)[0];
                _sysConnection.ConnectionString = WinBizControleur.GetConnectionString(directory.FullName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WinBizExercice> GetExercices()
        {
            List<WinBizExercice> exercices = new List<WinBizExercice>();
            try
            {
                _connection.Open();
                _sysConnection.Open();
                int numeroDossier;
                using (OleDbCommand command = _connection.CreateCommand())
                {
                    command.CommandText = "SELECT DOS_NUMERO FROM dos_info";
                    numeroDossier = Convert.ToInt32(command.ExecuteScalar());
                }
                using (OleDbCommand command = _sysConnection.CreateCommand())
                {
                    command.CommandText = "SELECT EXC_DEBUT, EXC_FIN, EXC_ANNEE FROM exercice " +
                        "WHERE EXC_DOSSIE = " + numeroDossier + ";";
                    using (OleDbDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            DateTime dateDebut = dataReader.GetDateTime(0);
                            DateTime dateFin = dataReader.GetDateTime(1);
                            int annee = Convert.ToInt32(dataReader.GetDecimal(2));
                            exercices.Add(new WinBizExercice(annee, dateDebut, dateFin));
                        }
                    }
                }
            }
            finally
            {
                _connection.Close();
                _sysConnection.Close();
            }
            return exercices;
        }
    }
}