﻿using Gestion_Des_Vendanges.BUSINESS;
using GSN.GDV.Data.Extensions;
using GSN.GDV.Data.Models;
using GSN.GDV.Helpers;
using System;
using System.Data.SqlClient;
using System.IO;

namespace Gestion_Des_Vendanges.DAO
{
    partial class BDVendangeUpdateManager
    {
        public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(BDVendangeUpdateManager));
        private const int VERSION = 43;

        private void Update1()
        {
            //Ajout de la table REP_REPORT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "CREATE TABLE [dbo].[REP_REPORT]( "
                + "[REP_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[REP_NOM] [varchar](50) NULL, "
                + "[REP_VERSION] [int] NULL "
                + ") ON [PRIMARY] "
                + "SET ANSI_PADDING OFF ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update2()
        {
            //Ajout du champ ANN_ENCAVEUR_SOCIETE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ANN_ANNEE] ADD ANN_ENCAVEUR_SOCIETE VARCHAR(50);";
            _command.ExecuteNonQuery();
        }

        private void Update3()
        {
            //Ajout du champ CLA_NUMERO
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[CLA_CLASSE] ADD CLA_NUMERO INT; ";
            _command.ExecuteNonQuery();
            //Initialisation de CLA_NUMERO
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE [dbo].[CLA_CLASSE] SET CLA_NUMERO = 1; "
                + "UPDATE [dbo].[CLA_CLASSE] SET CLA_NUMERO = 2 WHERE CLA_ID = 3;"
                + "UPDATE [dbo].[CLA_CLASSE] SET CLA_NUMERO = 3 WHERE CLA_ID = 10;";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update4()
        {
            //Create table COA_COMMUNE_ADRESSE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "CREATE TABLE [dbo].[COA_COMMUNE_ADRESSE]( "
                + "[COA_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[COA_NPA] [int] NOT NULL, "
                + "[COA_NOM] [varchar](50) NOT NULL, "
                + "CONSTRAINT [PK_COA_COMMUNE_ADRESSE] PRIMARY KEY CLUSTERED "
                + "( "
                + "[COA_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "SET ANSI_PADDING OFF ";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Insertion des éléments dans la table COA_COMMUNE_ADRESSE
            BUSINESS.FichierCSV fichierCSV = new FichierCSV(BUSINESS.Utilities.ApplicationPath + @"\Resources\CommuneSuisse.csv");
            string[,] fileCommuneSuisse = fichierCSV.GetFichier();
            for (int i = 0; i < fileCommuneSuisse.Length / 2; i++)
            {
                _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                    + "SET ANSI_NULLS ON "
                    + "SET QUOTED_IDENTIFIER ON "
                    + "SET ANSI_PADDING ON "
                    + "INSERT INTO [dbo].[COA_COMMUNE_ADRESSE] ([COA_NPA], [COA_NOM]) "
                    + "VALUES(" + int.Parse(fileCommuneSuisse[i, 0]) + ", '" + BUSINESS.Utilities.FormatStringToSQL(fileCommuneSuisse[i, 1]) + "') "
                    + "SET ANSI_PADDING OFF;";

                Log.Info("Execution of " + _command.CommandText);
                _command.ExecuteNonQuery();
            }
            //Création de la table PAI_PAIEMENT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "CREATE TABLE [dbo].[PAI_PAIEMENT]( "
                + "[PAI_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[PAI_TEXTE] [varchar](50) NULL, "
                + "[PAI_DATE] [datetime] NULL, "
                + "[ANN_ID] [int] NULL, "
                + "[PAI_NUMERO] [varchar](50) NULL, "
                + "[PRO_ID] [int] NULL, "
                + "[PAI_ACOMPTE] [bit] NULL, "
                + "[PAI_POURCENTAGE] [float] NULL, "
                + "CONSTRAINT [PK_PAI_PAIEMENT] PRIMARY KEY CLUSTERED "
                + "( "
                + "[PAI_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "SET ANSI_PADDING OFF "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT]  WITH CHECK ADD  CONSTRAINT [FK_PAI_PAIEMENT_ANN_ANNEE] FOREIGN KEY([ANN_ID]) "
                + "REFERENCES [dbo].[ANN_ANNEE] ([ANN_ID]) "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] CHECK CONSTRAINT [FK_PAI_PAIEMENT_ANN_ANNEE] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT]  WITH CHECK ADD  CONSTRAINT [FK_PAI_PAIEMENT_PRO_PROPRIETAIRE] FOREIGN KEY([PRO_ID]) "
                + "REFERENCES [dbo].[PRO_PROPRIETAIRE] ([PRO_ID]) "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] CHECK CONSTRAINT [FK_PAI_PAIEMENT_PRO_PROPRIETAIRE]";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Création de la table LIP_LIGNE_PAIEMENT_PESEE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "CREATE TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE]( "
                + "[LIP_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[PED_ID] [int] NULL, "
                + "[PAI_ID] [int] NULL, "
                + "[LIP_PRIX_KG] [float] NULL, "
                + "[LIP_MONTANT_TTC] [float] NULL, "
                + "[LIP_MONTANT_HT] [float] NULL, "
                + "CONSTRAINT [PK_LIP_LIGNE_PAIEMENT_PESEE] PRIMARY KEY CLUSTERED "
                + "( "
                + "[LIP_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE]  WITH CHECK ADD  CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_PAI_PAIEMENT] FOREIGN KEY([PAI_ID]) "
                + "REFERENCES [dbo].[PAI_PAIEMENT] ([PAI_ID]) "
                + "ON UPDATE CASCADE "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] CHECK CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_PAI_PAIEMENT] "
                + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE]  WITH CHECK ADD  CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_PED_PESEE_DETAIL] FOREIGN KEY([PED_ID]) "
                + "REFERENCES [dbo].[PED_PESEE_DETAIL] ([PED_ID]) "
                + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] CHECK CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_PED_PESEE_DETAIL]";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Création de la table LIM_LIGNE_PAIEMENT_MANUELLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "CREATE TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE]( "
                + "[LIM_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[LIM_TEXTE] [varchar](50) NULL, "
                + "[PAI_ID] [int] NULL, "
                + "[LIM_MONTANT_HT] [float] NULL, "
                + "[LIM_MONTANT_TTC] [float] NULL, "
                + "CONSTRAINT [PK_LIM_LIGNE_PAIEMENT_MANUEL] PRIMARY KEY CLUSTERED "
                + "( "
                + "[LIM_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "SET ANSI_PADDING OFF "
                + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE]  WITH CHECK ADD  CONSTRAINT [FK_LIM_LIGNE_PAIEMENT_MANUEL_PAI_PAIEMENT] FOREIGN KEY([PAI_ID]) "
                + "REFERENCES [dbo].[PAI_PAIEMENT] ([PAI_ID]) "
                + "ON UPDATE CASCADE "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] CHECK CONSTRAINT [FK_LIM_LIGNE_PAIEMENT_MANUEL_PAI_PAIEMENT]";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ ADR_ID_WINBIZ à la table ADR_ADRESSE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ADR_ADRESSE] ADD ADR_ID_WINBIZ INT;";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ PAM_NB_PRODUCTEUR
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PAM_PARAMETRES] ADD PAM_NB_PRODUCTEUR VARBINARY(50);";

            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ PRO_IS_PROPRIETAIRE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD PRO_IS_PROPRIETAIRE BIT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ PRO_IS_PRODUCTEUR
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD PRO_IS_PRODUCTEUR BIT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Set les valeurs de IS_PROPRIETAIRE ET IS_PRODUCTEUR
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE [dbo].[PRO_PROPRIETAIRE] "
                + "SET PRO_IS_PROPRIETAIRE = 1, PRO_IS_PRODUCTEUR = 1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Modification de la table PAM_PARAMETRE pour gérer le multi-poste
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "ALTER TABLE [dbo].[PAM_PARAMETRES] ADD [PAM_ID] [int] IDENTITY(1,1) NOT NULL "
                + "ALTER TABLE [dbo].[PAM_PARAMETRES] ADD [PAM_MACHINE_NAME] [varchar](50) NULL "
                + "ALTER TABLE [dbo].[PAM_PARAMETRES] ADD  CONSTRAINT [PK_PAM_PARAMETRES] PRIMARY KEY CLUSTERED "
                + "( "
                + "	[PAM_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + "SET ANSI_PADDING OFF;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialise le champ "PAM_MACHINE_NAME"
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE [dbo].[PAM_PARAMETRES] "
                + "SET PAM_MACHINE_NAME = '" + Environment.MachineName + "';";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout de la table MOD_MODE_TVA
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "SET ANSI_PADDING ON "
                + "CREATE TABLE [dbo].[MOD_MODE_TVA]( "
                + "[MOD_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[MOD_NOM] [varchar](50) NOT NULL, "
                + " CONSTRAINT [PK_MOD_MODE_TVA] PRIMARY KEY CLUSTERED "
                + "( [MOD_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "SET ANSI_PADDING OFF;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout des champs de gestion de la TVA
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ANN_ANNEE] ADD [ANN_TAUX_TVA] [FLOAT] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD [PAI_ID_WINBIZ] [INT] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD [PAI_TAUX_TVA] [FLOAT] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD [MOD_ID] [INT] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD [MOD_ID] [INT] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD [PRO_NUMERO_TVA] varchar(50);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout des references des champs MOD_ID
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT]  WITH CHECK ADD  CONSTRAINT [FK_PAI_PAIEMENT_MOD_MODE_TVA] FOREIGN KEY([MOD_ID]) "
                + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]) "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] CHECK CONSTRAINT [FK_PAI_PAIEMENT_MOD_MODE_TVA] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE]  WITH CHECK ADD  CONSTRAINT [FK_PRO_PROPRIETAIRE_PRO_PROPRIETAIRE] FOREIGN KEY([MOD_ID]) "
                + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]) "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] CHECK CONSTRAINT [FK_PRO_PROPRIETAIRE_PRO_PROPRIETAIRE];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Insertion des MODES de TVA
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "INSERT INTO [dbo].[MOD_MODE_TVA] VALUES('Sans TVA') "
                + "INSERT INTO [dbo].[MOD_MODE_TVA] VALUES('TVA Incluse') "
                + "INSERT INTO [dbo].[MOD_MODE_TVA] VALUES('TVA Exclue');";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout de la vue ViewPaiement
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "CREATE VIEW [dbo].[ViewPaiement] "
                + "AS "
                + "SELECT PAI_ID, PAI_TEXTE, PAI_DATE, ANN_ID, PAI_NUMERO, PRO_ID, PAI_ACOMPTE, PAI_ID_WINBIZ, PAI_TAUX_TVA, MOD_ID, PAI_POURCENTAGE, "
                + "TAUXFORTTC, TAUXFORHT, MONTANTLIM_HT, MONTANTLIM_TTC, MONTANTLIP_HT, MONTANTLIP_TTC, "
                + "MONTANTLIM_HT + MONTANTLIP_HT AS MONTANT_HT, MONTANTLIM_TTC + MONTANTLIP_TTC AS MONTANT_TTC, "
                + "ROUND(MONTANTLIM_TTC + MONTANTLIP_TTC - MONTANTLIM_HT - MONTANTLIP_HT, 2) AS TVA "
                + "FROM (SELECT PAI.PAI_ID, PAI.PAI_TEXTE, PAI.PAI_DATE, PAI.ANN_ID, PAI.PAI_NUMERO, PAI.PRO_ID, PAI.PAI_ACOMPTE, PAI.PAI_ID_WINBIZ, "
                + "PAI.PAI_TAUX_TVA, ISNULL(TAUXHT.PAI_ID, 1) AS TAUXFORHT, ISNULL(TAUXTTC.TAUXTTC, 1) AS TAUXFORTTC, PAI.MOD_ID, "
                + "PAI.PAI_POURCENTAGE, ISNULL(LIM.MONTANTHT, 0) AS MONTANTLIM_HT, ISNULL(LIM.MONTANTTTC, 0) AS MONTANTLIM_TTC, "
                + "ISNULL(LIP_1.MONTANTHT, 0) AS MONTANTLIP_HT, ISNULL(LIP_1.MONTANTTTC, 0) AS MONTANTLIP_TTC "
                + "FROM dbo.PAI_PAIEMENT AS PAI LEFT OUTER JOIN "
                + "(SELECT     PAI_ID, SUM(LIM_MONTANT_TTC) AS MONTANTTTC, SUM(LIM_MONTANT_HT) AS MONTANTHT "
                + "FROM          dbo.LIM_LIGNE_PAIEMENT_MANUELLE "
                + " GROUP BY PAI_ID) AS LIM ON PAI.PAI_ID = LIM.PAI_ID LEFT OUTER JOIN "
                + "(SELECT     LIP.PAI_ID, SUM(LIP.LIP_MONTANT_HT) AS MONTANTHT, SUM(LIP.LIP_MONTANT_TTC) AS MONTANTTTC "
                + " FROM          dbo.LIP_LIGNE_PAIEMENT_PESEE AS LIP LEFT OUTER JOIN "
                + "   dbo.PED_PESEE_DETAIL AS PED ON LIP.PED_ID = PED.PED_ID "
                + " GROUP BY LIP.PAI_ID) AS LIP_1 ON PAI.PAI_ID = LIP_1.PAI_ID LEFT OUTER JOIN "
                + "(SELECT     PAI_ID, 100 / (100 + PAI_TAUX_TVA) AS TAUXHT "
                + " FROM          dbo.PAI_PAIEMENT "
                + " WHERE      (MOD_ID = 2)) AS TAUXHT ON PAI.PAI_ID = TAUXHT.PAI_ID LEFT OUTER JOIN "
                + "(SELECT     PAI_ID, (PAI_TAUX_TVA + 100) / 100 AS TAUXTTC "
                + "FROM          dbo.PAI_PAIEMENT AS PAI_PAIEMENT_1 "
                + " WHERE      (MOD_ID = 3)) AS TAUXTTC ON PAI.PAI_ID = TAUXTTC.PAI_ID) AS PAI_1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop QUO_QUOTA
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QUO_QUOT_AVOIR9_ANN_ANNE]') AND parent_object_id = OBJECT_ID(N'[dbo].[QUO_QUOTA]')) "
                + "ALTER TABLE [dbo].[QUO_QUOTA] DROP CONSTRAINT [FK_QUO_QUOT_AVOIR9_ANN_ANNE] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QUO_QUOT_POUR_CEP_CEPA]') AND parent_object_id = OBJECT_ID(N'[dbo].[QUO_QUOTA]')) "
                + "ALTER TABLE [dbo].[QUO_QUOTA] DROP CONSTRAINT [FK_QUO_QUOT_POUR_CEP_CEPA] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QUO_QUOT_POUR2_CLA_CLAS]') AND parent_object_id = OBJECT_ID(N'[dbo].[QUO_QUOTA]')) "
                + "ALTER TABLE [dbo].[QUO_QUOTA] DROP CONSTRAINT [FK_QUO_QUOT_POUR2_CLA_CLAS] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QUO_QUOT_POUR3_REG_REGI]') AND parent_object_id = OBJECT_ID(N'[dbo].[QUO_QUOTA]')) "
                + "ALTER TABLE [dbo].[QUO_QUOTA] DROP CONSTRAINT [FK_QUO_QUOT_POUR3_REG_REGI] "
                + "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QUO_QUOTA]') AND type in (N'U')) "
                + "DROP TABLE [dbo].[QUO_QUOTA]";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop MEN_MENTION
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MEN_MENTION_AUT_AUTRE_MENTION]') AND parent_object_id = OBJECT_ID(N'[dbo].[MEN_MENTION]')) "
                + "ALTER TABLE [dbo].[MEN_MENTION] DROP CONSTRAINT [FK_MEN_MENTION_AUT_AUTRE_MENTION] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MEN_MENTION_COM_COMMUNE]') AND parent_object_id = OBJECT_ID(N'[dbo].[MEN_MENTION]')) "
                + "ALTER TABLE [dbo].[MEN_MENTION] DROP CONSTRAINT [FK_MEN_MENTION_COM_COMMUNE] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MEN_MENTION_LIE_LIEU_DE_PRODUCTION]') AND parent_object_id = OBJECT_ID(N'[dbo].[MEN_MENTION]')) "
                + "ALTER TABLE [dbo].[MEN_MENTION] DROP CONSTRAINT [FK_MEN_MENTION_LIE_LIEU_DE_PRODUCTION] "
                + "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MEN_MENTION]') AND type in (N'U')) "
                + "DROP TABLE [dbo].[MEN_MENTION];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View MET_MENTIONTXT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MET_MENTIONTXT]')) "
                + "DROP VIEW [dbo].[MET_MENTIONTXT];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View ProdByComAppCep
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ProdByComAppCep]')) "
                + "DROP VIEW [dbo].[ProdByComAppCep];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View RapportEncavage
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[RapportEncavage]')) "
                + "DROP VIEW [dbo].[RapportEncavage];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View ViewReportAcquits
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ViewReportAcquits]')) "
                + "DROP VIEW [dbo].[ViewReportAcquits];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View ViewReportEncavage
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ViewReportEncavage]')) "
                + "DROP VIEW [dbo].[ViewReportEncavage];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop Table REP_REPORT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[REP_REPORT]') AND type in (N'U')) "
                + "DROP TABLE [dbo].[REP_REPORT];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Drop View ViewReportAcquits
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ViewReportPesee]')) "
                + "DROP VIEW [dbo].[ViewReportPesee];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ ADR_ID_WINBIZ à la table PRO_PROPRIETAIRE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD ADR_ID_WINBIZ INT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ COU_CEPAGE à la table COU_COULEUR
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[COU_COULEUR] ADD COU_CEPAGE BIT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation du champ COU_CEPAGE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE [dbo].[COU_COULEUR] SET COU_CEPAGE = 'TRUE';";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout des types de vins
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "INSERT INTO [dbo].[COU_COULEUR](COU_NOM, COU_CEPAGE) VALUES('Rosé', 'FALSE');";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "INSERT INTO [dbo].[COU_COULEUR](COU_NOM, COU_CEPAGE) VALUES('Vin mousseux', 'FALSE');";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "INSERT INTO [dbo].[COU_COULEUR](COU_NOM, COU_CEPAGE) VALUES('Autres', 'FALSE');";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ CLA_NUMERO_CSCV
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[CLA_CLASSE] ADD CLA_NUMERO_CSCV VARCHAR(50);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation du champ CLA_NUMERO_CSCV
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE CLA_CLASSE SET CLA_NUMERO_CSCV = '3000';";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE CLA_CLASSE SET CLA_NUMERO_CSCV = '1101' WHERE CLA_NUMERO = 1;";
            _command.ExecuteNonQuery();
            Log.Info("Execution of " + _command.CommandText);
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE CLA_CLASSE SET CLA_NUMERO_CSCV = '2100' WHERE CLA_NUMERO = 2;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update5()
        {
            //create table MOC_MODE_COMPTABILISATION
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "SET ANSI_NULLS ON "
               + "SET QUOTED_IDENTIFIER ON "
               + "SET ANSI_PADDING ON "
               + "CREATE TABLE [dbo].[MOC_MODE_COMPTABILISATION]( "
               + "[MOC_ID] [int] IDENTITY(1,1) NOT NULL, "
               + "[MOC_ID_WINBIZ] [int] NOT NULL, "
               + "[MOC_TEXTE] [varchar](50) NULL, "
               + "[MOC_TAUX_TVA] [float] NULL, "
               + "[MOD_ID] [int] NOT NULL, "
               + "[ANN_ID] [int] NOT NULL, "
               + "CONSTRAINT [PK_MOC_MODE_COMPTABILISATION] PRIMARY KEY CLUSTERED "
               + "( "
               + "[MOC_ID] ASC "
               + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
               + ") ON [PRIMARY] "
               + "SET ANSI_PADDING OFF "
               + "ALTER TABLE [dbo].[MOC_MODE_COMPTABILISATION]  WITH CHECK ADD  CONSTRAINT [FK_MOC_MODE_COMPTABILISATION_ANN_ANNEE] FOREIGN KEY([ANN_ID]) "
               + "REFERENCES [dbo].[ANN_ANNEE] ([ANN_ID]) ON DELETE CASCADE "
               + "ALTER TABLE [dbo].[MOC_MODE_COMPTABILISATION] CHECK CONSTRAINT [FK_MOC_MODE_COMPTABILISATION_ANN_ANNEE] "
               + "ALTER TABLE [dbo].[MOC_MODE_COMPTABILISATION]  WITH CHECK ADD  CONSTRAINT [FK_MOC_MODE_COMPTABILISATION_MOD_MODE_TVA] FOREIGN KEY([MOD_ID]) "
               + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]) "
               + "ALTER TABLE [dbo].[MOC_MODE_COMPTABILISATION] CHECK CONSTRAINT [FK_MOC_MODE_COMPTABILISATION_MOD_MODE_TVA];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Create Table MCE_MOC_CEP
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "CREATE TABLE [dbo].[MCE_MOC_CEP]( "
                + "[MCE_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[MOC_ID] [int] NOT NULL, "
                + "[CEP_ID] [int] NOT NULL, "
                + "CONSTRAINT [PK_MCE_MOC_CEP] PRIMARY KEY CLUSTERED "
                + "( "
                + "[MCE_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "ALTER TABLE [dbo].[MCE_MOC_CEP]  WITH CHECK ADD  CONSTRAINT [FK_MCE_MOC_CEP_CEP_CEPAGE] FOREIGN KEY([CEP_ID]) "
                + "REFERENCES [dbo].[CEP_CEPAGE] ([CEP_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MCE_MOC_CEP] CHECK CONSTRAINT [FK_MCE_MOC_CEP_CEP_CEPAGE] "
                + "ALTER TABLE [dbo].[MCE_MOC_CEP]  WITH CHECK ADD  CONSTRAINT [FK_MCE_MOC_CEP_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
                + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MCE_MOC_CEP] CHECK CONSTRAINT [FK_MCE_MOC_CEP_MOC_MODE_COMPTABILISATION];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Create Table MCO_MOC_COU
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "CREATE TABLE [dbo].[MCO_MOC_COU]( "
                + "[MCO_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[MOC_ID] [int] NOT NULL, "
                + "[COU_ID] [int] NOT NULL, "
                + "CONSTRAINT [PK_MCO_MOC_COU] PRIMARY KEY CLUSTERED "
                + "( "
                + "[MCO_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "ALTER TABLE [dbo].[MCO_MOC_COU]  WITH CHECK ADD  CONSTRAINT [FK_MCO_MOC_COU_COU_COULEUR] FOREIGN KEY([COU_ID]) "
                + "REFERENCES [dbo].[COU_COULEUR] ([COU_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MCO_MOC_COU] CHECK CONSTRAINT [FK_MCO_MOC_COU_COU_COULEUR] "
                + "ALTER TABLE [dbo].[MCO_MOC_COU]  WITH CHECK ADD  CONSTRAINT [FK_MCO_MOC_COU_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
                + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MCO_MOC_COU] CHECK CONSTRAINT [FK_MCO_MOC_COU_MOC_MODE_COMPTABILISATION];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Add MOC_ID to ANN, LIM, LIP
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "ALTER TABLE [dbo].[ANN_ANNEE] ADD MOC_ID INT "
               + "ALTER TABLE [dbo].[ANN_ANNEE] WITH CHECK ADD CONSTRAINT [FK_ANN_ANNEE_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
               + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]) "
               + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] ADD MOC_ID INT "
               + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] WITH CHECK ADD CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
               + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]) "
               + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] ADD MOC_ID INT "
               + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] WITH CHECK ADD CONSTRAINT [FK_LIM_LIGNE_PAIEMENT_MANUELLE_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
               + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Add MOD_ID, TAUX TO LIP et LIM
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] ADD MOD_ID INT "
               + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] WITH CHECK ADD CONSTRAINT [FK_LIM_LIGNE_PAIEMENT_MANUELLE_MOD_MODE_TVA] FOREIGN KEY([MOD_ID]) "
               + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]) "
               + "ALTER TABLE [dbo].[LIM_LIGNE_PAIEMENT_MANUELLE] ADD LIM_TAUX_TVA FLOAT "
               + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] ADD MOD_ID INT "
               + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] WITH CHECK ADD CONSTRAINT [FK_LIP_LIGNE_PAIEMENT_PESEE_MOD_MODE_TVA] FOREIGN KEY([MOD_ID]) "
               + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]) "
               + "ALTER TABLE [dbo].[LIP_LIGNE_PAIEMENT_PESEE] ADD LIP_TAUX_TVA FLOAT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Remove TAUX, MOD_ID FROM PAI
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] DROP COLUMN PAI_TAUX_TVA "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] DROP FK_PAI_PAIEMENT_MOD_MODE_TVA "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] DROP COLUMN MOD_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Remove TAUX FROM ANN
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ANN_ANNEE] DROP COLUMN ANN_TAUX_TVA;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Remove MOD_ID TO PRO
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] DROP CONSTRAINT [FK_PRO_PROPRIETAIRE_PRO_PROPRIETAIRE] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] DROP COLUMN MOD_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Create Table MPR_MOC_PRO
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON "
                + "CREATE TABLE [dbo].[MPR_MOC_PRO]( "
                + "[MPR_ID] [int] IDENTITY(1,1) NOT NULL, "
                + "[MOC_ID] [int] NOT NULL, "
                + "[PRO_ID] [int] NOT NULL, "
                + "CONSTRAINT [PK_MPR_MOC_PRO] PRIMARY KEY CLUSTERED "
                + "( "
                + "[MPR_ID] ASC "
                + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
                + ") ON [PRIMARY] "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO]  WITH CHECK ADD  CONSTRAINT [FK_MPR_MOC_PRO] FOREIGN KEY([PRO_ID]) "
                + "REFERENCES [dbo].[PRO_PROPRIETAIRE] ([PRO_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO] CHECK CONSTRAINT [FK_MPR_MOC_PRO] "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO]  WITH CHECK ADD  CONSTRAINT [FK_MCO_MOC_PRO_MOC] FOREIGN KEY([MOC_ID]) "
                + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]) "
                + "ON DELETE CASCADE "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO] CHECK CONSTRAINT [FK_MCO_MOC_PRO_MOC];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update6()
        {
            //Ajout de MOD_ID à PRO_PROPRIETAIRE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] ADD MOD_ID INT "
                + "ALTER TABLE [dbo].[PRO_PROPRIETAIRE] WITH CHECK ADD CONSTRAINT [FK_PRO_PROPRIETAIRE_MOD_MODE_TVA] FOREIGN KEY([MOD_ID]) "
                + "REFERENCES [dbo].[MOD_MODE_TVA] ([MOD_ID]);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Suppression de MPR_MOC_PRO
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MCO_MOC_PRO_MOC]') AND parent_object_id = OBJECT_ID(N'[dbo].[MPR_MOC_PRO]')) "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO] DROP CONSTRAINT [FK_MCO_MOC_PRO_MOC] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MPR_MOC_PRO]') AND parent_object_id = OBJECT_ID(N'[dbo].[MPR_MOC_PRO]')) "
                + "ALTER TABLE [dbo].[MPR_MOC_PRO] DROP CONSTRAINT [FK_MPR_MOC_PRO] "
                + "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MPR_MOC_PRO]') AND type in (N'U')) "
                + "DROP TABLE [dbo].[MPR_MOC_PRO];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout des champs MONTANT_NET ET TTC à PAI_PAIEMENT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD PAI_MONTANT_LIM_NET FLOAT "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD PAI_MONTANT_LIM_TTC FLOAT "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD PAI_MONTANT_LIP_NET FLOAT "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD PAI_MONTANT_LIP_TTC FLOAT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout du champ Solde à PAI_PAIEMENT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[PAI_PAIEMENT] ADD PAI_SOLDE FLOAT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Suppression de ViewPaiement
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ViewPaiement]')) "
                + "DROP VIEW [dbo].[ViewPaiement];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update7()
        {
            //Ajout de MOC_ID à ART_ARTICLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
              + "ALTER TABLE [dbo].[ART_ARTICLE] ADD MOC_ID INT "
              + "ALTER TABLE [dbo].[ART_ARTICLE] WITH CHECK ADD CONSTRAINT [FK_ART_ARTICLE_MOC_MODE_COMPTABILISATION] FOREIGN KEY([MOC_ID]) "
              + "REFERENCES [dbo].[MOC_MODE_COMPTABILISATION] ([MOC_ID]);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Modification de ViewWinBizExportArticle
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "ALTER VIEW [dbo].[ViewWinBizExportArticle] AS "
                + "SELECT ART.ART_CODE, ART.ART_CODE_GROUPE + ' - ' + COU.COU_NOM AS GROUPE, ART.ART_DESIGNATION_COURTE, ART.ART_DESIGNATION_LONGUE, "
                + "ART.ART_LITRES, ART.ART_CODE_GROUPE, ANN.ANN_AN, ART.ART_ETAT_TRANSFERT, ART.ART_ID_WINBIZ, ART.ART_ID, "
                + "ISNULL(MOC.MOC_ID_WINBIZ, 0) AS MOC_ID_WINBIZ "
                + "FROM dbo.ART_ARTICLE AS ART LEFT OUTER JOIN "
                + "dbo.COU_COULEUR AS COU ON ART.COU_ID = COU.COU_ID LEFT OUTER JOIN "
                + "dbo.ANN_ANNEE AS ANN ON ART.ANN_ID = ANN.ANN_ID LEFT OUTER JOIN "
                + "dbo.MOC_MODE_COMPTABILISATION AS MOC ON ART.MOC_ID = MOC.MOC_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update8()
        {
            //Création du lien (LIE: St-Saphorin avec la commune Chardonne)
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
              + "SELECT COUNT(*) FROM LIC_LIEU_COMMUNE WHERE LIE_ID = 11 AND COM_ID = 277;";
            Log.Info("Execution of " + _command.CommandText);
            int nb = Convert.ToInt32(_command.ExecuteScalar());
            if (nb == 0)
            {
                _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                    + "INSERT INTO LIC_LIEU_COMMUNE VALUES(277, 11);";
                Log.Info("Execution of " + _command.CommandText);
                _command.ExecuteNonQuery();
            }
            //Suppression de la table CON_CONVERSION
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CON_CONVERSION_ANN_ANNEE]') AND parent_object_id = OBJECT_ID(N'[dbo].[CON_CONVERSION]')) "
                + "ALTER TABLE [dbo].[CON_CONVERSION] DROP CONSTRAINT [FK_CON_CONVERSION_ANN_ANNEE] "
                + "IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CON_CONVERSION_COU_COULEUR]') AND parent_object_id = OBJECT_ID(N'[dbo].[CON_CONVERSION]')) "
                + "ALTER TABLE [dbo].[CON_CONVERSION] DROP CONSTRAINT [FK_CON_CONVERSION_COU_COULEUR] "
                + "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CON_CONVERSION]') AND type in (N'U')) "
                + "DROP TABLE [dbo].[CON_CONVERSION];";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update9()
        {
            //Ajout de ART_ID à PED
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
              + "ALTER TABLE [dbo].[PED_PESEE_DETAIL] ADD ART_ID INT "
              + "ALTER TABLE [dbo].[PED_PESEE_DETAIL] WITH CHECK ADD CONSTRAINT [FK_PED_PESEE_DETAIL_ART_ARTICLE] FOREIGN KEY([ART_ID]) "
              + "REFERENCES [dbo].[ART_ARTICLE] ([ART_ID]);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Ajout de la table ASS_ASSOCIATION_ARTICLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "SET ANSI_NULLS ON "
               + "SET QUOTED_IDENTIFIER ON "
               + "SET ANSI_PADDING ON "
               + "CREATE TABLE [dbo].[ASS_ASSOCIATION_ARTICLE]( "
               + "[ASS_ID] [int] IDENTITY(1,1) NOT NULL, "
               + "[ASS_NOM_COLONNE] [varchar](50) NOT NULL, "
               + "[ASS_SELECTION] [bit] NOT NULL, "
               + "CONSTRAINT [PK_ASS_ASSOCIATION_ARTICLE] PRIMARY KEY CLUSTERED "
               + "( "
               + "[ASS_ID] ASC "
               + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] "
               + ") ON [PRIMARY] "
               + "SET ANSI_PADDING OFF;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Insertion des données dans ASS_ASSOCIATION_ARTICLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('CLA_ID', 'TRUE') "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('CEP_ID', 'TRUE') "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('LIE_ID', 'TRUE') "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('COM_ID', 'TRUE') "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('AUT_ID', 'TRUE') "
                //  + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('GRO_ID', 'TRUE') "
                + "INSERT INTO ASS_ASSOCIATION_ARTICLE VALUES('PRO_ID_VENDANGE', 'FALSE');";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Suppression de ART_ETAT_TRANSFERT
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ART_ARTICLE] DROP COLUMN ART_ETAT_TRANSFERT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout de ART_LITRES_STOCK à ART_ARTICLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
              + "ALTER TABLE [dbo].[ART_ARTICLE] ADD ART_LITRES_STOCK float ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation de ART_LITRES_STOCK
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE ART_ARTICLE SET ART_LITRES_STOCK = ART_LITRES;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Ajout de ART_VERSION à ART_ARTICLE
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ART_ARTICLE] ADD ART_VERSION INT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation de ART_VERSION
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE ART_ARTICLE SET ART_VERSION = 1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Modification de ViewArticle
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "ALTER VIEW [dbo].[ViewArticle] AS "
                + "SELECT     PED.PED_ID, ACQ.ANN_ID, ACQ.COM_ID, ACQ.CLA_ID, PEE.AUT_ID, PEE.LIE_ID, PEE.PEE_GRANDCRU, PED.CEP_ID, PED.PED_QUANTITE_LITRES AS LITRES, "
                + "ACQ.COU_ID, PED.ART_ID, ACQ.PRO_ID_VENDANGE "
                + "FROM         dbo.PED_PESEE_DETAIL AS PED INNER JOIN "
                + "dbo.PEE_PESEE_ENTETE AS PEE ON PED.PEE_ID = PEE.PEE_ID INNER JOIN "
                + "dbo.ACQ_ACQUIT AS ACQ ON ACQ.ACQ_ID = PEE.ACQ_ID INNER JOIN "
                + "dbo.COU_COULEUR AS COU ON COU.COU_ID = ACQ.COU_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Modification de ViewBonusMax
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "ALTER VIEW [dbo].[ViewBonusMax] AS "
                + "SELECT     BON1.BON_ID, ISNULL(BON2.BON_OE, 999) AS BON_OE_FIN, ISNULL(BON2.BON_BRIX, 100) AS BON_BRIX_FIN "
                + "FROM         dbo.BON_BONUS AS BON1 LEFT OUTER JOIN "
                + "dbo.BON_BONUS AS BON2 ON BON2.BON_OE IN "
                + "(SELECT     MIN(BON_OE) AS Expr1 "
                + "FROM          dbo.BON_BONUS "
                + "WHERE      (BON_OE > BON1.BON_OE) AND (VAL_ID = BON1.VAL_ID));";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Update ViewWinBizExportArticle
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "ALTER VIEW [dbo].[ViewWinBizExportArticle] AS "
                + "SELECT     ART.ART_CODE, ART.ART_CODE_GROUPE + ' - ' + COU.COU_NOM AS GROUPE, ART.ART_DESIGNATION_COURTE, ART.ART_DESIGNATION_LONGUE, "
                + "ART.ART_LITRES, ART.ART_CODE_GROUPE, ANN.ANN_AN, ART.ART_ID_WINBIZ, ART.ART_ID, "
                + "ISNULL(MOC.MOC_ID_WINBIZ, 0) AS MOC_ID_WINBIZ, ART.ART_LITRES_STOCK "
                + "FROM         dbo.ART_ARTICLE AS ART LEFT OUTER JOIN "
                + "dbo.COU_COULEUR AS COU ON ART.COU_ID = COU.COU_ID LEFT OUTER JOIN "
                + "dbo.ANN_ANNEE AS ANN ON ART.ANN_ID = ANN.ANN_ID LEFT OUTER JOIN "
                + "dbo.MOC_MODE_COMPTABILISATION AS MOC ON ART.MOC_ID = MOC.MOC_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update10()
        {
            //Modification de la colonne PAR_FOLIO en VARCHAR(50)
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
              + "ALTER TABLE PAR_PARCELLE ALTER COLUMN PAR_FOLIO VARCHAR(50);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update11()
        {
            //Modification de ViewWinBizExportArticle
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "SET ANSI_NULLS ON "
                + "SET QUOTED_IDENTIFIER ON ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "ALTER VIEW [dbo].[ViewDeclarationEncavage] AS " +
                "SELECT     DROIT.ANN_ID, COM.COM_NOM, CEP.CEP_NOM, LIE.LIE_NOM + ', ' + AUT.AUT_NOM AS MENTION, CLA.CLA_NOM, DROIT.DROITPRODUCTION, " +
                "PROD.OEAvgAOC, PROD.OEAvgGC, PROD.ProductionAOC, PROD.ProductionGC, CLA.CLA_ID " +
                "FROM         dbo.CLA_CLASSE AS CLA LEFT OUTER JOIN " +
                "dbo.COM_COMMUNE AS COM INNER JOIN " +
                "(SELECT     ACQ.CLA_ID, ACQ.ANN_ID, ACQ.COM_ID, PAR.CEP_ID, PAR.AUT_ID, PAR.LIE_ID, SUM(PAR.PAR_LITRES) AS DROITPRODUCTION " +
                " FROM          dbo.ACQ_ACQUIT AS ACQ INNER JOIN " +
                "              dbo.PAR_PARCELLE AS PAR ON ACQ.ACQ_ID = PAR.ACQ_ID " +
                "GROUP BY ACQ.COM_ID, PAR.CEP_ID, PAR.AUT_ID, PAR.LIE_ID, ACQ.ANN_ID, ACQ.CLA_ID) AS DROIT LEFT OUTER JOIN " +
                " (SELECT     ACQ.ANN_ID, ACQ.COM_ID, PEE.AUT_ID, PEE.LIE_ID, PED.CEP_ID, ACQ.CLA_ID, SUM(AOC.AOC_LITRES) AS ProductionAOC, " +
                "   SUM(GRANDCRU.GRANDCRU_LITRES) AS ProductionGC, SUM(AOC.AOC_OE * AOC.AOC_LITRES) / SUM(AOC.AOC_LITRES) AS OEAvgAOC, " +
                " SUM(GRANDCRU.GRANDCRU_OE * GRANDCRU.GRANDCRU_LITRES) / SUM(GRANDCRU.GRANDCRU_LITRES) AS OEAvgGC " +
                "  FROM          dbo.ACQ_ACQUIT AS ACQ INNER JOIN " +
                "   dbo.PEE_PESEE_ENTETE AS PEE ON PEE.ACQ_ID = ACQ.ACQ_ID INNER JOIN " +
                "dbo.PED_PESEE_DETAIL AS PED ON PEE.PEE_ID = PED.PEE_ID LEFT OUTER JOIN " +
                "      (SELECT     PEE.AUT_ID, PED.PED_ID, PEE.ACQ_ID, PED.PED_QUANTITE_LITRES AS AOC_LITRES, " +
                "  PED.PED_SONDAGE_OE AS AOC_OE " +
                "FROM          dbo.PEE_PESEE_ENTETE AS PEE INNER JOIN " +
                "           dbo.PED_PESEE_DETAIL AS PED ON PEE.PEE_ID = PED.PEE_ID " +
                "WHERE      (PEE.PEE_GRANDCRU = 0)) AS AOC ON PED.PED_ID = AOC.PED_ID LEFT OUTER JOIN " +
                "    (SELECT     PED.PED_ID, PEE.ACQ_ID, PED.PED_QUANTITE_LITRES AS GRANDCRU_LITRES, " +
                "                          PED.PED_SONDAGE_OE AS GRANDCRU_OE " +
                "FROM          dbo.PEE_PESEE_ENTETE AS PEE INNER JOIN " +
                " dbo.PED_PESEE_DETAIL AS PED ON PEE.PEE_ID = PED.PEE_ID " +
                " WHERE      (PEE.PEE_GRANDCRU = 1)) AS GRANDCRU ON GRANDCRU.PED_ID = PED.PED_ID " +
                "GROUP BY ACQ.COM_ID, PEE.AUT_ID, PEE.LIE_ID, PED.CEP_ID, ACQ.CLA_ID, ACQ.ANN_ID) AS PROD ON PROD.ANN_ID = DROIT.ANN_ID AND " +
                "PROD.CEP_ID = DROIT.CEP_ID AND PROD.LIE_ID = DROIT.LIE_ID AND PROD.COM_ID = DROIT.COM_ID AND PROD.AUT_ID = DROIT.AUT_ID AND " +
                "  PROD.CLA_ID = DROIT.CLA_ID INNER JOIN " +
                "dbo.ANN_ANNEE AS ANN ON DROIT.ANN_ID = ANN.ANN_ID INNER JOIN " +
                "    dbo.AUT_AUTRE_MENTION AS AUT ON DROIT.AUT_ID = AUT.AUT_ID INNER JOIN " +
                "  dbo.LIE_LIEU_DE_PRODUCTION AS LIE ON LIE.LIE_ID = DROIT.LIE_ID INNER JOIN " +
                "dbo.CEP_CEPAGE AS CEP ON DROIT.CEP_ID = CEP.CEP_ID ON COM.COM_ID = DROIT.COM_ID ON CLA.CLA_ID = DROIT.CLA_ID;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update12()
        {
            //Ajout de la colonne "Réception" à la table acquit
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE dbo.ACQ_ACQUIT ADD ACQ_RECEPTION DATETIME NULL;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation de la colonne "Réception"
            _command.CommandText = "UPDATE dbo.ACQ_ACQUIT SET ACQ_RECEPTION = ACQ_DATE;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update13()
        {
            //Modification des noms de classes (premier grand cru)
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "UPDATE dbo.CLA_CLASSE SET CLA_NOM = 'Classe 1 - Premier grand cru' WHERE CLA_ID = 2 AND CLA_NUMERO = 1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //AOC
            _command.CommandText = "UPDATE dbo.CLA_CLASSE SET CLA_NOM = 'Classe 1 - AOC et Grand cru' WHERE CLA_NUMERO = 1 AND CLA_ID = 1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Vin de pays
            _command.CommandText = "UPDATE dbo.CLA_CLASSE SET CLA_NOM = 'Classe 2 - Vin de pays' WHERE CLA_NUMERO = 2;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Vin de table
            _command.CommandText = "UPDATE dbo.CLA_CLASSE SET CLA_NOM = 'Classe 3 - Vin de table' WHERE CLA_NUMERO = 3;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update14()
        {
            //Ajout du champ ANN_IS_PRIX_KG
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ANN_ANNEE] ADD ANN_IS_PRIX_KG BIT;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Initialisation de ANN_IS_PRIX_KG à true
            _command.CommandText = "UPDATE [dbo].[ANN_ANNEE] SET ANN_IS_PRIX_KG = 1;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        private void Update15()
        {
            //Lecture du fichier "UpdateOCV" contenant les commandes de mise à jour des références OCV (commune, lieu, cépage)
            string[] updateFile = System.IO.File.ReadAllLines(BUSINESS.Utilities.ApplicationPath + @"\Resources\UpdateOCV.sql");
            foreach (string commande in updateFile)
            {
                _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                    + commande;
                Log.Info("Execution of " + _command.CommandText);
                _command.ExecuteNonQuery();
            }
        }

        private void Update16()
        {
            //Lecture du fichier "UpdateOCV2" contenant les commandes de mise à jour des références OCV (commune, lieu, cépage)
            string[] updateFile = System.IO.File.ReadAllLines(BUSINESS.Utilities.ApplicationPath + @"\Resources\UpdateOCV2.sql");
            foreach (string commande in updateFile)
            {
                try
                {
                    _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                        + commande;
                    Log.Info("Execution of " + _command.CommandText);
                    _command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                }
            }
        }

        //Update17 : ajout 2 colonnes de paramètre pour taux de conversion globale entre kilo/litre pour vin blanc et rouge
        // + Table SET_SETTINGS pour les paramètres globaux de l'application
        // + Table QUO_QUOTA_PRODUCTION pour stocker la liste officiel "QUANTITES DE PRODUCTION MAXIMALES DE RAISINS
        private void Update17()
        {
            //Ajout du champ ANN_TAUX_CONVERTION_BLANC & ANN_TAUX_CONVERTION_ROUGE DECIMAL pour stock taux de conversion par année
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE [dbo].[ANN_ANNEE] ADD ANN_TAUX_CONVERTION_BLANC DECIMAL(3,2) NOT NULL CONSTRAINT ANN_TAUX_CONVERTION_BLANC DEFAULT 0.8;"
                + "ALTER TABLE [dbo].[ANN_ANNEE] ADD ANN_TAUX_CONVERTION_ROUGE DECIMAL(3,2) NOT NULL CONSTRAINT ANN_TAUX_CONVERTION_ROUGE DEFAULT 0.8;";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Add table SET_SETTINGS for global settings
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "SET ANSI_NULLS ON "
               + "SET QUOTED_IDENTIFIER ON "
               + "CREATE TABLE [dbo].[SET_SETTING](\n"
               + " [SET_ID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,\n"
               + " [SET_NAME] [nvarchar](64) NOT NULL,\n"
               + " [SET_VALUE_STRING] [nvarchar](256) NULL default null,\n"
               + " [SET_VALUE_INTEGER] int NULL default null,\n"
               + " [SET_VALUE_DECIMAL] float NULL default null,\n"
               + " [SET_VALUE_DATETIME] datetime NULL default null,\n"
               + " [SET_VALUE_BOOLEAN] bit NULL default 0,\n"
               + " [SET_TYPE] int NOT NULL default(0)\n"
               + " ) ON [PRIMARY]";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "insert into SET_SETTING(SET_NAME, SET_TYPE, SET_VALUE_INTEGER, SET_VALUE_BOOLEAN)VALUES('" + EnumHelper.GetName(SettingExtension.SettingEnum.ApplicationUnite) + "', " + ((int)AbstractSettingModel.TypeEnum.Integer) + ", 0, NULL);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "insert into SET_SETTING(SET_NAME, SET_TYPE, SET_VALUE_BOOLEAN)VALUES('" + EnumHelper.GetName(SettingExtension.SettingEnum.AcceptExceedingQuota) + "', " + ((int)AbstractSettingModel.TypeEnum.Boolean) + ", 0);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            _command.CommandText = "insert into SET_SETTING(SET_NAME, SET_TYPE, SET_VALUE_INTEGER, SET_VALUE_BOOLEAN)VALUES('" + EnumHelper.GetName(SettingExtension.SettingEnum.BonusType) + "', " + ((int)AbstractSettingModel.TypeEnum.Integer) + ", 0, NULL);";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Add table QUO_QUOTA_PRODUCTION pour tableau officiel des quantités maximales de raisins par Litres/m2
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
               + "SET ANSI_NULLS ON "
               + "SET QUOTED_IDENTIFIER ON "
               + "CREATE TABLE [dbo].[QUO_QUOTA_PRODUCTION]( "
               + "[QUO_ID] [int] IDENTITY(1,1) NOT NULL, "
               + "[CEP_ID] [int] NOT NULL, "
               + "[REG_ID] [int] NOT NULL, "
               + "[ANN_AN] [int] NOT NULL, "
               + "[QUO_AOC] [decimal](3, 2) NOT NULL, "
               + "[QUO_PREMIER_GRANDS_CRUS] [decimal](3, 2) NOT NULL, "
               + "[QUO_VINS_PAYS] [decimal](3, 2) NOT NULL, "
               + "[QUO_VINS_TABLE] [decimal](3, 2) NOT NULL "
               + ") ON [PRIMARY] ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            //Add type BON_BONUS pour table de bonus par prix CHF || pourcent
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "
                + "ALTER TABLE BON_BONUS "
                + "ADD BON_TYPE smallint not null DEFAULT 0";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update18 : de la colonne RES_DEFAULT
        /// Ajout du champ ANN_TAUX_CONVERTION_BLANC et ANN_TAUX_CONVERTION_ROUGE DECIMAL pour stock taux de conversion par année
        /// </summary>
        /// <see>Releases#Update 18</see>
        private void Update18()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table RES_RESPONSABLE add RES_DEFAULT bit default 0 not null;", '\n'
            );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update19 :
        /// </summary>
        /// <see>Releases#Update 19</see>
        private void Update19()
        {
            _command.CommandText = "USE [" + ConnectionManager.Instance.GvDBName + "] "

            + " DELETE FROM QUO_QUOTA_PRODUCTION"
            + " SET IDENTITY_INSERT [dbo].[QUO_QUOTA_PRODUCTION] ON  "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1878, 170, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1879, 171, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1880, 172, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1881, 173, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1882, 174, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1883, 175, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1884, 176, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1885, 177, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1886, 178, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1887, 179, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1888, 180, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1889, 181, 6, 2013, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1890, 182, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1891, 183, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1892, 184, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1893, 185, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1894, 186, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1895, 187, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1896, 188, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1897, 189, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1898, 190, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1899, 191, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1900, 192, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1901, 193, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1902, 194, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1903, 195, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1904, 196, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1905, 197, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1906, 198, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1907, 199, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1908, 200, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1909, 201, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1910, 203, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1911, 204, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1912, 205, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1913, 206, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1914, 207, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1915, 209, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1916, 132, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1917, 133, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1918, 134, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1919, 135, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1920, 136, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1921, 137, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1922, 138, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1923, 139, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1924, 140, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1925, 141, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1926, 142, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1927, 143, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1928, 144, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1929, 145, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1930, 146, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1931, 147, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1932, 148, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1933, 149, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1934, 150, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1935, 151, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1936, 152, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1937, 153, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1938, 154, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1939, 155, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1940, 156, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1941, 157, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1942, 158, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1943, 159, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1944, 160, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1945, 161, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1946, 162, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1947, 163, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1948, 164, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1949, 165, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1950, 166, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1951, 167, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1952, 168, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1953, 169, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1954, 170, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1955, 171, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1956, 172, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1957, 173, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1958, 174, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1959, 175, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1960, 176, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1961, 177, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1962, 178, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1963, 179, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1964, 180, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1965, 181, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1966, 182, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1967, 183, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1968, 184, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1969, 185, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1970, 186, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1971, 187, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1972, 188, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1973, 189, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1974, 190, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1975, 191, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1976, 192, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1977, 193, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1978, 194, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1979, 195, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1980, 196, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1981, 197, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1982, 198, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1983, 199, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1984, 200, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1985, 201, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1986, 203, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1987, 204, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1988, 205, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1989, 206, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1990, 207, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1991, 209, 8, 2013, CAST(0.00 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1992, 132, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1993, 133, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1994, 134, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1995, 135, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1996, 136, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1997, 137, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1998, 138, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1999, 139, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2000, 140, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2001, 141, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2002, 142, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2003, 143, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2004, 144, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2005, 145, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2006, 146, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2007, 147, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2008, 148, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2009, 149, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2010, 150, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2011, 151, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2012, 152, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2013, 153, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2014, 154, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2015, 155, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2016, 156, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2017, 157, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2018, 158, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2019, 159, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2020, 160, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2021, 161, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2022, 162, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2023, 163, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2024, 164, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2025, 165, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2026, 166, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2027, 167, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2028, 168, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2029, 169, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2030, 170, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2031, 171, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2032, 172, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2033, 173, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2034, 174, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2035, 175, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2036, 176, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2037, 177, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2038, 178, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2039, 179, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2040, 180, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2041, 181, 1, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2042, 182, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2043, 183, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2044, 184, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2045, 185, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2046, 186, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2047, 187, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2048, 188, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1707, 151, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1708, 152, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1709, 153, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1710, 154, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1711, 155, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1712, 156, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1713, 157, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1714, 158, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1715, 159, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1716, 160, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1717, 161, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1718, 162, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1719, 163, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1720, 164, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1721, 165, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1722, 166, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1723, 167, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1724, 168, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1725, 169, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1726, 170, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1727, 171, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1728, 172, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1729, 173, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1730, 174, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1731, 175, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1732, 176, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1733, 177, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1734, 178, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1735, 179, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1736, 180, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1737, 181, 4, 2013, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1738, 182, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1739, 183, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1740, 184, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1741, 185, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1742, 186, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1743, 187, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1744, 188, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1745, 189, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1746, 190, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1747, 191, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1748, 192, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1749, 193, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1750, 194, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1751, 195, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1752, 196, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1753, 197, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1754, 198, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1755, 199, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1756, 200, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1757, 201, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1758, 203, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1759, 204, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1760, 205, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1761, 206, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1762, 207, 4, 2013, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1763, 209, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1764, 132, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1765, 133, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1766, 134, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1767, 135, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1768, 136, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1769, 137, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1770, 138, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1771, 139, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1772, 140, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1773, 141, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1774, 142, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1775, 143, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1776, 144, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1777, 145, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1778, 146, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1779, 147, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1780, 148, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1781, 149, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1782, 150, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1783, 151, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1784, 152, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1785, 153, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1786, 154, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1787, 155, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1788, 156, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1789, 157, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1790, 158, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1791, 159, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1792, 160, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1793, 161, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1794, 162, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1795, 163, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1796, 164, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1797, 165, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1798, 166, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1799, 167, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1800, 168, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1801, 169, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1802, 170, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1803, 171, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1804, 172, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1805, 173, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1806, 174, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1807, 175, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1808, 176, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1809, 177, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1810, 178, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1811, 179, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1812, 180, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1813, 181, 5, 2013, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1814, 182, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1815, 183, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1816, 184, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1817, 185, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1818, 186, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1819, 187, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1820, 188, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1821, 189, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1822, 190, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1823, 191, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1824, 192, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1825, 193, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1826, 194, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1827, 195, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1828, 196, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1829, 197, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1830, 198, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1831, 199, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1832, 200, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1833, 201, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1834, 203, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1835, 204, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1836, 205, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1837, 206, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1838, 207, 5, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1839, 209, 5, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1840, 132, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1841, 133, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1842, 134, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1843, 135, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1844, 136, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1845, 137, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1846, 138, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1847, 139, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1848, 140, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1849, 141, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1850, 142, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1851, 143, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1852, 144, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1853, 145, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1854, 146, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1855, 147, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1856, 148, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1857, 149, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1858, 150, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1859, 151, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1860, 152, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1861, 153, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1862, 154, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1863, 155, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1864, 156, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1865, 157, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1866, 158, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1867, 159, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1868, 160, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1869, 161, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1870, 162, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1871, 163, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1872, 164, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1873, 165, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1874, 166, 6, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1875, 167, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1876, 168, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1877, 169, 6, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2049, 189, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2050, 190, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2051, 191, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2052, 192, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2053, 193, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2054, 194, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2055, 195, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2056, 196, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2057, 197, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2058, 198, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2059, 199, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2060, 200, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2061, 201, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2062, 203, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2063, 204, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2064, 205, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2065, 206, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2066, 207, 1, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2067, 209, 1, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2068, 132, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2069, 133, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2070, 134, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2071, 135, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2072, 136, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2073, 137, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2074, 138, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2075, 139, 2, 2014, CAST(0.96 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2076, 140, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2077, 141, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2078, 142, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2079, 143, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2080, 144, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2081, 145, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2082, 146, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2083, 147, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2084, 148, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2085, 149, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2086, 150, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2087, 151, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2088, 152, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2089, 153, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2090, 154, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2091, 155, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2092, 156, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2093, 157, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2094, 158, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2095, 159, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2096, 160, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2097, 161, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2098, 162, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2099, 163, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2100, 164, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2101, 165, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2102, 166, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2103, 167, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2104, 168, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2105, 169, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2106, 170, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2107, 171, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2108, 172, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2109, 173, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2110, 174, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2111, 175, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2112, 176, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2113, 177, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2114, 178, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2115, 179, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2116, 180, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2117, 181, 2, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2118, 182, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2119, 183, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2120, 184, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2121, 185, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2122, 186, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2123, 187, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2124, 188, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2125, 189, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2126, 190, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2127, 191, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2128, 192, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2129, 193, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2130, 194, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2131, 195, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2132, 196, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2133, 197, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2134, 198, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2135, 199, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2136, 200, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2137, 201, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2138, 203, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2139, 204, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2140, 205, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2141, 206, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2142, 207, 2, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2143, 209, 2, 2014, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2144, 132, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2145, 133, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2146, 134, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2147, 135, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2148, 136, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2149, 137, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2150, 138, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2151, 139, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2152, 140, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2153, 141, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2154, 142, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2155, 143, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2156, 144, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2157, 145, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2158, 146, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2159, 147, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2160, 148, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2161, 149, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2162, 150, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2163, 151, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2164, 152, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2165, 153, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2166, 154, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2167, 155, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2168, 156, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2169, 157, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2170, 158, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2171, 159, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2172, 160, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2173, 161, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2174, 162, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2175, 163, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2176, 164, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2177, 165, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2178, 166, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2179, 167, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2180, 168, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2181, 169, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2182, 170, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2183, 171, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2184, 172, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2185, 173, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2186, 174, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2187, 175, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2188, 176, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2189, 177, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2190, 178, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2191, 179, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2192, 180, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2193, 181, 4, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2194, 182, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2195, 183, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2196, 184, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2197, 185, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2198, 186, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2199, 187, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2200, 188, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2201, 189, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2202, 190, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2203, 191, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2204, 192, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2205, 193, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2206, 194, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2207, 195, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2208, 196, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2209, 197, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2210, 198, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2211, 199, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2212, 200, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2213, 201, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2214, 203, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2215, 204, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2216, 205, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2217, 206, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2218, 207, 4, 2014, CAST(0.70 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2219, 209, 4, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2220, 132, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2221, 133, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2222, 134, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2223, 135, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2224, 136, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2225, 137, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2226, 138, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2227, 139, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2228, 140, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2229, 141, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2230, 142, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2231, 143, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2232, 144, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2233, 145, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2234, 146, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2235, 147, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2236, 148, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2237, 149, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2238, 150, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2239, 151, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2240, 152, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2241, 153, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2242, 154, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2243, 155, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2244, 156, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2245, 157, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2246, 158, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2247, 159, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2248, 160, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2249, 161, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2250, 162, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2251, 163, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2252, 164, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2253, 165, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2254, 166, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2255, 167, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2256, 168, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2257, 169, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2258, 170, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2259, 171, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2260, 172, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2261, 173, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2262, 174, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2263, 175, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2264, 176, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2265, 177, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2266, 178, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2267, 179, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2268, 180, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2269, 181, 5, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2270, 182, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2271, 183, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2272, 184, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2273, 185, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2274, 186, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2275, 187, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2276, 188, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2277, 189, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2278, 190, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2279, 191, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2280, 192, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2281, 193, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2282, 194, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2283, 195, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2284, 196, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2285, 197, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2286, 198, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2287, 199, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2288, 200, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2289, 201, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2290, 203, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2291, 204, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2292, 205, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2293, 206, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2294, 207, 5, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2295, 209, 5, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2296, 132, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2297, 133, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2298, 134, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2299, 135, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2300, 136, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2301, 137, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2302, 138, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2303, 139, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2304, 140, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2305, 141, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2306, 142, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2307, 143, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2308, 144, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2309, 145, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2310, 146, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2311, 147, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2312, 148, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2313, 149, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2314, 150, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2315, 151, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2316, 152, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2317, 153, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2318, 154, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2319, 155, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2320, 156, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2321, 157, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2322, 158, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2323, 159, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2324, 160, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2325, 161, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2326, 162, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2327, 163, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2328, 164, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2329, 165, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2330, 166, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2331, 167, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2332, 168, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2333, 169, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2334, 170, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2335, 171, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2336, 172, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2337, 173, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2338, 174, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2339, 175, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2340, 176, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2341, 177, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2342, 178, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2343, 179, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2344, 180, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2345, 181, 6, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2346, 182, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2347, 183, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2348, 184, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2349, 185, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2350, 186, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2351, 187, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2352, 188, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2353, 189, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2354, 190, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2355, 191, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2356, 192, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2357, 193, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2358, 194, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2359, 195, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2360, 196, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2361, 197, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2362, 198, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2363, 199, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2364, 200, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2365, 201, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2366, 203, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2367, 204, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2368, 205, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2369, 206, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2370, 207, 6, 2014, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2371, 209, 6, 2014, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2372, 132, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2373, 133, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2374, 134, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2375, 135, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2376, 136, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2377, 137, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2378, 138, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2379, 139, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2380, 140, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2381, 141, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2382, 142, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2383, 143, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2384, 144, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2385, 145, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2386, 146, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2387, 147, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2388, 148, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2389, 149, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2390, 150, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2391, 151, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2392, 152, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2393, 153, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2394, 154, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2395, 155, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2396, 156, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2397, 157, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2398, 158, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2399, 159, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2400, 160, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2401, 161, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2402, 162, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2403, 163, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2404, 164, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2405, 165, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2406, 166, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2407, 167, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2408, 168, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2409, 169, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2410, 170, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2411, 171, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2412, 172, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2413, 173, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2414, 174, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2415, 175, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2416, 176, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2417, 177, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2418, 178, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2419, 179, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2420, 180, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2421, 181, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2422, 182, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2423, 183, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2424, 184, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2425, 185, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2426, 186, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2427, 187, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2428, 188, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2429, 189, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2430, 190, 8, 2014, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2431, 191, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2432, 192, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2433, 193, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2434, 194, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2435, 195, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2436, 196, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2437, 197, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2438, 198, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2439, 199, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2440, 200, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2441, 201, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2442, 203, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2443, 204, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2444, 205, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2445, 206, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2446, 207, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (2447, 209, 8, 2014, CAST(0.88 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1536, 132, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1537, 133, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1538, 134, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1539, 135, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1540, 136, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1541, 137, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1542, 138, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1543, 139, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1544, 140, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1545, 141, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1546, 142, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1547, 143, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1548, 144, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1549, 145, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1550, 146, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1551, 147, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1552, 148, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1553, 149, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1554, 150, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1555, 151, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1556, 152, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1557, 153, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1558, 154, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1559, 155, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1560, 156, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1561, 157, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1562, 158, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1563, 159, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1564, 160, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1565, 161, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1566, 162, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1567, 163, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1568, 164, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1569, 165, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1570, 166, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1571, 167, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1572, 168, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1573, 169, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1574, 170, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1575, 171, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1576, 172, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1577, 173, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1578, 174, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1579, 175, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1580, 176, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1581, 177, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1582, 178, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1583, 179, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1584, 180, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1585, 181, 1, 2013, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1586, 182, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1587, 183, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1588, 184, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1589, 185, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1590, 186, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1591, 187, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1592, 188, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1593, 189, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1594, 190, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1595, 191, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1596, 192, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1597, 193, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1598, 194, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1599, 195, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1600, 196, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1601, 197, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1602, 198, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1603, 199, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1604, 200, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1605, 201, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1606, 203, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1607, 204, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1608, 205, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1609, 206, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1610, 207, 1, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1611, 209, 1, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1612, 132, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1613, 133, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1614, 134, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1615, 135, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1616, 136, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1617, 137, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1618, 138, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1619, 139, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1620, 140, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1621, 141, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1622, 142, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1623, 143, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1624, 144, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1625, 145, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1626, 146, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1627, 147, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1628, 148, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1629, 149, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1630, 150, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1631, 151, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1632, 152, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1633, 153, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1634, 154, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1635, 155, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1636, 156, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1637, 157, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1638, 158, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1639, 159, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1640, 160, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1641, 161, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1642, 162, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1643, 163, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1644, 164, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1645, 165, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1646, 166, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1647, 167, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1648, 168, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1649, 169, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1650, 170, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1651, 171, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1652, 172, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1653, 173, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1654, 174, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1655, 175, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1656, 176, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1657, 177, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1658, 178, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1659, 179, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1660, 180, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1661, 181, 2, 2013, CAST(0.80 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1662, 182, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1663, 183, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1664, 184, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1665, 185, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1666, 186, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1667, 187, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1668, 188, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1669, 189, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1670, 190, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.64 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1671, 191, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1672, 192, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1673, 193, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1674, 194, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1675, 195, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1676, 196, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1677, 197, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1678, 198, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1679, 199, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1680, 200, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1681, 201, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1682, 203, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1683, 204, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1684, 205, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1685, 206, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1686, 207, 2, 2013, CAST(0.72 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.28 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1687, 209, 2, 2013, CAST(0.92 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1688, 132, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1689, 133, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1690, 134, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1691, 135, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1692, 136, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1693, 137, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1694, 138, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1695, 139, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1696, 140, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.80 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1697, 141, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1698, 142, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1699, 143, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1700, 144, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1701, 145, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1702, 146, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1703, 147, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1704, 148, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1705, 149, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " INSERT [dbo].[QUO_QUOTA_PRODUCTION] ([QUO_ID], [CEP_ID], [REG_ID], [ANN_AN], [QUO_AOC], [QUO_PREMIER_GRANDS_CRUS], [QUO_VINS_PAYS], [QUO_VINS_TABLE]) VALUES (1706, 150, 4, 2013, CAST(0.90 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2)), CAST(1.44 AS Decimal(3, 2)), CAST(0.00 AS Decimal(3, 2))) "
            + " SET IDENTITY_INSERT [dbo].[QUO_QUOTA_PRODUCTION] OFF ";
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update20 : Integration de la table __MigrationHistory nécessaire au migration pour EntityFramework
        /// </summary>
        /// <see>Releases#Update 20</see>
        private void Update20()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                "CREATE TABLE [dbo].[__MigrationHistory] ( ", '\n',
                "[MigrationId] [nvarchar](150) NOT NULL, ", '\n',
                "[ContextKey] [nvarchar](300) NOT NULL, ", '\n',
                "[Model] [varbinary](max) NOT NULL, ", '\n',
                "[ProductVersion] [nvarchar](32) NOT NULL, ", '\n',
                "CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey]) ", '\n',
                ") ", '\n',
                "INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion]) ", '\n',
                "VALUES (N'201408121019173_AutomaticMigration', N'GSN.GDV.Data.Models.Migrations.Configuration',  0x1F8B0800000000000400ED5D5F6FE3B8117F2FD0EF60F8A9456FD79B640B5C17490BC5D6E68CB565AF64A7D7BE188ACDE48493A59C242F927EB53EF423F52B94D45F8A7F2492A26CDF3538EC21B6C4DF9033C3E1909C19FFF7DFFFB9FEDBCBDE1F7C0351EC85C1CDF0E2FD87E10004DB70E7054F37C343F2F8EEFBE1DFFEFAFBDF5D9BBBFDCBE0BE78EF0ABD075B06F1CDF0A72479FE341AC5DB9FC0DE8DDFEFBD6D14C6E163F27E1BEE47EE2E1C5D7EF8F097D1C5C508408821C41A0CAEED4390787B907E801FC761B005CFC9C1F5E7E10EF871FE3D7CE2A4A803CBDD83F8D9DD829BE19D63BDBF9BDCBF9FB889FB1E364CC04B02E2E1C0F03D17F6C601FEE370E0064198B809ECEBA7750C9C240A8327E7197EE1FAABD76700DF7B74FD18E463F854BD2E3A9C0F976838A3AA6101B53DC449B89704BCB8CAF933229B2B717958F22FE5EDFED9072F68D8291B6F862BF7F00219970B3DE5F8704012FE34F623D4866078269FF746100090FEFD2706DA7703D8E61D6CF30EB57997B5F9AED41EA864E8BFEF06E3839F1C227013804312B9B0D9F2F0E07BDB2FE07515FE0C829BE0E0FBF858E0689651F80CA2E4351F8A1D1E9EA014B38E4EC0D6DBBB702CCB08FE952BEA70E06C5D1F3EBD1C0E2C88E73EA00FB9F4478DE0B7BE1B6CF5805F8F3031344BC70CB6EE377088BACAA586733C8958873D88C2826BD320B9BA6CE3B3136E3D90946284B3151A9FE160EEBECC40F094FC7433847F0E079FBD17B02BBEC979BC0E3C68AB60A3243AB4CAD30AF7BDD380DA111C810CB27B51E83BE161EFC505B5DB30F4811B286BDE04C4DE53906A999AF24589B7F573F523C18EA7813368EB0F4DDAF4E70F5A2470881A7556818A84ACEEA2F0F00CBA8B09C3399E84C610FA649C5BDD1B4A6C436388A09D72BDA858F972A4E3310E414F7752C6B56E8F8FCEEDBF7BC1ADF72F4D0CC7C08EC77363B78B401C37B25D822173D821EFDEF5114FE5F8E1802481C2CB5851C11C8F1385F2F4BCB641FE822710496979E99DE5CE5A08C7D64A08F20BACE02EA46C557E6EF10B8B755665D935E1C62779C514C2D8FE72F012354B5E35ED510BE0B3DA1718436CF058086D47716D4436A4044D4E29CA85C618E1246104EE40002228A5DDD24D1210050803A40C6D5DAE7D378E656D275CE37DE8414BB64A7D70694AFBFD21906D855BC84ECB82D81C7313E9A9E21CA247B86B979B97332F89402CD7C6064F504B9498904DD444AAE53D08766EF0043A48C006E8C823DDE237F1F47A54598C663B526EFDD4378D6F56A4656A0BF8021C8320D502311E6DF1DC07CFF71279CD8A9FC320463CB8E87DB5C6885D1E93D8D531897DECDFE9899791F7F2C5F3FDF029722B9F84E95CD0CDEB8770656F81FFF89E79DA277912569C25D5808983AA96F32F512B866D4EBB6C6CDF2C997E2745655DC68E816ACA439F3549AAA4FE33049A46763852EB77EDDC45BACB2AEE64B6E9956C942D20FBE76C0589533E2B49DB0CA0A26D7F9613FA7DDD0C3137CCA2F6004E4B30473AAD7A284902BC59862EA7E44AA74582B2CED73AFC9C4356DA0C08ADF21EDCBA31C0CC81F110C397B749ADCFCD73233BFA693C3A10E4D7183CBB4F6A4B25D6F46D3EF461B515F6D7FAAFA7C435291BA39A2A616DDF74A959C43F4087399B7A92FEFD0975A33896E862965918E76C973B1EC5E4A7D19D3846439C35C3187EA130BBF203FF2EEC62409C33BBBA78C4330F1C5E20F8EEB055768A59186FB6FBC87EB116A741E0DC5B58B3962E3A794A223597B2DEFAD4DAB434E69BE9445EA38A76473B87B12C9A5ECBAD0FECE27866F61FCA04E9FC60383F6CE6C6F887A9751C82D67ABE31ADB1716FAE6D69AE983F2EA7B6B19A2EACD2BDF202377A15EB6B3BFEBD693B107C732B2FB099E1AC36B3E9D8B41C53BAF1DFA7D6EDF49F1B63629B8E636E26E66763BD3A8A3808CAE3B56D9BD67148A7AA9773FC28042BEDD9AC97136365F6A344D6ED66692F26EBF10A5371BD24F209BBB18CB9EE592BB3986C81AF78925F6B7CEAA5E4DC1D936C43297B2799868E4836FA1CFA5E532899A6305D8508E22CCCA0BEB5163BABFE7A8062E83F3EA1E8213A9307925D4CBD7559F962C7DE9ABC4310033041A10EBEDA9C26DABF4DEB1EA635941A908E17988572C12F4E18EC60D716A6E414C89ADD46DEF667C9D9F3F5E0229E8174E6A935FDE26537EB7BC9F6F9A5B2C63994C9487D0E61EDDFE6500FAB1C16F721D95225422E35EEFD5F64BBC16E1C1D244F9DD3BE4D40755074FA45888C09EF1C54FE36854E7EECD59A33A585CA0A5F3BFA22E28CAB6D635F34D482105180593A8DB1302E413390362D678D6C64DABD510F472BD36F24A367B238981A562DB9444FFC59BA1D40F3A6E3093B0FE7CDDCF4E0F52A057FCB47E51A8B715336F0C5F7783A70ABC9DB7B2022D7E5AEB0F75EB0745F638D702B37F59155F084E75C263EA56986357D9B59E71AD78539D48A42AEB77F93749B8380FC03E98CA05FA7A3271E49CABAB2970D266560E88D332828E443FD0D6A27FAFFA92E25BAC8FED7125B9262E059C67A1CE34C549D1848219C351B8FA19A698E9707675211B60F3F2056E4658D181C5EC7206772CC3411192E1C73ED04CC41E98D55470A0EE199C7A3161CE42D7360B0C4C33694EC309583534BFD6943C28E96387074E6400B66B6F760A3D562ADDB70B288580E502DD4B6058988D36223B203C25A90CB681D3626190AD48E96DED772C1F0ABE036ACEA9E8803475D44892066A7E60D88B563F93644EC3C8403491F54B660324E0CD8D0FC238A160AD966890D5ADB83B5E294FE380F8C74F85B10731BCD99C84CC78D65644B735A3EBB1E6505E4F22F50550666A5B9EBB9FBFC0CC1B1CA73F93703272B3B377EE7C8D762DB6718A36D3C6C30FE2525E8584123433CCD52DD3E7B519CA0A5E1C1452BCE78B7A75E23170F0ECB0B6AE4FA407A96951C8A16E8EFFC3E815983AFB6A0107015433FC331EEA1594E870BCA6ED50B6150AD07A814A0EBBB11C3731D87FE611FE4E3197F65C4FD3521549521709CF1CC90C5A9F2446A408BB52450999A591B571E2728D39FB2A444BD3FACC0C82620DC9491684B7B21895644779042B3D673D35EC82065B76D244E16B8258E5256A820819CB5FDD9184B6115812124D46CBAB24D4706A93AD1C4B16CF34E89DB65850B36D3375027963319585EF90B5A3536F7A63531AC3B293E62A531C81EDBE6D85C66119E34DEF588B031A44D1B51468DD828935652CC8616BEB10E135A39D30A16B4A1B1B001953634D87D540D6862AB183EBA370653D63C1046E90C1C70BE184BF6AA5E4D83EC1C9CD7CB85E518B7337373A1087BD90C4B9DDC09C25E35C35E29C27E6C86FD28034B57BDA054D1D92CEDE98F9B2F771CDCBCCC1679EBC8A887D138EF58553218E4B8FCC9EAD092BD5F19EB1FA175B5EE4D3B8D2BB6176B9E25E421E74568DB906F678635E6B06844F0489C8DF5BA1F8D0CAC5703911820D32180232CB2001A3D03EEAA5E9494E5A23A8BF1D4E4B90ADCBEA2D36B7E47177339B8E23C9C8BB8B44D6950A2346C0303D67338BBA00EADEC056FF56F559D532DB9D541928E45173F795258761B9B0B2FBCF6EA341B05A6D76AAF9ABD56B6BDA0AABD34B29DAA0123A1E545915FB2D313D399DE595916C76C61DDAD25677851D6B709176EE96CEE2643C4D87228EF1874C78B098F125B0478CD9A46EEE3956CA418C4EBE6E60E2E6FCB1EF8A2695F5D15D2A1A75D9EE824EF68D22576BA799B44D51DF69CDC38ABC5F88BDC862D778448C032C1EA7C7653C4C9BE0EFB4E5D052818F9760C614BBF96B5F4B4DF0121389EC189C4861DE16A9018AB149082CC5AAF0065C4E698B262CB2F1449907B63B63637B78BC5CCE4EE6D212B761ED2B5410A7233FC48E3ACFEC1B6B65D35404D64CC3A21BF3199A1C3C5D574CE5F9109A15D9DBDD018954A7E6B3233C7D3B9C1DBE05022BB3C7791B1AAA5FCC64436B556E69D698B8AECE26C44368DD1DF8BC73FF01C1106D7FFF82B949E459DD5218C2CDDFC5CE6C9AF6782B03772583452E338B1502F9903B23CBC893D019D953DB53827AEDC21173FDBA030A7B9577C45C0B982696FBA36CC12F354D678EE316D910DA3E2EC891DD01256EFCF6763F57813908E183CEB6928BF4E354D135A601F8E2730C53D5C1500A741FC78C49CBCDC1B5B8B8A7B6C2E4F15C4C1BAD540DD910F73A0F6FE29CC59EDFDB180471D8A8347482A684E637361D59196385EDB9344822233E4422068A94398F3923A23385583F499D1ACF25A200623AA0DB3A9D9F5C40E4170EFF2C46D480A236D4364C3894EA451B5A0640DBA444431CB6B511B00F76237AF22588B876256246C4229AEF2BA5DEF95E502C9CEA45FCAE1D4CB019280F5A772C8F5BA7F2472FDA91C325E1490C4C59FC9A1E2A5004954FC991C6ABD4A20895B7F2A87CC29214892E0BCD689565934B08558F99E824E96D7584C9D6CB8E412D39CB220205F81CA5724F5BE5E0D9052FCFA6339EC7A194012BAFEF49CD680329544CF1280E59E28AD004DED451D88A5211B0C591542E8B6A3A94A20750B93CF6BFF9183FABC984DE5B6330CA70601C93B3544D13F12308FDF56F0BDF3928024E0D7F56265740DAC4140F2E1E064F540EE5817F3E5CA948C59CF8B0B76F377C9524F62F7DDA7B231F514331D6686CC4953B034AD10C2C6C69C9CC8D89865F9C17A7F64B529AD48480E69B690F20DB02A852492B3B026D0266C16729922F5FA853CD05B7BFA456A7213C50D49DCAF6BC35A4D57A682D960D53EE4C27323ACD9F3BD2A8B2816C478CAA96E96B99FBAA6BA89258B2A4EF54608F1A92E6DA835F9034495C4FAB982230946A772A181C9FAD379294512062E666B1998AA28138974671BD6646C4BA1316B28FE3FADB5513D555AC70C8CC8DC6A8529D88E213C07E57320C9C34104217938C8CA1D40304D29036CA41563DD81402BB4DC48AD8F636A870C61B26F4F904D562F784809CC51DC5893C510D9C04B7B6AAE8C2997819CACA5A25662A3FA97151425AEA751033A3899EF264AEDDE90F26689A4E90034E720E1751F1BF9825783941852A3C235C7859F6B760CA7948406D3CBAD3D216F81C5A1440D31DCA09F68DFA32B5D5B4B066C5AA692E44BFAA5D4EA52AF4F49E2C165663E35EDCC257236D02992DA9994852A49DCFBA9050DA8F10F59B4BC4E25136E851253CFC825AAEAB068989178E116F949D8D85A74DE299425203D1704716607D144951B2DA222CAE2A8C8AB0D425C68B29B34ACAC6437CF8C217D4787FB8B60F8EE6FAF6A445520225F29A9E7DF949FCB0A4479F59F5A59A274F4A8C8503AEA38AF44449603CA5E415579C36FDE0E9502725EE304EC33FD737EF1C7BE07C75BBD307703EF11C449563D6F78F9E1E27238307CCF8DB3924B79A1A34F64F539A1CA471757A8F211D8ED476473F9FA4908258E7735356754F6E4150E122B98C93EFB682F9A59B4CB4A557988C1ADA531256B9E1755883012158248C5D5228C4919A0FEFB844A3D98770328B6D7EA43C0AA0B7502C17F136E07059CA451C4F2386565A10CEAD10F5DA5FE1407C21D608AA55B03738B2A421D058D150CEAD429AC50908CC4C4CB09736AED08DA1C66188F80CDA1A7A36E9B53ACE79D4C8661750028B2A63BF58028CC936105DFDC68FB931BFD61EFBEFCB12BEA652FA857BDA07ED4868A17E4C9201F3C2501716AE3E49395F14B0357F80F0DC82A35BF644E7F04A90A369D149AAE5CA349A2F5FA35BA410B875C372E59BE465017C5ED3BB7A88BA08567DE840A58F8BC5D9F16BEAB4F875787E9E2C370EAB5D45585FA81806E448AE22DDA8964955A7A812D2BAB6845EFBE37A8974E39E17A4F5546E9A895C4CF4EB77649DCAA34571111342DCC3B5E01D3B2EEDFB41475493A686AB75FF450E0273B2BB09D9F45BB1EF959A5556B583F19E9A8CA1E1C2B5FB6DBCE9891D6DB611633128F95CD0B9DF0A9491859FA27B35F5D7EAA839B3B29361DD8D75FEDD3A168D7E77958D7250B4FC1EC06A2E4D38A8B909BC428284366FAA2800C6906F721C322265BD9FC94D990FD89A03D83504C14ECF8AE765114ED7A14459993A8D5AFC4531495E798E4C1A4F80F083764F489C9939DC7D72ECFA2DD396FECCAB4400D0B1C9D11A809B49E0CD869AC78FE5F06043BF7E0056EF4AADC3D3CFBAF53E7EAE97E9DA038697D9A04C2CBE3D3A944C48EAC332823714F970210397BBA60EBF97AFDAD7B0D196FA206921510206220A98B07EDBE07EDA2CADF2C75B4B065CE9C164DD670B8CC4A96EBB0F3C252E53AA26838626426C775C0633865920A449FC1E899B58DD9638213979937263071F376E73C718B44990E0079065A27043CF3AC8B5AD3F9661DD1A82C335D78D53DA1F2696C6FF3859B82253A5FD476760C5DD4BE13E8BA4E15E1819D269C8E80992A834BEB26B59ECEA57C0E71BEEB41D498DF24A8E0CCCC260105A743B6742B789931A5572BB0042AEDC079429576DC2CC34AEFC563D71820468A95F21C63665529A315594F9D8686A73BF5C078D92B5561AB20967323661CD8D936EDC6A16877CEDE62F7B0C8AE317065CE0E3F3CE9E27B767C92283C3385A73F7258664FCF44F27C1F1D548467163791466C32B15368DA27134351754FA62A35E71877FACD092EA2CC64A5B6883093F279CF2EB8B64C95D1BAECE09933BD49B9CC8D29EF38C99FB5AEF7B14CB7297887A579307E251C7F9A65C4C0C9FF10C20165DD455B22F86F3DA5AAC8D094AAD86E0621EC21934E6AFA2DBA0A134D060F316410AA3D6692829B63F86FCAA8E44813A3228F1814E9779864E19E05FEB3CDCDDCB458C51369E2CC9F70A7E9335F637501454FC07F2B46F96D8A381E0E41D3AC3D659142CE04BB86184D09BFB56790AA3D66D24277E38BF58C51938022C6BC9FA689B25F6311477B59B4D3DE4CCCC27117912D71A34AF7807C81451B5D702C0D1B2E33AC1A472C9AD81D059324FE9C4DD14614C7E64C60F250A7AB0C92D42B4CAAE664B3341D7436827632D44F17B029E3E7541CCAB557D894CD9C329CB38C1F3EA5295327080CD2F43B4CDAE9B69A5F1483A2CDDDA7D05DE0BFCAEA09F211D3CB09190DC77D3BBA03B5A72C9AC88182FFC42811DE0F8B1CF90A9BA683E77FB097E972412E9F5D8F32B0FC0BF8914A4ABD1ED987001DE4659FB2DFF02C21D0AF1E04209542055ABC330D1EC3C2F18063C67B54BC426ED6A16AEDA0778096C247B830C0C75B10C7E92F97E4BFAA60EE1FC06E1A2C0EC9F32131E218EC1FFC577CBCD7A366FAA88A07D1E7EB45FAEBF3B18E21C06E7AE8EC7311DC1E3C7F57F6FB3323C08F03811CA5DC0745750312E48B3EBD964856180802E5EC9B806710200F7605F6CF3E048B1781E37E032A7D5BC760069EDCEDEB32CF2DE683B40BA2CEF6EB89E7A2AA75718E51B5871FA10EEFF62F7FFD1FA968A888C2D40000 , N'6.1.1-30610') ", '\n',
                '\n'
            );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update21 : Update de la BDD pour l'EntityFramework
        /// </summary>
        /// <see>Issue #43</see>
        /// <see>Releases#Update 21</see>
        private void Update21()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "]; ", '\n',
                "INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion]) ", '\n',
                " VALUES (N'201408290847537_AutomaticMigration', N'GSN.GDV.Data.Models.Migrations.Configuration',  0x1F8B0800000000000400ED5D5B6FE338967E5F60FF83E1A7196C575C496A80DE423203C556A58DF2AD243BD3B32F86623369A1653923C98564FEDA3EEC4FDABFB0A46EA6C84389A4E84BD7068D342AB1F81DEADC78489E73FCBFFFFD3F377F7BDD049DEF288AFD6D78DBBDBCF8D8EDA070B55DFBE1F36D77973C7DF8B9FBB7BFFEFBBFDDD8EBCD6BE7A178EE9A3C874786F16DF7B72479F9DCEBC5ABDFD0C68B2F36FE2ADAC6DBA7E462B5DDF4BCF5B677F5F1E37FF62E2F7B0843743156A773E3ECC2C4DFA0F417FC6B7F1BAED04BB2F382F1768D8238FF3BFEC44D513B136F83E2176F856EBBF7EEE4E27EF07031F012EF020F4CD06B82E26EC70A7C0FCFC645C153B7E385E136F1123CD7CF8B18B949B40D9FDD17FC072F98BFBD20FCDC9317C4287F87CFFBC7655FE7E315799DDE7E6001B5DAC5C976A30878799DF3A7C70ED7E272B7E45FCADBCD4B805EC96BA76CBCEDCEBDDD2B665C2EF494E3DD0E4BF8733F88C81886E1997C2EAC304428FDF77F00683F75F0980F78CC0732E64336E6A7527BB09291FF7EEAF47741B28BD06D887649E4E161B3DD63E0AFBEA2B7F9F67714DE86BB20A0DF05BFCD2CDABEA02879CB5FC5D9EE9EB114B3890ED0CADF78F85D6611FE57AEA8DD8EBBF202FCE955B733C178DE23F925977EAF16FC2EF0C29519F09B1E25867AE9D8E1CAFB8E76515BB954708E2791C96E83A26DC1B561985C5F35F1D9DDAE7C949462C4D68A9D4FB733F65E47287C4E7EBBEDE27F763B5FFC57B42EFE92F37811FAD857E14149B46B94E764BB39380DAC1DE111C810BF176D0377BBDBF87141ED6EBB0D90176A6BDE00C5FE73986A999EF24589BF0A72F563C18EA78123ECEB7775DAF4978F4624B08B6A7556838A82ACEEA3EDEE05B5171385733C09F531F4C938377FB0B4D846DE21C27ECAF3A362E5CB918EC738023D5C2B39D7AA3F3E3AB7FFEE8777FEBF0C319C023B1ECFADF53A42715CCB7605868CF184FC072F203C55E3878B92040B2F63C51EE6789C2894E7C06B1BE62F7A4691929697D1591EAC6DF1BB3512C2FC4273BC0B294795BF37C485C53AABB3ECDA78E393BC510A61ADFEB9F3133D4FBE1F7A402DC09F55FE4031C4414F85D0D61CD77AEC404ED0AC49712134C50837D946E81E8528C2525ACFBC244151483050CAD0C6E53AF0E258D577E2353EC011B4E2A8340657A6B4D9EC42D551B4876CB52CC8D99897289B8ABB8B9EF0AE5DCD2E477E12A1586D8C839EB19668312133D44469E4030AD75EF88C5A48C041E4C823DDE2D7F1F4A6B7F718F57EA4DCFAE96F1ADFBD4883694BC4020287A03482309E6CF1BC473FF01375CD8A5FB6614C787079F0D59A2276754C62D7C724F6E9F0414F3C8BFCD7AF7E106C9F236F1F9380C1053FBC7A0857CE16054F17E0699FE2495871965401660EAA1ACEBF64BD18B5396DB3B17DF764E683149D75993A06AA280F7FD6A4A892E6CF10781AD9E14865DE957317E529EB8493D9A6577150B6806C5EB215244EF9AC256D3BC48AB6FA5D4DE80F5537046E9865FD01364B34263AAD7B28C902BC7B8636A7E45AA74592B2CED73AFA9C4355DA0084517977EEBC1851EEC07A8CF1C3ABA432E77ADBC88E7E6A8F0E24F9D5472FDEB3DE52490D7DB78743786D8DFDB5F9EB29794DCA8E1FF454891AFBAE4B47F6AD660E767CECC4FC55D270C42DAD4BA9BD68EAD27EECBB2ED54BED17BCF9CADCB8E25EF1847EA638E26AB3C44318E7BCC6B73CD6CB6F365A718C87386B86017B0C7976E5BE4C8F51F4E077FF73AEFB84FC82B08D490010E76C126DD6E3918F76187BBD5B69EFA1018877EBF823467A12B764D27A35F3C8397512E9ED1AAAA34FAD4D336BBC1C0ED435AA1877B453DBC984A7D770478CA7D81FD9874F7CC4747EB1DC5F9663ABFFCB70721C8293C578694FFAD683BD7094B962FF3A1B3AD67C389D9401B41F7AD19BDC5C9BF11F6CC7C5E0CB3B75818D2C77BE1C0DFBF6C4B59507FF7D38B91BFED7D21A38B6EBDACB81FDC55ACC8F220E86727FE138F6E438A453D5CB397E14827BED592E66036B6E1F46892677CB99331D2CFA734AC5CD92C80D7639B1C6A6AD56653159A140F3DEAF32F8D44BC9B90726D991816A06439A68A638E8CB36F0EB124F0D25F56BD41B644949D5C313B99BAD6F3B2C86C36733153324377848718A245857152F754766283844314203921715E8993433FEDDAA0F60D5586A4839B968B455CB9473B7E11A4F6D6A2B5A4036EC2EF257BF2B1ACFB79D47788652C3D31BFAD5CFD270368AE3F30C14833694C948DF86A8F1EF367480458E4A12531CA9934E4B7CFBE1935EBC70DD8F4A3A92B70A646A03B43F253AFD12C4968FB4AE3F7937A0939F793596571AA132A7578E431171FBFB3DE3A168E8E52B935CD4D48CA98C4F492F900E2DAD463589F5C1AA66AE96957A8A897659CA5C05AB528766265535DD0B10BB6979BA2EC2797737078879B5EA44D413F8AD69BF5CDB81C601973FD39D031A5DDEC64711BB2CB7857DF0C399F7161B849B7B6984AC83276D7399F8B4CC8C1AFA6E59E77AB54B85D39A42AE8E7F9774538040E203E5E2C11F25D01BC603F4E461D973918AA16A16E8DA5F35811DC0309BAB5050C879F603AA39F9FFA9AE36DAC8FE8F929F9262D09D0DCC984F26AA560CE410CE9A8DC750CDB4AED4C79694CF608C7F21ACC85BA9011C5EC42867720CBA880C17BF73E520CD2525D5FB89141CA2BB1DF41A7048D82D80A18A9D9B50B23359014EA5DCB009893AA312C0F1D54A0D98D9260646ABD47734E16459F802A04A7A7F23529A832D42A293BB1B908A744A188AC9D46CC0AAA6A0C18860A65B036E99870443B2494ECD68E94DB4108CBEE46EC2DA5F8109E0B83B3619C4EC42A006B172E3D084481DF60820F953D8064CE0380486169FBF3450C8768230686583D988536E364460EC6EA601315F3704CE050C2621C75FBAF8F2B39B5ED64833FF03E94E0376DCBC197B2F2F189CEAC099FFA5E366ED37FB1F5CF59E949B0CA3B78ABB350B524909077BD8F1319F6625BF5FFC284EC872F5E89155B0BFDE708FB10B9A80E5053576CD62A3DDBD1C8A11E4DFF95D09D88BB4B2C831707B867EC1EFB8C14B45FABAA89C56B5211037BA435AA27A811701D1747F1BEC3661FE3EFD6F4046631DC2BE430E8DD31F59AA38FB7AB90AD074A1085496A857DE2BCF8054994FD95AA73A1F28E5B30E8876652CDACC992AA215792BACD0268BB1ED4C5590B28B4416274B499347293BF5B040EEC2F962F595B08A9417166A349C3BB6AB82B43FAEA5B11CFB5E8BDB65A71F98E94BAC13B3910AACA80D10AF1ACB077B32B026F74A7CA45A04B13376ECBE3DCB725779BC9B1EE363589FD6E39C1AB37967BDA49C0F2DE275132E741FE06B78D09AC1D20E54D9D150976D15A081A3E3F8F8D958A0AC4520400B211A703CED2BCEAADA55889D1CB6EBD974E25A77237B79A9097B550FCB1D4B4AC25ED7C35E6BC27EAA87FDA402CB77FFE154D15DCE9CE1AFCBAFF702DCBCDD207BA50AF405AAB53BA85B10404EC89FAC1F373BFBB9B5F8157BD7C983EDA419D3CE7421F28422E4BC197713F2DDC89AF4052CEA313C926763B5FF512D03AB5D91145E100C08F01B16F50DB5918170552F5A6B0B51DD697F688B4205E15CC9D1BC78A2D3B11A5C71D82F449C39B63228D322BB86018B31B62EAC4373672A5AFD1B55E7544BEEFE70CBC4A24B9F86692CBBB5C3A5175E677E9A8D0218B53AF3FAA815F6175CD7AB5AB673BDB014B4BC6876CE4E7A60BBC3FB49569F329A4EEE178A165EB437AFC3C55B3A47B8C99071B602CA6B806E7F3A1051824540F7EEAAE53EDDD14B8941A2692EEFF1F2363B005F0CEDABF70DC578B3CB4BB8D4034DBED558BB6893E93E06DBE4D29D4FFB5FD5366C7920C40296A563E7B39B626E1B4CF877EE7A42C3C93763487BFA85AAA7E7E30E0C21880C4E2436EA08D780C4A096681A326BBC9654119B6BAB8A2DBFE464411EACD1C25EDE4DA7235BB8B7C5AC58FB44D73A29C86DF7138F33FF07EC6DDB6A809EC8C01E373F98CCC8E1E27C3816AFC88CD0AECF5E6840979D1F4D66767F38B6441B1C4E6457E72E32A80BCC0F26B2E1646EDFDB8EACC82ECF4664C398FC7BFAF42751200270FDCF7F40E94DB8B33A829115D29F8B9DFC710C04DEC8511952B5EF49A59FA91C90E52957B001BA736738119CB80A5FB9F8FA1A0D9B165EF115D9F41AAEBDEEDA30AB39D459E385C7B44502AD4EB0277740CB78BDBF9C8DD71319209FC578D666A8BE4ED599092FB08FC71398E61E6E9F946740FC74169FBADC6B47CB8ABB6FCF4E95C401DD6A90E9A8A739707BFF14E6ACF6FE5412A609C5A1B3363534A776B8B4EA28A7C9F072C210C2DB1B05AD998E759263A8DEC634DA60E80ADEEB64BA53A6DD1AD11D2A4F574777EA86CBEB8EAAB7A07B1AB348D8DC2DB5F41940131767E631E8146913C718959C6A75B9378C9715BCD8B8A4454520CE4B547C06BA01814129EBEA6293429115DE6868B7151E8130E0F15318658FAF9A32782275AA141E18D024A652415D899A0044FC2E7AA056721EC17EAA7528C5757DBB2BFCB2D9293B99F48F6A38D566A62C60F55335E46AD75216B9FAA91A32DDD294C5A53F5343A51B99B2A8F4676AA8D51EA72C6EF55335644103549684E0B156B4CA96A70DC4CAE73474B2BCAA0675B2E6225B4E73CA76A662052A1F51D4FB6A2F534EF1AB1FAB61579B98B2D0D54FCF690D28CBC5CC2C01547D99D60A50375E36809859AA09CFFB4E2EED4E2DF61DDCDA95C2E49D4BD997FA321D0DD58E2C80A08600A907354CCB521630AFD1D0D823E50D4D59C06F8BE9DC6A9B3C4780D44B3ED8DEA7C2779D8E67735BB12E256B8DDA2EDC655BD5C9A5B49CCAC554AB484D7819B6EC54C3D1344248FB1A7B70225F6397CD53ABF351D5A6B49F2AFB4AA3A9526840F5586591DCE964805DC272AA560C56EDBE2A02BD73865F956C9B69CDCAE27E5B5893F9706E6B780DA873AB105E584401DBFBBEA9AB5C9EF2294DDD2ECBBB4D99BA4DD5836B9A7A2D84BCA92B3B6A43E100D3E3B57AACA07AD2C5576B9217530DA7B346B02C0A5ECB162A28FB9E722CD2BD634D067D47090DEA00FBFF69A58DAABD104CD85FC4364FD030C0660C690B542F72664F060984E2C920541C4460EA6A8260A439B0EA60A039596C9456C73EB73DC630D95F4F502E5A6DD7CA09CCD5DC55B3AD5C61E09933B4E7D650C840415962D1E9B556FDCBFEAF00B068D664005F7D200E1295B66E4479B34AF1F4050C1719D25D6B6BF942F7B25578A55A85AB2FFC38D7F23741AF1803AE57D85C46DD03CB43C93A62BC3B3FD1AEC7543F062325EE69935D962FE91F9556976A775D160F2F33E3A1ED641191BBC43191D2BEA46CB3CBE23E0C27D8815AFF5045CBBBEC82707352797E4621D1BED192018BA43B33A91B61ED6859BBD3E83BC2462E04E2CC4EA19936564644C5F4BDD291571384BCD054B7685453DC769119207D6156834AF84B60D4C35FAAD92D0B96DE7D8DC033A7832A25D7B08C7DA4A49EFFA5FCBD6C5896370BAB74314B39407A92A56F1EE78DCBD8EE61D923A443F9F6BBBF269DC3DCB738419B4C9BDD7F06FDC0C7EFBB7F60EC85FE138A93AC0168F7EAE3C79FBB1D2BF0BD38EBD096F745FBCC36D0946A9476794D1AA5A1F5A6C70E576FB74650E2785D311AA039B1A8CF985CCF5FF81CA5B9EF6F312EEB6CE713063776F755FCFE87A2691945628F20D317BAC85CD306A87E51ABD60CC6ED008ACDBAFE2B50CDC85A81D05F8EB9C6024ED2A203759CB2115906F5146C3DADF91487CB2D608A40C000738BA6632D054DF5176B3529AAAF988AC4E43BA20B5A7349FA1C302348C2E7F0E668DAE714D1412B97614D5A00144D165ACD80E9E3956185DFBD68F59B17FD69E3BDFEB92DEAD54150AF0F82FAC9182ADDBF2B837CF4B5042468A5951B2BF0AD2BD7F497AEA82AB5B8C3D6E108720DAF5A2934DFE8CA9044ABEDAE4C8316E1BD695CB6DB95A42ECAFB77610F28490F0FDEAA4A78F87CDC213D7CDB988E6E26D5268611B477AAAA0AF76529ED8814BD9E8C13C91A3B1D04B66CC46414BDFDDEA0DA69E984EB3DD748A9A55696B9A7925392F72AF54D87245D0B78632CE15A1687772D451BA3169ADAEE4B8934F809171137F3B31877407EEEBB3018583F81EA75ED080E2AAF6FB73306BA00B4B062A04F81B67BE1EBC30D0923AB1607E7D5E6DB8684A5D672E6005FA6359B4331EE90E7616D972CBA62BB1D88564C2B2F4261CDB3A40CC16A670919F2A775E66538A6B9672A9619B7976B51ED697AD1171721CB0A132A3F961126672D8730C822575F7B2D29AB990F674F35F5C07232802B819B6500E89469199415C6FA0625CDC7C6025D396EC21994CDDC2CC61D909B65C9AFD1BD165D01ACED9F140FEBA5855A57302B274FB84CB6599EC5B8733EEC28AB6E0D047D7CC1AD21D06AAD6DAB77A5CB6B4B8FF2E8875EF4A63D3DBAB8B6D5E4AAD5B4ADA00455B38604222A9335A944CC29456B50A02ED694023025B1A660ABE5B0870B1F6A0A4A651D24947223E320B9CB38E3211CBF6D53BF6D6DE961CB9254239A6CE0C205AA456D711A4155A2B6443170EC0ED69EB6C00382324505E2CF25CD586D6D75A6A4E182759912869B8F3B67C32D0AD15A00E4159EAD10E8CACE366ACDD773B644E3AA384DE1EDEFCEB56F280E662FC21247597BD1DBD901BA687C27D0769D2A12705B199C8924B27D89A4D14D6AB55E52FB38E77CD783A8B6825052C1C1DA410905E7D3184D2B78599368562BA81245E3C079C9A271DCAC86D1EC657CDBBC38A08851DBC6C0BA456DB4A2AEB0D5ABD105850760BC6A9A81B45790AB6A93730E703D5BB37328C69D73B4D83E55B86D5E68591597AF9E40CADEE5CF70CE9E2C3C582477387254EDDC8189E4157526A8485B96B0544DCE98E022B566630214D5B431ED8BDF8E91E7525F4226CB4CBDFB2B20E63DBB84F3B218CDE48D325D9A661AB7AC5203576CE5D48FB2E8ACBC6F660AC4188529EBD80A0150F5539D3DA1AC16ECA2F269566A863DC8E316CF3E9B2FD957E19FC5902BBBE329ED8B260042D487209D74FD98F09DD2783274EE2E40A8F231480AEFB0F1CF10E8B6CA13E352FA008AFC332059BCF1C13F8EBD1CDB13A8C1294F1C4A8103E8838F4153206949F8670E7C0D06479CCE33E269563E8548918804EEF3C753A2D3610052958F415AD33139EB1C2FF826B700312A5D0322467F0C135B9034E311D0A78423564D4CE0A9319F43E4C8FD3FFE993BC37EB3054217F83C55F0298836D9EB939308ECD28A8D8D8CDA3237CEFC04D80720DAE4026866397819867AAC4134A93B1C9024FD394CD12114FBF648C22F70A7CF0049EE1190AA3D58CE6C979C1D919D1EF7ED483065FA1C4F40B9F2084CD9CE296377047CB73A4F993B610148F3CF80B4D36307715B1E8EB6701FC74F41FC2834131243A797372A1A4EC7BEFC042A9F42344980897FE42831D121448E7D04A6E9D23563700452C61AE56737BD0C2CFF03FE952B64BFE939BB901C7466BF655F135E42902F560A512A853D68F1CC307CDA1611147E677A46C523EC610656AD350E7CC82AFF84D73CFCF10AC571FAE568F91737D99B47B41E86D35DF2B24BAC38469BC7E08D7EDF9B5E3D7DD2478899F3CDF485FC169B78053C4D9F9C0D4FC3BB9D1FACCB797F01224301048901F3189D742E4948ACFEFC56224DB6A12450CEBE017A412189F0E768F31260B0781ABADE77A433B7458C46E8D95BBDCDF27E046290664154D97E33F03DD23533CE31F6E3F1AF5887D79BD7BFFE1F275FB2B12DE20000 , N'6.1.1-30610') ", '\n',
                '\n'
            );
            Log.Info("Execution of " + _command.CommandText);
            try
            {
                _command.ExecuteNonQuery();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                Log.Info("Update21()", ex);
                var isPrimaryKeyException = false;
                /// <summary>
                ///	Looking for "Violation of PRIMARY KEY constraint 'PK_dbo.__MigrationHistory'. Cannot insert duplicate key in object 'dbo.__MigrationHistory'.
                /// </summary>
                foreach (SqlError e in ex.Errors)
                    if (isPrimaryKeyException = (e.Number == 2627 && string.IsNullOrWhiteSpace(e.Message) == false && e.Message.StartsWith("Violation of PRIMARY KEY constraint 'PK_dbo.__MigrationHistory'. Cannot insert duplicate key in object 'dbo.__MigrationHistory'")))
                        break;
                if (isPrimaryKeyException == false)
                    throw ex;
            }
        }

        /// <summary>
        /// Update 22 : Add column BonusPaid
        /// Ajout PAI_BONUS_PAYE pour gestion du paiement de l'entier du bonus sur une seule facture
        /// Ajout VAL_OECHSLE_MOYEN pour stocker la valeur réelle lors du paiement
        /// Ajout VAL_BON_TYPE pour type de bonus [pourcent=0; chf=1]
        /// Ajout LIP_POURCENTAGE pour pourcentage du bonus (%)
        /// Ajout LIP_TYPE type de ligne [pesée=0; bonus=1, manuelle=2]
        /// </summary>
        private void Update22()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table VAL_VALEUR_CEPAGE add VAL_OECHSLE_MOYEN float default 0, VAL_BON_TYPE int default 0;", '\n',
                " alter table PAI_PAIEMENT add PAI_BONUS_PAYE bit default 0;", '\n',
                " alter table LIP_LIGNE_PAIEMENT_PESEE add LIP_POURCENTAGE float default 0, LIP_TYPE int default 0;", '\n'
            );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update 23 : Add ZOT_ZONE_TRAVAIL
        /// Zone de travail correspond à la difficulté de travailler sur une parcelle dû par exemple à la pente ou à la taille qui empêche de faire de travail avec des machines
        /// Une zone contient une valeur qui sert à calculer un montant qui sera ajouté aux paiements des vendanges. Majoration lorsque le travail et plus difficile que la moyenne.
        /// </summary>
        private void Update23()
        {
            //Création nouvelle Table ZOT_ZONE_TRAVAIL
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " create table [dbo].[ZOT_ZONE_TRAVAIL] (", '\n',
                " ZOT_ID int not null PRIMARY KEY IDENTITY( 0,1 ),", '\n',
                " ZOT_DESCRIPTION nvarchar(100 ),", '\n',
                " ZOT_VALEUR FLOAT )", '\n'
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
            //Ajout clé étrangère à la table PAR_PARCELLE pour définir une zone de travail par parcelle
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table PAR_PARCELLE", '\n',
                " ADD ZOT_ID int,", '\n',
                " CONSTRAINT FK_PAR_PARCELLE_ZOT_ZONE_TRAVAIL FOREIGN KEY ( ZOT_ID) REFERENCES ZOT_ZONE_TRAVAIL(ZOT_ID)", '\n'
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update 24 : Modify default value for column ZOT_ID in PAR_PARCELLE
        /// </summary>
        private void Update24()
        {
            //Supprime la contrainte pour la valeur par default dans la colonne ZOT_ID de la table PAR_PARCELLE
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " if exists (SELECT name FROM sys.default_constraints where name like 'DF__PAR%')", '\n',
                " BEGIN", '\n',
                " declare @con varchar(50) = (SELECT name FROM sys.default_constraints where name like 'DF__PAR%')", '\n',
                " exec('alter table par_parcelle drop constraint ' + @con)", '\n',
                " END"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update 25 : add field for secteur visite and mode de culture
        /// </summary>
        private void Update25()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table PAR_PARCELLE add PAR_SECTEUR_VISITE NVARCHAR(50) default 'Aucun', PAR_MODE_CULTURE NVARCHAR(50);"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update 26 : add field for default mode de comptabilisation of zones de travail
        /// </summary>
        private void Update26()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table PAM_PARAMETRES add PAM_ZOT_DEFAULT_MOC_ID int"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update27 : Delete EntityFramework MigrationHistory table
        /// </summary>
        private void Update27()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @" IF EXISTS
                    (
                        SELECT	*
                        FROM	INFORMATION_SCHEMA.TABLES
                        WHERE	TABLE_SCHEMA = 'dbo'
                        AND	TABLE_NAME = '__MigrationHistory'
                    )
                    DROP TABLE dbo.__MigrationHistory"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update28 : add field for taux de conversion moût de raisin
        /// </summary>
        private void Update28()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " alter table ANN_ANNEE add ANN_TAUX_CONVERSION_MOUT_ROUGE decimal(3,2), ANN_TAUX_CONVERSION_MOUT_BLANC decimal(3,2)"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// add set_setting for canton
        /// </summary>
        private void Update29()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                "insert into SET_SETTING (SET_NAME, SET_TYPE, SET_VALUE_INTEGER, SET_VALUE_BOOLEAN) ",
                    "VALUES ('" + EnumHelper.GetName(SettingExtension.SettingEnum.Canton) + "', " +
                    ((int)AbstractSettingModel.TypeEnum.Integer) + ", " +
                    (int)SettingExtension.CantonEnum.Vaud +
                    ", NULL);"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// set SECTEUR_VISITE to 'Aucun' instead of NULL (for bug when updating a parcelle)
        /// </summary>
        private void Update30()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                " update PAR_PARCELLE set PAR_SECTEUR_VISITE = 'Aucun' where PAR_SECTEUR_VISITE is null"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// add COM_ID field to par_parcelle for Neuchâtel version
        /// </summary>
        private void Update31()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @" CREATE TABLE [dbo].[PLI_PAR_LIE]
                    (
                        PLI_ID int NOT NULL IDENTITY (1,1) PRIMARY KEY,
                        PAR_ID int,
                        LIE_ID int,
                        FOREIGN KEY (PAR_ID) REFERENCES PAR_PARCELLE(PAR_ID) ON DELETE CASCADE,
                        FOREIGN KEY (LIE_ID) REFERENCES LIE_LIEU_DE_PRODUCTION(LIE_ID)
                    )"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();

            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @" CREATE TABLE [dbo].[PEL_PED_LIE]
                    (
                        PEL_ID int NOT NULL IDENTITY (1,1) PRIMARY KEY,
                        PED_ID int,
                        LIE_ID int,
                        FOREIGN KEY (PED_ID) REFERENCES PED_PESEE_DETAIL(PED_ID) ON DELETE CASCADE,
                        FOREIGN KEY (LIE_ID) REFERENCES LIE_LIEU_DE_PRODUCTION(LIE_ID)
                    )"
                );
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// add PAR_KILOS column to par_parcelle for Neuchâtel version
        /// </summary>
        private void Update32()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"
                IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[PAR_PARCELLE]') AND name = 'PAR_KILOS')
                    BEGIN
                        alter table [dbo].[PAR_PARCELLE] add PAR_KILOS float null
                    END
                ");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Add Vaud quotas 2017
        /// </summary>
        private void Update33()
        {
            string quotas2017Path = @"Resources\QUO_QUOTA_PRODUCTION_2017.sql";
            string script = File.Exists(quotas2017Path) ? File.ReadAllText(quotas2017Path) : string.Empty;

            if (string.IsNullOrWhiteSpace(script)) return;

            _command.CommandText = script;
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Increase AUT_NOM field size
        /// </summary>
        private void Update34()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"
                ALTER TABLE AUT_AUTRE_MENTION
                    ALTER COLUMN AUT_NOM VARCHAR(256)
                ");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Create table plantation parcelle.
        /// The sum of the surface per PAR_ID must be equal to the area value stored in PAR_PARCELLE
        /// </summary>
        private void Update35()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"
                CREATE TABLE[dbo].[PLA_PLANTATION_PARCELLE](
                    [PLA_ID][int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
                    [PAR_ID] [int] FOREIGN KEY REFERENCES PAR_PARCELLE(PAR_ID) NOT NULL,
                    [PLA_ANNEE_PLANTATION] [smallint] NULL,
                    [PLA_SURFACE] [float] NULL)
                ");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update PEE_PESEE_ENTETE for adding the fields PEE_REMARQUES, PEE_DIVERS1 and PEE_DIVERS2
        /// </summary>
        private void Update36()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"
                ALTER TABLE dbo.PEE_PESEE_ENTETE ADD
                PEE_REMARQUES nvarchar(MAX) NULL,
                PEE_DIVERS1 nvarchar(MAX) NULL,
                PEE_DIVERS2 nvarchar(MAX) NULL,
                PEE_DIVERS3 nvarchar(MAX) NULL");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update MOC_MODE_COMPTABILISATION by adding two columns : MOC_NO_ACCOUNT and MOC_CPC
        /// </summary>
        private void Update37()
        {
            _command.CommandText = string.Concat(
               "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
               @"
                ALTER TABLE MOC_MODE_COMPTABILISATION
                ADD MOC_NO_COMPTE_COMPTABLE INT;
                ALTER TABLE MOC_MODE_COMPTABILISATION
                ADD MOC_DEBIT_CREDIT VARCHAR(50);");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update PRO_PROPRIETAIRE by adding one column : PRO_IBAN
        /// </summary>
        private void Update38()
        {
            _command.CommandText = string.Concat(
               "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
               @"
                ALTER TABLE PRO_PROPRIETAIRE
                ADD PRO_IBAN VARCHAR(50);");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update LIP_LIGNE_PAIEMENT_PESEE by adding one column : LIP_QUANTITE_KG
        /// </summary>
        private void Update39()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"
                 ALTER TABLE LIP_LIGNE_PAIEMENT_PESEE
                 ADD LIP_QUANTITE_KG FLOAT;");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update VAL_VALEUR_CEPAGE by adding one column : CLA_ID
        /// </summary>
        private void Update40()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"ALTER TABLE VAL_VALEUR_CEPAGE
                  ADD CLA_ID INT NULL;
                  ALTER TABLE VAL_VALEUR_CEPAGE
                  ADD CONSTRAINT FK_VAL_VALEUR_CEPAGE_CLA_CLASSE FOREIGN KEY(CLA_ID) REFERENCES CLA_CLASSE(CLA_ID); ");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update VAL_VALEUR_CEPAGE by adding one column : VAL_IS_SPECIFIQUE
        /// </summary>
        private void Update41()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"ALTER TABLE VAL_VALEUR_CEPAGE ADD [VAL_IS_SPECIFIQUE] BIT DEFAULT 'FALSE';");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Update VAL_VALEUR_CEPAGE by adding one column : VAL_IS_SPECIFIQUE
        /// </summary>
        private void Update42()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"UPDATE VAL_VALEUR_CEPAGE SET VAL_IS_SPECIFIQUE = 'FALSE';");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }

        /// <summary>
        /// Create table VPR_VAL_PRO
        /// Join table VAL_VALEUR_CEPAGE AND PRO_PROPRIETAIRE to store specific prices per producer
        /// </summary>
        private void Update43()
        {
            _command.CommandText = string.Concat(
                "USE [", ConnectionManager.Instance.GvDBName, "] ", '\n',
                @"CREATE TABLE [dbo].[VPR_VAL_PRO](
                  	[VPR_ID] [int] IDENTITY(1,1) NOT NULL,
                  	[VAL_ID] [int] NOT NULL,
                  	[PRO_ID] [int] NOT NULL,
                   CONSTRAINT [PK_VPR_VAL_PRO] PRIMARY KEY CLUSTERED
                  (
                  	[VPR_ID] ASC
                  )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                  ) ON [PRIMARY];

                  ALTER TABLE [dbo].[VPR_VAL_PRO] WITH CHECK ADD CONSTRAINT [FK_VPR_VAL_PRO_VAL_VALEUR_CEPAGE] FOREIGN KEY([VAL_ID])
                  REFERENCES [dbo].[VAL_VALEUR_CEPAGE] ([VAL_ID])
                  ON DELETE CASCADE;

                  ALTER TABLE [dbo].[VPR_VAL_PRO] CHECK CONSTRAINT [FK_VPR_VAL_PRO_VAL_VALEUR_CEPAGE];

                  ALTER TABLE [dbo].[VPR_VAL_PRO] WITH CHECK ADD CONSTRAINT [FK_VPR_VAL_PRO_PRO_PROPRIETAIRE] FOREIGN KEY([PRO_ID])
                  REFERENCES [dbo].[PRO_PROPRIETAIRE] ([PRO_ID])
                  ON DELETE CASCADE;

                  ALTER TABLE [dbo].[VPR_VAL_PRO] CHECK CONSTRAINT [FK_VPR_VAL_PRO_PRO_PROPRIETAIRE];
                ");
            Log.Info("Execution of " + _command.CommandText);
            _command.ExecuteNonQuery();
        }
    }
}