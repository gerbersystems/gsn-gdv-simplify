﻿using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Gère la mise à jour de la structure de la base de données
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 16.11.2009 - Ajout de la table COA_COMMUNE_ADRESSE à update4
    *   ATH - 19.11.2009 - Ajout des champs TVA & Multi-poste (update4)
    *   ATH - 20.11.2009 - Ajout de la table MOD_MODE_TVA
    *   ATH - 03.12.2009 - Ajout de CurrentVersion et de ViewPaiement
    * */

    partial class BDVendangeUpdateManager
    {
        private PAM_PARAMETRESTableAdapter _pamTable;
        private SqlConnection _connection;
        private SqlCommand _command;

        public static int CurrentVersion => VERSION;

        public BDVendangeUpdateManager()
        {
            _pamTable = ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();
            _connection = new SqlConnection(ConnectionManager.Instance.GVConnectionString);
            _command = new SqlCommand()
            {
                Connection = _connection
            };
        }

        public void UpdateBD()
        {
            try
            {
                _connection.Open();
                int currentVersion = (int)_pamTable.GetVersionBD();

                if (currentVersion == 0) return;

                while (currentVersion <= VERSION)
                {
                    MessageSplahScreen();
                    var updateMethod = GetType().GetMethod("Update" + currentVersion, BindingFlags.Instance | BindingFlags.NonPublic);
                    updateMethod.Invoke(this, null);
                    _pamTable.SetVersionBD(++currentVersion);
                }

                if (currentVersion == VERSION)
                {
                    // TODO | Mettre en place l'automatisation des données quota pour la nouvelle année
                    //QuotaProductionExtension.UpdateQuota(Utilities.ApplicationPath, new MainDataContext());

                    //MigrateToEntityFramework();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show($"Une erreur est survenue dans le numéro de version de la base de données [ {_connection.Database} ]",
                                "Erreur base de données",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                throw new NullReferenceException();
            }
            catch (NotFiniteNumberException)
            {
                MessageBox.Show("La base de données n'a pas été correctement mise à jour.\nLe programme peut être instable.\nVeuillez contacter le support technique.",
                                "Erreur mise à jour",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (SqlException ex)
            {
                MessageBox.Show($"La base de données: {_connection.Database} n'est pas disponible.",
                                "Gestion des vendanges",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                throw ex;
            }
            finally
            {
                _connection.Close();
            }
        }

        private void MessageSplahScreen()
        {
            if (Application.OpenForms[0] is GUI.SplashScreen) ((GUI.SplashScreen)Application.OpenForms[0]).Message = "Mise à jour de la base de données...";
        }
    }
}