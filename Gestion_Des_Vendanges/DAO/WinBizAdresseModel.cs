﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Gestion_Des_Vendanges.DAO
{
    /*
*  Description: Donnée d'un model d'adresse WinBIZ
*  Date de création: 09.11.2009
*  Modifications:

* */

    public class WinBizAdresseModel
    {
        private OleDbConnection _connection;
        private string _aD_MODELE;
        private decimal _aD_NUMERO;
        private List<decimal> _gA_GROUPES;
        private decimal _aD_RABTYP;
        private decimal _aD_RABGRP;
        private decimal _aD_RABTX;
        private decimal _aD_LANGUE;
        private decimal _aD_LIMCRD;
        private decimal _aD_VENDEUR;
        private decimal _aD_REGION;
        private decimal _aD_GROUPE;
        private decimal _aD_EXPEDIT;
        private bool _aD_RAPPEL;
        private decimal _aD_TBLPRIX;
        private bool _aD_DEBBLOC;
        private decimal _aD_MONNAI;
        private decimal _aD_MOD_PMT;
        private decimal _aD_TVAGEST;
        private decimal _aD_BAN_PAY;
        private decimal _aD_PMT_TYP;
        private string _aD_CPT_DEB;
        private string _aD_CPT_CRE;
        private decimal _aD_CNDPMT1;
        private decimal _aD_CNDPMT2;
        private decimal _aD_TXTACT;
        private bool _aD_IS_VNDR;
        private decimal _aD_COMMETH;
        private decimal _aD_COMTAUX;
        private decimal _aD_COMSYS;
        private string _aD_BVR;
        private decimal _aD_DOCPRES;
        private decimal _aD_PRTCODE;
        private decimal _aD_FACTMET;
        private bool _aD_SELECT;
        private decimal _aD_ESCMETH;
        private decimal _aD_CREMCPT;
        private decimal _aD_USRID;
        private decimal _aD_DOCMAIL;
        private decimal _aD_INVPER;
        private decimal _aD_TXTFMT;
        private decimal _aD_TVAGES1;

        public List<decimal> GA_GROUPES
        {
            get
            {
                return _gA_GROUPES;
            }
        }

        public decimal AD_RABTYP
        {
            get { return _aD_RABTYP; }
        }

        public decimal AD_RABGRP
        {
            get { return _aD_RABGRP; }
        }

        public decimal AD_RABTX
        {
            get { return _aD_RABTX; }
        }

        public decimal AD_LANGUE
        {
            get { return _aD_LANGUE; }
        }

        public decimal AD_LIMCRD
        {
            get { return _aD_LIMCRD; }
        }

        public decimal AD_VENDEUR
        {
            get { return _aD_VENDEUR; }
        }

        public decimal AD_REGION
        {
            get { return _aD_REGION; }
        }

        public decimal AD_GROUPE
        {
            get { return _aD_GROUPE; }
        }

        public decimal AD_EXPEDIT
        {
            get { return _aD_EXPEDIT; }
        }

        public int AD_RAPPEL
        {
            get { return Convert.ToInt32(_aD_RAPPEL); }
        }

        public decimal AD_TBLPRIX
        {
            get { return _aD_TBLPRIX; }
        }

        public int AD_DEBBLOC
        {
            get { return Convert.ToInt32(_aD_DEBBLOC); }
        }

        public decimal AD_MONNAI
        {
            get { return _aD_MONNAI; }
        }

        public decimal AD_MOD_PMT
        {
            get { return _aD_MOD_PMT; }
        }

        public decimal AD_TVAGEST
        {
            get { return _aD_TVAGEST; }
        }

        public decimal AD_BAN_PAY
        {
            get { return _aD_BAN_PAY; }
        }

        public decimal AD_PMT_TYP
        {
            get { return _aD_PMT_TYP; }
        }

        public string AD_CPT_DEB
        {
            get { return _aD_CPT_DEB; }
        }

        public string AD_CPT_CRE
        {
            get { return _aD_CPT_CRE; }
        }

        public decimal AD_CNDPMT1
        {
            get { return _aD_CNDPMT1; }
        }

        public decimal AD_CNDPMT2
        {
            get { return _aD_CNDPMT2; }
        }

        public decimal AD_TXTACT
        {
            get { return _aD_TXTACT; }
        }

        public decimal AD_IS_VNDR
        {
            get
            {
                return Convert.ToDecimal(_aD_IS_VNDR);
            }
        }

        public decimal AD_COMMETH
        {
            get { return _aD_COMMETH; }
        }

        public decimal AD_COMTAUX
        {
            get { return _aD_COMTAUX; }
        }

        public decimal AD_COMSYS
        {
            get { return _aD_COMSYS; }
        }

        public string AD_BVR
        {
            get { return _aD_BVR; }
        }

        public decimal AD_DOCPRES
        {
            get { return _aD_DOCPRES; }
        }

        public decimal AD_PRTCODE
        {
            get { return _aD_PRTCODE; }
        }

        public decimal AD_FACTMET
        {
            get { return _aD_FACTMET; }
        }

        public int AD_SELECT
        {
            get { return Convert.ToInt32(_aD_SELECT); }
        }

        public decimal AD_ESCMETH
        {
            get { return _aD_ESCMETH; }
        }

        public decimal AD_CREMCPT
        {
            get { return _aD_CREMCPT; }
        }

        public decimal AD_USRID
        {
            get { return _aD_USRID; }
        }

        public decimal AD_DOCMAIL
        {
            get { return _aD_DOCMAIL; }
        }

        public decimal AD_INVPER
        {
            get { return _aD_INVPER; }
        }

        public decimal AD_TXTFMT
        {
            get { return _aD_TXTFMT; }
        }

        public decimal AD_TVAGES1
        {
            get { return _aD_TVAGES1; }
        }

        public WinBizAdresseModel(OleDbConnection connection)
        {
            _aD_MODELE = "Producteur";
            _connection = connection;
            _gA_GROUPES = new List<decimal>();
            initValue();
        }

        public WinBizAdresseModel(string nomModel, OleDbConnection connection)
        {
            _aD_MODELE = nomModel;
            _connection = connection;
            _gA_GROUPES = new List<decimal>();
            initValue();
        }

        private void initValue()
        {
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                command.CommandText = "SELECT AD_NUMERO, AD_RABTYP, AD_RABGRP, " +
                "AD_RABTX, AD_LANGUE, AD_LIMCRD, AD_VENDEUR, AD_REGION, " +
                "AD_GROUPE, AD_EXPEDIT, AD_RAPPEL, AD_TBLPRIX, AD_DEBBLOC, " +
                "AD_MONNAI, AD_MOD_PMT, AD_TVAGEST, AD_BAN_PAY, AD_PMT_TYP, " +
                "AD_CPT_DEB, AD_CPT_CRE, AD_CNDPMT1, AD_CNDPMT2, AD_TXTACT, " +
                "AD_IS_VNDR, AD_COMMETH, AD_COMTAUX, AD_COMSYS, AD_BVR, " +
                "AD_DOCPRES, AD_PRTCODE, AD_FACTMET, AD_SELECT, AD_ESCMETH, " +
                "AD_CREMCPT, AD_USRID, AD_DOCMAIL, AD_INVPER, AD_TXTFMT, AD_TVAGES1 " +
                "FROM ADRESSES WHERE AD_MODELE = '" + _aD_MODELE + "'";
                OleDbDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    int i = 0;
                    _aD_NUMERO = dataReader.GetDecimal(i++);
                    _aD_RABTYP = dataReader.GetDecimal(i++);
                    _aD_RABGRP = dataReader.GetDecimal(i++);
                    _aD_RABTX = dataReader.GetDecimal(i++);
                    _aD_LANGUE = dataReader.GetDecimal(i++);
                    _aD_LIMCRD = dataReader.GetDecimal(i++);
                    _aD_VENDEUR = dataReader.GetDecimal(i++);
                    _aD_REGION = dataReader.GetDecimal(i++);
                    _aD_GROUPE = dataReader.GetDecimal(i++);
                    _aD_EXPEDIT = dataReader.GetDecimal(i++);
                    _aD_RAPPEL = dataReader.GetBoolean(i++);
                    _aD_TBLPRIX = dataReader.GetDecimal(i++);
                    _aD_DEBBLOC = dataReader.GetBoolean(i++);
                    _aD_MONNAI = dataReader.GetDecimal(i++);
                    _aD_MOD_PMT = dataReader.GetDecimal(i++);
                    _aD_TVAGEST = dataReader.GetDecimal(i++);
                    _aD_BAN_PAY = dataReader.GetDecimal(i++);
                    _aD_PMT_TYP = dataReader.GetDecimal(i++);
                    _aD_CPT_DEB = dataReader.GetString(i++);
                    _aD_CPT_CRE = dataReader.GetString(i++);
                    _aD_CNDPMT1 = dataReader.GetDecimal(i++);
                    _aD_CNDPMT2 = dataReader.GetDecimal(i++);
                    _aD_TXTACT = dataReader.GetDecimal(i++);
                    _aD_IS_VNDR = dataReader.GetBoolean(i++);
                    _aD_COMMETH = dataReader.GetDecimal(i++);
                    _aD_COMTAUX = dataReader.GetDecimal(i++);
                    _aD_COMSYS = dataReader.GetDecimal(i++);
                    _aD_BVR = dataReader.GetString(i++);
                    _aD_DOCPRES = dataReader.GetDecimal(i++);
                    _aD_PRTCODE = dataReader.GetDecimal(i++);
                    _aD_FACTMET = dataReader.GetDecimal(i++);
                    _aD_SELECT = dataReader.GetBoolean(i++);
                    _aD_ESCMETH = dataReader.GetDecimal(i++);
                    _aD_CREMCPT = dataReader.GetDecimal(i++);
                    _aD_USRID = dataReader.GetDecimal(i++);
                    _aD_DOCMAIL = dataReader.GetDecimal(i++);
                    _aD_INVPER = dataReader.GetDecimal(i++);
                    _aD_TXTFMT = dataReader.GetDecimal(i++);
                    _aD_TVAGES1 = dataReader.GetDecimal(i++);
                }
                else
                {
                    throw new BUSINESS.ModelAdresseWinBIZNotFoundGVException(_aD_MODELE);
                }
                command.CommandText = "SELECT GA_GROUPE FROM AD_GROUP " +
                    "WHERE GA_ADRESSE = " + _aD_NUMERO + ";";
                dataReader.Close();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    _gA_GROUPES.Add(dataReader.GetDecimal(0));
                }
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}