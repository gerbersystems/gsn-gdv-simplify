﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows;

namespace Gestion_Des_Vendanges.DAO
{
    internal class BDVendangeSauvegardeManager
    {
        private SqlConnection _connection;
        private SqlCommand _command;

        public BDVendangeSauvegardeManager()
        {
            _connection = new SqlConnection(ConnectionManager.Instance.GVConnectionString);
            _command = new SqlCommand();
            _command.Connection = _connection;
        }

        public void Sauvegarder(string path)
        {
            try
            {
                _connection.Open();
                _command.CommandText = "USE [" + DAO.ConnectionManager.Instance.GvDBName + "] " +
                    "BACKUP DATABASE " + DAO.ConnectionManager.Instance.GvDBName + " " +
                    "TO DISK = '" + path + "' " +
                    "WITH FORMAT, " +
                    "MEDIANAME = '" + path[0] + "_SQLServerBackups', " +
                    "NAME = 'Full Backup of " + DAO.ConnectionManager.Instance.GvDBName + " - " + DateTime.Now.ToShortDateString() + "';";
                _command.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }

        public void RestaurerToNewDB(string pathSauvegarde, string pathDB, string nomDB, bool isMaster = false)
        {
            SqlConnection connection = new SqlConnection(ConnectionManager.Instance.MasterConnectionString.Replace(ConnectionManager.Instance.MasterDbName, ""));
            SqlCommand command = new SqlCommand();
            string nomOriginal = (isMaster) ? "GV_MASTER_" + BUSINESS.Utilities.Version : "GESTIONVENDANGES_" + BUSINESS.Utilities.Version;
            string pathSauvegardeNew = Path.Combine(pathDB, new FileInfo(pathSauvegarde).Name);
            try
            {
                File.Copy(pathSauvegarde, pathSauvegardeNew, true);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "RESTORE DATABASE [" + nomDB + "] "
                    + "FROM  DISK = N'" + pathSauvegardeNew + "' WITH  FILE = 1, "
                    + "MOVE N'" + nomOriginal + "' TO N'" + pathDB + "\\" + nomDB + ".MDF', "
                    + "MOVE N'" + nomOriginal + "_log' TO N'" + pathDB + "\\" + nomDB + ".LDF', "
                    + "NOUNLOAD,  REPLACE,  STATS = 10;";
                command.ExecuteNonQuery();

                if (!isMaster)
                {
                    command.CommandText = "USE [" + nomDB + "] "
                        + "UPDATE ANN_ANNEE SET ANN_AN = " + DateTime.Now.AddMonths(-3).Year + ";";
                    command.ExecuteNonQuery();
                }
            }
            finally
            {
                connection.Close();
                File.Delete(pathSauvegardeNew);
            }
        }

        public void Restaurer(string path)
        {
            try
            {
                _connection.Open();
                _command.CommandText = "USE master " +
                    "ALTER DATABASE " + DAO.ConnectionManager.Instance.GvDBName + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE " +
                    "RESTORE DATABASE " + DAO.ConnectionManager.Instance.GvDBName + " " +
                    "FROM DISK = '" + path + "' " +
                    "WITH REPLACE;";
                _command.ExecuteNonQuery();
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}