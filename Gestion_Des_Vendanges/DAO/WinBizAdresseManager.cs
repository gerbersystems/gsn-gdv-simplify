﻿using Gestion_Des_Vendanges.BUSINESS;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Récupère les adresses de WinBIZ
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 04.11.2009 - Ajout de la méthode InsertAdresse
    *   ATH - 09.11.2009 - Modification de GetAdresses: Renvoi uniquement les adresses du groupe Producteur et exclu les adresses "model"
    *                      Modification InsertAdresse: Application du modèle Producteur
    * */

    public class WinBizAdresseManager
    {
        private OleDbConnection _connection;
        public WinBizAdresseModel _adresseModel;

        public WinBizAdresseManager(OleDbConnection connection)
        {
            _connection = connection;
        }

        public List<Adresse> GetAdresses()
        {
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                List<Adresse> adresses = new List<Adresse>();
                command.CommandText = "SELECT AD_NUMERO, AD_SOCIETE, AD_TITRE2, AD_NOM, AD_PRENOM, AD_RUE_1, AD_RUE_2, AD_NPA," +
                                        "AD_VILLE, AD_PAYS, AD_TEL1, AD_TEL2, AD_TEL3, AD_EMAIL, AD_TVANO,  AD_TVAGES1  " +
                                        "FROM ADRESSES, ADR_GRP, AD_GROUP " +
                                        "WHERE AD_NUMERO = GA_ADRESSE AND " +
                                            "GA_GROUPE = AG_NUMERO AND AG_TEXTE = 'Producteur' AND AD_MODELE = '';";
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        Adresse adresse = new Adresse();
                        adresse.NumeroDecimal = reader.GetDecimal(0);
                        adresse.Societe = reader.GetString(1);
                        adresse.Titre = reader.GetString(2);
                        adresse.Nom = reader.GetString(3);
                        adresse.Prenom = reader.GetString(4);
                        adresse.Rue1 = reader.GetString(5);
                        adresse.Rue2 = reader.GetString(6);
                        adresse.Npa = reader.GetString(7);
                        adresse.Ville = reader.GetString(8);
                        adresse.Pays = reader.GetString(9);
                        adresse.Telephone = reader.GetString(10);
                        adresse.Fax = reader.GetString(11);
                        adresse.Mobile = reader.GetString(12);
                        adresse.Email = reader.GetString(13);
                        adresse.NoTVA = reader.GetString(14);
                        decimal typeTVA = reader.GetDecimal(15);

                        if (typeTVA <= 1)
                        {
                            adresse.ModeTVA = null;
                        }
                        else if (typeTVA >= 4)
                        {
                            adresse.ModeTVA = 1;
                        }
                        else if (typeTVA == 2)
                        {
                            adresse.ModeTVA = 2;
                        }
                        else
                        {
                            adresse.ModeTVA = 3;
                        }
                        //  adresse.ModeComptabilisationWinBIZ = reader.GetDecimal(15);
                        adresses.Add(adresse);
                    }
                }

                return adresses;
            }
            finally
            {
                _connection.Close();
            }
        }

        private string formatString(string valeur)
        {
            if (valeur != null)
            {
                valeur = valeur.Replace('\'', ' ');
                valeur = valeur.Replace('"', ' ');
                valeur = valeur.Replace('?', ' ');
                valeur = valeur.Replace('*', ' ');
            }
            return valeur;
        }

        public int InsertAdresse(Adresse adresse)
        {
            if (_adresseModel == null)
            {
                _adresseModel = new WinBizAdresseModel(_connection);
            }
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                command.CommandText = "SELECT COUNT(*) FROM ADRESSES WHERE AD_NUMERO = " + adresse.Numero + ";";
                int nb = Convert.ToInt32(command.ExecuteScalar());

                if (nb == 0)
                {
                    decimal numero;
                    if (adresse.Numero > 0)
                    {
                        numero = adresse.Numero;
                    }
                    else
                    {
                        command.CommandText = "SELECT MAX(AD_NUMERO) FROM ADRESSES";
                        numero = Convert.ToDecimal(command.ExecuteScalar()) + 1;
                    }

                    WinBizControleur.SetNullOff(command);
                    command.CommandText = "INSERT INTO ADRESSES(AD_NUMERO, AD_SOCIETE, AD_TITRE2, AD_NOM, AD_PRENOM, AD_RUE_1, "
                        + "AD_RUE_2, AD_NPA, AD_VILLE, AD_PAYS, AD_TEL1, AD_TEL2, AD_TEL3, AD_EMAIL, AD_RABTYP, AD_RABGRP, " +
                            "AD_RABTX, AD_LANGUE, AD_LIMCRD, AD_VENDEUR, AD_REGION, " +
                            "AD_GROUPE, AD_EXPEDIT, AD_RAPPEL, AD_TBLPRIX, AD_DEBBLOC, " +
                            "AD_MONNAI, AD_MOD_PMT, AD_TVAGEST, AD_BAN_PAY, AD_PMT_TYP, " +
                            "AD_CPT_DEB, AD_CPT_CRE, AD_CNDPMT1, AD_CNDPMT2, AD_TXTACT, " +
                            "AD_IS_VNDR, AD_COMMETH, AD_COMTAUX, AD_COMSYS, AD_BVR, " +
                            "AD_DOCPRES, AD_PRTCODE, AD_FACTMET, AD_SELECT, AD_ESCMETH, " +
                            "AD_CREMCPT, AD_USRID, AD_DOCMAIL, AD_INVPER, AD_TXTFMT, AD_TVAGES1, AD_TVANO) VALUES( "
                        + numero + ", '"
                        + formatString(adresse.Societe) + "', '" + formatString(adresse.Titre) + "', '" + formatString(adresse.Nom) + "', '" + formatString(adresse.Prenom) + "', '" + formatString(adresse.Rue1) + "', '"
                        + formatString(adresse.Rue2) + "', '" + adresse.Npa + "', '" + formatString(adresse.Ville) + "', '" + formatString(adresse.Pays) + "', '" + adresse.Telephone + "', '"
                        + adresse.Fax + "', '" + adresse.Mobile + "', '" + formatString(adresse.Email) + "', " +
                        _adresseModel.AD_RABTYP + ", " +
                        _adresseModel.AD_RABGRP + ", " +
                        _adresseModel.AD_RABTX + ", " +
                        _adresseModel.AD_LANGUE + ", " +
                        _adresseModel.AD_LIMCRD + ", " +
                        _adresseModel.AD_VENDEUR + ", " +
                        _adresseModel.AD_REGION + ", " +
                        _adresseModel.AD_GROUPE + ", " +
                        _adresseModel.AD_EXPEDIT + ", " +
                        _adresseModel.AD_RAPPEL + ", " +
                        _adresseModel.AD_TBLPRIX + ", " +
                        _adresseModel.AD_DEBBLOC + ", " +
                        _adresseModel.AD_MONNAI + ", " +
                        _adresseModel.AD_MOD_PMT + ", " +
                        _adresseModel.AD_TVAGEST + ", " +
                        _adresseModel.AD_BAN_PAY + ", " +
                        _adresseModel.AD_PMT_TYP + ", '" +
                        _adresseModel.AD_CPT_DEB + "', '" +
                        _adresseModel.AD_CPT_CRE + "', " +
                        _adresseModel.AD_CNDPMT1 + ", " +
                        _adresseModel.AD_CNDPMT2 + ", " +
                        _adresseModel.AD_TXTACT + ", " +
                        _adresseModel.AD_IS_VNDR + ", " +
                        _adresseModel.AD_COMMETH + ", " +
                        _adresseModel.AD_COMTAUX + ", " +
                        _adresseModel.AD_COMSYS + ", '" +
                        _adresseModel.AD_BVR + "', " +
                        _adresseModel.AD_DOCPRES + ", " +
                        _adresseModel.AD_PRTCODE + ", " +
                        _adresseModel.AD_FACTMET + ", " +
                        _adresseModel.AD_SELECT + ", " +
                        _adresseModel.AD_ESCMETH + ", " +
                        _adresseModel.AD_CREMCPT + ", " +
                        _adresseModel.AD_USRID + ", " +
                        _adresseModel.AD_DOCMAIL + ", " +
                        _adresseModel.AD_INVPER + ", " +
                        _adresseModel.AD_TXTFMT + ", " +
                        _adresseModel.AD_TVAGES1 + ", '" +
                        adresse.NoTVA + "');";
                    command.ExecuteNonQuery();

                    command.CommandText = "SELECT AG_NUMERO FROM ADR_GRP WHERE AG_TEXTE = 'Producteur'";
                    var groupProducteurId = (decimal)command.ExecuteScalar();

                    if (!_adresseModel.GA_GROUPES.Contains(groupProducteurId))
                        _adresseModel.GA_GROUPES.Add(groupProducteurId);

                    foreach (decimal gaGroupe in _adresseModel.GA_GROUPES)
                    {
                        command.CommandText = "INSERT INTO AD_GROUP(GA_ADRESSE, GA_GROUPE) VALUES("
                            + numero + ", " + gaGroupe + ");";
                        command.ExecuteNonQuery();
                    }

                    return Convert.ToInt32(numero);
                }
                else
                {
                    return adresse.Numero;
                }
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}