﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Controle les classes se connectant à la base de données WinBIZ
    *  Date de création:
    *  Modifications:
    *   ATH - 02.11.2009 - Application des conventions de programmation
    *   ATH - 09.11.2009 - Ajout et implémentation de la méthode "CreateFacture"
    * */

    internal class WinBizControleur
    {
        #region properties

        private WinBizAdresseManager _adresseManager;
        private int _an;
        private int _annId;
        private WinBizArticleManager _articleManager;
        private OleDbConnection _connection;
        private string _connectionString;
        private WinBizExerciceManager _exerciceManager;
        private WinBizFactureManager _factureManager;
        private string _path;

        private WinBizAdresseManager _AdresseManager
        {
            get
            {
                if (_adresseManager == null)
                    _adresseManager = new WinBizAdresseManager(_connection);

                return _adresseManager;
            }
        }

        private WinBizArticleManager _ArticleManager
        {
            get
            {
                if (_articleManager == null)
                    _articleManager = new WinBizArticleManager(_connection);

                return _articleManager;
            }
        }

        private string _ConnectionString
        {
            get
            {
                if (_connectionString == "" || _connectionString == null)
                {
                    PAM_PARAMETRESTableAdapter pamTable = ConnectionManager.Instance.CreateGvTableAdapter<PAM_PARAMETRESTableAdapter>();
                    _path = Convert.ToString(pamTable.GetWinBIZAdresseCurrent());

                    if (_path == "")
                        _path = Convert.ToString(pamTable.GetWinBizAdresseDefaut());

                    _path += @"\" + _an;

                    if (System.IO.Directory.Exists(_path))
                        _connectionString = GetConnectionString(_path);
                    else
                        throw new System.IO.DirectoryNotFoundException();
                }

                return _connectionString;
            }
        }

        private WinBizExerciceManager _ExerciceManager
        {
            get
            {
                if (_exerciceManager == null)
                    _exerciceManager = new WinBizExerciceManager(_path);

                return _exerciceManager;
            }
        }

        private WinBizFactureManager _FactureManager
        {
            get
            {
                if (_factureManager == null)
                    _factureManager = new WinBizFactureManager(_connection, _annId);

                return _factureManager;
            }
        }

        #endregion properties

        public WinBizControleur(int annee, bool firstParamIsAnnId = true)
        {
            if (firstParamIsAnnId)
            {
                _annId = annee;
                _an = Utilities.GetAnnee(annee);
            }
            else
            {
                _an = annee;
            }

            _connection = new OleDbConnection();
            _connection.ConnectionString = _ConnectionString;
        }

        public static string GetConnectionString(string path)
        {
            return "Provider=VFPOLEDB.1;Data Source=" + @path + ";Collating Sequence=MACHINE;";
        }

        public static void SetNullOff(OleDbCommand command)
        {
            command.CommandText = "SET NULL OFF";
            command.ExecuteNonQuery();
        }

        public void CreateFactures(List<FactureWinBIZ> paiements)
        {
            _FactureManager.CreateFactures(paiements);
        }

        public void ExportArticle(ViewWinBizExportArticleTableAdapter exportArticleTable)
        {
            _ArticleManager.ExportArticle(exportArticleTable, _an);
        }

        public List<Adresse> GetAdresses()
        {
            return _AdresseManager.GetAdresses();
        }

        public List<int> GetArticleIds()
        {
            return _ArticleManager.GetArticleIds();
        }

        public List<WinBizExercice> GetExercices()
        {
            return _ExerciceManager.GetExercices();
        }

        public List<ModeComptabilisation> GetModeComptabilisations()
        {
            return _FactureManager.GetModeComptabilisations();
        }

        public CoupleIdsGvWinBIZ[] InsertAdresses(List<Adresse> adresses)
        {
            List<CoupleIdsGvWinBIZ> coupleIds = new List<CoupleIdsGvWinBIZ>();

            foreach (Adresse adresse in adresses)
                coupleIds.Add(new CoupleIdsGvWinBIZ(adresse.IdProGv, _AdresseManager.InsertAdresse(adresse)));

            return coupleIds.ToArray();
        }
    }
}