﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Gestion_Des_Vendanges.DAO
{
    /* *
    *  Description: Exporte les articles dans WinBIZ
    *  Date de création:
    *  Modifications: ATH - 02.11.2009 - Application des conventions de programmation
    * */

    internal class WinBizArticleManager
    {
        private OleDbConnection _connection;

        public WinBizArticleManager(OleDbConnection connection)
        {
            _connection = connection;
        }

        public void ExportArticle(ViewWinBizExportArticleTableAdapter exportArticleTable, int an)
        {
            try
            {
                _connection.Open();
                foreach (DSPesees.ViewWinBizExportArticleRow row in exportArticleTable.GetDataByAn(an))
                {
                    if (row.IsART_ID_WINBIZNull())
                        insertNewArticle(row);
                    else
                        updateArticle(row);
                }
            }
            finally
            {
                _connection.Close();
            }
        }

        private void insertNewArticle(DSPesees.ViewWinBizExportArticleRow row)
        {
            OleDbCommand command = _connection.CreateCommand();
            WinBizControleur.SetNullOff(command);

            try
            {
                ART_ARTICLETableAdapter articleTable = ConnectionManager.Instance.CreateGvTableAdapter<ART_ARTICLETableAdapter>();
                command.CommandText = "SELECT MAX(AR_NUMERO) FROM ARTICLES;";
                decimal numero = (decimal)command.ExecuteScalar();
                numero++;
                /*decimal numComptabilisation;
                try
                {
                    command.CommandText = "SELECT MIN(AC_NUMERO) FROM ART_CPT;";
                    numComptabilisation = (decimal)command.ExecuteScalar();
                }
                catch (InvalidCastException)
                {
                    numComptabilisation = 0;
                }*/
                ArticleExport article = new ArticleExport(row);
                checkGroupe(article.Groupe);
                command.CommandText = "INSERT INTO ARTICLES(ar_numero, ar_code, ar_abrege, ar_desc, ar_groupe, ar_qteini," +
                "ar_utilis1,ar_utilis3, ar_utilis7, AR_CPTMOD) VALUES(" + numero + ",'" +
                    article.CodeArticle + "', '" + article.DesignationCourte + "', '" + article.DesignationLongue +
                    "', '" + article.Groupe + "', " + article.Litres + ", '" + article.CodeGroupe +
                    "', " + article.Unite + ", '" + article.Annee + "', " + article.ModeComptabilisation + ")";
                command.ExecuteNonQuery();
                articleTable.SetWinBizId(Convert.ToInt32(numero), row.ART_ID);
            }
            catch (Exception ex)
            {
                string erreur = "Erreur: insertNewArticle(), \nCommande: " + command.CommandText
                    + "\nConnexion: " + _connection.ConnectionString
                    + "\nStack: " + ex.StackTrace;
                Utilities.WriteErrorLog(erreur);
                throw ex;
            }
            finally
            {
                string text = "OK: insertNewArticle(), \nCommande: " + command.CommandText
                    + "\nConnexion: " + _connection.ConnectionString;
                Utilities.WriteErrorLog(text);
            }
        }

        private void checkGroupe(string groupe)
        {
            OleDbCommand command = _connection.CreateCommand();
            command.CommandText = "SELECT COUNT(RL_NUMERO) FROM ref_list WHERE RL_TYPE = 'ar_groupe' AND RL_VALUE = '" + groupe + "';";
            if (Convert.ToInt32(command.ExecuteScalar()) == 0)
            {
                command.CommandText = "SELECT MAX(RL_NUMERO) FROM ref_list";
                decimal id = Convert.ToDecimal(command.ExecuteScalar()) + 1;
                command.CommandText = "INSERT INTO ref_list(RL_NUMERO, RL_TYPE, RL_VALUE) VALUES(" + id + ", 'ar_groupe', '" + groupe + "');";
                command.ExecuteNonQuery();
            }
        }

        private void updateArticle(DSPesees.ViewWinBizExportArticleRow row)
        {
            OleDbCommand command = _connection.CreateCommand();
            WinBizControleur.SetNullOff(command);
            command.CommandText = "SELECT COUNT(*) FROM ARTICLES WHERE ar_numero = " + row.ART_ID_WINBIZ + ";";
            if ((decimal)command.ExecuteScalar() == 0)
            {
                insertNewArticle(row);
            }
            else
            {
                ArticleExport article = new Gestion_Des_Vendanges.BUSINESS.ArticleExport(row);
                checkGroupe(article.Groupe);
                command.CommandText = "UPDATE ARTICLES SET ar_code = '" + row.ART_CODE +
                    "', ar_abrege = '" + article.DesignationCourte +
                    "', ar_desc = '" + article.DesignationLongue +
                    "', ar_groupe = '" + article.Groupe +
                    "', ar_qteini = " + article.Litres +
                    ", ar_utilis1 = '" + article.CodeGroupe +
                    "', ar_utilis3 = " + article.Unite +
                    ", ar_utilis7 = '" + article.Annee +
                    "', AR_CPTMOD = " + article.ModeComptabilisation +
                    " WHERE ar_numero = " + article.WinBizId + ";";
                command.ExecuteNonQuery();
            }
        }

        public List<int> GetArticleIds()
        {
            List<int> articleIds = new List<int>();
            try
            {
                _connection.Open();
                OleDbCommand command = _connection.CreateCommand();
                command.CommandText = "SELECT AR_NUMERO FROM ARTICLES;";
                OleDbDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    articleIds.Add(Convert.ToInt32(dr.GetDecimal(0)));
                }
            }
            finally
            {
                _connection.Close();
            }
            return articleIds;
        }

        /*  private void deleteArticle(Decimal numero)
          {
              OleDbCommand command = _connection.CreateCommand();
              command.CommandText = "DELETE FROM ARTICLES WHERE ar_numero = " + numero + ";";
              command.ExecuteNonQuery();
          }*/
    }
}