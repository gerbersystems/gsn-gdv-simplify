﻿using Gestion_Des_Vendanges.BUSINESS;
using Gestion_Des_Vendanges.DAO.DSPeseesTableAdapters;
using System;
using System.Collections.Generic;

namespace Gestion_Des_Vendanges.DAO
{
    partial class BDVendangeArticleManager
    {
        private PED_PESEE_DETAILTableAdapter _pedTable;

        private void InitSpecificTableAdapters()
        {
            _pedTable = DAO.ConnectionManager.Instance.CreateGvTableAdapter<PED_PESEE_DETAILTableAdapter>();
        }

        public void Grouper(List<int> articleIds)
        {
            PreArticle articlePrincipal = null;
            foreach (int artId in articleIds)
            {
                _pedTable.FillByArtId(_dsPesees.PED_PESEE_DETAIL, artId);
                foreach (DSPesees.PED_PESEE_DETAILRow pedRow in _pedTable.GetDataByArtId(artId))
                {
                    if (articlePrincipal == null)
                    {
                        articlePrincipal = getPreArticleComplet(pedRow.PED_ID);
                        articlePrincipal.LitresStock = 0;
                    }
                    else
                    {
                        articlePrincipal.Grouper(getPreArticleComplet(pedRow.PED_ID));
                    }
                }
                articlePrincipal.LitresStock += Convert.ToDouble(_artTable.GetLitresStock(artId));
                _pedTable.SetNullArtId(artId);
                _artTable.Delete(artId);
            }
            insertArticle(articlePrincipal);
        }

        public void Dissocier(int artId)
        {
            List<PreArticle> preArticles = new List<PreArticle>();
            _pedTable.FillByArtId(_dsPesees.PED_PESEE_DETAIL, artId);
            foreach (DSPesees.PED_PESEE_DETAILRow pedRow in _pedTable.GetDataByArtId(artId))
            {
                preArticles.Add(getPreArticleComplet(pedRow.PED_ID));
            }
            preArticles = grouperPreArticles(preArticles);
            insertArticles(preArticles);
            _artTable.Delete(artId);
        }

        private PreArticle getPreArticleComplet(int pedId)
        {
            _viewArtTable.FillByPedId(_dsPesees.ViewArticle, pedId);
            DSPesees.ViewArticleRow vartRow = _viewArtTable.GetDataByPedId(pedId)[0];
            PreArticle article = new PreArticle(vartRow.ANN_ID, vartRow.COU_ID, vartRow.PED_ID, Convert.ToDouble(vartRow.LITRES));
            article.AutId = vartRow.AUT_ID;
            article.CepId = vartRow.CEP_ID;
            article.ClaId = vartRow.CLA_ID;
            article.ComId = vartRow.COM_ID;
            //article.GroId = vartRow.GRO_ID;
            article.LieId = vartRow.LIE_ID;
            article.ProId = vartRow.PRO_ID_VENDANGE;
            return article;
        }

        private List<PreArticle> getPreArticles(int annId)
        {
            List<PreArticle> articles = new List<PreArticle>();
            bool isCepSelect = (bool)_assTable.GetSelection("CEP_ID");
            bool isLieSelect = (bool)_assTable.GetSelection("LIE_ID");
            bool isClaSelect = (bool)_assTable.GetSelection("CLA_ID");
            bool isAutSelect = (bool)_assTable.GetSelection("AUT_ID");
            bool isComSelect = (bool)_assTable.GetSelection("COM_ID");
            bool isProVendangeSelect = (bool)_assTable.GetSelection("PRO_ID_VENDANGE");

            _viewArtTable.FillByAnnNonGenerer(_dsPesees.ViewArticle, annId);
            foreach (DSPesees.ViewArticleRow row in _viewArtTable.GetDataByAnnNonGenerer(annId))
            {
                PreArticle article = new PreArticle(row.ANN_ID, row.COU_ID, row.PED_ID, Convert.ToDouble(row.LITRES));
                if (isCepSelect)
                {
                    article.CepId = row.CEP_ID;
                }
                if (isLieSelect)
                {
                    article.LieId = row.LIE_ID;
                }
                if (isClaSelect)
                {
                    article.ClaId = row.CLA_ID;
                }
                if (isAutSelect)
                {
                    article.AutId = row.AUT_ID;
                }
                if (isComSelect)
                {
                    article.ComId = row.COM_ID;
                }
                if (isProVendangeSelect)
                {
                    article.ProId = row.PRO_ID_VENDANGE;
                }
                /* if (isGroSelect)
                 {
                     article.GroId = row.GRO_ID;
                 }*/

                articles.Add(article);
            }
            return articles;
        }

        private void insertArticle(PreArticle article)
        {
            string designationLongue = null;
            string designationCourte = null;
            string codArticle;
            string an = _annTable.GetAnneeByAnnId(article.AnnId).ToString();
            codArticle = an.Substring(2, 2);
            //Cepage
            if (article.CepId != null)
            {
                string cepage = _cepTable.GetNomById((int)article.CepId);
                designationCourte = getStringCourt(cepage, 10);
                codArticle += getStringCourt(cepage, 2);
                designationLongue = "Cépage: " + cepage;
            }
            /*  else if (article.GroId != null) //Groupe de cépage
              {
                  string groupeCep = _groTable.GetGroNom((int)article.GroId);
                  designationCourte = getStringCourt(groupeCep, 10);
                  codArticle = getStringCourt(groupeCep, 2);
                  designationLongue += "Groupe de cépage: " + groupeCep;
              }*/
            else //couleur
            {
                string couleur = _couTable.GetNom(article.CouId);
                designationCourte = couleur;
                designationLongue = "Couleur: " + couleur;
                codArticle += getStringCourt(couleur, 2);
            }
            //Année
            designationCourte += " - " + an;
            designationLongue += " - Millésime: " + an;
            //Lieu de production
            if (article.LieId != null)
            {
                string lieuProd = _lieTable.GetLieNomByLieId((int)article.LieId).ToString();
                designationCourte += " - " + getStringCourt(lieuProd, 10);
                codArticle += getStringCourt(lieuProd, 2);
                designationLongue += " - " + lieuProd;
            }
            //Commune
            if (article.ComId != null)
            {
                string commune = _comTable.GetComNomByComID((int)article.ComId);
                designationCourte += " - " + getStringCourt(commune, 10);
                designationLongue += " - Commune: " + commune;
            }
            //Autre mention
            if (article.AutId != null)
            {
                string autMention = _autTable.GetAutNomByAutId((int)article.AutId);
                designationCourte += " - " + getStringCourt(autMention, 10);
                designationLongue += " - Parchet: " + autMention;
            }

            //Classe
            if (article.ClaId != null)
            {
                string classe = _claTable.GetNomById((int)article.ClaId);

                designationCourte += " - " + getStringCourt(classe, 10);
                designationLongue += " - Classe: " + classe;
            }
            //Producteur
            if (article.ProId != null)
            {
                string producteur = _proTable.GetNomCompletByProId((int)article.ProId).ToString();
                designationCourte += " - " + getStringCourt(producteur, 10);
                designationLongue += " - Producteur: " + producteur;
            }
            if (designationLongue.Length > 500)
            {
                designationLongue = designationLongue.Remove(500);
            }
            if (designationCourte.Length > 40)
            {
                designationCourte = designationCourte.Remove(40);
            }
            codArticle = codArticle.ToUpper();
            string codeGroupe;
            if (article.ClaId != null)
            {
                codeGroupe = Convert.ToString(_claTable.GetNumeroCSCV((int)article.ClaId));
            }
            else
            {
                bool identique = true;
                codeGroupe = Convert.ToString(_claTable.GetNumeroCSCV((int)_acqTable.GetClaByPed(article.PedIds[0])));
                for (int i = 1; i < article.PedIds.Length && identique; i++)
                {
                    if (codeGroupe != Convert.ToString(_claTable.GetNumeroCSCV((int)_acqTable.GetClaByPed(article.PedIds[0]))))
                    {
                        identique = false;
                        codeGroupe = string.Empty;
                    }
                }
            }
            int? mocId;
            if (article.CepId != null)
            {
                mocId = (int?)_cepTable.GetMoc(article.AnnId, (int)article.CepId);
            }
            else
            {
                mocId = (int?)_couTable.GetMocId(article.AnnId, (int)article.CouId);
                if (mocId == null)
                {
                    mocId = _annTable.GetMocId(article.AnnId);
                }
            }
            _artTable.Insert(article.AnnId, article.Litres, designationCourte, designationLongue,
                codArticle, codeGroupe, article.CouId, null, mocId, article.LitresStock, 2);
            int? artId = _artTable.GetMaxId();
            codArticle += artId;
            _artTable.UpdateLastArtCode(codArticle);
            foreach (int pedId in article.PedIds)
            {
                _pedTable.SetArtId(artId, pedId);
            }
        }
    }
}