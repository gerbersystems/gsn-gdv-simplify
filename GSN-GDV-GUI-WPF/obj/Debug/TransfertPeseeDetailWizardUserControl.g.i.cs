﻿#pragma checksum "..\..\TransfertPeseeDetailWizardUserControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "DA26AC2AA78DB43004098E74E8771BF6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AvalonWizard;
using GSN.GDV.GUI.WPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GSN.GDV.GUI.WPF {
    
    
    /// <summary>
    /// TransfertPeseeDetailWizardUserControl
    /// </summary>
    public partial class TransfertPeseeDetailWizardUserControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AvalonWizard.Wizard mainWizard;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AvalonWizard.WizardPage P0;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AvalonWizard.WizardPage page1;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal GSN.GDV.GUI.WPF.SelectionAcquitUserControl selectionAcquitUserControl;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AvalonWizard.WizardPage page2;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GSN-GDV-GUI-WPF;component/transfertpeseedetailwizardusercontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.mainWizard = ((AvalonWizard.Wizard)(target));
            
            #line 27 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.mainWizard.Cancelled += new System.Windows.RoutedEventHandler(this.OnCancelled);
            
            #line default
            #line hidden
            
            #line 29 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.mainWizard.Finished += new System.Windows.RoutedEventHandler(this.OnFinished);
            
            #line default
            #line hidden
            return;
            case 2:
            this.P0 = ((AvalonWizard.WizardPage)(target));
            
            #line 34 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.P0.Commit += new System.EventHandler<AvalonWizard.WizardPageConfirmEventArgs>(this.SelectionPeseeDetailWizardPage_Commit);
            
            #line default
            #line hidden
            
            #line 37 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.P0.Initialize += new System.EventHandler<AvalonWizard.WizardPageInitEventArgs>(this.SelectionPeseeDetailWizardPage_Initialize);
            
            #line default
            #line hidden
            return;
            case 3:
            this.page1 = ((AvalonWizard.WizardPage)(target));
            
            #line 97 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.page1.Commit += new System.EventHandler<AvalonWizard.WizardPageConfirmEventArgs>(this.SelectionAcquitWizardPage_Commit);
            
            #line default
            #line hidden
            
            #line 100 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.page1.Initialize += new System.EventHandler<AvalonWizard.WizardPageInitEventArgs>(this.SelectionAcquitWizardPage_Initialize);
            
            #line default
            #line hidden
            return;
            case 4:
            this.selectionAcquitUserControl = ((GSN.GDV.GUI.WPF.SelectionAcquitUserControl)(target));
            return;
            case 5:
            this.page2 = ((AvalonWizard.WizardPage)(target));
            
            #line 107 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.page2.Commit += new System.EventHandler<AvalonWizard.WizardPageConfirmEventArgs>(this.SaisisPeseeEnteteWizardPage_Commit);
            
            #line default
            #line hidden
            
            #line 110 "..\..\TransfertPeseeDetailWizardUserControl.xaml"
            this.page2.Initialize += new System.EventHandler<AvalonWizard.WizardPageInitEventArgs>(this.SaisisPeseeEnteteWizardPage_Initialize);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

