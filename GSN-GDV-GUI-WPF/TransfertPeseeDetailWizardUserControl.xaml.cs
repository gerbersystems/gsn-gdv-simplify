﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace GSN.GDV.GUI.WPF
{
    /// <summary>
    /// Interaction logic for TransfertPeseeDetailWizardUserControl.xaml
    /// </summary>
    public partial class TransfertPeseeDetailWizardUserControl : UserControl, ITransfertPeseeDetailWizardUserControl
    {
        public TransfertPeseeDetailWizardUserControl()
            : this(controller: null)
        {
        }

        public TransfertPeseeDetailWizardUserControl(ITransfertPeseeDetailWizardController controller)
        {
            InitializeComponent();
            this.Controller = controller;
            base.Loaded += (object sender, RoutedEventArgs e) =>
            {
                using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                    if (DesignerProperties.GetIsInDesignMode(this) == false)
                    {
                        SelectionAcquitUserControl.Controller = Controller.CreateSelectionAcquitController(ViewModel);
                        SelectionAcquitUserControl.ViewModel = ViewModel.AcquitSelector;
                    }
            };
        }

        public virtual ISelectionAcquitUserControl SelectionAcquitUserControl { get { return selectionAcquitUserControl; } }
        public virtual ITransfertPeseeDetailWizardController Controller { get; set; }

        public event System.Windows.RoutedEventHandler Cancelled;

        public event System.Windows.RoutedEventHandler Finished;

        public virtual ITransfertPeseeDetailWizardViewModel ViewModel { get { return DataContext as ITransfertPeseeDetailWizardViewModel; } set { DataContext = value; } }

        private void UpdateDataContext(AvalonWizard.WizardPage o)
        {
            var v = o.GetBindingExpression(AvalonWizard.WizardPage.DataContextProperty);
            if (v != null)
                v.UpdateTarget();
        }

        #region SelectionPeseeDetailWizardPage

        private void SelectionPeseeDetailWizardPage_Initialize(object sender, AvalonWizard.WizardPageInitEventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    Controller.InitializeSelectionPeseeDetailWizardPage(viewModel: ViewModel);
                    UpdateDataContext(e.Page);
                }
                catch (InvalidOperationException ex)
                {
                    e.Handled = true;
                    MessageBox.Show(ex.Message);
                }
        }

        private void SelectionPeseeDetailWizardPage_Commit(object sender, AvalonWizard.WizardPageConfirmEventArgs e)
        {
            try
            {
                Controller.CommitSelectionPeseeDetailWizardPage(ViewModel);

                //*****************for test : filter by Cépage

                //var t = ViewModel.AcquitSelector.Items.GetType();
                //var filteredByCepage = new List<IAcquitListViewItemModel> { };
                //var byCepage = new List<IAcquitListViewItemModel> { };
                //var byCepage = new SelectionAcquitViewModel();
                //filteredByCepage.AddRange((List<IAcquitListViewItemModel>)(from i in ViewModel.AcquitSelector where i.Cepage == ViewModel.SourceItem.Cepage select i).ToList());

                //foreach (var i in ViewModel.AcquitSelector)
                //{
                //    if (i != null && i.Cepage != null && ViewModel.SourceItem.Cepage != null &&
                //        i.Cepage.DisplayMember == ViewModel.SourceItem.Cepage.DisplayMember)
                //    {
                //        byCepage.Add(i);
                //    }
                //}

                var newModel = new SelectionAcquitViewModel
                               {
                                   Items = ViewModel.AcquitSelector,
                                   LieuSelector = ViewModel.AcquitSelector.LieuSelector,
                                   ProducteurSelector = ViewModel.AcquitSelector.ProducteurSelector,
                                   Selected = ViewModel.AcquitSelector.Selected,
                                   AutreMentionSelector = ViewModel.AcquitSelector.AutreMentionSelector,
                                   CepageSelector = ViewModel.AcquitSelector.CepageSelector
                               };

                //var nb = new List<ISelectorItemModel<int, string>> { };
                //var nb = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>();
                //foreach (var i in ViewModel.AcquitSelector.CepageSelector)
                //{
                //    if (i != null && i.DisplayMember != null && ViewModel.SourceItem.Cepage.DisplayMember != null && i.DisplayMember == ViewModel.SourceItem.Cepage.DisplayMember)
                //    {
                //        nb.Add(i);
                //    }
                //}
                //if (nb != null)
                //    newModel.CepageSelector.Items = nb.ToList();
                //var g = ViewModel.AcquitSelector.CepageSelector.GetType();
                //var z = ViewModel.AcquitSelector.CepageSelector.Equals(ViewModel.SourceItem.Cepage);
                //newModel.CepageSelector.Items = ViewModel.AcquitSelector.CepageSelector.Where(x => x.DisplayMember == ViewModel.SourceItem.Cepage.DisplayMember);
                //newModel.CepageSelector.Items = ViewModel.AcquitSelector.CepageSelector.Items.Where(
                //    x =>
                //    {
                //        var a = x.DisplayMember;
                //        return (a == null) ? false : x.DisplayMember == ViewModel.SourceItem.Cepage.DisplayMember;
                //    });
                //newModel.CepageSelector = ViewModel.SourceItem.Cepage;
                //var x = byCepage as ISelectionAcquitViewModel;
                //*************************************************************

                //this.SelectionAcquitUserControl.Controller.Set(ViewModel.AcquitSelector);
                SelectionAcquitUserControl.Controller.Set(newModel);
                UpdateDataContext(e.Page);
            }
            catch (InvalidOperationException ex)
            {
                e.Cancel = true;
                e.Handled = true;
                MessageBox.Show(ex.Message);
            }
        }

        #endregion SelectionPeseeDetailWizardPage

        #region SelectionAcquitWizardPage

        private void SelectionAcquitWizardPage_Initialize(object sender, AvalonWizard.WizardPageInitEventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    Controller.InitializeSelectionAcquitWizardPage(viewModel: ViewModel);
                    UpdateDataContext(e.Page);
                }
                catch (InvalidOperationException ex)
                {
                    e.Handled = true;
                    MessageBox.Show(ex.Message);
                }
        }

        private void SelectionAcquitWizardPage_Commit(object sender, AvalonWizard.WizardPageConfirmEventArgs e)
        {
            try
            {
                Controller.CommitSelectionAcquitWizardPage(viewModel: ViewModel);
                UpdateDataContext(e.Page);
            }
            catch (InvalidOperationException ex)
            {
                e.Cancel = true;
                e.Handled = true;
                MessageBox.Show(ex.Message);
            }
        }

        #endregion SelectionAcquitWizardPage

        #region SaisisPeseeEnteteWizardPage

        private void SaisisPeseeEnteteWizardPage_Commit(object sender, AvalonWizard.WizardPageConfirmEventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    Controller.CommitSaisisPeseeEnteteWizardPage(viewModel: ViewModel);
                    UpdateDataContext(e.Page);
                }
                catch (InvalidOperationException ex)
                {
                    e.Cancel = true;
                    e.Handled = true;
                    MessageBox.Show(ex.Message);
                }
        }

        private void SaisisPeseeEnteteWizardPage_Initialize(object sender, AvalonWizard.WizardPageInitEventArgs e)
        {
            try
            {
                Controller.InitializeSaisisPeseeEnteteWizardPage(viewModel: ViewModel);
                UpdateDataContext(e.Page);
            }
            catch (InvalidOperationException ex)
            {
                e.Handled = true;
                MessageBox.Show(ex.Message);
            }
        }

        #endregion SaisisPeseeEnteteWizardPage

        private void OnFinished(object sender, RoutedEventArgs e)
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                try
                {
                    Controller.Transfert(viewModel: ViewModel);
                    if (Finished != null)
                        Finished(sender, e: e);
                }
                catch (InvalidOperationException ex)
                {
                    e.Handled = true;
                    MessageBox.Show(ex.Message);
                }
        }

        private void OnCancelled(object sender, RoutedEventArgs e)
        {
            if (Cancelled != null)
                Cancelled(sender, e: e);
        }
    }

    public interface ITransfertPeseeDetailWizardUserControl
    {
        ITransfertPeseeDetailWizardController Controller { get; set; }
        ITransfertPeseeDetailWizardViewModel ViewModel { get; set; }

        event System.Windows.RoutedEventHandler Cancelled;

        event System.Windows.RoutedEventHandler Finished;
    }
}