﻿using GSN.GDV.GUI.Models;
using System.Windows.Controls;

namespace GSN.GDV.GUI.WPF
{
    /// <summary>
    /// Interaction logic for LimitationUserControl.xaml
    /// </summary>
    public partial class LimitationUserControl : UserControl, ILimitationUserControl
    {
        public LimitationUserControl()
        {
            InitializeComponent();
        }

        public virtual ILimitationViewModel ViewModel { get { return DataContext as ILimitationViewModel; } set { DataContext = value; } }
    }

    public interface ILimitationUserControl
    {
        ILimitationViewModel ViewModel { get; set; }
    }
}