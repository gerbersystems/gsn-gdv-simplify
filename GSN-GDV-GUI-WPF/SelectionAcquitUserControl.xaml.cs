﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Models;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace GSN.GDV.GUI.WPF
{
    /// <summary>
    /// Interaction logic for SelectionAcquitUserControl.xaml
    /// </summary>
    public partial class SelectionAcquitUserControl : UserControl, ISelectionAcquitUserControl
    {
        public SelectionAcquitUserControl()
            : this(controller: null)
        {
        }

        public SelectionAcquitUserControl(ISelectionAcquitController controller = null)
        {
            InitializeComponent();
            this.Controller = controller;
        }

        public virtual ISelectionAcquitController Controller { get; set; }
        public virtual ISelectionAcquitViewModel ViewModel { get { return DataContext as ISelectionAcquitViewModel; } set { DataContext = value; } }

        public event SelectionChangedEventHandler CepageSelectionChanged;

        public event SelectionChangedEventHandler ProducteurSelectionChanged;

        public event SelectionChangedEventHandler LieuxSelectionChanged;

        public event SelectionChangedEventHandler AutreMentionSelectionChanged;

        public event SelectionChangedEventHandler AcquitSelectionChanged;

        private void OnCepageSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CepageSelectionChanged != null)
                CepageSelectionChanged(sender, e);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                Controller.ExecuteFilterByCepage(ViewModel);
            UpdateBinding();
        }

        private void OnProducteurSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ProducteurSelectionChanged != null)
                ProducteurSelectionChanged(sender, e);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                Controller.ExecuteFilterByProducteur(ViewModel);
            UpdateBinding();
        }

        private void OnLieuSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LieuxSelectionChanged != null)
                LieuxSelectionChanged(sender, e);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                Controller.ExecuteFilterByLieu(ViewModel);
            UpdateBinding();
        }

        private void OnAutreMentionSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AutreMentionSelectionChanged != null)
                AutreMentionSelectionChanged(sender, e);
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                Controller.ExecuteFilterByAutreMention(ViewModel);
            UpdateBinding();
        }

        private void OnAcquitSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AcquitSelectionChanged != null)
                AcquitSelectionChanged(sender, e);
        }

        private void OnExecuteFilterBySelection(object sender, SelectionChangedEventArgs e)
        {
            ExecuteFilterBySelection();
        }

        private void OnFilterIgnoreChanged(object sender, RoutedEventArgs e)
        {
            ExecuteFilterBySelection();
        }

        private void ExecuteFilterBySelection()
        {
            using (new GSN.GDV.GUI.Common.WorkingGlass(this))
                Controller.ExecuteFilterBySelection(ViewModel);
            UpdateBinding();
        }

        private void UpdateBinding()
        {
            foreach (var o in new CheckBox[] { producteurCheckBox, cepageCheckBox, lieuCheckBox, autreMentionCheckBox, })
            {
                var a = new BindingExpression[]{
                 o.GetBindingExpression(CheckBox.IsEnabledProperty),
                 o.GetBindingExpression(CheckBox.IsCheckedProperty),
                };
                foreach (var p in a)
                    if (p != null)
                        p.UpdateTarget();
            }
            foreach (var o in new System.Windows.Controls.Primitives.Selector[] { producteurComboBox, cepageComboBox, lieuComboBox, autreMentionComboBox, acquitDataGrid })
            {
                var a = new BindingExpression[]{
                    o.GetBindingExpression(System.Windows.Controls.Primitives.Selector.IsEnabledProperty),
                    o.GetBindingExpression(System.Windows.Controls.Primitives.Selector.ItemsSourceProperty),
                    o.GetBindingExpression(System.Windows.Controls.Primitives.Selector.SelectedItemProperty),
                    o.GetBindingExpression(System.Windows.Controls.Primitives.Selector.SelectedValueProperty),
                };
                foreach (var p in a)
                    if (p != null)
                        p.UpdateTarget();
            }
            this.UpdateLayout();
        }
    }

    public interface ISelectionAcquitUserControl
    {
        ISelectionAcquitController Controller { get; set; }
        ISelectionAcquitViewModel ViewModel { get; set; }
    }
}