﻿using GSN.GDV.GUI.Common;
using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Models;
using System.Windows;
using System.Windows.Controls;

namespace GSN.GDV.GUI.WPF
{
    /// <summary>
    /// Interaction logic for SaisieDetailPeseeUserControl.xaml
    /// </summary>
    public partial class SaisieDetailPeseeUserControl : UserControl, ISaisieDetailPeseeUserControl
    {
        public SaisieDetailPeseeUserControl()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler Cancelled;

        public event RoutedEventHandler Commited;

        private void OnImprimer(object sender, RoutedEventArgs e)
        {
            Controller.ExecuteImprimer(ViewModel);
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            Controller.ExecuteOK(ViewModel);
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            Controller.ExecuteAnnuler(ViewModel);
        }

        public ISaisieDetailPeseeController Controller { get; set; }

        public virtual ISaisieDetailPeseeViewModel ViewModel
        {
            get { return DataContext as ISaisieDetailPeseeViewModel; }
            set { DataContext = value; }
        }

        private void QuantiteKilogrammes_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                using (new WorkingGlass(this))
                {
                    Controller.DoQuantiteKilogrammesChanged(ViewModel);
                }
            }

            UpdateLimitation(sender as TextBox);
        }

        private void QuantiteLitres_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                using (new WorkingGlass(this))
                {
                    Controller.DoQuantiteLitresChanged(ViewModel);
                }
            }

            UpdateLimitation(sender as TextBox);
        }

        private void ConversionLitresKilogrammes_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                using (new WorkingGlass(this))
                {
                    Controller.DoConversionLitresKilogrammesChanged(ViewModel);
                }
            }

            UpdateLimitation(sender as TextBox);
        }

        private bool _isUpdateLimitationStarted = false;

        private void UpdateLimitation(TextBox txt)
        {
            if (ViewModel == null || _isUpdateLimitationStarted) return;

            using (new WorkingGlass(this))
            {
                try
                {
                    _isUpdateLimitationStarted = true;
                    var vm = ViewModel;
                    var dc = DataContext;
                    DataContext = null;
                    Controller.UpdateLimitation(vm);
                    DataContext = dc;
                }
                finally
                {
                    _isUpdateLimitationStarted = false;
                }
            }
        }
    }

    public interface ISaisieDetailPeseeUserControl
    {
        ISaisieDetailPeseeController Controller { get; set; }
        ISaisieDetailPeseeViewModel ViewModel { get; set; }

        event RoutedEventHandler Cancelled;

        event RoutedEventHandler Commited;
    }
}