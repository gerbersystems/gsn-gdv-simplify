﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Models;
using System.Windows;
using System.Windows.Controls;

namespace GSN.GDV.GUI.WPF
{
    /// <summary>
    /// Interaction logic for SaisieEntetePeseeUserControl.xaml
    /// </summary>
    public partial class SaisieEntetePeseeUserControl : UserControl, ISaisieEntetePeseeUserControl
    {
        public SaisieEntetePeseeUserControl()
        {
            InitializeComponent();
            base.Loaded += (object sender, RoutedEventArgs e) =>
            {
                SelectionAcquitUserControl.Controller = Controller.CreateSelectionAcquitController(ViewModel);
            };
        }

        public virtual ISaisieEntetePeseeController Controller { get; set; }
        public virtual ISaisieEntetePeseeViewModel ViewModel { get { return DataContext as ISaisieEntetePeseeViewModel; } set { DataContext = value; } }
        public virtual ISelectionAcquitUserControl SelectionAcquitUserControl { get { return selectionAcquitUserControl; } }

        private void OnPeseeDetailSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }

    public interface ISaisieEntetePeseeUserControl
    {
        ISaisieEntetePeseeController Controller { get; set; }
        ISaisieEntetePeseeViewModel ViewModel { get; set; }
        ISelectionAcquitUserControl SelectionAcquitUserControl { get; }
    }
}