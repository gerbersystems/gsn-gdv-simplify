﻿using Microsoft.Reporting.WinForms;
using System.Collections.Generic;

namespace GSN.GDV.Reports
{
    public interface IReportInfo
    {
        List<ReportDataSource> DataSources { get; }
        FormatReport Format { get; }
        string Name { get; }
        List<ReportDataSource> SubReportDataSources { get; }
    }
}