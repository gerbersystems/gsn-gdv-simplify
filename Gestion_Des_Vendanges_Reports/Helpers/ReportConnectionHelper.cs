﻿using GSN.GDV.Business;

namespace Gestion_Des_Vendanges_Reports.Helpers
{
    public static class ReportConnectionHelper
    {
        private static IConnectionManager _connectionManager = null;

        public static void Init(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        public static IConnectionManager GetConnectionManager()
        {
            return _connectionManager;
        }
    }
}