﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_QuittanceV2Report : ReportInfo
    {
        public R_QuittanceV2Report(FormatReport format, int peeID = 0, int annID = 0, int proID = 0 )
            : base(format, "R_QuittanceV2")
        {
            DAO.DSReportsTableAdapters.R_QuittanceV2TableAdapter attTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_QuittanceV2TableAdapter>();
            DAO.DSReportsTableAdapters.R_QuittanceV2DetailPeseesTableAdapter attDetailPeseesTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_QuittanceV2DetailPeseesTableAdapter>();

            if (peeID == 0)
            {
                if (proID == 0)
                {
                    attTable.FillByAnnee(DsReports.R_QuittanceV2, annID);
                    attDetailPeseesTable.FillByAnnee(dataTable: DsReports.R_QuittanceV2DetailPesees, annid: annID);

                    AddDataSource("R_QuittanceV2DataSet", attTable.GetDataByAnnee(annID));
                    AddSubReportDataSource("R_QuittanceV2DetailPeseesDataSet", attDetailPeseesTable.GetDataByAnnee(annID));
                }
                else
                {
                    attTable.FillByAnneeAndProducteur(DsReports.R_QuittanceV2, annID, proID);
                    attDetailPeseesTable.FillByAnneeAndProducteur(dataTable: DsReports.R_QuittanceV2DetailPesees, annid: annID, proid: proID);

                    AddDataSource("R_QuittanceV2DataSet", attTable.GetDataByAnneeAndProducteur(annID, proID));
                    AddSubReportDataSource("R_QuittanceV2DetailPeseesDataSet", attDetailPeseesTable.GetDataByAnneeAndProducteur(annID, proID));
                }
            }
            else
            {
                attTable.FillByPEEID(DsReports.R_QuittanceV2, PEE_ID: peeID);
                attDetailPeseesTable.FillByPEEID(dataTable: DsReports.R_QuittanceV2DetailPesees, peeid: peeID);

                AddDataSource("R_QuittanceV2DataSet", attTable.GetDataByPEEID(PEE_ID: peeID));
                AddSubReportDataSource("R_QuittanceV2DetailPeseesDataSet", attDetailPeseesTable.GetDataByPEEID(peeid: peeID));
            }
        }
    }
}