﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ApportsParProducteurReport : ReportInfo
    {
        public R_ApportsParProducteurReport(FormatReport format, int annId, int peeId = 0)
            : base(format, "R_ApportsParProducteur")
        {
            DAO.DSReportsTableAdapters.R_ApportsProducteurTableAdapter appTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ApportsProducteurTableAdapter>();

            if(peeId == 0)
            {
                appTable.FillByAnn(DsReports.R_ApportsProducteur, annId);
                AddDataSource("R_ApportsParProducteurDataSet", appTable.GetDataByAnn(annId));
            }
            else
            {
                appTable.FillByAnnPee(DsReports.R_ApportsProducteur, annId, peeId);
                AddDataSource("R_ApportsParProducteurDataSet", appTable.GetDataByAnnPee(annId, peeId));
            }
        }
    }
}
