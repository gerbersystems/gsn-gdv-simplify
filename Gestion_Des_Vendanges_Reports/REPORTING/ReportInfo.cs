﻿using GSN.GDV.Reports;
using Microsoft.Reporting.WinForms;
using System.Collections.Generic;
using System.Data;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public abstract class ReportInfo : IReportInfo
    {
        private List<ReportDataSource> _dataSources;
        private List<ReportDataSource> _subReportDataSources;
        private FormatReport _format;
        private string _name;
        private DAO.DSReports _dsReports;

        public FormatReport Format
        {
            get { return _format; }
            set { _format = value; }
        }

        public DAO.DSReports DsReports
        {
            get { return _dsReports; }
            set { _dsReports = value; }
        }

        public string Name
        {
            get { return _name; }
        }

        public List<ReportDataSource> DataSources
        {
            get { return _dataSources; }
        }

        public List<ReportDataSource> SubReportDataSources
        {
            get { return _subReportDataSources; }
        }

        public ReportInfo(FormatReport format, string nameReportFileWithoutExtension)
        {
            _dsReports = new DAO.DSReports();
            _format = format;
            _name = nameReportFileWithoutExtension;
            _dataSources = new List<ReportDataSource>();
            _subReportDataSources = new List<ReportDataSource>();
        }

        protected void AddDataSource(string name, DataTable table)
        {
            _dataSources.Add(new ReportDataSource(name, table));
        }

        protected void AddSubReportDataSource(string name, DataTable table)
        {
            _subReportDataSources.Add(new ReportDataSource(name, table));
        }

        //public ReportInfo(FormatReport format, string name, bool isDSReport)
        //{
        //    if (isDSReport == true)
        //    {
        //        //_dsReports = new DAO.DSReports();
        //    }
        //    else
        //    {
        //        //_dsPessees = new DAO.DSPesees();
        //    }
        //    _format = format;
        //    _name = name;
        //    _dataSources = new List<ReportDataSource>();
        //    _subReportDataSources = new List<ReportDataSource>();
        //}
    }
}