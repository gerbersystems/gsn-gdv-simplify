﻿using Gestion_Des_Vendanges_Reports.Helpers;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_testReport : ReportInfo
    {
        public R_testReport(FormatReport format)
            : base(format, "R_test")
        {
            DAO.DSReportsTableAdapters.R_TestTableAdapter listeTable = ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_TestTableAdapter>();
            listeTable.Fill(DsReports.R_Test);
            AddDataSource("R_testDataSet", listeTable.GetData());
        }
    }
}