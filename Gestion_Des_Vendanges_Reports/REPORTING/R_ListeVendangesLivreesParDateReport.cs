﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeVendangesLivreesParDateReport : ReportInfo
    {
        public R_ListeVendangesLivreesParDateReport(FormatReport format, int aNNID)
            : base(format: format, nameReportFileWithoutExtension: "R_ListeVendangesLivreesParDate")
        {
            DAO.DSReportsTableAdapters.R_VendangesLivreesParDateTableAdapter listeTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_VendangesLivreesParDateTableAdapter>();

            listeTable.FillByANNID(DsReports.R_VendangesLivreesParDate, aNNID);
            AddDataSource("ListeVendangesLivreesParDateDataset", listeTable.GetDataByANNID(aNNID));
        }
    }
}