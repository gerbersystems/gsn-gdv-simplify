﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_SondageMoyenParLieuDeProductionReport : ReportInfo
    {
        public R_SondageMoyenParLieuDeProductionReport(FormatReport format, int annID)
            : base(format, "R_SondageMoyenParLieuDeProduction")
        {
            DAO.DSReportsTableAdapters.R_SondageMoyenParLieuDeProductionTableAdapter _tableAdapterSondageMoyen = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_SondageMoyenParLieuDeProductionTableAdapter>();
            DAO.DSReportsTableAdapters.R_EncaveurTableAdapter _tableAdapterEncaveur = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_EncaveurTableAdapter>();

            _tableAdapterSondageMoyen.FillByAnnId(DsReports.R_SondageMoyenParLieuDeProduction, ANN_ID: annID);
            _tableAdapterEncaveur.FillByAn(DsReports.R_Encaveur, ANN_ID: annID);

            AddDataSource("R_SondageMoyenParLieuDeProductionDataSet", _tableAdapterSondageMoyen.GetDataByAnnId(ANN_ID: annID));
            AddDataSource("R_EncaveurDataSet", _tableAdapterEncaveur.GetDataByAn(ANN_ID: annID));
        }
    }
}