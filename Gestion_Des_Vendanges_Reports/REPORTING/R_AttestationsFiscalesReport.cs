﻿using GSN.GDV.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_AttestationsFiscalesReport : ReportInfo
    {
        public R_AttestationsFiscalesReport(FormatReport format, int annId, int proId)
            : base(format, "R_AttestationsFiscales")
        {
            DAO.DSReportsTableAdapters.ANN_ANNEETableAdapter annTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.ANN_ANNEETableAdapter>();

            var annee = (int)(annTable.GetAnneeByAnneeId(annId) ?? -1);

            if (annee == -1)
                return;

            DAO.DSReportsTableAdapters.R_AttestationsFiscalesTableAdapter attTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationsFiscalesTableAdapter>();
            DAO.DSReportsTableAdapters.R_EncaveurTableAdapter encTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_EncaveurTableAdapter>();

            encTable.FillByAn(DsReports.R_Encaveur, annId);
            AddDataSource("R_EncaveurDataSet", encTable.GetDataByAn(annId));

            if (proId == 0)
            {
                attTable.FillByAnnee(DsReports.R_AttestationsFiscales, annee);
                AddDataSource("R_AttestationsFiscalesDataSet", attTable.GetDataByAnnee(annee));
            }
            else
            {
                attTable.FillByAnneeProducteurId(DsReports.R_AttestationsFiscales, annee, proId);
                AddDataSource("R_AttestationsFiscalesDataSet", attTable.GetDataByAnneeProducteurId(annee, proId));
            }
        }
    }
}