﻿using Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters;
using GSN.GDV.Reports;
using System;
using System.Data;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_PaiementFactureReport : ReportInfo
    {
        private readonly int annId;
        private R_EncaveurTableAdapter encTable;
        private R_PaiementFactureTableAdapter paiTable;

        public R_PaiementFactureReport(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
            : base(format, "R_PaiementFacture")
        {
            encTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_EncaveurTableAdapter>();
            paiTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_PaiementFactureTableAdapter>();

            this.annId = annId;

            if (paiId > 0)
                FillReportDataSourceByPaiementId(paiId);
            else
                FillReportDataSource(proId, dateRangeFrom, dateRangeTo);

            encTable.FillByAn(DsReports.R_Encaveur, annId);
            AddDataSource("R_Encaveur", (DataTable)encTable.GetDataByAn(annId));
        }

        private static bool IsDateRangeValid(DateTime? dateRangeFrom, DateTime? dateRangeTo)
        {
            return (dateRangeFrom != null && dateRangeTo != null);
        }

        private void FillReportDataSource(int proId, DateTime? dateRangeFrom, DateTime? dateRangeTo)
        {
            if (!IsAnnIdSelected())
                return;

            if (proId < 1)
                FillReportDataSourceFilteredByDateRange(dateRangeFrom, dateRangeTo);
            else
                FillReportDataSourceFilteredByProducteurIdAndDateRange(proId, dateRangeFrom, dateRangeTo);
        }

        private void FillReportDataSourceByAnnId()
        {
            paiTable.FillByAnnId(DsReports.R_PaiementFacture, annId);
            AddDataSource("R_PaiementFacture", (DataTable)paiTable.GetDataByAnnId(annId));
        }

        private void FillReportDataSourceByPaiementId(int paiId)
        {
            paiTable.FillByPaiId(DsReports.R_PaiementFacture, paiId);
            AddDataSource("R_PaiementFacture", (DataTable)paiTable.GetDataByPaiId(paiId));
        }

        private void FillReportDataSourceByProducteurId(int proId)
        {
            paiTable.FillByProId(DsReports.R_PaiementFacture, annId, proId);
            AddDataSource("R_PaiementFacture", (DataTable)paiTable.GetDataByProId(annId, proId));
        }

        private void FillReportDataSourceFilteredByDateRange(DateTime? dateRangeFrom, DateTime? dateRangeTo)
        {
            if (IsDateRangeValid(dateRangeFrom, dateRangeTo))
            {
                paiTable.FillByDateRange(DsReports.R_PaiementFacture, annId, dateRangeFrom, dateRangeTo);
                AddDataSource("R_PaiementFacture", (DataTable)paiTable.GetDataByDateRange(annId, dateRangeFrom, dateRangeTo));
            }
            else
            {
                FillReportDataSourceByAnnId();
            }
        }

        private void FillReportDataSourceFilteredByProducteurIdAndDateRange(int proId, DateTime? dateRangeFrom, DateTime? dateRangeTo)
        {
            if (IsDateRangeValid(dateRangeFrom, dateRangeTo))
            {
                paiTable.FillByProIdDateRange(DsReports.R_PaiementFacture, annId, proId, dateRangeFrom, dateRangeTo);
                AddDataSource("R_PaiementFacture", (DataTable)paiTable.GetDataByProIdDateRange(annId, proId, dateRangeFrom, dateRangeTo));
            }
            else
            {
                FillReportDataSourceByProducteurId(proId);
            }
        }

        private bool IsAnnIdSelected()
        {
            if (annId <= 0)
                System.Windows.Forms.MessageBox.Show("Veuillez sélectionner une année");

            return (annId > 0);
        }
    }
}