﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_AttestationControleV2Report : ReportInfo
    {
        public R_AttestationControleV2Report(FormatReport format, int annID, int proID = 0, int peeID = 0)
            : base(format, "R_AttestationControleV2-1")
        {
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeTableAdapter attTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeTableAdapter>();
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailPeseesTableAdapter attDetailPeseesTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailPeseesTableAdapter>();

            if (peeID == 0)
            {
                if (proID == 0)
                {
                    attTable.FillByAnnee(DsReports.R_AttestationControleAvecSolde, annID);
                    attDetailPeseesTable.FillByAnnee(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, annid: annID);

                    AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByAnnee(annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnnee(annID));
                }
                else
                {
                    attTable.FillByAnneeAndProducteur(DsReports.R_AttestationControleAvecSolde, annID, proID);
                    attDetailPeseesTable.FillByAnneeAndProducteur(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, annid: annID, proid: proID);

                    AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByAnneeAndProducteur(annID, proID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnneeAndProducteur(annID, proID));
                }
            }
            else
            {
                attTable.FillByPEEID(DsReports.R_AttestationControleAvecSolde, PEE_ID: peeID);
                attDetailPeseesTable.FillByPEEID(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, peeid: peeID);

                AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByPEEID(PEE_ID: peeID));
                AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByPEEID(peeid: peeID));
            }
        }
    }
}