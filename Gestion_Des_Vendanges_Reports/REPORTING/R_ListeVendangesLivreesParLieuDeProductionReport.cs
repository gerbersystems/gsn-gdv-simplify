﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeVendangesLivreesParLieuDeProductionReport : ReportInfo
    {
        public R_ListeVendangesLivreesParLieuDeProductionReport(FormatReport format, int annID)
            : base(format, "R_ListeVendangesLivreesParLieuDeProduction")
        {
            DAO.DSReportsTableAdapters.R_VendangesLivreesParLieuDeProductionTableAdapter _tableAdapter = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_VendangesLivreesParLieuDeProductionTableAdapter>();

            _tableAdapter.FillByAnnId(DsReports.R_VendangesLivreesParLieuDeProduction, ANN_ID: annID);

            AddDataSource("R_VendangesLivreesParLieuDeProductionDataSet", _tableAdapter.GetDataByAnnId(ANN_ID: annID));
        }
    }
}