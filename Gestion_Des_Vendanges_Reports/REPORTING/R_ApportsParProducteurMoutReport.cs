﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ApportsParProducteurMoutReport : ReportInfo
    {
        public R_ApportsParProducteurMoutReport(FormatReport format, int annId, int peeId = 0)
            : base(format, "R_ApportsParProducteurMout")
        {
            DAO.DSReportsTableAdapters.R_ApportsProducteurMoutTableAdapter appTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ApportsProducteurMoutTableAdapter>();

            if (peeId == 0)
            {
                appTable.FillByAnn(DsReports.R_ApportsProducteurMout, annId);
                AddDataSource("R_ApportsProducteurMoutDataSet", appTable.GetDataByAnn(annId));
            }
            else
            {
                appTable.FillByAnnPee(DsReports.R_ApportsProducteurMout, annId, peeId);
                AddDataSource("R_ApportsProducteurMoutDataSet", appTable.GetDataByAnnPee(annId, peeId));
            }
        }
    }
}
