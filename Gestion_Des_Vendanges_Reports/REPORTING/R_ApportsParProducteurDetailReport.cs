﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ApportsParProducteurDetailReport : ReportInfo
    {
        //Non implémenté dans le logiciel. Problème avec les totaux de droit en litres, droit en kg et solde à livrer : le rapport prend en compte chaque ligne de pesée
        //pour calculer le total au lieu de prendre qu'une fois la valeur par acquit
        public R_ApportsParProducteurDetailReport(FormatReport format, int annId, int peeId = 0)
            : base(format, "R_ApportsParProducteurDetail")
        {
            DAO.DSReportsTableAdapters.R_ApportsProducteurDetailTableAdapter appTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ApportsProducteurDetailTableAdapter>();
            //quiTable.Connection.ConnectionString = DAO.ConnectionManager.Instance._gvConnectionString;
            if (peeId == 0)
            {
                appTable.FillByAnn(DsReports.R_ApportsProducteurDetail, annId);
                AddDataSource("R_ApportsParProducteurDetailDataSet", appTable.GetDataByAnn(annId));
            }
            else
            {
                appTable.FillByAnnPee(DsReports.R_ApportsProducteurDetail, annId, peeId);
                AddDataSource("R_ApportsParProducteurDetailDataSet", appTable.GetDataAnnPee(annId, peeId));
            }
        }
    }
}
