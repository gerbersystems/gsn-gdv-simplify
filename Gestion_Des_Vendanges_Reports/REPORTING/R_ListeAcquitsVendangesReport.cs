﻿using GSN.GDV.Reports;
using System;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeAcquitsVendangesReport : ReportInfo
    {
        public R_ListeAcquitsVendangesReport(FormatReport format, int annId, float taux)
            : base(format, "R_ListeAcquitsVendanges")
        {
            decimal tauxInDecimal = Convert.ToDecimal(taux);
            DAO.DSReportsTableAdapters.R_ListeAcquitsVendangeTableAdapter listeTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ListeAcquitsVendangeTableAdapter>();
            listeTable.FillAnneeID(DsReports.R_ListeAcquitsVendange, tauxInDecimal, annId);
            AddDataSource("R_ListeAcquitsVendangeDataSet", listeTable.GetDataAnneeID(tauxInDecimal, annId));
        }
    }
}