﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeVendangesParCepageReport : ReportInfo
    {
        public R_ListeVendangesParCepageReport(FormatReport format, int annID)
            : base(format, "R_ListeVendangesParCepage")
        {
            DAO.DSReportsTableAdapters.R_ListeVendangesTableAdapter liste = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ListeVendangesTableAdapter>();
            liste.FillByAnnID(DsReports.R_ListeVendanges, ANNID: annID);
            AddDataSource("R_ListeVendangesCepageDataSet", liste.GetDataByAnnID(ANNID: annID));
        }
    }
}