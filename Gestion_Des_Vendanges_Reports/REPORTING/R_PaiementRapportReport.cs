﻿using GSN.GDV.Reports;
using System.Data;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_PaiementRapportReport : ReportInfo
    {
        public R_PaiementRapportReport(FormatReport format, int annId = 0)
            : base(format: format, nameReportFileWithoutExtension: "R_PaiementRapport")
        {
            DAO.DSReportsTableAdapters.R_PaiementRapportTableAdapter paiementRapportTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_PaiementRapportTableAdapter>();
            if (annId > 0)
            {
                paiementRapportTable.FillByAnnId(dataTable: DsReports.R_PaiementRapport, ANN_ID: annId);
                AddDataSource("R_PaiementRapport", (DataTable)paiementRapportTable.GetDataByAnnId(ANN_ID: annId));
            }
        }
    }
}