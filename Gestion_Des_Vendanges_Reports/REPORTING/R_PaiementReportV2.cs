﻿using Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters;
using GSN.GDV.Reports;
using System;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_PaiementReportV2 : ReportInfo
    {
        public R_PaiementReportV2(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
            : base(format, "R_PaiementV2")
        {
            var encTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_EncaveurTableAdapter>();
            var paiTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_PaiementTableAdapter>();

            if (paiId > 0)
            {
                AddDataSource("R_Paiement", paiTable.GetDataByPai(paiId));
            }
            else
            {
                if (annId <= 0)
                {
                    MessageBox.Show("Veuillez sélectionner une année");
                }
                else
                {
                    if (proId > 0)
                    {
                        if (dateRangeFrom == null || dateRangeTo == null)
                        {
                            AddDataSource("R_Paiement", paiTable.GetDataByAnnPro(annId, proId));
                        }
                        else
                        {
                            AddDataSource("R_Paiement", paiTable.GetDataByAnnProDateRange(annId, proId, dateRangeFrom.GetValueOrDefault(), dateRangeTo.GetValueOrDefault()));
                        }
                    }
                    else
                    {
                        if (dateRangeFrom == null || dateRangeTo == null)
                        {
                            AddDataSource("R_Paiement", paiTable.GetDataByAn(annId));
                        }
                        else
                        {
                            try
                            {
                                AddDataSource("R_Paiement", paiTable.GetDataByAnDateRange(annId, dateRangeFrom.GetValueOrDefault(), dateRangeTo.GetValueOrDefault()));
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show($"Erreur lors de la création du rapport\n{ex.Message}",
                                                "Erreur rapports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                throw new Exception(nameof(paiTable.GetDataByAnDateRange));
                            }
                        }
                    }
                }
            }

            AddDataSource("R_Encaveur", encTable.GetDataByAn(annId));
        }
    }
}