﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_AttestationControleAvecSoldeParAcquitReport : ReportInfo
    {
        public R_AttestationControleAvecSoldeParAcquitReport(FormatReport format, int annID, int acqID = 0, int peeID = 0, int proID = 0)
            : base(format, "R_AttestationControleAvecSoldeParAcquit")
        {
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitTableAdapter attTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitTableAdapter>();
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitDetailTableAdapter attDetailTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitDetailTableAdapter>();
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitDetailPeseesTableAdapter attDetailPeseesTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeParAcquitDetailPeseesTableAdapter>();
            DAO.DSReportsTableAdapters.R_ProprietairePeseeTableAdapter proTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ProprietairePeseeTableAdapter>();

            if (peeID == 0)
            {
                if (proID == 0)
                {
                    attTable.FillByAnnee(DsReports.R_AttestationControleAvecSoldeParAcquit, annID);
                    attDetailTable.FillByAnnee(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetail, annid: annID);
                    attDetailPeseesTable.FillByAnnee(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetailPesees, annid: annID);

                    AddDataSource("R_AttestationControleAvecSoldeParAcquitDataSet", attTable.GetDataByAnnee(annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailDataSet", attDetailTable.GetDataByAnnee(annid: annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnnee(annID));

                }
                else
                {
                    attTable.FillByAnneeAndProducteur(DsReports.R_AttestationControleAvecSoldeParAcquit, annID, proID);
                    attDetailTable.FillByAnneeAndProducteur(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetail, annid: annID, proid: proID);
                    attDetailPeseesTable.FillByAnneeAndProducteur(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetailPesees, annid: annID, proid: proID);

                    AddDataSource("R_AttestationControleAvecSoldeParAcquitDataSet", attTable.GetDataByAnneeAndProducteur(annID, proID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailDataSet", attDetailTable.GetDataByAnneeAndProducteur(annid: annID, proid: proID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnneeAndProducteur(annID, proID));
                }
            }
            else
            {
                attTable.Fill(DsReports.R_AttestationControleAvecSoldeParAcquit, PEE_ID: peeID);
                //attTable.Fill(DsReports.R_AttestationControle, PEE_ID: peeID);
                //attTable.FillByAcquitProducteurPesee(DsReports.R_AttestationControle, PEE_ID: peeID, ACQID: acqID);
                //attTable.FillByPEEID(DsReports.R_AttestationControle, PEE_ID: peeID);
                attDetailTable.FillByAcquitPeseseProducteur(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetail, proid: proID, annid: annID, ACQID: acqID);
                //attDetailTable.FillByAnneeAndProprietaire(dataTable: DsReports.R_AttestationControleDetail, taux: taux, proid: proID, annid: annID);
                attDetailPeseesTable.Fill(dataTable: DsReports.R_AttestationControleAvecSoldeParAcquitDetailPesees, pee_id: peeID);
                proTable.FillByPEEID(dataTable: DsReports.R_ProprietairePesee, PEEID: peeID);

                AddDataSource("R_AttestationControleAvecSoldeParAcquitDataSet", attTable.GetData(PEE_ID: peeID));
                //AddDataSource("R_AttestationControlDataSet", attTable.GetDataByAcquitProducteurPesee(PEE_ID: peeID, ACQID: acqID));
                //AddDataSource("R_AttestationControlDataSet", attTable.GetDataByPEEID(PEE_ID: peeID));
                AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailDataSet", attDetailTable.GetDataByAcquitPeseseProducteur(proid: proID, annid: annID, ACQID: acqID));
                //AddSubReportDataSource("R_AttestationControleDetailDataSet", attDetailTable.GetDataByAnneeAndProprietaire(taux: taux, proid: proID, annid: annID));
                AddSubReportDataSource("R_AttestationControleAvecSoldeParAcquitDetailPeseesDataSet", attDetailPeseesTable.GetData(pee_id: peeID));
                AddSubReportDataSource("R_AttestationControleProprietaireDataSet", proTable.GetDataByPEEID(PEEID: peeID));
                //GetDataByEnteteID(PEE_ID: peeID));
                //AddDataSource("R_AttestationControleDetailDataSet", attDetailTable.GetDataByEnteteID(PEE_ID: peeID));
            }
        }
    }

    //public class R_AttestationControleDetailReportV2 : ReportInfo
    //{
    //    public R_AttestationControleDetailReportV2(FormatReport format, int peeID, float taux, int proID, int annID)
    //        : base(format, "R_AttestationControleDetail")
    //    {
    //        DAO.DSReportsTableAdapters.R_AttestationControleDetailTableAdapter attDetailTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleDetailTableAdapter>();

    //        attDetailTable.FillByAnneeAndProprietaire(dataTable: DsReports.R_AttestationControleDetail, taux: taux, proid: proID, annid: annID);

    //        AddDataSource("R_AttestationControleDetailDataSet", attDetailTable.GetDataByAnneeAndProprietaire(taux: taux, proid: proID, annid: annID));
    //    }
    //}
}