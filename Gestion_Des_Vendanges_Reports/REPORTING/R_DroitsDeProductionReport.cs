﻿namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    using DAO.DSReportsTableAdapters;
    using GSN.GDV.Reports;

    public class R_DroitsDeProductionReport : ReportInfo
    {
        public R_DroitsDeProductionReport(FormatReport format, int annID)
            : base(format, "R_DroitsDeProduction")
        {
            R_DroitsDeProductionTableAdapter _droitsDeProductiontable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_DroitsDeProductionTableAdapter>();
            R_EncaveurTableAdapter _encaveurTable = Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<R_EncaveurTableAdapter>();
            _droitsDeProductiontable.FillByAnnId(DsReports.R_DroitsDeProduction, annID);
            _encaveurTable.FillByAn(DsReports.R_Encaveur, annID);
            AddDataSource("R_DroitsDeProductionDataSet", _droitsDeProductiontable.GetDataByAnnId(annID));
            AddDataSource("R_EncaveurDataSet", _encaveurTable.GetDataByAn(annID));
        }
    }
}