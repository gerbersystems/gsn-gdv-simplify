﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeVendangesLivreesParProducteurReport : ReportInfo
    {
        public R_ListeVendangesLivreesParProducteurReport(FormatReport format, int annID)
            : base(format, "R_ListeVendangesLivreesParProducteur")
        {
            DAO.DSReportsTableAdapters.R_VendangesLivreesParProducteurTableAdapter table = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_VendangesLivreesParProducteurTableAdapter>();
            table.FillByANNID(DsReports.R_VendangesLivreesParProducteur, ANN_ID: annID);
            AddDataSource("R_VendangesLivreesParProducteurDataSet", table.GetDataByANNID(ANN_ID: annID));
        }
    }
}