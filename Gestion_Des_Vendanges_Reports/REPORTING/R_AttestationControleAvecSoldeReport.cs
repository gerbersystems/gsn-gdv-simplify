﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_AttestationControleAvecSoldeReport : ReportInfo
    {
        public R_AttestationControleAvecSoldeReport(FormatReport format, int annID, int proID = 0, int peeID = 0)
            : base(format, "R_AttestationControleAvecSolde")
        {
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeTableAdapter attTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeTableAdapter>();
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailTableAdapter attDetailTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailTableAdapter>();
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailPeseesTableAdapter attDetailPeseesTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailPeseesTableAdapter>();
            //DAO.DSReportsTableAdapters.R_ProprietairePeseeTableAdapter proTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ProprietairePeseeTableAdapter>();

            if (peeID == 0)
            {
                if (proID == 0)
                {
                    attTable.FillByAnnee(DsReports.R_AttestationControleAvecSolde, annID);
                    attDetailTable.FillByAnnee(dataTable: DsReports.R_AttestationControleAvecSoldeDetail, annid: annID);
                    attDetailPeseesTable.FillByAnnee(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, annid: annID);
                    //proTable.Fill(dataTable: DsReports.R_ProprietairePesee);

                    AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByAnnee(annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailDataSet", attDetailTable.GetDataByAnnee(annid: annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnnee(annID));
                    //AddSubReportDataSource("R_AttestationControleProprietaireDataSet", proTable.GetData());
                }
                else
                {
                    attTable.FillByAnneeAndProducteur(DsReports.R_AttestationControleAvecSolde, annID, proID);
                    attDetailTable.FillByAnneeAndProprietaire(dataTable: DsReports.R_AttestationControleAvecSoldeDetail, proid: proID, annid: annID);
                    attDetailPeseesTable.FillByAnneeAndProducteur(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, annid: annID, proid: proID);
                    //proTable.Fill(dataTable: DsReports.R_ProprietairePesee);

                    AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByAnneeAndProducteur(annID, proID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailDataSet", attDetailTable.GetDataByAnneeAndProprietaire(proid: proID, annid: annID));
                    AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByAnneeAndProducteur(annID, proID));
                    //AddSubReportDataSource("R_AttestationControleProprietaireDataSet", proTable.GetData());
                }
            }
            else
            {
                //attTable.FillByPEEID(DsReports.R_AttestationControle, PEE_ID: peeID);
                attTable.FillByPEEID(DsReports.R_AttestationControleAvecSolde, PEE_ID: peeID);
                attDetailTable.FillByAnneeAndProprietaire(dataTable: DsReports.R_AttestationControleAvecSoldeDetail, proid: proID, annid: annID);
                attDetailPeseesTable.FillByPEEID(dataTable: DsReports.R_AttestationControleAvecSoldeDetailPesees, peeid: peeID);
                //proTable.FillByPEEID(dataTable: DsReports.R_ProprietairePesee, PEEID: peeID);

                //AddDataSource("R_AttestationControleDataSet", attTable.GetDataByPEEID(PEE_ID: peeID));
                AddDataSource("R_AttestationControleAvecSoldeDataSet", attTable.GetDataByPEEID(PEE_ID: peeID));
                AddSubReportDataSource("R_AttestationControleAvecSoldeDetailDataSet", attDetailTable.GetDataByAnneeAndProprietaire( proid: proID, annid: annID));
                AddSubReportDataSource("R_AttestationControleAvecSoldeDetailPeseesDataSet", attDetailPeseesTable.GetDataByPEEID(peeid: peeID));
                //AddSubReportDataSource("R_AttestationControleProprietaireDataSet", proTable.GetDataByPEEID(PEEID: peeID));
                //GetDataByEnteteID(PEE_ID: peeID));
                //AddDataSource("R_AttestationControleDetailDataSet", attDetailTable.GetDataByEnteteID(PEE_ID: peeID));
            }
        }
    }

    public class R_AttestationControleDetailReport : ReportInfo
    {
        public R_AttestationControleDetailReport(FormatReport format, int peeID, float taux, int proID, int annID)
            : base(format, "R_AttestationControleAvecSoldeDetail")
        {
            DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailTableAdapter attDetailTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_AttestationControleAvecSoldeDetailTableAdapter>();

            attDetailTable.FillByAnneeAndProprietaire(dataTable: DsReports.R_AttestationControleAvecSoldeDetail, proid: proID, annid: annID);

            AddDataSource("R_AttestationControleAvecSoldeDetailDataSet", attDetailTable.GetDataByAnneeAndProprietaire(proid: proID, annid: annID));
        }
    }
}