﻿using Gestion_Des_Vendanges_Reports.Helpers;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ApportsReport : ReportInfo
    {
        public R_ApportsReport(FormatReport format, int annId, int? proId = null)
            : base(format, @"R_Apports")
        {
            var apports = ReportConnectionHelper.GetConnectionManager()
                                    .CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ApportsTableAdapter>();

            apports.Fill(DsReports.R_Apports, proId, annId);
            AddDataSource("R_ApportsDataSet", DsReports.R_Apports);
        }
    }
}