﻿using GSN.GDV.Reports;
using System;
using System.Data;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_PaiementDetailBonusReport : ReportInfo
    {
        public R_PaiementDetailBonusReport(FormatReport format, int annId = 0, int paiId = 0, int proId = 0, DateTime? dateRangeFrom = null, DateTime? dateRangeTo = null)
            : base(format, "R_PaiementDetailBonus")
        {
            DAO.DSReportsTableAdapters.R_EncaveurTableAdapter encTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_EncaveurTableAdapter>();
            DAO.DSReportsTableAdapters.R_PaiementTableAdapter paiTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_PaiementTableAdapter>();

            if (paiId > 0)
            {
                paiTable.FillByPai(DsReports.R_Paiement, paiId);
                AddDataSource("R_PaiementDetailBonus", (DataTable)paiTable.GetDataByPai(paiId));
                AddSubReportDataSource("R_PaiementDetailBonusSubReport", (DataTable)paiTable.GetDataByPai(PAI_ID: paiId));
            }
            else
            {
                if (annId <= 0)
                {
                    System.Windows.Forms.MessageBox.Show("Veuillez sélectionner une année");
                }
                else
                {
                    if (proId > 0)
                    {
                        if (dateRangeFrom == null || dateRangeTo == null)
                        {
                            paiTable.FillByAnnPro(dataTable: DsReports.R_Paiement, ANN_ID: annId, PRO_ID: proId);
                            AddDataSource("R_PaiementDetailBonus", (DataTable)paiTable.GetDataByAnnPro(ANN_ID: annId, PRO_ID: proId));
                            AddSubReportDataSource("R_PaiementDetailBonusSubReport", (DataTable)paiTable.GetDataByAnnPro(ANN_ID: annId, PRO_ID: proId));
                        }
                        else
                        {
                            paiTable.FillByAnnProDateRange(dataTable: DsReports.R_Paiement, ANN_ID: annId, PRO_ID: proId, DATE_FROM: dateRangeFrom.GetValueOrDefault(), DATE_TO: dateRangeTo.GetValueOrDefault());
                            AddDataSource("R_PaiementDetailBonus", (DataTable)paiTable.GetDataByAnnProDateRange(ANN_ID: annId, PRO_ID: proId, DATE_FROM: dateRangeFrom.GetValueOrDefault(), DATE_TO: dateRangeTo.GetValueOrDefault()));
                            AddSubReportDataSource("R_PaiementDetailBonusSubReport", (DataTable)paiTable.GetDataByAnnProDateRange(ANN_ID: annId, PRO_ID: proId, DATE_FROM: dateRangeFrom.GetValueOrDefault(), DATE_TO: dateRangeTo.GetValueOrDefault()));
                        }
                    }
                    else
                    {
                        if (dateRangeFrom == null || dateRangeTo == null)
                        {
                            paiTable.FillByAn(DsReports.R_Paiement, annId);
                            AddDataSource("R_PaiementDetailBonus", (DataTable)paiTable.GetDataByAn(annId));
                            AddSubReportDataSource("R_PaiementDetailBonusSubReport", (DataTable)paiTable.GetDataByAn(annId));
                        }
                        else
                        {
                            paiTable.FillByAnDateRange(DsReports.R_Paiement, annId, dateRangeFrom.GetValueOrDefault(), dateRangeTo.GetValueOrDefault());
                            AddDataSource("R_PaiementDetailBonus", (DataTable)paiTable.GetDataByAnDateRange(annId, dateRangeFrom.GetValueOrDefault(), dateRangeTo.GetValueOrDefault()));
                            AddSubReportDataSource("R_PaiementDetailBonusSubReport", (DataTable)paiTable.GetDataByAnDateRange(annId, dateRangeFrom.GetValueOrDefault(), dateRangeTo.GetValueOrDefault()));
                        }
                    }
                }
            }
            encTable.FillByAn(DsReports.R_Encaveur, annId);
            AddDataSource("R_Encaveur", (DataTable)encTable.GetDataByAn(annId));
        }
    }
}