﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeVendangesLivreesParDateEnLitresReport : ReportInfo
    {
        public R_ListeVendangesLivreesParDateEnLitresReport(FormatReport format, int aNNID)
            : base(format: format, nameReportFileWithoutExtension: "R_ListeVendangesLivreesParDateEnLitres")
        {
            DAO.DSReportsTableAdapters.R_VendangesLivreesParLieuDeProductionTableAdapter listeTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_VendangesLivreesParLieuDeProductionTableAdapter>();

            listeTable.FillByAnnId(DsReports.R_VendangesLivreesParLieuDeProduction, aNNID);
            AddDataSource("ListeVendangesLivreesParDateEnLitresDataset", listeTable.GetDataByAnnId(aNNID));
        }
    }
}