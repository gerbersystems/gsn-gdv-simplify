﻿using Gestion_Des_Vendanges_Reports.DAO.DSReportsTableAdapters;
using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeParcellesVisiteVignesReport : ReportInfo
    {
        public R_ListeParcellesVisiteVignesReport(FormatReport format, int aNNID)
            : base(format, "R_ListeParcellesVisiteVigne")
        {
            R_ListeParcellesVisiteVignesTableAdapter listeTable = Helpers.ReportConnectionHelper
                                                                    .GetConnectionManager()
                                                                    .CreateGvTableAdapter<R_ListeParcellesVisiteVignesTableAdapter>();

            listeTable.FillByAnnee(DsReports.R_ListeParcellesVisiteVignes, aNNID);
            AddDataSource("R_ListeParcellesVisiteVignesDataSet", listeTable.GetDataByAnnee(aNNID));
        }
    }
}