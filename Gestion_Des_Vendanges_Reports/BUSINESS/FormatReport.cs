﻿namespace GSN.GDV.Reports
{
    /* *
    *  Description: Enum des formats des reports
    *  Date de création:
    *  Modifications:
    * */

    public enum FormatReport
    {
        PDF, Excel, Word, Apercu
    }
}