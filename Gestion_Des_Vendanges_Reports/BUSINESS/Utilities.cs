﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Gestion_Des_Vendanges_Reports.BUSINESS
{
    public static class Reports_Utilities
    {
        public static int getProducteurID(int pee_id)
        {
            //DAO.DSReportsTableAdapters.PRODUCTEURTableAdapter producteur = new DAO.DSReportsTableAdapters.PRODUCTEURTableAdapter();
            int producteurID = 0;
            try
            {
                DAO.DSReportsTableAdapters.PRODUCTEURTableAdapter producteur = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.PRODUCTEURTableAdapter>();
                //producteurID = (int)producteur.ScalarQuery(pee_id);
                producteurID = producteur.GetProducteurIDByPEE_ID(pee_id).FirstOrDefault().PRO_ID_VENDANGE;
            }
            catch (Exception)
            {
                MessageBox.Show("Erreur avec l\'ID Producteur");
            }
            return producteurID;
        }
    }
}