﻿using GSN.GDV.Reports;

namespace Gestion_Des_Vendanges_Reports.REPORTING
{
    public class R_ListeAcquitsVendangeCepageReport : ReportInfo
    {
        public R_ListeAcquitsVendangeCepageReport(FormatReport format, int annID, float tauxConversionKgLitres)
            : base(format, "R_ListeAcquitsVendangeCepage")
        //FormatReport format, int annId, string taux) : base(format, "R_ListeAcquitsVendangeCepage")
        {
            DAO.DSReportsTableAdapters.R_ListeAcquitsVendangeCepageTableAdapter listeTable = Gestion_Des_Vendanges_Reports.Helpers.ReportConnectionHelper.GetConnectionManager().CreateGvTableAdapter<DAO.DSReportsTableAdapters.R_ListeAcquitsVendangeCepageTableAdapter>();
            listeTable.FillByAnneeID(DsReports.R_ListeAcquitsVendangeCepage, TAUX: tauxConversionKgLitres, annid: annID);
            AddDataSource("R_ListeAcquitsVendangeCepageDataSet", listeTable.GetDataByAnneeID(TAUX: tauxConversionKgLitres, annid: annID));
        }
    }
}