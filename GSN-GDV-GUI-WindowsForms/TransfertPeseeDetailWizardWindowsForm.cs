using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.GUI.Models;
using GSN.GDV.GUI.WPF;
using System;
using System.Windows;
using System.Windows.Forms;

namespace GSN.GDV.GUI.WindowsForms
{
    public partial class TransfertPeseeDetailWizardWindowsForm : Form, ITransfertPeseeDetailWizardWindowsForm
    {
        public TransfertPeseeDetailWizardWindowsForm(ITransfertPeseeDetailWizardViewModel viewModel = null)
        {
            InitializeComponent();
            ///<Note> this code need be placed on InitializeComponent() but actually when you are in design mode the system crack</Note>
            if (this.userControl == null)
                this.userControl = new GSN.GDV.GUI.WPF.TransfertPeseeDetailWizardUserControl();
            this.elementHost.Child = this.userControl;

            UserControl.ViewModel = viewModel;
            this.Load += (object sender, EventArgs e) =>
            {
                StyleBuilderExtension.ApplyStyle(this);
                if (UserControl.Controller == null)
                    UserControl.Controller = Controller;
            };
            UserControl.Cancelled += (object sender, RoutedEventArgs e) =>
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            };
            UserControl.Finished += (object sender, RoutedEventArgs e) =>
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            };
        }

        public virtual ITransfertPeseeDetailWizardUserControl UserControl { get { return userControl; } }
        public virtual ITransfertPeseeDetailWizardViewModel ViewModel { get { return UserControl == null ? null : UserControl.ViewModel; } }
        public virtual ITransfertPeseeDetailWizardController Controller { get; set; }
    }

    public interface ITransfertPeseeDetailWizardWindowsForm
    {
        ITransfertPeseeDetailWizardUserControl UserControl { get; }
        ITransfertPeseeDetailWizardViewModel ViewModel { get; }
        ITransfertPeseeDetailWizardController Controller { get; set; }
    }
}