﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.GUI.Models;
using GSN.GDV.GUI.WPF;
using System;
using System.Windows.Forms;

namespace GSN.GDV.GUI.WindowsForms
{
    public partial class SelectionAcquitWindowsForm : Form
    {
        public SelectionAcquitWindowsForm(ISelectionAcquitViewModel viewModel = null)
        {
            InitializeComponent();
            if (this.selectionAcquitUserControl == null)
                this.selectionAcquitUserControl = new GSN.GDV.GUI.WPF.SelectionAcquitUserControl();

            UserControl.ViewModel = viewModel;
            Load += (object sender, EventArgs e) =>
            {
                StyleBuilderExtension.ApplyStyle(this);
                if (UserControl.Controller == null)
                    UserControl.Controller = Controller;
            };
        }

        public virtual ISelectionAcquitUserControl UserControl { get { return this.selectionAcquitElementHost.Child as ISelectionAcquitUserControl; } }
        public virtual ISelectionAcquitViewModel ViewModel { get { return UserControl == null ? null : UserControl.ViewModel; } }
        public virtual ISelectionAcquitController Controller { get; set; }
    }

    public interface ISelectionAcquitWindowsForm
    {
        ISelectionAcquitUserControl UserControl { get; }
        ISelectionAcquitViewModel ViewModel { get; }
        ISelectionAcquitController Controller { get; set; }
    }
}