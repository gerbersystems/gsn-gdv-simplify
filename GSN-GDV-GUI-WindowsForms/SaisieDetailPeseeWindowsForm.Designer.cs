﻿namespace GSN.GDV.GUI.WindowsForms
{
	partial class SaisieDetailPeseeWindowsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.saisieDetailPeseeElementHost = new System.Windows.Forms.Integration.ElementHost();
			this.saisieDetailPeseeUserControl = new GSN.GDV.GUI.WPF.SaisieDetailPeseeUserControl();
			this.SuspendLayout();
			// 
			// saisieDetailPeseeElementHost
			// 
			this.saisieDetailPeseeElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
			this.saisieDetailPeseeElementHost.Location = new System.Drawing.Point(0, 0);
			this.saisieDetailPeseeElementHost.Margin = new System.Windows.Forms.Padding(10);
			this.saisieDetailPeseeElementHost.Name = "saisieDetailPeseeElementHost";
			this.saisieDetailPeseeElementHost.Size = new System.Drawing.Size(634, 211);
			this.saisieDetailPeseeElementHost.TabIndex = 0;
			this.saisieDetailPeseeElementHost.Child = this.saisieDetailPeseeUserControl;
			// 
			// SaisieDetailPeseeWindowsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 211);
			this.Controls.Add(this.saisieDetailPeseeElementHost);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "SaisieDetailPeseeWindowsForm";
			this.Text = "SaisieDetailPeseeWindowsForm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Integration.ElementHost saisieDetailPeseeElementHost;
		private WPF.SaisieDetailPeseeUserControl saisieDetailPeseeUserControl;
	}
}