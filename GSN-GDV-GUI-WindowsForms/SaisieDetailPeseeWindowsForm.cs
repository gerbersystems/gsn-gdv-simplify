﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.GUI.Models;
using GSN.GDV.GUI.WPF;
using System;
using System.Windows.Forms;

namespace GSN.GDV.GUI.WindowsForms
{
    public partial class SaisieDetailPeseeWindowsForm : Form, ISaisieDetailPeseeWindowsForm
    {
        public SaisieDetailPeseeWindowsForm(GSN.GDV.GUI.Models.ISaisieDetailPeseeViewModel viewModel = null)
        {
            InitializeComponent();
            if (this.saisieDetailPeseeUserControl == null)
                this.saisieDetailPeseeUserControl = new GSN.GDV.GUI.WPF.SaisieDetailPeseeUserControl();

            UserControl.ViewModel = viewModel;
            //this.UserControl.Commited += (object sender, EventArgs e) =>{ this.Close();};
            //this.UserControl.Cancelled += (object sender, EventArgs e) =>{ this.Close();};
            Load += (object sender, EventArgs e) =>
            {
                StyleBuilderExtension.ApplyStyle(this);
                if (UserControl.Controller == null)
                    UserControl.Controller = Controller;
            };
        }

        public virtual ISaisieDetailPeseeUserControl UserControl { get { return this.saisieDetailPeseeElementHost.Child as ISaisieDetailPeseeUserControl; } }
        public virtual ISaisieDetailPeseeViewModel ViewModel { get { return UserControl == null ? null : UserControl.ViewModel; } }
        public virtual ISaisieDetailPeseeController Controller { get; set; }
    }

    public interface ISaisieDetailPeseeWindowsForm
    {
        ISaisieDetailPeseeUserControl UserControl { get; }
        ISaisieDetailPeseeViewModel ViewModel { get; }
        ISaisieDetailPeseeController Controller { get; set; }
    }
}