﻿namespace GSN.GDV.GUI.WindowsForms
{
	partial class SelectionAcquitWindowsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.selectionAcquitElementHost = new System.Windows.Forms.Integration.ElementHost();
			this.selectionAcquitUserControl = new GSN.GDV.GUI.WPF.SelectionAcquitUserControl();
			this.SuspendLayout();
			// 
			// selectionAcquitElementHost
			// 
			this.selectionAcquitElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
			this.selectionAcquitElementHost.Location = new System.Drawing.Point(0, 0);
			this.selectionAcquitElementHost.Name = "selectionAcquitElementHost";
			this.selectionAcquitElementHost.Size = new System.Drawing.Size(548, 171);
			this.selectionAcquitElementHost.TabIndex = 0;
			this.selectionAcquitElementHost.Child = this.selectionAcquitUserControl;
			// 
			// SelectionAcquitWindowsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(548, 171);
			this.Controls.Add(this.selectionAcquitElementHost);
			this.MinimumSize = new System.Drawing.Size(564, 210);
			this.Name = "SelectionAcquitWindowsForm";
			this.Text = "SelectionAcquitWindowsForm";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Integration.ElementHost selectionAcquitElementHost;
		private WPF.SelectionAcquitUserControl selectionAcquitUserControl;
	}
}