﻿using GSN.GDV.GUI.Controllers;
using GSN.GDV.GUI.Extensions;
using GSN.GDV.GUI.Models;
using GSN.GDV.GUI.WPF;
using System;
using System.Windows.Forms;

namespace GSN.GDV.GUI.WindowsForms
{
    public partial class SaisieEntetePeseeWindowsForm : Form, ISaisieEntetePeseeWindowsForm
    {
        public SaisieEntetePeseeWindowsForm(ISaisieEntetePeseeViewModel viewModel = null)
        {
            InitializeComponent();
            if (this.saisieEntetePeseeUserControl1 == null)
                this.saisieEntetePeseeUserControl1 = new GSN.GDV.GUI.WPF.SaisieEntetePeseeUserControl();

            UserControl.ViewModel = viewModel;
            Load += (object sender, EventArgs e) =>
            {
                StyleBuilderExtension.ApplyStyle(this);
                if (UserControl.Controller == null)
                    UserControl.Controller = Controller;
            };
        }

        public virtual ISaisieEntetePeseeUserControl UserControl { get { return this.saisieEntetePeseeElementHost.Child as SaisieEntetePeseeUserControl; } }
        public virtual ISaisieEntetePeseeViewModel ViewModel { get { return UserControl == null ? null : UserControl.ViewModel; } }

        public virtual ISaisieEntetePeseeController Controller { get; set; }
    }

    public interface ISaisieEntetePeseeWindowsForm
    {
        ISaisieEntetePeseeUserControl UserControl { get; }
        ISaisieEntetePeseeViewModel ViewModel { get; }
        ISaisieEntetePeseeController Controller { get; set; }
    }
}