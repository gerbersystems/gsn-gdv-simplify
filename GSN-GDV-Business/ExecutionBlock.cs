﻿using System;

namespace GSN.GDV.Commons
{
    public class ExecutionBlock : IDisposable
    {
        readonly private System.Action executeAfter;

        public ExecutionBlock(System.Action executeNow, Action executeAfter)
        {
            executeNow();
            this.executeAfter = executeAfter;
        }

        public virtual void Dispose()
        {
            executeAfter();
        }
    }
}