﻿using static GSN.GDV.Data.Extensions.AnneeExtension;
using static GSN.GDV.Data.Extensions.BonusExtension;
using static GSN.GDV.Data.Extensions.SettingExtension;

namespace GSN.GDV.Business
{
    public interface IGlobalParameterProvider
    {
        #region Properties

        /// <summary>
        /// Canton
        /// </summary>
        CantonEnum Canton { get; set; }

        /// <summary>
        /// Erp pour l'export des factures
        /// </summary>
        ErpEnum Erp { get; set; }

        /// <summary>
        /// Synchronisation des contacts avec l'Erp
        /// </summary>
        SyncContactsEnum SyncContacts { get; set; }

        /// <summary>
        /// Taux de conversion vin clair
        /// </summary>
        decimal TauxConversionBlanc { get; set; }

        decimal TauxConversionRouge { get; set; }

        /// <summary>
        /// Taux de conversion moût de raisin
        /// </summary>
        decimal? TauxConversionBlancMout { get; set; }

        decimal? TauxConversionRougeMout { get; set; }

        /// <summary>
        /// Accepte ou non les apports supplémentaires, dépassant le quota permit
        /// </summary>
        bool AcceptExceedingQuota { get; set; }

        /// <summary>
        /// Valeur de sondage affiché en Oechsle ou Brix
        /// </summary>
        bool ShowSondageOechsle { get; set; }

        bool ShowSondageBrix { get; set; }

        // TODO Est-ce utilisé ???
        BonusUniteEnum BonusUnite { get; set; }

        /// <summary>
        /// Type de facture a afficher
        /// </summary>
        FactureTypeEnum FactureType { get; set; }

        /// <summary>
        /// Type de bonus calculé en pourcent ou en franc
        /// </summary>
        BonusTypeEnum BonusType { get; set; }

        /// <summary>
        /// Degré valeur en oechsle ou brix
        /// </summary>
        DegreValeur DegreValeur { get; set; }

        /// <summary>
        /// Unité de l'application en kilo ou litres
        /// </summary>
        ApplicationUniteEnum ApplicationUnite { get; set; }

        ILimitationProductionController LimitationProductionController { get; }

        #endregion Properties

        #region Methods

        double ConvertToKilogramme(double v, int AcquitId);

        decimal GetTauxConversion(AnneeCouleurVinEnum couleur);

        decimal GetTauxConversion(AnneeCouleurVinEnum Couleur, int Annee);

        decimal GetTauxConversion(int AcquitId, int Annee);

        decimal GetTauxConversionByPeseeEntete(int PeseeEnteteId);

        decimal GetTauxConversionByPeseeEntete(int PeseeEnteteId, int Annee);

        bool CanSetCanton();

        #endregion Methods
    }
}