﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Extensions;
using System;

namespace GSN.GDV.Business
{
    public class LimitationProductionController : ILimitationProductionController
    {
        private readonly IMainDataContext _dataContext;
        private readonly IGlobalParameterProvider _globalParameterProvider;

        public LimitationProductionController(IMainDataContext dataContext, IGlobalParameterProvider globalParameterProvider)
        {
            this._dataContext = dataContext;
            this._globalParameterProvider = globalParameterProvider;
        }

        public double? GetLitreMaxByAcquit(int AcquitId)
        {
            return GSN.GDV.Data.Extensions.AcquitExtension.GetLitreMax(me: _dataContext, AcquitId: AcquitId);
        }

        private double? GetKilogrammeMaxByAcquit(int AcquitId)
        {
            /// <Summary>Can't be null!!!</Summary>
            var litreMax = GetLitreMaxByAcquit(AcquitId: AcquitId);
            if (litreMax == null)
                return 0;
            return _globalParameterProvider.ConvertToKilogramme(litreMax.Value, AcquitId: AcquitId);
        }

        public double? GetLitreMaxByAcquit(int AcquitId, int CepageId)
        {
            return GSN.GDV.Data.Extensions.ParcelleExtension.GetSumLitreByAcquit(me: _dataContext, AcquitId: AcquitId, CepageId: CepageId);
        }

        public double? GetLitreMaxByLieu(int LieuId, int AcquitId, int CepageId)
        {
            return GSN.GDV.Data.Extensions.ParcelleExtension.GetSumLitreByLieu(me: _dataContext, CepageId: CepageId, AcquitId: AcquitId, LieuId: LieuId);
        }

        public double? GetLitreMaxByAutreMention(int AutreMentionId, int AcquitId, int CepageId)
        {
            return GSN.GDV.Data.Extensions.ParcelleExtension.GetSumLitreByAutreMention(me: _dataContext, CepageId: CepageId, AcquitId: AcquitId, AutreMentionId: AutreMentionId);
        }

        public double? GetKilogrammeMaxByAutreMention(int AutreMentionId, int AcquitId, int CepageId)
        {
            /// <Summary>Can't be null!!!</Summary>
            var litreMax = GetLitreMaxByAutreMention(AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId);
            if (litreMax == null)
                return 0;
            return _globalParameterProvider.ConvertToKilogramme(litreMax.Value, AcquitId: AcquitId);
        }

        public double? GetKilogrammeMaxByLieu(int LieuId, int AcquitId, int CepageId)
        {
            /// <Summary>Can't be null!!!</Summary>
            var litreMax = GetLitreMaxByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId);
            if (litreMax == null)
                return 0;
            return _globalParameterProvider.ConvertToKilogramme(litreMax.Value, AcquitId: AcquitId);
        }

        public double? GetKilogrammeMaxByAcquit(int AcquitId, int CepageId)
        {
            /// <Summary>Can't be null!!!</Summary>
            var litreMax = GetLitreMaxByAcquit(AcquitId: AcquitId, CepageId: CepageId);
            if (litreMax == null)
                return 0;
            return _globalParameterProvider.ConvertToKilogramme(litreMax.Value, AcquitId: AcquitId);
        }

        public double? GetMaximumByAcquit(int AcquitId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType)
        {
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Kilo)
                return GetKilogrammeMaxByAcquit(AcquitId: AcquitId);
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre)
                return GetLitreMaxByAcquit(AcquitId: AcquitId);
            throw new NotImplementedException("UniteType : " + UniteType);
        }

        public double? GetMaximumByAcquit(int AcquitId, int CepageId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Kilo)
                return GetKilogrammeMaxByAcquit(AcquitId: AcquitId, CepageId: CepageId);
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre)
                return GetLitreMaxByAcquit(AcquitId: AcquitId, CepageId: CepageId);
            throw new NotImplementedException("UniteType : " + UniteType);
        }

        public double? GetMaximumByLieu(int LieuId, int AcquitId, int CepageId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Kilo)
                return GetKilogrammeMaxByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId);
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre)
                return GetLitreMaxByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId);
            throw new NotImplementedException("UniteType : " + UniteType);
        }

        public double? GetMaximumByAutreMention(int AutreMentionId, int AcquitId, int CepageId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Kilo)
                return GetKilogrammeMaxByAutreMention(AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId);
            if (UniteType == GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum.Litre)
                return GetLitreMaxByAutreMention(AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId);
            throw new NotImplementedException("UniteType : " + UniteType);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        public double? GetSoldeByAcquit(int AcquitId, int CepageId, int ExcludedPeseeDetailId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType)
        {
            var max = GetMaximumByAcquit(AcquitId: AcquitId, CepageId: CepageId, UniteType: UniteType);
            double? sum = GetSaisiByAcquit(AcquitId: AcquitId, CepageId: CepageId, PeseeDetailExcludedId: ExcludedPeseeDetailId, UniteType: UniteType);
            return CalculateSolde(maximum: max, saisie: sum);
        }

        public double? GetSoldeByAcquit(int AcquitId, int ExcludedPeseeDetailId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType)
        {
            var max = GetMaximumByAcquit(AcquitId: AcquitId, UniteType: UniteType);
            double? sum = GetSaisiByAcquit(AcquitId: AcquitId, ExcludedPeseeDetailId: ExcludedPeseeDetailId, UniteType: UniteType);
            return CalculateSolde(maximum: max, saisie: sum);
        }

        public double CalculateSolde(double? maximum, double? saisie)
        {
            return (maximum ?? 0) - (saisie ?? 0);
        }

        public double? GetSaisiByAcquit(int AcquitId, int CepageId, int PeseeDetailExcludedId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            switch (UniteType)
            {
                case SettingExtension.ApplicationUniteEnum.Litre:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetLitreSaisiByAcquit(me: _dataContext, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: PeseeDetailExcludedId);

                case SettingExtension.ApplicationUniteEnum.Kilo:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetKilogrammeSaisiByAcquit(me: _dataContext, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: PeseeDetailExcludedId);

                default:
                    throw new NotImplementedException("UniteType : " + UniteType);
            }
        }

        public double? GetSaisiByLieu(int LieuId, int AcquitId, int CepageId, int ExcludedId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            switch (UniteType)
            {
                case SettingExtension.ApplicationUniteEnum.Litre:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetLitreSaisiByLieu(me: _dataContext, LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);

                case SettingExtension.ApplicationUniteEnum.Kilo:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetKilogrammeSaisiByLieu(me: _dataContext, LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);

                default:
                    throw new NotImplementedException("UniteType : " + UniteType);
            }
        }

        public double? GetSaisiByAutreMention(int AutreMentionId, int AcquitId, int CepageId, int ExcludedId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            switch (UniteType)
            {
                case SettingExtension.ApplicationUniteEnum.Litre:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetLitreSaisiByAutreMention(me: _dataContext, AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);

                case SettingExtension.ApplicationUniteEnum.Kilo:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetKilogrammeSaisiByAutreMention(me: _dataContext, AutreMentionId: AutreMentionId, AcquitId: AcquitId, CepageId: CepageId, ExcludedId: ExcludedId);

                default:
                    throw new NotImplementedException("UniteType : " + UniteType);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        public double? GetSaisiByAcquit(int AcquitId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            switch (UniteType)
            {
                case SettingExtension.ApplicationUniteEnum.Litre:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetLitreSaisiByAcquit(me: _dataContext, AcquitId: AcquitId, ExcludedId: ExcludedPeseeDetailId);

                case SettingExtension.ApplicationUniteEnum.Kilo:
                    return GSN.GDV.Data.Extensions.PeseeDetailExtension.GetKilogrammeSaisiByAcquit(me: _dataContext, AcquitId: AcquitId, ExcludedId: ExcludedPeseeDetailId);

                default:
                    throw new NotImplementedException("UniteType : " + UniteType);
            }
        }

        //public double? GetParcelleSumLitre(int AcquitId)
        //{
        //	return ParcelleExtension.GetSumLitreByAcquit(me:_dataContext, AcquitId:AcquitId);
        //}
        //public double? GetParcelleSumKilogramme(int AcquitId)
        //{
        //	var sommeLitre = GetParcelleSumLitre(AcquitId: AcquitId);
        //	if(sommeLitre==null)
        //		return 0;
        //	return ConvertToKilogramme(sommeLitre.Value, AcquitId:AcquitId);
        //}
    }
}