﻿using GSN.GDV.Data.Extensions;

namespace GSN.GDV.Business
{
    public interface ILimitationProductionController
    {
        double CalculateSolde(double? maximum, double? saisie);

        //double? GetKilogrammeMaxByAcquit(int AcquitId);
        //double? GetKilogrammeMaxByAcquit(int AcquitId, int LieuId);
        //double? GetKilogrammeMaxByAcquit(int AcquitId, int LieuId, int CepageId);

        //double? GetKilogrammeMaxByAcquit(int AcquitId, int CepageId);
        //double? GetKilogrammeMaxByLieu(int LieuId, int CepageId);

        //double? GetLitreMaxByAcquit(int AcquitId);
        //double? GetLitreMaxByAcquit(int AcquitId, int LieuId);
        //double? GetLitreMaxByAcquit(int AcquitId, int LieuId, int CepageId);

        //double? GetLitreMaxByAcquit(int AcquitId, int CepageId);
        //double? GetLitreMaxByLieu(int LieuId, int CepageId);

        //double? GetMaximumByAcquit(int AcquitId);
        //double? GetMaximumByAcquit(int AcquitId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetMaximumByAcquit(int AcquitId, int LieuId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetMaximumByAcquit(int AcquitId, int LieuId, int CepageId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        double? GetMaximumByAcquit(int AcquitId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        double? GetMaximumByAcquit(int AcquitId, int CepageId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        double? GetMaximumByLieu(int LieuId, int AcquitId, int CepageId, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        double? GetMaximumByAutreMention(int AutreMentionId, int AcquitId, int CepageId, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        //double? GetParcelleSumKilogramme(int AcquitId);
        //double? GetParcelleSumLitre(int AcquitId);

        //double? GetSaisiByAcquit(int AcquitId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSaisiByAcquit(int AcquitId, int LieuId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSaisiByAcquit(int AcquitId, int LieuId, int CepageId, int PeseeDetailExcludedId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSaisiByLieu(int AcquitId, int CepageId, int PeseeDetailExcludedId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSaisiByLieu(int LieuId, int CepageId, int PeseeDetailExcludedId, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSoldeByAcquit(int AcquitId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);
        //double? GetSoldeByAcquit(int AcquitId, int LieuId, GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="CepageId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSoldeByAcquit(int AcquitId, int CepageId, int ExcludedPeseeDetailId, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSoldeByAcquit(int AcquitId, int ExcludedPeseeDetailId, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSaisiByAcquit(int AcquitId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="AcquitId"></param>
        /// <param name="CepageId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSaisiByAcquit(int AcquitId, int CepageId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="LieuId"></param>
        /// <param name="AcquitId"></param>
        /// <param name="CepageId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSaisiByLieu(int LieuId, int AcquitId, int CepageId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="AutreMentionId"></param>
        /// <param name="AcquitId"></param>
        /// <param name="CepageId"></param>
        /// <param name="ExcludedPeseeDetailId">-1 for include all</param>
        /// <param name="UniteType"></param>
        /// <returns></returns>
        double? GetSaisiByAutreMention(int AutreMentionId, int AcquitId, int CepageId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType);
    }
}