﻿namespace GSN.GDV.Business
{
    public interface IConnectionManager
    {
        void AddGvTableAdapter(object tableAdapter);

        void ChangeDBGv(string name);

        T CreateGvTableAdapter<T>() where T : new();

        GSN.GDV.Data.Contextes.MainDataContext CreateMainDataContext();

        string GVConnectionString { get; }
        string GvDBDescription { get; }
        string GvDBName { get; }

        void InitGvConnectionString(int licId);

        string MasterConnectionString { get; }
        string MasterDbName { get; }
    }
}