﻿using System;
using System.Collections.Generic;

namespace GSN.GDV.Utilities
{
    public class LambdaEqualityComparer<T> : IEqualityComparer<T>
    {
        public readonly Func<T, T, bool> EqualsFunction;

        public readonly Func<T, int> GetHashCodeFunction;

        public LambdaEqualityComparer(Func<T, T, bool> equalsFunction, Func<T, int> getHashCodeFunction)
        {
            EqualsFunction = equalsFunction;
            GetHashCodeFunction = getHashCodeFunction;
        }

        public bool Equals(T x, T y)
        {
            return EqualsFunction(x, y);
        }

        public int GetHashCode(T obj)
        {
            return GetHashCodeFunction(obj);
        }
    }
}