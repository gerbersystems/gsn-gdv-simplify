﻿using System;

namespace GSN.GDV.GUI.Models
{
    public interface ISelectorItemModel<V, D>
    {
        V ValueMember { get; set; }
        D DisplayMember { get; set; }
    }

    public interface ISelectorItemModel<T, V, D> : ISelectorItemModel<V, D>
    {
        T DataMember { get; set; }
    }

    public class SelectorItemModel<V, D> : ISelectorItemModel<V, D>
    {
        public virtual V ValueMember { get; set; }
        public virtual D DisplayMember { get; set; }

        public override string ToString()
        {
            return Convert.ToString(DisplayMember);
        }

        public static ISelectorItemModel<V, D> Create(V v)
        {
            return Create(v, default(D));
        }

        public static ISelectorItemModel<V, D> Create(V v, D d)
        {
            return new SelectorItemModel<V, D> { ValueMember = v, DisplayMember = d };
        }
    }

    public class SelectorItemModel<T, V, D> : SelectorItemModel<V, D>, ISelectorItemModel<T, V, D>
    {
        public virtual T DataMember { get; set; }

        public static ISelectorItemModel<T, V, D> Create(T t)
        {
            return Create(t, default(V));
        }

        public static ISelectorItemModel<T, V, D> Create(T t, V v)
        {
            return Create(t, v, default(D));
        }

        public static ISelectorItemModel<T, V, D> Create(T t, V v, D d)
        {
            return new SelectorItemModel<T, V, D> { DataMember = t, ValueMember = v, DisplayMember = d };
        }
    }
}