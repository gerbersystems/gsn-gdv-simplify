﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IResponsableListViewModel : ISelectorListModel<IResponsableListViewItemModel, IEnumerable<IResponsableListViewItemModel>>
    {
    }

    public interface IResponsableListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }

        string Nom { get; set; }
        string Prenom { get; set; }

        ISelectorItemModel<int, string> Address { get; set; }
    }

    public class ResponsableListViewModel : SelectorListModel<IResponsableListViewItemModel, IEnumerable<IResponsableListViewItemModel>>, IResponsableListViewModel
    {
        public class ItemModel : IResponsableListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Nom { get; set; }
            public virtual string Prenom { get; set; }

            public virtual ISelectorItemModel<int, string> Address { get; set; }

            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get { return string.Concat(Nom, ", ", Prenom); } set { } }
        }
    }
}