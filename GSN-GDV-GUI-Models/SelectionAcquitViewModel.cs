﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface ISelectionAcquitViewModel : IAcquitListViewModel//ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
    {
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ProducteurSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> CepageSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }
    }

    //: SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
    //public class SelectionAcquitViewModel : SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>, ISelectionAcquitViewModel
    public class SelectionAcquitViewModel : AcquitListViewModel, ISelectionAcquitViewModel
    {
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ProducteurSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> CepageSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }
    }
}