﻿namespace GSN.GDV.GUI.Models
{
    public interface ILimitationViewModel
    {
        //GSN.GDV.Data.Extensions.SettingExtension.ApplicationUniteEnum ApplicationUnite { get; set; }
        double Admis { get; set; }

        double Saisis { get; set; }
        double Solde { get; set; }
    }

    public class LimitationViewModel : ILimitationViewModel
    {
        public virtual double Admis { get; set; }
        public virtual double Saisis { get; set; }
        public virtual double Solde { get; set; }
    }
}