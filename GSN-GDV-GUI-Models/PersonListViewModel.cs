﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IPersonListViewModel : ISelectorListModel<IPersonListViewItemModel, IEnumerable<IPersonListViewItemModel>>
    {
    }

    public interface IPersonListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        string Titre { get; set; }
        string Nom { get; set; }
        string Prenom { get; set; }

        string Societe { get; set; }

        string NumeroTva { get; set; }

        bool IsProducteur { get; set; }
        bool IsProprietaire { get; set; }

        ISelectorItemModel<int, string> Address { get; set; }
        ISelectorItemModel<int, string> ModeTva { get; set; }
        ISelectorItemModel<int, string> AddressWinBiz { get; set; }
    }

    public class PersonListViewModel : SelectorListModel<IPersonListViewItemModel, IEnumerable<IPersonListViewItemModel>>, IPersonListViewModel
    {
        public class ItemModel : IPersonListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Titre { get; set; }
            public virtual string Nom { get; set; }
            public virtual string Prenom { get; set; }

            public virtual string Societe { get; set; }

            public virtual string NumeroTva { get; set; }

            public virtual bool IsProducteur { get; set; }
            public virtual bool IsProprietaire { get; set; }

            public virtual ISelectorItemModel<int, string> Address { get; set; }
            public virtual ISelectorItemModel<int, string> ModeTva { get; set; }
            public virtual ISelectorItemModel<int, string> AddressWinBiz { get; set; }

            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get { return string.Concat(Nom, ", ", Prenom); } set { } }
        }
    }
}