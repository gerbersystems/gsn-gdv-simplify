﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface ISaisieEntetePeseeViewModel : IPeseeEnteteListViewModel
    {
        ILimitationViewModel ParAcquis { get; set; }
        ILimitationViewModel ParLieu { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ResponsableSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuProductionSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }

        ISelectionAcquitViewModel AcquitSelector { get; set; }
        IPeseeDetailListViewModel PeseeDetailSelector { get; set; }
    }

    public class SaisieEntetePeseeViewModel : PeseeEnteteListViewModel, ISaisieEntetePeseeViewModel
    {
        public virtual ILimitationViewModel ParAcquis { get; set; }
        public virtual ILimitationViewModel ParLieu { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ResponsableSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuProductionSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }

        public virtual ISelectionAcquitViewModel AcquitSelector { get; set; }
        public virtual IPeseeDetailListViewModel PeseeDetailSelector { get; set; }
    }
}