﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public class TransfertPeseeDetailWizardViewModel : ITransfertPeseeDetailWizardViewModel
    {
        public virtual IPageWizardViewModel Page0 { get; set; }
        public virtual IPageWizardViewModel Page1 { get; set; }
        public virtual IPageWizardViewModel Page2 { get; set; }
        public virtual IPageWizardViewModel Page3 { get; set; }
        public bool IsSplit => Selected.QuantiteKilogrammesATransferer.HasValue;
        public string SplitLabel => $"Kilos (Max : {Selected.QuantiteMaxKilogrammesATransferer} Kg)";
        public virtual IPeseeEnteteListViewItemModel Selected { get; set; }
        public virtual IPeseeDetailListViewModel PeseeDetailList { get; set; }
        public virtual ISelectionAcquitViewModel AcquitSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ResponsableSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuProductionSelector { get; set; }
        public virtual ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }
        public virtual IPeseeEnteteListViewItemModel SourceItem { get; set; }
        public string Page0Header => $"Sélectionnez les pesées à { (IsSplit ? "splitter" : "transférer") }.";
        public string Page3Header => $"Cliquez sur le bouton enregistrer ou sur la flèche arrière pour modifier le { (IsSplit ? "split" : "transfert") } de pesée.";
    }

    public interface ITransfertPeseeDetailWizardViewModel
    {
        IPageWizardViewModel Page0 { get; set; }
        IPageWizardViewModel Page1 { get; set; }
        IPageWizardViewModel Page2 { get; set; }
        IPageWizardViewModel Page3 { get; set; }
        bool IsSplit { get; }
        string SplitLabel { get; }
        IPeseeEnteteListViewItemModel Selected { get; set; }
        IPeseeDetailListViewModel PeseeDetailList { get; set; }

        ISelectionAcquitViewModel AcquitSelector { get; set; }

        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> ResponsableSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> LieuProductionSelector { get; set; }
        ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> AutreMentionSelector { get; set; }

        IPeseeEnteteListViewItemModel SourceItem { get; set; }
        string Page0Header { get; }
        string Page3Header { get; }
    }
}