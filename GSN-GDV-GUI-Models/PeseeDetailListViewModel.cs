﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IPeseeDetailListViewModel : ISelectorListModel<IPeseeDetailListViewItemModel, IEnumerable<IPeseeDetailListViewItemModel>>
    {
    }

    public interface IPeseeDetailListViewItemModel : ISelectorItemModel<int, string>
    {
        bool IsSelected { get; set; }
        int Id { get; set; }
        ISelectorItemModel<int, string> Acquit { get; set; }
        ISelectorItemModel<int, string> Entete { get; set; }

        ISelectorItemModel<int, string> Article { get; set; }

        ISelectorItemModel<int, string> Cepage { get; set; }
        int? Lot { get; set; }
        double? ConversionLitresKilogrammes { get; set; }
        double? QuantiteKilogrammes { get; set; }
        double? QuantiteLitres { get; set; }
        double? SondageBricks { get; set; }
        double? SondageOE { get; set; }
    }

    public class PeseeDetailListViewModel : SelectorListModel<IPeseeDetailListViewItemModel, IEnumerable<IPeseeDetailListViewItemModel>>, IPeseeDetailListViewModel
    {
        public class ItemModel : IPeseeDetailListViewItemModel
        {
            public virtual bool IsSelected { get; set; }
            public virtual int Id { get; set; }
            public virtual ISelectorItemModel<int, string> Acquit { get; set; }
            public virtual ISelectorItemModel<int, string> Entete { get; set; }

            public virtual ISelectorItemModel<int, string> Article { get; set; }

            public virtual ISelectorItemModel<int, string> Cepage { get; set; }
            public virtual int? Lot { get; set; }
            public virtual double? ConversionLitresKilogrammes { get; set; }
            public virtual double? QuantiteKilogrammes { get; set; }
            public virtual double? QuantiteLitres { get; set; }
            public virtual double? SondageBricks { get; set; }
            public virtual double? SondageOE { get; set; }

            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get; set; }

            public override string ToString()
            {
                return string.Format(@"{0}{{
				Id					:{1},
				Acquit				:{2},
				Entete				:{3},
				Article				:{4},
				Cepage				:{5},
				Lot					:{6},
				QuantiteKilogrammes	:{7},
				QuantiteLitres		:{8},
				SondageBricks		:{9},
				SondageOE			:{10}
				}}", GetType().Name, Id, Acquit, Entete, Article, Cepage, Lot, QuantiteKilogrammes, QuantiteLitres, SondageBricks, SondageOE);
            }
        }
    }
}