﻿namespace GSN.GDV.GUI.Models
{
    public interface ISaisieDetailPeseeViewModel
    {
        IPeseeDetailListViewItemModel Selected { get; set; }
        ILimitationViewModel Limitation { get; set; }
        ISelectorItemModel<int, string> Producteur { get; set; }

        string UniteLabel { get; set; }

        bool IsUniteKilogrammes { get; set; }
        bool IsUniteLitres { get; set; }

        bool LockQuantiteKilogrammes { get; set; }
        bool LockQuantiteLitres { get; set; }
        bool LockConversionLitresKilogrammes { get; set; }
    }

    public class SaisieDetailPeseeViewModel : ISaisieDetailPeseeViewModel
    {
        public virtual IPeseeDetailListViewItemModel Selected { get; set; }
        public virtual ILimitationViewModel Limitation { get; set; }
        public virtual ISelectorItemModel<int, string> Producteur { get; set; }

        public virtual string UniteLabel { get; set; }

        public virtual bool IsUniteKilogrammes { get; set; }
        public virtual bool IsUniteLitres { get; set; }

        public virtual bool LockQuantiteKilogrammes { get; set; }
        public virtual bool LockQuantiteLitres { get; set; }
        public virtual bool LockConversionLitresKilogrammes { get; set; }
    }
}