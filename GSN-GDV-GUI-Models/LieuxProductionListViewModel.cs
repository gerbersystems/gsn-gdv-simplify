﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface ILieuxProductionListViewModel : ISelectorListModel<ILieuxProductionListViewItemModel, IEnumerable<ILieuxProductionListViewItemModel>>
    {
    }

    public interface ILieuxProductionListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        string Nom { get; set; }
        int? Numero { get; set; }
        ISelectorItemModel<int, string> Region { get; set; }
    }

    public class LieuxProductionListViewModel : SelectorListModel<ILieuxProductionListViewItemModel, IEnumerable<ILieuxProductionListViewItemModel>>, ILieuxProductionListViewModel
    {
        public class ItemModel : ILieuxProductionListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Nom { get; set; }
            public virtual int? Numero { get; set; }
            public virtual ISelectorItemModel<int, string> Region { get; set; }
            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get { return Nom; } set { Nom = value; } }
        }
    }
}