﻿namespace GSN.GDV.GUI.Models
{
    public abstract class AbstractPageWizardViewModel : IPageWizardViewModel
    {
        public virtual bool AllowNext { get; set; }
    }

    public class SimplePageWizardViewModel : AbstractPageWizardViewModel
    {
    }
}