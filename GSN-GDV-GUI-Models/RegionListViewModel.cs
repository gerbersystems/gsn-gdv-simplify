﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IRegionListViewModel : ISelectorListModel<IRegionListViewItemModel, IEnumerable<IRegionListViewItemModel>>
    {
    }

    public interface IRegionListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        string Nom { get; set; }
    }

    public class RegionListViewModel : SelectorListModel<IRegionListViewItemModel, IEnumerable<IRegionListViewItemModel>>, IRegionListViewModel
    {
        public class ItemModel : IRegionListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Nom { get; set; }
            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get { return Nom; } set { Nom = value; } }
        }
    }
}