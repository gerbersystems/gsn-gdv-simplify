﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface ISelectorListModel<T, L> : IEnumerable<T>
        where L : IEnumerable<T>
    {
        bool IsFilter { get; set; }
        T Selected { get; set; }
        L Items { get; set; }
    }

    public class SelectorListModel<T, L> : ISelectorListModel<T, L>
        where L : IEnumerable<T>
    {
        public virtual bool IsFilter { get; set; }
        public virtual T Selected { get; set; }
        public virtual L Items { get; set; }

        public IEnumerator<T> GetEnumerator()
        {
            return Items == null ? null : Items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Items == null ? null : Items.GetEnumerator();
        }
    }
}