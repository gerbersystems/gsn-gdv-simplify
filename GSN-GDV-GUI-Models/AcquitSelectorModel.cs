﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    internal class AcquitSelectorModel : SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
    {
        public virtual SelectorListModel<SelectorItemModel<int, string>, IEnumerable<SelectorItemModel<int, string>>> ProducteurSelector { get; set; }
        public virtual SelectorListModel<SelectorItemModel<int, string>, IEnumerable<SelectorItemModel<int, string>>> CepageSelector { get; set; }
        public virtual SelectorListModel<SelectorItemModel<int, string>, IEnumerable<SelectorItemModel<int, string>>> LieuxSelector { get; set; }
        public virtual SelectorListModel<SelectorItemModel<int, string>, IEnumerable<SelectorItemModel<int, string>>> AutreMentionSelector { get; set; }
    }
}