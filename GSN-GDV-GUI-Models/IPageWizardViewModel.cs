﻿namespace GSN.GDV.GUI.Models
{
    public interface IPageWizardViewModel
    {
        bool AllowNext { get; set; }
    }
}