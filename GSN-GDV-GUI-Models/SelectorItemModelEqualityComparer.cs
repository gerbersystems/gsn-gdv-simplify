﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public class SelectorItemModelEqualityComparer<T, V, D> : IEqualityComparer<T>, IComparer<T>
        where T : ISelectorItemModel<V, D>
    {
        public bool Equals(T x, T y)
        {
            if (x == null && y == null)
                return true;
            if (x != null && y == null)
                return false;
            if (x == null && y != null)
                return false;

            return Compare(x, y) == 0;
        }

        public int GetHashCode(T obj)
        {
            if (obj == null)
                return 0;
            var m = int.MaxValue / 2;
            var v = (obj.ValueMember == null ? 0 : obj.ValueMember.GetHashCode()) % m;
            var d = (obj.DisplayMember == null ? 0 : obj.DisplayMember.GetHashCode()) % m;
            return d + v;
        }

        public int Compare(T x, T y)
        {
            var v = Comparer<V>.Default.Compare(x.ValueMember, y.ValueMember);
            if (v != 0)
                return v;
            var d = Comparer<D>.Default.Compare(x.DisplayMember, y.DisplayMember);

            return d;
        }

        public static readonly SelectorItemModelEqualityComparer<T, V, D> Default = new SelectorItemModelEqualityComparer<T, V, D>();
    }
}