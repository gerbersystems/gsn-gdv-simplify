﻿using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IAutreMentionListViewModel : ISelectorListModel<IAutreMentionListViewItemModel, IEnumerable<IAutreMentionListViewItemModel>>
    {
    }

    public interface IAutreMentionListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        string Nom { get; set; }
    }

    public class AutreMentionListViewModel : SelectorListModel<IAutreMentionListViewItemModel, IEnumerable<IAutreMentionListViewItemModel>>, IAutreMentionListViewModel
    {
        public class ItemModel : IAutreMentionListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Nom { get; set; }
            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get { return Nom; } set { Nom = value; } }
        }
    }
}