﻿using System;
using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IAcquitListViewModel : ISelectorListModel<IAcquitListViewItemModel, IEnumerable<IAcquitListViewItemModel>>
    {
    }

    public interface IAcquitListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        int? Numero { get; set; }
        int? NumeroComplet { get; set; }
        DateTime? Date { get; set; }
        DateTime? Reception { get; set; }
        double? Surface { get; set; }
        double? Litres { get; set; }

        ISelectorItemModel<int, string> Classe { get; set; }
        ISelectorItemModel<int, string> Couleur { get; set; }
        ISelectorItemModel<int, string> Annee { get; set; }
        ISelectorItemModel<int, string> Comune { get; set; }
        ISelectorItemModel<int, string> Proprietaire { get; set; }
        ISelectorItemModel<int, string> Producteur { get; set; }
        ISelectorItemModel<int, string> Region { get; set; }

        ISelectorItemModel<int, string> Cepage { get; set; }
        ISelectorItemModel<int, string> Lieu { get; set; }
        ISelectorItemModel<int, string> AutreMention { get; set; }
    }

    public class AcquitListViewModel : SelectorListModel<IAcquitListViewItemModel, IEnumerable<IAcquitListViewItemModel>>, IAcquitListViewModel
    {
        public class ItemModel : IAcquitListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual int? Numero { get; set; }
            public virtual int? NumeroComplet { get; set; }
            public virtual DateTime? Date { get; set; }
            public virtual DateTime? Reception { get; set; }
            public virtual double? Surface { get; set; }
            public virtual double? Litres { get; set; }

            public virtual ISelectorItemModel<int, string> Classe { get; set; }
            public virtual ISelectorItemModel<int, string> Couleur { get; set; }
            public virtual ISelectorItemModel<int, string> Annee { get; set; }
            public virtual ISelectorItemModel<int, string> Comune { get; set; }
            public virtual ISelectorItemModel<int, string> Proprietaire { get; set; }
            public virtual ISelectorItemModel<int, string> Producteur { get; set; }
            public virtual ISelectorItemModel<int, string> Region { get; set; }

            public virtual ISelectorItemModel<int, string> Cepage { get; set; }
            public virtual ISelectorItemModel<int, string> Lieu { get; set; }
            public virtual ISelectorItemModel<int, string> AutreMention { get; set; }

            public int ValueMember { get { return Id; } set { Id = value; } }
            public string DisplayMember { get; set; }
        }
    }
}