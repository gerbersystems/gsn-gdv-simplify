﻿using System;
using System.Collections.Generic;

namespace GSN.GDV.GUI.Models
{
    public interface IPeseeEnteteListViewModel : ISelectorListModel<IPeseeEnteteListViewItemModel, IEnumerable<IPeseeEnteteListViewItemModel>>
    {
    }

    public interface IPeseeEnteteListViewItemModel : ISelectorItemModel<int, string>
    {
        int Id { get; set; }
        string Lieu { get; set; }
        string Remarques { get; set; }
        string Divers1 { get; set; }
        string Divers2 { get; set; }
        string Divers3 { get; set; }
        DateTime? Date { get; set; }
        bool? IsGrandCru { get; set; }
        double? QuantiteKilogrammesATransferer { get; set; }
        double? QuantiteMaxKilogrammesATransferer { get; set; }
        ISelectorItemModel<int, string> Responsable { get; set; }

        ISelectorItemModel<int, string> Acquit { get; set; }
        ISelectorItemModel<int, string> Cepage { get; set; }
        ISelectorItemModel<int, string> LieuProduction { get; set; }
        ISelectorItemModel<int, string> AutreMention { get; set; }
    }

    public class PeseeEnteteListViewModel : SelectorListModel<IPeseeEnteteListViewItemModel, IEnumerable<IPeseeEnteteListViewItemModel>>, IPeseeEnteteListViewModel
    {
        public class ItemModel : IPeseeEnteteListViewItemModel
        {
            public virtual int Id { get; set; }
            public virtual string Lieu { get; set; }
            public virtual string Remarques { get; set; }
            public virtual string Divers1 { get; set; }
            public virtual string Divers2 { get; set; }
            public virtual string Divers3 { get; set; }
            public virtual DateTime? Date { get; set; }
            public virtual bool? IsGrandCru { get; set; }
            public virtual double? QuantiteKilogrammesATransferer { get; set; }
            public virtual double? QuantiteMaxKilogrammesATransferer { get; set; }
            public virtual ISelectorItemModel<int, string> Responsable { get; set; }

            public virtual ISelectorItemModel<int, string> Acquit { get; set; }
            public virtual ISelectorItemModel<int, string> Cepage { get; set; }
            public virtual ISelectorItemModel<int, string> LieuProduction { get; set; }
            public virtual ISelectorItemModel<int, string> AutreMention { get; set; }

            public virtual int ValueMember { get { return Id; } set { Id = value; } }
            public virtual string DisplayMember { get; set; }
        }
    }
}