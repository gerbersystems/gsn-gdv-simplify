﻿using GSN.GDV.Business;
using GSN.GDV.Data.Extensions;
using GSN.GDV.GUI.Models;

namespace GSN.GDV.GUI.Controllers
{
    public class LimitationController
    {
        private IControllerHelper helper;

        public LimitationController(IControllerHelper helper)
        {
            this.helper = helper;
        }

        #region CreateViewModelBy...

        public virtual ILimitationViewModel CreateViewModelByLieu(int LieuId, int AcquitId, int CepageId, int ExcludedPeseeDetailId = -1)
        {
            var gpp = helper.GetGlobalParameterProvider();
            return CreateViewModelByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId, UniteType: gpp.ApplicationUnite);
        }

        public virtual ILimitationViewModel CreateViewModelByAcquit(int AcquitId, int CepageId, int ExcludedPeseeDetailId = -1)
        {
            var gpp = helper.GetGlobalParameterProvider();
            return CreateViewModelByAcquit(AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId, UniteType: gpp.ApplicationUnite);
        }

        public virtual ILimitationViewModel CreateViewModelByLieu(int LieuId, int AcquitId, int CepageId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            var gpp = helper.GetGlobalParameterProvider();
            var lpc = gpp.LimitationProductionController;
            return CreateViewModelByLieu(helper: lpc, UniteType: UniteType, LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId);
        }

        public virtual ILimitationViewModel CreateViewModelByAcquit(int AcquitId, int CepageId, int ExcludedPeseeDetailId, SettingExtension.ApplicationUniteEnum UniteType)
        {
            var gpp = helper.GetGlobalParameterProvider();
            var lpc = gpp.LimitationProductionController;
            return CreateViewModelByAcquit(helper: lpc, UniteType: UniteType, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId);
        }

        #endregion CreateViewModelBy...

        #region static methods

        public static ILimitationViewModel CreateViewModelByLieu(ILimitationProductionController helper, SettingExtension.ApplicationUniteEnum UniteType, int LieuId, int AcquitId, int CepageId, int ExcludedPeseeDetailId = -1)
        {
            var r = new LimitationViewModel();

            r.Admis = helper.GetMaximumByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, UniteType: UniteType) ?? 0;
            r.Saisis = helper.GetSaisiByLieu(LieuId: LieuId, AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId, UniteType: UniteType) ?? 0;
            r.Solde = helper.CalculateSolde(maximum: r.Admis, saisie: r.Saisis);
            return r;
        }

        public static ILimitationViewModel CreateViewModelByAcquit(ILimitationProductionController helper, Data.Extensions.SettingExtension.ApplicationUniteEnum UniteType, int AcquitId, int CepageId, int ExcludedPeseeDetailId = -1)
        {
            var r = new LimitationViewModel();

            r.Admis = helper.GetMaximumByAcquit(AcquitId: AcquitId, CepageId: CepageId, UniteType: UniteType) ?? 0;
            r.Saisis = helper.GetSaisiByAcquit(AcquitId: AcquitId, CepageId: CepageId, ExcludedPeseeDetailId: ExcludedPeseeDetailId, UniteType: UniteType) ?? 0;
            r.Solde = helper.CalculateSolde(maximum: r.Admis, saisie: r.Saisis);
            return r;
        }

        #endregion static methods
    }
}