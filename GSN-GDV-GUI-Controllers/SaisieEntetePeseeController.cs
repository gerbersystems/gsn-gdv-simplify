﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.GUI.Models;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SaisieEntetePeseeController : ISaisieEntetePeseeController
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SaisieEntetePeseeController));
        private readonly IControllerHelper helper;

        public SaisieEntetePeseeController(IControllerHelper helper)
        {
            this.helper = helper;
        }

        ISaisieEntetePeseeViewModel ISaisieEntetePeseeController.CreateEmptyViewModel()
        {
            return new SaisieEntetePeseeViewModel
            {
                AcquitSelector = new SelectionAcquitViewModel { },
                PeseeDetailSelector = new PeseeDetailListViewModel { },
            };
        }

        ISelectionAcquitController ISaisieEntetePeseeController.CreateSelectionAcquitController(ISaisieEntetePeseeViewModel viewModel)
        {
            return new SelectionAcquitController(viewModel.AcquitSelector);
        }

        public static ISaisieEntetePeseeViewModel CreateViewModel(IMainDataContext db, int? Id = null)
        {
            var responsableList = new List<SelectorItemModel<int, string>> { };
            //var lieuProductionList = new List<SelectorItemModel<int, string>> { };
            //var autreMentionList = new List<SelectorItemModel<int, string>> { };
            var model = new SaisieEntetePeseeViewModel
            {
                AcquitSelector = SelectionAcquitController.CreateViewModel(db),
                ResponsableSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = responsableList,
                },
                //LieuProductionSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                //{
                //    Items = lieuProductionList,
                //},
                //AutreMentionSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                //{
                //    Items = autreMentionList,
                //},
            };

            responsableList.AddRange((from t in db.ResponsableSet select new SelectorItemModel<int, string> { ValueMember = t.Id, DisplayMember = string.Concat(t.Nom, ", ", t.Prenom) }));
            //lieuProductionList.AddRange((from dr in db.LieuProductionSet from r in db.RegionSet where  r.Id==dr.RegionId select new SelectorItemModel<int, string> { ValueMember = dr.Id, DisplayMember = string.Concat(r.Nom, ", ", dr.Nom) }));
            //autreMentionList.AddRange((from dr in db.AutreMentionSet select new SelectorItemModel<int, string> { ValueMember = dr.Id, DisplayMember = dr.Nom }));
            model.LieuProductionSelector = model.AcquitSelector.LieuSelector;
            model.AutreMentionSelector = model.AcquitSelector.AutreMentionSelector;
            model.PeseeDetailSelector = SelectionPeseeDetailController.CreateListViewModel(db, q: Id == null ? null : (from t in db.PeseeDetailSet where t.EnteteId == Id select t).ToList().AsQueryable());
            if (Id == null)
                return model;
            var dr = (
                from t in db.PeseeEnteteSet
                where t.Id == Id
                select t
            ).FirstOrDefault();
            if (dr == null)
                return model;

            model.Selected = SelectionPeseeEnteteController.CreateListItemModel(db: db, Id: Id);
            if (model.Selected != null)
            {
                model.AutreMentionSelector.Selected = model.Selected.AutreMention;
                model.LieuProductionSelector.Selected = model.Selected.LieuProduction;
                model.ResponsableSelector.Selected = model.Selected.Responsable;
            }
            //model.LieuProductionSelector = SelectionLieuProductionController.CreateSelector(db, (from dr in db.LieuProductionSet select dr), SelectedId:dr.LieuDeProductionId);
            //model.ResponsableSelector = SelectionResponsableController.CreateSelector(db, (from dr in db.ResponsableSet select dr), SelectedId: dr.ResponsableId);
            //model.AutreMentionSelector = SelectionAutreMentionController.CreateSelector(db, (from dr in db.AutreMentionSet select dr), SelectedId: dr.AutreMentionId);
            return model;
        }

        public virtual ISaisieDetailPeseeController CreateSaisieDetailPeseeController(ISaisieEntetePeseeViewModel viewModel)
        {
            var r = new GSN.GDV.GUI.Controllers.SaisieDetailPeseeController(helper: helper);
            return r;
        }
    }
}