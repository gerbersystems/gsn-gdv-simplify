﻿using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionArticleController
    {
        public static ISelectorItemModel<int, string> CreateSelectorItemModel(Data.Contextes.IMainDataContext db, int? Id = null)
        {
            return CreateSelectorItemModel(db: db, dr: (from t in db.ArticleSet where t.Id == Id select t).FirstOrDefault());
        }

        public static ISelectorItemModel<int, string> CreateSelectorItemModel(Data.Contextes.IMainDataContext db, ArticleModel dr)
        {
            var r = new SelectorItemModel<int, string>
            {
                ValueMember = dr == null ? 0 : dr.Id,
                DisplayMember = dr == null ? string.Empty : dr.Designation.Courte
            };
            return r;
        }
    }
}