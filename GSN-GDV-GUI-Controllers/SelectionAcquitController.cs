﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionAcquitController : ISelectionAcquitController
    {
        public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SaisieEntetePeseeController));
        private ISelectionAcquitViewModel ViewModel;

        private IList<IAcquitListViewItemModel> items { get; set; }

        public SelectionAcquitController(ISelectionAcquitViewModel viewModel)
        {
            this.ViewModel = viewModel;

            Set(viewModel);
        }

        public virtual void Set(ISelectionAcquitViewModel viewModel)
        {
            this.ViewModel = viewModel;

            LoadItems(ViewModel.Items);

            var equalityComparer = new GSN.GDV.Utilities.LambdaEqualityComparer<ISelectorItemModel<int, string>>(
                (ISelectorItemModel<int, string> x, ISelectorItemModel<int, string> y) =>
                {
                    if (x == null && y == null)
                        return true;
                    if (x != null && y == null)
                        return false;
                    if (x == null && y != null)
                        return false;
                    return x.ValueMember == y.ValueMember;
                },
                (ISelectorItemModel<int, string> o) =>
                {
                    //return o == null ? 0 : o.ValueMember;
                    return 0;
                }
            );
            var producteurList = (from t in items where t.Producteur != null orderby t.Producteur.DisplayMember select t.Producteur).Distinct(equalityComparer).ToList();
            if (this.ViewModel.ProducteurSelector != null)
                this.ViewModel.ProducteurSelector.Items = producteurList;
            else
                this.ViewModel.ProducteurSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> { Items = producteurList };
        }

        public virtual void LoadItems(IEnumerable<IAcquitListViewItemModel> argv)
        {
            if (items == null)
                items = new System.Collections.ObjectModel.ObservableCollection<IAcquitListViewItemModel>(argv);
            else
            {
                items.Clear();
                foreach (var o in argv)
                    items.Add(o);
            }
        }

        public static bool HaveSelection<T, L>(ISelectorListModel<T, L> argv) where L : IEnumerable<T>
        {
            return argv != null && argv.IsFilter == true && argv.Selected != null;
        }

        public static ISelectionAcquitViewModel CreateViewModel(IMainDataContext db)
        {
            return CreateViewModel(db: db, items: CreateAcquitQueryable(db));
        }

        public static ISelectionAcquitViewModel CreateViewModel(IMainDataContext db, IEnumerable<IAcquitListViewItemModel> items)
        {
            var model = new SelectionAcquitViewModel
            {
                Items = items,
                ProducteurSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = (from t in items select t.Producteur).Distinct().ToList(),
                },
                CepageSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = (from t in items select t.Cepage).Distinct().ToList(),
                },
                LieuSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = (from t in items select t.Lieu).Distinct().ToList(),
                },
                AutreMentionSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = (from t in items select t.AutreMention).Distinct().ToList(),
                },
            };
            return model;
        }

        private static IEnumerable<IAcquitListViewItemModel> CreateAcquitQueryable(IMainDataContext db)
        {
            return db.ParcelleSet
                     .Join(db.AcquitSet, parcel => parcel.AcquitId, acquit => acquit.Id, (parcel, acquit) =>
                               new {Acquit = acquit, Parcel = parcel})
                     .Join(db.AnneeSet, acquitParcel => acquitParcel.Acquit.AnneeId, annee => annee.Id, (acquitParcel, annee) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, Annee = annee})
                     .Join(db.ProprietaireSet, acquitParcel => acquitParcel.Acquit.ProprietaireId, prop => prop.Id, (acquitParcel, prop) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, acquitParcel.Annee, Proprietaire = prop})
                     .Join(db.ProprietaireSet, acquitParcel => acquitParcel.Acquit.ProducteurId, vendProp => vendProp.Id, (acquitParcel, vendProp) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, acquitParcel.Annee, acquitParcel.Proprietaire, Producteur = vendProp})
                     .Join(db.CepageSet, acquitParcel => acquitParcel.Parcel.CepageId, cepage => cepage.Id, (acquitParcel, cepage) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, acquitParcel.Annee, acquitParcel.Proprietaire, acquitParcel.Producteur, Cepage = cepage})
                     .Join(db.LieuProductionSet, acquitParcel => acquitParcel.Parcel.LieuId, lieu => lieu.Id, (acquitParcel, lieu) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, acquitParcel.Annee, acquitParcel.Proprietaire, acquitParcel.Producteur, acquitParcel.Cepage, Lieu = lieu})
                     .Join(db.AutreMentionSet, acquitParcel => acquitParcel.Parcel.AutreMentionId, autreMention => autreMention.Id, (acquitParcel, autreMention) =>
                               new {acquitParcel.Acquit, acquitParcel.Parcel, acquitParcel.Annee, acquitParcel.Proprietaire, acquitParcel.Producteur, acquitParcel.Cepage, acquitParcel.Lieu, AutreMention = autreMention})
                     .Where(a => a.Acquit.AnneeId == TransfertPeseeDetailWizardController.YearId)
                     .GroupBy(a => new {a.Acquit, a.Lieu, a.AutreMention, a.Cepage, a.Proprietaire, a.Producteur, a.Annee})
                     .Select(a => new AcquitListViewModel.ItemModel
                                  {
                                      Id = a.Key.Acquit.Id,
                                      Numero = a.Key.Acquit.Numero,
                                      NumeroComplet = a.Key.Acquit.NumeroComplet,
                                      Litres = a.Sum(acq => acq.Parcel.Litres),
                                      Annee = new SelectorItemModel<int, string> {DisplayMember = a.Key.Annee.Annee.ToString(), ValueMember = a.Key.Annee.Id},
                                      Proprietaire = new SelectorItemModel<int, string> {DisplayMember = a.Key.Proprietaire.Nom + " " + a.Key.Proprietaire.Prenom, ValueMember = a.Key.Proprietaire.Id},
                                      Producteur = new SelectorItemModel<int, string> {DisplayMember = a.Key.Producteur.Nom + " " + a.Key.Producteur.Prenom, ValueMember = a.Key.Producteur.Id},
                                      Cepage = new SelectorItemModel<int, string> {DisplayMember = a.Key.Cepage.Nom, ValueMember = a.Key.Cepage.Id},
                                      Lieu = new SelectorItemModel<int, string> {DisplayMember = a.Key.Lieu.Nom, ValueMember = a.Key.Lieu.Id},
                                      AutreMention = new SelectorItemModel<int, string> {DisplayMember = a.Key.AutreMention.Nom, ValueMember = a.Key.AutreMention.Id},
                                      DisplayMember = "Producteur: " + a.Key.Producteur.Nom + ", " + a.Key.Producteur.Prenom + Environment.NewLine +
                                                      "Acquit: " + a.Key.Acquit.Numero + " - " + "Acquit partiel: " + a.Key.Acquit.NumeroComplet + Environment.NewLine +
                                                      "Cépage: " + a.Key.Cepage.Nom + " - Lieu: " + a.Key.Lieu.Nom + " - Nom local: " + a.Key.AutreMention.Nom
                                  }
                            )
                     .ToList();
        }

        public static ISelectorItemModel<int, string> CreateSelector(IMainDataContext db, int? Id = null)
        {
            return CreateSelector((from t in db.AcquitSet select t).FirstOrDefault());
        }

        public static ISelectorItemModel<int, string> CreateSelector(AcquitModel dr)
        {
            if (dr == null)
                return null;
            var r = new SelectorItemModel<int, string>();
            return BuildItemModel<ISelectorItemModel<int, string>, AcquitModel>(r, dr);
        }

        public static T BuildItemModel<T, M>(T r, AcquitModel dr)
            where T : ISelectorItemModel<int, string>
            where M : AcquitModel
        {
            if (dr == null)
                return r;
            r.ValueMember = dr.Id;
            r.DisplayMember = string.Concat(dr.Numero, "|", dr.Litres, "|", dr.Date, "|", dr.NumeroComplet, "|", dr.Reception, "|", dr.Surface);
            return r;
        }

        public static IAcquitListViewItemModel CreateListItemModel(IMainDataContext db, int? Id, bool deep = false)
        {
            return CreateListItemModel(db: db, dr: (from t in db.AcquitSet where t.Id == Id select t).FirstOrDefault(), deep: deep);
        }

        public static IAcquitListViewItemModel CreateListItemModel(IMainDataContext db, AcquitModel dr, bool deep = false)
        {
            var r = new AcquitListViewModel.ItemModel
            {
                Id = dr.Id,
                Numero = dr.Numero,
                NumeroComplet = dr.NumeroComplet,
                Litres = dr.Litres,
                Annee = new SelectorItemModel<int, string> { ValueMember = dr.AnneeId ?? 0 },
            };
            if (deep == false || dr.ProducteurId == null)
                r.Producteur = new SelectorItemModel<int, string> { ValueMember = dr.ProducteurId ?? 0 };
            else
                r.Producteur = SelectionPersonController.CreateViewItemModel(db: db, Id: dr.ProducteurId.Value);

            if (deep == false || dr.ProprietaireId == null)
                r.Proprietaire = new SelectorItemModel<int, string> { ValueMember = dr.ProprietaireId ?? 0 };
            else
                r.Proprietaire = SelectionPersonController.CreateViewItemModel(db: db, Id: dr.ProprietaireId.Value);

            return r;
        }

        public void ExecuteFilterBySelection(ISelectionAcquitViewModel model)
        {
            var q = (from t in items select t).Cast<IAcquitListViewItemModel>();

            ExecuteFilterBySelection(model: model, q: q);
        }

        public static void ExecuteFilterBySelection(ISelectionAcquitViewModel model, IEnumerable<IAcquitListViewItemModel> q)
        {
            var equalityComparer = new GSN.GDV.Utilities.LambdaEqualityComparer<ISelectorItemModel<int, string>>(
                (ISelectorItemModel<int, string> x, ISelectorItemModel<int, string> y) =>
                {
                    if (x == null && y == null)
                        return true;
                    if (x != null && y == null)
                        return false;
                    if (x == null && y != null)
                        return false;
                    return x.ValueMember == y.ValueMember;
                },
                (ISelectorItemModel<int, string> o) =>
                {
                    //return o == null ? 0 : o.ValueMember;
                    return 0;
                }
            );
            if (HaveSelection(model.ProducteurSelector))
            {
                q = q.Where(t => t.Producteur != null && t.Producteur.ValueMember == model.ProducteurSelector.Selected.ValueMember);
            }
            else if (model.ProducteurSelector.IsFilter == false)
                model.CepageSelector.IsFilter = false;

            model.CepageSelector.Items = (from t in q where t.Cepage != null orderby t.Cepage.DisplayMember select t.Cepage).Distinct(equalityComparer);

            if (HaveSelection(model.CepageSelector))
                q = q.Where(t => t.Cepage != null && t.Cepage.ValueMember == model.CepageSelector.Selected.ValueMember);
            else if (model.CepageSelector.IsFilter == false)
            {
                model.LieuSelector.IsFilter = false;
                model.AutreMentionSelector.IsFilter = false;
            }
            model.LieuSelector.Items = (from t in q where t.Lieu != null orderby t.Lieu.DisplayMember select t.Lieu).Distinct(equalityComparer);

            model.AutreMentionSelector.Items = (from t in q where t.AutreMention != null orderby t.AutreMention.DisplayMember select t.AutreMention).Distinct(equalityComparer);

            if (HaveSelection(model.LieuSelector))
                q = q.Where(t => t.Lieu != null && t.Lieu.ValueMember == model.LieuSelector.Selected.ValueMember);

            if (HaveSelection(model.AutreMentionSelector))
                q = q.Where(t => t.AutreMention != null && t.AutreMention.ValueMember == model.AutreMentionSelector.Selected.ValueMember);

            model.Items = q;
        }

        public void ExecuteFilterByProducteur(ISelectionAcquitViewModel model)
        {
            model.CepageSelector.Selected = null;

            ExecuteFilterByCepage(model);
        }

        public void ExecuteFilterByCepage(ISelectionAcquitViewModel model)
        {
            model.LieuSelector.Selected = null;
            model.AutreMentionSelector.Selected = null;
            ExecuteFilterByAutreMention(model);
        }

        public void ExecuteFilterByAutreMention(ISelectionAcquitViewModel model)
        {
            ExecuteFilterBySelection(model);
        }

        public void ExecuteFilterByLieu(ISelectionAcquitViewModel model)
        {
            ExecuteFilterBySelection(model);
        }
    }
}