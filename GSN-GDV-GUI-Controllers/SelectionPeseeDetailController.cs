﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionPeseeDetailController
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SelectionPeseeDetailController));

        public static IPeseeDetailListViewModel CreateListViewModel(IMainDataContext db, IQueryable<PeseeDetailModel> q = null)
        {
            var items = new List<IPeseeDetailListViewItemModel>();
            var model = new PeseeDetailListViewModel
            {
                IsFilter = false,
                Items = items,
            };
            if (q == null)
                return model;

            foreach (var t in q)
            {
                var o = CreateListViewItemModel(db: db, dr: t);

                items.Add(o);
                Log.DebugFormat("CreateListViewModel.Item: {0}", o);
            }
            return model;
        }

        public static IPeseeDetailListViewItemModel CreateListViewItemModel(IMainDataContext db, int Id)
        {
            return CreateListViewItemModel(db: db, dr: db.PeseeDetailSet.First(t => t.Id == Id));
        }

        public static IPeseeDetailListViewItemModel CreateListViewItemModel(IMainDataContext db, PeseeDetailModel dr)
        {
            return CreateListViewItemModel(db: db,
                dr: dr,
                entete: SelectionPeseeEnteteController.CreateListItemModel(db, dr.EnteteId)
            );
        }

        public static IPeseeDetailListViewItemModel CreateListViewItemModel(IMainDataContext db, PeseeDetailModel dr, IPeseeEnteteListViewItemModel entete)
        {
            var o = new PeseeDetailListViewModel.ItemModel
            {
                Id = dr.Id,
                Entete = entete,
                Article = SelectionArticleController.CreateSelectorItemModel(db, dr.ArticleId),
                Cepage = entete != null && entete.Cepage != null && entete.Cepage.ValueMember > 0 ? entete.Cepage : SelectionCepageController.CreateSelectorItemModel(db, dr.CepageId),
                Lot = dr.Lot,
                QuantiteKilogrammes = dr.QuantiteKilogramme,
                QuantiteLitres = dr.QuantiteLitre,
                SondageBricks = dr.SondageBricks,
                SondageOE = dr.SondageOE,
            };
            if (entete != null)
            {
                if (entete.Cepage == null || entete.Cepage.ValueMember < 1)
                    entete.Cepage = o.Cepage = SelectionCepageController.CreateSelectorItemModel(db, dr.CepageId);
                o.Acquit = entete.Acquit;
            }

            o.ConversionLitresKilogrammes = (o.QuantiteLitres ?? 0) / (o.QuantiteKilogrammes ?? 1.0);
            Log.DebugFormat("CreateListViewModel.Item: {0}", o);
            return o;
        }
    }
}