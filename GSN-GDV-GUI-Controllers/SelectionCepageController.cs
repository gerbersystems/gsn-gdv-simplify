﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionCepageController
    {
        public static Models.ISelectorItemModel<int, string> CreateSelectorItemModel(IMainDataContext db, int? Id)
        {
            return CreateSelector(db, Id: Id);
        }

        public static ISelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>> CreateSelector(IMainDataContext db, IQueryable<CepageModel> q, int? SelectedId = null)
        {
            var items = new List<ISelectorItemModel<int, string>>();
            var r = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
            {
                Items = items
            };
            foreach (var o in q)
                items.Add(CreateSelectorItemModel(o));

            r.Selected = (from t in items where t.ValueMember == (SelectedId ?? 0) select t).FirstOrDefault();
            return r;
        }

        public static ISelectorItemModel<int, string> CreateSelector(IMainDataContext db, int? Id)
        {
            if (Id == null)
                return null;
            var dr = (from t in db.CepageSet where t.Id == Id.Value select t).FirstOrDefault();
            return CreateSelectorItemModel(dr: dr);
        }

        public static ISelectorItemModel<int, string> CreateSelectorItemModel(Data.Models.CepageModel dr)
        {
            return new SelectorItemModel<int, string>
            {
                ValueMember = dr.Id,
                DisplayMember = dr.Nom
            };
        }
    }
}