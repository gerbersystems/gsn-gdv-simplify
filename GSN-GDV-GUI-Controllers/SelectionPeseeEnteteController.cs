﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.GUI.Models;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionPeseeEnteteController
    {
        public static IPeseeEnteteListViewItemModel CreateListItemModel(IMainDataContext db, int? Id = null)
        {
            var r = new PeseeEnteteListViewModel.ItemModel
            {
                Id = Id ?? 0,
            };
            if (Id == null)
                return r;
            var dr = (from t in db.PeseeEnteteSet where t.Id == Id select t).FirstOrDefault();
            return Build(r, db, dr);
        }

        public static IPeseeEnteteListViewItemModel CreateListItemModel(IMainDataContext db, Data.Models.PeseeEnteteModel dr)
        {
            return Build(new PeseeEnteteListViewModel.ItemModel(), db, dr);
        }

        public static T Build<T, M>(T model, IMainDataContext db, M dr)
            where T : IPeseeEnteteListViewItemModel
            where M : Data.Models.PeseeEnteteModel
        {
            if (dr == null)
                return model;
            model = BuildItemModel(model, db, dr);
            model.Id = dr.Id;
            model.Lieu = dr.Lieu;
            model.Date = dr.Date;
            model.IsGrandCru = dr.GrandCru;

            model.LieuProduction = SelectionLieuxProductionController.CreateSelector(db, Id: dr.LieuDeProductionId);
            model.Responsable = SelectionResponsableController.CreateSelector(db, Id: dr.ResponsableId);
            model.Acquit = SelectionAcquitController.CreateSelector(db, Id: dr.AcquitId);
            model.AutreMention = SelectionAutreMentionController.CreateSelector(db, Id: dr.AutreMentionId);

            model.Remarques = dr.Remarques;
            model.Divers1 = dr.Divers1;
            model.Divers2 = dr.Divers2;
            model.Divers3 = dr.Divers3;
            return model;
        }

        public static ISelectorItemModel<int, string> CreateSelector(IMainDataContext db, int? Id = null)
        {
            if (Id == null)
                return new SelectorItemModel<int, string>
                {
                    ValueMember = 0,
                };
            var dr = (from t in db.PeseeEnteteSet where t.Id == Id select t).FirstOrDefault();
            return CreateSelector(db: db, dr: dr);
        }

        public static ISelectorItemModel<int, string> CreateSelector(IMainDataContext db, Data.Models.PeseeEnteteModel dr)
        {
            var r = new SelectorItemModel<int, string>
            {
                ValueMember = 0,
            };
            return BuildItemModel(r, db, dr);
        }

        public static T BuildItemModel<T, M>(T r, IMainDataContext db, M dr)
            where T : ISelectorItemModel<int, string>
            where M : Data.Models.PeseeEnteteModel
        {
            if (dr == null)
                return r;
            r.ValueMember = dr.Id;
            r.DisplayMember = string.Concat(dr.Lieu, dr.GrandCru == true ? ", GrandCru" : "");
            return r;
        }
    }
}