﻿using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SelectionPersonController
    {
        public static ISelectorItemModel<int, string> CreateSelectorItemModel(Data.Contextes.IMainDataContext db, int? Id = null)
        {
            return CreateSelectorItemModel(db: db, dr: (from t in db.ProprietaireSet where t.Id == Id select t).FirstOrDefault());
        }

        public static ISelectorItemModel<int, string> CreateSelectorItemModel(Data.Contextes.IMainDataContext db, ProprietaireModel dr)
        {
            var r = new SelectorItemModel<int, string>
            {
                ValueMember = dr == null ? 0 : dr.Id,
                DisplayMember = dr == null ? string.Empty : string.Concat(dr.Titre, " ", dr.Nom, " ", dr.Prenom)
            };
            return r;
        }

        public static IPersonListViewItemModel CreateViewItemModel(Data.Contextes.IMainDataContext db, int Id)
        {
            return CreateViewItemModel(db: db, dr: (from t in db.ProprietaireSet where t.Id == Id select t).FirstOrDefault());
        }

        public static IPersonListViewItemModel CreateViewItemModel(Data.Contextes.IMainDataContext db, ProprietaireModel dr)
        {
            var r = new PersonListViewModel.ItemModel
            {
                Id = dr.Id,
                IsProducteur = dr.IsProducteur ?? false,
                IsProprietaire = dr.IsProprietaire ?? false,
                Nom = dr.Nom,
                Prenom = dr.Prenom,
                Societe = dr.SCTE,
                Titre = dr.Titre,
                NumeroTva = dr.TVA.Numero,
                ModeTva = new SelectorItemModel<int, string> { ValueMember = dr.TVA.ModeId ?? 0, DisplayMember = dr.TVA.Numero },
                Address = new SelectorItemModel<int, string> { ValueMember = dr.AddressId ?? 0 },
                AddressWinBiz = new SelectorItemModel<int, string> { ValueMember = dr.WinBiz.AddressId ?? 0 },
            };
            return r;
        }
    }
}