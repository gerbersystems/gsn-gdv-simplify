﻿using GSN.GDV.Data.Contextes;
using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    using Data.Extensions;

    public class TransfertPeseeDetailWizardController : ITransfertPeseeDetailWizardController
    {
        private IControllerHelper _helper;
        public static int YearId;

        public TransfertPeseeDetailWizardController(IControllerHelper helper)
        {
            this._helper = helper;
        }

        public virtual ITransfertPeseeDetailWizardViewModel CreateViewModel(int yearId, IMainDataContext db, int sourcePeseeEnteteId)
        {
            return CreateViewModel(yearId, db, SelectionPeseeEnteteController.CreateListItemModel(db, sourcePeseeEnteteId));
        }

        public virtual ITransfertPeseeDetailWizardViewModel CreateViewModel(int yearId, IMainDataContext db, int sourcePeseeEnteteId, int? pedId)
        {
            return CreateViewModel(yearId, db, SelectionPeseeEnteteController.CreateListItemModel(db, sourcePeseeEnteteId), pedId);
        }

        public virtual ITransfertPeseeDetailWizardViewModel CreateViewModel(int yearId, IMainDataContext db, int sourcePeseeEnteteId, int? pedId, double? kilosToTransfert, double? maxKilos)
        {
            return CreateViewModel(yearId, db, SelectionPeseeEnteteController.CreateListItemModel(db, sourcePeseeEnteteId), pedId, kilosToTransfert, maxKilos);
        }

        public virtual ITransfertPeseeDetailWizardViewModel CreateViewModel(int yearId, IMainDataContext db, PeseeEnteteModel sourcePeseeEntete)
        {
            return CreateViewModel(yearId, db, SelectionPeseeEnteteController.CreateListItemModel(db, sourcePeseeEntete));
        }

        public virtual ITransfertPeseeDetailWizardViewModel CreateViewModel(int yearId, IMainDataContext db, IPeseeEnteteListViewItemModel sourcePeseeEntete, int? pedId = null, double? kilosToTransfert = null, double? maxKilos = null)
        {
            YearId = yearId;
            var model = new TransfertPeseeDetailWizardViewModel
            {
                SourceItem = sourcePeseeEntete,
                Page0 = new SimplePageWizardViewModel { AllowNext = true },
                Page1 = new SimplePageWizardViewModel { AllowNext = true },
                Page2 = new SimplePageWizardViewModel { AllowNext = true },
                Page3 = new SimplePageWizardViewModel { AllowNext = true },

                Selected = new PeseeEnteteListViewModel.ItemModel
                {
                    Acquit = new SelectorItemModel<int, string> { },
                    Date = DateTime.Now,
                    LieuProduction = new SelectorItemModel<int, string> { },
                    AutreMention = new SelectorItemModel<int, string> { },
                    Responsable = new SelectorItemModel<int, string> { },
                    QuantiteKilogrammesATransferer = kilosToTransfert,
                    QuantiteMaxKilogrammesATransferer = maxKilos
                },
                PeseeDetailList = new PeseeDetailListViewModel
                {
                    Items = new List<IPeseeDetailListViewItemModel> { }
                },
                AcquitSelector = new SelectionAcquitViewModel
                {
                    Items = new List<IAcquitListViewItemModel> { }
                },
                ResponsableSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = new List<ISelectorItemModel<int, string>> { }
                },
                LieuProductionSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = new List<SelectorItemModel<int, string>>()
                },
                AutreMentionSelector = new SelectorListModel<ISelectorItemModel<int, string>, IEnumerable<ISelectorItemModel<int, string>>>
                {
                    Items = new List<SelectorItemModel<int, string>>()
                },
            };
            Build(db: db, model: model, pedId: pedId);
            return model;
        }

        private void Build(IMainDataContext db, ITransfertPeseeDetailWizardViewModel model, int? pedId)
        {
            model.AcquitSelector = SelectionAcquitController.CreateViewModel(db);
            var sourcePeseeEntete = model.SourceItem;

            if (sourcePeseeEntete != null)
            {
                var sourcePeseeEnteteId = sourcePeseeEntete.Id;
                var q = (
                    from t in db.PeseeDetailSet
                    where
                        t.EnteteId == sourcePeseeEnteteId &&
                        (t.ArticleId == null || t.ArticleId <= 0) && /// <Note>Ces articles ne peuvent pas être transféré</Note>
                        (pedId == null || t.Id == pedId.Value)
                    select t
                );

                model.PeseeDetailList.Items = SaisieDetailPeseeController.CreateListBy(q, model.Selected);
                if (sourcePeseeEntete.Cepage == null || sourcePeseeEntete.Cepage.ValueMember < 1)
                {
                    var CepageId = (from t in q where t.CepageId != null orderby t.CepageId descending select t.CepageId).FirstOrDefault();

                    if (CepageId != null)
                        sourcePeseeEntete.Cepage = SelectionCepageController.CreateSelector(db: db, Id: CepageId);
                }
                model.Selected.Cepage = sourcePeseeEntete.Cepage;
                model.Selected.Lieu = sourcePeseeEntete.Lieu;
                model.Selected.Responsable = sourcePeseeEntete.Responsable;
            }

            model.ResponsableSelector.Items = (
                from t in db.ResponsableSet
                orderby t.IsDefault descending
                select new SelectorItemModel<int, string> { ValueMember = t.Id, DisplayMember = string.Concat(t.Nom, ", ", t.Prenom) }
            ).ToList();

            if (model.Selected.Responsable == null)
            {
                model.ResponsableSelector.Selected = (from t in model.ResponsableSelector select t).FirstOrDefault();
                model.Selected.Responsable = model.ResponsableSelector.Selected;
            }
            else
            {
                model.ResponsableSelector.Selected = model.Selected.Responsable;
            }
            model.LieuProductionSelector = model.AcquitSelector.LieuSelector;
            model.AutreMentionSelector = model.AcquitSelector.AutreMentionSelector;
        }

        public virtual ISelectionAcquitController CreateSelectionAcquitController(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            return new SelectionAcquitController(viewModel.AcquitSelector);
        }

        public virtual void Transfert(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            using (var db = _helper.CreateMainDataContext())
            {
                var selected = viewModel.Selected;
                var dataModel = new PeseeEnteteModel
                {
                    Date = viewModel.SourceItem.Date,
                    Lieu = selected.Lieu,
                    GrandCru = selected.IsGrandCru,
                    ResponsableId = selected.Responsable.ValueMember,
                    AcquitId = selected.Acquit.ValueMember,

                    LieuDeProductionId = selected.LieuProduction.ValueMember,
                    AutreMentionId = selected.AutreMention.ValueMember,
                    Remarques = viewModel.SourceItem.Remarques,
                    Divers1 = viewModel.SourceItem.Divers1,
                    Divers2 = viewModel.SourceItem.Divers2,
                    Divers3 = viewModel.SourceItem.Divers3
                };
                db.PeseeEnteteSet.Add(dataModel);
                db.SaveChanges();

                var q = viewModel.PeseeDetailList.Where(p => p.IsSelected);

                if (viewModel.IsSplit && viewModel.Selected.QuantiteKilogrammesATransferer <= viewModel.Selected.QuantiteMaxKilogrammesATransferer)
                {
                    var year = db.AnneeSet.Find(YearId);
                    var lot = PeseeDetailExtension.NextLot(db, year?.Annee ?? DateTime.Now.Year);

                    foreach (var ped in q)
                    {
                        var oldPed = db.PeseeDetailSet.FirstOrDefault(t => t.Id == ped.Id);
                        if (oldPed == null) continue;
                        var newPed = new PeseeDetailModel
                        {
                            CepageId = selected.Cepage.ValueMember,
                            Lot = lot,
                            EnteteId = dataModel.Id,
                            ArticleId = oldPed.ArticleId,
                            QuantiteKilogramme = viewModel.Selected.QuantiteKilogrammesATransferer,
                            QuantiteLitre = viewModel.Selected.QuantiteKilogrammesATransferer * Math.Round(oldPed.QuantiteLitre / oldPed.QuantiteKilogramme ?? 0, 1),
                            SondageBricks = oldPed.SondageBricks,
                            SondageOE = oldPed.SondageOE
                        };
                        oldPed.QuantiteKilogramme = oldPed.QuantiteKilogramme - newPed.QuantiteKilogramme;
                        oldPed.QuantiteLitre = oldPed.QuantiteLitre - newPed.QuantiteLitre;
                        db.PeseeDetailSet.Add(newPed);
                    }
                    db.SaveChanges();
                }
                else
                {
                    foreach (var ped in q)
                    {
                        var ped_id = ped.Id;
                        if (ped_id < 1) continue;
                        var dr = db.PeseeDetailSet.FirstOrDefault(t => t.Id == ped_id);
                        dr.CepageId = selected.Cepage.ValueMember;
                        dr.Lot = ped.Lot;
                        dr.EnteteId = dataModel.Id;
                    }
                    db.SaveChanges();
                    if (db.PeseeDetailSet.Any(p => p.EnteteId == viewModel.SourceItem.Id)) return;
                    var pesee = db.PeseeEnteteSet.Find(viewModel.SourceItem.Id);
                    if (pesee == null) return;
                    db.PeseeEnteteSet.Remove(pesee);
                    db.SaveChanges();
                }
            }
        }

        #region SelectionPeseeDetailWizardPage

        public virtual void CommitSelectionPeseeDetailWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            var model = viewModel;
            var q = (from t in model.PeseeDetailList where t.IsSelected == true select t);
            var v = q.Count() > 0;
            if (v == false)
            {
                throw new InvalidOperationException("Sélectionnez au moins une pesée");
            }
            var ped = q.FirstOrDefault(t => t.Cepage != null);
            if (ped != null)
            {
                var CepageId = ped.Cepage.ValueMember;
                model.AcquitSelector.Items = (from t in model.AcquitSelector.Items where t.Cepage != null && t.Cepage.ValueMember == CepageId select t).ToList();
            }
            model.Page0.AllowNext = true;
        }

        public virtual void InitializeSelectionPeseeDetailWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
        }

        #endregion SelectionPeseeDetailWizardPage

        #region SelectionAcquitWizardPage

        public virtual void InitializeSelectionAcquitWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            var model = viewModel.AcquitSelector;
        }

        public virtual void CommitSelectionAcquitWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            var model = viewModel;
            var v = model.AcquitSelector.Selected != null && model.AcquitSelector.Selected.Id > 0;
            if (v == false)
            {
                throw new InvalidOperationException("Sélectionnez un Acquit");
            }
            else
            {
                var AcquitId = model.AcquitSelector.Selected.Id;
                model.Selected.Acquit = model.AcquitSelector.Selected;
                model.Selected.Cepage = model.AcquitSelector.Selected.Cepage;
                var q = (from t in model.AcquitSelector.Items where t.Id == AcquitId select t);
                model.LieuProductionSelector.Items = q.Where(l => l.Lieu.ValueMember == model.AcquitSelector.Selected.Lieu.ValueMember).Select(l => l.Lieu).Take(1).ToList();
                model.AutreMentionSelector.Items = q.Where(l => l.AutreMention.ValueMember == model.AcquitSelector.Selected.AutreMention.ValueMember).Select(l => l.AutreMention).Take(1).ToList();
                model.Selected.AutreMention = model.AutreMentionSelector.Items.FirstOrDefault(a => a.ValueMember == model.AcquitSelector.Selected.AutreMention.ValueMember);
                model.Selected.LieuProduction = model.LieuProductionSelector.Items.FirstOrDefault(a => a.ValueMember == model.AcquitSelector.Selected.Lieu.ValueMember);
                model.Selected.Responsable = model.ResponsableSelector.Items.FirstOrDefault(r => r.ValueMember == viewModel.SourceItem.Responsable.ValueMember);
            }
            model.Page1.AllowNext = true;
        }

        #endregion SelectionAcquitWizardPage

        #region SaisisPeseeEnteteWizardPage

        public virtual void InitializeSaisisPeseeEnteteWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
        }

        public virtual void CommitSaisisPeseeEnteteWizardPage(ITransfertPeseeDetailWizardViewModel viewModel)
        {
            var model = viewModel;
            var selected = model.Selected;
            var q = model.PeseeDetailList.Where(p => p.IsSelected).ToList();
            var v =
                selected != null &&
                string.IsNullOrWhiteSpace(selected.Lieu) == false &&
                selected.Date != null &&
                selected.Responsable != null &&
                selected.Responsable.ValueMember >= 0 &&
                selected.Acquit != null &&
                selected.Acquit.ValueMember >= 0 &&
                selected.Cepage != null &&
                selected.Cepage.ValueMember >= 0 &&
                selected.LieuProduction != null &&
                selected.LieuProduction.ValueMember >= 0 &&
                selected.AutreMention != null &&
                selected.AutreMention.ValueMember >= 0 &&
                (!selected.QuantiteKilogrammesATransferer.HasValue || selected.QuantiteKilogrammesATransferer > 0 && selected.QuantiteKilogrammesATransferer <= selected.QuantiteMaxKilogrammesATransferer) &&
                q.Any();

            if (v == false)
            {
                throw new InvalidOperationException("Le formulaire n'est pas valide");
            }
            else
            {
                var s = "\n";
                foreach (var o in q)
                {
                    s = string.Concat(s, "- ", "ID: ", o.Id, ", LOT: ", o.Lot,
                                      " - QUANTITE: ", string.Format(viewModel.IsSplit ? viewModel.Selected.QuantiteKilogrammesATransferer.ToString() : o.QuantiteKilogrammes.ToString(), "N0"), " Kg / ", viewModel.IsSplit ? viewModel.Selected.QuantiteKilogrammesATransferer * Math.Round(o.QuantiteLitres / o.QuantiteKilogrammes ?? 0, 1) : o.QuantiteLitres,
                                      " Lt - SONDAGE: ", o.SondageBricks, "% °Bx / ", o.SondageOE, "° Oe\n");
                }

                selected.DisplayMember = string.Concat(
                    "Lieu et date de livraison: ", selected.Lieu, " - ", model.SourceItem.Date, "\n",
                    "Responsable: ", selected.Responsable.DisplayMember, "\n",
                    Environment.NewLine,
                    selected.Acquit.DisplayMember, "\n",
                    Environment.NewLine,
                    "Pesées:             ", s, "\n"
                );
            }
            model.Page2.AllowNext = true;
        }

        #endregion SaisisPeseeEnteteWizardPage
    }
}