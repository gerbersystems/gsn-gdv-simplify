﻿using GSN.GDV.Data.Models;
using GSN.GDV.GUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GSN.GDV.GUI.Controllers
{
    public class SaisieDetailPeseeController : ISaisieDetailPeseeController
    {
        static public readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SaisieEntetePeseeController));
        private readonly IControllerHelper helper;

        public SaisieDetailPeseeController(IControllerHelper helper)
        {
            this.helper = helper;
        }

        public virtual void ExecuteImprimer(Models.ISaisieDetailPeseeViewModel model)
        {
            throw new NotImplementedException();
        }

        public virtual void ExecuteAnnuler(Models.ISaisieDetailPeseeViewModel model)
        {
            throw new NotImplementedException();
        }

        public virtual void ExecuteOK(Models.ISaisieDetailPeseeViewModel model)
        {
            using (var db = helper.CreateMainDataContext())
            {
                var selected = model.Selected;
                var Id = model.Selected.Id;
                PeseeDetailModel dr;
                if (Id > 0)
                {
                    dr = (from t in db.PeseeDetailSet where t.Id == Id select t).First();
                }
                else
                {
                    dr = db.PeseeDetailSet.Create();
                    db.PeseeDetailSet.Add(dr);
                }
                dr.EnteteId = selected.Entete.ValueMember;
                dr.CepageId = selected.Cepage.ValueMember;
                dr.ArticleId = selected.Article.ValueMember;

                dr.Lot = selected.Lot;
                dr.QuantiteKilogramme = selected.QuantiteKilogrammes;
                dr.QuantiteLitre = selected.QuantiteLitres;
                dr.SondageBricks = selected.SondageBricks;
                dr.SondageOE = selected.SondageOE;

                db.SaveChanges();
            };
        }

        public virtual void SynchronizeConversion(Models.ISaisieDetailPeseeViewModel model)
        {
            var qkg = model.Selected.QuantiteKilogrammes ?? 0;
            var ql = model.Selected.QuantiteLitres ?? 0;
            if (ql != 0 && qkg != 0)
                model.Selected.ConversionLitresKilogrammes = ql / qkg;
        }

        public virtual void UpdateLimitation(Models.ISaisieDetailPeseeViewModel model)
        {
            var quantite = (model.IsUniteLitres ? model.Selected.QuantiteLitres : model.Selected.QuantiteKilogrammes) ?? 0.0d;
            model.Limitation.Solde = model.Limitation.Admis - model.Limitation.Saisis - quantite;
        }

        public virtual void SynchronizeLimitation(Models.ISaisieDetailPeseeViewModel model)
        {
            throw new NotImplementedException();
        }

        public void DoQuantiteKilogrammesChanged(Models.ISaisieDetailPeseeViewModel model)
        {
            model.Selected.QuantiteLitres = model.Selected.ConversionLitresKilogrammes * model.Selected.QuantiteKilogrammes;
        }

        public void DoQuantiteLitresChanged(Models.ISaisieDetailPeseeViewModel model)
        {
            model.Selected.QuantiteKilogrammes = model.Selected.QuantiteLitres / ((model.Selected.ConversionLitresKilogrammes ?? 1) == 0 ? 1 : model.Selected.ConversionLitresKilogrammes);
        }

        public void DoConversionLitresKilogrammesChanged(Models.ISaisieDetailPeseeViewModel model)
        {
            var v = model.Selected.ConversionLitresKilogrammes ?? 1;
            if (v == 0)
                v = 1;
            model.Selected.ConversionLitresKilogrammes = v;
            DoQuantiteKilogrammesChanged(model);
            DoQuantiteLitresChanged(model);
        }

        public static IList<Models.IPeseeDetailListViewItemModel> CreateListBy(IQueryable<PeseeDetailModel> q, IPeseeEnteteListViewItemModel entete)
        {
            ISelectorItemModel<int, string> e = entete;
            var l = CreateListBy(q: q, entete: e);
            foreach (var o in l)
            {
                o.Cepage = entete.Cepage;
                o.Acquit = entete.Acquit;
                o.IsSelected = true;
            }
            return l;
        }

        public static IList<Models.IPeseeDetailListViewItemModel> CreateListBy(IQueryable<PeseeDetailModel> q, ISelectorItemModel<int, string> entete)
        {
            var l = new List<Models.IPeseeDetailListViewItemModel>();
            foreach (var t in q)
            {
                var o = new PeseeDetailListViewModel.ItemModel
                {
                    Id = t.Id,
                    Lot = t.Lot,
                    QuantiteKilogrammes = t.QuantiteKilogramme,
                    QuantiteLitres = t.QuantiteLitre,
                    SondageBricks = t.SondageBricks,
                    SondageOE = t.SondageOE,
                    Entete = entete,
                    Acquit = new SelectorItemModel<int, string> { },
                    Article = new SelectorItemModel<int, string> { ValueMember = t.ArticleId ?? 0, DisplayMember = (t.ArticleId != null && t.ArticleId > 0).ToString() },
                    Cepage = new SelectorItemModel<int, string> { },
                };
                l.Add(o);
            }
            return l;
        }
    }
}