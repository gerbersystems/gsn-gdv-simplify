﻿namespace GSN.GDV.GUI.Controllers
{
    public interface IControllerHelper
    {
        global::GSN.GDV.Business.IConnectionManager GetConnectionManager();

        global::GSN.GDV.Business.IGlobalParameterProvider GetGlobalParameterProvider();

        global::GSN.GDV.Data.Contextes.IMainDataContext CreateMainDataContext();
    }
}