﻿using System;
using System.Drawing;
using System.Windows.Forms;
using LocalResources = global::GSN.GDV.GUI.Common.Properties.Resources;
using LocalSettings = global::GSN.GDV.GUI.Common.Properties.Settings;

namespace GSN.GDV.GUI.Extensions
{
    public static class StyleBuilderExtension
    {
        public enum TextBoxStyleEnum
        {
            ReadOnly, Calucation, Editable, DateTime
        }

        public enum FormStyleEnum
        {
            Report, Editor, Dialog
        }

        static public void ApplyStyle(this Form me, FormStyleEnum style = FormStyleEnum.Editor)
        {
            me.SuspendLayout();

            me.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            me.BackColor = LocalSettings.Default.BackColor2;
            //me.ClientSize = new System.Drawing.Size(680, 86);
            me.Icon = LocalResources.FormIcon;
            me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            me.ResumeLayout(false);
        }

        static public void ApplyStyle(this TextBoxBase me, TextBoxStyleEnum style = TextBoxStyleEnum.ReadOnly)
        {
            //me.BackColor = me.ReadOnly ? SystemColors.InactiveBorder : TextBoxBase.DefaultBackColor;

            //me.BackColor = me.ReadOnly ? SystemColors.InactiveBorder : Color.FromKnownColor(KnownColor.WindowText);
            me.BackColor = me.ReadOnly ? SystemColors.InactiveBorder : Color.White;
        }

        static public void ApplyStyle(TextBoxStyleEnum style = TextBoxStyleEnum.ReadOnly, params TextBoxBase[] args)
        {
            foreach (var argv in args)
                ApplyStyle(argv, style: style);
        }

        static public void ActivateStyle(this TextBoxBase me, TextBoxStyleEnum style = TextBoxStyleEnum.ReadOnly)
        {
            me.ReadOnlyChanged += delegate(object sender, EventArgs e)
            {
                ApplyStyle(me: me, style: style);
            };
            ApplyStyle(me: me, style: style);
        }

        static public void ActivateStyle(TextBoxStyleEnum style = TextBoxStyleEnum.ReadOnly, params TextBoxBase[] args)
        {
            foreach (var argv in args)
                ActivateStyle(argv, style: style);
        }
    }
}