﻿using GSN.GDV.GUI.Models;

namespace GSN.GDV.GUI.Controllers
{
    public interface ISaisieDetailPeseeController
    {
        void ExecuteImprimer(ISaisieDetailPeseeViewModel model);

        void ExecuteAnnuler(ISaisieDetailPeseeViewModel model);

        void ExecuteOK(ISaisieDetailPeseeViewModel model);

        void SynchronizeConversion(ISaisieDetailPeseeViewModel model);

        void UpdateLimitation(ISaisieDetailPeseeViewModel model);

        void SynchronizeLimitation(ISaisieDetailPeseeViewModel model);

        void DoQuantiteKilogrammesChanged(ISaisieDetailPeseeViewModel ViewModel);

        void DoQuantiteLitresChanged(ISaisieDetailPeseeViewModel ViewModel);

        void DoConversionLitresKilogrammesChanged(ISaisieDetailPeseeViewModel ViewModel);
    }
}