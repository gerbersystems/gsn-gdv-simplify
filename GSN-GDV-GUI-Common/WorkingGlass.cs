﻿using GSN.GDV.Commons;
using System;
using System.Windows.Forms;

namespace GSN.GDV.GUI.Common
{
    public class WorkingGlass : IDisposable
    {
        readonly private Cursor _windowsFormLastCursor;
        readonly private System.Windows.Input.Cursor _wpfLastCursor;
        readonly private ExecutionBlock executionBlock;

        public WorkingGlass()
        {
            Cursor.Current = Cursors.WaitCursor;
        }

        public WorkingGlass(Control me)
        {
            _windowsFormLastCursor = me.Cursor;
            executionBlock = new ExecutionBlock(() =>
            {
                me.Cursor = Cursors.WaitCursor;
            }, () =>
            {
                me.Cursor = _windowsFormLastCursor;
            });
        }

        public WorkingGlass(System.Windows.FrameworkElement me)
        {
            _wpfLastCursor = me.Cursor;
            executionBlock = new ExecutionBlock(() =>
            {
                me.Cursor = System.Windows.Input.Cursors.Wait;
            }, () =>
            {
                me.Cursor = _wpfLastCursor;
            });
        }

        public virtual void Dispose()
        {
            //Bug export PDF : program tries to dispose executionBlock when it is null -> implement executionBlock in first contructor (with no parameters)
            //temporary solution : test if executionBlock is null
            if(executionBlock != null)
                executionBlock.Dispose();
        }
    }
}