﻿using System.Drawing;
using System.Windows.Forms;

namespace GSN.GDV.GUI.Common.Helpers
{
    public static class ControlHelper
    {
        static public void ActiveIt(bool needBeActivated, params Control[] args)
        {
            foreach (var argv in args)
            {
                if (needBeActivated)
                {
                    argv.BackColor = Color.White;
                    if (argv is TextBoxBase)
                        (argv as TextBoxBase).ReadOnly = false;
                    else if (argv is NumericUpDown)
                        (argv as NumericUpDown).ReadOnly = false;
                    else if (argv is ListControl)
                        (argv as ListControl).Enabled = false;
                }
                else
                {
                    argv.BackColor = Color.FromKnownColor(KnownColor.Control);
                    if (argv is TextBoxBase)
                        (argv as TextBoxBase).ReadOnly = true;
                    else if (argv is NumericUpDown)
                        (argv as NumericUpDown).ReadOnly = true;
                    else if (argv is ListControl)
                        (argv as ListControl).Enabled = true;
                }
            }
        }
    }
}