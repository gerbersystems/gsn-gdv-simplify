﻿using GSN.GDV.GUI.Models;
using System.Collections.Generic;

namespace GSN.GDV.GUI.Controllers
{
    public interface ISelectionAcquitController
    {
        void Set(ISelectionAcquitViewModel viewModel);

        void LoadItems(IEnumerable<IAcquitListViewItemModel> argv);

        void ExecuteFilterBySelection(ISelectionAcquitViewModel model);

        void ExecuteFilterByAutreMention(ISelectionAcquitViewModel model);

        void ExecuteFilterByLieu(ISelectionAcquitViewModel model);

        void ExecuteFilterByProducteur(ISelectionAcquitViewModel model);

        void ExecuteFilterByCepage(ISelectionAcquitViewModel model);
    }
}