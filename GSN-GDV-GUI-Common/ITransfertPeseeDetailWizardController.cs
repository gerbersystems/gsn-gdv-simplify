﻿using GSN.GDV.GUI.Models;

namespace GSN.GDV.GUI.Controllers
{
    public interface ITransfertPeseeDetailWizardController
    {
        ISelectionAcquitController CreateSelectionAcquitController(ITransfertPeseeDetailWizardViewModel viewModel);

        #region SelectionPeseeDetailWizardPage

        void CommitSelectionPeseeDetailWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        void InitializeSelectionPeseeDetailWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        #endregion SelectionPeseeDetailWizardPage

        #region SelectionAcquitWizardPage

        void CommitSelectionAcquitWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        void InitializeSelectionAcquitWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        #endregion SelectionAcquitWizardPage

        #region SaisisPeseeEnteteWizardPage

        void CommitSaisisPeseeEnteteWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        void InitializeSaisisPeseeEnteteWizardPage(ITransfertPeseeDetailWizardViewModel viewModel);

        #endregion SaisisPeseeEnteteWizardPage

        void Transfert(ITransfertPeseeDetailWizardViewModel viewModel);
    }
}