﻿using GSN.GDV.GUI.Models;

namespace GSN.GDV.GUI.Controllers
{
    public interface ISaisieEntetePeseeController
    {
        ISaisieEntetePeseeViewModel CreateEmptyViewModel();

        ISelectionAcquitController CreateSelectionAcquitController(ISaisieEntetePeseeViewModel viewModel);

        ISaisieDetailPeseeController CreateSaisieDetailPeseeController(ISaisieEntetePeseeViewModel viewModel);
    }
}