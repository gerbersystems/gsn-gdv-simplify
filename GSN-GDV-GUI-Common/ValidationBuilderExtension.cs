﻿using GSN.GDV.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace GSN.GDV.GUI.Extensions
{
    public static class ValidationBuilderExtension
    {
        //private static readonly Dictionary<ValidationTypeEnum, ErrorProvider> _errorProviders;
        //static ValidationBuilderExtension()
        //{
        //	var p = new System.Windows.Forms.ErrorProvider();
        //	//p.SetIconAlignment(this.nameTextBox1, ErrorIconAlignment.MiddleRight);
        //	//p.SetIconPadding(this.nameTextBox1, 2);
        //	p.BlinkRate = 1000;
        //	p.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
        //	_errorProviders.Add(Numeric, p);
        //}
        [Flags]
        public enum ValidationTypeEnum
        {
            NotNull = 1, NotEmptyString = 2,
            Numeric = 4, Integer = 1 + 2 + 4 + 8,
            Time = 1 + 2 + 16, Date = 1 + 2 + 32, DateTime = 1 + 2 + 16 + 32,
            RegExp = 64
        }

        static public ErrorProvider ApplyValidation(this ListControl me, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, string description = null, System.Func<bool, object, CancelEventArgs, bool> validationAction = null)
        {
            return ApplyValidation(me: me, value: me.SelectedValue, type: type, description: description, validationAction: validationAction);
        }

        static public ErrorProvider ApplyValidation(this TextBoxBase me, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, string description = null, System.Func<bool, object, CancelEventArgs, bool> validationAction = null)
        {
            return ApplyValidation(me: me, value: me.Text, type: type, description: description, validationAction: validationAction);
        }

        static public ErrorProvider ApplyValidation(this Control me, object value, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, string description = null, System.Func<bool, object, CancelEventArgs, bool> validationAction = null)
        {
            return ApplyValidation(me: me, validationAction: delegate(object sender, CancelEventArgs e)
            {
                return IsValid(me: me, value: value, type: type, validationAction: validationAction, sender: sender, e: e);
            }, description: description);
        }

        static public bool IsValid(this Control me, object value, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, System.Func<bool, object, CancelEventArgs, bool> validationAction = null, object sender = null, CancelEventArgs e = null)
        {
            var isActive = me.Enabled;
            var isValid = true;

            if (isActive && me is TextBoxBase && (me as TextBoxBase).ReadOnly)
                isActive = false;

            if (isActive && isValid)
                isValid = IsValid(value: value, type: type);

            if (isActive && validationAction != null)
                isValid = validationAction.Invoke(isValid, sender, e);
            return isValid;
        }

        static public bool IsValid(object value, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString)
        {
            var isValid = true;

            if (isValid && (type & ValidationTypeEnum.NotNull) == ValidationTypeEnum.NotNull)
                isValid = value != null;

            if (isValid && (type & ValidationTypeEnum.NotEmptyString) == ValidationTypeEnum.NotEmptyString)
                isValid = string.IsNullOrWhiteSpace(value.ToString()) == false;

            if (isValid && (type & ValidationTypeEnum.Numeric) == ValidationTypeEnum.Numeric)
                isValid = ValidationHelper.IsNumeric(value);

            if (isValid && (type & ValidationTypeEnum.Integer) == ValidationTypeEnum.Integer)
                isValid = ValidationHelper.IsInteger(value);

            if (isValid && (type & ValidationTypeEnum.Time) == ValidationTypeEnum.Time)
                isValid = ValidationHelper.IsInteger(value);

            if (isValid && (type & ValidationTypeEnum.Date) == ValidationTypeEnum.Date)
                isValid = ValidationHelper.IsInteger(value);

            if (isValid && (type & ValidationTypeEnum.DateTime) == ValidationTypeEnum.DateTime)
                isValid = ValidationHelper.IsInteger(value);
            return isValid;
        }

        static public ErrorProvider ApplyValidation(this Control me, object value, CancelEventHandler handler, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, string description = null)
        {
            return ApplyValidation(me: me, value: value, type: type, validationAction: delegate(bool isValid, object sender, CancelEventArgs e)
            {
                if (handler != null)
                    handler.Invoke(sender, e);
                return isValid;
            });
        }

        static public ErrorProvider ApplyValidation(this Control me, System.Func<object, CancelEventArgs, bool> validationAction, string description = null)
        {
            var p = CreateErrorProvider(me);
            me.Validating += delegate(object sender, CancelEventArgs e)
            {
                var isValid = validationAction.Invoke(sender, e);
                Validate(me: me, isValid: isValid, description: description, setError: true, errorProvider: p);
            };
            return p;
        }

        static public bool Validate(this Control me, object value, ValidationTypeEnum type = ValidationTypeEnum.NotEmptyString, string description = null, bool setError = true, ErrorProvider errorProvider = null)
        {
            var isValid = IsValid(me: me, value: value, type: type);

            return Validate(me: me, isValid: isValid, description: description, setError: setError, errorProvider: errorProvider);
        }

        static public bool Validate(this Control me, bool isValid, string description = null, bool setError = true, ErrorProvider errorProvider = null)
        {
            var p = errorProvider;
            if (setError)
            {
                if (p == null)
                    p = CreateErrorProvider(me);

                if (isValid)
                {
                    p.SetError(me, string.Empty);
                }
                else
                {
                    p.SetError(me, description ?? "Non valide");
                }
            }
            return isValid;
        }

        private static Dictionary<Control, ErrorProvider> controlErrorProviderMap = new Dictionary<Control, ErrorProvider>();
        private static object mapLocker = new object();

        static public ErrorProvider CreateErrorProvider(this Control me, IContainer container = null, bool createNew = false)
        {
            lock (mapLocker)
            {
                System.Windows.Forms.ErrorProvider p = null;
                if (createNew == false && controlErrorProviderMap.TryGetValue(me, out p) && p != null)
                    return p;
                container = container ?? me.Container;
                p = container == null ? new System.Windows.Forms.ErrorProvider() : new System.Windows.Forms.ErrorProvider(container);
                Build(me: me, p: p);
                controlErrorProviderMap.Add(me, p);
                return p;
            }
        }

        static public ErrorProvider CreateErrorProvider(this Control me, ContainerControl container)
        {
            var p = new System.Windows.Forms.ErrorProvider(container);
            Build(me: me, p: p);
            return p;
        }

        static public void Build(this Control me, ErrorProvider p)
        {
            p.SetIconAlignment(me, ErrorIconAlignment.MiddleRight);
            p.SetIconPadding(me, 2);
            p.BlinkRate = 1000;
            p.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.BlinkIfDifferentError;
        }
    }
}