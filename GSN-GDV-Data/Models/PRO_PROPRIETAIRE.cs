namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PRO_PROPRIETAIRE
    {
        public PRO_PROPRIETAIRE()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
            ACQ_ACQUIT1 = new HashSet<ACQ_ACQUIT>();
            PAI_PAIEMENT = new HashSet<PAI_PAIEMENT>();
        }

        [Key]
        public int PRO_ID { get; set; }

        public int? ADR_ID { get; set; }

        [StringLength(50)]
        public string PRO_NOM { get; set; }

        [StringLength(50)]
        public string PRO_PRENOM { get; set; }

        [StringLength(50)]
        public string PRO_TITRE { get; set; }

        [StringLength(50)]
        public string PRO_SCTE { get; set; }

        public bool? PRO_IS_PROPRIETAIRE { get; set; }

        public bool? PRO_IS_PRODUCTEUR { get; set; }

        [StringLength(50)]
        public string PRO_NUMERO_TVA { get; set; }

        public int? ADR_ID_WINBIZ { get; set; }

        public int? MOD_ID { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT1 { get; set; }

        public virtual ADR_ADRESSE ADR_ADRESSE { get; set; }

        public virtual MOD_MODE_TVA MOD_MODE_TVA { get; set; }

        public virtual ICollection<PAI_PAIEMENT> PAI_PAIEMENT { get; set; }
    }
}