namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class RES_RESPONSABLE
    {
        public RES_RESPONSABLE()
        {
            PEE_PESEE_ENTETE = new HashSet<PEE_PESEE_ENTETE>();
        }

        [Key]
        public int RES_ID { get; set; }

        public int? ADR_ID { get; set; }

        [StringLength(50)]
        public string RES_NOM { get; set; }

        [StringLength(50)]
        public string RES_PRENOM { get; set; }

        public bool RES_DEFAULT { get; set; }

        public virtual ADR_ADRESSE ADR_ADRESSE { get; set; }

        public virtual ICollection<PEE_PESEE_ENTETE> PEE_PESEE_ENTETE { get; set; }
    }
}