namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CLA_CLASSE
    {
        public CLA_CLASSE()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
        }

        [Key]
        public int CLA_ID { get; set; }

        [StringLength(50)]
        public string CLA_NOM { get; set; }

        public int? CLA_NUMERO { get; set; }

        [StringLength(50)]
        public string CLA_NUMERO_CSCV { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }
    }
}