namespace GSN.GDV.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ACQ_ACQUIT
    {
        public ACQ_ACQUIT()
        {
            PAR_PARCELLE = new HashSet<PAR_PARCELLE>();
            PEE_PESEE_ENTETE = new HashSet<PEE_PESEE_ENTETE>();
        }

        [Key]
        public int ACQ_ID { get; set; }

        public int? CLA_ID { get; set; }

        public int? COU_ID { get; set; }

        public int? ANN_ID { get; set; }

        public int? COM_ID { get; set; }

        public int? PRO_ID { get; set; }

        public int? ACQ_NUMERO { get; set; }

        public DateTime? ACQ_DATE { get; set; }

        public double? ACQ_SURFACE { get; set; }

        public double? ACQ_LITRES { get; set; }

        public int? REG_ID { get; set; }

        public int? ACQ_NUMERO_COMPL { get; set; }

        public int? PRO_ID_VENDANGE { get; set; }

        public DateTime? ACQ_RECEPTION { get; set; }

        public virtual PRO_PROPRIETAIRE PRO_PROPRIETAIRE { get; set; }

        public virtual CLA_CLASSE CLA_CLASSE { get; set; }

        public virtual COU_COULEUR COU_COULEUR { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE { get; set; }

        public virtual COM_COMMUNE COM_COMMUNE { get; set; }

        public virtual PRO_PROPRIETAIRE PRO_PROPRIETAIRE1 { get; set; }

        public virtual REG_REGION REG_REGION { get; set; }

        public virtual ICollection<PAR_PARCELLE> PAR_PARCELLE { get; set; }

        public virtual ICollection<PEE_PESEE_ENTETE> PEE_PESEE_ENTETE { get; set; }
    }
}