namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ANN_ANNEE
    {
        public ANN_ANNEE()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
            ART_ARTICLE = new HashSet<ART_ARTICLE>();
            MOC_MODE_COMPTABILISATION1 = new HashSet<MOC_MODE_COMPTABILISATION>();
            PAI_PAIEMENT = new HashSet<PAI_PAIEMENT>();
            PAM_PARAMETRES = new HashSet<PAM_PARAMETRES>();
            VAL_VALEUR_CEPAGE = new HashSet<VAL_VALEUR_CEPAGE>();
        }

        [Key]
        public int ANN_ID { get; set; }

        public int? ADR_ID { get; set; }

        public int? ANN_AN { get; set; }

        [StringLength(50)]
        public string ANN_ENCAVEUR_NOM { get; set; }

        [StringLength(50)]
        public string ANN_ENCAVEUR_PRENOM { get; set; }

        public bool? ANN_ENCAVEUR_SOUMIS_CONTROL { get; set; }

        [StringLength(50)]
        public string ANN_RESPONSABLE_1 { get; set; }

        [StringLength(50)]
        public string ANN_RESPONSABLE_2 { get; set; }

        [StringLength(50)]
        public string ANN_RESPONSABLE_3 { get; set; }

        [StringLength(50)]
        public string ANN_RESPONSABLE_4 { get; set; }

        public int? ANN_ENCAVEUR_NUMERO { get; set; }

        [StringLength(50)]
        public string ANN_ENCAVEUR_SOCIETE { get; set; }

        public int? MOC_ID { get; set; }

        public bool? ANN_IS_PRIX_KG { get; set; }

        public decimal ANN_TAUX_CONVERTION_BLANC { get; set; }

        public decimal ANN_TAUX_CONVERTION_ROUGE { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual ADR_ADRESSE ADR_ADRESSE { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }

        public virtual ICollection<ART_ARTICLE> ART_ARTICLE { get; set; }

        public virtual ICollection<MOC_MODE_COMPTABILISATION> MOC_MODE_COMPTABILISATION1 { get; set; }

        public virtual ICollection<PAI_PAIEMENT> PAI_PAIEMENT { get; set; }

        public virtual ICollection<PAM_PARAMETRES> PAM_PARAMETRES { get; set; }

        public virtual ICollection<VAL_VALEUR_CEPAGE> VAL_VALEUR_CEPAGE { get; set; }
    }
}