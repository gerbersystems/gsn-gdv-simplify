namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class MCO_MOC_COU
    {
        [Key]
        public int MCO_ID { get; set; }

        public int MOC_ID { get; set; }

        public int COU_ID { get; set; }

        public virtual COU_COULEUR COU_COULEUR { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }
    }
}