namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MOD_MODE_TVA
    {
        public MOD_MODE_TVA()
        {
            LIM_LIGNE_PAIEMENT_MANUELLE = new HashSet<LIM_LIGNE_PAIEMENT_MANUELLE>();
            LIP_LIGNE_PAIEMENT_PESEE = new HashSet<LIP_LIGNE_PAIEMENT_PESEE>();
            MOC_MODE_COMPTABILISATION = new HashSet<MOC_MODE_COMPTABILISATION>();
            PRO_PROPRIETAIRE = new HashSet<PRO_PROPRIETAIRE>();
        }

        [Key]
        public int MOD_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string MOD_NOM { get; set; }

        public virtual ICollection<LIM_LIGNE_PAIEMENT_MANUELLE> LIM_LIGNE_PAIEMENT_MANUELLE { get; set; }

        public virtual ICollection<LIP_LIGNE_PAIEMENT_PESEE> LIP_LIGNE_PAIEMENT_PESEE { get; set; }

        public virtual ICollection<MOC_MODE_COMPTABILISATION> MOC_MODE_COMPTABILISATION { get; set; }

        public virtual ICollection<PRO_PROPRIETAIRE> PRO_PROPRIETAIRE { get; set; }
    }
}