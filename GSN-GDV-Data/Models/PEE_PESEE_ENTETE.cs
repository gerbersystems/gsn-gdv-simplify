namespace GSN.GDV.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PEE_PESEE_ENTETE
    {
        public PEE_PESEE_ENTETE()
        {
            PED_PESEE_DETAIL = new HashSet<PED_PESEE_DETAIL>();
        }

        [Key]
        public int PEE_ID { get; set; }

        public int? ACQ_ID { get; set; }

        public int? RES_ID { get; set; }

        public DateTime? PEE_DATE { get; set; }

        [StringLength(50)]
        public string PEE_LIEU { get; set; }

        public bool? PEE_GRANDCRU { get; set; }

        public int? LIE_ID { get; set; }

        public int? AUT_ID { get; set; }

        public virtual ACQ_ACQUIT ACQ_ACQUIT { get; set; }

        public virtual AUT_AUTRE_MENTION AUT_AUTRE_MENTION { get; set; }

        public virtual LIE_LIEU_DE_PRODUCTION LIE_LIEU_DE_PRODUCTION { get; set; }

        public virtual ICollection<PED_PESEE_DETAIL> PED_PESEE_DETAIL { get; set; }

        public virtual RES_RESPONSABLE RES_RESPONSABLE { get; set; }
    }
}