﻿namespace GSN.GDV.Data.Models
{
    public abstract class Classe
    {
        public int ClasseId { get; set; }
        public string Nom { get; set; }
    }
}