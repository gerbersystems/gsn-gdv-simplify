namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PED_PESEE_DETAIL
    {
        public PED_PESEE_DETAIL()
        {
            LIP_LIGNE_PAIEMENT_PESEE = new HashSet<LIP_LIGNE_PAIEMENT_PESEE>();
        }

        [Key]
        public int PED_ID { get; set; }

        public int? CEP_ID { get; set; }

        public int? PEE_ID { get; set; }

        public int? PED_LOT { get; set; }

        public double? PED_SONDAGE_OE { get; set; }

        public double? PED_SONDAGE_BRIKS { get; set; }

        public double? PED_QUANTITE_KG { get; set; }

        public double? PED_QUANTITE_LITRES { get; set; }

        public int? ART_ID { get; set; }

        public virtual ART_ARTICLE ART_ARTICLE { get; set; }

        public virtual CEP_CEPAGE CEP_CEPAGE { get; set; }

        public virtual ICollection<LIP_LIGNE_PAIEMENT_PESEE> LIP_LIGNE_PAIEMENT_PESEE { get; set; }

        public virtual PEE_PESEE_ENTETE PEE_PESEE_ENTETE { get; set; }
    }
}