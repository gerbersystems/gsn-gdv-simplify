namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ADR_ADRESSE
    {
        public ADR_ADRESSE()
        {
            ANN_ANNEE = new HashSet<ANN_ANNEE>();
            PRE_PRESSOIR = new HashSet<PRE_PRESSOIR>();
            PRO_PROPRIETAIRE = new HashSet<PRO_PROPRIETAIRE>();
            RES_RESPONSABLE = new HashSet<RES_RESPONSABLE>();
        }

        [Key]
        public int ADR_ID { get; set; }

        [StringLength(50)]
        public string ADR_ADRESSE1 { get; set; }

        [StringLength(50)]
        public string ADR_ADRESSE2 { get; set; }

        [StringLength(50)]
        public string ADR_ADRESSE3 { get; set; }

        [StringLength(10)]
        public string ADR_NPA { get; set; }

        [StringLength(50)]
        public string ADR_VILLE { get; set; }

        [StringLength(50)]
        public string ADR_TEL { get; set; }

        [StringLength(50)]
        public string ADR_FAX { get; set; }

        [StringLength(50)]
        public string ADR_NATEL { get; set; }

        [StringLength(50)]
        public string ADR_EMAIL { get; set; }

        [StringLength(50)]
        public string ADR_PAYS { get; set; }

        public int? ADR_ID_WINBIZ { get; set; }

        public virtual ICollection<ANN_ANNEE> ANN_ANNEE { get; set; }

        public virtual ICollection<PRE_PRESSOIR> PRE_PRESSOIR { get; set; }

        public virtual ICollection<PRO_PROPRIETAIRE> PRO_PROPRIETAIRE { get; set; }

        public virtual ICollection<RES_RESPONSABLE> RES_RESPONSABLE { get; set; }
    }
}