namespace GSN.GDV.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class SET_SETTING
    {
        [Key]
        public int SET_ID { get; set; }

        [Required]
        [StringLength(64)]
        public string SET_NAME { get; set; }

        [StringLength(256)]
        public string SET_VALUE_STRING { get; set; }

        public int? SET_VALUE_INTEGER { get; set; }

        public double? SET_VALUE_DECIMAL { get; set; }

        public DateTime? SET_VALUE_DATETIME { get; set; }

        public bool? SET_VALUE_BOOLEAN { get; set; }

        public int SET_TYPE { get; set; }
    }
}