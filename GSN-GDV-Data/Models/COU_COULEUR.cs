namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class COU_COULEUR
    {
        public COU_COULEUR()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
            ART_ARTICLE = new HashSet<ART_ARTICLE>();
            CEP_CEPAGE = new HashSet<CEP_CEPAGE>();
            MCO_MOC_COU = new HashSet<MCO_MOC_COU>();
        }

        [Key]
        public int COU_ID { get; set; }

        [StringLength(50)]
        public string COU_NOM { get; set; }

        public bool? COU_CEPAGE { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual ICollection<ART_ARTICLE> ART_ARTICLE { get; set; }

        public virtual ICollection<CEP_CEPAGE> CEP_CEPAGE { get; set; }

        public virtual ICollection<MCO_MOC_COU> MCO_MOC_COU { get; set; }
    }
}