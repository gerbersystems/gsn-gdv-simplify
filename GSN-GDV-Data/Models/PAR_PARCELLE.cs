namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class PAR_PARCELLE
    {
        [Key]
        public int PAR_ID { get; set; }

        public int? CEP_ID { get; set; }

        public int? ACQ_ID { get; set; }

        [StringLength(50)]
        public string PAR_FOLIO { get; set; }

        public int? PAR_NUMERO { get; set; }

        public double? PAR_SURFACE_CEPAGE { get; set; }

        public double? PAR_QUOTA { get; set; }

        public double? PAR_LITRES { get; set; }

        public double? PAR_SURFACE_COMPTEE { get; set; }

        public int? LIE_ID { get; set; }

        public int? AUT_ID { get; set; }

        public int? ZOT_ID { get; set; }

        [StringLength(50)]
        public string PAR_SECTEUR_VISITE { get; set; }

        [StringLength(50)]
        public string PAR_MODE_CULTURE { get; set; }

        public virtual ACQ_ACQUIT ACQ_ACQUIT { get; set; }

        public virtual AUT_AUTRE_MENTION AUT_AUTRE_MENTION { get; set; }

        public virtual CEP_CEPAGE CEP_CEPAGE { get; set; }

        public virtual LIE_LIEU_DE_PRODUCTION LIE_LIEU_DE_PRODUCTION { get; set; }

        public virtual ZOT_ZONE_TRAVAIL ZOT_ZONE_TRAVAIL { get; set; }
    }
}