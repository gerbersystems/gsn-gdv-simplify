namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class QUO_QUOTA_PRODUCTION
    {
        [Key]
        [Column(Order = 0)]
        public int QUO_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CEP_ID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REG_ID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ANN_AN { get; set; }

        [Key]
        [Column(Order = 4)]
        public decimal QUO_AOC { get; set; }

        [Key]
        [Column(Order = 5)]
        public decimal QUO_PREMIER_GRANDS_CRUS { get; set; }

        [Key]
        [Column(Order = 6)]
        public decimal QUO_VINS_PAYS { get; set; }

        [Key]
        [Column(Order = 7)]
        public decimal QUO_VINS_TABLE { get; set; }
    }
}