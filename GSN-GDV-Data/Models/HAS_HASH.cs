namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class HAS_HASH
    {
        [Key]
        public int HAS_ID { get; set; }

        [Column("HAS_HASH")]
        [Required]
        [StringLength(50)]
        public string HAS_HASH1 { get; set; }
    }
}