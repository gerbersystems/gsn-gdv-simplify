namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class REP_REPORT
    {
        [Key]
        public int REP_ID { get; set; }

        [StringLength(50)]
        public string REP_NOM { get; set; }

        public int? REP_VERSION { get; set; }
    }
}