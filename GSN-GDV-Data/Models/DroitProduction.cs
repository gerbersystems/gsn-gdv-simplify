﻿namespace GSN.GDV.Data.Models
{
    using System;

    public abstract class DroitProduction
    {
        public int DroitProductionId { get; set; }
        public DateTime? Date { get; set; }
        public int AnneeId { get; set; }
    }
}