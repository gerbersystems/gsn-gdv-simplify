﻿namespace GSN.GDV.Data.Models
{
    using System;

    public class DroitProductionVd : DroitProduction
    {
        public string Numero { get; set; }
        public string NumeroComplementaire { get; set; }
        public DateTime? DateReception { get; set; }
        public int CouleurId { get; set; }
        public int CommuneId { get; set; }
        public int RegionId { get; set; }
    }
}