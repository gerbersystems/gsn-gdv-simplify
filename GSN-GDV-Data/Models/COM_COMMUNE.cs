namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class COM_COMMUNE
    {
        public COM_COMMUNE()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
            LIE_LIEU_DE_PRODUCTION = new HashSet<LIE_LIEU_DE_PRODUCTION>();
        }

        [Key]
        public int COM_ID { get; set; }

        [StringLength(50)]
        public string COM_NOM { get; set; }

        public int? COM_NUMERO { get; set; }

        public int? DIS_ID { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual DIS_DISTRICT DIS_DISTRICT { get; set; }

        public virtual ICollection<LIE_LIEU_DE_PRODUCTION> LIE_LIEU_DE_PRODUCTION { get; set; }
    }
}