namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class BON_BONUS
    {
        [Key]
        public int BON_ID { get; set; }

        public int VAL_ID { get; set; }

        public double BON_OE { get; set; }

        public double BON_BRIX { get; set; }

        [Column("BON_BONUS")]
        public double BON_VALEUR { get; set; }

        public short BON_TYPE { get; set; }

        public virtual VAL_VALEUR_CEPAGE VAL_VALEUR_CEPAGE { get; set; }
    }
}