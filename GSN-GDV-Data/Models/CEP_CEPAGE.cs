namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CEP_CEPAGE
    {
        public CEP_CEPAGE()
        {
            MCE_MOC_CEP = new HashSet<MCE_MOC_CEP>();
            PAR_PARCELLE = new HashSet<PAR_PARCELLE>();
            PED_PESEE_DETAIL = new HashSet<PED_PESEE_DETAIL>();
            VAL_VALEUR_CEPAGE = new HashSet<VAL_VALEUR_CEPAGE>();
        }

        [Key]
        public int CEP_ID { get; set; }

        public int? COU_ID { get; set; }

        [StringLength(50)]
        public string CEP_NOM { get; set; }

        public int? CEP_NUMERO { get; set; }

        public virtual COU_COULEUR COU_COULEUR { get; set; }

        public virtual ICollection<MCE_MOC_CEP> MCE_MOC_CEP { get; set; }

        public virtual ICollection<PAR_PARCELLE> PAR_PARCELLE { get; set; }

        public virtual ICollection<PED_PESEE_DETAIL> PED_PESEE_DETAIL { get; set; }

        public virtual ICollection<VAL_VALEUR_CEPAGE> VAL_VALEUR_CEPAGE { get; set; }
    }
}