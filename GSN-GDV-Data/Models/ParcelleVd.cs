﻿namespace GSN.GDV.Data.Models
{
    public class ParcelleVd : Parcelle
    {
        public string Folio { get; set; }
        public double? SurfaceComptee { get; set; }
        public double? DroitLitres { get; set; }
    }
}