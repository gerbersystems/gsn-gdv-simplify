namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class DEG_DEGRE_OECHSLE_BRIX
    {
        [Key]
        public int DEG_ID { get; set; }

        public double DEG_OECHSLE { get; set; }

        public double DEG_BRIX { get; set; }
    }
}