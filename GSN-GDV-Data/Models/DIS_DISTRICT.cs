namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class DIS_DISTRICT
    {
        public DIS_DISTRICT()
        {
            COM_COMMUNE = new HashSet<COM_COMMUNE>();
        }

        [Key]
        public int DIS_ID { get; set; }

        [StringLength(50)]
        public string DIS_NOM { get; set; }

        public virtual ICollection<COM_COMMUNE> COM_COMMUNE { get; set; }
    }
}