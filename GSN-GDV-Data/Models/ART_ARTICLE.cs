namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ART_ARTICLE
    {
        public ART_ARTICLE()
        {
            PED_PESEE_DETAIL = new HashSet<PED_PESEE_DETAIL>();
        }

        [Key]
        public int ART_ID { get; set; }

        public int ANN_ID { get; set; }

        public double? ART_LITRES { get; set; }

        [StringLength(40)]
        public string ART_DESIGNATION_COURTE { get; set; }

        [StringLength(500)]
        public string ART_DESIGNATION_LONGUE { get; set; }

        [StringLength(30)]
        public string ART_CODE { get; set; }

        [StringLength(25)]
        public string ART_CODE_GROUPE { get; set; }

        public int? COU_ID { get; set; }

        public int? ART_ID_WINBIZ { get; set; }

        public int? MOC_ID { get; set; }

        public double? ART_LITRES_STOCK { get; set; }

        public int? ART_VERSION { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE { get; set; }

        public virtual COU_COULEUR COU_COULEUR { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }

        public virtual ICollection<PED_PESEE_DETAIL> PED_PESEE_DETAIL { get; set; }
    }
}