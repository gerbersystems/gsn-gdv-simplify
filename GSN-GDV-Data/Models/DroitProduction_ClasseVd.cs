﻿namespace GSN.GDV.Data.Models
{
    public class DroitProduction_ClasseVd : DroitProduction_Classe
    {
        public double? LitresVinClair { get; set; }
    }
}