namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class MCE_MOC_CEP
    {
        [Key]
        public int MCE_ID { get; set; }

        public int MOC_ID { get; set; }

        public int CEP_ID { get; set; }

        public virtual CEP_CEPAGE CEP_CEPAGE { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }
    }
}