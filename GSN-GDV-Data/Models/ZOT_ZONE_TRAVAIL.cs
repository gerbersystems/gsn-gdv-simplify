namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class ZOT_ZONE_TRAVAIL
    {
        public ZOT_ZONE_TRAVAIL()
        {
            PAR_PARCELLE = new HashSet<PAR_PARCELLE>();
        }

        [Key]
        public int ZOT_ID { get; set; }

        [StringLength(100)]
        public string ZOT_DESCRIPTION { get; set; }

        public double? ZOT_VALEUR { get; set; }

        public virtual ICollection<PAR_PARCELLE> PAR_PARCELLE { get; set; }
    }
}