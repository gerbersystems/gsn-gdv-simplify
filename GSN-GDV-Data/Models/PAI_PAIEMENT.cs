namespace GSN.GDV.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PAI_PAIEMENT
    {
        public PAI_PAIEMENT()
        {
            LIM_LIGNE_PAIEMENT_MANUELLE = new HashSet<LIM_LIGNE_PAIEMENT_MANUELLE>();
            LIP_LIGNE_PAIEMENT_PESEE = new HashSet<LIP_LIGNE_PAIEMENT_PESEE>();
        }

        [Key]
        public int PAI_ID { get; set; }

        [StringLength(50)]
        public string PAI_TEXTE { get; set; }

        public DateTime? PAI_DATE { get; set; }

        public int? ANN_ID { get; set; }

        [StringLength(50)]
        public string PAI_NUMERO { get; set; }

        public int? PRO_ID { get; set; }

        public bool? PAI_ACOMPTE { get; set; }

        public int? PAI_ID_WINBIZ { get; set; }

        public double? PAI_POURCENTAGE { get; set; }

        public double? PAI_MONTANT_LIM_NET { get; set; }

        public double? PAI_MONTANT_LIM_TTC { get; set; }

        public double? PAI_MONTANT_LIP_NET { get; set; }

        public double? PAI_MONTANT_LIP_TTC { get; set; }

        public double? PAI_SOLDE { get; set; }

        public bool? PAI_BONUS_PAYE { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE { get; set; }

        public virtual ICollection<LIM_LIGNE_PAIEMENT_MANUELLE> LIM_LIGNE_PAIEMENT_MANUELLE { get; set; }

        public virtual ICollection<LIP_LIGNE_PAIEMENT_PESEE> LIP_LIGNE_PAIEMENT_PESEE { get; set; }

        public virtual PRO_PROPRIETAIRE PRO_PROPRIETAIRE { get; set; }
    }
}