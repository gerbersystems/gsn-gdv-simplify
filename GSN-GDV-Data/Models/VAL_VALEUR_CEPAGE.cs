namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class VAL_VALEUR_CEPAGE
    {
        public VAL_VALEUR_CEPAGE()
        {
            BON_BONUS = new HashSet<BON_BONUS>();
        }

        [Key]
        public int VAL_ID { get; set; }

        public int CEP_ID { get; set; }

        public int ANN_ID { get; set; }

        public double? VAL_VALEUR { get; set; }

        public int? LIE_ID { get; set; }

        public double? VAL_OECHSLE_MOYEN { get; set; }

        public int? VAL_BON_TYPE { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE { get; set; }

        public virtual ICollection<BON_BONUS> BON_BONUS { get; set; }

        public virtual CEP_CEPAGE CEP_CEPAGE { get; set; }

        public virtual LIE_LIEU_DE_PRODUCTION LIE_LIEU_DE_PRODUCTION { get; set; }
    }
}