namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class PRE_PRESSOIR
    {
        [Key]
        public int PRE_ID { get; set; }

        public int ADR_ID { get; set; }

        public virtual ADR_ADRESSE ADR_ADRESSE { get; set; }
    }
}