namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class LIP_LIGNE_PAIEMENT_PESEE
    {
        [Key]
        public int LIP_ID { get; set; }

        public int? PED_ID { get; set; }

        public int? PAI_ID { get; set; }

        public double? LIP_PRIX_KG { get; set; }

        public double? LIP_MONTANT_TTC { get; set; }

        public double? LIP_MONTANT_HT { get; set; }

        public int? MOC_ID { get; set; }

        public int? MOD_ID { get; set; }

        public double? LIP_TAUX_TVA { get; set; }

        public double? LIP_POURCENTAGE { get; set; }

        //LIP_TYPE type de ligne [pesee=0, bonus=1, manuelle=2, litresDeMout=3]
        public int? LIP_TYPE { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }

        public virtual MOD_MODE_TVA MOD_MODE_TVA { get; set; }

        public virtual PAI_PAIEMENT PAI_PAIEMENT { get; set; }

        public virtual PED_PESEE_DETAIL PED_PESEE_DETAIL { get; set; }
    }
}