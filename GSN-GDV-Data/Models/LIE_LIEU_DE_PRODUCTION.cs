namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class LIE_LIEU_DE_PRODUCTION
    {
        public LIE_LIEU_DE_PRODUCTION()
        {
            PAR_PARCELLE = new HashSet<PAR_PARCELLE>();
            PEE_PESEE_ENTETE = new HashSet<PEE_PESEE_ENTETE>();
            VAL_VALEUR_CEPAGE = new HashSet<VAL_VALEUR_CEPAGE>();
            COM_COMMUNE = new HashSet<COM_COMMUNE>();
        }

        [Key]
        public int LIE_ID { get; set; }

        [StringLength(50)]
        public string LIE_NOM { get; set; }

        public int? REG_ID { get; set; }

        public int? LIE_NUMERO { get; set; }

        public virtual REG_REGION REG_REGION { get; set; }

        public virtual ICollection<PAR_PARCELLE> PAR_PARCELLE { get; set; }

        public virtual ICollection<PEE_PESEE_ENTETE> PEE_PESEE_ENTETE { get; set; }

        public virtual ICollection<VAL_VALEUR_CEPAGE> VAL_VALEUR_CEPAGE { get; set; }

        public virtual ICollection<COM_COMMUNE> COM_COMMUNE { get; set; }
    }
}