namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class LIM_LIGNE_PAIEMENT_MANUELLE
    {
        [Key]
        public int LIM_ID { get; set; }

        [StringLength(50)]
        public string LIM_TEXTE { get; set; }

        public int? PAI_ID { get; set; }

        public double? LIM_MONTANT_HT { get; set; }

        public double? LIM_MONTANT_TTC { get; set; }

        public int? MOC_ID { get; set; }

        public int? MOD_ID { get; set; }

        public double? LIM_TAUX_TVA { get; set; }

        public virtual PAI_PAIEMENT PAI_PAIEMENT { get; set; }

        public virtual MOC_MODE_COMPTABILISATION MOC_MODE_COMPTABILISATION { get; set; }

        public virtual MOD_MODE_TVA MOD_MODE_TVA { get; set; }
    }
}