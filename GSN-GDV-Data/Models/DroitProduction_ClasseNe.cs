﻿namespace GSN.GDV.Data.Models
{
    public class DroitProduction_ClasseNe : DroitProduction_Classe
    {
        public int CepageId { get; set; }
        public double Kilos { get; set; }
    }
}