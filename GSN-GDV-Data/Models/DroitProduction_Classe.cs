﻿namespace GSN.GDV.Data.Models
{
    public abstract class DroitProduction_Classe
    {
        public int DroitProductionId { get; set; }
        public int ClasseId { get; set; }
        public double? Coefficient { get; set; }
    }
}