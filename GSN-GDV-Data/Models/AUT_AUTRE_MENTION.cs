namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class AUT_AUTRE_MENTION
    {
        public AUT_AUTRE_MENTION()
        {
            PAR_PARCELLE = new HashSet<PAR_PARCELLE>();
            PEE_PESEE_ENTETE = new HashSet<PEE_PESEE_ENTETE>();
        }

        [Key]
        public int AUT_ID { get; set; }

        [StringLength(50)]
        public string AUT_NOM { get; set; }

        public virtual ICollection<PAR_PARCELLE> PAR_PARCELLE { get; set; }

        public virtual ICollection<PEE_PESEE_ENTETE> PEE_PESEE_ENTETE { get; set; }
    }
}