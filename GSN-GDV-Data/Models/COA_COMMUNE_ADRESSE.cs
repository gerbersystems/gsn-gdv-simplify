namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class COA_COMMUNE_ADRESSE
    {
        [Key]
        public int COA_ID { get; set; }

        public int COA_NPA { get; set; }

        [Required]
        [StringLength(50)]
        public string COA_NOM { get; set; }
    }
}