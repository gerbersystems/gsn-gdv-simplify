namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MOC_MODE_COMPTABILISATION
    {
        public MOC_MODE_COMPTABILISATION()
        {
            ANN_ANNEE = new HashSet<ANN_ANNEE>();
            ART_ARTICLE = new HashSet<ART_ARTICLE>();
            LIM_LIGNE_PAIEMENT_MANUELLE = new HashSet<LIM_LIGNE_PAIEMENT_MANUELLE>();
            LIP_LIGNE_PAIEMENT_PESEE = new HashSet<LIP_LIGNE_PAIEMENT_PESEE>();
            MCE_MOC_CEP = new HashSet<MCE_MOC_CEP>();
            MCO_MOC_COU = new HashSet<MCO_MOC_COU>();
        }

        [Key]
        public int MOC_ID { get; set; }

        public int MOC_ID_WINBIZ { get; set; }

        [StringLength(50)]
        public string MOC_TEXTE { get; set; }

        public double? MOC_TAUX_TVA { get; set; }

        public int MOD_ID { get; set; }

        public int ANN_ID { get; set; }

        public virtual ICollection<ANN_ANNEE> ANN_ANNEE { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE1 { get; set; }

        public virtual ICollection<ART_ARTICLE> ART_ARTICLE { get; set; }

        public virtual ICollection<LIM_LIGNE_PAIEMENT_MANUELLE> LIM_LIGNE_PAIEMENT_MANUELLE { get; set; }

        public virtual ICollection<LIP_LIGNE_PAIEMENT_PESEE> LIP_LIGNE_PAIEMENT_PESEE { get; set; }

        public virtual ICollection<MCE_MOC_CEP> MCE_MOC_CEP { get; set; }

        public virtual ICollection<MCO_MOC_COU> MCO_MOC_COU { get; set; }

        public virtual MOD_MODE_TVA MOD_MODE_TVA { get; set; }
    }
}