﻿namespace GSN.GDV.Data.Models
{
    public abstract class Parcelle
    {
        public int ParcelleId { get; set; }
        public int? Numero { get; set; }
        public double? Surface { get; set; }
        public int PropietaireId { get; set; }
        public int? CepageId { get; set; }
        public int? DroitProductionId { get; set; }
        public int? LieuId { get; set; }
        public int? NomLocalId { get; set; }
        public int? GroupeId { get; set; }
        public int? SecteurId { get; set; }
        public int? ModeCultureId { get; set; }
    }
}