namespace GSN.GDV.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class REG_REGION
    {
        public REG_REGION()
        {
            ACQ_ACQUIT = new HashSet<ACQ_ACQUIT>();
            LIE_LIEU_DE_PRODUCTION = new HashSet<LIE_LIEU_DE_PRODUCTION>();
        }

        [Key]
        public int REG_ID { get; set; }

        [StringLength(50)]
        public string REG_NAME { get; set; }

        public virtual ICollection<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual ICollection<LIE_LIEU_DE_PRODUCTION> LIE_LIEU_DE_PRODUCTION { get; set; }
    }
}