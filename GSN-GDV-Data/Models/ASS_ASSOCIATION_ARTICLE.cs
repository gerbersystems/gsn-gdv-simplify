namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class ASS_ASSOCIATION_ARTICLE
    {
        [Key]
        public int ASS_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ASS_NOM_COLONNE { get; set; }

        public bool ASS_SELECTION { get; set; }
    }
}