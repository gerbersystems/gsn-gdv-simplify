namespace GSN.GDV.Data
{
    using System.ComponentModel.DataAnnotations;

    public partial class PAM_PARAMETRES
    {
        public int? ANN_ID { get; set; }

        [StringLength(50)]
        public string PAM_CLE { get; set; }

        [StringLength(50)]
        public string PAM_HASH_MACHINE { get; set; }

        public int? PAM_NUM_ENCAVEUR { get; set; }

        [MaxLength(50)]
        public byte[] PAM_EXPIRATION { get; set; }

        public int? PAM_VERSION_BD { get; set; }

        public int? PAM_LAST_LICENSE { get; set; }

        public string PAM_WINBIZ_ADRESSE_DEFAUT { get; set; }

        public string PAM_WINBIZ_ADRESSE_CURRENT { get; set; }

        [StringLength(50)]
        public string PAM_HASH_VERSION { get; set; }

        [MaxLength(50)]
        public byte[] PAM_EXPIRATION_UPDATE { get; set; }

        [MaxLength(50)]
        public byte[] PAM_NB_PRODUCTEUR { get; set; }

        [Key]
        public int PAM_ID { get; set; }

        [StringLength(50)]
        public string PAM_MACHINE_NAME { get; set; }

        public virtual ANN_ANNEE ANN_ANNEE { get; set; }
    }
}