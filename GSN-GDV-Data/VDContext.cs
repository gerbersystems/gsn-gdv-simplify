namespace GSN.GDV.Data
{
    using System.Data.Entity;

    public partial class VDContext : DbContext
    {
        public VDContext()
            : base()
        {
            Database.SetInitializer<VDContext>(new CreateDatabaseIfNotExists<VDContext>());
        }

        public virtual DbSet<ACQ_ACQUIT> ACQ_ACQUIT { get; set; }

        public virtual DbSet<ADR_ADRESSE> ADR_ADRESSE { get; set; }

        public virtual DbSet<ANN_ANNEE> ANN_ANNEE { get; set; }

        public virtual DbSet<ART_ARTICLE> ART_ARTICLE { get; set; }

        public virtual DbSet<ASS_ASSOCIATION_ARTICLE> ASS_ASSOCIATION_ARTICLE { get; set; }

        public virtual DbSet<AUT_AUTRE_MENTION> AUT_AUTRE_MENTION { get; set; }

        public virtual DbSet<BON_BONUS> BON_BONUS { get; set; }

        public virtual DbSet<CEP_CEPAGE> CEP_CEPAGE { get; set; }

        public virtual DbSet<CLA_CLASSE> CLA_CLASSE { get; set; }

        public virtual DbSet<COA_COMMUNE_ADRESSE> COA_COMMUNE_ADRESSE { get; set; }

        public virtual DbSet<COM_COMMUNE> COM_COMMUNE { get; set; }

        public virtual DbSet<COU_COULEUR> COU_COULEUR { get; set; }

        public virtual DbSet<DEG_DEGRE_OECHSLE_BRIX> DEG_DEGRE_OECHSLE_BRIX { get; set; }

        public virtual DbSet<DIS_DISTRICT> DIS_DISTRICT { get; set; }

        public virtual DbSet<HAS_HASH> HAS_HASH { get; set; }

        public virtual DbSet<LIE_LIEU_DE_PRODUCTION> LIE_LIEU_DE_PRODUCTION { get; set; }

        public virtual DbSet<LIM_LIGNE_PAIEMENT_MANUELLE> LIM_LIGNE_PAIEMENT_MANUELLE { get; set; }

        public virtual DbSet<LIP_LIGNE_PAIEMENT_PESEE> LIP_LIGNE_PAIEMENT_PESEE { get; set; }

        public virtual DbSet<MCE_MOC_CEP> MCE_MOC_CEP { get; set; }

        public virtual DbSet<MCO_MOC_COU> MCO_MOC_COU { get; set; }

        public virtual DbSet<MOC_MODE_COMPTABILISATION> MOC_MODE_COMPTABILISATION { get; set; }

        public virtual DbSet<MOD_MODE_TVA> MOD_MODE_TVA { get; set; }

        public virtual DbSet<PAI_PAIEMENT> PAI_PAIEMENT { get; set; }

        public virtual DbSet<PAM_PARAMETRES> PAM_PARAMETRES { get; set; }

        public virtual DbSet<PAR_PARCELLE> PAR_PARCELLE { get; set; }

        public virtual DbSet<PED_PESEE_DETAIL> PED_PESEE_DETAIL { get; set; }

        public virtual DbSet<PEE_PESEE_ENTETE> PEE_PESEE_ENTETE { get; set; }

        public virtual DbSet<PRE_PRESSOIR> PRE_PRESSOIR { get; set; }

        public virtual DbSet<PRO_PROPRIETAIRE> PRO_PROPRIETAIRE { get; set; }

        public virtual DbSet<REG_REGION> REG_REGION { get; set; }

        public virtual DbSet<RES_RESPONSABLE> RES_RESPONSABLE { get; set; }

        public virtual DbSet<SET_SETTING> SET_SETTING { get; set; }

        public virtual DbSet<VAL_VALEUR_CEPAGE> VAL_VALEUR_CEPAGE { get; set; }

        public virtual DbSet<ZOT_ZONE_TRAVAIL> ZOT_ZONE_TRAVAIL { get; set; }

        public virtual DbSet<QUO_QUOTA_PRODUCTION> QUO_QUOTA_PRODUCTION { get; set; }

        public virtual DbSet<REP_REPORT> REP_REPORT { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACQ_ACQUIT>()
                .HasMany(e => e.PAR_PARCELLE)
                .WithOptional(e => e.ACQ_ACQUIT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_ADRESSE1)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_ADRESSE2)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_ADRESSE3)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_NPA)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_VILLE)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_TEL)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_FAX)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_NATEL)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_EMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .Property(e => e.ADR_PAYS)
                .IsUnicode(false);

            modelBuilder.Entity<ADR_ADRESSE>()
                .HasMany(e => e.PRE_PRESSOIR)
                .WithRequired(e => e.ADR_ADRESSE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_ENCAVEUR_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_ENCAVEUR_PRENOM)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_RESPONSABLE_1)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_RESPONSABLE_2)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_RESPONSABLE_3)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_RESPONSABLE_4)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_ENCAVEUR_SOCIETE)
                .IsUnicode(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_TAUX_CONVERTION_BLANC)
                .HasPrecision(3, 2);

            modelBuilder.Entity<ANN_ANNEE>()
                .Property(e => e.ANN_TAUX_CONVERTION_ROUGE)
                .HasPrecision(3, 2);

            modelBuilder.Entity<ANN_ANNEE>()
                .HasMany(e => e.ACQ_ACQUIT)
                .WithOptional(e => e.ANN_ANNEE)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ANN_ANNEE>()
                .HasMany(e => e.ART_ARTICLE)
                .WithRequired(e => e.ANN_ANNEE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ANN_ANNEE>()
                .HasMany(e => e.MOC_MODE_COMPTABILISATION1)
                .WithRequired(e => e.ANN_ANNEE1)
                .HasForeignKey(e => e.ANN_ID);

            modelBuilder.Entity<ART_ARTICLE>()
                .Property(e => e.ART_DESIGNATION_COURTE)
                .IsUnicode(false);

            modelBuilder.Entity<ART_ARTICLE>()
                .Property(e => e.ART_DESIGNATION_LONGUE)
                .IsUnicode(false);

            modelBuilder.Entity<ART_ARTICLE>()
                .Property(e => e.ART_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<ART_ARTICLE>()
                .Property(e => e.ART_CODE_GROUPE)
                .IsUnicode(false);

            modelBuilder.Entity<ASS_ASSOCIATION_ARTICLE>()
                .Property(e => e.ASS_NOM_COLONNE)
                .IsUnicode(false);

            modelBuilder.Entity<AUT_AUTRE_MENTION>()
                .Property(e => e.AUT_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<CEP_CEPAGE>()
                .Property(e => e.CEP_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<CLA_CLASSE>()
                .Property(e => e.CLA_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<CLA_CLASSE>()
                .Property(e => e.CLA_NUMERO_CSCV)
                .IsUnicode(false);

            modelBuilder.Entity<COA_COMMUNE_ADRESSE>()
                .Property(e => e.COA_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<COM_COMMUNE>()
                .Property(e => e.COM_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<COM_COMMUNE>()
                .HasMany(e => e.LIE_LIEU_DE_PRODUCTION)
                .WithMany(e => e.COM_COMMUNE)
                .Map(m => m.ToTable("LIC_LIEU_COMMUNE").MapLeftKey("COM_ID").MapRightKey("LIE_ID"));

            modelBuilder.Entity<COU_COULEUR>()
                .Property(e => e.COU_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<DIS_DISTRICT>()
                .Property(e => e.DIS_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<LIE_LIEU_DE_PRODUCTION>()
                .Property(e => e.LIE_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<LIM_LIGNE_PAIEMENT_MANUELLE>()
                .Property(e => e.LIM_TEXTE)
                .IsUnicode(false);

            modelBuilder.Entity<MOC_MODE_COMPTABILISATION>()
                .Property(e => e.MOC_TEXTE)
                .IsUnicode(false);

            modelBuilder.Entity<MOC_MODE_COMPTABILISATION>()
                .HasMany(e => e.ANN_ANNEE)
                .WithOptional(e => e.MOC_MODE_COMPTABILISATION)
                .HasForeignKey(e => e.MOC_ID);

            modelBuilder.Entity<MOD_MODE_TVA>()
                .Property(e => e.MOD_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<MOD_MODE_TVA>()
                .HasMany(e => e.MOC_MODE_COMPTABILISATION)
                .WithRequired(e => e.MOD_MODE_TVA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PAI_PAIEMENT>()
                .Property(e => e.PAI_TEXTE)
                .IsUnicode(false);

            modelBuilder.Entity<PAI_PAIEMENT>()
                .Property(e => e.PAI_NUMERO)
                .IsUnicode(false);

            modelBuilder.Entity<PAI_PAIEMENT>()
                .HasMany(e => e.LIM_LIGNE_PAIEMENT_MANUELLE)
                .WithOptional(e => e.PAI_PAIEMENT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PAI_PAIEMENT>()
                .HasMany(e => e.LIP_LIGNE_PAIEMENT_PESEE)
                .WithOptional(e => e.PAI_PAIEMENT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_CLE)
                .IsUnicode(false);

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_HASH_MACHINE)
                .IsUnicode(false);

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_WINBIZ_ADRESSE_DEFAUT)
                .IsUnicode(false);

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_WINBIZ_ADRESSE_CURRENT)
                .IsUnicode(false);

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_HASH_VERSION)
                .IsUnicode(false);

            modelBuilder.Entity<PAM_PARAMETRES>()
                .Property(e => e.PAM_MACHINE_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PAR_PARCELLE>()
                .Property(e => e.PAR_FOLIO)
                .IsUnicode(false);

            modelBuilder.Entity<PEE_PESEE_ENTETE>()
                .Property(e => e.PEE_LIEU)
                .IsUnicode(false);

            modelBuilder.Entity<PEE_PESEE_ENTETE>()
                .HasMany(e => e.PED_PESEE_DETAIL)
                .WithOptional(e => e.PEE_PESEE_ENTETE)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .Property(e => e.PRO_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .Property(e => e.PRO_PRENOM)
                .IsUnicode(false);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .Property(e => e.PRO_TITRE)
                .IsUnicode(false);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .Property(e => e.PRO_SCTE)
                .IsUnicode(false);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .Property(e => e.PRO_NUMERO_TVA)
                .IsUnicode(false);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .HasMany(e => e.ACQ_ACQUIT)
                .WithOptional(e => e.PRO_PROPRIETAIRE)
                .HasForeignKey(e => e.PRO_ID);

            modelBuilder.Entity<PRO_PROPRIETAIRE>()
                .HasMany(e => e.ACQ_ACQUIT1)
                .WithOptional(e => e.PRO_PROPRIETAIRE1)
                .HasForeignKey(e => e.PRO_ID_VENDANGE);

            modelBuilder.Entity<REG_REGION>()
                .Property(e => e.REG_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<RES_RESPONSABLE>()
                .Property(e => e.RES_NOM)
                .IsUnicode(false);

            modelBuilder.Entity<RES_RESPONSABLE>()
                .Property(e => e.RES_PRENOM)
                .IsUnicode(false);

            modelBuilder.Entity<QUO_QUOTA_PRODUCTION>()
                .Property(e => e.QUO_AOC)
                .HasPrecision(3, 2);

            modelBuilder.Entity<QUO_QUOTA_PRODUCTION>()
                .Property(e => e.QUO_PREMIER_GRANDS_CRUS)
                .HasPrecision(3, 2);

            modelBuilder.Entity<QUO_QUOTA_PRODUCTION>()
                .Property(e => e.QUO_VINS_PAYS)
                .HasPrecision(3, 2);

            modelBuilder.Entity<QUO_QUOTA_PRODUCTION>()
                .Property(e => e.QUO_VINS_TABLE)
                .HasPrecision(3, 2);

            modelBuilder.Entity<REP_REPORT>()
                .Property(e => e.REP_NOM)
                .IsUnicode(false);
        }
    }
}