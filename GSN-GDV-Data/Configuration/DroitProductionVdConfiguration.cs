﻿namespace GSN.GDV.Data.Configuration
{
    using Models;
    using System.Data.Entity.ModelConfiguration;

    internal class DroitProductionVdConfiguration : EntityTypeConfiguration<DroitProductionVd>
    {
        public DroitProductionVdConfiguration()
        {
            HasKey(d => d.DroitProductionId);
        }
    }
}