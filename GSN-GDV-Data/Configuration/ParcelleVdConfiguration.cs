﻿namespace GSN.GDV.Data.Configuration
{
    using Models;
    using System.Data.Entity.ModelConfiguration;

    internal class ParcelleVdConfiguration : EntityTypeConfiguration<ParcelleVd>
    {
        public ParcelleVdConfiguration()
        {
            HasKey(p => p.ParcelleId);
        }
    }
}