﻿namespace GSN.GDV.Data.Configuration
{
    using Models;
    using System.Data.Entity.ModelConfiguration;

    internal class ClasseVdConfiguration : EntityTypeConfiguration<ClasseVd>
    {
        public ClasseVdConfiguration()
        {
            HasKey(c => c.ClasseId);
        }
    }
}