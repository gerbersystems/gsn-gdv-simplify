﻿namespace GSN.GDV.Data.Contexts
{
    using Models;
    using System.Data.Entity;

    public class VdContext : DbContext
    {
        public DbSet<ParcelleVd> Parcelles { get; set; }
        public DbSet<DroitProductionVd> DroitsProduction { get; set; }
        public DbSet<ClasseVd> Classes { get; set; }
        public DbSet<DroitProduction_ClasseVd> DroitsProduction_Classes { get; set; }

        public VdContext()
            : base("Default")
        {
            //Database.SetInitializer<VdContext>(new DropCreateDatabaseAlways<VdContext>());
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Configurations.Add(new ParcelleVdConfiguration());
        //    modelBuilder.Configurations.Add(new DroitProductionVdConfiguration());
        //    modelBuilder.Configurations.Add(new ClasseVdConfiguration());
        //    modelBuilder.Configurations.Add(new DroitProduction_ClasseVdConfiguration());
        //}
    }
}