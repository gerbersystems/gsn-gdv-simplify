﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSN.GDV.Data
{
    public class VdDbInitializer : CreateDatabaseIfNotExists<VDContext>
    {
        protected override void Seed(VDContext context)
        {
            context.CEP_CEPAGE.Add(new CEP_CEPAGE
            {
                CEP_NOM = "test",
                CEP_NUMERO = 123
            });
            base.Seed(context);
        }
    }
}